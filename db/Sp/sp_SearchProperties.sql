if exists(select * from sys.objects where type = 'p' and name = 'sp_SearchProperties' )
begin
	drop procedure sp_SearchProperties
end
GO

CREATE PROCEDURE [dbo].[sp_SearchProperties]( 
	@destinationId int,
	@dateFrom datetime,
	@dateTo datetime,
	@numberOfGuests int = NULL,
	@numberOfBathrooms int = NULL,
	@numberOfBedrooms int = NULL,
	@selectedPropertyTypes xml = NULL,
	@selectedAmenities xml = NULL,
	@selectedTags xml = NULL,
	@beginLatitude decimal(9,6) = NULL,
	@endLatitude decimal(9,6) = NULL,
	@beginLongitude decimal(9,6) = NULL,
	@endLongitude decimal(9,6) = NULL,
	@languageCode varchar(5))
AS
BEGIN
	DECLARE @lengthOfStay int
	DECLARE @fsTreshhold FLOAT 
	SELECT @fsTreshhold = CAST(CAST(SettingValue AS nvarchar(MAX)) AS FLOAT) FROM OTP_Settings WHERE SettingCode = 'FuzzySearch.Threshold'
	DECLARE @pricePerNight Bit = 0	
	IF(@dateFrom IS NULL AND @dateTo IS NULL)
		SET @pricePerNight = 1
	SET @lengthOfStay = DATEdiff(day, @dateFrom, @dateTo);

	SELECT * FROM
	( 
		SELECT	p.PropertyID AS ID,
				u.UnitID,
				u.MaxNumberOfBathrooms AS NumberOfBathrooms,
				u.MaxNumberOfBedRooms AS NumberOfBedrooms,
				u.MaxNumberOfGuests AS NumberOfGuests,
				dbo.fn_GetPropertyImagesByType(p.PropertyID, 2) AS Image,
				dbo.fn_GetPropertyImagesByType(p.PropertyID, 4) AS Thumbnails,
				p.PropertyName_i18n.value('(/i18nString/i18nContent/i18nPair[Code=sql:variable("@languageCode")]/Content)[1]', 'varchar(100)') AS Title,
				p.Short_Description_i18n.value('(/i18nString/i18nContent/i18nPair[Code=sql:variable("@languageCode")]/Content)[1]', 'varchar(100)') AS Description,
				p.Latitude, 
				p.Longitude,
				dbo.fn_CalculateFinalScore(p.PropertyID, u.UnitID, @numberOfGuests, @numberOfBedrooms, @numberOfBathrooms, @selectedPropertyTypes, @selectedAmenities, @selectedTags) AS FinalScore,
				@pricePerNight AS IsPricePerNight,
				p.StarRating AS Rating,
				dbo.fn_CalculateUnitPrice(u.UnitID, @dateFrom, @dateTo) AS Price,
				dbo.fn_GetPropertyCommision(p.PropertyID, @dateFrom) AS Commission,
				dbo.fn_GetMlosByPeriod(u.UnitTypeId, @dateFrom, @dateTo) AS MLOS,
				@languageCode AS LanguageCode,
                dc.CultureCode,
				dest.Destination_i18n.value('(/i18nString/i18nContent/i18nPair[Code=sql:variable("@languageCode")]/Content)[1]', 'varchar(100)') as Destination,
				dbo.fn_CheckRatesForPeriod(u.UnitID, @dateFrom, @dateTo) AS UnitRatesDefined,
				ROW_NUMBER() OVER (PARTITION BY p.PropertyID ORDER BY dbo.fn_GetBestMatching(u.UnitID, @numberOfGuests, @numberOfBedrooms, @numberOfBathrooms)) AS UnitRank
		
		FROM	Properties p 
				JOIN Units u on u.PropertyID = p.PropertyID
				JOIN UnitTypes ut on ut.PropertyID = p.PropertyID				
                JOIN DictionaryCultures dc on dc.CultureId = p.CultureId
				JOIN Destinations dest on dest.DestinationID = p.OTP_DestinationID
		WHERE	p.PropertyLive = 1 
				AND dest.Active = 1 
				AND p.OTP_DestinationID = ISNULL(@destinationId, p.OTP_DestinationID)
				AND dbo.fn_GetUnitTypeCTA(u.UnitTypeID, @dateFrom) = 0
				AND u.UnitID NOT in
				(
					SELECT	UnitID
					FROM	UnitInvBlockings bl
					WHERE	UnitID = u.UnitID AND
					(				
						(@dateTo IS NOT NULL) AND (@dateFrom IS NOT NULL) AND
						(DATEADD(day, 1, bl.DateFrom) BETWEEN @dateFrom AND @dateTo) OR 
						(DATEADD(day, -1, bl.DateUntil) BETWEEN @dateFrom AND @dateTo) OR
						((@dateFrom >= bl.DateFrom) AND (@dateTo <= bl.DateUntil))				
					)
				)
				AND 
				(
					(
						 @beginLatitude IS NOT NULL AND 
						 @endLatitude IS NOT NULL AND 
						 @beginLongitude IS  NOT NULL AND 
						 @endLongitude IS NOT NULL AND
						 p.Latitude BETWEEN @beginLatitude AND @endLatitude AND
						 p.Longitude BETWEEN @beginLongitude AND @endLongitude
					) 
					OR
					(
						@beginLatitude IS NULL AND 
						@endLatitude IS NULL AND 
						@beginLongitude IS NULL AND 
						@endLongitude IS NULL
					)	
				)
	) AS Source
	WHERE		UnitRank = 1
				AND FinalScore > = @fsTreshhold
				AND (ISNULL(MLOS, 0) <= ISNULL(@lengthOfStay, 0))
				AND UnitRatesDefined = 1
	ORDER BY	FinalScore desc , Commission desc, Rating desc, Price desc
END