if exists(select * from sys.objects where type = 'p' and name = 'sp_GetUnitAvailability' )
begin
	drop procedure sp_GetUnitAvailability
end
GO

CREATE PROCEDURE [dbo].[sp_GetUnitAvailability]( 	
	@dateFrom datetime,
	@dateTo datetime,
	@unitId integer
	)
AS
BEGIN

	WITH T(monthDay, priceValue, isBlocked, unitBlockingType, mlos, cta)
	AS
	( 
		SELECT @DateFrom, 
			   dbo.fn_CalculateUnitPriceAccomodation(@unitId, @dateFrom, DateAdd(day,1,@dateFrom)), 
			   dbo.fn_GetUnitBlockings(@unitId, @dateFrom ),
			   dbo.fn_GetUnitBlockingType(@unitId, @dateFrom ),
			   dbo.fn_GetMlosByDate(@dateFrom,@unitId),
			   dbo.fn_GetUnitTypeCTA(@unitId,@dateFrom)
		UNION ALL
		SELECT DateAdd(day,1,T.monthDay), 
			   dbo.fn_CalculateUnitPriceAccomodation(@unitId, DateAdd(day,1,T.monthDay), 
			   DateAdd(day,2,T.monthDay)),
			   dbo.fn_GetUnitBlockings(@unitId, DateAdd(day,1,T.monthDay)),
			   dbo.fn_GetUnitBlockingType(@unitId, DateAdd(day,1,T.monthDay)),
			   dbo.fn_GetMlosByDate(DateAdd(day,1,T.monthDay),@unitId),
			   dbo.fn_GetUnitTypeCTA(@unitId,DateAdd(day,1,T.monthDay))
		FROM T 
		WHERE T.monthDay < @dateTo
	)

	SELECT monthDay, priceValue, isBlocked, unitBlockingType, mlos, cta 
	FROM T OPTION (MAXRECURSION 32767);
END







