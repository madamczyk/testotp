if exists(select * from sys.objects where type = 'p' and name = 'sp_GetFinalScoreDetails' )
begin
	drop procedure sp_GetFinalScoreDetails
end
GO

CREATE PROCEDURE [dbo].[sp_GetFinalScoreDetails]
(		
	@unitId int,
	@propertyId int,
	@numberOfGuests int = NULL,
	@numberOfBathrooms int = NULL,
	@numberOfBedrooms int = NULL,
	@selectedPropertyTypes xml = NULL,
	@selectedAmenities xml = NULL,
	@selectedTags xml = NULL
)
AS
BEGIN	
	SET NOCOUNT ON;
	DECLARE @matchingPropertyTypes INT 
	DECLARE @nonMatchingPropertyTypes NVARCHAR(MAX) 
	DECLARE @nonMatchingAmenities NVARCHAR(MAX) 
	DECLARE @nonMatchingTags NVARCHAR(MAX) 

	DECLARE @guests INT = NULL
	DECLARE @bedrooms INT  = NULL
	DECLARE @bathrooms INT  = NULL

	DECLARE @selectedPropertyTypesTable TABLE (ID int) 
	DECLARE @selectedAmenitiesTable TABLE (ID int) 
	DECLARE @selectedTagsTable TABLE (ID int)

	INSERT INTO @selectedPropertyTypesTable (ID) SELECT ParamValues.ID.value('@id','VARCHAR(20)') FROM @selectedPropertyTypes.nodes('/PropertyTypes/Item') as ParamValues(ID)
	INSERT INTO @selectedAmenitiesTable (ID) SELECT ParamValues.ID.value('@id','VARCHAR(20)') FROM @selectedAmenities.nodes('/Amenities/Item') as ParamValues(ID)
	INSERT INTO @selectedTagsTable (ID) SELECT ParamValues.ID.value('@id','VARCHAR(20)') FROM @selectedTags.nodes('/Tags/Item') as ParamValues(ID)
			
	--non matching amenities (ids)
	SELECT @nonMatchingAmenities = COALESCE(@nonMatchingAmenities + ';', '') + CAST(sA.ID as VARCHAR)
	FROM @selectedAmenitiesTable sA
	WHERE sA.ID NOT IN 
	(	
		SELECT PA.AmenityID
		FROM @selectedAmenitiesTable sA
		JOIN PropertyAmenities PA ON sA.ID = PA.AmenityID
		WHERE PA.PropertyID = @propertyId 
	)

	--non matching tags (ids)
	SELECT @nonMatchingTags = COALESCE(@nonMatchingTags + ';', '') + CAST(sPT.ID as VARCHAR)
	FROM @selectedTagsTable sPT
	WHERE sPT.ID NOT IN 
	(	
		SELECT PT.TagID
		FROM @selectedAmenitiesTable sA
		JOIN PropertyTags PT ON sPT.ID = PT.TagID
		WHERE PT.PropertyID = @propertyId 
	)

	--matching propertyTypes
	SELECT @matchingPropertyTypes = COUNT(*) 
	FROM @selectedPropertyTypesTable sPT
	JOIN Properties Pr ON sPT.ID = Pr.PropertyTypeId 
	WHERE Pr.PropertyID = @propertyId

	IF(@matchingPropertyTypes = 0)
	BEGIN
		SELECT @nonMatchingPropertyTypes = COALESCE(@nonMatchingPropertyTypes + ';', '') + CAST(sPT.ID as VARCHAR)  
		FROM @selectedPropertyTypesTable sPT
	END

	SELECT	@guests = unit.MaxNumberOfGuests,
			@bedrooms = unit.MaxNumberOfBedRooms,
			@bathrooms = unit.MaxNumberOfBathrooms
	FROM	Units unit
	WHERE	unit.UnitID = @unitId

	IF((@numberOfGuests IS NOT NULL AND @guests = @numberOfGuests) OR (@numberOfGuests IS NULL))
		SET @guests = NULL
	IF((@numberOfBedrooms IS NOT NULL AND @bedrooms = @numberOfBedrooms) OR (@numberOfBedrooms IS NULL))
		SET @bedrooms = NULL
	IF((@numberOfBathrooms IS NOT NULL AND @bathrooms = @numberOfBathrooms) OR (@numberOfBathrooms IS NULL))
		SET @bathrooms = NULL

	SELECT	
	--Final Score
	dbo.fn_CalculateFinalScore(@propertyId, @unitId, @numberOfGuests, @numberOfBedrooms, @numberOfBathrooms, @selectedPropertyTypes, @selectedAmenities, @selectedTags) AS FinalScore,
	@guests AS UnitGuests,
	@bedrooms  AS UnitBedrooms,
	@bathrooms AS UnitBathrooms,
	@nonMatchingPropertyTypes AS NotMatchingPropertyTypes,
	@nonMatchingAmenities AS NotMatchingAmenities,
	@nonMatchingTags AS NotMatchingTags 
	--
	--FROM	Units u
	--WHERE	u.UnitID = @unitId
		
END