if exists(select * from sys.objects where type = 'p' and name = 'sp_SearchForMyTrips' )
begin
	drop procedure sp_SearchForMyTrips
end
GO

CREATE PROCEDURE [dbo].[sp_SearchForMyTrips]( 
	@userId int)
AS
BEGIN 
	SELECT * FROM
	( 
		SELECT  dbo.fn_GetPropertyImagesByType(p.PropertyID, 0) AS Thumbnails, r.*
		FROM	Reservations r JOIN Properties p on r.PropertyID = p.PropertyID
		WHERE	r.GuestID = @userId
				AND r.BookingStatus <> 6 -- withdrew 
	) AS Source
END

