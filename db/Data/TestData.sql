declare @i18nXmlPattern nvarchar(max)
declare @i18nXmlEnglishPair nvarchar(max)
declare @i18nXmlPolishPair nvarchar(max)
declare @usTimeZoneEastern nvarchar(max)

set @i18nXmlPattern = '<?xml version="1.0" encoding="utf-16"?><i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent>#pair#</i18nContent></i18nString>'
set @i18nXmlEnglishPair = '<i18nPair><Code>en-US</Code><Content>#phrase#</Content></i18nPair>'
set @i18nXmlPolishPair = '<i18nPair><Code>pl-PL</Code><Content>#phrase#</Content></i18nPair>'
set @usTimeZoneEastern = 'Eastern Standard Time'

-- Globals
declare @countryId INT, @roleGuest INT, @roleOwner INT, @roleManager INT, @roleAdmin INT, @propertyTypeId INT, @ownerId INT, @cultureEnUsId INT

set @countryId = (SELECT [CountryId] FROM [dbo].[DictionaryCountries] WHERE [CountryCode2Letters] = N'us')
SET @roleGuest = (SELECT [RoleId] FROM [dbo].[Roles] WHERE [Name] = N'Guest')
SET @roleManager = (SELECT [RoleId] FROM [dbo].[Roles] WHERE [Name] = N'Upload Manager')
SET @roleAdmin = (SELECT [RoleId] FROM [dbo].[Roles] WHERE [Name] = N'Administrator')
SET @roleOwner = (SELECT [RoleId] FROM [dbo].[Roles] WHERE [Name] = N'Owner')
SET @propertyTypeId = (SELECT [PropertyTypeId] FROM [dbo].[PropertyType] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en"]/Content)[1]', 'nvarchar(100)') = N'House')
SET @cultureEnUsId = (SELECT [CultureId] FROM [dbo].[DictionaryCultures] WHERE [CultureCode] = N'en-US')

-- Taxes
SET IDENTITY_INSERT [dbo].[Taxes] ON

INSERT [dbo].[Taxes] ([TaxId], [DestinationId], [Name_i18n], [Percentage]) VALUES
	(1, 1, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(2, 2, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(3, 3, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(4, 4, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(5, 5, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(6, 6, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(7, 7, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(8, 8, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(9, 8, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(10, 10, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(11, 11, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(12, 12, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(13, 13, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(14, 14, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(15, 15, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(16, 16, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(17, 17, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(18, 18, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(19, 19, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23),
	(20, 20, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Value Added Tax 23%')), 23)

SET IDENTITY_INSERT [dbo].[Taxes] OFF

-- Users
SET IDENTITY_INSERT [dbo].[Users] ON

INSERT [dbo].[Users] ([UserID], [Lastname], [Firstname], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [CellPhone], [LandLine], [email], [DriverLicenseNbr], [PassportNbr], [SocialsecurityNbr], [DirectDepositInfo], [SecurityQuestion], [SecurityAnswer], [AcceptedTCs], [AcceptedTCsInitials], [Password], [SendMePromotions], [SendInfoFavoritePlaces], [SendNews], [CultureId]) VALUES
	(1, N'Admin', N'LGBS', NULL, NULL, NULL, NULL, NULL, @countryId, NULL, NULL, N'test@test.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1A1DC91C907325C69271DDF0C944BC72', NULL, NULL, NULL, @cultureEnUsId),
	(2, N'Manager', N'OTP', NULL, NULL, NULL, NULL, NULL, @countryId, NULL, NULL, N'manager@test.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1A1DC91C907325C69271DDF0C944BC72', NULL, NULL, NULL, @cultureEnUsId),
	(3, N'LGBS', N'GUEST', N'268 Rolland Drive, Unit 12', NULL, N'Virginia', N'76001', N'Arlington', @countryId, N'2023650092', N'202-505-4221', N'guest@test.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1A1DC91C907325C69271DDF0C944BC72', 0, 1, 0, @cultureEnUsId),
	(4, N'LGBS', N'Owner', N'268 Rolland Drive, Unit 22', NULL, N'Kansas', N'76001', N'Somewhere', @countryId, N'2023650092', N'202-505-4221', N'owner@test.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1A1DC91C907325C69271DDF0C944BC72', 0, 0, 0, @cultureEnUsId)

SET IDENTITY_INSERT [dbo].[Users] OFF

SET @ownerId = 4

-- UserRoles
INSERT [dbo].[UserRoles] ([UserId], [RoleId], [Status], [ActivationToken]) VALUES
	(1, @roleAdmin, 3, null),
	(2, @roleManager, 3, null),
	(3, @roleGuest, 3, null),
	(4, @roleOwner, 3, null)

-- Properties
------ Property no 1
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut],[KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (1, @ownerId, 2, N'tst-1', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>House on Hills</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Nice and cozy apartment for two</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId,  32.3667, -86.3000, 1, 1, 15.00, '14:00', '12:00',0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (1, 1, N'unit-type code 1', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 1 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 1 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (1, 1, 1, N'unit code 1', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 1 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 1 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES (1, N'2012-10-10 00:00:00.000', N'2012-10-16 00:00:00.000', 1, NULL)
INSERT [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES (1, N'2012-10-27 00:00:00.000', N'2012-10-30 00:00:00.000', 1, NULL)

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (1, 0, NULL, N'/prop1/thumbs/thumb_house_1.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (1, 0, NULL, N'/prop1/thumbs/thumb_house_2.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (1, 0, NULL, N'/prop1/thumbs/thumb_house_3.jpg')

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (1, 4, NULL, N'/prop1/thumbs/thumb_house_1.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (1, 4, NULL, N'/prop1/thumbs/thumb_house_2.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (1, 4, NULL, N'/prop1/thumbs/thumb_house_3.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (1, 2, NULL, N'/prop1/house_1.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (1,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (1, '2012-01-01', '2020-01-01', 200)


------ Property no 2
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut],[KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (2, @ownerId, 2, N'tst-2', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Cozy Apartment</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Short, catchy description</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 32.4072, -87.0211, 1, 1, 15.00, '14:00', '12:00',0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (2, 2, N'unit-type code 2', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 2 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 2 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (2, 2, 2, N'unit code 2', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 2 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 2 desc')), 1, 3, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (2, 0, NULL, N'/prop2/thumbs/thumb_house_4.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (2, 0, NULL, N'/prop2/thumbs/thumb_house_5.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (2, 0, NULL, N'/prop2/thumbs/thumb_house_6.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (2, 4, NULL, N'/prop2/thumbs/thumb_house_4.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (2, 4, NULL, N'/prop2/thumbs/thumb_house_5.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (2, 4, NULL, N'/prop2/thumbs/thumb_house_6.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (2, 2, NULL, N'/prop2/house_2.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (2,5,@ownerId)

INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (2, '2012-01-01', '2020-01-01', 200)

------ Property no 3
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (3, @ownerId, 2, N'tst-3', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Shiny House</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Aparmentment with beautiful views</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 30.7619, -86.5706, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (3, 3, N'unit-type code 3', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 3 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 3 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (3, 3, 3, N'unit code 3', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 3 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 3 desc')), 1, 5, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES (3, N'2012-11-01 00:00:00.000', N'2012-11-16 00:00:00.000', 1, NULL)

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (3, 0, NULL, N'/prop3/thumbs/thumb_house_7.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (3, 0, NULL, N'/prop3/thumbs/thumb_house_8.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (3, 4, NULL, N'/prop3/thumbs/thumb_house_7.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (3, 4, NULL, N'/prop3/thumbs/thumb_house_8.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (3, 2, NULL, N'/prop3/house_3.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (3,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (3, '2012-01-01', '2020-01-01', 200)
------ Property no 4
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (4, @ownerId, 2, N'tst-4', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Calm Villa</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>House under the rainbow</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 31.0236, -87.4939, 1, 1, 15.00, '14:00', '12:00', 1, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (4, 4, N'unit-type code 4', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 4 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 4 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms],[KeysLockboxCode],[KeysLockboxLocation]) VALUES (4, 4, 4, N'unit code 4', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 4 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 4 desc')), 1, 2, 3, 1,'1234','In garden')
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES (4, N'2012-10-10 00:00:00.000', N'2012-12-10 00:00:00.000', 1, NULL)

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (4, 0, NULL, N'/prop4/thumbs/thumb_house_9.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (4, 0, NULL, N'/prop4/thumbs/thumb_house_10.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (4, 4, NULL, N'/prop4/thumbs/thumb_house_9.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (4, 4, NULL, N'/prop4/thumbs/thumb_house_10.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (4, 2, NULL, N'/prop4/house_4.jpg')

INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (4, '2012-01-01', '2020-01-01', 200)

------ Property no 5
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (5, @ownerId, 3, N'tst-5', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>House on Hills</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Nice and cozy apartment for two</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 64.8378, -147.7164, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (5, 5, N'unit-type code 5', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 5 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 5 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (5, 5, 5, N'unit code 5', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 5 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 5 desc')), 1, 5, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (5, 0, NULL, N'/prop5/thumbs/thumb_house_11.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (5, 0, NULL, N'/prop5/thumbs/thumb_house_12.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (5, 0, NULL, N'/prop5/thumbs/thumb_house_13.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (5, 4, NULL, N'/prop5/thumbs/thumb_house_11.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (5, 4, NULL, N'/prop5/thumbs/thumb_house_12.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (5, 4, NULL, N'/prop5/thumbs/thumb_house_13.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (5, 2, NULL, N'/prop5/house_5.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (5,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (5, '2012-01-01', '2020-01-01', 200)

------ Property no 6
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (6, @ownerId, 3, N'tst-6', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Cozy Apartment</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Short, catchy description</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 61.1919, -149.7621, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (6, 6, N'unit-type code 6', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 6 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 6 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (6, 6, 6, N'unit code 6', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 6 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 6 desc')), 1, 6, 2, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (6, 0, NULL, N'/prop6/thumbs/thumb_house_14.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (6, 0, NULL, N'/prop6/thumbs/thumb_house_15.jpg')

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (6, 4, NULL, N'/prop6/thumbs/thumb_house_14.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (6, 4, NULL, N'/prop6/thumbs/thumb_house_15.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (6, 2, NULL, N'/prop6/house_6.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (6,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (6, '2012-01-01', '2020-01-01', 200)

------ Property no 7
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (7, @ownerId, 4, N'tst-7', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Shiny House</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Aparmentment with beautiful views</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 33.2654, -112.0426, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (7, 7, N'unit-type code 7', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 7 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 7 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (7, 7, 7, N'unit code 7', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 7 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 7 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (7, 0, NULL, N'/prop7/thumbs/thumb_house_16.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (7, 0, NULL, N'/prop7/thumbs/thumb_house_17.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (7, 0, NULL, N'/prop7/thumbs/thumb_house_18.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (7, 4, NULL, N'/prop7/thumbs/thumb_house_16.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (7, 4, NULL, N'/prop7/thumbs/thumb_house_17.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (7, 4, NULL, N'/prop7/thumbs/thumb_house_18.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (7, 2, NULL, N'/prop7/house_7.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (7,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (7, '2012-01-01', '2020-01-01', 200)

------ Property no 8
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (8, @ownerId, 4, N'tst-8', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Calm Villa</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>House under the rainbow</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 35.2654, -112.0426, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (8, 8, N'unit-type code 8', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 8 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 8 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (8, 8, 8, N'unit code 8', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 8 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 8 desc')), 1, 2, 3, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (8, 0, NULL, N'/prop8/thumbs/thumb_house_19.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (8, 0, NULL, N'/prop8/thumbs/thumb_house_20.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (8, 0, NULL, N'/prop8/thumbs/thumb_house_21.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (8, 4, NULL, N'/prop8/thumbs/thumb_house_19.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (8, 4, NULL, N'/prop8/thumbs/thumb_house_20.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (8, 4, NULL, N'/prop8/thumbs/thumb_house_21.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (8, 2, NULL, N'/prop8/house_8.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (8,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (8, '2012-01-01', '2020-01-01', 200)

------ Property no 9
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (9, @ownerId, 5, N'tst-9', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>House on Hills</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Nice and cozy apartment for two</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 34.7464, -92.2894, 1, 1, 15.00, '14:00', '12:00', 1, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (9, 9, N'unit-type code 9', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 9 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 9 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms],[KeysLockboxCode],[KeysLockboxLocation]) VALUES (9, 9, 9, N'unit code 9', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 9 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 9 desc')), 1, 4, 2, 1,'2421','Right beside doors')
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (9, 0, NULL, N'/prop9/thumbs/thumb_house_1.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (9, 0, NULL, N'/prop9/thumbs/thumb_house_2.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (9, 0, NULL, N'/prop9/thumbs/thumb_house_3.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (9, 4, NULL, N'/prop9/thumbs/thumb_house_1.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (9, 4, NULL, N'/prop9/thumbs/thumb_house_2.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (9, 4, NULL, N'/prop9/thumbs/thumb_house_3.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (9, 2, NULL, N'/prop9/house_9.jpg')

INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (9, '2012-01-01', '2020-01-01', 200)

------ Property no 10
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (10, @ownerId, 5, N'tst-10', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Cozy Apartment</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Short, catchy description</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 34.3622, -92.8128, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (10, 10, N'unit-type code 10', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 10 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 10 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (10, 10, 10, N'unit code 10', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 10 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 10 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (10, 0, NULL, N'/prop10/thumbs/thumb_house_4.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (10, 4, NULL, N'/prop10/thumbs/thumb_house_4.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (10, 2, NULL, N'/prop10/house_10.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (10,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (10, '2012-01-01', '2020-01-01', 200)

------ Property no 11
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (11, @ownerId, 6, N'tst-11', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Shiny House</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Aparmentment with beautiful views</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 38.5817, -121.4933, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (11, 11, N'unit-type code 11', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 11 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 11 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (11, 11, 11, N'unit code 11', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 11 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 11 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (11, 0, NULL, N'/prop11/thumbs/thumb_house_5.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (11, 0, NULL, N'/prop11/thumbs/thumb_house_6.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (11, 4, NULL, N'/prop11/thumbs/thumb_house_5.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (11, 4, NULL, N'/prop11/thumbs/thumb_house_6.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (11, 2, NULL, N'/prop11/house_11.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (11,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (11, '2012-01-01', '2020-01-01', 200)

------ Property no 12
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (12, @ownerId, 7, N'tst-12', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Calm Villa</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>House under the rainbow</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 39.7392, -104.9842, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (12, 12, N'unit-type code 12', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 12 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 12 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (12, 12, 12, N'unit code 12', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 12 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 12 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (12, 0, NULL, N'/prop12/thumbs/thumb_house_7.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (12, 0, NULL, N'/prop12/thumbs/thumb_house_8.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (12, 4, NULL, N'/prop12/thumbs/thumb_house_7.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (12, 4, NULL, N'/prop12/thumbs/thumb_house_8.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (12, 2, NULL, N'/prop12/house_12.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (12,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (12, '2012-01-01', '2020-01-01', 200)

------ Property no 13
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (13, @ownerId, 7, N'tst-13', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>House on Hills</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Nice and cozy apartment for two</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 38.8339, -104.8208, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (13, 13, N'unit-type code 13', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 13 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 13 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (13, 13, 13, N'unit code 13', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 13 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 13 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (13, 0, NULL, N'/prop13/thumbs/thumb_house_9.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (13, 0, NULL, N'/prop13/thumbs/thumb_house_10.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (13, 4, NULL, N'/prop13/thumbs/thumb_house_9.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (13, 4, NULL, N'/prop13/thumbs/thumb_house_10.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (13, 2, NULL, N'/prop13/house_13.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (13,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (13, '2012-01-01', '2020-01-01', 200)

------ Property no 14
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (14, @ownerId, 7, N'tst-14', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Cozy Apartment</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Short, catchy description</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 40.1672, -105.1014, 1, 1, 15.00, '14:00', '12:00', 1, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (14, 14, N'unit-type code 14', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 14 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 14 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms],[KeysLockboxCode],[KeysLockboxLocation]) VALUES (14, 14, 14, N'unit code 14', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 14 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 14 desc')), 1, 1, 1, 1,'klpk','In the yard')
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (14, 0, NULL, N'/prop14/thumbs/thumb_house_11.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (14, 0, NULL, N'/prop14/thumbs/thumb_house_12.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (14, 4, NULL, N'/prop14/thumbs/thumb_house_11.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (14, 4, NULL, N'/prop14/thumbs/thumb_house_12.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (14, 2, NULL, N'/prop14/house_14.jpg')

INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (14, '2012-01-01', '2020-01-01', 200)

------ Property no 15
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (15, @ownerId, 8, N'tst-15', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Shiny House</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Aparmentment with beautiful views</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 41.7636, -72.6856, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (15, 15, N'unit-type code 15', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 15 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 15 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (15, 15, 15, N'unit code 15', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 15 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 15 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (15, 0, NULL, N'/prop15/thumbs/thumb_house_13.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (15, 4, NULL, N'/prop15/thumbs/thumb_house_13.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (15, 2, NULL, N'/prop15/house_15.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (15,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (15, '2012-01-01', '2020-01-01', 200)

------ Property no 16
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (16, @ownerId, 9, N'tst-16', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Calm Villa</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>House under the rainbow</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 39.1581, -75.5247, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (16, 16, N'unit-type code 16', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 16 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 16 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (16, 16, 16, N'unit code 16', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 16 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 16 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (16, 0, NULL, N'/prop16/thumbs/thumb_house_14.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (16, 4, NULL, N'/prop16/thumbs/thumb_house_14.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (16, 2, NULL, N'/prop16/house_16.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (16,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (16, '2012-01-01', '2020-01-01', 200)

------ Property no 17
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (17, @ownerId, 9, N'tst-17', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>House on Hills</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Nice and cozy apartment for two</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 38.5914, -75.2917, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (17, 17, N'unit-type code 17', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 17 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 17 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (17, 17, 17, N'unit code 17', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 17 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 17 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (17, 0, NULL, N'/prop17/thumbs/thumb_house_15.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (17, 0, NULL, N'/prop17/thumbs/thumb_house_16.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (17, 4, NULL, N'/prop17/thumbs/thumb_house_15.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (17, 4, NULL, N'/prop17/thumbs/thumb_house_16.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (17, 2, NULL, N'/prop17/house_17.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (17,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (17, '2012-01-01', '2020-01-01', 200)

------ Property no 18
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (18, @ownerId, 10, N'tst-18', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Cozy Apartment</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Short, catchy description</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 27.9347, -82.2892, 1, 1, 15.00, '14:00', '12:00', 1, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (18, 18, N'unit-type code 18', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 18 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 18 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms],[KeysLockboxCode],[KeysLockboxLocation]) VALUES (18, 18, 18, N'unit code 18', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 18 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 18 desc')), 1, 1, 1, 1,'2321','Somwhere in the sky')
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (18, 0, NULL, N'/prop18/thumbs/thumb_house_17.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (18, 0, NULL, N'/prop18/thumbs/thumb_house_18.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (18, 4, NULL, N'/prop18/thumbs/thumb_house_17.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (18, 4, NULL, N'/prop18/thumbs/thumb_house_18.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (18, 2, NULL, N'/prop18/house_18.jpg')
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (18, '2012-01-01', '2020-01-01', 200)

------ Property no 19
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (19, @ownerId, 10, N'tst-19', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Shiny House</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Aparmentment with beautiful views</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 28.5381, -81.3794, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (19, 19, N'unit-type code 19', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 19 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 19 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (19, 19, 19, N'unit code 19', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 19 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 19 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (19, 0, NULL, N'/prop19/thumbs/thumb_house_19.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (19, 0, NULL, N'/prop19/thumbs/thumb_house_20.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (19, 4, NULL, N'/prop19/thumbs/thumb_house_19.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (19, 4, NULL, N'/prop19/thumbs/thumb_house_20.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (19, 2, NULL, N'/prop19/house_19.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (19,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (19, '2012-01-01', '2020-01-01', 200)

------ Property no 20
SET IDENTITY_INSERT [dbo].[Properties] ON
INSERT [dbo].[Properties] ([PropertyID], [OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut], [KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (20, @ownerId, 10, N'tst-20', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Calm Villa</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>House under the rainbow</Content></i18nPair></i18nContent></i18nString>', N'<i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent><i18nPair><Code>en-US</Code><Content>Long Description</Content></i18nPair></i18nContent></i18nString>', N'Address 1', N'Address 2', N'New York', N'102345', N'New York', @countryId, 26.6403, -81.8725, 1, 1, 15.00, '14:00', '12:00', 0, 1, @cultureEnUsId, @usTimeZoneEastern)
SET IDENTITY_INSERT [dbo].[Properties] OFF

SET IDENTITY_INSERT [dbo].[UnitTypes] ON
INSERT [dbo].[UnitTypes] ([UnitTypeID], [PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (20, 20, N'unit-type code 20', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 20 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit type 20 desc')))
SET IDENTITY_INSERT [dbo].[UnitTypes] OFF

SET IDENTITY_INSERT [dbo].[Units] ON
INSERT [dbo].[Units] ([UnitID], [PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (20, 20, 20, N'unit code 20', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 20 title')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'unit 20 desc')), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Units] OFF

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (20, 0, NULL, N'/prop20/thumbs/thumb_house_21.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (20, 0, NULL, N'/prop20/thumbs/thumb_house_1.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (20, 4, NULL, N'/prop20/thumbs/thumb_house_21.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (20, 4, NULL, N'/prop20/thumbs/thumb_house_1.jpg')
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n], [ContentValue]) VALUES (20, 2, NULL, N'/prop20/house_20.jpg')

INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (20,5,@ownerId)
INSERT [dbo].[UnitRates] ([UnitID], [DateFrom],[DateUntil],[DailyRate]) VALUES (20, '2012-01-01', '2020-01-01', 200)