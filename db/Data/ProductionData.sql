declare @i18nXmlPatternProd nvarchar(max)
declare @i18nXmlEnglishPairProd nvarchar(max)
declare @i18nXmlPolishPairProd nvarchar(max)
declare @timeZoneEastern nvarchar(max)
set @i18nXmlPatternProd = '<?xml version="1.0" encoding="utf-16"?><i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent>#pair#</i18nContent></i18nString>'
set @i18nXmlEnglishPairProd = '<i18nPair><Code>en-US</Code><Content>#phrase#</Content></i18nPair>'
set @i18nXmlPolishPairProd = '<i18nPair><Code>pl-PL</Code><Content>#phrase#</Content></i18nPair>'
set @timeZoneEastern = 'Eastern Standard Time'

-- Shared variables
DECLARE @countryId INT, @propertyId INT, @propertyTypeId INT, @destinationId INT, @ownerId INT, @roleId INT, @tmpId INT, @tmpGroupId INT, @unitId INT, @unitTypeId INT, @position INT, @roleAdmin int, @roleGuest int, @cultureEnUsId int

SET @countryId = (SELECT [CountryId] FROM [dbo].[DictionaryCountries] WHERE [CountryCode2Letters] = N'us')
SET @roleId = (SELECT [RoleId] FROM [dbo].[Roles] WHERE [Name] = N'Owner')
SET @destinationId = (SELECT [DestinationID] FROM [dbo].[Destinations] WHERE [Destination_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Maine')
SET @propertyTypeId = (SELECT [PropertyTypeId] FROM [dbo].[PropertyType] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'House')

SET @roleAdmin = (SELECT [RoleId] FROM [dbo].[Roles] WHERE [Name] = N'Administrator')
SET @roleGuest = (SELECT [RoleId] FROM [dbo].[Roles] WHERE [Name] = N'Guest')
SET @cultureEnUsId = (SELECT [CultureId] FROM [dbo].[DictionaryCultures] WHERE [CultureCode] = N'en-US')

-- Add system administrator
declare @adminId int
INSERT [dbo].[Users] ([Lastname], [Firstname], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [CellPhone], [LandLine], [email], [DriverLicenseNbr], [PassportNbr], [SocialsecurityNbr], [DirectDepositInfo], [SecurityQuestion], [SecurityAnswer], [AcceptedTCs], [AcceptedTCsInitials], [Password], [SendMePromotions], [SendInfoFavoritePlaces], [SendNews], [CultureId]) VALUES
	(N'System', N'Administrator', NULL, NULL, NULL, NULL, NULL, @countryId, NULL, NULL, N'admin@otp.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1A1DC91C907325C69271DDF0C944BC72', NULL, NULL, NULL, @cultureEnUsId)
select @adminId = SCOPE_IDENTITY()
INSERT [dbo].[UserRoles] ([UserId], [RoleId], [Status], [ActivationToken]) VALUES
	(@adminId, @roleAdmin, 3, null)

-----------------------------------------
-- The Lake Retreat
-----------------------------------------

-- Owner
INSERT [dbo].[Users] ([Lastname], [Firstname], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [CellPhone], [LandLine], [email], [DriverLicenseNbr], [PassportNbr], [SocialsecurityNbr], [DirectDepositInfo], [SecurityQuestion], [SecurityAnswer], [AcceptedTCs], [AcceptedTCsInitials], [Password],[SendMePromotions],[SendInfoFavoritePlaces],[SendNews], [CultureId]) VALUES (N'Stanley', N'Jim', N'268 Rolland Drive, Unit 12', NULL, N'Virginia', N'76001', N'Arlington', @countryId, N'202-365-0092', N'202-505-4221', N'jamesstanley@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'JS', N'1A1DC91C907325C69271DDF0C944BC72',0,1,1, @cultureEnUsId)
SET @ownerId = SCOPE_IDENTITY()

-- UserRole
INSERT INTO [dbo].[UserRoles] ([UserId], [RoleId], [Status], [ActivationToken]) VALUES (@ownerId, @roleId, 3, N'')

-- Property
INSERT [dbo].[Properties] ([OwnerID], [OTP_DestinationID], [PropertyCode], [PropertyName_i18n], [Short_Description_i18n], [Long_Description_i18n], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [Latitude], [Longitude], [Altitude], [PropertyLive], [StandardCommission], [CheckIn], [CheckOut],[KeyProcessType], [PropertyTypeId], [CultureId], [TimeZone]) VALUES (@ownerId, @destinationId, N'prd-1', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'The Lake Retreat')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'An airy escape by a grand lake in Maine')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'This stunning home is located on scenic Sebago Lake in Maine, a short 3 minute walk from the lakeshore. The house sits on a small hill and has partial lake views from the kitchen, dining, family and sun rooms. The home was completely renovated in 2010 with high end finishes and beautiful details throughout. The kitchen includes stunning granite countertops, stainless steel appliances, and a wine refrigerator. The great room is stunning with vaulted, bead boarded ceilings and a panorama of large paned windows. Wood flooring throughout the entire home. With this home you will be able to relax and enjoy a little touch of luxury at the lake.')), N'98 Wild Acres Road', NULL, N'Maine', N'04071', N'Raymond', @countryId, 43.844649,-70.499228, 278, 1, 15.00, '14:00', '12:00',0, @propertyTypeID, @cultureEnUsId, @timeZoneEastern)
SET @propertyId = SCOPE_IDENTITY()

-- Property KeyManager
INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (@propertyId,5,@ownerId)


-- PropertyStaticContent
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n]) VALUES
	-- SquareFootage
	(@propertyId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2000 sq ft')))

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n]) VALUES
	-- Feature
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful views of the outdoors and partial lake views'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lake access, 3 minute walk to the lake shore'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Entire home renovated in 2010 with high end finishes'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Large, great room with stunning vaulted, bead board ceilings'))),

	-- LocalAreaDescription
	(@propertyId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sebago Lake is one of the largest and deepest lake in Maine. The lake is surrounded by several small towns including Raymond, Casco, Naples and Windham. Several recreational activities are available including fishing, hiking, biking, boating, swimming, and sailing. The fishing at Sebago is legendary with several species of lake indigenous or stocked by the state in the lake.'))),

	-- DistanceToMainAttraction
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to lakeshore'))),

	-- Arrival Info
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Keys: Keys are located in lockbox. Two keys are required for entry, a key for the regular lock (gold key) and a key for the dead bolt (silver key). Both keys turn left to open, right to lock.'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lockbox instructions: Lockbox is located on back door, code is 3346. Must disable alarm upon entry!'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Alarm instructions: Alarm is located inside closet next to downstairs bathroom, you will have one minute to disarm so you must move quickly once door is opened. If alarm is set off, call Oh The Places at (enter out 800 number here). Alarm may be left disarmed for duration of your stay.'))), 

	-- Parking Info
	(@propertyId, 15, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 car parking is available in driveway. Please, there is no street parking, your car will be ticketed if it is parked on the street')))

	-- Directions to Airport


INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	(@propertyId, 0, N'/prop21/thumbs/thumb_house_1.jpg'),
	(@propertyId, 0, N'/prop21/thumbs/thumb_house_2.jpg'),
	(@propertyId, 0, N'/prop21/thumbs/thumb_house_3.jpg'),
	(@propertyId, 4, N'/prop21/thumbs/thumb_house_1.jpg'),
	(@propertyId, 4, N'/prop21/thumbs/thumb_house_2.jpg'),
	(@propertyId, 4, N'/prop21/thumbs/thumb_house_3.jpg'),
	(@propertyId, 2, N'/prop21/house_21.jpg'),
    (@propertyId, 8, N'<FloorPlan xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Name_i18n>&lt;i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en&lt;/Code&gt;&lt;Content&gt;First floor&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><Panoramas><Panorama><Id>1</Id><OffsetX>140</OffsetX><OffsetY>59</OffsetY><PanoramaPath>/properties/id21/BathroomPano</PanoramaPath></Panorama></Panoramas><OriginalImagePath>/properties/id21/1stFloor.jpg</OriginalImagePath><DisplayImagePath>/properties/id21/disp_1stFloor.png</DisplayImagePath></FloorPlan>'),
	(@propertyId, 16, N'<DirectionToAirport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Name_i18n>&lt;i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en&lt;/Code&gt;&lt;Content&gt;Portland International Jetport&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><DirectionLink>https://maps.google.com/maps?saddr=Portland+International+Jetport&amp;daddr=98+Wild+Acres+Road,+Raymond,+Maine+04071&amp;hl=pl&amp;ll=43.756217,-70.396957&amp;spn=0.304517,0.780029&amp;sll=44.559163,-70.054321&amp;sspn=2.403205,6.240234&amp;geocode=FZ_8mQId_SzP-yHPPvZLhaXH-ymBqy4kWZmyTDHPPvZLhaXH-w%3BFTYDnQIdeETM-yk90qZyvfSyTDH-RRb_eA5pdQ&amp;mra=ls&amp;t=m&amp;z=11></DirectionLink></DirectionToAirport>'),
    (@propertyId, 9, N'/properties/id21/BathroomPano')

-- Amenities
SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Flat Screen TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cable / Satellite TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sound System')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wifi')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')))

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Small gas grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

-- UnitType
INSERT [dbo].[UnitTypes] ([PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (@propertyId, N'unit-type code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'unit type title')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'unit type desc')))
SET @unitTypeId = SCOPE_IDENTITY()

-- Unit
INSERT [dbo].[Units] ([PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (@propertyId, @unitTypeId, N'unit code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'unit title')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'unit desc')), 1, 7, 3, 2)
SET @unitId = SCOPE_IDENTITY()

-- UnitStaticContent
INSERT [dbo].[UnitStaticContent] ([UnitId], [ContentCode], [ContentValue_i18n]) VALUES
	-- Bedrooms
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / King Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),

	-- Bathrooms
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / Full bath (ensuite)'))),
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Full Bath'))),

	-- LivingAreasAndDining
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 Couches / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Loveseat / Seats 2'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'3 Chairs / Seats 3'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Dining Table / Seats 10'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sun Room Dining Table / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Breakfast Bar / Seats 5')))

	-- KitchenAndLaundry
	-- to be filled, type = 13

-- Appliances
SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel stove and oven')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel dishwasher')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Washer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Dryer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cooking utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Toaster')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Slow cooker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food processor')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Mixer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine Refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine opener')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine rack')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garbage disposal')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Water filter')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Ice Maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel, branded appliances')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Granite or marble countertops')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end flooring')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end cabinetry and finishes')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Pots and pans')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Roasting pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Baking sheets')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cupcake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food storage containers')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster pots')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster eating utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Juice maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Waffle maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Highchair')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sets include a plate, bowl, knife, fork and spoon')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

-- Unit Rates
INSERT INTO [dbo].[UnitRates] ([UnitID], [DateFrom], [DateUntil], [DailyRate]) VALUES
	(@unitId, '2012-10-29', '2012-11-15', 400.00),
	(@unitId, '2012-11-16', '2012-11-24', 650.00),
	(@unitId, '2012-11-25', '2012-12-20', 400.00),
	(@unitId, '2012-12-21', '2013-01-02', 650.00),
	(@unitId, '2013-01-03', '2013-05-15', 400.00),
	(@unitId, '2013-05-16', '2013-08-31', 650.00),
	(@unitId, '2013-09-01', '2013-11-21', 400.00),
	(@unitId, '2013-11-22', '2020-12-31', 400.00)

-- MLOS - max and min values found on: http://sqlserverplanet.com/tsql/max-date-value
INSERT INTO [dbo].[UnitTypeMLOS] ([UnitTypeID], [DateFrom], [DateUntil], [MLOS]) VALUES (@unitTypeId, '1753-01-01 00:00:00.000', '9999-12-31 23:59:59.998', 7)

