declare @i18nXmlPatternProd nvarchar(max)
declare @i18nXmlEnglishPairProd nvarchar(max)
declare @i18nXmlPolishPairProd nvarchar(max)
declare @usTimeZoneEastern nvarchar(max)

set @usTimeZoneEastern = 'Eastern Standard Time'
set @i18nXmlPatternProd = '<?xml version="1.0" encoding="utf-16"?><i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent>#pair#</i18nContent></i18nString>'
set @i18nXmlEnglishPairProd = '<i18nPair><Code>en-US</Code><Content>#phrase#</Content></i18nPair>'
set @i18nXmlPolishPairProd = '<i18nPair><Code>pl-PL</Code><Content>#phrase#</Content></i18nPair>'

-- Global variables
declare @countryId INT, @roleGuest INT, @roleManager INT, @roleAdmin INT, @propertyTypeId INT, @guestId int, @ownerId int, @cultureEnUsId int
declare @reservationID int, @unitId int, @unitTypeId int, @tmpId int
declare @guestExample INT;

set @countryId = (SELECT [CountryId] FROM [dbo].[DictionaryCountries] WHERE [CountryCode2Letters] = N'us')
SET @roleManager = (SELECT [RoleId] FROM [dbo].[Roles] WHERE [Name] = N'Upload Manager')
SET @roleAdmin = (SELECT [RoleId] FROM [dbo].[Roles] WHERE [Name] = N'Administrator')
SET @propertyTypeId = (SELECT [PropertyTypeId] FROM [dbo].[PropertyType] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'House')
set @ownerId = (select top 1 u.UserId from Users u join UserRoles ur on ur.UserId = u.UserId join Roles r on r.RoleId = ur.RoleId where r.RoleLevel = 1)
SET @cultureEnUsId = (SELECT [CultureId] FROM [dbo].[DictionaryCultures] WHERE [CultureCode] = N'en-US')
SET @roleGuest = (SELECT [RoleId] FROM [dbo].[Roles] WHERE [Name] = N'Guest')

-- Guest example
INSERT [dbo].[Users] ([Lastname], [Firstname], [Address1], [Address2], [State], [ZIPCode], [City], [CountryId], [CellPhone], [LandLine], [email], [DriverLicenseNbr], [PassportNbr], [SocialsecurityNbr], [DirectDepositInfo], [SecurityQuestion], [SecurityAnswer], [AcceptedTCs], [AcceptedTCsInitials], [Password], [SendMePromotions], [SendInfoFavoritePlaces], [SendNews], [CultureId]) VALUES
	(N'Heinze', N'Michael', N'268 Rolland Drive, Unit 12', NULL, N'Virginia', N'76001', N'Arlington', @countryId, N'202-365-0092', N'202-505-4221', N'michael@test.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1A1DC91C907325C69271DDF0C944BC72', 0, 1, 0, @cultureEnUsId)
select @guestExample = SCOPE_IDENTITY()

INSERT [dbo].[UserRoles] ([UserId], [RoleId], [Status], [ActivationToken]) VALUES
	(@guestExample, @roleGuest, 3, null)

set @guestId = (select top 1 u.UserId from Users u join UserRoles ur on ur.UserId = u.UserId join Roles r on r.RoleId = ur.RoleId where r.RoleLevel = 0)

-- Taxes
declare @floridaDestinationId int
select @floridaDestinationId = DestinationId from Destinations where Destination_i18n.value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Florida'
INSERT [dbo].[Taxes] ([DestinationId], [Name_i18n], [Percentage]) VALUES
	(@floridaDestinationId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', 'Sales Tax 10%')), 10),
	(@floridaDestinationId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', 'Sales Tax 5%')), 5)

-- Test data for production data (Lake Retreat)
declare @propPrdId INT
declare @unitPrdId INT
declare @unitTypePrdId INT
set @propPrdId = (SELECT TOP 1 [PropertyID] FROM [dbo].[Properties])
set @unitPrdId = (SELECT TOP 1 [UnitID] FROM [dbo].[Units] WHERE [PropertyID] = @propPrdId)
set @unitTypePrdId = (SELECT TOP 1 [UnitTypeID] FROM [dbo].[UnitTypes] WHERE [PropertyID] = @propPrdId)

-- Blockings
INSERT INTO [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES
	(@unitPrdId, '2012-11-22', '2012-12-02', 2, NULL),
	(@unitPrdId, '2012-12-20', '2013-01-02', 2, NULL)

-- Reservation
INSERT [dbo].[Reservations] ([PropertyID], [UnitID], [UnitTypeId], [GuestID], [ConfirmationID], [BookingDate], [BookingStatus], [DateArrival], [DateDeparture], [TotalPrice], [ReservationCultureId], [TripBalance], [TransactionFee], [LinkAuthorizationToken]) VALUES 
	(@propPrdId, @unitPrdId, @unitTypePrdId, 1, N'1', N'2013-01-10 00:00:00.000', 1, N'2013-02-05 00:00:00.000', N'2013-03-10 00:00:00.000', 1.0000, 82, 1.0000, 0, N'B2892DE7-FF80-46BF-88EB-931B0C8058DD')
set @reservationID = SCOPE_IDENTITY()

INSERT INTO [dbo].[GuestReviews] ([GuestID], [PropertyID], [ReservationID], [ReviewContent], [Satisfaction_Experience], [Satisfaction_Cleanliness], [Satisfaction_Location], [ReviewTitle], [ReviewDate]) VALUES
	( @guestId, @propPrdId, @reservationID, 'Review content data', 5, 4, 5, 'Superb House', GETDATE() ),
	( @guestId, @propPrdId, @reservationID, 'Another review content data', 5, 5, 4, 'Excellent neighbergood', DATEADD(day, -12, GETDATE()) ),
	( @guestId, @propPrdId, @reservationID, 'Third review content data', 4, 4, 5, 'Fantastic House, Super views', DATEADD(day, -6, GETDATE()) ),
	( @guestId, @propPrdId, @reservationID, 'And another Review content data', 3, 5, 5, 'Good trip', GETDATE() ),
	( @guestId, @propPrdId, @reservationID, 'Third review content data', 5, 5, 4, 'Fantastic House, Super views', DATEADD(day, -2, GETDATE()) ),
	( @guestId, @propPrdId, @reservationID, 'Third review content data', 5, 4, 5, 'Fantastic House, Super views', DATEADD(day, -32, GETDATE()) )
-- END Test data


declare @propertyId int
-----------------------
-- Property 1
-----------------------

-- Property

INSERT [dbo].[Properties] (
	[OwnerID], 
	[OTP_DestinationID], 
	[PropertyCode], 
	[PropertyName_i18n], 
	[Short_Description_i18n], 
	[Long_Description_i18n], 
	[Address1], 
	[Address2], 
	[State], 
	[ZIPCode], 
	[City], 
	[CountryId], 
	[Latitude], 
	[Longitude], 
	[Altitude], 
	[PropertyLive], 
	[StandardCommission], 
	[CheckIn], 
	[CheckOut],
	[KeyProcessType], 
	[PropertyTypeId], [CultureId], [TimeZone]) 
	VALUES 
	(@ownerId, 
	@floridaDestinationId, 
	N'property-code', 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Villa at the Beach')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'A fabulous trip at the beach area')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'This stunning home is located on scenic Sebago Lake in Maine, a short 3 minute walk from the lakeshore. The house sits on a small hill and has partial lake views from the kitchen, dining, family and sun rooms. The home was completely renovated in 2010 with high end finishes and beautiful details throughout. The kitchen includes stunning granite countertops, stainless steel appliances, and a wine refrigerator. The great room is stunning with vaulted, bead boarded ceilings and a panorama of large paned windows. Wood flooring throughout the entire home. With this home you will be able to relax and enjoy a little touch of luxury at the lake.')), 
	N'98 Wild Acres Road', 
	NULL, 
	N'Maine', 
	N'04071', 
	N'Raymond', 
	@countryId, 
	27.449790,
	-80.903320, 
	278, 
	1, 
	15.00, 
	'14:00', 
	'12:00',
	0,
	@propertyTypeID, @cultureEnUsId, @usTimeZoneEastern)

SET @propertyId = SCOPE_IDENTITY()


-- Property KeyManager
INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (@propertyId,5,@ownerId)


-- PropertyStaticContent
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	-- SquareFootage
	(@propertyId, 10, N'2000')

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n]) VALUES
	-- Feature
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful views of the outdoors and partial lake views'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lake access, 3 minute walk to the lake shore'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Entire home renovated in 2010 with high end finishes'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Large, great room with stunning vaulted, bead board ceilings'))),

	-- LocalAreaDescription
	(@propertyId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sebago Lake is one of the largest and deepest lake in Maine. The lake is surrounded by several small towns including Raymond, Casco, Naples and Windham. Several recreational activities are available including fishing, hiking, biking, boating, swimming, and sailing. The fishing at Sebago is legendary with several species of lake indigenous or stocked by the state in the lake.'))),

	-- DistanceToMainAttraction
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to lakeshore'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.5 mile to zoo'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to sea'))),

	-- Arrival Info
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Keys: Keys are located in lockbox. Two keys are required for entry, a key for the regular lock (gold key) and a key for the dead bolt (silver key). Both keys turn left to open, right to lock.'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lockbox instructions: Lockbox is located on back door, code is 3346. Must disable alarm upon entry!'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Alarm instructions: Alarm is located inside closet next to downstairs bathroom, you will have one minute to disarm so you must move quickly once door is opened. If alarm is set off, call Oh The Places at (enter out 800 number here). Alarm may be left disarmed for duration of your stay.'))), 

	-- Parking Info
	(@propertyId, 15, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 car parking is available in driveway. Please, there is no street parking, your car will be ticketed if it is parked on the street')))

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	(@propertyId, 0, N'/prop1/thumb1.jpg'),
	(@propertyId, 0, N'/prop1/thumb2.jpg'),
	(@propertyId, 4, N'/prop1/listview1.jpg'),
	(@propertyId, 4, N'/prop1/listview2.jpg'),
	(@propertyId, 2, N'/prop1/fullscreen1.jpg'),
	-- Directions to Airport
	(@propertyId, 16, N'<DirectionToAirport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Name_i18n>&lt;i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US-US&lt;/Code&gt;&lt;Content&gt;Portland International Jetport&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><DirectionLink>https://maps.google.com/maps?saddr=Portland+International+Jetport&amp;daddr=98+Wild+Acres+Road,+Raymond,+Maine+04071&amp;hl=pl&amp;ll=43.756217,-70.396957&amp;spn=0.304517,0.780029&amp;sll=44.559163,-70.054321&amp;sspn=2.403205,6.240234&amp;geocode=FZ_8mQId_SzP-yHPPvZLhaXH-ymBqy4kWZmyTDHPPvZLhaXH-w%3BFTYDnQIdeETM-yk90qZyvfSyTDH-RRb_eA5pdQ&amp;mra=ls&amp;t=m&amp;z=11></DirectionLink></DirectionToAirport>'),
    (@propertyId, 8, N'<FloorPlan xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Name_i18n>&lt;i18nString xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;First Floor&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><Panoramas><Panorama><Id>1</Id><OffsetX>324</OffsetX><OffsetY>62</OffsetY><PanoramaPath>/properties/id1/Bathroom Pano</PanoramaPath></Panorama><Panorama><Id>2</Id><OffsetX>232</OffsetX><OffsetY>254</OffsetY><PanoramaPath>/properties/id1/Living room kitchen</PanoramaPath></Panorama><Panorama><Id>3</Id><OffsetX>480</OffsetX><OffsetY>355</OffsetY><PanoramaPath>/properties/id1/Sebago lake pano</PanoramaPath></Panorama></Panoramas><OriginalImagePath>/properties/id1/First+Floor.png</OriginalImagePath><DisplayImagePath>/properties/id1/disp_First+Floor.png</DisplayImagePath></FloorPlan>'),
    (@propertyId, 9, N'/properties/id1/Bathroom Pano'),
	(@propertyId, 9, N'/properties/id1/Living room kitchen'),
	(@propertyId, 9, N'/properties/id1/Sebago lake pano')

-- Amenities
SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Flat Screen TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cable / Satellite TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sound System')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wifi')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')))

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Small gas grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'BBQ Grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Outdoor Table')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garage')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Central Air')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Tennis Courts')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Great Views')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

-- UnitType
INSERT [dbo].[UnitTypes] ([PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (@propertyId, N'unit-type code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit Type')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit type in the Villa at the beach')))
SET @unitTypeId = SCOPE_IDENTITY()

-- Unit
INSERT [dbo].[Units] ([PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (@propertyId, @unitTypeId, N'unit code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit 1')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit in the Villa at the beach')), 1, 7, 3, 2)
SET @unitId = SCOPE_IDENTITY()

-- UnitStaticContent
INSERT [dbo].[UnitStaticContent] ([UnitId], [ContentCode], [ContentValue_i18n]) VALUES
	-- Bedrooms
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / King Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),

	-- Bathrooms
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / Full bath (ensuite)'))),
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Full Bath'))),

	-- LivingAreasAndDining
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 Couches / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Loveseat / Seats 2'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'3 Chairs / Seats 3'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Dining Table / Seats 10'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sun Room Dining Table / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Breakfast Bar / Seats 5')))

	-- KitchenAndLaundry
	-- to be filled, type = 13

-- Appliances
SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel stove and oven')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel dishwasher')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Washer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Dryer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cooking utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Toaster')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Slow cooker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food processor')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Mixer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine Refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine opener')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine rack')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garbage disposal')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Water filter')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Ice Maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel, branded appliances')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Granite or marble countertops')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end flooring')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end cabinetry and finishes')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Pots and pans')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Roasting pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Baking sheets')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cupcake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food storage containers')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster pots')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster eating utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Juice maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Waffle maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Highchair')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sets include a plate, bowl, knife, fork and spoon')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

-- Unit Rates
INSERT INTO [dbo].[UnitRates] ([UnitID], [DateFrom], [DateUntil], [DailyRate]) VALUES
	(@unitId, '2012-10-29', '2012-11-15', 400.00),
	(@unitId, '2012-11-16', '2012-11-24', 650.00),
	(@unitId, '2012-11-25', '2012-12-20', 400.00),
	(@unitId, '2012-12-21', '2013-01-02', 650.00),
	(@unitId, '2013-01-03', '2013-05-15', 400.00),
	(@unitId, '2013-05-16', '2013-08-31', 650.00),
	(@unitId, '2013-09-01', '2020-11-21', 400.00)

-- MLOS - max and min values found on: http://sqlserverplanet.com/tsql/max-date-value
INSERT INTO [dbo].[UnitTypeMLOS] ([UnitTypeID], [DateFrom], [DateUntil], [MLOS]) VALUES (@unitTypeId, '2013-01-01 00:00:00.000', '2013-12-31 23:59:59.998', 7)

-- Blockings
INSERT INTO [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES
	(@unitId, '2013-01-01', '2013-05-02', 2, NULL)

-- Reservation
INSERT [dbo].[Reservations] ([PropertyID], [UnitID], [UnitTypeId], [GuestID], [ConfirmationID], [BookingDate], [BookingStatus], [DateArrival], [DateDeparture], [TotalPrice], [ReservationCultureId], [TripBalance], [TransactionFee], [LinkAuthorizationToken]) VALUES 
	(@propertyId, @unitId, @unitTypeId, @guestId, N'123eds', N'2012-10-10 00:00:00.000', 1, N'2012-10-10 00:00:00.000', N'2012-10-10 00:00:00.000', 1.0000, 82, 1.0000, 0, N'2FEB5E0D-AEC5-4A1D-80C0-140363BBC5A6')
set @reservationID = SCOPE_IDENTITY()

-- Reviews
INSERT INTO [dbo].[GuestReviews] ([GuestID], [PropertyID], [ReservationID], [ReviewContent], [Satisfaction_Experience], [Satisfaction_Cleanliness], [Satisfaction_Location], [ReviewTitle], [ReviewDate]) VALUES
	( @guestId, @propertyId, @reservationID, 'Review content data', 5, 4, 5, 'Superb House', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Another review content data', 5, 5, 4, 'Excellent neighbergood', DATEADD(day, -12, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 4, 4, 5, 'Fantastic House, Super views', DATEADD(day, -6, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'And another Review content data', 3, 5, 5, 'Good trip', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 5, 4, 'Fantastic House, Super views', DATEADD(day, -2, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 4, 5, 'Fantastic House, Super views', DATEADD(day, -32, GETDATE()) )

-----------------------
-- Property 2
-----------------------

-- Property

INSERT [dbo].[Properties] (
	[OwnerID], 
	[OTP_DestinationID], 
	[PropertyCode], 
	[PropertyName_i18n], 
	[Short_Description_i18n], 
	[Long_Description_i18n], 
	[Address1], 
	[Address2], 
	[State], 
	[ZIPCode], 
	[City], 
	[CountryId], 
	[Latitude], 
	[Longitude], 
	[Altitude], 
	[PropertyLive], 
	[StandardCommission], 
	[CheckIn], 
	[CheckOut], 
	[PropertyTypeId], [CultureId], [TimeZone]) 
	VALUES 
	(@ownerId, 
	@floridaDestinationId, 
	N'property-code', 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'House on Hills')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful House with fabulous views')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'This stunning home is located on scenic Sebago Lake in Maine, a short 3 minute walk from the lakeshore. The house sits on a small hill and has partial lake views from the kitchen, dining, family and sun rooms. The home was completely renovated in 2010 with high end finishes and beautiful details throughout. The kitchen includes stunning granite countertops, stainless steel appliances, and a wine refrigerator. The great room is stunning with vaulted, bead boarded ceilings and a panorama of large paned windows. Wood flooring throughout the entire home. With this home you will be able to relax and enjoy a little touch of luxury at the lake.')), 
	N'98 Wild Acres Road', 
	NULL, 
	N'Maine', 
	N'04071', 
	N'Raymond', 
	@countryId, 
	29.458731,
	-83.012695, 
	278, 
	1, 
	15.00, 
	'14:00', 
	'12:00', 
	@propertyTypeID, @cultureEnUsId, @usTimeZoneEastern)

SET @propertyId = SCOPE_IDENTITY()


-- Property KeyManager
INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (@propertyId,5,@ownerId)


-- PropertyStaticContent
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	-- SquareFootage
	(@propertyId, 10, N'2000')

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n]) VALUES
	-- Feature
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful views of the outdoors and partial lake views'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lake access, 3 minute walk to the lake shore'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Entire home renovated in 2010 with high end finishes'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Large, great room with stunning vaulted, bead board ceilings'))),

	-- LocalAreaDescription
	(@propertyId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sebago Lake is one of the largest and deepest lake in Maine. The lake is surrounded by several small towns including Raymond, Casco, Naples and Windham. Several recreational activities are available including fishing, hiking, biking, boating, swimming, and sailing. The fishing at Sebago is legendary with several species of lake indigenous or stocked by the state in the lake.'))),

	-- DistanceToMainAttraction
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to lakeshore'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.5 mile to zoo'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to sea'))),

	-- Arrival Info
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Keys: Keys are located in lockbox. Two keys are required for entry, a key for the regular lock (gold key) and a key for the dead bolt (silver key). Both keys turn left to open, right to lock.'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lockbox instructions: Lockbox is located on back door, code is 3346. Must disable alarm upon entry!'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Alarm instructions: Alarm is located inside closet next to downstairs bathroom, you will have one minute to disarm so you must move quickly once door is opened. If alarm is set off, call Oh The Places at (enter out 800 number here). Alarm may be left disarmed for duration of your stay.'))), 

	-- Parking Info
	(@propertyId, 15, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 car parking is available in driveway. Please, there is no street parking, your car will be ticketed if it is parked on the street')))

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	(@propertyId, 0, N'/prop2/thumb1.jpg'),
	(@propertyId, 0, N'/prop2/thumb2.jpg'),
	(@propertyId, 4, N'/prop2/listview1.jpg'),
	(@propertyId, 4, N'/prop2/listview2.jpg'),
	(@propertyId, 2, N'/prop2/fullscreen1.jpg'),
	-- Directions to Airport
	(@propertyId, 16, N'<DirectionToAirport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Name_i18n>&lt;i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;Portland International Jetport&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><DirectionLink>https://maps.google.com/maps?saddr=Portland+International+Jetport&amp;daddr=98+Wild+Acres+Road,+Raymond,+Maine+04071&amp;hl=pl&amp;ll=43.756217,-70.396957&amp;spn=0.304517,0.780029&amp;sll=44.559163,-70.054321&amp;sspn=2.403205,6.240234&amp;geocode=FZ_8mQId_SzP-yHPPvZLhaXH-ymBqy4kWZmyTDHPPvZLhaXH-w%3BFTYDnQIdeETM-yk90qZyvfSyTDH-RRb_eA5pdQ&amp;mra=ls&amp;t=m&amp;z=11></DirectionLink></DirectionToAirport>'),
    (@propertyId, 8, N'<FloorPlan xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Name_i18n>&lt;i18nString xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;First Floor&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><Panoramas><Panorama><Id>1</Id><OffsetX>324</OffsetX><OffsetY>62</OffsetY><PanoramaPath>/properties/id1/Bathroom Pano</PanoramaPath></Panorama><Panorama><Id>2</Id><OffsetX>232</OffsetX><OffsetY>254</OffsetY><PanoramaPath>/properties/id1/Living room kitchen</PanoramaPath></Panorama><Panorama><Id>3</Id><OffsetX>480</OffsetX><OffsetY>355</OffsetY><PanoramaPath>/properties/id1/Sebago lake pano</PanoramaPath></Panorama></Panoramas><OriginalImagePath>/properties/id1/First+Floor.png</OriginalImagePath><DisplayImagePath>/properties/id1/disp_First+Floor.png</DisplayImagePath></FloorPlan>'),
    (@propertyId, 9, N'/properties/id1/Bathroom Pano'),
	(@propertyId, 9, N'/properties/id1/Living room kitchen'),
	(@propertyId, 9, N'/properties/id1/Sebago lake pano')

-- Amenities
SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Flat Screen TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cable / Satellite TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sound System')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wifi')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')))

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Small gas grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'BBQ Grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Outdoor Table')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garage')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Central Air')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Tennis Courts')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Great Views')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

-- UnitType
INSERT [dbo].[UnitTypes] ([PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (@propertyId, N'unit-type code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit Type')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit type in the Villa at the beach')))
SET @unitTypeId = SCOPE_IDENTITY()

-- Unit
INSERT [dbo].[Units] ([PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (@propertyId, @unitTypeId, N'unit code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit 1')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit in the Villa at the beach')), 1, 7, 3, 2)
SET @unitId = SCOPE_IDENTITY()

-- UnitStaticContent
INSERT [dbo].[UnitStaticContent] ([UnitId], [ContentCode], [ContentValue_i18n]) VALUES
	-- Bedrooms
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / King Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),

	-- Bathrooms
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / Full bath (ensuite)'))),
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Full Bath'))),

	-- LivingAreasAndDining
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 Couches / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Loveseat / Seats 2'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'3 Chairs / Seats 3'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Dining Table / Seats 10'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sun Room Dining Table / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Breakfast Bar / Seats 5')))

	-- KitchenAndLaundry
	-- to be filled, type = 13

-- Appliances
SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel stove and oven')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel dishwasher')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Washer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Dryer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cooking utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Toaster')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Slow cooker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food processor')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Mixer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine Refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine opener')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine rack')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garbage disposal')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Water filter')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Ice Maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel, branded appliances')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Granite or marble countertops')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end flooring')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end cabinetry and finishes')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Pots and pans')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Roasting pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Baking sheets')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cupcake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food storage containers')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster pots')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster eating utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Juice maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Waffle maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Highchair')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sets include a plate, bowl, knife, fork and spoon')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

-- Unit Rates
INSERT INTO [dbo].[UnitRates] ([UnitID], [DateFrom], [DateUntil], [DailyRate]) VALUES
	(@unitId, '2012-10-29', '2012-11-15', 400.00),
	(@unitId, '2012-11-16', '2012-11-24', 650.00),
	(@unitId, '2012-11-25', '2012-12-20', 400.00),
	(@unitId, '2012-12-21', '2013-01-02', 650.00),
	(@unitId, '2013-01-03', '2013-05-15', 400.00),
	(@unitId, '2013-05-16', '2013-08-31', 650.00),
	(@unitId, '2013-09-01', '2020-11-21', 400.00)

-- MLOS - max and min values found on: http://sqlserverplanet.com/tsql/max-date-value
INSERT INTO [dbo].[UnitTypeMLOS] ([UnitTypeID], [DateFrom], [DateUntil], [MLOS]) VALUES (@unitTypeId, '2013-01-01 00:00:00.000', '2013-12-31 23:59:59.998', 7)

-- Blockings
INSERT INTO [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES
	(@unitId, '2013-01-01', '2013-05-02', 2, NULL)

-- Reservation
INSERT [dbo].[Reservations] ([PropertyID], [UnitID], [UnitTypeId], [GuestID], [ConfirmationID], [BookingDate], [BookingStatus], [DateArrival], [DateDeparture], [TotalPrice], [ReservationCultureId], [TripBalance], [TransactionFee], [LinkAuthorizationToken]) VALUES 
	(@propertyId, @unitId, @unitTypeId, @guestId, N'1', N'2012-10-10 00:00:00.000', 1, N'2012-10-10 00:00:00.000', N'2012-10-10 00:00:00.000', 1.0000, 82, 1.0000, 0, N'F3DE0590-3552-44E6-B879-25D4B6421EA5')
set @reservationID = SCOPE_IDENTITY()

-- Reviews
INSERT INTO [dbo].[GuestReviews] ([GuestID], [PropertyID], [ReservationID], [ReviewContent], [Satisfaction_Experience], [Satisfaction_Cleanliness], [Satisfaction_Location], [ReviewTitle], [ReviewDate]) VALUES
	( @guestId, @propertyId, @reservationID, 'Review content data', 5, 4, 5, 'Superb House', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Another review content data', 5, 5, 4, 'Excellent neighbergood', DATEADD(day, -12, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 4, 4, 5, 'Fantastic House, Super views', DATEADD(day, -6, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'And another Review content data', 3, 5, 5, 'Good trip', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 5, 4, 'Fantastic House, Super views', DATEADD(day, -2, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 4, 5, 'Fantastic House, Super views', DATEADD(day, -32, GETDATE()) )

-----------------------
-- Property 3
-----------------------

-- Property

INSERT [dbo].[Properties] (
	[OwnerID], 
	[OTP_DestinationID], 
	[PropertyCode], 
	[PropertyName_i18n], 
	[Short_Description_i18n], 
	[Long_Description_i18n], 
	[Address1], 
	[Address2], 
	[State], 
	[ZIPCode], 
	[City], 
	[CountryId], 
	[Latitude], 
	[Longitude], 
	[Altitude], 
	[PropertyLive], 
	[StandardCommission], 
	[CheckIn], 
	[CheckOut], 
	[PropertyTypeId], [CultureId], [TimeZone]) 
	VALUES 
	(@ownerId, 
	@floridaDestinationId, 
	N'property-code', 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Calm Villa')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'House with nice views')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'This stunning home is located on scenic Sebago Lake in Maine, a short 3 minute walk from the lakeshore. The house sits on a small hill and has partial lake views from the kitchen, dining, family and sun rooms. The home was completely renovated in 2010 with high end finishes and beautiful details throughout. The kitchen includes stunning granite countertops, stainless steel appliances, and a wine refrigerator. The great room is stunning with vaulted, bead boarded ceilings and a panorama of large paned windows. Wood flooring throughout the entire home. With this home you will be able to relax and enjoy a little touch of luxury at the lake.')), 
	N'98 Wild Acres Road', 
	NULL, 
	N'Maine', 
	N'04071', 
	N'Raymond', 
	@countryId, 
	25.244696,
	-80.727539, 
	278, 
	1, 
	15.00, 
	'14:00', 
	'12:00', 
	@propertyTypeID, @cultureEnUsId, @usTimeZoneEastern)

SET @propertyId = SCOPE_IDENTITY()

-- Property KeyManager
INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (@propertyId,5,@ownerId)

-- PropertyStaticContent
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	-- SquareFootage
	(@propertyId, 10, N'2000')

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n]) VALUES
	-- Feature
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful views of the outdoors and partial lake views'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lake access, 3 minute walk to the lake shore'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Entire home renovated in 2010 with high end finishes'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Large, great room with stunning vaulted, bead board ceilings'))),

	-- LocalAreaDescription
	(@propertyId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sebago Lake is one of the largest and deepest lake in Maine. The lake is surrounded by several small towns including Raymond, Casco, Naples and Windham. Several recreational activities are available including fishing, hiking, biking, boating, swimming, and sailing. The fishing at Sebago is legendary with several species of lake indigenous or stocked by the state in the lake.'))),

	-- DistanceToMainAttraction
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to lakeshore'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.5 mile to zoo'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to sea'))),

	-- Arrival Info
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Keys: Keys are located in lockbox. Two keys are required for entry, a key for the regular lock (gold key) and a key for the dead bolt (silver key). Both keys turn left to open, right to lock.'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lockbox instructions: Lockbox is located on back door, code is 3346. Must disable alarm upon entry!'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Alarm instructions: Alarm is located inside closet next to downstairs bathroom, you will have one minute to disarm so you must move quickly once door is opened. If alarm is set off, call Oh The Places at (enter out 800 number here). Alarm may be left disarmed for duration of your stay.'))), 

	-- Parking Info
	(@propertyId, 15, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 car parking is available in driveway. Please, there is no street parking, your car will be ticketed if it is parked on the street')))

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	(@propertyId, 0, N'/prop3/thumb1.jpg'),
	(@propertyId, 0, N'/prop3/thumb2.jpg'),
	(@propertyId, 4, N'/prop3/listview1.jpg'),
	(@propertyId, 4, N'/prop3/listview2.jpg'),
	(@propertyId, 2, N'/prop3/fullscreen1.jpg'),
	-- Directions to Airport
	(@propertyId, 16, N'<DirectionToAirport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Name_i18n>&lt;i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;Portland International Jetport&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><DirectionLink>https://maps.google.com/maps?saddr=Portland+International+Jetport&amp;daddr=98+Wild+Acres+Road,+Raymond,+Maine+04071&amp;hl=pl&amp;ll=43.756217,-70.396957&amp;spn=0.304517,0.780029&amp;sll=44.559163,-70.054321&amp;sspn=2.403205,6.240234&amp;geocode=FZ_8mQId_SzP-yHPPvZLhaXH-ymBqy4kWZmyTDHPPvZLhaXH-w%3BFTYDnQIdeETM-yk90qZyvfSyTDH-RRb_eA5pdQ&amp;mra=ls&amp;t=m&amp;z=11></DirectionLink></DirectionToAirport>'),
    (@propertyId, 8, N'<FloorPlan xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Name_i18n>&lt;i18nString xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;First Floor&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><Panoramas><Panorama><Id>1</Id><OffsetX>324</OffsetX><OffsetY>62</OffsetY><PanoramaPath>/properties/id1/Bathroom Pano</PanoramaPath></Panorama><Panorama><Id>2</Id><OffsetX>232</OffsetX><OffsetY>254</OffsetY><PanoramaPath>/properties/id1/Living room kitchen</PanoramaPath></Panorama><Panorama><Id>3</Id><OffsetX>480</OffsetX><OffsetY>355</OffsetY><PanoramaPath>/properties/id1/Sebago lake pano</PanoramaPath></Panorama></Panoramas><OriginalImagePath>/properties/id1/First+Floor.png</OriginalImagePath><DisplayImagePath>/properties/id1/disp_First+Floor.png</DisplayImagePath></FloorPlan>'),
    (@propertyId, 9, N'/properties/id1/Bathroom Pano'),
	(@propertyId, 9, N'/properties/id1/Living room kitchen'),
	(@propertyId, 9, N'/properties/id1/Sebago lake pano')

-- Amenities
SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Flat Screen TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cable / Satellite TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sound System')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wifi')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')))

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Small gas grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'BBQ Grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Outdoor Table')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garage')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Central Air')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Tennis Courts')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Great Views')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

-- UnitType
INSERT [dbo].[UnitTypes] ([PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (@propertyId, N'unit-type code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit Type')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit type in the Villa at the beach')))
SET @unitTypeId = SCOPE_IDENTITY()

-- Unit
INSERT [dbo].[Units] ([PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (@propertyId, @unitTypeId, N'unit code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit 1')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit in the Villa at the beach')), 1, 7, 3, 2)
SET @unitId = SCOPE_IDENTITY()

-- UnitStaticContent
INSERT [dbo].[UnitStaticContent] ([UnitId], [ContentCode], [ContentValue_i18n]) VALUES
	-- Bedrooms
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / King Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),

	-- Bathrooms
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / Full bath (ensuite)'))),
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Full Bath'))),

	-- LivingAreasAndDining
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 Couches / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Loveseat / Seats 2'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'3 Chairs / Seats 3'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Dining Table / Seats 10'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sun Room Dining Table / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Breakfast Bar / Seats 5')))

	-- KitchenAndLaundry
	-- to be filled, type = 13

-- Appliances
SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel stove and oven')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel dishwasher')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Washer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Dryer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cooking utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Toaster')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Slow cooker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food processor')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Mixer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine Refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine opener')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine rack')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garbage disposal')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Water filter')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Ice Maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel, branded appliances')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Granite or marble countertops')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end flooring')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end cabinetry and finishes')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Pots and pans')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Roasting pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Baking sheets')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cupcake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food storage containers')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster pots')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster eating utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Juice maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Waffle maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Highchair')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sets include a plate, bowl, knife, fork and spoon')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

-- Unit Rates
INSERT INTO [dbo].[UnitRates] ([UnitID], [DateFrom], [DateUntil], [DailyRate]) VALUES
	(@unitId, '2012-10-29', '2012-11-15', 400.00),
	(@unitId, '2012-11-16', '2012-11-24', 650.00),
	(@unitId, '2012-11-25', '2012-12-20', 400.00),
	(@unitId, '2012-12-21', '2013-01-02', 650.00),
	(@unitId, '2013-01-03', '2013-05-15', 400.00),
	(@unitId, '2013-05-16', '2013-08-31', 650.00),
	(@unitId, '2013-09-01', '2020-11-21', 400.00)

-- MLOS - max and min values found on: http://sqlserverplanet.com/tsql/max-date-value
INSERT INTO [dbo].[UnitTypeMLOS] ([UnitTypeID], [DateFrom], [DateUntil], [MLOS]) VALUES (@unitTypeId, '2013-01-01 00:00:00.000', '2013-12-31 23:59:59.998', 7)

-- Blockings
INSERT INTO [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES
	(@unitId, '2013-01-01', '2013-05-02', 2, NULL)

-- Reservation
INSERT [dbo].[Reservations] ([PropertyID], [UnitID], [UnitTypeId], [GuestID], [ConfirmationID], [BookingDate], [BookingStatus], [DateArrival], [DateDeparture], [TotalPrice], [ReservationCultureId], [TripBalance], [TransactionFee], [LinkAuthorizationToken]) VALUES 
	(@propertyId, @unitId, @unitTypeId, @guestId, N'1', N'2012-10-10 00:00:00.000', 1, N'2012-10-10 00:00:00.000', N'2012-10-10 00:00:00.000', 1.0000, 82, 1.0000, 0, N'E155FE07-88E7-44FA-995B-2D3480332507')
set @reservationID = SCOPE_IDENTITY()

-- Reviews
INSERT INTO [dbo].[GuestReviews] ([GuestID], [PropertyID], [ReservationID], [ReviewContent], [Satisfaction_Experience], [Satisfaction_Cleanliness], [Satisfaction_Location], [ReviewTitle], [ReviewDate]) VALUES
	( @guestId, @propertyId, @reservationID, 'Review content data', 5, 4, 5, 'Superb House', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Another review content data', 5, 5, 4, 'Excellent neighbergood', DATEADD(day, -12, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 4, 4, 5, 'Fantastic House, Super views', DATEADD(day, -6, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'And another Review content data', 3, 5, 5, 'Good trip', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 5, 4, 'Fantastic House, Super views', DATEADD(day, -2, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 4, 5, 'Fantastic House, Super views', DATEADD(day, -32, GETDATE()) )

-----------------------
-- Property 4
-----------------------

-- Property

INSERT [dbo].[Properties] (
	[OwnerID], 
	[OTP_DestinationID], 
	[PropertyCode], 
	[PropertyName_i18n], 
	[Short_Description_i18n], 
	[Long_Description_i18n], 
	[Address1], 
	[Address2], 
	[State], 
	[ZIPCode], 
	[City], 
	[CountryId], 
	[Latitude], 
	[Longitude], 
	[Altitude], 
	[PropertyLive], 
	[StandardCommission], 
	[CheckIn], 
	[CheckOut], 
	[PropertyTypeId], [CultureId], [TimeZone]) 
	VALUES 
	(@ownerId, 
	@floridaDestinationId, 
	N'property-code', 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Riviera House')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful House with fabulous views')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'This stunning home is located on scenic Sebago Lake in Maine, a short 3 minute walk from the lakeshore. The house sits on a small hill and has partial lake views from the kitchen, dining, family and sun rooms. The home was completely renovated in 2010 with high end finishes and beautiful details throughout. The kitchen includes stunning granite countertops, stainless steel appliances, and a wine refrigerator. The great room is stunning with vaulted, bead boarded ceilings and a panorama of large paned windows. Wood flooring throughout the entire home. With this home you will be able to relax and enjoy a little touch of luxury at the lake.')), 
	N'98 Wild Acres Road', 
	NULL, 
	N'Maine', 
	N'04071', 
	N'Raymond', 
	@countryId, 
	27.254630,
	-82.441406, 
	278, 
	1, 
	15.00, 
	'14:00', 
	'12:00', 
	@propertyTypeID, @cultureEnUsId, @usTimeZoneEastern)

SET @propertyId = SCOPE_IDENTITY()

-- Property KeyManager
INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (@propertyId,5,@ownerId)

-- PropertyStaticContent
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	-- SquareFootage
	(@propertyId, 10, N'2000')

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n]) VALUES
	-- Feature
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful views of the outdoors and partial lake views'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lake access, 3 minute walk to the lake shore'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Entire home renovated in 2010 with high end finishes'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Large, great room with stunning vaulted, bead board ceilings'))),

	-- LocalAreaDescription
	(@propertyId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sebago Lake is one of the largest and deepest lake in Maine. The lake is surrounded by several small towns including Raymond, Casco, Naples and Windham. Several recreational activities are available including fishing, hiking, biking, boating, swimming, and sailing. The fishing at Sebago is legendary with several species of lake indigenous or stocked by the state in the lake.'))),

	-- DistanceToMainAttraction
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to lakeshore'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.5 mile to zoo'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to sea'))),

	-- Arrival Info
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Keys: Keys are located in lockbox. Two keys are required for entry, a key for the regular lock (gold key) and a key for the dead bolt (silver key). Both keys turn left to open, right to lock.'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lockbox instructions: Lockbox is located on back door, code is 3346. Must disable alarm upon entry!'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Alarm instructions: Alarm is located inside closet next to downstairs bathroom, you will have one minute to disarm so you must move quickly once door is opened. If alarm is set off, call Oh The Places at (enter out 800 number here). Alarm may be left disarmed for duration of your stay.'))), 

	-- Parking Info
	(@propertyId, 15, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 car parking is available in driveway. Please, there is no street parking, your car will be ticketed if it is parked on the street')))

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	(@propertyId, 0, N'/prop4/thumb1.jpg'),
	(@propertyId, 0, N'/prop4/thumb2.jpg'),
	(@propertyId, 4, N'/prop4/listview1.jpg'),
	(@propertyId, 4, N'/prop4/listview2.jpg'),
	(@propertyId, 2, N'/prop4/fullscreen1.jpg'),
	-- Directions to Airport
	(@propertyId, 16, N'<DirectionToAirport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Name_i18n>&lt;i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;Portland International Jetport&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><DirectionLink>https://maps.google.com/maps?saddr=Portland+International+Jetport&amp;daddr=98+Wild+Acres+Road,+Raymond,+Maine+04071&amp;hl=pl&amp;ll=43.756217,-70.396957&amp;spn=0.304517,0.780029&amp;sll=44.559163,-70.054321&amp;sspn=2.403205,6.240234&amp;geocode=FZ_8mQId_SzP-yHPPvZLhaXH-ymBqy4kWZmyTDHPPvZLhaXH-w%3BFTYDnQIdeETM-yk90qZyvfSyTDH-RRb_eA5pdQ&amp;mra=ls&amp;t=m&amp;z=11></DirectionLink></DirectionToAirport>'),
    (@propertyId, 8, N'<FloorPlan xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Name_i18n>&lt;i18nString xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;First Floor&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><Panoramas><Panorama><Id>1</Id><OffsetX>324</OffsetX><OffsetY>62</OffsetY><PanoramaPath>/properties/id1/Bathroom Pano</PanoramaPath></Panorama><Panorama><Id>2</Id><OffsetX>232</OffsetX><OffsetY>254</OffsetY><PanoramaPath>/properties/id1/Living room kitchen</PanoramaPath></Panorama><Panorama><Id>3</Id><OffsetX>480</OffsetX><OffsetY>355</OffsetY><PanoramaPath>/properties/id1/Sebago lake pano</PanoramaPath></Panorama></Panoramas><OriginalImagePath>/properties/id1/First+Floor.png</OriginalImagePath><DisplayImagePath>/properties/id1/disp_First+Floor.png</DisplayImagePath></FloorPlan>'),
    (@propertyId, 9, N'/properties/id1/Bathroom Pano'),
	(@propertyId, 9, N'/properties/id1/Living room kitchen'),
	(@propertyId, 9, N'/properties/id1/Sebago lake pano')

-- Amenities
SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Flat Screen TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cable / Satellite TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sound System')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wifi')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')))

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Small gas grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'BBQ Grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Outdoor Table')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garage')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Central Air')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Tennis Courts')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Great Views')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

-- UnitType
INSERT [dbo].[UnitTypes] ([PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (@propertyId, N'unit-type code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit Type')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit type in the Villa at the beach')))
SET @unitTypeId = SCOPE_IDENTITY()

-- Unit
INSERT [dbo].[Units] ([PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (@propertyId, @unitTypeId, N'unit code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit 1')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit in the Villa at the beach')), 1, 7, 3, 2)
SET @unitId = SCOPE_IDENTITY()

-- UnitStaticContent
INSERT [dbo].[UnitStaticContent] ([UnitId], [ContentCode], [ContentValue_i18n]) VALUES
	-- Bedrooms
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / King Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),

	-- Bathrooms
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / Full bath (ensuite)'))),
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Full Bath'))),

	-- LivingAreasAndDining
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 Couches / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Loveseat / Seats 2'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'3 Chairs / Seats 3'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Dining Table / Seats 10'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sun Room Dining Table / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Breakfast Bar / Seats 5')))

	-- KitchenAndLaundry
	-- to be filled, type = 13

-- Appliances
SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel stove and oven')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel dishwasher')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Washer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Dryer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cooking utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Toaster')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Slow cooker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food processor')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Mixer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine Refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine opener')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine rack')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garbage disposal')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Water filter')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Ice Maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel, branded appliances')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Granite or marble countertops')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end flooring')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end cabinetry and finishes')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Pots and pans')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Roasting pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Baking sheets')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cupcake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food storage containers')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster pots')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster eating utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Juice maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Waffle maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Highchair')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sets include a plate, bowl, knife, fork and spoon')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

-- Unit Rates
INSERT INTO [dbo].[UnitRates] ([UnitID], [DateFrom], [DateUntil], [DailyRate]) VALUES
	(@unitId, '2012-10-29', '2012-11-15', 400.00),
	(@unitId, '2012-11-16', '2012-11-24', 650.00),
	(@unitId, '2012-11-25', '2012-12-20', 400.00),
	(@unitId, '2012-12-21', '2013-01-02', 650.00),
	(@unitId, '2013-01-03', '2013-05-15', 400.00),
	(@unitId, '2013-05-16', '2013-08-31', 650.00),
	(@unitId, '2013-09-01', '2020-11-21', 400.00)

-- MLOS - max and min values found on: http://sqlserverplanet.com/tsql/max-date-value
INSERT INTO [dbo].[UnitTypeMLOS] ([UnitTypeID], [DateFrom], [DateUntil], [MLOS]) VALUES (@unitTypeId, '2013-01-01 00:00:00.000', '2013-12-31 23:59:59.998', 7)

-- Blockings
INSERT INTO [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES
	(@unitId, '2013-01-01', '2013-05-02', 2, NULL)

-- Reservation
INSERT [dbo].[Reservations] ([PropertyID], [UnitID], [UnitTypeId], [GuestID], [ConfirmationID], [BookingDate], [BookingStatus], [DateArrival], [DateDeparture], [TotalPrice], [ReservationCultureId], [TripBalance], [TransactionFee], [LinkAuthorizationToken]) VALUES 
	(@propertyId, @unitId, @unitTypeId, @guestId, N'1', N'2012-10-10 00:00:00.000', 1, N'2012-10-10 00:00:00.000', N'2012-10-10 00:00:00.000', 1.0000, 82, 1.0000, 0, N'DC7DF36E-2027-4E1B-BB06-51BD532467EE')
set @reservationID = SCOPE_IDENTITY()

-- Reviews
INSERT INTO [dbo].[GuestReviews] ([GuestID], [PropertyID], [ReservationID], [ReviewContent], [Satisfaction_Experience], [Satisfaction_Cleanliness], [Satisfaction_Location], [ReviewTitle], [ReviewDate]) VALUES
	( @guestId, @propertyId, @reservationID, 'Review content data', 5, 4, 5, 'Superb House', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Another review content data', 5, 5, 4, 'Excellent neighbergood', DATEADD(day, -12, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 4, 4, 5, 'Fantastic House, Super views', DATEADD(day, -6, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'And another Review content data', 3, 5, 5, 'Good trip', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 5, 4, 'Fantastic House, Super views', DATEADD(day, -2, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 4, 5, 'Fantastic House, Super views', DATEADD(day, -32, GETDATE()) )

-----------------------
-- Property 5
-----------------------

-- Property

INSERT [dbo].[Properties] (
	[OwnerID], 
	[OTP_DestinationID], 
	[PropertyCode], 
	[PropertyName_i18n], 
	[Short_Description_i18n], 
	[Long_Description_i18n], 
	[Address1], 
	[Address2], 
	[State], 
	[ZIPCode], 
	[City], 
	[CountryId],
	[Latitude], 
	[Longitude], 
	[Altitude], 
	[PropertyLive], 
	[StandardCommission], 
	[CheckIn], 
	[CheckOut], 
	[PropertyTypeId], [CultureId], [TimeZone]) 
	VALUES 
	(@ownerId, 
	@floridaDestinationId, 
	N'property-code', 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Malibu Villa')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful House with fabulous views')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'This stunning home is located on scenic Sebago Lake in Maine, a short 3 minute walk from the lakeshore. The house sits on a small hill and has partial lake views from the kitchen, dining, family and sun rooms. The home was completely renovated in 2010 with high end finishes and beautiful details throughout. The kitchen includes stunning granite countertops, stainless steel appliances, and a wine refrigerator. The great room is stunning with vaulted, bead boarded ceilings and a panorama of large paned windows. Wood flooring throughout the entire home. With this home you will be able to relax and enjoy a little touch of luxury at the lake.')), 
	N'98 Wild Acres Road', 
	NULL, 
	N'Maine', 
	N'04071', 
	N'Raymond', 
	@countryId,
	30.107118,
	-81.474609, 
	278, 
	1, 
	15.00, 
	'14:00', 
	'12:00', 
	@propertyTypeID, @cultureEnUsId, @usTimeZoneEastern)

SET @propertyId = SCOPE_IDENTITY()

-- Property KeyManager
INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (@propertyId,5,@ownerId)

-- PropertyStaticContent
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	-- SquareFootage
	(@propertyId, 10, N'2000')

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n]) VALUES
	-- Feature
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful views of the outdoors and partial lake views'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lake access, 3 minute walk to the lake shore'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Entire home renovated in 2010 with high end finishes'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Large, great room with stunning vaulted, bead board ceilings'))),

	-- LocalAreaDescription
	(@propertyId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sebago Lake is one of the largest and deepest lake in Maine. The lake is surrounded by several small towns including Raymond, Casco, Naples and Windham. Several recreational activities are available including fishing, hiking, biking, boating, swimming, and sailing. The fishing at Sebago is legendary with several species of lake indigenous or stocked by the state in the lake.'))),

	-- DistanceToMainAttraction
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to lakeshore'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.5 mile to zoo'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to sea'))),

	-- Arrival Info
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Keys: Keys are located in lockbox. Two keys are required for entry, a key for the regular lock (gold key) and a key for the dead bolt (silver key). Both keys turn left to open, right to lock.'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lockbox instructions: Lockbox is located on back door, code is 3346. Must disable alarm upon entry!'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Alarm instructions: Alarm is located inside closet next to downstairs bathroom, you will have one minute to disarm so you must move quickly once door is opened. If alarm is set off, call Oh The Places at (enter out 800 number here). Alarm may be left disarmed for duration of your stay.'))), 

	-- Parking Info
	(@propertyId, 15, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 car parking is available in driveway. Please, there is no street parking, your car will be ticketed if it is parked on the street')))

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	(@propertyId, 0, N'/prop5/thumb1.jpg'),
	(@propertyId, 0, N'/prop5/thumb2.jpg'),
	(@propertyId, 4, N'/prop5/listview1.jpg'),
	(@propertyId, 4, N'/prop5/listview2.jpg'),
	(@propertyId, 2, N'/prop5/fullscreen1.jpg'),
	-- Directions to Airport
	(@propertyId, 16, N'<DirectionToAirport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Name_i18n>&lt;i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;Portland International Jetport&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><DirectionLink>https://maps.google.com/maps?saddr=Portland+International+Jetport&amp;daddr=98+Wild+Acres+Road,+Raymond,+Maine+04071&amp;hl=pl&amp;ll=43.756217,-70.396957&amp;spn=0.304517,0.780029&amp;sll=44.559163,-70.054321&amp;sspn=2.403205,6.240234&amp;geocode=FZ_8mQId_SzP-yHPPvZLhaXH-ymBqy4kWZmyTDHPPvZLhaXH-w%3BFTYDnQIdeETM-yk90qZyvfSyTDH-RRb_eA5pdQ&amp;mra=ls&amp;t=m&amp;z=11></DirectionLink></DirectionToAirport>'),
    (@propertyId, 8, N'<FloorPlan xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Name_i18n>&lt;i18nString xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;First Floor&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><Panoramas><Panorama><Id>1</Id><OffsetX>324</OffsetX><OffsetY>62</OffsetY><PanoramaPath>/properties/id1/Bathroom Pano</PanoramaPath></Panorama><Panorama><Id>2</Id><OffsetX>232</OffsetX><OffsetY>254</OffsetY><PanoramaPath>/properties/id1/Living room kitchen</PanoramaPath></Panorama><Panorama><Id>3</Id><OffsetX>480</OffsetX><OffsetY>355</OffsetY><PanoramaPath>/properties/id1/Sebago lake pano</PanoramaPath></Panorama></Panoramas><OriginalImagePath>/properties/id1/First+Floor.png</OriginalImagePath><DisplayImagePath>/properties/id1/disp_First+Floor.png</DisplayImagePath></FloorPlan>'),
    (@propertyId, 9, N'/properties/id1/Bathroom Pano'),
	(@propertyId, 9, N'/properties/id1/Living room kitchen'),
	(@propertyId, 9, N'/properties/id1/Sebago lake pano')

-- Amenities
SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Flat Screen TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cable / Satellite TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sound System')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wifi')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')))

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Small gas grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'BBQ Grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Outdoor Table')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garage')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Central Air')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Tennis Courts')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Great Views')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

-- UnitType
INSERT [dbo].[UnitTypes] ([PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (@propertyId, N'unit-type code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit Type')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit type in the Villa at the beach')))
SET @unitTypeId = SCOPE_IDENTITY()

-- Unit
INSERT [dbo].[Units] ([PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (@propertyId, @unitTypeId, N'unit code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit 1')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit in the Villa at the beach')), 1, 7, 3, 2)
SET @unitId = SCOPE_IDENTITY()

-- UnitStaticContent
INSERT [dbo].[UnitStaticContent] ([UnitId], [ContentCode], [ContentValue_i18n]) VALUES
	-- Bedrooms
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / King Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),

	-- Bathrooms
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / Full bath (ensuite)'))),
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Full Bath'))),

	-- LivingAreasAndDining
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 Couches / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Loveseat / Seats 2'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'3 Chairs / Seats 3'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Dining Table / Seats 10'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sun Room Dining Table / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Breakfast Bar / Seats 5')))

	-- KitchenAndLaundry
	-- to be filled, type = 13

-- Appliances
SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel stove and oven')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel dishwasher')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Washer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Dryer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cooking utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Toaster')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Slow cooker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food processor')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Mixer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine Refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine opener')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine rack')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garbage disposal')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Water filter')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Ice Maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel, branded appliances')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Granite or marble countertops')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end flooring')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end cabinetry and finishes')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Pots and pans')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Roasting pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Baking sheets')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cupcake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food storage containers')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster pots')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster eating utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Juice maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Waffle maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Highchair')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sets include a plate, bowl, knife, fork and spoon')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

-- Unit Rates
INSERT INTO [dbo].[UnitRates] ([UnitID], [DateFrom], [DateUntil], [DailyRate]) VALUES
	(@unitId, '2012-10-29', '2012-11-15', 400.00),
	(@unitId, '2012-11-16', '2012-11-24', 650.00),
	(@unitId, '2012-11-25', '2012-12-20', 400.00),
	(@unitId, '2012-12-21', '2013-01-02', 650.00),
	(@unitId, '2013-01-03', '2013-05-15', 400.00),
	(@unitId, '2013-05-16', '2013-08-31', 650.00),
	(@unitId, '2013-09-01', '2020-11-21', 400.00)

-- MLOS - max and min values found on: http://sqlserverplanet.com/tsql/max-date-value
INSERT INTO [dbo].[UnitTypeMLOS] ([UnitTypeID], [DateFrom], [DateUntil], [MLOS]) VALUES (@unitTypeId, '2013-01-01 00:00:00.000', '2013-12-31 23:59:59.998', 7)

-- Blockings
INSERT INTO [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES
	(@unitId, '2013-01-01', '2013-05-02', 2, NULL)

-- Reservation
INSERT [dbo].[Reservations] ([PropertyID], [UnitID], [UnitTypeId], [GuestID], [ConfirmationID], [BookingDate], [BookingStatus], [DateArrival], [DateDeparture], [TotalPrice], [ReservationCultureId], [TripBalance], [TransactionFee], [LinkAuthorizationToken]) VALUES 
	(@propertyId, @unitId, @unitTypeId, @guestId, N'1', N'2012-10-10 00:00:00.000', 1, N'2012-10-10 00:00:00.000', N'2012-10-10 00:00:00.000', 1.0000, 82, 1.0000, 0, N'5B9AD415-064D-4562-B2EB-30A4787E0FCB')
set @reservationID = SCOPE_IDENTITY()

-- Reviews
INSERT INTO [dbo].[GuestReviews] ([GuestID], [PropertyID], [ReservationID], [ReviewContent], [Satisfaction_Experience], [Satisfaction_Cleanliness], [Satisfaction_Location], [ReviewTitle], [ReviewDate]) VALUES
	( @guestId, @propertyId, @reservationID, 'Review content data', 5, 4, 5, 'Superb House', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Another review content data', 5, 5, 4, 'Excellent neighbergood', DATEADD(day, -12, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 4, 4, 5, 'Fantastic House, Super views', DATEADD(day, -6, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'And another Review content data', 3, 5, 5, 'Good trip', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 5, 4, 'Fantastic House, Super views', DATEADD(day, -2, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 4, 5, 'Fantastic House, Super views', DATEADD(day, -32, GETDATE()) )

-----------------------
-- Property 6
-----------------------

-- Property

INSERT [dbo].[Properties] (
	[OwnerID], 
	[OTP_DestinationID], 
	[PropertyCode], 
	[PropertyName_i18n], 
	[Short_Description_i18n], 
	[Long_Description_i18n], 
	[Address1], 
	[Address2], 
	[State], 
	[ZIPCode], 
	[City], 
	[CountryId], 
	[Latitude], 
	[Longitude], 
	[Altitude], 
	[PropertyLive], 
	[StandardCommission], 
	[CheckIn], 
	[CheckOut], 
	[PropertyTypeId], [CultureId], [TimeZone]) 
	VALUES 
	(@ownerId, 
	@floridaDestinationId, 
	N'property-code', 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'The sea House')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful House with fabulous views')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'This stunning home is located on scenic Sebago Lake in Maine, a short 3 minute walk from the lakeshore. The house sits on a small hill and has partial lake views from the kitchen, dining, family and sun rooms. The home was completely renovated in 2010 with high end finishes and beautiful details throughout. The kitchen includes stunning granite countertops, stainless steel appliances, and a wine refrigerator. The great room is stunning with vaulted, bead boarded ceilings and a panorama of large paned windows. Wood flooring throughout the entire home. With this home you will be able to relax and enjoy a little touch of luxury at the lake.')), 
	N'98 Wild Acres Road', 
	NULL, 
	N'Maine', 
	N'04071', 
	N'Raymond', 
	@countryId, 
	29.036961,
	-81.166992, 
	278, 
	1, 
	15.00, 
	'14:00', 
	'12:00', 
	@propertyTypeID, @cultureEnUsId, @usTimeZoneEastern)

SET @propertyId = SCOPE_IDENTITY()

-- Property KeyManager
INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (@propertyId,5,@ownerId)

-- PropertyStaticContent
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	-- SquareFootage
	(@propertyId, 10, N'2000')

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n]) VALUES
	-- Feature
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful views of the outdoors and partial lake views'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lake access, 3 minute walk to the lake shore'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Entire home renovated in 2010 with high end finishes'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Large, great room with stunning vaulted, bead board ceilings'))),

	-- LocalAreaDescription
	(@propertyId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sebago Lake is one of the largest and deepest lake in Maine. The lake is surrounded by several small towns including Raymond, Casco, Naples and Windham. Several recreational activities are available including fishing, hiking, biking, boating, swimming, and sailing. The fishing at Sebago is legendary with several species of lake indigenous or stocked by the state in the lake.'))),

	-- DistanceToMainAttraction
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to lakeshore'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.5 mile to zoo'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to sea'))),

	-- Arrival Info
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Keys: Keys are located in lockbox. Two keys are required for entry, a key for the regular lock (gold key) and a key for the dead bolt (silver key). Both keys turn left to open, right to lock.'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lockbox instructions: Lockbox is located on back door, code is 3346. Must disable alarm upon entry!'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Alarm instructions: Alarm is located inside closet next to downstairs bathroom, you will have one minute to disarm so you must move quickly once door is opened. If alarm is set off, call Oh The Places at (enter out 800 number here). Alarm may be left disarmed for duration of your stay.'))), 

	-- Parking Info
	(@propertyId, 15, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 car parking is available in driveway. Please, there is no street parking, your car will be ticketed if it is parked on the street')))

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	(@propertyId, 0, N'/prop6/thumb1.jpg'),
	(@propertyId, 0, N'/prop6/thumb2.jpg'),
	(@propertyId, 4, N'/prop6/listview1.jpg'),
	(@propertyId, 4, N'/prop6/listview2.jpg'),
	(@propertyId, 2, N'/prop6/fullscreen1.jpg'),
	-- Directions to Airport
	(@propertyId, 16, N'<DirectionToAirport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Name_i18n>&lt;i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;Portland International Jetport&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><DirectionLink>https://maps.google.com/maps?saddr=Portland+International+Jetport&amp;daddr=98+Wild+Acres+Road,+Raymond,+Maine+04071&amp;hl=pl&amp;ll=43.756217,-70.396957&amp;spn=0.304517,0.780029&amp;sll=44.559163,-70.054321&amp;sspn=2.403205,6.240234&amp;geocode=FZ_8mQId_SzP-yHPPvZLhaXH-ymBqy4kWZmyTDHPPvZLhaXH-w%3BFTYDnQIdeETM-yk90qZyvfSyTDH-RRb_eA5pdQ&amp;mra=ls&amp;t=m&amp;z=11></DirectionLink></DirectionToAirport>'),
    (@propertyId, 8, N'<FloorPlan xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Name_i18n>&lt;i18nString xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;First Floor&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><Panoramas><Panorama><Id>1</Id><OffsetX>324</OffsetX><OffsetY>62</OffsetY><PanoramaPath>/properties/id1/Bathroom Pano</PanoramaPath></Panorama><Panorama><Id>2</Id><OffsetX>232</OffsetX><OffsetY>254</OffsetY><PanoramaPath>/properties/id1/Living room kitchen</PanoramaPath></Panorama><Panorama><Id>3</Id><OffsetX>480</OffsetX><OffsetY>355</OffsetY><PanoramaPath>/properties/id1/Sebago lake pano</PanoramaPath></Panorama></Panoramas><OriginalImagePath>/properties/id1/First+Floor.png</OriginalImagePath><DisplayImagePath>/properties/id1/disp_First+Floor.png</DisplayImagePath></FloorPlan>'),
    (@propertyId, 9, N'/properties/id1/Bathroom Pano'),
	(@propertyId, 9, N'/properties/id1/Living room kitchen'),
	(@propertyId, 9, N'/properties/id1/Sebago lake pano')

-- Amenities
SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Flat Screen TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cable / Satellite TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sound System')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wifi')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')))

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Small gas grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'BBQ Grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Outdoor Table')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garage')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Central Air')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Tennis Courts')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Great Views')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

-- UnitType
INSERT [dbo].[UnitTypes] ([PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (@propertyId, N'unit-type code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit Type')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit type in the Villa at the beach')))
SET @unitTypeId = SCOPE_IDENTITY()

-- Unit
INSERT [dbo].[Units] ([PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (@propertyId, @unitTypeId, N'unit code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit 1')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit in the Villa at the beach')), 1, 7, 3, 2)
SET @unitId = SCOPE_IDENTITY()

-- UnitStaticContent
INSERT [dbo].[UnitStaticContent] ([UnitId], [ContentCode], [ContentValue_i18n]) VALUES
	-- Bedrooms
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / King Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),

	-- Bathrooms
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / Full bath (ensuite)'))),
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Full Bath'))),

	-- LivingAreasAndDining
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 Couches / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Loveseat / Seats 2'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'3 Chairs / Seats 3'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Dining Table / Seats 10'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sun Room Dining Table / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Breakfast Bar / Seats 5')))

	-- KitchenAndLaundry
	-- to be filled, type = 13

-- Appliances
SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel stove and oven')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel dishwasher')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Washer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Dryer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cooking utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Toaster')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Slow cooker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food processor')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Mixer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine Refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine opener')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine rack')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garbage disposal')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Water filter')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Ice Maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel, branded appliances')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Granite or marble countertops')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end flooring')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end cabinetry and finishes')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Pots and pans')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Roasting pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Baking sheets')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cupcake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food storage containers')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster pots')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster eating utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Juice maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Waffle maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Highchair')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sets include a plate, bowl, knife, fork and spoon')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

-- Unit Rates
INSERT INTO [dbo].[UnitRates] ([UnitID], [DateFrom], [DateUntil], [DailyRate]) VALUES
	(@unitId, '2012-10-29', '2012-11-15', 400.00),
	(@unitId, '2012-11-16', '2012-11-24', 650.00),
	(@unitId, '2012-11-25', '2012-12-20', 400.00),
	(@unitId, '2012-12-21', '2013-01-02', 650.00),
	(@unitId, '2013-01-03', '2013-05-15', 400.00),
	(@unitId, '2013-05-16', '2013-08-31', 650.00),
	(@unitId, '2013-09-01', '2020-11-21', 400.00)

-- MLOS - max and min values found on: http://sqlserverplanet.com/tsql/max-date-value
INSERT INTO [dbo].[UnitTypeMLOS] ([UnitTypeID], [DateFrom], [DateUntil], [MLOS]) VALUES (@unitTypeId, '2013-01-01 00:00:00.000', '2013-12-31 23:59:59.998', 7)

-- Blockings
INSERT INTO [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES
	(@unitId, '2013-01-01', '2013-05-02', 2, NULL)

-- Reservation
INSERT [dbo].[Reservations] ([PropertyID], [UnitID], [UnitTypeId], [GuestID], [ConfirmationID], [BookingDate], [BookingStatus], [DateArrival], [DateDeparture], [TotalPrice], [ReservationCultureId], [TripBalance], [TransactionFee], [LinkAuthorizationToken]) VALUES 
	(@propertyId, @unitId, @unitTypeId, @guestId, N'1', N'2012-10-10 00:00:00.000', 1, N'2012-10-10 00:00:00.000', N'2012-10-10 00:00:00.000', 1.0000, 82, 1.0000, 0, N'81015B27-E706-4B5D-8177-6365D3805EF8')
set @reservationID = SCOPE_IDENTITY()

-- Reviews
INSERT INTO [dbo].[GuestReviews] ([GuestID], [PropertyID], [ReservationID], [ReviewContent], [Satisfaction_Experience], [Satisfaction_Cleanliness], [Satisfaction_Location], [ReviewTitle], [ReviewDate]) VALUES
	( @guestId, @propertyId, @reservationID, 'Review content data', 5, 4, 5, 'Superb House', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Another review content data', 5, 5, 4, 'Excellent neighbergood', DATEADD(day, -12, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 4, 4, 5, 'Fantastic House, Super views', DATEADD(day, -6, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'And another Review content data', 3, 5, 5, 'Good trip', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 5, 4, 'Fantastic House, Super views', DATEADD(day, -2, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 4, 5, 'Fantastic House, Super views', DATEADD(day, -32, GETDATE()) )

-----------------------
-- Property 7
-----------------------

-- Property

INSERT [dbo].[Properties] (
	[OwnerID], 
	[OTP_DestinationID], 
	[PropertyCode], 
	[PropertyName_i18n], 
	[Short_Description_i18n], 
	[Long_Description_i18n], 
	[Address1], 
	[Address2], 
	[State], 
	[ZIPCode], 
	[City], 
	[CountryId], 
	[Latitude], 
	[Longitude], 
	[Altitude], 
	[PropertyLive], 
	[StandardCommission], 
	[CheckIn], 
	[CheckOut], 
	[PropertyTypeId], [CultureId], [TimeZone]) 
	VALUES 
	(@ownerId, 
	@floridaDestinationId, 
	N'property-code', 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beach Villa')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful House with fabulous views')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'This stunning home is located on scenic Sebago Lake in Maine, a short 3 minute walk from the lakeshore. The house sits on a small hill and has partial lake views from the kitchen, dining, family and sun rooms. The home was completely renovated in 2010 with high end finishes and beautiful details throughout. The kitchen includes stunning granite countertops, stainless steel appliances, and a wine refrigerator. The great room is stunning with vaulted, bead boarded ceilings and a panorama of large paned windows. Wood flooring throughout the entire home. With this home you will be able to relax and enjoy a little touch of luxury at the lake.')), 
	N'98 Wild Acres Road', 
	NULL, 
	N'Maine', 
	N'04071', 
	N'Raymond', 
	@countryId, 
	26.352498,
	-81.870117, 
	278, 
	1, 
	15.00, 
	'14:00', 
	'12:00', 
	@propertyTypeID, @cultureEnUsId, @usTimeZoneEastern)

SET @propertyId = SCOPE_IDENTITY()

-- Property KeyManager
INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (@propertyId,5,@ownerId)

-- PropertyStaticContent
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	-- SquareFootage
	(@propertyId, 10, N'2000')

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n]) VALUES
	-- Feature
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful views of the outdoors and partial lake views'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lake access, 3 minute walk to the lake shore'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Entire home renovated in 2010 with high end finishes'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Large, great room with stunning vaulted, bead board ceilings'))),

	-- LocalAreaDescription
	(@propertyId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sebago Lake is one of the largest and deepest lake in Maine. The lake is surrounded by several small towns including Raymond, Casco, Naples and Windham. Several recreational activities are available including fishing, hiking, biking, boating, swimming, and sailing. The fishing at Sebago is legendary with several species of lake indigenous or stocked by the state in the lake.'))),

	-- DistanceToMainAttraction
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to lakeshore'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.5 mile to zoo'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to sea'))),

	-- Arrival Info
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Keys: Keys are located in lockbox. Two keys are required for entry, a key for the regular lock (gold key) and a key for the dead bolt (silver key). Both keys turn left to open, right to lock.'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lockbox instructions: Lockbox is located on back door, code is 3346. Must disable alarm upon entry!'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Alarm instructions: Alarm is located inside closet next to downstairs bathroom, you will have one minute to disarm so you must move quickly once door is opened. If alarm is set off, call Oh The Places at (enter out 800 number here). Alarm may be left disarmed for duration of your stay.'))), 

	-- Parking Info
	(@propertyId, 15, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 car parking is available in driveway. Please, there is no street parking, your car will be ticketed if it is parked on the street')))

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	(@propertyId, 0, N'/prop7/thumb1.jpg'),
	(@propertyId, 0, N'/prop7/thumb2.jpg'),
	(@propertyId, 4, N'/prop7/listview1.jpg'),
	(@propertyId, 4, N'/prop7/listview2.jpg'),
	(@propertyId, 2, N'/prop7/fullscreen1.jpg'),
	-- Directions to Airport
	(@propertyId, 16, N'<DirectionToAirport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Name_i18n>&lt;i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;Portland International Jetport&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><DirectionLink>https://maps.google.com/maps?saddr=Portland+International+Jetport&amp;daddr=98+Wild+Acres+Road,+Raymond,+Maine+04071&amp;hl=pl&amp;ll=43.756217,-70.396957&amp;spn=0.304517,0.780029&amp;sll=44.559163,-70.054321&amp;sspn=2.403205,6.240234&amp;geocode=FZ_8mQId_SzP-yHPPvZLhaXH-ymBqy4kWZmyTDHPPvZLhaXH-w%3BFTYDnQIdeETM-yk90qZyvfSyTDH-RRb_eA5pdQ&amp;mra=ls&amp;t=m&amp;z=11></DirectionLink></DirectionToAirport>'),
    (@propertyId, 8, N'<FloorPlan xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Name_i18n>&lt;i18nString xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;First Floor&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><Panoramas><Panorama><Id>1</Id><OffsetX>324</OffsetX><OffsetY>62</OffsetY><PanoramaPath>/properties/id1/Bathroom Pano</PanoramaPath></Panorama><Panorama><Id>2</Id><OffsetX>232</OffsetX><OffsetY>254</OffsetY><PanoramaPath>/properties/id1/Living room kitchen</PanoramaPath></Panorama><Panorama><Id>3</Id><OffsetX>480</OffsetX><OffsetY>355</OffsetY><PanoramaPath>/properties/id1/Sebago lake pano</PanoramaPath></Panorama></Panoramas><OriginalImagePath>/properties/id1/First+Floor.png</OriginalImagePath><DisplayImagePath>/properties/id1/disp_First+Floor.png</DisplayImagePath></FloorPlan>'),
    (@propertyId, 9, N'/properties/id1/Bathroom Pano'),
	(@propertyId, 9, N'/properties/id1/Living room kitchen'),
	(@propertyId, 9, N'/properties/id1/Sebago lake pano')

-- Amenities
SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Flat Screen TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cable / Satellite TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sound System')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wifi')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')))

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Small gas grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'BBQ Grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Outdoor Table')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garage')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Central Air')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Tennis Courts')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Great Views')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

-- UnitType
INSERT [dbo].[UnitTypes] ([PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (@propertyId, N'unit-type code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit Type')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit type in the Villa at the beach')))
SET @unitTypeId = SCOPE_IDENTITY()

-- Unit
INSERT [dbo].[Units] ([PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (@propertyId, @unitTypeId, N'unit code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit 1')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit in the Villa at the beach')), 1, 7, 3, 2)
SET @unitId = SCOPE_IDENTITY()

-- UnitStaticContent
INSERT [dbo].[UnitStaticContent] ([UnitId], [ContentCode], [ContentValue_i18n]) VALUES
	-- Bedrooms
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / King Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),

	-- Bathrooms
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / Full bath (ensuite)'))),
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Full Bath'))),

	-- LivingAreasAndDining
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 Couches / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Loveseat / Seats 2'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'3 Chairs / Seats 3'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Dining Table / Seats 10'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sun Room Dining Table / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Breakfast Bar / Seats 5')))

	-- KitchenAndLaundry
	-- to be filled, type = 13

-- Appliances
SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel stove and oven')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel dishwasher')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Washer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Dryer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cooking utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Toaster')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Slow cooker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food processor')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Mixer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine Refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine opener')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine rack')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garbage disposal')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Water filter')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Ice Maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel, branded appliances')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Granite or marble countertops')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end flooring')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end cabinetry and finishes')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Pots and pans')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Roasting pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Baking sheets')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cupcake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food storage containers')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster pots')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster eating utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Juice maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Waffle maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Highchair')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sets include a plate, bowl, knife, fork and spoon')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

-- Unit Rates
INSERT INTO [dbo].[UnitRates] ([UnitID], [DateFrom], [DateUntil], [DailyRate]) VALUES
	(@unitId, '2012-10-29', '2012-11-15', 400.00),
	(@unitId, '2012-11-16', '2012-11-24', 650.00),
	(@unitId, '2012-11-25', '2012-12-20', 400.00),
	(@unitId, '2012-12-21', '2013-01-02', 650.00),
	(@unitId, '2013-01-03', '2013-05-15', 400.00),
	(@unitId, '2013-05-16', '2013-08-31', 650.00),
	(@unitId, '2013-09-01', '2020-11-21', 400.00)

-- MLOS - max and min values found on: http://sqlserverplanet.com/tsql/max-date-value
INSERT INTO [dbo].[UnitTypeMLOS] ([UnitTypeID], [DateFrom], [DateUntil], [MLOS]) VALUES (@unitTypeId, '2013-01-01 00:00:00.000', '2013-12-31 23:59:59.998', 7)

-- Blockings
INSERT INTO [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES
	(@unitId, '2013-01-01', '2013-05-02', 2, NULL)

-- Reservation
INSERT [dbo].[Reservations] ([PropertyID], [UnitID], [UnitTypeId], [GuestID], [ConfirmationID], [BookingDate], [BookingStatus], [DateArrival], [DateDeparture], [TotalPrice], [ReservationCultureId], [TripBalance], [TransactionFee], [LinkAuthorizationToken]) VALUES 
	(@propertyId, @unitId, @unitTypeId, @guestId, N'1', N'2012-10-10 00:00:00.000', 1, N'2012-10-10 00:00:00.000', N'2012-10-10 00:00:00.000', 1.0000, 82, 1.0000, 0, N'2AD7CFBF-8D49-4F70-9153-02B500BD35BC')
set @reservationID = SCOPE_IDENTITY()

-- Reviews
INSERT INTO [dbo].[GuestReviews] ([GuestID], [PropertyID], [ReservationID], [ReviewContent], [Satisfaction_Experience], [Satisfaction_Cleanliness], [Satisfaction_Location], [ReviewTitle], [ReviewDate]) VALUES
	( @guestId, @propertyId, @reservationID, 'Review content data', 5, 4, 5, 'Superb House', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Another review content data', 5, 5, 4, 'Excellent neighbergood', DATEADD(day, -12, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 4, 4, 5, 'Fantastic House, Super views', DATEADD(day, -6, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'And another Review content data', 3, 5, 5, 'Good trip', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 5, 4, 'Fantastic House, Super views', DATEADD(day, -2, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 4, 5, 'Fantastic House, Super views', DATEADD(day, -32, GETDATE()) )

-----------------------
-- Property 8
-----------------------

-- Property

INSERT [dbo].[Properties] (
	[OwnerID], 
	[OTP_DestinationID], 
	[PropertyCode], 
	[PropertyName_i18n], 
	[Short_Description_i18n], 
	[Long_Description_i18n], 
	[Address1], 
	[Address2], 
	[State], 
	[ZIPCode], 
	[City], 
	[CountryId], 
	[Latitude], 
	[Longitude], 
	[Altitude], 
	[PropertyLive], 
	[StandardCommission], 
	[CheckIn], 
	[CheckOut], 
	[PropertyTypeId], [CultureId], [TimeZone]) 
	VALUES 
	(@ownerId, 
	@floridaDestinationId, 
	N'property-code', 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'House on the sand')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful House with fabulous views')), 
	replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'This stunning home is located on scenic Sebago Lake in Maine, a short 3 minute walk from the lakeshore. The house sits on a small hill and has partial lake views from the kitchen, dining, family and sun rooms. The home was completely renovated in 2010 with high end finishes and beautiful details throughout. The kitchen includes stunning granite countertops, stainless steel appliances, and a wine refrigerator. The great room is stunning with vaulted, bead boarded ceilings and a panorama of large paned windows. Wood flooring throughout the entire home. With this home you will be able to relax and enjoy a little touch of luxury at the lake.')), 
	N'98 Wild Acres Road', 
	NULL, 
	N'Maine', 
	N'04071', 
	N'Raymond', 
	@countryId, 
	29.726222,
	-85.122070, 
	278, 
	1, 
	15.00, 
	'14:00', 
	'12:00', 
	@propertyTypeID, @cultureEnUsId, @usTimeZoneEastern)

SET @propertyId = SCOPE_IDENTITY()

-- Property KeyManager
INSERT [dbo].[PropertyAssignedManagers] ([PropertyId],[ManagerRoleId],[ManagerId]) VALUES (@propertyId,5,@ownerId)

-- PropertyStaticContent
INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	-- SquareFootage
	(@propertyId, 10, N'2000')

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue_i18n]) VALUES
	-- Feature
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Beautiful views of the outdoors and partial lake views'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lake access, 3 minute walk to the lake shore'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Entire home renovated in 2010 with high end finishes'))),
	(@propertyId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Large, great room with stunning vaulted, bead board ceilings'))),

	-- LocalAreaDescription
	(@propertyId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sebago Lake is one of the largest and deepest lake in Maine. The lake is surrounded by several small towns including Raymond, Casco, Naples and Windham. Several recreational activities are available including fishing, hiking, biking, boating, swimming, and sailing. The fishing at Sebago is legendary with several species of lake indigenous or stocked by the state in the lake.'))),

	-- DistanceToMainAttraction
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to lakeshore'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.5 mile to zoo'))),
	(@propertyId, 13, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'0.1 mile to sea'))),

	-- Arrival Info
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Keys: Keys are located in lockbox. Two keys are required for entry, a key for the regular lock (gold key) and a key for the dead bolt (silver key). Both keys turn left to open, right to lock.'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Lockbox instructions: Lockbox is located on back door, code is 3346. Must disable alarm upon entry!'))),
	(@propertyId, 14, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Alarm instructions: Alarm is located inside closet next to downstairs bathroom, you will have one minute to disarm so you must move quickly once door is opened. If alarm is set off, call Oh The Places at (enter out 800 number here). Alarm may be left disarmed for duration of your stay.'))), 

	-- Parking Info
	(@propertyId, 15, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 car parking is available in driveway. Please, there is no street parking, your car will be ticketed if it is parked on the street')))

INSERT [dbo].[PropertyStaticContent] ([PropertyID], [ContentCode], [ContentValue]) VALUES
	(@propertyId, 0, N'/prop8/thumb1.jpg'),
	(@propertyId, 0, N'/prop8/thumb2.jpg'),
	(@propertyId, 4, N'/prop8/listview1.jpg'),
	(@propertyId, 4, N'/prop8/listview2.jpg'),
	(@propertyId, 2, N'/prop8/fullscreen1.jpg'),
	-- Directions to Airport
	(@propertyId, 16, N'<DirectionToAirport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Name_i18n>&lt;i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;Portland International Jetport&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><DirectionLink>https://maps.google.com/maps?saddr=Portland+International+Jetport&amp;daddr=98+Wild+Acres+Road,+Raymond,+Maine+04071&amp;hl=pl&amp;ll=43.756217,-70.396957&amp;spn=0.304517,0.780029&amp;sll=44.559163,-70.054321&amp;sspn=2.403205,6.240234&amp;geocode=FZ_8mQId_SzP-yHPPvZLhaXH-ymBqy4kWZmyTDHPPvZLhaXH-w%3BFTYDnQIdeETM-yk90qZyvfSyTDH-RRb_eA5pdQ&amp;mra=ls&amp;t=m&amp;z=11></DirectionLink></DirectionToAirport>'),
    (@propertyId, 8, N'<FloorPlan xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Name_i18n>&lt;i18nString xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;&lt;i18nContent&gt;&lt;i18nPair&gt;&lt;Code&gt;en-US&lt;/Code&gt;&lt;Content&gt;First Floor&lt;/Content&gt;&lt;/i18nPair&gt;&lt;/i18nContent&gt;&lt;/i18nString&gt;</Name_i18n><Panoramas><Panorama><Id>1</Id><OffsetX>324</OffsetX><OffsetY>62</OffsetY><PanoramaPath>/properties/id1/Bathroom Pano</PanoramaPath></Panorama><Panorama><Id>2</Id><OffsetX>232</OffsetX><OffsetY>254</OffsetY><PanoramaPath>/properties/id1/Living room kitchen</PanoramaPath></Panorama><Panorama><Id>3</Id><OffsetX>480</OffsetX><OffsetY>355</OffsetY><PanoramaPath>/properties/id1/Sebago lake pano</PanoramaPath></Panorama></Panoramas><OriginalImagePath>/properties/id1/First+Floor.png</OriginalImagePath><DisplayImagePath>/properties/id1/disp_First+Floor.png</DisplayImagePath></FloorPlan>'),
    (@propertyId, 9, N'/properties/id1/Bathroom Pano'),
	(@propertyId, 9, N'/properties/id1/Living room kitchen'),
	(@propertyId, 9, N'/properties/id1/Sebago lake pano')

-- Amenities
SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Flat Screen TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cable / Satellite TV')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sound System')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wifi')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')))

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Small gas grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'BBQ Grill')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Outdoor Table')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garage')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Central Air')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Tennis Courts')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), 1)

SET @tmpId = (SELECT [AmenityID] FROM [dbo].[Amenities] WHERE [AmenityTitle_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Great Views')
INSERT INTO [dbo].[PropertyAmenities] ([PropertyID], [AmenityID], [Details_i18n], [Quantity]) VALUES (@propertyId, @tmpId, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'')), null)

-- UnitType
INSERT [dbo].[UnitTypes] ([PropertyID], [UnitTypeCode], [UnitTypeTitle_i18n], [UnitTypeDesc_i18n]) VALUES (@propertyId, N'unit-type code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit Type')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit type in the Villa at the beach')))
SET @unitTypeId = SCOPE_IDENTITY()

-- Unit
INSERT [dbo].[Units] ([PropertyID], [UnitTypeID], [UnitCode], [UnitTitle_i18n], [UnitDesc_i18n], [CleaningStatus], [MaxNumberOfGuests], [MaxNumberOfBedRooms], [MaxNumberOfBathrooms]) VALUES (@propertyId, @unitTypeId, N'unit code', replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Property Unit 1')), replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Fantastic unit in the Villa at the beach')), 1, 7, 3, 2)
SET @unitId = SCOPE_IDENTITY()

-- UnitStaticContent
INSERT [dbo].[UnitStaticContent] ([UnitId], [ContentCode], [ContentValue_i18n]) VALUES
	-- Bedrooms
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / King Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),
	(@unitId, 10, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Guest Bedroom / 1 Bunk Bed + 1 Twin Bed'))),

	-- Bathrooms
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Master Bedroom / Full bath (ensuite)'))),
	(@unitId, 11, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Full Bath'))),

	-- LivingAreasAndDining
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'2 Couches / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'1 Loveseat / Seats 2'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'3 Chairs / Seats 3'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Dining Table / Seats 10'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Sun Room Dining Table / Seats 6'))),
	(@unitId, 12, replace(@i18nXmlPatternProd, '#pair#', replace(@i18nXmlEnglishPairProd, '#phrase#', N'Breakfast Bar / Seats 5')))

	-- KitchenAndLaundry
	-- to be filled, type = 13

-- Appliances
SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel stove and oven')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel dishwasher')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Washer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Dryer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cooking utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Microwave')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Toaster')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Slow cooker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food processor')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Mixer')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine Refrigerator')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine opener')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Wine rack')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Garbage disposal')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Water filter')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Ice Maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Stainless steel, branded appliances')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Granite or marble countertops')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end flooring')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'High end cabinetry and finishes')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Pots and pans')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Roasting pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Baking sheets')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cupcake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Cake pan')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Food storage containers')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster pots')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Lobster eating utensils')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Juice maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Waffle maker')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Highchair')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

SET @tmpId = (SELECT [ApplianceId] FROM [dbo].[Appliances] WHERE [Name_i18n].value('(/i18nString/i18nContent/i18nPair[Code="en-US"]/Content)[1]', 'nvarchar(100)') = N'Sets include a plate, bowl, knife, fork and spoon')
INSERT INTO [dbo].[UnitAppliancies] ([ApplianceId], [UnitId]) VALUES (@tmpId, @unitId)

-- Unit Rates
INSERT INTO [dbo].[UnitRates] ([UnitID], [DateFrom], [DateUntil], [DailyRate]) VALUES
	(@unitId, '2012-10-29', '2012-11-15', 400.00),
	(@unitId, '2012-11-16', '2012-11-24', 650.00),
	(@unitId, '2012-11-25', '2012-12-20', 400.00),
	(@unitId, '2012-12-21', '2013-01-02', 650.00),
	(@unitId, '2013-01-03', '2013-05-15', 400.00),
	(@unitId, '2013-05-16', '2013-08-31', 650.00),
	(@unitId, '2013-09-01', '2020-11-21', 400.00)

-- MLOS - max and min values found on: http://sqlserverplanet.com/tsql/max-date-value
INSERT INTO [dbo].[UnitTypeMLOS] ([UnitTypeID], [DateFrom], [DateUntil], [MLOS]) VALUES (@unitTypeId, '2013-01-01 00:00:00.000', '2013-12-31 23:59:59.998', 7)

-- Blockings
INSERT INTO [dbo].[UnitInvBlockings] ([UnitID], [DateFrom], [DateUntil], [Type], [ReservationID]) VALUES
	(@unitId, '2013-01-01', '2013-05-02', 2, NULL)

-- Reservation
INSERT [dbo].[Reservations] ([PropertyID], [UnitID], [UnitTypeId], [GuestID], [ConfirmationID], [BookingDate], [BookingStatus], [DateArrival], [DateDeparture], [TotalPrice], [ReservationCultureId], [TripBalance], [TransactionFee], [LinkAuthorizationToken]) VALUES 
	(@propertyId, @unitId, @unitTypeId, @guestId, N'1', N'2012-10-10 00:00:00.000', 1, N'2012-10-10 00:00:00.000', N'2012-10-10 00:00:00.000', 1.0000, 82, 1.0000, 0, N'23F77D99-407D-4209-AC4A-7A858B1160AF')
set @reservationID = SCOPE_IDENTITY()

-- Reviews
INSERT INTO [dbo].[GuestReviews] ([GuestID], [PropertyID], [ReservationID], [ReviewContent], [Satisfaction_Experience], [Satisfaction_Cleanliness], [Satisfaction_Location], [ReviewTitle], [ReviewDate]) VALUES
	( @guestId, @propertyId, @reservationID, 'Review content data', 5, 4, 5, 'Superb House', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Another review content data', 5, 5, 4, 'Excellent neighbergood', DATEADD(day, -12, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 4, 4, 5, 'Fantastic House, Super views', DATEADD(day, -6, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'And another Review content data', 3, 5, 5, 'Good trip', GETDATE() ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 5, 4, 'Fantastic House, Super views', DATEADD(day, -2, GETDATE()) ),
	( @guestId, @propertyId, @reservationID, 'Third review content data', 5, 4, 5, 'Fantastic House, Super views', DATEADD(day, -32, GETDATE()) )
	

-- Change Jim Stanley mail, add bank info
UPDATE USERS
SET [email] = 'fake@test.com', [BankName] = 'SU5H', [RoutingNumber] = 'MDMxMTc2MTEw', [AccountNumber] = 'MTI3MiA3MjYyIDI2MTYgMTcyMiAxNzEyIDE3MTcgMjcyMg=='
WHERE [LastName] like 'Stanley'
