declare @i18nXmlPattern nvarchar(max)
declare @i18nXmlEnglishPair nvarchar(max)
declare @i18nXmlPolishPair nvarchar(max)

set @i18nXmlPattern = '<?xml version="1.0" encoding="utf-16"?><i18nString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><i18nContent>#pair#</i18nContent></i18nString>'
set @i18nXmlEnglishPair = '<i18nPair><Code>en-US</Code><Content>#phrase#</Content></i18nPair>'
set @i18nXmlPolishPair = '<i18nPair><Code>pl-PL</Code><Content>#phrase#</Content></i18nPair>'

DECLARE @tmpId INT, @catId INT, @subItemId INT, @itemId INT, @position INT, 
@umRoleId INT, --Upload Manager role
@admRoleId INT, --Administration role
@kmRoleId INT, --Key Manager role
@pmRoleId INT, --Property Manager role
@demRoleId INT --Data Entry Manager role

-- Roles
INSERT INTO [dbo].[Roles] ([Name], [RoleLevel]) VALUES ('Guest', 0)
INSERT INTO [dbo].[Roles] ([Name], [RoleLevel]) VALUES ('Owner', 1)
INSERT INTO [dbo].[Roles] ([Name], [RoleLevel]) VALUES ('Upload Manager', 2)
SET @umRoleId = SCOPE_IDENTITY()

INSERT INTO [dbo].[Roles] ([Name], [RoleLevel]) VALUES	('Administrator', 3)
SET @admRoleId = SCOPE_IDENTITY()

INSERT INTO [dbo].[Roles] ([Name], [RoleLevel]) VALUES	('Key Manager', 4)
SET @kmRoleId =  SCOPE_IDENTITY()

INSERT INTO [dbo].[Roles] ([Name], [RoleLevel]) VALUES	('Property Manager', 5)
SET @pmRoleId =  SCOPE_IDENTITY()

INSERT INTO [dbo].[Roles] ([Name], [RoleLevel]) VALUES	('Data Entry Manager', 6)
SET @demRoleId =  SCOPE_IDENTITY()


--Intranet SiteMap
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Main page')), NULL, N'~/Default.aspx', NULL)
SET @catId = SCOPE_IDENTITY()

-- Dictionaries
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Dictionaries')), NULL, N'', @catId)
SET @itemId = SCOPE_IDENTITY()

		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Zip Code Ranges')), NULL, N'~/Forms/ZipCodeRanges/ZipCodeRangesList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)
		
		-- odkomentowa� po zaimplementowaniu
		-- INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Unit Appliances')), NULL, N'~/Forms/UnitAppliances/UnitAppliancesList.aspx', @itemId)
		-- SET @tmpId = SCOPE_IDENTITY()
		-- INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		-- INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Unit Appliance Groups')), NULL, N'~/Forms/UnitApplianceGroups/UnitApplianceGroupsList.aspx', @itemId)
		-- SET @tmpId = SCOPE_IDENTITY()
		-- INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)
		
		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sales People')), NULL, N'~/Forms/SalesPersons/SalesPersonsList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Property Types')), NULL, N'~/Forms/PropertyTypes/PropertyTypesList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tags')), NULL, N'~/Forms/Tags/TagsList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Destinations')), NULL, N'~/Forms/Destinations/DestinationsList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Amenity Groups')), NULL, N'~/Forms/AmenityGroups/AmenityGroupsList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Amenities')), NULL, N'~/Forms/Amenities/AmenitiesList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)


--Logs
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Logs')), NULL, N'', @catId)
SET @itemId = SCOPE_IDENTITY()

		--Scheduler Log
		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Scheduler Log')), NULL, N'', @itemId)
		SET @subItemId = SCOPE_IDENTITY()

				--Task Instances
				INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Task Instances')), NULL, N'~/Forms/Logs/TaskInstancesList.aspx', @subItemId)
				SET @tmpId = SCOPE_IDENTITY()
				INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

				--Scheduled Tasks
				INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Scheduled Tasks')), NULL, N'~/Forms/Logs/ScheduledTasksList.aspx', @subItemId)
				SET @tmpId = SCOPE_IDENTITY()
				INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		--Payment Gateway Operation Log
		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Payment Gateway Operation Log')), NULL, N'~/Forms/Logs/PaymentGatewayOperationLogList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		--Event Log
		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Event Log')), NULL, N'~/Forms/Logs/EventLogList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		--CRM Log
		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Crm Operation Log')), NULL, N'~/Forms/Logs/CrmOperationLogList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

--Administration
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Administration')), NULL, N'', @catId)
SET @itemId = SCOPE_IDENTITY()

		--Emails
		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Emails')), NULL, N'~/Forms/Notifications/NotificationsList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		--Content Update Requests
		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Content Update Requests')), NULL, N'~/Forms/Properties/ContentUpdateRequestsList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

--Backend Users
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Backend Users')), NULL, N'', @catId)
SET @itemId = SCOPE_IDENTITY()

		--OTP Upload Managers
		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'OTP Upload Managers')), NULL, N'~/Forms/Users/ManagersList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		--OTP Data Entry Managers
		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'OTP Data Entry Managers')), NULL, N'~/Forms/Users/DataEntryManagersList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

		--OTP Administrators
		INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'OTP Administrators')), NULL, N'~/Forms/Users/AdministratorsList.aspx', @itemId)
		SET @tmpId = SCOPE_IDENTITY()
		INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

--Reservations
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Reservations')), NULL, N'', @catId)
SET @itemId = SCOPE_IDENTITY()


	--Reservations List
	INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Reservations List')), NULL, N'~/Forms/Reservations/ReservationsList.aspx', @itemId)
	SET @tmpId = SCOPE_IDENTITY()
	INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @tmpId)

	--Key Management
	INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Key Management')), NULL, N'~/Forms/Reservations/KeyManagement.aspx', @itemId)
	SET @tmpId = SCOPE_IDENTITY()
	INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @itemId), (@umRoleId, @itemId)


--Properties
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Properties')), NULL, N'~/Forms/Properties/PropertyList.aspx', @catId)
SET @itemId = SCOPE_IDENTITY()
INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@umRoleId, @itemId), (@admRoleId, @itemId), (@demRoleId, @itemId)

--Property Managers
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Property Managers')), NULL, N'~/Forms/Users/PropertyManagersList.aspx', @catId)
SET @itemId = SCOPE_IDENTITY()
INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @itemId)


--Key Managers
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Key Managers')), NULL, N'~/Forms/Users/KeyManagersList.aspx', @catId)
SET @itemId = SCOPE_IDENTITY()
INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@admRoleId, @itemId)

--Owners
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Owners')), NULL, N'~/Forms/Users/OwnersList.aspx', @catId)
SET @itemId = SCOPE_IDENTITY()
INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES (@umRoleId, @itemId), (@admRoleId, @itemId), (@demRoleId, @itemId)

--Guests
INSERT [dbo].[IntranetSiteMapActions] ([DisplayName_i18n], [Description_i18n], [Url], [ParentActionId]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Guests')), NULL, N'~/Forms/Users/GuestsList.aspx', @catId)
SET @itemId = SCOPE_IDENTITY()
INSERT INTO [dbo].[RoleIntranetSiteMapActions] ([RoleId], [IntranetSiteMapActionId]) VALUES  (@admRoleId, @itemId)

-- Countries - source: http://userpage.chemie.fu-berlin.de/diverse/doc/ISO_3166.html
INSERT [dbo].[DictionaryCountries] ([CountryName_i18n], [CountryCode2Letters], [CountryCode3Letters]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Aaland Islands')), N'ax', N'ala'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Afghanistan')), N'af', N'afg'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Albania')), N'al', N'alb'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Algeria')), N'dz', N'dza'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'American Samoa')), N'as', N'asm'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Andorra')), N'ad', N'and'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Angola')), N'ao', N'ago'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Anguilla')), N'ai', N'aia'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Antarctica')), N'aq', N'ata'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Antigua And Barbuda')), N'ag', N'atg'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Argentina')), N'ar', N'arg'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Armenia')), N'am', N'arm'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Aruba')), N'aw', N'abw'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Australia')), N'au', N'aus'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Austria')), N'at', N'aut'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Azerbaijan')), N'az', N'aze'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bahamas')), N'bs', N'bhs'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bahrain')), N'bh', N'bhr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bangladesh')), N'bd', N'bgd'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Barbados')), N'bb', N'brb'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Belarus')), N'by', N'blr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Belgium')), N'be', N'bel'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Belize')), N'bz', N'blz'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Benin')), N'bj', N'ben'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bermuda')), N'bm', N'bmu'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bhutan')), N'bt', N'btn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bolivia')), N'bo', N'bol'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bosnia And Herzegowina')), N'ba', N'bih'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Botswana')), N'bw', N'bwa'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bouvet Island')), N'bv', N'bvt'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Brazil')), N'br', N'bra'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'British Indian Ocean Territory')), N'io', N'iot'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Brunei Darussalam')), N'bn', N'brn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bulgaria')), N'bg', N'bgr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Burkina Faso')), N'bf', N'bfa'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Burundi')), N'bi', N'bdi'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cambodia')), N'kh', N'khm'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cameroon')), N'cm', N'cmr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Canada')), N'ca', N'can'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cape Verde')), N'cv', N'cpv'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cayman Islands')), N'ky', N'cym'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Central African Republic')), N'cf', N'caf'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chad')), N'td', N'tcd'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chile')), N'cl', N'chl'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'China')), N'cn', N'chn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Christmas Island')), N'cx', N'cxr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cocos (Keeling) Islands')), N'cc', N'cck'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Colombia')), N'co', N'col'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Comoros')), N'km', N'com'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Congo, Democratic Republic Of')), N'cd', N'cod'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Congo, Republic Of')), N'cg', N'cog'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cook Islands')), N'ck', N'cok'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Costa Rica')), N'cr', N'cri'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cote D''Ivoire')), N'ci', N'civ'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Croatia (Local Name: Hrvatska)')), N'hr', N'hrv'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cuba')), N'cu', N'cub'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cyprus')), N'cy', N'cyp'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Czech Republic')), N'cz', N'cze'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Denmark')), N'dk', N'dnk'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Djibouti')), N'dj', N'dji'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Dominica')), N'dm', N'dma'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Dominican Republic')), N'do', N'dom'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Ecuador')), N'ec', N'ecu'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Egypt')), N'eg', N'egy'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'El Salvador')), N'sv', N'slv'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Equatorial Guinea')), N'gq', N'gnq'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Eritrea')), N'er', N'eri'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Estonia')), N'ee', N'est'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Ethiopia')), N'et', N'eth'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Falkland Islands (Malvinas)')), N'fk', N'flk'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Faroe Islands')), N'fo', N'fro'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Fiji')), N'fj', N'fji'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Finland')), N'fi', N'fin'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'France')), N'fr', N'fra'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'French Guiana')), N'gf', N'guf'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'French Polynesia')), N'pf', N'pyf'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'French Southern Territories')), N'tf', N'atf'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Gabon')), N'ga', N'gab'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Gambia')), N'gm', N'gmb'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Georgia')), N'ge', N'geo'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Germany')), N'de', N'deu'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Ghana')), N'gh', N'gha'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Gibraltar')), N'gi', N'gib'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Greece')), N'gr', N'grc'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Greenland')), N'gl', N'grl'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Grenada')), N'gd', N'grd'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Guadeloupe')), N'gp', N'glp'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Guam')), N'gu', N'gum'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Guatemala')), N'gt', N'gtm'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Guinea')), N'gn', N'gin'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Guinea-Bissau')), N'gw', N'gnb'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Guyana')), N'gy', N'guy'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Haiti')), N'ht', N'hti'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Heard And Mc Donald Islands')), N'hm', N'hmd'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Honduras')), N'hn', N'hnd'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hong Kong')), N'hk', N'hkg'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hungary')), N'hu', N'hun'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Iceland')), N'is', N'isl'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'India')), N'in', N'ind'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Indonesia')), N'id', N'idn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Iran (Islamic Republic Of)')), N'ir', N'irn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Iraq')), N'iq', N'irq'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Ireland')), N'ie', N'irl'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Israel')), N'il', N'isr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Italy')), N'it', N'ita'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Jamaica')), N'jm', N'jam'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Japan')), N'jp', N'jpn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Jordan')), N'jo', N'jor'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kazakhstan')), N'kz', N'kaz'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kenya')), N'ke', N'ken'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kiribati')), N'ki', N'kir'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Korea, Democratic People''s Republic Of')), N'kp', N'prk'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Korea, Republic Of')), N'kr', N'kor'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kuwait')), N'kw', N'kwt'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kyrgyzstan')), N'kg', N'kgz'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Lao People''s Democratic Republic')), N'la', N'lao'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Latvia')), N'lv', N'lva'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Lebanon')), N'lb', N'lbn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Lesotho')), N'ls', N'lso'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Liberia')), N'lr', N'lbr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Libyan Arab Jamahiriya')), N'ly', N'lby'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Liechtenstein')), N'li', N'lie'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Lithuania')), N'lt', N'ltu'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Luxembourg')), N'lu', N'lux'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Macau')), N'mo', N'mac'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Macedonia, The Former Yugoslav Republic Of')), N'mk', N'mkd'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Madagascar')), N'mg', N'mdg'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Malawi')), N'mw', N'mwi'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Malaysia')), N'my', N'mys'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Maldives')), N'mv', N'mdv'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mali')), N'ml', N'mli'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Malta')), N'mt', N'mlt'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Marshall Islands')), N'mh', N'mhl'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Martinique')), N'mq', N'mtq'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mauritania')), N'mr', N'mrt'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mauritius')), N'mu', N'mus'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mayotte')), N'yt', N'myt'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mexico')), N'mx', N'mex'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Micronesia, Federated States Of')), N'fm', N'fsm'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Moldova, Republic Of')), N'md', N'mda'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Monaco')), N'mc', N'mco'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mongolia')), N'mn', N'mng'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Montserrat')), N'ms', N'msr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Morocco')), N'ma', N'mar'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mozambique')), N'mz', N'moz'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Myanmar')), N'mm', N'mmr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Namibia')), N'na', N'nam'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Nauru')), N'nr', N'nru'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Nepal')), N'np', N'npl'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Netherlands')), N'nl', N'nld'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Netherlands Antilles')), N'an', N'ant'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'New Caledonia')), N'nc', N'ncl'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'New Zealand')), N'nz', N'nzl'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Nicaragua')), N'ni', N'nic'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Niger')), N'ne', N'ner'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Nigeria')), N'ng', N'nga'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Niue')), N'nu', N'niu'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Norfolk Island')), N'nf', N'nfk'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Northern Mariana Islands')), N'mp', N'mnp'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Norway')), N'no', N'nor'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Oman')), N'om', N'omn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Pakistan')), N'pk', N'pak'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Palau')), N'pw', N'plw'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Palestinian Territory, Occupied')), N'ps', N'pse'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Panama')), N'pa', N'pan'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Papua New Guinea')), N'pg', N'png'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Paraguay')), N'py', N'pry'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Peru')), N'pe', N'per'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Philippines')), N'ph', N'phl'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Pitcairn')), N'pn', N'pcn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Poland')), N'pl', N'pol'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Portugal')), N'pt', N'prt'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Puerto Rico')), N'pr', N'pri'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Qatar')), N'qa', N'qat'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Reunion')), N're', N'reu'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Romania')), N'ro', N'rou'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Russian Federation')), N'ru', N'rus'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Rwanda')), N'rw', N'rwa'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Saint Helena')), N'sh', N'shn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Saint Kitts And Nevis')), N'kn', N'kna'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Saint Lucia')), N'lc', N'lca'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Saint Pierre And Miquelon')), N'pm', N'spm'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Saint Vincent And The Grenadines')), N'vc', N'vct'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Samoa')), N'ws', N'wsm'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'San Marino')), N'sm', N'smr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sao Tome And Principe')), N'st', N'stp'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Saudi Arabia')), N'sa', N'sau'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Senegal')), N'sn', N'sen'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbia And Montenegro')), N'cs', N'scg'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Seychelles')), N'sc', N'syc'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sierra Leone')), N'sl', N'sle'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Singapore')), N'sg', N'sgp'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Slovakia')), N'sk', N'svk'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Slovenia')), N'si', N'svn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Solomon Islands')), N'sb', N'slb'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Somalia')), N'so', N'som'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'South Africa')), N'za', N'zaf'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'South Georgia And The South Sandwich Islands')), N'gs', N'sgs'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spain')), N'es', N'esp'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sri Lanka')), N'lk', N'lka'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sudan')), N'sd', N'sdn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Suriname')), N'sr', N'sur'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Svalbard And Jan Mayen Islands')), N'sj', N'sjm'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Swaziland')), N'sz', N'swz'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sweden')), N'se', N'swe'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Switzerland')), N'ch', N'che'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Syrian Arab Republic')), N'sy', N'syr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Taiwan')), N'tw', N'twn'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tajikistan')), N'tj', N'tjk'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tanzania, United Republic Of')), N'tz', N'tza'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Thailand')), N'th', N'tha'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Timor-Leste')), N'tl', N'tls'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Togo')), N'tg', N'tgo'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tokelau')), N'tk', N'tkl'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tonga')), N'to', N'ton'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Trinidad And Tobago')), N'tt', N'tto'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tunisia')), N'tn', N'tun'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Turkey')), N'tr', N'tur'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Turkmenistan')), N'tm', N'tkm'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Turks And Caicos Islands')), N'tc', N'tca'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tuvalu')), N'tv', N'tuv'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Uganda')), N'ug', N'uga'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Ukraine')), N'ua', N'ukr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'United Arab Emirates')), N'ae', N'are'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'United Kingdom')), N'gb', N'gbr'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'United States')), N'us', N'usa'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'United States Minor Outlying Islands')), N'um', N'umi'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Uruguay')), N'uy', N'ury'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Uzbekistan')), N'uz', N'uzb'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Vanuatu')), N'vu', N'vut'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Vatican City State (Holy See)')), N'va', N'vat'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Venezuela')), N've', N'ven'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Viet Nam')), N'vn', N'vnm'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Virgin Islands (British)')), N'vg', N'vgb'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Virgin Islands (U.S.)')), N'vi', N'vir'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Wallis And Futuna Islands')), N'wf', N'wlf'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Western Sahara')), N'eh', N'esh'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Yemen')), N'ye', N'yem'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Zambia')), N'zm', N'zmb'),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Zimbabwe')), N'zw', N'zwe')
	
-- Cultures
INSERT INTO [dbo].[DictionaryCultures] ([CultureCode],[CultureName_i18n]) VALUES
	(N'af-ZA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Afrikaans (South Africa)')))),
	(N'am-ET', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Amharic (Ethiopia)')))),
	(N'ar-AE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (U.A.E.)')))),
	(N'ar-BH', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Bahrain)')))),
	(N'ar-DZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Algeria)')))),
	(N'ar-EG', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Egypt)')))),
	(N'ar-IQ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Iraq)')))),
	(N'ar-JO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Jordan)')))),
	(N'ar-KW', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Kuwait)')))),
	(N'ar-LB', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Lebanon)')))),
	(N'ar-LY', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Libya)')))),
	(N'ar-MA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Morocco)')))),
	(N'arn-CL', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mapudungun (Chile)')))),
	(N'ar-OM', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Oman)')))),
	(N'ar-QA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Qatar)')))),
	(N'ar-SA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Saudi Arabia)')))),
	(N'ar-SY', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Syria)')))),
	(N'ar-TN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Tunisia)')))),
	(N'ar-YE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arabic (Yemen)')))),
	(N'as-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Assamese (India)')))),
	(N'az-Cyrl', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Azeri (Cyrillic)')))),
	(N'az-Cyrl-AZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Azeri (Cyrillic, Azerbaijan)')))),
	(N'az-Latn', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Azeri (Latin)')))),
	(N'az-Latn-AZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Azeri (Latin, Azerbaijan)')))),
	(N'ba-RU', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bashkir (Russia)')))),
	(N'be-BY', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Belarusian (Belarus)')))),
	(N'bg-BG', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bulgarian (Bulgaria)')))),
	(N'bn-BD', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bengali (Bangladesh)')))),
	(N'bn-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bengali (India)')))),
	(N'bo-CN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tibetan (PRC)')))),
	(N'br-FR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Breton (France)')))),
	(N'bs-Cyrl', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bosnian (Cyrillic)')))),
	(N'bs-Cyrl-BA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bosnian (Cyrillic, Bosnia and Herzegovina)')))),
	(N'bs-Latn', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bosnian (Latin)')))),
	(N'bs-Latn-BA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Bosnian (Latin, Bosnia and Herzegovina)')))),
	(N'ca-ES', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Catalan (Catalan)')))),
	(N'co-FR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Corsican (France)')))),
	(N'cs-CZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Czech (Czech Republic)')))),
	(N'cy-GB', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Welsh (United Kingdom)')))),
	(N'da-DK', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Danish (Denmark)')))),
	(N'de-AT', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'German (Austria)')))),
	(N'de-CH', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'German (Switzerland)')))),
	(N'de-DE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'German (Germany)')))),
	(N'de-LI', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'German (Liechtenstein)')))),
	(N'de-LU', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'German (Luxembourg)')))),
	(N'dsb-DE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Lower Sorbian (Germany)')))),
	(N'dv-MV', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Divehi (Maldives)')))),
	(N'el-GR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Greek (Greece)')))),
	(N'en-029', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Caribbean)')))),
	(N'en-AU', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Australia)')))),
	(N'en-BZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Belize)')))),
	(N'en-CA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Canada)')))),
	(N'en-GB', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (United Kingdom)')))),
	(N'en-IE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Ireland)')))),
	(N'en-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (India)')))),
	(N'en-JM', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Jamaica)')))),
	(N'en-MY', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Malaysia)')))),
	(N'en-NZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (New Zealand)')))),
	(N'en-PH', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Republic of the Philippines)')))),
	(N'en-SG', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Singapore)')))),
	(N'en-TT', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Trinidad and Tobago)')))),
	(N'en-US', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (United States)')))),
	(N'en-ZA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (South Africa)')))),
	(N'en-ZW', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'English (Zimbabwe)')))),
	(N'es-AR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Argentina)')))),
	(N'es-BO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Bolivia)')))),
	(N'es-CL', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Chile)')))),
	(N'es-CO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Colombia)')))),
	(N'es-CR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Costa Rica)')))),
	(N'es-DO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Dominican Republic)')))),
	(N'es-EC', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Ecuador)')))),
	(N'es-ES', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Spain)')))),
	(N'es-GT', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Guatemala)')))),
	(N'es-HN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Honduras)')))),
	(N'es-MX', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Mexico)')))),
	(N'es-NI', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Nicaragua)')))),
	(N'es-PA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Panama)')))),
	(N'es-PE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Peru)')))),
	(N'es-PR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Puerto Rico)')))),
	(N'es-PY', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Paraguay)')))),
	(N'es-SV', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (El Salvador)')))),
	(N'es-US', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (United States)')))),
	(N'es-UY', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Uruguay)')))),
	(N'es-VE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spanish (Bolivarian Republic of Venezuela)')))),
	(N'et-EE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Estonian (Estonia)')))),
	(N'eu-ES', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Basque (Basque)')))),
	(N'fa-IR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Persian')))),
	(N'fi-FI', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Finnish (Finland)')))),
	(N'fil-PH', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Filipino (Philippines)')))),
	(N'fo-FO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Faroese (Faroe Islands)')))),
	(N'fr-BE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'French (Belgium)')))),
	(N'fr-CA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'French (Canada)')))),
	(N'fr-CH', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'French (Switzerland)')))),
	(N'fr-FR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'French (France)')))),
	(N'fr-LU', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'French (Luxembourg)')))),
	(N'fr-MC', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'French (Monaco)')))),
	(N'fy-NL', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Frisian (Netherlands)')))),
	(N'ga-IE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Irish (Ireland)')))),
	(N'gd-GB', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Scottish Gaelic (United Kingdom)')))),
	(N'gl-ES', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Galician (Galician)')))),
	(N'gsw-FR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Alsatian (France)')))),
	(N'gu-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Gujarati (India)')))),
	(N'ha-Latn', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hausa (Latin)')))),
	(N'ha-Latn-NG', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hausa (Latin, Nigeria)')))),
	(N'he-IL', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hebrew (Israel)')))),
	(N'hi-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hindi (India)')))),
	(N'hr-BA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Croatian (Latin, Bosnia and Herzegovina)')))),
	(N'hr-HR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Croatian (Croatia)')))),
	(N'hsb-DE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Upper Sorbian (Germany)')))),
	(N'hu-HU', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hungarian (Hungary)')))),
	(N'hy-AM', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Armenian (Armenia)')))),
	(N'id-ID', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Indonesian (Indonesia)')))),
	(N'ig-NG', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Igbo (Nigeria)')))),
	(N'ii-CN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Yi (PRC)')))),
	(N'is-IS', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Icelandic (Iceland)')))),
	(N'it-CH', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Italian (Switzerland)')))),
	(N'it-IT', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Italian (Italy)')))),
	(N'iu-Cans', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Inuktitut (Syllabics)')))),
	(N'iu-Cans-CA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Inuktitut (Syllabics, Canada)')))),
	(N'iu-Latn', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Inuktitut (Latin)')))),
	(N'iu-Latn-CA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Inuktitut (Latin, Canada)')))),
	(N'ja-JP', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Japanese (Japan)')))),
	(N'ka-GE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Georgian (Georgia)')))),
	(N'kk-KZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kazakh (Kazakhstan)')))),
	(N'kl-GL', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Greenlandic (Greenland)')))),
	(N'km-KH', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Khmer (Cambodia)')))),
	(N'kn-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kannada (India)')))),
	(N'kok-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Konkani (India)')))),
	(N'ko-KR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Korean (Korea)')))),
	(N'ky-KG', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kyrgyz (Kyrgyzstan)')))),
	(N'lb-LU', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Luxembourgish (Luxembourg)')))),
	(N'lo-LA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Lao (Lao P.D.R.)')))),
	(N'lt-LT', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Lithuanian (Lithuania)')))),
	(N'lv-LV', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Latvian (Latvia)')))),
	(N'mi-NZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Maori (New Zealand)')))),
	(N'mk-MK', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Macedonian (Former Yugoslav Republic of Macedonia)')))),
	(N'ml-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Malayalam (India)')))),
	(N'mn-Cyrl', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mongolian (Cyrillic)')))),
	(N'mn-MN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mongolian (Cyrillic, Mongolia)')))),
	(N'mn-Mong', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mongolian (Traditional Mongolian)')))),
	(N'mn-Mong-CN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mongolian (Traditional Mongolian, PRC)')))),
	(N'moh-CA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mohawk (Mohawk)')))),
	(N'mr-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Marathi (India)')))),
	(N'ms-BN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Malay (Brunei Darussalam)')))),
	(N'ms-MY', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Malay (Malaysia)')))),
	(N'mt-MT', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Maltese (Malta)')))),
	(N'nb-NO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Norwegian, Bokmal (Norway)')))),
	(N'ne-NP', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Nepali (Nepal)')))),
	(N'nl-BE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Dutch (Belgium)')))),
	(N'nl-NL', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Dutch (Netherlands)')))),
	(N'nn-NO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Norwegian, Nynorsk (Norway)')))),
	(N'nso-ZA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sesotho sa Leboa (South Africa)')))),
	(N'oc-FR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Occitan (France)')))),
	(N'or-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Oriya (India)')))),
	(N'pa-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Punjabi (India)')))),
	(N'pl-PL', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Polish (Poland)')))),
	(N'prs-AF', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Dari (Afghanistan)')))),
	(N'ps-AF', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Pashto (Afghanistan)')))),
	(N'pt-BR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Portuguese (Brazil)')))),
	(N'pt-PT', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Portuguese (Portugal)')))),
	(N'qut-GT', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'K iche (Guatemala)')))),
	(N'quz-BO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Quechua (Bolivia)')))),
	(N'quz-EC', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Quechua (Ecuador)')))),
	(N'quz-PE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Quechua (Peru)')))),
	(N'rm-CH', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Romansh (Switzerland)')))),
	(N'ro-RO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Romanian (Romania)')))),
	(N'ru-RU', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Russian (Russia)')))),
	(N'rw-RW', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kinyarwanda (Rwanda)')))),
	(N'sah-RU', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sakha (Russia)')))),
	(N'sa-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sanskrit (India)')))),
	(N'se-FI', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sami, Northern (Finland)')))),
	(N'se-NO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sami, Northern (Norway)')))),
	(N'se-SE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sami, Northern (Sweden)')))),
	(N'si-LK', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sinhala (Sri Lanka)')))),
	(N'sk-SK', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Slovak (Slovakia)')))),
	(N'sl-SI', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Slovenian (Slovenia)')))),
	(N'sma-NO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sami, Southern (Norway)')))),
	(N'sma-SE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sami, Southern (Sweden)')))),
	(N'smj-NO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sami, Lule (Norway)')))),
	(N'smj-SE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sami, Lule (Sweden)')))),
	(N'smn-FI', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sami, Inari (Finland)')))),
	(N'sms-FI', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sami, Skolt (Finland)')))),
	(N'sq-AL', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Albanian (Albania)')))),
	(N'sr-Cyrl', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbian (Cyrillic)')))),
	(N'sr-Cyrl-BA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbian (Cyrillic, Bosnia and Herzegovina)')))),
	(N'sr-Cyrl-CS', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbian (Cyrillic, Serbia and Montenegro (Former))')))),
	(N'sr-Cyrl-ME', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbian (Cyrillic, Montenegro)')))),
	(N'sr-Cyrl-RS', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbian (Cyrillic, Serbia)')))),
	(N'sr-Latn', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbian (Latin)')))),
	(N'sr-Latn-BA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbian (Latin, Bosnia and Herzegovina)')))),
	(N'sr-Latn-CS', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbian (Latin, Serbia and Montenegro (Former))')))),
	(N'sr-Latn-ME', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbian (Latin, Montenegro)')))),
	(N'sr-Latn-RS', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Serbian (Latin, Serbia)')))),
	(N'sv-FI', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Swedish (Finland)')))),
	(N'sv-SE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Swedish (Sweden)')))),
	(N'sw-KE', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kiswahili (Kenya)')))),
	(N'syr-SY', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Syriac (Syria)')))),
	(N'ta-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tamil (India)')))),
	(N'te-IN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Telugu (India)')))),
	(N'tg-Cyrl', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tajik (Cyrillic)')))),
	(N'tg-Cyrl-TJ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tajik (Cyrillic, Tajikistan)')))),
	(N'th-TH', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Thai (Thailand)')))),
	(N'tk-TM', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Turkmen (Turkmenistan)')))),
	(N'tn-ZA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Setswana (South Africa)')))),
	(N'tr-TR', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Turkish (Turkey)')))),
	(N'tt-RU', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tatar (Russia)')))),
	(N'tzm-Latn', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tamazight (Latin)')))),
	(N'tzm-Latn-DZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tamazight (Latin, Algeria)')))),
	(N'ug-CN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Uyghur (PRC)')))),
	(N'uk-UA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Ukrainian (Ukraine)')))),
	(N'ur-PK', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Urdu (Islamic Republic of Pakistan)')))),
	(N'uz-Cyrl', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Uzbek (Cyrillic)')))),
	(N'uz-Cyrl-UZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Uzbek (Cyrillic, Uzbekistan)')))),
	(N'uz-Latn', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Uzbek (Latin)')))),
	(N'uz-Latn-UZ', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Uzbek (Latin, Uzbekistan)')))),
	(N'vi-VN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Vietnamese (Vietnam)')))),
	(N'wo-SN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Wolof (Senegal)')))),
	(N'xh-ZA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'isiXhosa (South Africa)')))),
	(N'yo-NG', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Yoruba (Nigeria)')))),
	(N'zh-CHS', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chinese (Simplified) Legacy')))),
	(N'zh-CHT', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chinese (Traditional) Legacy')))),
	(N'zh-CN', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chinese (Simplified, PRC)')))),
	(N'zh-Hans', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chinese (Simplified)')))),
	(N'zh-Hant', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chinese (Traditional)')))),
	(N'zh-HK', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chinese (Traditional, Hong Kong S.A.R.)')))),
	(N'zh-MO', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chinese (Traditional, Macao S.A.R.)')))),
	(N'zh-SG', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chinese (Simplified, Singapore)')))),
	(N'zh-TW', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Chinese (Traditional, Taiwan)')))),
	(N'zu-ZA', (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'isiZulu (South Africa)'))))

-- Amenity groups - based on JIRA dictionary
SET @position = (SELECT ISNULL(MAX([Position]), 0) + 1 FROM [dbo].[AmenityGroup])
INSERT [dbo].[AmenityGroup] ([Name_i18n], [AmenityIcon], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Other')), 0, @position)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Amenities] ([AmenityCode], [AmenityTitle_i18n], [Description_i18n], [AmenityGroupId], [Countable]) VALUES
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Gourmet Kitchen')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'A Gourmet Kitchen includes all of the following:  high end flooring, high end appliances (Wolf, Sub Zero or similar), and granite or similar countertops.')), @tmpId, 0),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Great Views')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'A great view would be of the ocean for a beach house, of the ski slope for a ski house, etc.  We show you photographs of the view from eachproperty with a Great View.')), @tmpId, 0)

SET @position = (SELECT ISNULL(MAX([Position]), 0) + 1 FROM [dbo].[AmenityGroup])
INSERT [dbo].[AmenityGroup] ([Name_i18n], [AmenityIcon], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Pool / Spa')), 4, @position)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Amenities] ([AmenityCode], [AmenityTitle_i18n], [Description_i18n], [AmenityGroupId], [Countable]) VALUES
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Pool - Outdoor')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Pool - Indoor')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hot tub - Outdoor')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hot tub - Indoor')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1)

SET @position = (SELECT ISNULL(MAX([Position]), 0) + 1 FROM [dbo].[AmenityGroup])
INSERT [dbo].[AmenityGroup] ([Name_i18n], [AmenityIcon], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Sports')), 10, @position)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Amenities] ([AmenityCode], [AmenityTitle_i18n], [Description_i18n], [AmenityGroupId], [Countable]) VALUES
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Golf Course Access')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 0),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tennis Courts')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Fitness Equipment')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 0),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kayaks / Canoe')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Boat Dock')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 0)

SET @position = (SELECT ISNULL(MAX([Position]), 0) + 1 FROM [dbo].[AmenityGroup])
INSERT [dbo].[AmenityGroup] ([Name_i18n], [AmenityIcon], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Entertainment')), 8, @position)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Amenities] ([AmenityCode], [AmenityTitle_i18n], [Description_i18n], [AmenityGroupId], [Countable]) VALUES
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cable / Satellite TV')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Flat Screen TV')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sound System')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Blue Ray / DVD Player')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1)

SET @position = (SELECT ISNULL(MAX([Position]), 0) + 1 FROM [dbo].[AmenityGroup])
INSERT [dbo].[AmenityGroup] ([Name_i18n], [AmenityIcon], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Outdoor Entertainment')), 3, @position)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Amenities] ([AmenityCode], [AmenityTitle_i18n], [Description_i18n], [AmenityGroupId], [Countable]) VALUES
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'BBQ Grill')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),	
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Outdoor Table')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Seating Area')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Small gas grill')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1)

SET @position = (SELECT ISNULL(MAX([Position]), 0) + 1 FROM [dbo].[AmenityGroup])
INSERT [dbo].[AmenityGroup] ([Name_i18n], [AmenityIcon], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Internet')), 1, @position)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Amenities] ([AmenityCode], [AmenityTitle_i18n], [Description_i18n], [AmenityGroupId], [Countable]) VALUES
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Wifi')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 0),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Wired Internet')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 0)

SET @position = (SELECT ISNULL(MAX([Position]), 0) + 1 FROM [dbo].[AmenityGroup])
INSERT [dbo].[AmenityGroup] ([Name_i18n], [AmenityIcon], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Comfort')), 7, @position)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Amenities] ([AmenityCode], [AmenityTitle_i18n], [Description_i18n], [AmenityGroupId], [Countable]) VALUES
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Central Air')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 0),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Forced Air Heat')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 0),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Elevator')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 0),
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Laundry Facilities - Private')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 0)

SET @position = (SELECT ISNULL(MAX([Position]), 0) + 1 FROM [dbo].[AmenityGroup])
INSERT [dbo].[AmenityGroup] ([Name_i18n], [AmenityIcon], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Parking Facilities')), 9, @position)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Amenities] ([AmenityCode], [AmenityTitle_i18n], [Description_i18n], [AmenityGroupId], [Countable]) VALUES
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Garage')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 1)


SET @position = (SELECT ISNULL(MAX([Position]), 0) + 1 FROM [dbo].[AmenityGroup])
INSERT [dbo].[AmenityGroup] ([Name_i18n], [AmenityIcon], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', 'Allowances')), 6, @position)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Amenities] ([AmenityCode], [AmenityTitle_i18n], [Description_i18n], [AmenityGroupId], [Countable]) VALUES
	(N' ', replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Pets Allowed')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'')), @tmpId, 0)

-- Appliance groups and appliances - from spreadsheet (of Type = 1 for kitchen)
INSERT INTO [dbo].[ApplianceGroup] ([Name_i18n]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Deluxe Kitchen')))
SET @tmpId = SCOPE_IDENTITY()
INSERT INTO [dbo].[Appliances] ([Name_i18n], [Type], [ApplianceGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Stainless steel, branded appliances')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Granite or marble countertops')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'High end flooring')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'High end cabinetry and finishes')), 1, @tmpId)

INSERT INTO [dbo].[ApplianceGroup] ([Name_i18n]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cooking and Baking')))
SET @tmpId = SCOPE_IDENTITY()
INSERT INTO [dbo].[Appliances] ([Name_i18n], [Type], [ApplianceGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Pots and pans')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Roasting pan')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Baking sheets')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cupcake pan')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cake pan')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Food storage containers')), 1, @tmpId)

INSERT INTO [dbo].[ApplianceGroup] ([Name_i18n]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Major Appliances')))
SET @tmpId = SCOPE_IDENTITY()
INSERT INTO [dbo].[Appliances] ([Name_i18n], [Type], [ApplianceGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Stainless steel stove and oven')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Stainless steel refrigerator')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Stainless steel dishwasher')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Stainless steel microwave')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Washer')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Dryer')), 1, @tmpId)

INSERT INTO [dbo].[ApplianceGroup] ([Name_i18n]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Minor Appliances')))
SET @tmpId = SCOPE_IDENTITY()
INSERT INTO [dbo].[Appliances] ([Name_i18n], [Type], [ApplianceGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Cooking utensils')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Microwave')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Toaster')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Slow cooker')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Food processor')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mixer')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Wine Refrigerator')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Wine opener')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Wine rack')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Garbage disposal')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Water filter')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Ice Maker')), 1, @tmpId)

INSERT INTO [dbo].[ApplianceGroup] ([Name_i18n]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Special Items')))
SET @tmpId = SCOPE_IDENTITY()
INSERT INTO [dbo].[Appliances] ([Name_i18n], [Type], [ApplianceGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Lobster pots')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Lobster eating utensils')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Juice maker')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Waffle maker')), 1, @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Highchair')), 1, @tmpId)

INSERT INTO [dbo].[ApplianceGroup] ([Name_i18n]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Plating Sets')))
SET @tmpId = SCOPE_IDENTITY()
INSERT INTO [dbo].[Appliances] ([Name_i18n], [Type], [ApplianceGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sets include a plate, bowl, knife, fork and spoon')), 1, @tmpId)

--Property Types - based on JIRA dictionary
INSERT [dbo].[PropertyType] ([Name_i18n]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'House'))),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Apartment'))),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Condo'))),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Townhome')))

--Tags - based on JIRA dictionary
INSERT INTO [dbo].[TagGroup] ([Name_i18n], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Location')), 1)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Tags] ([Tag_i18n], [TagGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Beach')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'City')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Country')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mountain or Ski Slope')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Lake or River')), @tmpId)

INSERT INTO [dbo].[TagGroup] ([Name_i18n], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Distance from the Beach')), 2)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Tags] ([Tag_i18n], [TagGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'On the Beach')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Walk (&lt; 1 mi.) to the Beach')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Drive to the Beach')), @tmpId)

INSERT INTO [dbo].[TagGroup] ([Name_i18n], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Distance from the Ski Slope')), 3)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Tags] ([Tag_i18n], [TagGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'On the Ski Slope')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Walk (&lt; 1 mi.) to the Ski Slope')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Shuttle to the Ski Slope')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Drive to the Ski Slope')), @tmpId)

INSERT INTO [dbo].[TagGroup] ([Name_i18n], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sports')), 4)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Tags] ([Tag_i18n], [TagGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Biking - Mountain')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Biking - Road')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Boating - Charter')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Fishing')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Golfing')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hiking')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Scuba Diving / Snorkeling')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Ski - Cross Country')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Ski - Downhill')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Snowboard')), @tmpId)

INSERT INTO [dbo].[TagGroup] ([Name_i18n], [Position]) VALUES (replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Entertainment')), 5)
SET @tmpId = SCOPE_IDENTITY()
INSERT [dbo].[Tags] ([Tag_i18n], [TagGroupId]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Amusement Park')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Nightlife')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Performance Arts / Museums')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Great Restaurants')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Spa Retreat')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Sporting Events')), @tmpId),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Wine Tasting')), @tmpId)

--Destinations
INSERT INTO [dbo].[Destinations] ([Destination_i18n], [Description_i18n], [Active]) VALUES
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'National')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of National')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Alabama')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Alabama')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Alaska')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Alaska')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arizona')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Arizona')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Arkansas')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Arkansas')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'California')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of California')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Colorado')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Colorado')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Connecticut')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Connecticut')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Delaware')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Delaware')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Florida')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Florida')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Georgia')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Georgia')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Hawaii')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Hawaii')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Idaho')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Idaho')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Illinois')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Illinois')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Indiana')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Indiana')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Iowa')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Iowa')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kansas')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Kansas')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Kentucky')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Kentucky')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Louisiana')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Louisiana')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Maine')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Maine')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Maryland')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Maryland')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Massachusetts')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Massachusetts')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Michigan')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Michigan')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Minnesota')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Minnesota')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Mississippi')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Mississippi')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Missouri')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Missouri')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Montana')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Montana')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Nebraska')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Nebraska')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Nevada')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Nevada')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'New Hampshire')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of New Hampshire')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'New Jersey')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of New Jersey')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'New Mexico')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of New Mexico')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'New York')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of New York')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'North Carolina')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of North Carolina')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'North Dakota')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of North Dakota')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Ohio')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Ohio')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Oklahoma')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Oklahoma')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Oregon')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Oregon')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Pennsylvania')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Pennsylvania')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Rhode Island')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Rhode Island')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'South Carolina')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of South Carolina')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'South Dakota')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of South Dakota')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tennessee')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Tennessee')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Texas')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Texas')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Utah')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Utah')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Vermont')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Vermont')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Virginia')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Virginia')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Washington')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Washington')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'West Virginia')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of West Virginia')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Wisconsin')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Wisconsin')), 1),
	(replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Wyoming')), replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Short description of Wyoming')), 1)

-- OTP Settings
INSERT INTO OTP_Settings VALUES
	(N'Email.ServerAccount.SendGrid', N'Server=https://sendgrid.com/api/mail.send.xml;UserName=Michaelheinze;Password=iD6WiOvLlnvR'),
	(N'Email.Sender.Default', N'DisplayFrom=OTP Administrator;AddressFrom=administrator@ohtheplaces.com'),
	(N'Email.Sender.Support', N'DisplayFrom=Oh The Places Support;AddressFrom=support@ohtheplaces.com'),
	(N'Email.Sender.Homeowner', N'DisplayFrom=Oh The Places Support;AddressFrom=homeowner@ohtheplaces.com'),
	(N'ScheduledTasks.EventLogSummary.Recipients', N'otp-dev@lgbs.pl'), -- seperated by comma
	(N'DeploymentType', N'Development'),
	(N'FuzzySearch.DimensionWeight.Guests', N'25'),
	(N'FuzzySearch.DimensionWeight.Bathrooms', N'15'),
	(N'FuzzySearch.DimensionWeight.Bedrooms', N'15'),
	(N'FuzzySearch.DimensionWeight.Amenities', N'15'),
	(N'FuzzySearch.DimensionWeight.PropertyTypes', N'15'),
	(N'FuzzySearch.DimensionWeight.Tags', N'15'),
	(N'FuzzySearch.Threshold', N'1.75'),
	(N'FuzzySearch.Parameters.GuestsMax', N'2'),
	(N'FuzzySearch.Parameters.GuestsMin', N'2'),
	(N'FuzzySearch.Parameters.BathroomsMax', N'2'),
	(N'FuzzySearch.Parameters.BathroomsMin', N'2'),
	(N'FuzzySearch.Parameters.BedroomsMax', N'2'),
	(N'FuzzySearch.Parameters.BedroomsMin', N'2'),
	(N'FuzzySearch.Parameters.AmenitiesMax', N'2'),
	(N'FuzzySearch.Parameters.AmenitiesMin', N'2'),
	(N'FuzzySearch.Parameters.TagsMax', N'2'),
	(N'FuzzySearch.Parameters.TagsMin', N'2'),
	(N'FuzzySearch.Parameters.PropertyTypesMax', N'2'),
	(N'FuzzySearch.Parameters.PropertyTypesMin', N'2'),
	(N'Version', N'0.76'),
	(N'Properties.StandardCommission', N'15.0'),
	(N'ExtranetHost', N'127.0.0.1:81'),
    (N'EVS.User', ''),
	(N'EVS.Password', ''),
	(N'EVS.FailNotifyEmail', 'admin_evs@otp.com'),
	(N'Payment.ApiAuthToken', 'cb53cf23852b27bd288c010858e7f2678d9cd877'),
	(N'Payment.RedirectURL', 'http://127.0.0.1:81/Booking/PaymentGatewayRedirect'),
	(N'Payment.ApiLogin', 'EWAPAqOs1jMnOypWmerHt9orjJ1'),
	(N'Payment.ApiSecret', '4cmGB14YAAc2bjNdXS5OZmKvSzcxpdHxfBDpjbtibT2Pn2pwoJlcopWMV9xxfYhZ'),
	(N'Payment.GatewayToken', '4JQlPrraxv5gjZEdIjgqiemrXtF'),
	(N'Payment.Policy.FirstStepDays', '30'),
	(N'Payment.Policy.FirstStepDays.GreaterThan.PercentToAuthorize', '50'),
	(N'Payment.Policy.SecondStepDays', '15'),
	(N'Payment.Policy.SecondStepDays.GreaterThan.PercentToAuthorize', '100'),	
    (N'StaticContent.Image.Type_Thumbnail.MinSize', N'100x50'),
	(N'StaticContent.Image.Type_Thumbnail.MaxSize', N'300x300'),	
	(N'StaticContent.Image.Type_Thumbnail_Retina.MinSize', N'100x100'),
	(N'StaticContent.Image.Type_Thumbnail_Retina.MaxSize', N'300x300'),		
	(N'StaticContent.Image.Type_FullScreen.MinSize', N'1920x1080'),
	(N'StaticContent.Image.Type_FullScreen.MaxSize', N'2000x1100'),	
	(N'StaticContent.Image.Type_FullScreen_Retina.MinSize', N'1920x1080'),
	(N'StaticContent.Image.Type_FullScreen_Retina.MaxSize', N'2500x1650'),	
	(N'StaticContent.Image.Type_ListView.MinSize', N'250x200'),
	(N'StaticContent.Image.Type_ListView.MaxSize', N'300x250'),	
	(N'StaticContent.Image.Type_ListView_Retina.MinSize', N'350x320'),
	(N'StaticContent.Image.Type_ListView_Retina.MaxSize', N'400x370'),		
	(N'StaticContent.Image.Type_GalleryImage.MinSize', N'500x400'),
	(N'StaticContent.Image.Type_GalleryImage.MaxSize', N'600x500'),		
	(N'StaticContent.Image.Type_GalleryImage_Retina.MinSize', N'500x350'),
	(N'StaticContent.Image.Type_GalleryImage_Retina.MaxSize', N'800x600'),
	(N'EmailAccount.HomeownersTicketSystem', N'homeowners@ohtheplaces.com'),
	(N'ContactUs.GuestServicesEmail', N'support@ohtheplaces.com'),
	(N'ContactUs.GuestServicesPhone', N'(202) 733-1810'),
	(N'ContactUs.HomeownersServicesEmail', N'homeowners@ohtheplaces.com'),
	(N'ContactUs.HomeownersServicesPhone', N'(202) 733-1810'),
	(N'ContactUs.OTPHeadquartersAddressLine1', N'2000 N Street NW, Suite 1007'),
	(N'ContactUs.OTPHeadquartersAddressLine2', N'Washington DC 20036'),
	(N'ContactUs.OTPHeadquartersEmail', N'contact@ohtheplaces.com'),
	(N'ContactUs.OTPHeadquartersPhone', N'(202) 733-1810'),
	(N'FAQ.SupportEmail', N'contact@ohtheplaces.com'),
	(N'FAQ.SupportPhone', N'(202) 733-1810'),
	(N'FAQ.HomeownersEmail', N'homeowners@ohtheplaces.com'),
	(N'TicketSystem.JoinEmailAddress', N'otp-qa@lgbs.pl'),
	(N'Properties.MaxFileSizeInBytes', N'10485760'),
	(N'EmailAccount.OTPSupport', 'support@ohtheplaces.com'),	
	(N'Reservation.CompletedDaysAfterDeparture', '14'),
	(N'Containers.Properties', 'properties'),
	(N'Containers.ContentUpdateRequests', 'contentupdaterequests'),
	(N'Containers.PropertyDawData', 'propertyrawdata'),
	(N'ZohoCrm.AuthorizationToken', '3d3cb50004b304d8b15d39014468a7b1'),
	(N'WebAnalitics.GoogleTrackingId', 'UA-39132816-1') --  PROD: UA-38815400-1

-- Templates - based on JIRA templates
declare @cultureEN INT
set @cultureEN = (SELECT [CultureId] FROM [dbo].[DictionaryCultures] WHERE [CultureCode] = N'en-US')

INSERT [dbo].[Templates] ([CultureId], [Type], [Title], [Content]) VALUES
	-- email templates
	(@cultureEN, 0, N'Thanks for joining Oh The Places', N'<html><head></head><body style="font-family: Georgia;"><b>Dear {{UserFirstName}}, </b><p>Welcome to Oh The Places!</p><p>Please click the button below to confirm your email address.<br /><table style="background-color: #214F74; text-align: center; padding: 5px 15px; margin-top: 5px;"><tr><td><a href="{{ActivationLinkUrl}}" style="color: #fff; text-decoration: none;">{{ActivationLinkTitle}}</a></td></tr></table></p><p>Thank you for joining, <br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- GuestSignupConfirmation
	(@cultureEN, 1, N'Thanks for joining Oh The Places', N'<html><head></head><body style="font-family: Georgia;"><b>Dear {{UserFirstName}}, </b><p>Welcome to Oh The Places!</p><p>Your temporary generated password is: <b>{{TempPassword}}</b></p><br /><p>Please click the button below to confirm your email address.<br /><table style="background-color: #214F74; text-align: center; padding: 5px 15px; margin-top: 5px;"><tr><td><a href="{{ActivationLinkUrl}}" style="color: #fff; text-decoration: none;">{{ActivationLinkTitle}}</a></td></tr></table></p><br /><p>Thank you for joining, <br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- GuestSignupWithTempPassword
	(@cultureEN, 2, N'Thanks for joining Oh The Places', N'<html><head></head><body style="font-family: Georgia;"><b>Dear {{UserFirstName}}, </b><p>Thank you for joining Oh The Places!</p><p>We will call you at {{PhoneNumber}} during your preferred dates and times (listed below) to schedule a Welcome Meeting.</p><table><tr><td width="15%">Preferred Dates:</td><td width="30%">{{PreferredDates}}</td><td withd="10%">&nbsp;</td><td width="15%">Preferred Times:</td><td width="30%">{{PreferredTimeFrom}} - {{PreferredTimeTo}}</td></tr></table><p>We look forward to speaking with you.</p><p>Sincerely, <br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- OwnerJoinLetter	
	(@cultureEN, 4, N'Account added', N'<html><head></head><body style="font-family: Georgia;"><b>Dear {{UserFirstName}}, </b><p>Welcome to Oh The Places!</p><p>Your account has been added. You can login using the following login / password:</p><table><tr><td width="80px">Login:</td><td>{{UserLogin}}</td></tr><tr><td>Password:</td><td>{{UserPassword}}</td></tr></table><p>Please click the button below to confirm your email address before logging in.<br /><table style="background-color: #214F74; text-align: center; padding: 5px 15px; margin-top: 5px;"><tr><td><a href="{{ActivationLinkUrl}}" style="color: #fff; text-decoration: none;">{{ActivationLinkTitle}}</a></td></tr></table></p><br /><p>Thank you for joining, <br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- OwnerActivationEmail
	(@cultureEN, 5, N'Password reset', N'<html><head></head><body style="font-family: Georgia;"><b>Dear {{UserFirstName}}, </b><p>Your account password has been changed.</p><p>New temporary password is <b>{{TempPassword}}</b>. You should change it as soon as possible.</p><br /><p>Best regards, <br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- ResetPasswordEmail
	(@cultureEN, 6, N'EVS service error', N'<html><head></head><body style="font-family: Georgia;"><b>EVS system throw an error for user: {{UserName}}, id: {{UserId}}, </b><p>Error message {{Message}}</p></body></html>'), -- EVS Exception
	(@cultureEN, 7, N'EVS user blockage', N'<html><head></head><body style="font-family: Georgia;"><b>User: {{UserName}}, id: {{UserId}} has been blocked</b><p>User provided wrong answers for identity verification.</p></body></html>'), -- EVS User Block
	(@cultureEN, 8, N'Reservation Confirmation for the Owner', N'<html><head><style type="text/css">body { font-size: 14px; font-family: Georgia; } .rowHeader { color: White; background-color: Black; font-size: 14px; } table { border-color: White; border-width: 0px 0px 0px 0px; border-style: solid; } td { border-color: White; border-width: 1px 1px 1px 1px; border-style: solid; }</style></head><body><b>Dear {{OwnerFirstName}}, </b><p>We have just received a reservation for {{PropertyName}}!  Below is a summary of the reservation.</p><p>You can view all your reservations anytime at <a href="{{OTPFrontEnd}}">OhThePlaces.com</a> � login in to your Homeowner Account and go to Reservations.</p><p><table cellspacing="0"><tbody><tr class="rowHeader"><td valign="bottom" width="77">Confirm #</td><td valign="bottom" width="77">Booked</td><td valign="bottom" width="77">CheckIn</td><td valign="bottom" width="77">CheckOut</td><td valign="bottom" width="134">Guest</td><td valign="bottom" width="96">Gross Due</td><td valign="bottom" width="86">Transaction Fee</td><td valign="bottom" width="96">Owner Net</td></tr><tr><td valign="bottom" width="77"><p>{{ConfirmationId}}</p></td><td valign="bottom" width="77"><p>{{Booked}}</p></td><td valign="bottom" width="77"><p>{{CheckIn}}</p></td><td valign="bottom" width="77"><p>{{CheckOut}}</p></td><td valign="bottom" width="134"><p>{{GuestFullName}}</p></td><td valign="bottom" width="96"><p>{{GrossDue}}</p></td><td valign="bottom" width="86"><p>{{TransactionFee}}</p></td><td valign="bottom" width="96"><p>{{OwnerNet}}</p></td></tr></tbody></table></p><p>Sincerely, <br /><span class="header"><b>Oh The Places</b></span></p></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Reservation confirmation (Owner)
	(@cultureEN, 9, N'Reservation Confirmation for the Guest', N'<html><head><style type="text/css">body { font-size: 14px; font-family: Georgia; } .rowHeader { color: White; background-color: Black; font-size: 14px; } td { vertical-align: top; text-align: left; } .borderRight { border-right: 1px solid Black; } .borderTop { border-top: 1px solid Black; } .borderBottom { border-bottom: 1px solid Black; }</style></head><body><b>Dear {{GuestFirstName}}, </b><p>Thank you for staying with Oh The Places!  For your convenience, below are your Trip Details.</p><p>You can view and print your Trip Details anytime at <a href="{{OTPFrontEnd}}">OhThePlaces.com</a> - go to Guest Login and your Trip Details page.</p><p>Have a great vacation and please visit us at at <a href="{{OTPFrontEnd}}">OhThePlaces.com</a> if you need any further assistance.</p><table style="border-bottom: 2px solid Black; margin-bottom: 0px; font-size: 14px; font-family: Georgia; width: 100%" cellpadding="0"><tr><td><b>Trip Details</b></td><td style="text-align: right" align="right">Confirmation code: <b>{{ConfirmationCode}}</b></td></tr></table><table cellspacing="0"><p><br /><tr><td colspan="2">{{GuestFullName}} ({{GuestsNumber}} {{CardinalityPerson}})</td><td style="width: 20px;" class="borderRight">&nbsp;</td><td style="width: 20px;"></td><td>{{PropertyName}}</td></tr><tr><td class="borderTop" style="width: 120px;">Itinerary</td><td class="borderTop" style="width: 350px;">{{CheckIn}} to {{CheckOut}}<br />{{NightsNumber}} nights<br />Check in: <span style="padding-left: 1em">{{CheckInTime}}</span><br />Check out: <span style="padding-left: 1em">{{CheckOutTime}}</span><br />Reservation made {{BookedDate}}</td><td style="width: 20px;" class="borderRight">&nbsp;</td><td style="width: 20px;"></td><td style="width: 350px;" class="borderTop">{{PropertyAddress}}<br />{{PropertyCountry}}<br /><div style="width: 300px; height: 200px"><a href="https://maps.google.com/?q={{Latitude}}, {{Longitude}}"><img src="ecid:http://maps.google.com/maps/api/staticmap?center={{Latitude}}, {{Longitude}}&markers=icon:http://maps.google.com/mapfiles/marker.png|{{Latitude}},{{Longitude}}&zoom=13&size=220x200&sensor=true" alt="Map" /></a></div></td></tr><tr><td>Arrival Info</td><td>On {{DateBeforeArrival_14}}, 14 days prior to your arrival, we will send you the information about accessing the property (keys, etc.) to {{GuestEmail}}.</td><td style="width: 20px;" class="borderRight">&nbsp;</td><td style="width: 20px;"></td><td>{{Directions}}<br />Parking Information:<br />{{OnStreetParkingAvailable}}</td></tr><tr><td>Maximum Guests</td><td>The maximum number of guests for this property is {{MaxNoOfGuests}}. Please review the guest contract if you have any questions.</td><td style="width: 20px;" class="borderRight">&nbsp;</td><td style="width: 20px;"></td><td></td></tr><tr><td class="borderBottom">Cancellation Policy</td><td class="borderBottom">{{CancellationPolicy}} For additional information on cancellation policies, please refer to <a href="http://ohtheplaces.com/terms">Oh The Places� terms and conditions</a>.</td><td style="width: 20px;" class="borderRight">&nbsp;</td><td style="width: 20px;"></td><td class="borderBottom">&nbsp;</td></tr></p></table><p>Sincerely, <br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Reservation confirmation (Guest)
	(@cultureEN, 10, N'User supplied wrong birth date', N'<html><head></head><body style="font-family: Georgia;"><b>Age verification failed o payment process for user {{UserName}}.</b><p>Supplied birth date {{BirthDate}}</p></body></html>'), -- Age verification failed
	(@cultureEN, 11, N'Password reset', N'<html><head></head><body style="font-family: Georgia;"><b>Dear {{UserFirstName}}, </b><p>Your account password has been changed.</p><p>New temporary password is <b>{{TempPassword}}</b>. You should change it as soon as possible.</p><table style="background-color: #214F74; text-align: center; padding: 5px 15px; margin-top: 5px;"><tr><td><a href="{{LinkUrl}}" style="color: #fff; text-decoration: none;">Back to Booking</a></td></tr></table><p>Sincerely, <br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- ResetPasswordEmail
	(@cultureEN, 12, N'Reservation Cancellation Confirmation', N'<html><head></head><body style="font-family: Georgia;"><b>Dear {{UserFirstName}}, </b><p>This is to confirm that we have canceled your Trip, confirmation number {{ConfirmationNumber}} for {{NameOfPlace}} from {{FromDate}} to {{ToDate}}.</p><p>We hope you stay with us in the future.</p><p>Sincerely, <br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'),
	(@cultureEN, 13, N'Homeowners Contact Us Form',N'{{Message}}'),
	(@cultureEN, 14, N'[OTP][{{Source}}][{{DeploymentType}}] Event Log Summary - {{SummaryDay}}', N'<html><head></head><body style="font-family: Georgia;"><p>Daily event log summary for {{Source}} ({{DeploymentType}}) - {{SummaryDay}}</p><table><tr><td width="150px">Errors:</td><td>{{NumberOfErrors}}</td></tr><tr><td>Warnings:</td><td>{{NumberOfWarnings}}</td></tr></table><br /><p>Sincerely, <br /><b>Oh The Places</b></p></body></html>'), -- EventLogSummaryEmail
	(@cultureEN, 15, N'Request for Homeowner Join', N'<html><head><link href="http://fonts.googleapis.com/css?family=Armata" rel="stylesheet" type="text/css"><link href="http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300,700" rel="stylesheet" type="text/css"></head><body style="font-family: \''Yanone Kaffeesatz\'';"><h3>New homeowner signed up: {{FullName}}</h3><p>First name: {{FirstName}}, Last name: {{LastName}}, Initials: {{Initials}}</p><p> Email address: {{Email}}, Phone number: {{PhoneNumber}}</p><p>Address: {{AddressLine1}}, {{AddressLine2}}</p><p>City: {{City}}, State: {{State}}</p><p>Zip Code: {{Zip Code}}, Country: {{Country}}</p></p><h4>Welcome meeting: </h4></p><p>Preferred Dates: {{PreferredDates}} </p> <p>Preferred Times:{{PreferredTimeFrom}} - {{PreferredTimeTo}}</p></p> <img src="otplogo.jpg" alt="Oh The Places Logo" /></body></html>'), -- HomeOwner Join - to ticket system
	(@cultureEN, 16, N'Account added', N'<html><head></head><body style="font-family: Georgia;"><b>Dear {{UserFirstName}}, </b><p>Welcome to Oh The Places!</p><p>Your account has been added. You can login using the following login / password:</p><table><tr><td width="80px">Login:</td><td>{{UserLogin}}</td></tr><tr><td>Password:</td><td>{{UserPassword}}</td></tr></table><p>Thank you for joining, <br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- UserAccountAdded
	(@cultureEN, 17, N'Check In Information for the Guest', N'<html><head><style type="text/css">body { font-size: 14px; font-family: Georgia; } .rowHeader { color: White; background-color: Black; font-size: 14px; } td { vertical-align: top; text-align: left; } .borderRight { border-right: 1px solid Black; } .borderTop { border-top: 1px solid Black; } .borderBottom { border-bottom: 1px solid Black; }</style></head><body><b>Dear {{GuestFirstName}}, </b><p>Your arrival information (including key access) is detailed below.</p><p>So that we may best plan for your arrival, please let us know your arrival details using our quick and easy arrival information tool below:</p><table style="margin-bottom: 0px; font-size: 14px; font-family: Georgia; width: 100%" cellpadding="0"><tr><td style="width: 180px;">Estimated Arrival Time:</td><td>{{ArrivalTimeRanges}}<td></td></tr><tr style="height: 20px;"><td colspan="2"></td></tr><tr><td colspan="2"><b style="text-decoration: underline;">Arrival Information</b></td></tr><tr><td colspan="2">{{PropertyName}}</td></tr><tr><td>{{PropertyAddress}}</td></tr><tr><td>Arrival Date:</td><td>{{ArrivalDate}}</td></tr><tr><td>Check In:</td><td>{{CheckIn}}</td></tr><tr><td>Key Pick Up:</td><td>The keys will be stored in the lockbox.<br />Lockbox code: {{LockboxCode}}<br />Lockbox location: {{LockboxLocation}}</td></tr>{{AlarmCode}}{{AlarmLocation}}{{OtherAccessInformation}}<tr><td>Wifi Access:</td><td>Access Point Name: {{WifiName}}, Password: {{WifiPassword}}</td></tr><tr><td>Directions:</td><td><a href="https://maps.google.com/?q={{Latitude}}, {{Longitude}}"><img src="ecid:http://maps.google.com/maps/api/staticmap?center={{Latitude}}, {{Longitude}}&markers=icon:http://maps.google.com/mapfiles/marker.png|{{Latitude}},{{Longitude}}&zoom=13&size=220x200&sensor=true" alt="Map" /></a></td></tr></table><p style="text-decoration: underline; font-weight: bold;">Confirm Check In</p><p>Please let us know that you arrived:<table style="background-color: #214F74; text-align: center; padding: 5px 15px; margin-top: 5px;"><tr><td><a href="{{CheckInLinkUrl}}" style="color: #fff; text-decoration: none;">Confirm Check In</a></td></tr></table><br /></p><p style="text-decoration: underline; font-weight: bold;">Trip Details Always Online!</p><p>We look forward to seeing You! </p><p>The arrival information, along with all your trip information, is always available online in your Trip Details.<p><p>Just go to <a href="{{ExtranetUrl}}">OhThePlaces.com</a>, Guest Join / Login, enter your Email and Password and click Trip Details.</p><br />Sincerely,<br /><b>Oh The Places</b><br /><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Check In (Lockbox) Email
	(@cultureEN, 18, N'Check In Information for the Guest', N'<html><head><style type="text/css">body { font-size: 14px; font-family: Georgia; } .rowHeader { color: White; background-color: Black; font-size: 14px; } td { vertical-align: top; text-align: left; } .borderRight { border-right: 1px solid Black; } .borderTop { border-top: 1px solid Black; } .borderBottom { border-bottom: 1px solid Black; }</style></head><body><b>Dear {{GuestFirstName}}, </b><p>Your arrival information (including key access) is detailed below.</p><p>So that we may best plan for your arrival, please let us know your arrival details using our quick and easy arrival information tool below:</p><table style="margin-bottom: 0px; font-size: 14px; font-family: Georgia; width: 100%" cellpadding="0"><tr><td style="width: 180px;">Estimated Arrival Time:</td><td>{{ArrivalTimeRanges}}<td></td></tr><tr style="height: 20px;"><td colspan="2"></td></tr><tr><td colspan="2"><b style="text-decoration: underline;">Arrival Information</b></td></tr><tr><td colspan="2">{{PropertyName}}</td></tr><tr><td>{{PropertyAddress}}</td></tr><tr><td>Arrival Date:</td><td>{{ArrivalDate}}</td></tr><tr><td>Check In:</td><td>{{CheckIn}}</td></tr><tr><td>Key Pick Up:</td><td>The keys should be picked up from {{KeyManagerFullName}} in {{KeyManagerAddress}}</td></tr>{{AlarmCode}}{{AlarmLocation}}{{OtherAccessInformation}}<tr><td>Wifi Access:</td><td>Access Point Name: {{WifiName}}, Password: {{WifiPassword}}</td></tr><tr><td>Directions:</td><td><a href="https://maps.google.com/?q={{Latitude}}, {{Longitude}}"><img src="ecid:http://maps.google.com/maps/api/staticmap?center={{Latitude}}, {{Longitude}}&markers=icon:http://maps.google.com/mapfiles/marker.png|{{Latitude}},{{Longitude}}&zoom=13&size=220x200&sensor=true" alt="Map" /></a></td></tr></table><p style="text-decoration: underline; font-weight: bold;">Confirm Check In</p><p>Please let us know that you arrived:<table style="background-color: #214F74; text-align: center; padding: 5px 15px; margin-top: 5px;"><tr><td><a href="{{CheckInLinkUrl}}" style="color: #fff; text-decoration: none;">Confirm Check In</a></td></tr></table><br /></p><p style="text-decoration: underline; font-weight: bold;">Trip Details Always Online!</p><p>We look forward to seeing You! </p><p>The arrival information, along with all your trip information, is always available online in your Trip Details.<p><p>Just go to <a href="{{ExtranetUrl}}">OhThePlaces.com</a>, Guest Join / Login, enter your Email and Password and click Trip Details.</p><br />Sincerely,<br /><b>Oh The Places</b><br /><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Check In (Pick Up from Key Handler) Email
	(@cultureEN, 19, N'Check Out Information for the Guest', N'<html><head></head><body style="font-size: 14px; font-family: Georgia;"><b>Dear {{GuestFirstName}}, </b><p>We hope you have enjoyed your stay with Oh The Places. At Check Out tomorrow, {{DepartureDate}}, please follow the instructions below to return the keys:</p><p>Leave the keys in the lockbox.<br />Lockbox code: {{LockboxCode}}<br />Lockbox location: {{LockboxLocation}}</p><p>We look forward to seeing you again!</p><p>Sincerely,<br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Check out (Lockbox strategy) email
	(@cultureEN, 20, N'Check Out Information for the Guest', N'<html><head></head><body style="font-size: 14px; font-family: Georgia;"><b>Dear {{GuestFirstName}}, </b><p>We hope you have enjoyed your stay with Oh The Places. At Check Out tomorrow, {{DepartureDate}}, please follow the instructions below to return the keys:</p><p>Give the keys to the {{KeyManagerFullName}} who is responsible for storing the keys for the property. You can find the person in: {{KeyManagerAddress}}</p><p>We look forward to seeing you again!</p><p>Sincerely,<br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Check out (Pick Up from Key handler strategy) email
	(@cultureEN, 21, N'Set of Arrivals for the Key Manager', N'<html><head><style type="text/css">body { font-size: 14px; font-family: Georgia; } .rowHeader { color: White; background-color: Black; font-size: 14px; } table { border-color: White; border-width: 0px 0px 0px 0px; border-style: solid; } td { border-color: White; border-width: 1px 1px 1px 1px; border-style: solid; }</style></head><body><b>Dear {{KeyManagerFirstName}},</b><p>The following arrivals are schedule for tomorrow, {{TomorrowDate}}, please remember to confirm the pickup in your Key Handler area:<p><table cellspacing="0"><tbody><tr class="rowHeader"><td valign="bottom" width="77">Check In</td><td valign="bottom" width="77">Estimated Arrival</td><td valign="bottom" width="77">Place</td><td valign="bottom" width="77">Unit</td><td valign="bottom" width="134">Guest Name</td></tr><tr><td valign="bottom" width="77"><p>{{CheckIn}}</p></td><td valign="bottom" width="77"><p>{{EstimatedArrivalTime}}</p></td><td valign="bottom" width="77"><p>{{PropertyName}}</p></td><td valign="bottom" width="77"><p>{{UnitName}}</p></td><td valign="bottom" width="134"><p>{{GuestFullName}}</p></td></tr></tbody></table><p>If possible, when Guest checks in, please check identification and confirm the Guest is older than 25 years. If the Guest is not 25 years of age or older, please call Oh The Places and do not give the Guest the key.</p><p>Thank You!</p>Sincerely,<br /><b>Oh The Places</b><br /><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Key Handler set of arrivals email
	(@cultureEN, 22, N'Key Drop-off Confirm', N'<html><head></head><body style="font-size: 14px; font-family: Georgia;"><b>Dear {{GuestFirstName}}, </b><p>Thanks so much for staying with us!</p><p>Please confirm you have returned the key by simply clicking the button below.<br /><table style="background-color: #214F74; text-align: center; padding: 5px 15px; margin-top: 5px;"><tr><td><a href="{{ActivationLinkUrl}}" style="color: #fff; text-decoration: none;">Key Return Confirmed</a></td></tr></table></p><p>We look forward to seeing you again!<br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Key drop of email - locbox
	(@cultureEN, 23, N'Key Drop-off Confirm', N'<html><head></head><body style="font-size: 14px; font-family: Georgia;"><b>Dear {{GuestFirstName}}, </b><p>Thanks so much for staying with us!</p><p>Please confirm you have returned the key by simply clicking the button below.<br /><table style="background-color: #214F74; text-align: center; padding: 5px 15px; margin-top: 5px;"><tr><td><a href="{{ActivationLinkUrl}}" style="color: #fff; text-decoration: none;">Key Return Confirmed</a></td></tr></table></p><p>We look forward to seeing you again!<br /><b>Oh The Places</b></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Key drop of email - pick up from key manager
	(@cultureEN, 24, N'Key Drop-off Alert', N'<html><head><style type="text/css">body { font-size: 14px; font-family: Georgia; } .rowHeader { color: White; background-color: Black; font-size: 14px; } td { vertical-align: top; text-align: left; } .borderRight { border-right: 1px solid Black; } .borderTop { border-top: 1px solid Black; } .borderBottom { border-bottom: 1px solid Black; }</style></head><body><b>Dear {{UploadManagerFirstName}}, </b><p>Key from property still has not been returned. Check and resolve the problem.</p><table style="margin-bottom: 0px; font-size: 14px; font-family: Georgia; width: 100%" cellpadding="0"><tr><td colspan="2"><b style="text-decoration: underline;">Informations</b></td></tr><tr><td style="width: 210px;">Property Name:</td><td>{{PropertyName}}</td></tr><tr><td> Property Address:</td><td>{{PropertyAddress}}</td></tr><tr><td>Departure Date:</td><td>{{DepartureDate}}</td></tr><tr><td>Guest:</td><td>{{GuestFullName}}</td></tr><tr><td>Reservartion Confirmation ID:</td><td>{{ReservationConfirmationId}}</td></tr></table><br />Sincerely,<br /><b>Oh The Places</b><br /><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Key drop alert - pick up from key manager
	(@cultureEN, 25, N'Key Drop-off Second Alert', N'<html><head><style type="text/css">body { font-size: 14px; font-family: Georgia; } .rowHeader { color: White; background-color: Black; font-size: 14px; } td { vertical-align: top; text-align: left; } .borderRight { border-right: 1px solid Black; } .borderTop { border-top: 1px solid Black; } .borderBottom { border-bottom: 1px solid Black; }</style></head><body><b>Dear {{UploadManagerFirstName}}, </b><p>Key from property still has not been returned. Resolve the problem as soon as possible.</p><table style="margin-bottom: 0px; font-size: 14px; font-family: Georgia; width: 100%" cellpadding="0"><tr><td colspan="2"><b style="text-decoration: underline;">Informations</b></td></tr><tr><td style="width: 210px;">Property Name:</td><td>{{PropertyName}}</td></tr><tr><td> Property Address:</td><td>{{PropertyAddress}}</td></tr><tr><td>Departure Date:</td><td>{{DepartureDate}}</td></tr><tr><td>Guest:</td><td>{{GuestFullName}}</td></tr><tr><td>Reservartion Confirmation ID:</td><td>{{ReservationConfirmationId}}</td></tr></table><br />Sincerely,<br /><b>Oh The Places</b><br /><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Key drop second alert email - pick up from key manager
	(@cultureEN, 26, N'[Payment Error] The reservation cannot be completed because of no funds', N'<html><head></head><body style="font-family: Georgia;"><img src="cid:LogoMin" alt="Oh The Places Logo" /><br /><p>The reservation cannot be completed because there are not enough funds on the credit card.<br /> Security deposit has been authorized and needs to be released manually. Reservation�s data:</p> <br /> <table border="0"> <tr> <td>Guest: </td> <td>{{GuestFullName}}</td> </tr> <tr> <td>Property name: </td> <td>{{PropertyName}}</td> </tr> <tr> <td>Travel period: </td> <td>{{TravelPeriod}}</td> </tr> </table></body></html>'), -- Reservation NoFunds send to support
	(@cultureEN, 27, N'[CRM Error] The lead for the homeowner {{FirstName}} {{LastName}} has not been created', N'<html><head><style type="text/css">body { font-size: 14px; font-family: Georgia; } .rowHeader { color: White; background-color: Black; font-size: 14px; } td { vertical-align: top; text-align: left; } .borderRight { border-right: 1px solid Black; } .borderTop { border-top: 1px solid Black; } .borderBottom { border-bottom: 1px solid Black; }</style></head><body><p>There was an error while communicating with the Zoho CRM. The lead for the homeowner has not been created. This step must be done manually. Homeowner�s data:</p><table style="margin-bottom: 0px; font-size: 14px; font-family: Georgia; width: 100%" cellpadding="0"><tr><td>{{FirstName}} {{LastName}}</td></tr><tr><td>{{Address1}}</td></tr><tr><td>{{Address2}}</td></tr></table></body></html>'), -- Error occurred while creating a lead
	(@cultureEN, 28, N'[CRM Error] The lead for the homeowner {{FirstName}} {{LastName}} has not been converted ', N'<html><head><style type="text/css">body { font-size: 14px; font-family: Georgia; } .rowHeader { color: White; background-color: Black; font-size: 14px; } td { vertical-align: top; text-align: left; } .borderRight { border-right: 1px solid Black; } .borderTop { border-top: 1px solid Black; } .borderBottom { border-bottom: 1px solid Black; }</style></head><body><p>There was an error while communicating with the Zoho CRM. The lead for the homeowner has not been converted into the account and the contact. This step must be done manually. Homeowner�s data:</p><table style="margin-bottom: 0px; font-size: 14px; font-family: Georgia; width: 100%" cellpadding="0"><tr><td>{{FirstName}} {{LastName}}</td></tr><tr><td>{{Address1}}</td></tr><tr><td>{{Address2}}</td></tr></table></body></html>'), -- Error occurred while converting a lead
	(@cultureEN, 29, N'Property {{PropertyName}} "sign off"', N'<html><head></head><body style="font-family: Georgia;"><b>Dear {{UserFirstName}}, </b><p>The information about your property has been prepared in our system. Please review the data about the property clicking the below and confirm it if the data is correct. After your confirmation the property will be visible for guests.<br /><p><a href="{{PreviewLink}}">{{PreviewLinkTitle}}</a></p><p><a href="{{ConfirmSignOffLink}}">{{ConfirmSignOffLinkTitle}}</a></p><br /><p>Sincerely,</p><p>Oh The Places</p><a href="ohtheplaces.com"/>ohtheplaces.com</a><p></p><img src="cid:LogoMin" alt="Oh The Places Logo" /></body></html>'), -- Sign Off information to Homeowner

	-- document templates
	(@cultureEN, 100, N'Ohtheplaces_Agreement_{{ConfirmationCode}}', N'<html><head><link href="http://fonts.googleapis.com/css?family=Armata" rel="stylesheet" type="text/css"><link href="http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400, 200, 300, 700" rel="stylesheet" type="text/css"><style type="text/css"> .header { text-align: center; } body { font-family: Georgia; text-align: justify !important; }</style></head><body><div><p class="header"><b><span>Owner and Guest</span> Short Term Rental Agreement</b></p></div><p>This Short Term Rental Agreement (the �Agreement�) is made by and between {{OwnerFullName}} (�Owner�) and {{GuestFullName}} (�Guest�) as of this date: {{CreatedDate}}. For good and valuable consideration, the sufficiency of which is acknowledged, the parties hereby agree as follows:</p><br /><p><b>1. Property:</b> The Property is located at:<br />{{PropertyAddress}}.<br />The Property is furnished and includes {{Amenities}}.</p><p><b>2. Rental Party:</b> The rental party shall consist of Guest and other persons not to exceed {{GuestsNumbers}}. If the number of occupants at the Property exceeds {{GuestsNumbers}}, Guest may be asked to immediately vacate the Property and to forfeit any remaining rental days and all rental fees paid.</p><p><b>3. Term of the Lease:</b> The lease begins at 4 p.m. on {{CheckInDate}} (the �Check-in Date�) and ends at 11 a.m. on {{CheckOutDate}} (the "Checkout Date").</p><p><b>4. Rental Rules:</b> Guest agrees to abide by the Rental Rules attached as Exhibit A at all times while at the Property and shall cause all members of the rental party and anyone else Guest permits on the Property to abide by the following rules at all times while at the Property.</p><p><b>5. Access:</b> Upon reasonable notice in advance, Guest shall allow Owner or a repair or service person authorized by Owner or Oh the Places, LLC. ("OTP") to access the Property for purposes of repair, service and inspection. Owner (or the authorized repair or service person) shall exercise this right of access in a reasonable manner.</p><p><b>6. Rental Rate and Fees:</b></p><p>{{PaymentInfo}}</p><p><b>7. Cancellation Policy:</b> If Guest wishes to cancel his/her reservation, Guest shall notify Owner via OTP as defined in {{TermsOfServiceHref}} that Guest is cancelling the reservation. The terms and conditions set forth in the Guest Agreement between Guest and OTP shall govern Guests financial responsibilities in the event of cancellation.</p><p><b>8. Insurance:</b> We encourage Guests and all members of the rental party to purchase traveler insurance.</p><p><b>9. Acceptance:</b>Guest has given permission to OTP charge Guests''s credit card for the amounts above. Guest agrees that all rental monies are refundable only in accordance with the Guest Agreement between Guest and OTP. Guest has read Guest''s rights to purchase travel insurance.</p><p>The parties agree to the terms of this Owner and Guest Short Term Rental Agreement, as evidenced by the signatures and initials (entered electronically by each party) as set forth below.<br /><br />BY ELECTRONICALLY ENTERING YOUR INITIALS BELOW, YOU AGREE TO BE BOUND BY THE TERMS AND CONDITIONS DESCRIBED IN THIS AGREEMENT AND ALL POLICIES AND GUIDELINES INCORPORATED BY REFERENCE. IF YOU DO NOT AGREE TO THESE TERMS OR CONDITIONS, DO NOT ENTER YOUR INITIALS ELECTRONICALLY.</p><p style="page-break-after:always"><table style="width: 100%;"><tbody><tr><td style="width: 15%;"></td><td style="width:30%; text-align: center;">Owner:</td><td style="width: 5%;"></td><td style="width:30%; text-align: center;">Guest:</td><td style="width: 20%;"></td></tr><tr><td>Name (printed):</td><td style="border-bottom: 1px solid Black; text-align: center;">{{OwnerFullName}}</td><td></td><td style="border-bottom: 1px solid Black; text-align: center;">{{GuestFullName}}</td><td></td></tr><tr><td>Initials:</td><td style="border-bottom: 1px solid Black; text-align: center;">{{OwnerInitials}}</td><td></td><td style="border-bottom: 1px solid Black; text-align: center;">{{GuestInitials}}</td><td></td></tr></tbody></table></p><div style="text-align: center;"><p><b>Exhibit A<br />Rental Rules</b></p></div><p>1. Smoking is NOT allowed anywhere on, in or upon the Property.</p><p>2. People other than those in the Guest party set forth above may not stay overnight in the Property. Any person on, in or upon the Property is the sole responsibility of Guest.</p><p>3. All of the units are privately owned; Owner is not responsible for any accidents, injuries or illness that occurs while on the Property or its facilities. Owner is not responsible for the loss of personal belongings or valuables of the Guest or any member of the guest''s party. By accepting this reservation, it is agreed that Guest or any member of the guest''s party are expressly assuming the risk of any harm arising from their use of the Property or others whom they invite to use the Property.</p><p>4. Guest or any member of the guest''s party shall keep the Property and all furnishings in good order.</p><p>5. Guest or any member of the guest\s party shall only use appliances for their intended uses.</p><p>6. Housekeeping: There is no daily housekeeping service. While linens and bath towels are included in the unit, daily maid service is not included in the rental rate. It is suggested that Guests bring their own beach towels to the Property. We do not permit towels or linens to be taken from the Property.</p><p>7. Hot Tub: When using the hot tub, remember there is a certain health risk associated with this facility. Use at your own risk. The hot tub is drained, sanitized, refilled and replenished with chemicals prior to your arrival; therefore, it may not be warm until later that evening. DO NOT STAND ON THE HOT TUB COVERS. Hot tub covers are for insulation purposes and are not designed to support a person or persons. They will break and you may be charged for replacement. Remember when not using the hot tub, leave cover on so hot tub will stay warm.</p></body></html>'), -- Agreement document
	(@cultureEN, 101, N'Ohtheplaces_TripDetails_{{ConfirmationCode}}', N'<html><head><link href="http://fonts.googleapis.com/css?family=Armata" rel="stylesheet" type="text/css"><link href="http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400, 200, 300, 700" rel="stylesheet" type="text/css"><style type="text/css"> body { font-size: 12px; font-family: Georgia; } .rowHeader { color: White; background-color: Black; font-size: 12px; } table { width: 100%; font-size: 12px; } td { vertical-align: top; text-align: left; padding-bottom: 15px; } .borderRight { border-right: 1px solid Black; } .borderTop { border-top: 1px solid Black; } .borderBottom { border-bottom: 1px solid Black; }</style></head><body><div style="text-align: right"><img src="cid:Logo150110" alt="Oh The Places Logo" /></div><h3>{{GuestFullName}}</h3></p><div style="border-bottom: 2px solid Black; margin-bottom: 0px;"><b>Trip Details</b><div style="float: right; ">Confirmation code:<b>{{ConfirmationCode}}</b></div></div><br /><br /><table cellspacing="0"><tr><td colspan="2">{{GuestFullName}} ({{GuestsNumber}} {{CardinalityPeron}})</td><td style="width: 20px;" class="borderRight">&nbsp;</td><td style="width: 20px;"></td><td>{{PropertyName}}</td></tr><tr><td class="borderTop" style="width: 120px;">Itineary</td><td class="borderTop" style="width: 230px;">{{CheckIn}} to {{CheckOut}}<br /> {{NightsNumber}} nights<br /> Check in:<span style="padding-left: 1em">{{CheckInTime}}</span><br /> Check out:<span style="padding-left: 1em">{{CheckOutTime}}</span><br /> Reservation made {{BookedDate}}</td><td style="width: 20px;" class="borderRight">&nbsp;</td><td style="width: 20px;"></td><td style="width: 230px;" class="borderTop">{{PropertyAddress}}<br /> {{PropertyCountry}}<br /><div style="width: 220px; height: 200px"><a href="https://maps.google.com/?q={{Latitude}}, {{Longitude}}"><img src="http://maps.google.com/maps/api/staticmap?center={{Latitude}}, {{Longitude}}&markers=icon:http://maps.google.com/mapfiles/marker.png|{{Latitude}},{{Longitude}}&zoom=13&size=220x200&sensor=true" alt="Map" /></a></div></td></tr><tr><td>Arrival Info</td><td>On {{DateBeforeArrival}}, 2 days prior to your arrival, we will send you the information about accessing the property (keys, etc.) to {{GuestEmail}}.</td><td style="width: 20px;" class="borderRight">&nbsp;</td><td style="width: 20px;"></td><td>Directions from the airport:<br /> {{Directions}}<br /> Parking Information:<br /> {{OnStreetParkingAvailable}}</td></tr><tr><td>Maximum Guests</td><td>The maximum number of guests for this property is {{MaxNoOfGuests}}. Please review the guest contract if you have any questions.</td><td style="width: 20px;" class="borderRight">&nbsp;</td><td style="width: 20px;"></td><td></td></tr><tr><td class="borderBottom">Cancellation Policy</td><td class="borderBottom">{{CancellationPolicy}} For additional information on cancellation policies, please refer to <a href="{{TermsOfServiceHref}}">Oh The Places� terms and conditions</a>.</td><td style="width: 20px;" class="borderRight">&nbsp;</td><td style="width: 20px;"></td><td class="borderBottom">&nbsp;</td></tr></table></body></html>'), -- Trip details document
	(@cultureEN, 102, N'Ohtheplaces_Invoice_{{ConfirmationId}}', N'<html><head><link href="http://fonts.googleapis.com/css?family=Armata" rel="stylesheet" type="text/css"><link href="http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400, 200, 300, 700" rel="stylesheet" type="text/css"><style type="text/css"> body { font-family: Georgia; } .header { font-size: 18px; } .rowHeader { color: White; background-color: #808080; font-size: 16px; font-weight: bold; height: 32px; padding-left: 5px; padding-right: 5px; border-bottom: 1px solid #808080; } .billInfo { font-size: 14px; color: #808080; } .billInfoRow{ padding-left: 8px; padding-right: 8px; border-bottom: 1px solid #808080; } .invoiceInfo { color: #808080; } .invoiceInfo tr{ height: 20px; } .invoiceInfo td { border-top: 1px solid #D9D9D9; font-size: 14px; color: #808080; } .invoiceInfoHeader { background-color: #808080; height: 20px; } .invoiceInfoHeader{ color: White !important; font-weight: bold; font-size: 14px; } .alignCenter { text-align: center; border-bottom: 1px solid #D9D9D9; } .alignRight { text-align: right; border-bottom: 1px solid #D9D9D9; } .alignLeft { text-align: left; border-bottom: 1px solid #D9D9D9; } .lastRow { border-top: 1px solid #808080 !important; } .invoiceSummary { font-size: 14px; color: #808080; } .invoiceSummary tr { height:20px; } .sumAlignLeft { text-align: left; width: 75%; border-bottom: 1px solid #D9D9D9; } .sumAlignRight { text-align:right; width: 30%; border-bottom: 1px solid #D9D9D9; } .boldRow { border-bottom: 1px solid #808080 !important; font-weight: bold; } .message { margin-top: 30px; color: #808080; } .footer { width: 80%; border-top:1px solid #D9D9D9 !important; width: 600px ; margin-left: auto ; margin-right: auto; position: fixed; top: 780px; } .footer ul{ font-family: "Cambria"; font-size: 13px; color: #D9D9D9; } .line { } .header { font-family: "Arial"; font-size: 14px; color: #808080; }</style></head><body><div ><img style="float:left; margin-top: 10px;" src="cid:Logo150110" /><p style="float:left; margin-left: 70px; line-height: 20px;" class="header"> Oh The Places LLC<br /> 2000 N St. NW, Suite 1007<br /> Washington, DC 20036<br /> p. 202-733-1810<br /></p></div><div style="clear: both;"></div><div style="margin-top: 25px;"><table cellspacing="0" style="width: 100%;"><tbody><tr class="rowHeader"><td style="text-align: left; padding-left: 10px;" valign="middle" width="50%">INVOICE {{InvoiceId}}</td><td style="text-align: right; padding-right: 10px;" valign="middle" width="50%">{{CreatedDate}}</td></tr></tbody></table><br /></div><div><table cellspacing="0" style="width: 350px;" class="billInfo"><tbody><tr><td class="billInfoRow" valign="middle" width="50%">BILL TO</td><td class="billInfoRow" valign="middle" width="50%">TRIP DETAILS</td></tr><tr><td>{{GuestFullName}}</td><td>{{PropertyName}}</td></tr><tr><td>{{GuestAddress1}}</td><td>{{PropertyAddress}}</td></tr><tr><td>{{GuestAddress2}}</td><td>{{ConfirmationId}}</td></tr></tbody></table><br /><br /></div><div><table cellspacing="0" style="width: 100%;" class="invoiceInfo"><tbody><tr><td class="invoiceInfoHeader" valign="middle" width="60%">DESCRIPTION</td><td class="invoiceInfoHeader" valign="middle" style="text-align: right" width="15%">UNIT PRICE</td><td class="invoiceInfoHeader" valign="middle" style="text-align: center" width="15%">QUANTITY</td><td class="invoiceInfoHeader" valign="middle" style="text-align: right" width="15%">TOTAL</td></tr> {{InvoicePositions}}<tr><td colspan="4" class="lastRow">&nbsp;</td></tr></tbody></table></div><div style="float:right;"><table cellspacing="0" style="width: 350px;" class="invoiceSummary"><tbody><tr><td class="sumAlignLeft">SUBTOTAL</td><td class="sumAlignRight">{{Subtotal}}</td></tr>{{SevenDayDiscount}}{{SalesAndTaxes}}<tr><td class="sumAlignLeft boldRow">TOTAL DUE</td><td class="sumAlignRight boldRow">{{Total}}</td></tr><tr><td class="sumAlignLeft boldRow">{{DuePercent}} {{DueByDate}}</td><td class="sumAlignRight boldRow">&nbsp;</td></tr> {{SecondPartDue}}</tbody></table><div><p class="message">Thank you for staying with Oh The Places!</p></div></div><div style="clear: both;"></div><div class="footer" ><ul><li>{{CancellationPolicy}}</li>{{SecurityDeposit}}</ul></div></body></html>'), -- Invoice Document
	(@cultureEN, 103, N'Ohtheplaces_Agreement_{{ConfirmationCode}}', N'<html> <head> <link href="http://fonts.googleapis.com/css?family=Armata" rel="stylesheet" type="text/css"> <link href="http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400, 200, 300, 700" rel="stylesheet" type="text/css"> <style type="text/css"> .header { text-align: center; } body { font-family: Georgia; text-align: justify !important; } </style> </head> <body> <div> <p class="header"><b><span>Owner and Guest</span> Short Term Rental Agreement</b></p> </div> <p>Both parties agree to change the travel dates of Reservation {{ConfirmationCode}}: New arrival is {{CheckInDate}}, the new departure is {{CheckOutDate}}.</p> <p style="page-break-after: always"> <table style="width: 100%;"> <tbody> <tr> <td style="width: 15%;"></td> <td style="width: 30%; text-align: center;">Owner:</td> <td style="width: 5%;"></td> <td style="width: 30%; text-align: center;">Guest:</td> <td style="width: 20%;"></td> </tr> <tr> <td>Name (printed):</td> <td style="border-bottom: 1px solid Black; text-align: center;">{{OwnerFullName}}</td> <td></td> <td style="border-bottom: 1px solid Black; text-align: center;">{{GuestFullName}}</td> <td></td> </tr> <tr> <td>Initials:</td> <td style="border-bottom: 1px solid Black; text-align: center;">{{OwnerInitials}}</td> <td></td> <td style="border-bottom: 1px solid Black; text-align: center;">{{GuestInitials}}</td> <td></td> </tr> </tbody> </table> </p> </body> </html>') -- Agreement on reservation date change document

INSERT INTO [dbo].[CreditCardTypes] (Name, CreditCardIcon) VALUES ('VISA', 1), ('MASTERCARD', 2), ('AMEX', 3)

-- Transaction Codes
INSERT INTO [dbo].[TransactionCodes] ([TransactionCode], [TransactionTitle_i18n], [Comment_i18n]) VALUES
	(1, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Payment')), NULL),
	(2, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Due Payment')), NULL),
	(3, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Tax')), NULL),
	(4, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'Revenue')), NULL),
	(5, replace(@i18nXmlPattern, '#pair#', replace(@i18nXmlEnglishPair, '#phrase#', N'7 day discount')), NULL)
	
-- Task scheduler
DECLARE @summaryDate datetime = GETDATE()
DECLARE @storageCleanerDate datetime
DECLARE @securityDepositTakeDate datetime
DECLARE @nextHour datetime = dateadd(hour,1+datediff(hour,0,GETDATE()),0);

SET @summaryDate = DATEADD(day, 1, @summaryDate)
SET @summaryDate = DATETIMEFROMPARTS(YEAR(@summaryDate), MONTH(@summaryDate), DAY(@summaryDate), 0, 15, 0, 0)

SET @storageCleanerDate = DATETIMEFROMPARTS(YEAR(@summaryDate), MONTH(@summaryDate), DAY(@summaryDate), 5, 0, 0, 0)
SET @securityDepositTakeDate = DATETIMEFROMPARTS(YEAR(@summaryDate), MONTH(@summaryDate), DAY(@summaryDate), 6, 0, 0, 0)


INSERT INTO [dbo].[ScheduledTasks] ([ScheduledTaskType], [ScheduleType], [ScheduleIntervalType], [TimeInterval], [ScheduleTimestamp], [LastExecutionTime], [NextPlannedExecutionTime], [TaskCompleted]) VALUES 
    --Email task
	(0, 0, 0, 5, NULL, GETDATE(), GETDATE(), 0),
	--CreditCardRedact Task
    (1, 1, NULL, NULL, '2013-12-12 00:00:01.000', GETDATE(), GETDATE(), 0),
	--Reservation
    (2, 0, 1, 1, NULL, GETDATE(), GETDATE(), 0),
	--ReservationPayment
    (3, 0, 2, 1, NULL, GETDATE(), GETDATE(), 0),
	--EventLogSummary
    (4, 1, NULL, NULL, '2013-01-16 00:15:00.000', GETDATE(), @summaryDate, 0),
	--Session Db Cleaner
	(5, 0, 1, 59, NULL, GETDATE(), GETDATE(), 0),	
	-- Check In
	(6, 0, 2, 1, NULL, @nextHour, @nextHour, 0),
	-- Check Out
	(7, 0, 2, 1, NULL, @nextHour, @nextHour, 0),
	-- Sets of arrivals informations
	(8, 0, 2, 1, NULL, @nextHour, @nextHour, 0),
	-- Key drop off confirm
	(9, 0, 2, 1, NULL, @nextHour, @nextHour, 0),
	-- Key drop off alert
	(10, 0, 2, 1, NULL, @nextHour, @nextHour, 0),
	-- Key drop off second alert
	(11, 0, 2, 1, NULL, @nextHour, @nextHour, 0),
	-- Storage Cleaner
	(12, 1, NULL, NULL, '2013-02-25 05:00:00.000', GETDATE(), @storageCleanerDate, 0),
	-- SecurityDeposit aquire retry
	(13, 1, NULL, NULL, '2013-02-25 05:00:00.000', GETDATE(), @securityDepositTakeDate, 0)
	

-- Zip Code Ranges
INSERT [dbo].[ZipCodeRanges] ([ZipCodeStart], [ZipCodeEnd]) VALUES (N'00000', N'00100')
INSERT [dbo].[ZipCodeRanges] ([ZipCodeStart], [ZipCodeEnd]) VALUES (N'00101', N'00200')
INSERT [dbo].[ZipCodeRanges] ([ZipCodeStart], [ZipCodeEnd]) VALUES (N'00201', N'00300')
INSERT [dbo].[ZipCodeRanges] ([ZipCodeStart], [ZipCodeEnd]) VALUES (N'00301', N'00400')
INSERT [dbo].[ZipCodeRanges] ([ZipCodeStart], [ZipCodeEnd]) VALUES (N'00401', N'00500')
INSERT [dbo].[ZipCodeRanges] ([ZipCodeStart], [ZipCodeEnd]) VALUES (N'00501', N'00600')
INSERT [dbo].[ZipCodeRanges] ([ZipCodeStart], [ZipCodeEnd]) VALUES (N'00601', N'00700')
INSERT [dbo].[ZipCodeRanges] ([ZipCodeStart], [ZipCodeEnd]) VALUES (N'00701', N'00800')
INSERT [dbo].[ZipCodeRanges] ([ZipCodeStart], [ZipCodeEnd]) VALUES (N'00801', N'00900')
INSERT [dbo].[ZipCodeRanges] ([ZipCodeStart], [ZipCodeEnd]) VALUES (N'00901', N'99999')