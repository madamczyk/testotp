CREATE TABLE Affiliates ( 
	AffiliateId int NOT NULL,
	Name_i18n nvarchar(50) NOT NULL,
	BankName nvarchar(50) NOT NULL,
	RoutingNumber nvarchar(50) NOT NULL,
	AccountNumber nvarchar(50) NOT NULL,
	DefaultCommision decimal(10,2),
	Address1 nvarchar(255) NOT NULL,
	Address2 nvarchar(255),
	City nvarchar(50) NOT NULL,
	ZipCode nvarchar(50) NOT NULL,
	State nvarchar(50) NOT NULL,
	CountryId int NOT NULL
)
GO

CREATE TABLE Amenities ( 
	AmenityID int identity(1,1)  NOT NULL,
	AmenityCode nvarchar(255) NOT NULL,
	AmenityTitle_i18n xml NOT NULL,
	Description_i18n xml,
	AmenityGroupId int NOT NULL,
	Countable bit DEFAULT 0 NOT NULL
)
GO

CREATE TABLE AmenityGroup ( 
	AmenityGroupId int identity(1,1)  NOT NULL,
	AmenityIcon int NOT NULL,
	Name_i18n xml NOT NULL,
	Position int NOT NULL
)
GO

CREATE TABLE ApplianceGroup ( 
	ApplianceGroupId int identity(1,1)  NOT NULL,
	Name_i18n xml NOT NULL
)
GO

CREATE TABLE Appliances ( 
	ApplianceId int identity(1,1)  NOT NULL,
	Name_i18n xml NOT NULL,
	Type int NOT NULL,
	ApplianceGroupId int NOT NULL
)
GO

CREATE TABLE Attachments ( 
	AttachementId int identity(1,1)  NOT NULL,
	Content varbinary(max) NOT NULL,
	MessageId int NOT NULL,
	Type int NOT NULL,
	Title nvarchar(max)
)
GO

CREATE TABLE ContentUpdateRequest ( 
	ContentUpdateRequestId int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	Description nvarchar(max) NOT NULL,
	RequestDateTime datetime NOT NULL
)
GO

CREATE TABLE ContentUpdateRequestDetails ( 
	ContentUpdateRequestDetailId int identity(1,1)  NOT NULL,
	ContentUpdateRequestId int NOT NULL,
	ContentCode int NOT NULL,
	ContentValueXml xml,
	ContentValue nvarchar(max)
)
GO

CREATE TABLE CreditCardTypes ( 
	CreditCardTypeId int identity(1,1)  NOT NULL,
	Name nvarchar(50) NOT NULL,
	CreditCardIcon int NOT NULL
)
GO

CREATE TABLE CrmOperationLog ( 
	Id int identity(1,1)  NOT NULL,
	OperationType int NOT NULL,
	Message nvarchar(max),
	CrmRequest xml,
	CrmResponse xml,
	OwnerId int,
	RowDate datetime NOT NULL
)
GO

CREATE TABLE Destinations ( 
	DestinationID int identity(1,1)  NOT NULL,
	Destination_i18n xml NOT NULL,
	Description_i18n xml NOT NULL,
	Active bit NOT NULL
)
GO

CREATE TABLE DictionaryCountries ( 
	CountryId int identity(1,1)  NOT NULL,
	CountryName_i18n xml NOT NULL,
	CountryCode2Letters nvarchar(2) NOT NULL,
	CountryCode3Letters nvarchar(3) NOT NULL
)
GO

CREATE TABLE DictionaryCultures ( 
	CultureId int identity(1,1)  NOT NULL,
	CultureCode nvarchar(15) NOT NULL,
	CultureName_i18n xml NOT NULL
)
GO

CREATE TABLE EventLog ( 
	Id int identity(1,1)  NOT NULL,
	CorrelationId uniqueidentifier,
	Timestamp datetime NOT NULL,
	Category nvarchar(50) NOT NULL,
	Source nvarchar(50) NOT NULL,
	Message nvarchar(max) NOT NULL,
	RelatedObjectId int
)
GO

CREATE TABLE GuestPaymentMethods ( 
	GuestPaymentMethodID int identity(1,1)  NOT NULL,
	GuestID int,
	CreditCardTypeId int NOT NULL,
	CreditCardLast4Digits nvarchar(255) NOT NULL,
	ReferenceToken nvarchar(255)
)
GO

CREATE TABLE GuestReviews ( 
	GuestReviewID int identity(1,1)  NOT NULL,
	GuestID int NOT NULL,
	PropertyID int NOT NULL,
	ReservationID int NOT NULL,
	ReviewTitle nvarchar(max) NOT NULL,
	ReviewContent nvarchar(max),
	ReviewDate datetime NOT NULL,
	Satisfaction_Experience int NOT NULL,
	Satisfaction_Cleanliness int NOT NULL,
	Satisfaction_Location int NOT NULL
)
GO

CREATE TABLE IdentityVerificationDetails ( 
	IdentityVerificationDetailsID int identity(1,1)  NOT NULL,
	VerificationDate datetime NOT NULL,
	VerificationPositive bit NOT NULL,
	VerificationDetails xml,
	UserID int NOT NULL
)
GO

CREATE TABLE IntranetSiteMapActions ( 
	Id int identity(1,1)  NOT NULL,
	DisplayName_i18n xml NOT NULL,
	Description_i18n xml,
	Url nvarchar(100),
	ParentActionId int
)
GO

CREATE TABLE ManagerZipCodeRanges ( 
	ManagerZipCodeRangeId int identity(1,1)  NOT NULL,
	ZipCodeRangeId int NOT NULL,
	ManagerRoleId int NOT NULL
)
GO

CREATE TABLE Messages ( 
	MessageId int identity(1,1)  NOT NULL,
	Recipients nvarchar(255) NOT NULL,
	CC nvarchar(255),
	BCC nvarchar(255),
	Subject nvarchar(255) NOT NULL,
	Status int NOT NULL,
	Created datetime NOT NULL,
	CompleteDate datetime,
	SendAttempts int DEFAULT 0 NOT NULL,
	ReplyTo nvarchar(255),
	Body ntext NOT NULL,
	Type int NOT NULL,
	DisplayFrom nvarchar(255) NOT NULL,
	AddressFrom nvarchar(255) NOT NULL
)
GO

CREATE TABLE OTP_Settings ( 
	OTP_SettingID int identity(1,1)  NOT NULL,
	SettingCode nvarchar(255),
	SettingValue ntext
)
GO

CREATE TABLE PaymentGatewayOperationsLog ( 
	Id int identity(1,1)  NOT NULL,
	OperationType int NOT NULL,
	Message nvarchar(max),
	GatewayResponse xml,
	ReservationId int,
	RowDate datetime DEFAULT GETDATE() NOT NULL
)
GO

CREATE TABLE Properties ( 
	PropertyID int identity(1,1)  NOT NULL,
	OwnerID int NOT NULL,
	OTP_DestinationID int NOT NULL,
	PropertyCode nvarchar(255),
	PropertyName_i18n xml NOT NULL,
	Short_Description_i18n xml NOT NULL,
	Long_Description_i18n xml NOT NULL,
	Address1 nvarchar(255) NOT NULL,
	Address2 nvarchar(255),
	State nvarchar(255) NOT NULL,
	ZIPCode nvarchar(255) NOT NULL,
	City nvarchar(255) NOT NULL,
	CountryId int NOT NULL,
	Latitude decimal(9,6) NOT NULL,
	Longitude decimal(9,6) NOT NULL,
	Altitude decimal(10,2),
	PropertyLive bit NOT NULL,
	StandardCommission decimal(10,2) NOT NULL,
	PropertyTypeId int,
	CheckIn time(7) NOT NULL,
	CheckOut time(7) NOT NULL,
	CultureId int NOT NULL,
	StarRating int DEFAULT 1 NOT NULL,
	Created datetime DEFAULT GETDATE(),
	SalesPersonId int,
	TimeZone nvarchar(50) NOT NULL,
	OverrideOwnerAccountData bit DEFAULT 0 NOT NULL,
	AccountNumber nvarchar(50),
	RoutingNumber nvarchar(50),
	BankName nvarchar(50),
	KeyProcessType int DEFAULT 0 NOT NULL,
	KeyHandlerInformation nvarchar(300),
	AffiliateId int,
	SecurityDeposit decimal(10,2),
	SignOffCode nvarchar(255)
)
GO

CREATE TABLE PropertyAddOns ( 
	PropertyAddOnID int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	AddOnTitle_i18n xml NOT NULL,
	AddOnDescrption_i18n xml NOT NULL,
	Unit int NOT NULL,
	Price money NOT NULL,
	IsMandatory bit NOT NULL,
	TaxId int NOT NULL
)
GO

CREATE TABLE PropertyAmenities ( 
	PropertyAmenityID int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	AmenityID int NOT NULL,
	Quantity int,
	Details_i18n xml
)
GO

CREATE TABLE PropertyAssignedManagers ( 
	PropertyAssignedManagerId int identity(1,1)  NOT NULL,
	PropertyId int NOT NULL,
	ManagerRoleId int NOT NULL,
	ManagerId int NOT NULL
)
GO

CREATE TABLE PropertyCommissionOverride ( 
	PropertyInvCommID int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	DateFrom datetime NOT NULL,
	DateUntil datetime NOT NULL,
	CommissionOverride decimal(10,2) NOT NULL
)
GO

CREATE TABLE PropertyRawData ( 
	PropertyRawDataRecordID int identity(1,1)  NOT NULL,
	PropertyId int,
	RawRecordType int NOT NULL,
	RecordName nvarchar(50) NOT NULL,
	Description nvarchar(max) NOT NULL,
	RawRecordContent nvarchar(max) NOT NULL
)
GO

CREATE TABLE PropertyServiceProviders ( 
	PropertyServiceProviderID int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	ServiceProviderId int NOT NULL,
	PointOfContact nvarchar(255),
	email nvarchar(255),
	CellPhone nvarchar(255)
)
GO

CREATE TABLE PropertySettings ( 
	PropertySettingID int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	SettingCode nvarchar(255),
	SettingValue nvarchar(255)
)
GO

CREATE TABLE PropertyStaticContent ( 
	PropertyStaticContent int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	ContentCode int NOT NULL,
	ContentValue_i18n xml,
	ContentValue nvarchar(max)
)
GO

CREATE TABLE PropertySupportedLanguages ( 
	SupportedLanguagesID int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	CultureId int NOT NULL
)
GO

CREATE TABLE PropertyTags ( 
	PropertyTagID int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	TagID int NOT NULL
)
GO

CREATE TABLE PropertyType ( 
	PropertyTypeId int identity(1,1)  NOT NULL,
	Name_i18n xml NOT NULL
)
GO

CREATE TABLE ReservationBillingAddresses ( 
	Id int identity(1,1)  NOT NULL,
	UserId int NOT NULL,
	ReservationId int NOT NULL,
	Firstname nvarchar(50) NOT NULL,
	Lastname nvarchar(50) NOT NULL,
	Street nvarchar(50) NOT NULL,
	City nvarchar(50) NOT NULL,
	State nvarchar(50) NOT NULL,
	Zip nvarchar(50) NOT NULL,
	CountryId int NOT NULL
)
GO

CREATE TABLE ReservationDetails ( 
	ResvDetailID int identity(1,1)  NOT NULL,
	ReservationID int,
	Sequence int,
	Type int,
	Caption_i18n xml,
	StartDate datetime,
	EndDate datetime,
	BasePrice money,
	BasePurchasePrice decimal(10,2),
	Base int,
	LinePrice money,
	LinePurchasePrice decimal(10,2),
	ServiceProviderProductId int
)
GO

CREATE TABLE Reservations ( 
	ReservationID int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	UnitID int NOT NULL,
	UnitTypeId int NOT NULL,
	GuestID int NOT NULL,
	ConfirmationID char(6),
	BookingDate datetime NOT NULL,
	BookingStatus int NOT NULL,
	DateArrival datetime NOT NULL,
	DateDeparture datetime NOT NULL,
	TotalPrice money NOT NULL,
	ReservationCultureId int NOT NULL,
	TripBalance decimal(10,2) NOT NULL,
	Agreement varbinary(max),
	GuestPaymentMethodID int,
	NumberOfGuests int,
	Invoice varbinary(max),
	TransactionFee decimal(10,2) NOT NULL,
	TransactionToken nvarchar(max),
	UpfrontPaymentProcessed bit,
	ParentReservationId int,
	EstimatedArrivalTime int,
	GuestConfirmedKeyDropOff bit,
	KeysDeliveredToGuestDateTime datetime,
	KeysReceivedFromGuestDateTime datetime,
	LinkAuthorizationToken nvarchar(255) NOT NULL,
	AffiliateCode nvarchar(50),
	TakeSecurityDeposit bit,
	SecurityDeposit decimal(10,2),
	SecurityDepositToken nvarchar(max),
	CapturedSecurityDeposit decimal(10,2)
)
GO

CREATE TABLE ReservationServiceProviderProducts ( 
	ReservationServiceProviderProductId bigint NOT NULL,
	ProductId int NOT NULL,
	ReservationId int NOT NULL,
	ProductInstanceDetails xml NOT NULL
)
GO

CREATE TABLE ReservationTransactions ( 
	ResvTransactionID int identity(1,1)  NOT NULL,
	ReservationID int NOT NULL,
	ResvDetailID int,
	Charge bit DEFAULT ((0)) NOT NULL,
	Payment bit DEFAULT ((0)) NOT NULL,
	TransactionDate datetime NOT NULL,
	Caption_i18n xml NOT NULL,
	TransactionCodeId int NOT NULL,
	ReferenceDetails nvarchar(255),
	Amount money NOT NULL
)
GO

CREATE TABLE RoleIntranetSiteMapActions ( 
	Id int identity(1,1)  NOT NULL,
	RoleId int NOT NULL,
	IntranetSiteMapActionId int NOT NULL
)
GO

CREATE TABLE Roles ( 
	RoleId int identity(1,1)  NOT NULL,
	Name nvarchar(50) NOT NULL,
	RoleLevel int NOT NULL
)
GO

CREATE TABLE SalesPersons ( 
	SalesPersonId int identity(1,1)  NOT NULL,
	FirstName nvarchar(255) NOT NULL,
	LastName nvarchar(255) NOT NULL,
	CommisionDate datetime NOT NULL,
	Percentage decimal(10,2) NOT NULL
)
GO

CREATE TABLE ScheduledTasks ( 
	ScheduledTaskId int identity(1,1)  NOT NULL,
	ScheduledTaskType int NOT NULL,
	ScheduleType int NOT NULL,
	ScheduleIntervalType int,
	TimeInterval int,
	ScheduleTimestamp datetime,
	LastExecutionTime datetime,
	NextPlannedExecutionTime datetime,
	TaskCompleted bit NOT NULL
)
GO

CREATE TABLE ScheduledVisits ( 
	VisitId int identity(1,1)  NOT NULL,
	OwnerId int NOT NULL,
	Dates xml NOT NULL,
	HourFrom int NOT NULL,
	HourUntil int NOT NULL,
	DateCreated datetime NOT NULL
)
GO

CREATE TABLE SecurityDeposits ( 
	SecurityDepositId int identity(1,1)  NOT NULL,
	CultureId int NOT NULL,
	Amount decimal(10,2) NOT NULL
)
GO

CREATE TABLE ServiceProviderProducts ( 
	ProductId int NOT NULL,
	ProductName_i18n xml NOT NULL,
	ProductDescription_i18n xml NOT NULL,
	ProductSellingPrice decimal(10,2) NOT NULL,
	ProductPurchasePrice decimal(10,2) NOT NULL,
	UnitOfMeasure int NOT NULL,
	ServiceProviderId int NOT NULL,
	IsPreselected bit NOT NULL,
	UrlToTcs nvarchar(100) NOT NULL,
	ProductWorkflowType int NOT NULL,
	BookableAfterCheckin bit NOT NULL,
	ReplacesSecurityDeposit bit NOT NULL
)
GO

CREATE TABLE ServiceProviders ( 
	ServiceProviderId int identity(1,1)  NOT NULL,
	ServiceProviderTypeId int NOT NULL,
	ServiceProviderName nvarchar(50) NOT NULL,
	PointOfContact nvarchar(255) NOT NULL,
	Email nvarchar(50) NOT NULL,
	CellPhone nvarchar(50) NOT NULL,
	Caption_i18n xml NOT NULL,
	Description_i18n xml NOT NULL
)
GO

CREATE TABLE ServiceProvidersDestinations ( 
	ServiceProviderDestinationId int identity(1,1)  NOT NULL,
	ServiceProviderProductId int NOT NULL,
	DestinationId int NOT NULL
)
GO

CREATE TABLE ServiceProviderTypes ( 
	ServiceProviderTypeID int identity(1,1)  NOT NULL,
	ServiceProviderType nvarchar(50) NOT NULL
)
GO

CREATE TABLE TagGroup ( 
	TagGroupId int identity(1,1)  NOT NULL,
	Name_i18n xml NOT NULL,
	Position int NOT NULL
)
GO

CREATE TABLE Tags ( 
	TagID int identity(1,1)  NOT NULL,
	Tag_i18n xml NOT NULL,
	TagGroupId int NOT NULL
)
GO

CREATE TABLE TaskInstanceDetails ( 
	Id int identity(1,1)  NOT NULL,
	Category int,
	Message nvarchar(max),
	TaskInstanceId int NOT NULL
)
GO

CREATE TABLE TaskInstances ( 
	Id int identity(1,1)  NOT NULL,
	ExecutionTimestamp datetime NOT NULL,
	ExecutionResult int,
	TaskDefinitionId int NOT NULL
)
GO

CREATE TABLE Taxes ( 
	TaxId int identity(1,1)  NOT NULL,
	DestinationId int NOT NULL,
	Name_i18n xml NOT NULL,
	Percentage decimal(10,2) NOT NULL
)
GO

CREATE TABLE Templates ( 
	TemplateId int identity(1,1)  NOT NULL,
	CultureId int NOT NULL,
	Type int NOT NULL,
	Title nvarchar(max),
	Content nvarchar(max) NOT NULL
)
GO

CREATE TABLE TransactionCodes ( 
	TransactionCodeID int identity(1,1)  NOT NULL,
	TransactionCode int,
	TransactionTitle_i18n xml,
	Comment_i18n xml
)
GO

CREATE TABLE UnitAppliancies ( 
	UnitApplianceId int identity(1,1)  NOT NULL,
	ApplianceId int NOT NULL,
	UnitId int NOT NULL,
	Quantity int
)
GO

CREATE TABLE UnitInvBlockings ( 
	UnitInvBlockingID int identity(1,1)  NOT NULL,
	UnitID int NOT NULL,
	DateFrom datetime NOT NULL,
	DateUntil datetime NOT NULL,
	Type int NOT NULL,
	ReservationID int,
	Comment nvarchar(max)
)
GO

CREATE TABLE UnitRates ( 
	UnitRateID int identity(1,1)  NOT NULL,
	UnitID int NOT NULL,
	DateFrom datetime NOT NULL,
	DateUntil datetime NOT NULL,
	DailyRate money NOT NULL
)
GO

CREATE TABLE Units ( 
	UnitID int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	UnitTypeID int NOT NULL,
	UnitCode nvarchar(255) NOT NULL,
	UnitTitle_i18n xml NOT NULL,
	UnitDesc_i18n xml NOT NULL,
	CleaningStatus int NOT NULL,
	MaxNumberOfGuests int NOT NULL,
	MaxNumberOfBedRooms int NOT NULL,
	MaxNumberOfBathrooms int NOT NULL,
	KeysLockboxCode nvarchar(20),
	KeysLockboxLocation nvarchar(100),
	AlarmCode nvarchar(50),
	AlarmLocation nvarchar(300),
	OtherAccessInformation nvarchar(300),
	Created datetime DEFAULT GETDATE()
)
GO

CREATE TABLE UnitStaticContent ( 
	UnitStaticContent int identity(1,1)  NOT NULL,
	UnitId int NOT NULL,
	ContentCode int NOT NULL,
	ContentValue_i18n xml,
	ContentValue nvarchar(max)
)
GO

CREATE TABLE UnitType7dayDiscounts ( 
	UnitType7dayID int identity(1,1)  NOT NULL,
	UnitTypeID int NOT NULL,
	DateFrom datetime NOT NULL,
	DateUntil datetime NOT NULL,
	Discount decimal(10,2) NOT NULL
)
GO

CREATE TABLE UnitTypeCTA ( 
	UnitTypeCTAID int identity(1,1)  NOT NULL,
	UnitTypeID int NOT NULL,
	DateFrom datetime NOT NULL,
	DateUntil datetime NOT NULL,
	Monday bit NOT NULL,
	Tuesday bit NOT NULL,
	Wednesday bit NOT NULL,
	Thursday bit NOT NULL,
	Friday bit NOT NULL,
	Saturday bit NOT NULL,
	Sunday bit NOT NULL
)
GO

CREATE TABLE UnitTypeMLOS ( 
	UnitTypeMLOSID int identity(1,1)  NOT NULL,
	UnitTypeID int NOT NULL,
	DateFrom datetime NOT NULL,
	DateUntil datetime NOT NULL,
	MLOS int NOT NULL
)
GO

CREATE TABLE UnitTypes ( 
	UnitTypeID int identity(1,1)  NOT NULL,
	PropertyID int NOT NULL,
	UnitTypeCode nvarchar(255) NOT NULL,
	UnitTypeTitle_i18n xml NOT NULL,
	UnitTypeDesc_i18n xml NOT NULL,
	Created datetime DEFAULT GETDATE()
)
GO

CREATE TABLE UserRoles ( 
	UserRoleId int identity(1,1)  NOT NULL,
	UserId int NOT NULL,
	RoleId int NOT NULL,
	Status int NOT NULL,
	ActivationToken nvarchar(255)
)
GO

CREATE TABLE Users ( 
	UserID int identity(1,1)  NOT NULL,
	Lastname nvarchar(255) NOT NULL,
	Firstname nvarchar(255) NOT NULL,
	Address1 nvarchar(255),
	Address2 nvarchar(255),
	State nvarchar(255),
	ZIPCode nvarchar(255),
	City nvarchar(255),
	CountryId int,
	CellPhone nvarchar(255),
	LandLine nvarchar(255),
	email nvarchar(255) NOT NULL,
	DriverLicenseNbr nvarchar(255),
	PassportNbr nvarchar(255),
	SocialsecurityNbr nvarchar(255),
	DirectDepositInfo nvarchar(255),
	SecurityQuestion nvarchar(255),
	SecurityAnswer nvarchar(255),
	AcceptedTCs datetime,
	AcceptedTCsInitials nvarchar(50),
	Password nvarchar(50) NOT NULL,
	IsGeneratedPassword bit DEFAULT 0 NOT NULL,
	VerificationPositive bit,
	SendMePromotions bit,
	SendInfoFavoritePlaces bit,
	SendNews bit,
	Gender char(1),
	BirthDate date,
	BankName nvarchar(max),
	RoutingNumber nvarchar(max),
	AccountNumber nvarchar(max),
	CultureId int NOT NULL,
	SignUpCode nvarchar(255)
)
GO

CREATE TABLE ZipCodeRanges ( 
	ZipCodeRangeId int identity(1,1)  NOT NULL,
	ZipCodeStart nvarchar(50) NOT NULL,
	ZipCodeEnd nvarchar(50) NOT NULL
)
GO


ALTER TABLE SecurityDeposits
	ADD CONSTRAINT UQ_SecurityDeposits_CultureId UNIQUE (CultureId)
GO

ALTER TABLE Affiliates ADD CONSTRAINT PK_Affiliates 
	PRIMARY KEY CLUSTERED (AffiliateId)
GO

ALTER TABLE Amenities ADD CONSTRAINT Amenities_PK 
	PRIMARY KEY CLUSTERED (AmenityID)
GO

ALTER TABLE AmenityGroup ADD CONSTRAINT PK_AmenityGroup 
	PRIMARY KEY CLUSTERED (AmenityGroupId)
GO

ALTER TABLE ApplianceGroup ADD CONSTRAINT PK_ApplianceGroup 
	PRIMARY KEY CLUSTERED (ApplianceGroupId)
GO

ALTER TABLE Appliances ADD CONSTRAINT PK_Appliances 
	PRIMARY KEY CLUSTERED (ApplianceId)
GO

ALTER TABLE Attachments ADD CONSTRAINT PK_Attachments 
	PRIMARY KEY CLUSTERED (AttachementId)
GO

ALTER TABLE ContentUpdateRequest ADD CONSTRAINT ContentUpdateRequest_PK 
	PRIMARY KEY CLUSTERED (ContentUpdateRequestId)
GO

ALTER TABLE ContentUpdateRequestDetails ADD CONSTRAINT PK_ContentUpdateRequestDetails 
	PRIMARY KEY CLUSTERED (ContentUpdateRequestDetailId)
GO

ALTER TABLE CreditCardTypes ADD CONSTRAINT PK_CreditCardTypes 
	PRIMARY KEY CLUSTERED (CreditCardTypeId)
GO

ALTER TABLE CrmOperationLog ADD CONSTRAINT PK_CrmOperationLog 
	PRIMARY KEY CLUSTERED (Id)
GO

ALTER TABLE Destinations ADD CONSTRAINT Destinations_PK 
	PRIMARY KEY CLUSTERED (DestinationID)
GO

ALTER TABLE DictionaryCountries ADD CONSTRAINT PK_DictionaryCountries 
	PRIMARY KEY CLUSTERED (CountryId)
GO

ALTER TABLE DictionaryCultures ADD CONSTRAINT PK_DictionaryCultures 
	PRIMARY KEY CLUSTERED (CultureId)
GO

ALTER TABLE EventLog ADD CONSTRAINT PK_EventLog 
	PRIMARY KEY CLUSTERED (Id)
GO

ALTER TABLE GuestPaymentMethods ADD CONSTRAINT GuestPaymentMethods_PK 
	PRIMARY KEY CLUSTERED (GuestPaymentMethodID)
GO

ALTER TABLE GuestReviews ADD CONSTRAINT GuestReviews_PK 
	PRIMARY KEY CLUSTERED (GuestReviewID)
GO

ALTER TABLE IdentityVerificationDetails ADD CONSTRAINT PK_IdentityVerificationDetails 
	PRIMARY KEY CLUSTERED (IdentityVerificationDetailsID)
GO

ALTER TABLE IntranetSiteMapActions ADD CONSTRAINT PK_IntranetSiteMapActions 
	PRIMARY KEY CLUSTERED (Id)
GO

ALTER TABLE ManagerZipCodeRanges ADD CONSTRAINT PK_ManagerZipCodeRanges 
	PRIMARY KEY CLUSTERED (ManagerZipCodeRangeId)
GO

ALTER TABLE Messages ADD CONSTRAINT PK_Messages 
	PRIMARY KEY CLUSTERED (MessageId)
GO

ALTER TABLE OTP_Settings ADD CONSTRAINT OTP_Settings_PK 
	PRIMARY KEY CLUSTERED (OTP_SettingID)
GO

ALTER TABLE PaymentGatewayOperationsLog ADD CONSTRAINT PK_PaymentGatewayOperationsLog 
	PRIMARY KEY CLUSTERED (Id)
GO

ALTER TABLE Properties ADD CONSTRAINT PK_Properties 
	PRIMARY KEY CLUSTERED (PropertyID)
GO

ALTER TABLE PropertyAddOns ADD CONSTRAINT PropertyAddOns_PK 
	PRIMARY KEY CLUSTERED (PropertyAddOnID)
GO

ALTER TABLE PropertyAmenities ADD CONSTRAINT PropertyAmenities_PK 
	PRIMARY KEY CLUSTERED (PropertyAmenityID)
GO

ALTER TABLE PropertyAssignedManagers ADD CONSTRAINT PK_PropertyManagers 
	PRIMARY KEY CLUSTERED (PropertyAssignedManagerId)
GO

ALTER TABLE PropertyCommissionOverride ADD CONSTRAINT PropertyCommissionOverride_PK 
	PRIMARY KEY CLUSTERED (PropertyInvCommID)
GO

ALTER TABLE PropertyRawData ADD CONSTRAINT PK_PropertyRawData 
	PRIMARY KEY CLUSTERED (PropertyRawDataRecordID)
GO

ALTER TABLE PropertyServiceProviders ADD CONSTRAINT PropertyServiceProviders_PK 
	PRIMARY KEY CLUSTERED (PropertyServiceProviderID)
GO

ALTER TABLE PropertySettings ADD CONSTRAINT PropertySettings_PK 
	PRIMARY KEY CLUSTERED (PropertySettingID)
GO

ALTER TABLE PropertyStaticContent ADD CONSTRAINT PropertyStaticContent_PK 
	PRIMARY KEY CLUSTERED (PropertyStaticContent)
GO

ALTER TABLE PropertySupportedLanguages ADD CONSTRAINT PropertySupportedLanguages_PK 
	PRIMARY KEY CLUSTERED (SupportedLanguagesID)
GO

ALTER TABLE PropertyTags ADD CONSTRAINT PropertyTags_PK 
	PRIMARY KEY CLUSTERED (PropertyTagID)
GO

ALTER TABLE PropertyType ADD CONSTRAINT PK_PropertyType 
	PRIMARY KEY CLUSTERED (PropertyTypeId)
GO

ALTER TABLE ReservationBillingAddresses ADD CONSTRAINT PK_ReservationBillingAddresses 
	PRIMARY KEY CLUSTERED (Id)
GO

ALTER TABLE ReservationDetails ADD CONSTRAINT ReservationDetails_PK 
	PRIMARY KEY CLUSTERED (ResvDetailID)
GO

ALTER TABLE Reservations ADD CONSTRAINT Reservations_PK 
	PRIMARY KEY CLUSTERED (ReservationID)
GO

ALTER TABLE ReservationServiceProviderProducts ADD CONSTRAINT PK_ReservationServiceProviderProducts 
	PRIMARY KEY CLUSTERED (ReservationServiceProviderProductId)
GO

ALTER TABLE ReservationTransactions ADD CONSTRAINT ReservationTransactions_PK 
	PRIMARY KEY CLUSTERED (ResvTransactionID)
GO

ALTER TABLE RoleIntranetSiteMapActions ADD CONSTRAINT PK_RoleIntranetSiteMapActions 
	PRIMARY KEY CLUSTERED (Id)
GO

ALTER TABLE Roles ADD CONSTRAINT PK_Roles 
	PRIMARY KEY CLUSTERED (RoleId)
GO

ALTER TABLE SalesPersons ADD CONSTRAINT PK_SalesPeople 
	PRIMARY KEY CLUSTERED (SalesPersonId)
GO

ALTER TABLE ScheduledTasks ADD CONSTRAINT PK_ScheduledTasks 
	PRIMARY KEY CLUSTERED (ScheduledTaskId)
GO

ALTER TABLE ScheduledVisits ADD CONSTRAINT PK_ScheduledVisits 
	PRIMARY KEY CLUSTERED (VisitId)
GO

ALTER TABLE SecurityDeposits ADD CONSTRAINT PK_SecurityDeposits 
	PRIMARY KEY CLUSTERED (SecurityDepositId)
GO

ALTER TABLE ServiceProviderProducts ADD CONSTRAINT PK_ServiceProviderProducts 
	PRIMARY KEY CLUSTERED (ProductId)
GO

ALTER TABLE ServiceProviders ADD CONSTRAINT PK_ServiceProviders 
	PRIMARY KEY CLUSTERED (ServiceProviderId)
GO

ALTER TABLE ServiceProvidersDestinations ADD CONSTRAINT PK_ServiceProvidersDestinations 
	PRIMARY KEY CLUSTERED (ServiceProviderDestinationId)
GO

ALTER TABLE ServiceProviderTypes ADD CONSTRAINT ServiceProviderTypes_PK 
	PRIMARY KEY CLUSTERED (ServiceProviderTypeID)
GO

ALTER TABLE TagGroup ADD CONSTRAINT PK_TagGroup 
	PRIMARY KEY CLUSTERED (TagGroupId)
GO

ALTER TABLE Tags ADD CONSTRAINT Tags_PK 
	PRIMARY KEY CLUSTERED (TagID)
GO

ALTER TABLE TaskInstanceDetails ADD CONSTRAINT PK_TaskSchedulerEventLog 
	PRIMARY KEY CLUSTERED (Id)
GO

ALTER TABLE TaskInstances ADD CONSTRAINT PK_TaskInstances 
	PRIMARY KEY CLUSTERED (Id)
GO

ALTER TABLE Taxes ADD CONSTRAINT PK_Taxes 
	PRIMARY KEY CLUSTERED (TaxId)
GO

ALTER TABLE Templates ADD CONSTRAINT PK_Templates 
	PRIMARY KEY CLUSTERED (TemplateId)
GO

ALTER TABLE TransactionCodes ADD CONSTRAINT TransactionCodes_PK 
	PRIMARY KEY CLUSTERED (TransactionCodeID)
GO

ALTER TABLE UnitAppliancies ADD CONSTRAINT PK_UnitAppliancies 
	PRIMARY KEY CLUSTERED (UnitApplianceId)
GO

ALTER TABLE UnitInvBlockings ADD CONSTRAINT UnitInvBlockings_PK 
	PRIMARY KEY CLUSTERED (UnitInvBlockingID)
GO

ALTER TABLE UnitRates ADD CONSTRAINT UnitRates_PK 
	PRIMARY KEY CLUSTERED (UnitRateID)
GO

ALTER TABLE Units ADD CONSTRAINT Units_PK 
	PRIMARY KEY CLUSTERED (UnitID)
GO

ALTER TABLE UnitStaticContent ADD CONSTRAINT PK_UnitStaticContent 
	PRIMARY KEY CLUSTERED (UnitStaticContent)
GO

ALTER TABLE UnitType7dayDiscounts ADD CONSTRAINT UnitType7dayDiscounts_PK 
	PRIMARY KEY CLUSTERED (UnitType7dayID)
GO

ALTER TABLE UnitTypeCTA ADD CONSTRAINT UnitTypeCTA_PK 
	PRIMARY KEY CLUSTERED (UnitTypeCTAID)
GO

ALTER TABLE UnitTypeMLOS ADD CONSTRAINT UnitTypeMLOS_PK 
	PRIMARY KEY CLUSTERED (UnitTypeMLOSID)
GO

ALTER TABLE UnitTypes ADD CONSTRAINT UnitTypes_PK 
	PRIMARY KEY CLUSTERED (UnitTypeID)
GO

ALTER TABLE UserRoles ADD CONSTRAINT PK_UserRoles 
	PRIMARY KEY CLUSTERED (UserRoleId)
GO

ALTER TABLE Users ADD CONSTRAINT Users_PK 
	PRIMARY KEY CLUSTERED (UserID)
GO

ALTER TABLE ZipCodeRanges ADD CONSTRAINT PK_ManagersZipCodeRanges 
	PRIMARY KEY CLUSTERED (ZipCodeRangeId)
GO



ALTER TABLE Affiliates ADD CONSTRAINT FK_Affiliates_DictionaryCountries 
	FOREIGN KEY (CountryId) REFERENCES DictionaryCountries (CountryId)
GO

ALTER TABLE Amenities ADD CONSTRAINT FK_Amenities_AmenityGroup 
	FOREIGN KEY (AmenityGroupId) REFERENCES AmenityGroup (AmenityGroupId)
GO

ALTER TABLE Appliances ADD CONSTRAINT FK_Appliances_ApplianceGroup 
	FOREIGN KEY (ApplianceGroupId) REFERENCES ApplianceGroup (ApplianceGroupId)
GO

ALTER TABLE Attachments ADD CONSTRAINT FK_MessageId 
	FOREIGN KEY (MessageId) REFERENCES Messages (MessageId)
GO

ALTER TABLE ContentUpdateRequest ADD CONSTRAINT FK_ContentUpdateRequest_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE ContentUpdateRequestDetails ADD CONSTRAINT FK_ContentUpdateRequestDetails_ContentUpdateRequest 
	FOREIGN KEY (ContentUpdateRequestId) REFERENCES ContentUpdateRequest (ContentUpdateRequestId)
	ON DELETE CASCADE
GO

ALTER TABLE CrmOperationLog ADD CONSTRAINT FK_CrmOperationLog_Users 
	FOREIGN KEY (OwnerId) REFERENCES Users (UserID)
	ON DELETE SET NULL
GO

ALTER TABLE GuestPaymentMethods ADD CONSTRAINT FK_GuestPaymentMethods_CreditCardTypes 
	FOREIGN KEY (CreditCardTypeId) REFERENCES CreditCardTypes (CreditCardTypeId)
GO

ALTER TABLE GuestPaymentMethods ADD CONSTRAINT FK_GuestPaymentMethods_Users 
	FOREIGN KEY (GuestID) REFERENCES Users (UserID)
	ON DELETE CASCADE
GO

ALTER TABLE GuestReviews ADD CONSTRAINT FK_GuestReviews_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE GuestReviews ADD CONSTRAINT FK_GuestReviews_Guest 
	FOREIGN KEY (GuestID) REFERENCES Users (UserID)
GO

ALTER TABLE GuestReviews ADD CONSTRAINT FK_GuestReviews_Reservations 
	FOREIGN KEY (ReservationID) REFERENCES Reservations (ReservationID)
	ON DELETE CASCADE
GO

ALTER TABLE IdentityVerificationDetails ADD CONSTRAINT FK_IdentityVerificationDetails_Users 
	FOREIGN KEY (UserID) REFERENCES Users (UserID)
	ON DELETE CASCADE
GO

ALTER TABLE IntranetSiteMapActions ADD CONSTRAINT FK_IntranetSiteMapActions_IntranetSiteMapActions 
	FOREIGN KEY (ParentActionId) REFERENCES IntranetSiteMapActions (Id)
GO

ALTER TABLE ManagerZipCodeRanges ADD CONSTRAINT FK_ManagerZipCodeRanges_UserRoles 
	FOREIGN KEY (ManagerRoleId) REFERENCES UserRoles (UserRoleId)
GO

ALTER TABLE ManagerZipCodeRanges ADD CONSTRAINT FK_ManagerZipCodeRanges_ZipCodeRanges 
	FOREIGN KEY (ZipCodeRangeId) REFERENCES ZipCodeRanges (ZipCodeRangeId)
GO

ALTER TABLE PaymentGatewayOperationsLog ADD CONSTRAINT FK_PaymentGatewayOperationsLog_Reservations 
	FOREIGN KEY (ReservationId) REFERENCES Reservations (ReservationID)
GO

ALTER TABLE Properties ADD CONSTRAINT FK_Properties_Affiliates 
	FOREIGN KEY (AffiliateId) REFERENCES Affiliates (AffiliateId)
GO

ALTER TABLE Properties ADD CONSTRAINT FK_Properties_DictionaryCountries 
	FOREIGN KEY (CountryId) REFERENCES DictionaryCountries (CountryId)
GO

ALTER TABLE Properties ADD CONSTRAINT FK_Properties_SalesPersons 
	FOREIGN KEY (SalesPersonId) REFERENCES SalesPersons (SalesPersonId)
	ON DELETE SET NULL
GO

ALTER TABLE Properties ADD CONSTRAINT FK_Properties_Users 
	FOREIGN KEY (OwnerID) REFERENCES Users (UserID)
GO

ALTER TABLE Properties ADD CONSTRAINT FK_Properties_DictionaryCultures 
	FOREIGN KEY (CultureId) REFERENCES DictionaryCultures (CultureId)
GO

ALTER TABLE Properties ADD CONSTRAINT FK_Properties_PropertyType 
	FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType (PropertyTypeId)
GO

ALTER TABLE Properties ADD CONSTRAINT FK_Properties_Destinations 
	FOREIGN KEY (OTP_DestinationID) REFERENCES Destinations (DestinationID)
GO

ALTER TABLE PropertyAddOns ADD CONSTRAINT FK_PropertyAddOns_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE PropertyAddOns ADD CONSTRAINT FK_PropertyAddOns_Taxes 
	FOREIGN KEY (TaxId) REFERENCES Taxes (TaxId)
GO

ALTER TABLE PropertyAmenities ADD CONSTRAINT FK_PropertyAmenities_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE PropertyAmenities ADD CONSTRAINT FK_PropertyAmenities_Amenities 
	FOREIGN KEY (AmenityID) REFERENCES Amenities (AmenityID)
	ON DELETE CASCADE
GO

ALTER TABLE PropertyAssignedManagers ADD CONSTRAINT FK_PropertyAssignedManagers_Properties 
	FOREIGN KEY (PropertyId) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE PropertyAssignedManagers ADD CONSTRAINT FK_PropertyAssignedManagers_Roles 
	FOREIGN KEY (ManagerRoleId) REFERENCES Roles (RoleId)
GO

ALTER TABLE PropertyAssignedManagers ADD CONSTRAINT FK_PropertyAssignedManagers_Users 
	FOREIGN KEY (ManagerId) REFERENCES Users (UserID)
GO

ALTER TABLE PropertyCommissionOverride ADD CONSTRAINT FK_PropertyCommissionOverride_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE PropertyRawData ADD CONSTRAINT FK_PropertyRawData_Properties 
	FOREIGN KEY (PropertyId) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE PropertyServiceProviders ADD CONSTRAINT FK_PropertyServiceProviders_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE PropertyServiceProviders ADD CONSTRAINT FK_PropertyServiceProviders_ServiceProviders 
	FOREIGN KEY (ServiceProviderId) REFERENCES ServiceProviders (ServiceProviderId)
	ON DELETE CASCADE
GO

ALTER TABLE PropertySettings ADD CONSTRAINT FK_PropertySettings_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE PropertyStaticContent ADD CONSTRAINT FK_PropertyStaticContent_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE PropertySupportedLanguages ADD CONSTRAINT FK_PropertySupportedLanguages_DictionaryCultures 
	FOREIGN KEY (CultureId) REFERENCES DictionaryCultures (CultureId)
GO

ALTER TABLE PropertySupportedLanguages ADD CONSTRAINT FK_PropertySupportedLanguages_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
	ON DELETE NO ACTION
GO

ALTER TABLE PropertyTags ADD CONSTRAINT FK_PropertyTags_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
	ON DELETE CASCADE
GO

ALTER TABLE PropertyTags ADD CONSTRAINT FK_PropertyTags_Tags 
	FOREIGN KEY (TagID) REFERENCES Tags (TagID)
	ON DELETE CASCADE
GO

ALTER TABLE ReservationBillingAddresses ADD CONSTRAINT FK_ReservationBillingAddresses_DictionaryCountries 
	FOREIGN KEY (CountryId) REFERENCES DictionaryCountries (CountryId)
GO

ALTER TABLE ReservationBillingAddresses ADD CONSTRAINT FK_ReservationBillingAddresses_Reservations 
	FOREIGN KEY (ReservationId) REFERENCES Reservations (ReservationID)
GO

ALTER TABLE ReservationBillingAddresses ADD CONSTRAINT FK_ReservationBillingAddresses_Users 
	FOREIGN KEY (UserId) REFERENCES Users (UserID)
GO

ALTER TABLE ReservationDetails ADD CONSTRAINT FK_ReservationDetails_ServiceProviderProducts 
	FOREIGN KEY (ServiceProviderProductId) REFERENCES ServiceProviderProducts (ProductId)
GO

ALTER TABLE ReservationDetails ADD CONSTRAINT FK_ReservationDetails_Reservations 
	FOREIGN KEY (ReservationID) REFERENCES Reservations (ReservationID)
	ON DELETE CASCADE
GO

ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_DictionaryCultures 
	FOREIGN KEY (ReservationCultureId) REFERENCES DictionaryCultures (CultureId)
GO

ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_GuestPaymentMethods 
	FOREIGN KEY (GuestPaymentMethodID) REFERENCES GuestPaymentMethods (GuestPaymentMethodID)
	ON DELETE SET NULL
GO

ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
GO

ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_UnitTypes 
	FOREIGN KEY (UnitTypeId) REFERENCES UnitTypes (UnitTypeID)
GO

ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_Users 
	FOREIGN KEY (GuestID) REFERENCES Users (UserID)
GO

ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_Units 
	FOREIGN KEY (UnitID) REFERENCES Units (UnitID)
GO

ALTER TABLE ReservationServiceProviderProducts ADD CONSTRAINT FK_ReservationServiceProviderProducts_Reservations 
	FOREIGN KEY (ReservationId) REFERENCES Reservations (ReservationID)
GO

ALTER TABLE ReservationServiceProviderProducts ADD CONSTRAINT FK_ReservationServiceProviderProducts_ServiceProviderProducts 
	FOREIGN KEY (ProductId) REFERENCES ServiceProviderProducts (ProductId)
GO

ALTER TABLE ReservationTransactions ADD CONSTRAINT FK_ReservationTransactions_ReservationDetails 
	FOREIGN KEY (ResvDetailID) REFERENCES ReservationDetails (ResvDetailID)
GO

ALTER TABLE ReservationTransactions ADD CONSTRAINT FK_ReservationTransactions_Reservation 
	FOREIGN KEY (ReservationID) REFERENCES Reservations (ReservationID)
	ON DELETE CASCADE
GO

ALTER TABLE ReservationTransactions ADD CONSTRAINT FK_ReservationTransactions_TransactionCodes 
	FOREIGN KEY (TransactionCodeId) REFERENCES TransactionCodes (TransactionCodeID)
GO

ALTER TABLE RoleIntranetSiteMapActions ADD CONSTRAINT FK_RoleIntranetSiteMapActions_IntranetSiteMapActions 
	FOREIGN KEY (IntranetSiteMapActionId) REFERENCES IntranetSiteMapActions (Id)
GO

ALTER TABLE RoleIntranetSiteMapActions ADD CONSTRAINT FK_RoleIntranetSiteMapActions_Roles 
	FOREIGN KEY (RoleId) REFERENCES Roles (RoleId)
GO

ALTER TABLE ScheduledVisits ADD CONSTRAINT FK_ScheduledVisits_Users 
	FOREIGN KEY (OwnerId) REFERENCES Users (UserID)
	ON DELETE CASCADE
GO

ALTER TABLE SecurityDeposits ADD CONSTRAINT FK_SecurityDeposits_DictionaryCultures 
	FOREIGN KEY (CultureId) REFERENCES DictionaryCultures (CultureId)
GO

ALTER TABLE ServiceProviderProducts ADD CONSTRAINT FK_ServiceProviderProducts_ServiceProviders 
	FOREIGN KEY (ServiceProviderId) REFERENCES ServiceProviders (ServiceProviderId)
GO

ALTER TABLE ServiceProviders ADD CONSTRAINT FK_ServiceProviders_ServiceProviderTypes 
	FOREIGN KEY (ServiceProviderTypeId) REFERENCES ServiceProviderTypes (ServiceProviderTypeID)
GO

ALTER TABLE ServiceProvidersDestinations ADD CONSTRAINT FK_ServiceProvidersDestinations_OTP_Destinations 
	FOREIGN KEY (DestinationId) REFERENCES Destinations (DestinationID)
	ON DELETE CASCADE
GO

ALTER TABLE ServiceProvidersDestinations ADD CONSTRAINT FK_ServiceProvidersDestinations_ServiceProviderProducts 
	FOREIGN KEY (ServiceProviderProductId) REFERENCES ServiceProviderProducts (ProductId)
GO

ALTER TABLE Tags ADD CONSTRAINT FK_Tags_TagGroup 
	FOREIGN KEY (TagGroupId) REFERENCES TagGroup (TagGroupId)
GO

ALTER TABLE TaskInstanceDetails ADD CONSTRAINT FK_TaskInstanceDetails_TaskInstances 
	FOREIGN KEY (TaskInstanceId) REFERENCES TaskInstances (Id)
GO

ALTER TABLE TaskInstances ADD CONSTRAINT FK_TaskInstances_ScheduledTasks 
	FOREIGN KEY (TaskDefinitionId) REFERENCES ScheduledTasks (ScheduledTaskId)
GO

ALTER TABLE Taxes ADD CONSTRAINT FK_Taxes_OTP_Destinations 
	FOREIGN KEY (DestinationId) REFERENCES Destinations (DestinationID)
GO

ALTER TABLE Templates ADD CONSTRAINT FK_Templates_DictionaryCultures 
	FOREIGN KEY (CultureId) REFERENCES DictionaryCultures (CultureId)
GO

ALTER TABLE UnitAppliancies ADD CONSTRAINT FK_UnitAppliancies_Appliances 
	FOREIGN KEY (ApplianceId) REFERENCES Appliances (ApplianceId)
	ON DELETE CASCADE
GO

ALTER TABLE UnitAppliancies ADD CONSTRAINT FK_UnitAppliancies_Units 
	FOREIGN KEY (UnitId) REFERENCES Units (UnitID)
	ON DELETE CASCADE
GO

ALTER TABLE UnitInvBlockings ADD CONSTRAINT FK_UnitInvBlockings_Reservations 
	FOREIGN KEY (ReservationID) REFERENCES Reservations (ReservationID)
GO

ALTER TABLE UnitInvBlockings ADD CONSTRAINT FK_UnitInvBlockings_Units 
	FOREIGN KEY (UnitID) REFERENCES Units (UnitID)
	ON DELETE CASCADE
GO

ALTER TABLE UnitRates ADD CONSTRAINT FK_UnitRates_Units 
	FOREIGN KEY (UnitID) REFERENCES Units (UnitID)
	ON DELETE CASCADE
GO

ALTER TABLE Units ADD CONSTRAINT FK_Units_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
GO

ALTER TABLE Units ADD CONSTRAINT FK_Units_UnitTypes 
	FOREIGN KEY (UnitTypeID) REFERENCES UnitTypes (UnitTypeID)
GO

ALTER TABLE UnitStaticContent ADD CONSTRAINT FK_UnitStaticContent_Units 
	FOREIGN KEY (UnitId) REFERENCES Units (UnitID)
	ON DELETE CASCADE
GO

ALTER TABLE UnitType7dayDiscounts ADD CONSTRAINT FK_UnitType7dayDiscounts_UnitTypes 
	FOREIGN KEY (UnitTypeID) REFERENCES UnitTypes (UnitTypeID)
	ON DELETE CASCADE
GO

ALTER TABLE UnitTypeCTA ADD CONSTRAINT FK_UnitTypeCTA_UnitTypes 
	FOREIGN KEY (UnitTypeID) REFERENCES UnitTypes (UnitTypeID)
	ON DELETE CASCADE
GO

ALTER TABLE UnitTypeMLOS ADD CONSTRAINT FK_UnitTypeMLOS_UnitTypes 
	FOREIGN KEY (UnitTypeID) REFERENCES UnitTypes (UnitTypeID)
	ON DELETE CASCADE
GO

ALTER TABLE UnitTypes ADD CONSTRAINT FK_UnitTypes_Properties 
	FOREIGN KEY (PropertyID) REFERENCES Properties (PropertyID)
GO

ALTER TABLE UserRoles ADD CONSTRAINT FK_UserRoles_Roles 
	FOREIGN KEY (RoleId) REFERENCES Roles (RoleId)
	ON DELETE CASCADE
GO

ALTER TABLE UserRoles ADD CONSTRAINT FK_UserRoles_Users 
	FOREIGN KEY (UserId) REFERENCES Users (UserID)
	ON DELETE CASCADE
GO

ALTER TABLE Users ADD CONSTRAINT FK_User_DictionaryCultures 
	FOREIGN KEY (CultureId) REFERENCES DictionaryCultures (CultureId)
GO

ALTER TABLE Users ADD CONSTRAINT FK_Users_DictionaryCountries 
	FOREIGN KEY (CountryId) REFERENCES DictionaryCountries (CountryId)
GO
