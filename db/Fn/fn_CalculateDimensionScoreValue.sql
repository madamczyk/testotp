if exists(select * from sys.objects where type = 'fn' and name = 'fn_CalculateDimensionScoreValue' )
begin	
drop 
function 
fn_CalculateDimensionScoreValue
end
GO

CREATE FUNCTION [dbo].[fn_CalculateDimensionScoreValue](    @paramName NVARCHAR(MAX),	@X INT,	@Xs INT)RETURNS FLOATAS 	BEGIN		DECLARE @param_P INT		DECLARE @param_Q INT		DECLARE @weight INT			SET @param_P = dbo.fn_GetSettingValue('FuzzySearch.Parameters.' + @paramName + 'Min');		SET @param_Q = dbo.fn_GetSettingValue('FuzzySearch.Parameters.' + @paramName + 'Max');		SET @weight = dbo.fn_GetSettingValue('FuzzySearch.DimensionWeight.' + @paramName);		DECLARE @value FLOAT = dbo.fn_CalculateDimensionScore(@X, @param_P, @param_Q, @Xs)		DECLARE @returnValue FLOAT		SET @returnValue =  ((@weight * @value) / 100)		RETURN @returnValue    END
