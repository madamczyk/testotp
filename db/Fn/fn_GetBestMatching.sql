if exists(select * from sys.objects where type = 'fn' and name = 'fn_GetBestMatching' )
begin
	drop function fn_GetBestMatching
end
GO

CREATE FUNCTION [dbo].[fn_GetBestMatching]
(
    @unitId int,
	@guests int = NULL,
	@bedrooms int = NULL,
	@bathrooms int = NULL
)
RETURNS INT
AS 
BEGIN
	DECLARE @result int = 0;
	DECLARE @guests_S int = 0;
	DECLARE @baths_S int = 0;
	DECLARE @beds_S int = 0;

	Select 
		@guests_S = MaxNumberOfGuests,
		@baths_S = MaxNumberOfBathrooms,
		@beds_S = MaxNumberOfBedRooms
	FROM dbo.Units
	WHERE UnitID = @unitId

	SET @result = SQUARE(@guests_S - ISNULL(@guests, 0)) + SQUARE(@baths_S - ISNULL(@bathrooms, 0)) + SQUARE(@beds_S - ISNULL(@bedrooms, 0))

	RETURN @result;
END



