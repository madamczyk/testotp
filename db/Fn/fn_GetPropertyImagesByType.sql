if exists(select * from sys.objects where type = 'fn' and name = 'fn_GetPropertyImagesByType' )
begin
	drop function fn_GetPropertyImagesByType
end
GO

CREATE FUNCTION [dbo].[fn_GetPropertyImagesByType]
(
	@propertyId int,
	@imageType int
)
RETURNS NVARCHAR(MAX) 

AS 
	BEGIN
		DECLARE @images NVARCHAR(MAX) 

		SELECT	@images = COALESCE(@images + ',', '') + psc.ContentValue
		FROM	PropertyStaticContent psc
		WHERE	psc.PropertyID = @propertyId AND psc.ContentCode = @imageType
		
		RETURN @images
	END



