if exists(select * from sys.objects where type = 'fn' and name = 'fn_GetMlosByPeriod' )
begin
	drop function fn_GetMlosByPeriod
end
GO

CREATE FUNCTION [dbo].[fn_GetMlosByPeriod]
(
	@unitTypeId INT,
	@dateFrom DATETIME = NULL,
	@dateTo DATETIME = NULL
)
RETURNS INT
AS 
	BEGIN
		DECLARE @result DECIMAL(10, 2)
		
		--Travel period is defined
		IF(@dateFrom IS NOT NULL AND @dateTo IS NOT NULL)
			BEGIN
				SET	@result = (
					SELECT	TOP 1 MLOS
					FROM	UnitTypeMLOS utMlos
					WHERE	utMlos.UnitTypeID = @unitTypeId
							AND		 	
							(
								(utMlos.DateFrom BETWEEN @dateFrom AND @dateTo) OR
								(utMlos.DateUntil BETWEEN @dateFrom AND @dateTo) OR
								((@dateFrom >= utMlos.DateFrom) AND (@dateTo <= utMlos.DateUntil))
							)
					ORDER BY MLOS DESC
				)
				IF @result IS NULL
					Set @result = 0
			END
		--Travel period is not defined
		ELSE
			BEGIN
				
				SET @result = 0
			END

		RETURN @result
    END