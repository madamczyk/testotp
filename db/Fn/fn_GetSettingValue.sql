if exists(select * from sys.objects where type = 'fn' and name = 'fn_GetSettingValue' )
begin
	drop function fn_GetSettingValue
end
GO

CREATE FUNCTION [dbo].[fn_GetSettingValue]
(
	@settingCode NVARCHAR(255)
)
RETURNS INT

AS 
	BEGIN
		DECLARE @retVal INT
		SELECT @retVal = CAST(CAST(SettingValue AS nvarchar(MAX)) AS int) FROM OTP_Settings WHERE SettingCode = @settingCode
		RETURN @retVal
	END
