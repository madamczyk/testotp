if exists(select * from sys.objects where type = 'fn' and name = 'fn_CalculateFinalScore' )
begin
	drop function fn_CalculateFinalScore
end
GO

CREATE FUNCTION [dbo].[fn_CalculateFinalScore]
(
	@propertyId INT,
	@unitId INT,
	@numberOfGuests INT = NULL,
	@numberOfBedrooms INT = NULL,
	@numberOfBathrooms INT = NULL,
	@propertyTypes xml = NULL,
	@amenities xml = NULL,
	@tags xml = NULL
)
RETURNS FLOAT
AS 
	BEGIN
		
		DECLARE @selectedPropertyTypesCount INT
		DECLARE @selectedAmenitiesCount INT
		DECLARE @selectedTagsCount INT

		DECLARE @matchingPropertyTypes INT
		DECLARE @matchingAmenities INT
		DECLARE @matchingTags INT

		DECLARE @selectedPropertyTypesTable TABLE (ID int) 
		DECLARE @selectedAmenitiesTable TABLE (ID int) 
		DECLARE @selectedTagsTable TABLE (ID int)

		INSERT INTO @selectedPropertyTypesTable (ID) SELECT ParamValues.ID.value('@id','VARCHAR(20)') FROM @propertyTypes.nodes('/PropertyTypes/Item') as ParamValues(ID)
		INSERT INTO @selectedAmenitiesTable (ID) SELECT ParamValues.ID.value('@id','VARCHAR(20)') FROM @amenities.nodes('/Amenities/Item') as ParamValues(ID)
		INSERT INTO @selectedTagsTable (ID) SELECT ParamValues.ID.value('@id','VARCHAR(20)') FROM @tags.nodes('/Tags/Item') as ParamValues(ID)
			
		--number of matching amenities
		SELECT @matchingAmenities =  COUNT(*) 
		FROM @selectedAmenitiesTable sA
		JOIN PropertyAmenities PA ON sA.ID = PA.AmenityID
		WHERE PA.PropertyID = @propertyId

		--number of matching tags
		SELECT @matchingTags =  COUNT(*) 
		FROM @selectedTagsTable sPT
		JOIN PropertyTags PT ON sPT.ID = PT.TagID
		WHERE PT.PropertyID = @propertyId

		--number of matching propertyTypes
		SELECT @matchingPropertyTypes =  COUNT(*) 
		FROM @selectedPropertyTypesTable sPT
		JOIN Properties Pr ON sPT.ID = Pr.PropertyTypeId 
		WHERE Pr.PropertyID = @propertyId
		
		SELECT @selectedPropertyTypesCount = COUNT(*) FROM @selectedPropertyTypesTable
		SELECT @selectedAmenitiesCount = COUNT(*) FROM @selectedAmenitiesTable
		SELECT @selectedTagsCount = COUNT(*) FROM @selectedTagsTable		
		
		DECLARE @param_P INT
		DECLARE @param_Q INT
		DECLARE @x INT
		DECLARE @result FLOAT = 0
		
		--guests
		IF(@numberOfGuests IS NOT NULL)		
		BEGIN	
			SELECT @x = MaxNumberOfGuests FROM Units WHERE UnitID = @unitId	
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('Guests', @x, @numberOfGuests)
		END
		ELSE
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('Guests', 1, 1)
		
		--bedrooms
		IF(@numberOfBedrooms IS NOT NULL)
		BEGIN
			SELECT @x = MaxNumberOfBedRooms FROM Units WHERE UnitID = @unitId
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('Bedrooms', @x, @numberOfBedrooms)
		END
		ELSE
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('Bedrooms', 1, 1)


		--bathrooms
		IF(@numberOfBathrooms IS NOT NULL)
		BEGIN		
			SELECT @x = MaxNumberOfBathrooms FROM Units WHERE UnitID = @unitId
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('Bathrooms', @x, @numberOfBathrooms)
		END
		ELSE
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('Bathrooms', 1, 1)
		
		--multiselection	 
		IF(@selectedAmenitiesCount > 0)		
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('Amenities', @selectedAmenitiesCount, @matchingAmenities)
		ELSE	
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('Amenities', 1, 1)
			
		IF(@selectedPropertyTypesCount > 0)
			BEGIN
				IF(@matchingPropertyTypes = 1)
					SET @result = @result + dbo.fn_CalculateDimensionScoreValue('PropertyTypes', @selectedPropertyTypesCount, @selectedPropertyTypesCount)
				ELSE
					SET @result = @result + dbo.fn_CalculateDimensionScoreValue('PropertyTypes', @selectedPropertyTypesCount, @matchingPropertyTypes)
					
			END
		ELSE
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('PropertyTypes', 1, 1)
				
		IF(@selectedTagsCount > 0)
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('Tags', @selectedTagsCount, @matchingTags)
		ELSE
			SET @result = @result + dbo.fn_CalculateDimensionScoreValue('Tags', 1, 1)
				
		return @result
    
	END