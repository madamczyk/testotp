if exists(select * from sys.objects where type = 'fn' and name = 'fn_CalculateDimensionScore' )
begin	
drop function fn_CalculateDimensionScore
end
GO

CREATE FUNCTION [dbo].[fn_CalculateDimensionScore](    @X FLOAT,	@P FLOAT,	@Q FLOAT,	@Xs FLOAT = NULL)RETURNS FLOATAS 	BEGIN		DECLARE @result FLOAT		DECLARE @maxY INT	= 10		DECLARE @a FLOAT		DECLARE @b FLOAT		DECLARE @x0 FLOAT		DECLARE @x1 FLOAT		IF(@Xs IS NULL)			RETURN 0		SET @x0 = @X - @P		SET @x1 = @X + @Q					IF(@Xs < @X0 OR @Xs > @X1)			RETURN 0		IF(@Xs <= @X)			BEGIN						SET @a = @maxY / CASE WHEN @P = 0 THEN @maxY ELSE @P END				SET @b = CASE WHEN @P = 0 THEN 0 ELSE - @a * @x0 END				IF(@Xs = @X)					BEGIN 						SET @a = @maxY						SET @Xs = 1						SET @b = 0					END			END		ELSE						BEGIN				SET @a = - @maxY / CASE WHEN @Q = 0 THEN @maxY ELSE @Q END				SET @b = CASE WHEN @Q = 0 THEN 0 ELSE @maxY - @a * @X END			END					SET @result = @a * @Xs + @b						RETURN @result    END	