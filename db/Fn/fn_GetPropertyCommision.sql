if exists(select * from sys.objects where type = 'fn' and name = 'fn_GetPropertyCommision' )
begin
	drop function fn_GetPropertyCommision
end
GO

CREATE FUNCTION [dbo].[fn_GetPropertyCommision]
(
	@propertyId INT,
	@dateFrom DATETIME = NULL
)
RETURNS DECIMAL(10, 2)

AS 
	BEGIN
		DECLARE @commision DECIMAL(10, 2)

		IF(@dateFrom IS NULL)
			RETURN NULL
		
		SELECT	@commision = CommissionOverride 
		FROM	PropertyCommissionOverride
		WHERE	PropertyID = @propertyId
				AND (@dateFrom IS NOT NULL) 
				AND (@dateFrom BETWEEN DateFrom AND DateUntil)
		
		IF(@commision IS NULL)
			BEGIN
				SELECT		@commision = StandardCommission 
				FROM	Properties 
				WHERE	PropertyID = @propertyId
			END

			RETURN ISNULL(@commision, 0);
	END



