if exists(select * from sys.objects where type = 'fn' and name = 'fn_GetUnitBlockingType' )
begin
	drop function fn_GetUnitBlockings
end
GO

CREATE FUNCTION [dbo].[fn_GetUnitBlockingType]
(
	@unitId INT,
	@date DATETIME = NULL
)
RETURNS INT
AS 
	BEGIN				
		DECLARE @result INT

		SET @result = 0; /* default value when there is no reservations */

		SELECT TOP 1 @result = b.Type FROM UnitInvBlockings b
		WHERE
			(@date BETWEEN b.DateFrom and b.DateUntil)
			and b.UnitID = @unitId
			

		RETURN @result
    END
