if exists(select * from sys.objects where type = 'fn' and name = 'fn_GetMlosByDate' )
begin
	drop function fn_GetMlosByDate
end
GO

CREATE FUNCTION [dbo].[fn_GetMlosByDate]
(
	@date datetime,
	@UnitId integer
)
RETURNS int
AS
BEGIN
Return
(
	SELECT TOP 1 MLOS FROM 
		[dbo].[UnitTypeMLOS] mlos join [dbo].[Units] unit on mlos.[UnitTypeID] = unit.[UnitTypeID]
	WHERE
		@date between [DateFrom] AND [DateUntil]
		AND [UnitId]=@UnitId
	ORDER BY MLOS desc
)
END

GO