if exists(select * from sys.objects where type = 'fn' and name = 'fn_CalculateUnitPriceAccomodation' )
begin
	drop function fn_CalculateUnitPriceAccomodation
end
GO

CREATE FUNCTION [dbo].[fn_CalculateUnitPriceAccomodation]
(
	@unitId INT,
	@dateFrom DATETIME = NULL,
	@dateTo DATETIME = NULL
)
RETURNS DECIMAL(10, 2)
AS 
	BEGIN
		DECLARE @result DECIMAL(10, 2)
		
		--Travel period is defined
		IF(@dateFrom IS NOT NULL AND @dateTo IS NOT NULL)
			BEGIN
				--Daily rate of the unit * number of days
				
				DECLARE @dailyRate FLOAT = 0
				DECLARE @dayOfStay datetime = @dateFrom

				WHILE (@dayOfStay < @dateTo)
				BEGIN
					SELECT	@dailyRate += (ISNULL(DailyRate, 0))
					FROM	UnitRates 
					WHERE	UnitID = @unitId
							AND (@dayOfStay BETWEEN DateFrom AND DateUntil)

					SET @dayOfStay = DATEADD(day,1,@dayOfStay)
				END
				
				SET @result = ISNULL(@dailyRate, 0)
			END
		--Travel period is not defined
		ELSE
			BEGIN
				
				SELECT	@result = MIN(DailyRate)
				FROM	UnitRates 
				WHERE	UnitID = @unitId 
						AND DateUntil > GETDATE() 	
			END

		RETURN @result
    END
GO


