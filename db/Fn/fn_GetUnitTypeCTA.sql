if exists(select * from sys.objects where type = 'fn' and name = 'fn_GetUnitTypeCTA' )
begin
	drop function fn_GetUnitTypeCTA
end
GO

CREATE FUNCTION [dbo].[fn_GetUnitTypeCTA]
(
	@unitId INT,
	@date DATETIME = NULL
)
RETURNS BIT
AS 
	BEGIN				
		DECLARE @result BIT

		SET @result = 0; /* default value when there is no CTA declared */

		SELECT @result = CASE datepart(dw,@date)
			  WHEN 2 THEN cta.Monday
			  WHEN 3 THEN cta.Tuesday
			  WHEN 4 THEN cta.Wednesday
			  WHEN 5 THEN cta.Thursday
			  WHEN 6 THEN cta.Friday
			  WHEN 7 THEN cta.Saturday
			  WHEN 1 THEN cta.Sunday
			  END
		FROM [dbo].[UnitTypeCTA] cta join [dbo].[units] unit on unit.UnitTypeID=cta.UnitTypeID
		WHERE unit.UnitID=@unitId and (@date BETWEEN cta.DateFrom and cta.DateUntil)
			

		RETURN @result
    END

