if exists(select * from sys.objects where type = 'fn' and name = 'fn_GetUnitBlockings' )
begin
	drop function fn_GetUnitBlockings
end
GO

CREATE FUNCTION [dbo].fn_GetUnitBlockings
(
	@unitId INT,
	@date DATETIME = NULL
)
RETURNS BIT
AS 
	BEGIN				
		DECLARE @result BIT

		SET @result = 0; /* default value when there is no reservations */

		SELECT @result = 1 FROM UnitInvBlockings b
		WHERE
			(@date BETWEEN b.DateFrom and b.DateUntil)
			and b.UnitID = @unitId
			

		RETURN @result
    END