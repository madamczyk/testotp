if exists(select * from sys.objects where type = 'fn' and name = 'fn_GetPropertyReviewRate' )
begin
	drop function fn_GetPropertyReviewRate
end
GO

Create FUNCTION [dbo].[fn_GetPropertyReviewRate]
(
	@propertyId int
)
RETURNS INT

AS 
	BEGIN
		DECLARE @experienceRating FLOAT
		DECLARE @cleanlinessRating FLOAT
		DECLARE @locationRating FLOAT

		SELECT 
				@experienceRating =		AVG(Satisfaction_Experience),
				@cleanlinessRating =	AVG(Satisfaction_Cleanliness),
				@locationRating =		AVG(Satisfaction_Location)
		FROM	GuestReviews
		WHERE	PropertyID = @propertyId

		RETURN (@experienceRating + @cleanlinessRating + @locationRating) / 3
	END

