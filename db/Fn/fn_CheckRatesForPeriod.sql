if exists(select * from sys.objects where type = 'fn' and name = 'fn_CheckRatesForPeriod' )
begin
	drop function fn_CheckRatesForPeriod
end
GO

CREATE FUNCTION [dbo].[fn_CheckRatesForPeriod]
(
	@unitId INT,
	@dateFrom DATETIME = NULL,
	@dateTo DATETIME = NULL
)
RETURNS BIT
AS 
	BEGIN
		DECLARE @result BIT = NULL
		
		--Period is defined
		IF(@dateFrom IS NOT NULL AND @dateTo IS NOT NULL)
			BEGIN
				SET @result = 1
				DECLARE @dailyRate money = NULL
				DECLARE @dayOfStay datetime = DATEADD(dd, 0, DATEDIFF(dd, 0, @dateFrom))

				WHILE (@dayOfStay < @dateTo)
				BEGIN
					SET @dailyRate = NULL
					SELECT	@dailyRate = DailyRate
					FROM	UnitRates 
					WHERE	UnitID = @unitId
							AND (@dayOfStay BETWEEN DateFrom AND DateUntil)

					IF(@dailyRate IS NULL)
						BEGIN
							SET @result = 0
							BREAK
						END					
						
					SET @dayOfStay = DATEADD(dd, 1, DATEDIFF(dd, 0, @dayOfStay))
				END
				
				
			END
		--Period is not defined
		ELSE
			BEGIN
				SET @result = 1
			END

		RETURN @result
    END
