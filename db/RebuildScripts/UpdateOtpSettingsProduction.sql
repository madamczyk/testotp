update OTP_Settings Set SettingValue = 'http://otpfrontend.cloudapp.net/Booking/PaymentGatewayRedirect' where SettingCode = 'Payment.RedirectURL'
update OTP_Settings Set SettingValue = 'otpfrontend.cloudapp.net' where SettingCode = 'ExtranetHost'
update OTP_Settings set SettingValue = convert(varchar(20),getdate(),20) where SettingCode = 'Version'
update OTP_Settings set SettingValue = 'Production' where SettingCode = 'DeploymentType'
update OTP_Settings set SettingValue = 'join@ohtheplaces.com' where SettingCode = 'TicketSystem.JoinEmailAddress'