set SCRIPTFILE=.\CreateOtpStaging.sql

if exist %SCRIPTFILE% (
	del %SCRIPTFILE%
)


rem dbschema.sql
type ..\dbschema.sql >> %SCRIPTFILE%
echo.>> %SCRIPTFILE%
echo GO>> %SCRIPTFILE%
echo.>> %SCRIPTFILE%


rem Functions
for %%i in (..\Fn\*.sql) do (
	type %%i >> %SCRIPTFILE%
	echo.>> %SCRIPTFILE%
	echo GO>> %SCRIPTFILE%
	echo.>> %SCRIPTFILE%
)

rem Procedures
for %%i in (..\Sp\*.sql) do (
	type %%i >> %SCRIPTFILE%
	echo.>> %SCRIPTFILE%
	echo GO>> %SCRIPTFILE%
	echo.>> %SCRIPTFILE%
)


rem BaseData
type ..\Data\BaseData.sql >> %SCRIPTFILE%
echo.>> %SCRIPTFILE%
echo GO>> %SCRIPTFILE%
echo.>> %SCRIPTFILE%

rem TestData
type ..\Data\TestData.sql >> %SCRIPTFILE%
echo.>> %SCRIPTFILE%
echo GO>> %SCRIPTFILE%
echo.>> %SCRIPTFILE%

rem ProductionData
type ..\Data\ProductionData.sql >> %SCRIPTFILE%
echo.>> %SCRIPTFILE%
echo GO>> %SCRIPTFILE%
echo.>> %SCRIPTFILE%

rem Update OTP_Settings
echo.>> %SCRIPTFILE%
echo update OTP_Settings Set SettingValue = 'http://1ebc7dfe48ed4c96a1e78a749f969dff.cloudapp.net.cloudapp.net/Booking/PaymentGatewayRedirect' where SettingCode = 'Payment.RedirectURL'>>%SCRIPTFILE%
echo update OTP_Settings Set SettingValue = '1ebc7dfe48ed4c96a1e78a749f969dff.cloudapp.net.cloudapp.net' where SettingCode = 'Activation.ExtranetURL'>>%SCRIPTFILE%
echo.>> %SCRIPTFILE%


