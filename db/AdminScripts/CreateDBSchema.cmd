rem Usage CreateDB.cms SQLCMD CMDARGS SQLINSTANCE DEVDATA

rem Copy all scripts into a temp file
set TMPFILE=%TMP%\otp_db_scripts.sql
if exist %TMPFILE% (
	del %TMPFILE%
)

set TMPFILETEST=%TMP%\otp_db_testdata.sql
if exist %TMPFILETEST% (
	del %TMPFILETEST%
)

set TMPFILEPRODUCTION=%TMP%\otp_db_productiondata.sql
if exist %TMPFILEPRODUCTION% (
	del %TMPFILEPRODUCTION%
)

if (%4) == () (
	set ARGS= -S %2 -d OTPTest
	set DEVDATA= %3
) else (
	set ARGS= %2 -S %3 -d OTPTest
	set DEVDATA= %4 
)

rem dbschema.sql
type dbschema.sql >> %TMPFILE%
echo.>> %TMPFILE%
echo GO>> %TMPFILE%
echo.>> %TMPFILE%


rem Functions
for %%i in (.\Fn\*.sql) do (
	type %%i >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)

rem Procedures
for %%i in (.\Sp\*.sql) do (
	type %%i >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)


rem BaseData
type .\Data\BaseData.sql >> %TMPFILE%
echo.>> %TMPFILE%
echo GO>> %TMPFILE%
echo.>> %TMPFILE%
type .\RebuildScripts\UpdateOtpSettingsLocal.sql >> %TMPFILE%
echo.>> %TMPFILE%
echo GO>> %TMPFILE%
echo.>> %TMPFILE%

rem Execute Script
%1 -I %ARGS% -i %TMPFILE% -o .\AdminScripts\rebuildDB_CreateSchema.log


rem TestData & ProductionData
if %errorlevel% EQU 0 if %DEVDATA% EQU 1 (
	type .\Data\TestData.sql >> %TMPFILETEST%
	echo.>> %TMPFILETEST%
	
	rem Execute TestData Script
	%1 %ARGS% -i %TMPFILETEST% -o .\AdminScripts\rebuildDB_LoadTestData.log	
	
	type .\Data\ProductionData.sql >> %TMPFILEPRODUCTION%
	echo.>> %TMPFILEPRODUCTION%
	
	rem Execute ProductionData Script
	%1 %ARGS% -i %TMPFILEPRODUCTION% -o .\AdminScripts\rebuildDB_LoadProductionData.log
)

if %errorlevel% EQU 0 (
	del %TMPFILE%
	if exist %TMPFILETEST% (
		del %TMPFILETEST%
	)
	if exist %TMPFILEPRODUCTION% (
		del %TMPFILEPRODUCTION%
	)
) else (
	ECHO ON
	ECHO !!!!!!!!!!!!!!!!!!!!!!
	ECHO Error while creating [OTPTest] Database
	ECHO Check log file for details
	ECHO !!!!!!!!!!!!!!!!!!!!!!
)




