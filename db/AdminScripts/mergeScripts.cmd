rem Usage CreateDB.cms SQLCMD CMDARGS SQLINSTANCE DEVDATA

rem Copy all scripts into a temp file
set FNFILE=functions.sql
if exist %FNFILE% (
	del %FNFILE%
)

set PRFILE=storedProcedures.sql
if exist %PRFILE% (
	del %PRFILE%
)

rem Functions
for %%i in (..\Fn\*.sql) do (
	type %%i >> %FNFILE%
	echo.>> %FNFILE%
	echo GO>> %FNFILE%
	echo.>> %FNFILE%
)

rem Procedures
for %%i in (..\Sp\*.sql) do (
	type %%i >> %PRFILE%
	echo.>> %PRFILE%
	echo GO>> %PRFILE%
	echo.>> %PRFILE%
)




