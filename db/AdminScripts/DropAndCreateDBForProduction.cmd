ECHO OFF
if exist "C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\100\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\100\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\90\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\90\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\80\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\80\Tools\Binn\SQLCMD.exe"
)

if (%3) == () (
	set CMDARGS=
) else  (
	set CMDARGS= -U %2 -P %3
)

if (%4) == () if (%2) == () (
	set SQLINST= localhost\sqlexpress
) else if (%3) == () (
	set SQLINST= %2
) else (
	set SQLINST= %4
)

rem Drop OTPTest Database
%SQLCMD% -I %CMDARGS% -S %SQLINST% -i .\AdminScripts\DropOTPDB.sql -o .\AdminScripts\rebuildDB_drop.log

if %errorlevel% EQU 0 (
	rem Create db
	%SQLCMD% -I %CMDARGS% -S %SQLINST% -i .\AdminScripts\CreateOTPDB.sql -o .\AdminScripts\rebuildDB_create.log	
)
	
if %errorlevel% EQU 0 (
	.\AdminScripts\CreateDBSchemaForProduction.cmd %SQLCMD% -I %CMDARGS% %SQLINST% %1	
) else ( 
	ECHO ON
	ECHO !!!!!!!!!!!!!!!!!!!!!!
	ECHO Error while recreating [OTPTest] Database
	ECHO Check log file for details
	ECHO !!!!!!!!!!!!!!!!!!!!!!
)

