﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class OTPEntitiesContext : OTPEntities, IEntityContext
    {
        public OTPEntitiesContext()
        : base("name=OTPEntities")
        {

        }

        public OTPEntitiesContext(string connectionString)
            : base(connectionString)
        {

        }
    }
}
