﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.CustomObjects
{
    public class PropertiesSearchResult
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public IList<string> Thumbnails { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal PricePerNight { get; set; }
        public bool IsPricePerNight { get; set; }        
        public int Rating { get; set; }
        public int FinalScore { get; set; }

        public int Bathrooms { get; set; }
        public int Bedrooms { get; set; }
        public int Sleeps { get; set; }

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string CultureCode { get; set; }
        public string Destination { get; set; }

        public PropertiesSearchResult()
        {            
            Thumbnails = new List<string>();                                    
            IsPricePerNight = false;
        }
    }
}
