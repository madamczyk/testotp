﻿using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.CustomObjects
{
    public class UnitAvailabilityResult
    {
        public String Date { get; set; }
        public Decimal Price { get; set; }
        public Boolean IsBlocked { get; set; }
        public UnitBlockingType UnitBlockingType { get; set; }
        public Int32 Mlos { get; set; }
        public Boolean Cta { get; set; }
    }
}
