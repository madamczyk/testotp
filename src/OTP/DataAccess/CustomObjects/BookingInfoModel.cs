﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess.CustomObjects
{
    [Serializable]
    public class BookingInfoModel
    {
        public DateTime dateTripFrom { get; set; }
        public DateTime dateTripUntil { get; set; }
        public Int32 PropertyId { get; set; }
        public Int32 UnitId { get; set; }
    }
}