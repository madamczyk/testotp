﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.CustomObjects
{
    public class PropertyStaticContententResult
    {
        public int ContentCode { get; set; }
        public IEnumerable<PropertyStaticContent> ContentItems { get; set; }

        public PropertyStaticContententResult()
        {
            ContentItems = new List<PropertyStaticContent>();
        }
    }
}
