﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace DataAccess.CustomObjects
{
    [Serializable]
    public class PropertyDetailsInvoice
    {
        public Int32 LengthOfStay { get; set; }
        public Decimal PricePerNight { get; set; }
        public Decimal TotalAccomodation { get; set; }
        public IEnumerable<PropertyAddOn> AddonsList { get; set; }
        public Decimal TotalAddons { get; set; }

        public Decimal SubtotalAccomodationAddons { get; set; }

        public IEnumerable<Tax> TaxList { get; set; }
        public Decimal TotalTax { get; set; }

        public Decimal InvoiceTotal { get; set; }

        /// <summary>
        /// 7 Days Discount calculated only for Accomodation value
        /// </summary>
        public decimal TotalSevenDaysDiscount { get; set; }

        public CultureInfo Culture { get; set; }
        
        public PropertyDetailsInvoice()
        {
            AddonsList = new List<PropertyAddOn>();
            TaxList = new List<Tax>();
            TotalSevenDaysDiscount = 0;
        }


        
    }
}