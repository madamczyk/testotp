﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.CustomObjects
{
    public class ReservationForUser
    {
        public int ReservationID { get; set; }

        public string ConfirmationID { get; set; }

        public System.DateTime BookingDate { get; set; }

        public int BookingStatus { get; set; }

        public System.DateTime DateArrival { get; set; }

        public System.DateTime DateDeparture { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal TripBalance { get; set; }

        public int PropertyID { get; set; }

        public Property Property { get; set; }

        public IList<String> Thumbnails { get; set; }

        public ReservationForUser()
        {
            Thumbnails = new List<string>();
        }

        
    }
}
