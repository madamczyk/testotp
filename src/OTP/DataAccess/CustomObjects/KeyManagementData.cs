﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess.CustomObjects
{
    public class KeyManagementData
    {
        public Property Property { get; set; }
        public Unit Unit { get; set; }
        public Reservation Reservation { get; set; }
        public User Guest { get; set; }
    }
}