﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.CustomObjects
{
    public class PropertyAmenities
    {
        public int Icon { get; set; }
        public string AmenityGroupName { get; set; }
        public List<string> Amenities { get; set; }
        public int Position { get; set; }

        public PropertyAmenities()
        {
            this.Amenities = new List<string>();
        }
    }
}
