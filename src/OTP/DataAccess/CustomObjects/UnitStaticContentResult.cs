﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.CustomObjects
{
    public class UnitStaticContentResult
    {
        public int ContentCode { get; set; }
        public IEnumerable<UnitStaticContent> ContentItems { get; set; }

        public UnitStaticContentResult()
        {
            ContentItems = new List<UnitStaticContent>();
        }
    }
}
