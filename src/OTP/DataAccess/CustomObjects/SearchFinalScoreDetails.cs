﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess.CustomObjects
{
    public class SearchFinalScoreDetails
    {
        public double FinalScore { get; set; }
        
        public int? UnitGuests { get; set; }
        public int? UnitBedrooms { get; set; }
        public int? UnitBathrooms { get; set; }
        public string PropertyType { get; set; }

        public List<string> NotMatchedAmenities { get; set; }
        public List<string> NotMatchedPropertyTypes { get; set; }
        public List<string> NotMatchedExperiences { get; set; }

        public SearchFinalScoreDetails()
        {
            this.NotMatchedAmenities = new List<string>();
            this.NotMatchedExperiences = new List<string>();
            this.NotMatchedPropertyTypes = new List<string>();
        }
    }
}