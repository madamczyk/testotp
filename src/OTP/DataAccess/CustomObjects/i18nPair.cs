﻿using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.CustomObjects
{
    [Serializable]
    public class i18nPair
    {
        //name of Culture (example: 'en-US', 'de-DE')
        public String Code { get; set; }
     
        public String Content { get; set; }
    }   
}
