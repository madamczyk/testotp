﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess.CustomObjects
{
    [Serializable]
    public class ReservationForHomeOwner
    {
        public string ConfirmationId { get; set; }
        public DateTime Booked { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public decimal GrossDue { get; set; }
        public decimal TransactionFee { get; set; }
        public decimal OwnerNet { get; set; }
        public decimal TaxValue { get; set; }
        public string CultureCode { get; set; }
        public int PropertyId { get; set; }
        public int UnitId { get; set; }
        public string PropertyName { get; set; }            
        public int BookingStatus { get; set; }

        public User User { get; set; }


        
    }
}