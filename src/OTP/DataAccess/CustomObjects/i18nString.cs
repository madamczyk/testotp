﻿using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

namespace DataAccess.CustomObjects
{
    /// <summary>
    /// Custom String implementation that holds i18n content
    /// </summary>
    [Serializable]
    public class i18nString
    {
        public List<i18nPair> i18nContent { get; set; }

        public i18nString()
        {
            this.i18nContent = new List<i18nPair>();
        }

        /// <summary>
        /// Returns the string value based on the user context language or en-US if current language value is empty
        /// </summary>
        /// <returns>localized string value</returns>
        public override string ToString()
        {
            i18nPair result = i18nContent.Where(s => s.Code == Thread.CurrentThread.CurrentCulture.Name).FirstOrDefault();
            if (result == null)
            {
                var q = i18nContent.Where(s => s.Code == CultureCode.en_US).FirstOrDefault();
                if (q == null)
                    return "";
                return q.Content;
            }
            else
                return result.Content;
        }

        /// <summary>
        /// Returns the string value based on the specified language
        /// </summary>
        /// <param name="code">requested language</param>
        /// <returns>localized string value</returns>
        public string ToString(string cultureCode)
        {
            CultureInfo ci = CultureInfo.CreateSpecificCulture(cultureCode);
            i18nPair result = i18nContent.Where(s => s.Code == ci.Name).FirstOrDefault();
            if (result == null)
                return i18nContent.Where(s => s.Code == ci.Name).FirstOrDefault().Content;
            else
                return result.Content;
        }

        public void SetValue(string cultureCode, string content)
        {
            cultureCode = string.IsNullOrWhiteSpace(cultureCode) ? CultureCode.en_US : cultureCode;

            CultureInfo ci = CultureInfo.CreateSpecificCulture(cultureCode);
            i18nPair result = i18nContent.Where(s => s.Code == ci.Name).FirstOrDefault();
            if (result == null)
            {
                result = new i18nPair();
                result.Code = ci.Name;
                result.Content = content;
                i18nContent.Add(result);
            }
            else
                result.Content = content;
        }
    }
}
