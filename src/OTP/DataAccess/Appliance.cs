//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    [Serializable()]
    public partial class Appliance
    {
        public Appliance()
        {
            this.UnitAppliancies = new HashSet<UnitAppliancy>();
        }
    
        public int ApplianceId { get; set; }
        public string Name_i18n { get; set; }
        public int Type { get; set; }
    
        public virtual ApplianceGroup ApplianceGroup { get; set; }
        public virtual ICollection<UnitAppliancy> UnitAppliancies { get; set; }
    }
}
