//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    [Serializable()]
    public partial class GuestPaymentMethod
    {
        public GuestPaymentMethod()
        {
            this.Reservations = new HashSet<Reservation>();
        }
    
        public int GuestPaymentMethodID { get; set; }
        public string CreditCardLast4Digits { get; set; }
        public string ReferenceToken { get; set; }
    
        public virtual CreditCardType CreditCardType { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
        public virtual User User { get; set; }
    }
}
