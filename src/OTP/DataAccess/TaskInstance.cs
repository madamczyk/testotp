//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    [Serializable()]
    public partial class TaskInstance
    {
        public TaskInstance()
        {
            this.TaskInstanceDetails = new HashSet<TaskInstanceDetail>();
        }
    
        public int Id { get; set; }
        public System.DateTime ExecutionTimestamp { get; set; }
        public Nullable<int> ExecutionResult { get; set; }
    
        public virtual ScheduledTask ScheduledTask { get; set; }
        public virtual ICollection<TaskInstanceDetail> TaskInstanceDetails { get; set; }
    }
}
