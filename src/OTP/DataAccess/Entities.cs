﻿using DataAccess.ObjectExtensions.i18n;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Text;
using Microsoft.Practices.TransientFaultHandling;
using Microsoft.Practices.EnterpriseLibrary.WindowsAzure.TransientFaultHandling.SqlAzure;

namespace DataAccess
{
    public partial class OTPEntities
    {
        #region Private members

        private RetryPolicy sqlAzureRetryPolicy = 
            new RetryPolicy<SqlAzureTransientErrorDetectionStrategy>(3, TimeSpan.FromSeconds(5));

        #endregion

        public OTPEntities(string connectionString)
            : base(connectionString)
        {
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += ObjectContext_ObjectMaterialized;
        }

        /// <summary>
        /// Event raised when all the scalar or complex properties are set to on Entity
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ObjectContext_ObjectMaterialized(object sender, System.Data.Objects.ObjectMaterializedEventArgs e)
        {
            // persist the i18n values of the current Thread language in special properties
            ILocalizable localizableEntity = e.Entity as ILocalizable;
            if (localizableEntity != null)
                localizableEntity.PersistI18nValues();

            if (e.Entity is GuestReview)
            {
                var ent = ((GuestReview)e.Entity);

                ent.OverallRating = (ent.Satisfaction_Cleanliness + ent.Satisfaction_Experience + ent.Satisfaction_Location) / 3;
            }
        }

        /// <summary>
        /// Overrides default save changes in order to apply retry policy
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            return sqlAzureRetryPolicy.ExecuteAction<int>(() => 
                {
                    return base.SaveChanges();
                });
        }

        /// <summary>
        /// Executes linq query using retry policy
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public T AutoRetryQuery<T>(Func<T> query)
        {
            return sqlAzureRetryPolicy.ExecuteAction<T>(query);
        }


        [EdmFunction("OTPModel.Store","fn_CalculateUnitPrice")]
        public static decimal? GetUnitPrice(int unitId, DateTime? dateFrom, DateTime? dateTo)
        {
            throw new NotSupportedException("Only from LINQ");
        }

        [EdmFunction("OTPModel.Store", "fn_CalculateUnitPriceAccomodation")]
        public static decimal? GetUnitPriceAccomodation(int unitId, DateTime? dateFrom, DateTime? dateTo)
        {
            throw new NotSupportedException("Only from LINQ");
        }

        [EdmFunction("OTPModel.Store", "fn_GetPropertyReviewRate")]
        public static int? GetPropertyRating(int propertyId)
        {
            throw new NotSupportedException("Only from LINQ");
        }

        [EdmFunction("OTPModel.Store", "fn_CheckRatesForPeriod")]
        public static bool CheckRatesForPeriod(int unitId, DateTime dateFrom, DateTime dateTo)
        {
            throw new NotSupportedException("Only from LINQ");
        }
    }    
}
