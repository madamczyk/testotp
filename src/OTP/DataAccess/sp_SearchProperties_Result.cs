//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    
    public partial class sp_SearchProperties_Result
    {
        public int ID { get; set; }
        public int UnitID { get; set; }
        public int NumberOfBathrooms { get; set; }
        public int NumberOfBedrooms { get; set; }
        public int NumberOfGuests { get; set; }
        public string Image { get; set; }
        public string Thumbnails { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public Nullable<double> FinalScore { get; set; }
        public Nullable<bool> IsPricePerNight { get; set; }
        public int Rating { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> Commission { get; set; }
        public Nullable<int> MLOS { get; set; }
        public string LanguageCode { get; set; }
        public string Destination { get; set; }
        public Nullable<long> UnitRank { get; set; }
        public string CultureCode { get; set; }
        public Nullable<bool> UnitRatesDefined { get; set; }
    }
}
