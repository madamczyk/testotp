//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    [Serializable()]
    public partial class PropertyRawData
    {
        public int PropertyRawDataRecordID { get; set; }
        public int RawRecordType { get; set; }
        public string Description { get; set; }
        public string RawRecordContent { get; set; }
        public string RecordName { get; set; }
    
        public virtual Property Property { get; set; }
    }
}
