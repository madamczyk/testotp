//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    [Serializable()]
    public partial class UnitStaticContent
    {
        public int UnitStaticContentId { get; set; }
        public int ContentCode { get; set; }
        public string ContentValue_i18n { get; set; }
        public string ContentValue { get; set; }
    
        public virtual Unit Unit { get; set; }
    }
}
