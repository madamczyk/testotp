﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public partial class GuestReview
    {
        /// <summary>
        /// Average rating calculated based on 3 rating counters
        /// </summary>
        public int OverallRating { get; set; }        
    }
}
