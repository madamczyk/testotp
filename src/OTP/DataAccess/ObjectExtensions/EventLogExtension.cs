﻿using DataAccess.ObjectExtensions.i18n;
using System;

namespace DataAccess
{
    public partial class EventLog : ILocalizable
    {
        /// <summary>
        /// Id represented as string
        /// </summary>
        public string IdString { get; set; }

        /// <summary>
        /// CorrelationId represented as string
        /// </summary>
        public string CorrelationIdString { get; set; }

        public void PersistI18nValues()
        {
            this.IdString = Id.ToString();

            if (CorrelationId.HasValue)
                this.CorrelationIdString = CorrelationId.Value.ToString();
            else
                this.CorrelationIdString = String.Empty;
        }
    }
}
