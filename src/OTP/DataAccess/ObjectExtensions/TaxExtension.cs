﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public partial class Tax
    {
        /// <summary>
        /// Calculated price based on the percent value
        /// </summary>
        public decimal CalculatedValue { get; set; }
    }
}
