﻿using DataAccess.ObjectExtensions.i18n;
using System;
using System.Linq;

namespace DataAccess
{
    public partial class Reservation : ILocalizable
    {
        /// <summary>
        /// Sum of all payment transactions for the reservation
        /// </summary>
        public decimal PaymentReceived { get; set; }

        /// <summary>
        /// Preformatted billing address of the user, extracted from ReservationBillingAddresses table
        /// </summary>
        public string BillingAddress { get; set; }

        /// <summary>
        /// Reservation ID represented as string
        /// </summary>
        public string ReservationIDString { get; set; }

        public void PersistI18nValues()
        {
            var data = this.ReservationBillingAddresses.Where(rba => rba.User.UserID == this.User.UserID && rba.Reservation.ReservationID == this.ReservationID).SingleOrDefault();

            this.PaymentReceived = this.ReservationTransactions.Where(rt => rt.Payment).Sum(rt => rt.Amount);

            if (data != null)
                this.BillingAddress = String.Format(
                    "{0}, {1}, {2} {3}",
                    data.Street,
                    data.City,
                    data.State,
                    data.Zip);
            else
                this.BillingAddress = String.Empty;

            this.ReservationIDString = ReservationID.ToString();
        }
    }
}
