﻿using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;
using System;

namespace DataAccess
{
    public partial class PaymentGatewayOperationsLog : ILocalizable
    {
        /// <summary>
        /// Id of the reservation, if reservation not null
        /// </summary>
        public int? ReservationId { get; set; }

        /// <summary>
        /// Confirmation Id of the reservation, if reservation not null
        /// </summary>
        public string ReservationConfirmationID { get; set; }

        /// <summary>
        /// Booking date of the reservation, if reservation not null
        /// </summary>
        public DateTime? ReservationBookingDate { get; set; }

        /// <summary>
        /// Booking status of the reservation, if reservation not null
        /// </summary>
        public BookingStatus? ReservationBookingStatus { get; set; }

        public void PersistI18nValues()
        {
            if (Reservation != null)
            {
                ReservationId = Reservation.ReservationID;
                ReservationConfirmationID = Reservation.ConfirmationID;
                ReservationBookingDate = Reservation.BookingDate;
                ReservationBookingStatus = (BookingStatus)Reservation.BookingStatus;
            }
            else
            {
                ReservationId = null;
                ReservationConfirmationID = String.Empty;
                ReservationBookingDate = null;
                ReservationBookingStatus = null;
            }
        }
    }
}
