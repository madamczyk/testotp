﻿using DataAccess.ObjectExtensions.i18n;

namespace DataAccess
{
    public partial class TaskInstanceDetail : ILocalizable
    {
        /// <summary>
        /// Id represented as string
        /// </summary>
        public string IdString { get; set; }

        public void PersistI18nValues()
        {
            this.IdString = Id.ToString();
        }
    }
}
