﻿using Common.Extensions;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;
using System;

namespace DataAccess
{
    public partial class ScheduledTask : ILocalizable
    {
        /// <summary>
        /// Scheduled details formatted accordingly to task data
        /// </summary>
        public string ScheduleDetails { get; set; }

        public void PersistI18nValues()
        {
            switch ((ScheduleType)ScheduleType)
            {
                case Enums.ScheduleType.Every:
                    if (TimeInterval.HasValue && ScheduleIntervalType.HasValue)
                        ScheduleDetails = String.Format(
                            "{0} {1}",
                            TimeInterval.Value,
                            EnumConverter.GetValue((ScheduleIntervalType)ScheduleIntervalType.Value));
                    else
                        ScheduleDetails = String.Empty;
                    break;

                case Enums.ScheduleType.EverydayAt:
                    if (ScheduleTimestamp.HasValue)
                        ScheduleDetails = ScheduleTimestamp.Value.ToLocalizedTimeString();
                    else
                        ScheduleDetails = String.Empty;
                    break;

                case Enums.ScheduleType.Once:
                    ScheduleDetails = NextPlannedExecutionTime.HasValue ? NextPlannedExecutionTime.Value.ToLocalizedDateTimeString() : Common.Resources.Common.ScheduledTask_TimeInterval_Immediately;
                    break;

                default:
                    ScheduleDetails = String.Empty;
                    break;
            }
        }
    }
}
