﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Common.Serialization;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;

namespace DataAccess
{
    public partial class DictionaryCountry : ILocalizable
    {
        #region CountryName i18n Wrapper
        private i18nString countryNameInternal;

        public i18nString CountryName
        {
            get
            {
                if (countryNameInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.CountryName_i18n))
                    {
                        countryNameInternal = (i18nString)SerializationHelper.DeserializeObject(this.CountryName_i18n, typeof(i18nString));
                    }
                    else
                    {
                        countryNameInternal = new i18nString();
                    }
                }

                return countryNameInternal;
            }
        }

        public string CountryNameCurrentLanguage { get; set; }

        public void SetCountryNameValue(string content, string code = null)
        {
            if (countryNameInternal == null)
                countryNameInternal = new i18nString();
            this.countryNameInternal.SetValue(code, content);
            this.CountryName_i18n = SerializationHelper.SerializeObject(countryNameInternal);
        }
        #endregion
        
        public void PersistI18nValues()
        {
            this.CountryNameCurrentLanguage = this.CountryName.ToString();
        }
    }
}
