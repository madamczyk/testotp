﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Common.Serialization;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;

namespace DataAccess
{
    public partial class UnitType : ILocalizable
    {
        #region UnitTypeTitle i18n Wrapper
        private i18nString titleInternal;

        public i18nString UnitTypeTitle
        {
            get
            {
                if (titleInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.UnitTypeTitle_i18n))
                    {
                        titleInternal = (i18nString)SerializationHelper.DeserializeObject(this.UnitTypeTitle_i18n, typeof(i18nString));
                    }
                    else
                    {
                        titleInternal = new i18nString();
                    }
                }

                return titleInternal;
            }
        }

        public string UnitTypeTitleCurrentLanguage { get; set; }

        public void SetTitleValue(string content, string code = null)
        {
            if (titleInternal == null)
                titleInternal = new i18nString();
            this.titleInternal.SetValue(code, content);
            this.UnitTypeTitle_i18n = SerializationHelper.SerializeObject(titleInternal);
        }
        #endregion

        #region UnitTypeDesc i18n Wrapper
        private i18nString descInternal;

        public i18nString UnitTypeDesc
        {
            get
            {
                if (descInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.UnitTypeDesc_i18n))
                    {
                        descInternal = (i18nString)SerializationHelper.DeserializeObject(this.UnitTypeDesc_i18n, typeof(i18nString));
                    }
                    else
                    {
                        descInternal = new i18nString();
                    }
                }

                return descInternal;
            }
        }

        public string UnitTypeDescCurrentLanguage { get; set; }

        public void SetDescValue(string content, string code = null)
        {
            if (descInternal == null)
                descInternal = new i18nString();
            this.descInternal.SetValue(code, content);
            this.UnitTypeDesc_i18n = SerializationHelper.SerializeObject(descInternal);
        }
        #endregion

        public void PersistI18nValues()
        {
            this.UnitTypeTitleCurrentLanguage = this.UnitTypeTitle.ToString();
            this.UnitTypeDescCurrentLanguage = this.UnitTypeDesc.ToString();            
        }
    }
}
