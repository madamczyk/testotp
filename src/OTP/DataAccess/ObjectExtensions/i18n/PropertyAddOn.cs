﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Common.Serialization;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;

namespace DataAccess
{
    public partial class PropertyAddOn : ILocalizable
    {
        #region Total Price
        /// <summary>
        /// Total price depending on unit of measure for the Property Add On
        /// </summary>
        public decimal TotalPrice
        {
            get;
            set;
        }

        #endregion

        #region AddOnTitle i18n Wrapper
        private i18nString addOnTitle;

        public i18nString AddOnTitle
        {
            get
            {
                if (addOnTitle == null)
                {
                    if (!string.IsNullOrEmpty(this.AddOnTitle_i18n))
                    {
                        addOnTitle = (i18nString)SerializationHelper.DeserializeObject(this.AddOnTitle_i18n, typeof(i18nString));
                    }
                    else
                    {
                        addOnTitle = new i18nString();
                    }
                }

                return addOnTitle;
            }
        }

        public string AddOnTitleCurrentLanguage { get; set; }

        public void SetTitleValue(string content, string code = null)
        {
            if (addOnTitle == null)
                addOnTitle = new i18nString();
            this.addOnTitle.SetValue(code, content);
            this.AddOnTitle_i18n = SerializationHelper.SerializeObject(addOnTitle);
        }
        #endregion

        #region AddOnDesc i18n Wrapper
        private i18nString addOnDesc;

        public i18nString AddOnDesc
        {
            get
            {
                if (addOnDesc == null)
                {
                    if (!string.IsNullOrEmpty(this.AddOnDescrption_i18n))
                    {
                        addOnDesc = (i18nString)SerializationHelper.DeserializeObject(this.AddOnDescrption_i18n, typeof(i18nString));
                    }
                    else
                    {
                        addOnDesc = new i18nString();
                    }
                }

                return addOnDesc;
            }
        }

        public string AddOnDescCurrentLanguage { get; set; }

        public void SetDescValue(string content, string code = null)
        {
            if (addOnDesc == null)
                addOnDesc = new i18nString();
            this.addOnDesc.SetValue(code, content);
            this.AddOnDescrption_i18n = SerializationHelper.SerializeObject(addOnDesc);
        }

        #endregion

        public void PersistI18nValues()
        {
            this.AddOnDescCurrentLanguage = this.AddOnDesc.ToString();
            this.AddOnTitleCurrentLanguage = this.AddOnTitle.ToString();
        }
    }
}
