﻿using Common.Serialization;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public partial class IntranetSiteMapAction : ILocalizable
    {
        #region DisplayName i18n Wrapper
        private i18nString displayNameInternal;

        public i18nString DisplayName
        {
            get
            {
                if (displayNameInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.DisplayName_i18n))
                    {
                        displayNameInternal = (i18nString)SerializationHelper.DeserializeObject(this.DisplayName_i18n, typeof(i18nString));
                    }
                    else
                    {
                        displayNameInternal = new i18nString();
                    }
                }

                return displayNameInternal;
            }
        }

        public string DisplayNameCurrentLanguage { get; set; }

        public void SetDisplayNameValue(string content, string code = null)
        {
            if (displayNameInternal == null)
                displayNameInternal = new i18nString();
            this.displayNameInternal.SetValue(code, content);
            this.DisplayName_i18n = SerializationHelper.SerializeObject(displayNameInternal);
            this.DisplayNameCurrentLanguage = this.DisplayName.ToString();
        }
        #endregion

        #region Description i18n Wrapper
        private i18nString descriptionInternal;

        public i18nString Description
        {
            get
            {
                if (descriptionInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Description_i18n))
                    {
                        descriptionInternal = (i18nString)SerializationHelper.DeserializeObject(this.Description_i18n, typeof(i18nString));
                    }
                    else
                    {
                        descriptionInternal = new i18nString();
                    }
                }

                return descriptionInternal;
            }
        }
        public string DescriptionCurrentLanguage { get; set; }

        public void SetDescriptionValue(string content, string code = null)
        {
            if (descriptionInternal == null)
                descriptionInternal = new i18nString();
            this.descriptionInternal.SetValue(code, content);
            this.Description_i18n = SerializationHelper.SerializeObject(descriptionInternal);
            this.DescriptionCurrentLanguage = this.Description.ToString();
        }
        #endregion

        public void PersistI18nValues()
        {
            this.DisplayNameCurrentLanguage = this.DisplayName.ToString();
            this.DescriptionCurrentLanguage = this.Description.ToString();
        }    
    }
}
