﻿using Common.Serialization;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public partial class ReservationTransaction : ILocalizable
    {
        #region Caption i18n Wrapper
        private i18nString captionInternal;

        public i18nString Caption
        {
            get
            {
                if (captionInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Caption_i18n))
                    {
                        captionInternal = (i18nString)SerializationHelper.DeserializeObject(this.Caption_i18n, typeof(i18nString));
                    }
                    else
                    {
                        captionInternal = new i18nString();
                    }
                }

                return captionInternal;
            }
        }

        public string CaptionCurrentLanguage { get; set; }

        public void SetNameValue(string content, string code = null)
        {
            if (captionInternal == null)
                captionInternal = new i18nString();
            this.captionInternal.SetValue(code, content);
            this.Caption_i18n = SerializationHelper.SerializeObject(captionInternal);
            this.CaptionCurrentLanguage = this.Caption.ToString();
        }
        #endregion

        public void PersistI18nValues()
        {
            this.CaptionCurrentLanguage = this.Caption.ToString();
        }    
    }
}
