﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Common.Serialization;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;

namespace DataAccess
{
    public partial class Destination : ILocalizable
    {
        #region DestinationName i18n Wrapper
        private i18nString destinationNameInternal;

        public i18nString DestinationName
        {
            get
            {
                if (destinationNameInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Destination_i18n))
                    {
                        destinationNameInternal = (i18nString)SerializationHelper.DeserializeObject(this.Destination_i18n, typeof(i18nString));
                    }
                    else
                    {
                        destinationNameInternal = new i18nString();
                    }
                }

                return destinationNameInternal;
            }
        }

        public string DestinationNameCurrentLanguage { get; set; }

        public void SetDestinationNameValue(string content, string code = null)
        {
            if (destinationNameInternal == null)
                destinationNameInternal = new i18nString();
            this.destinationNameInternal.SetValue(code, content);
            this.Destination_i18n = SerializationHelper.SerializeObject(destinationNameInternal);
        }
        #endregion

        #region DestinationDescription i18n Wrapper
        private i18nString destinationDescriptionInternal;

        public i18nString DestinationDescription
        {
            get
            {
                if (destinationDescriptionInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Description_i18n))
                    {
                        destinationDescriptionInternal = (i18nString)SerializationHelper.DeserializeObject(this.Description_i18n, typeof(i18nString));
                    }
                    else
                    {
                        destinationDescriptionInternal = new i18nString();
                    }
                }

                return destinationDescriptionInternal;
            }
        }

        public string DestinationDescriptionCurrentLanguage { get; set; }

        public void SetDestinationDescriptionValue(string content, string code = null)
        {
            if (destinationDescriptionInternal == null)
                destinationDescriptionInternal = new i18nString();
            this.destinationDescriptionInternal.SetValue(code, content);
            this.Description_i18n = SerializationHelper.SerializeObject(destinationDescriptionInternal);
        }
        #endregion

        public void PersistI18nValues()
        {
            this.DestinationNameCurrentLanguage = this.DestinationName.ToString();
            this.DestinationDescriptionCurrentLanguage = this.DestinationDescription.ToString();
        }
    }
}
