﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Serialization;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;
using System.Globalization;

namespace DataAccess
{
    public partial class DictionaryCulture : ILocalizable
    {
        #region Name i18n Wrapper
        private i18nString cultureNameInternal = null;

        public i18nString CultureName
        {
            get
            {
                if (cultureNameInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.CultureName_i18n))
                    {
                        cultureNameInternal = (i18nString)SerializationHelper.DeserializeObject(this.CultureName_i18n, typeof(i18nString));
                    }
                    else
                    {
                        cultureNameInternal = new i18nString();
                    }
                }

                return cultureNameInternal;
            }
        }

        public CultureInfo CultureInfo
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(this.CultureCode))
                {
                    CultureInfo ci = new CultureInfo(this.CultureCode);
                    ci.NumberFormat.CurrencyNegativePattern = 1;
                    return ci;
                }
                //default value: English(United States) "en-US"
                return new CultureInfo(1033);
            }            
        }

        public string ISOCurrencySymbol
        {
            get
            {
                try
                {
                    return new RegionInfo(CultureInfo.LCID).ISOCurrencySymbol;
                }
                catch (ArgumentException)
                {
                    return "USD";
                }
            }
        }

        public string CultureNameCurrentLanguage { get; set; }

        public void SetNameValue(string content, string code = null)
        {
            if (cultureNameInternal == null)
                cultureNameInternal = new i18nString();
            this.cultureNameInternal.SetValue(code, content);
            this.CultureName_i18n = SerializationHelper.SerializeObject(cultureNameInternal);
            this.CultureNameCurrentLanguage = this.CultureName.ToString();
        }

        #endregion

        public void PersistI18nValues()
        {
            this.CultureNameCurrentLanguage = this.CultureName.ToString();
        }    
    }
}
