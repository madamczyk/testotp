﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Common.Serialization;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;

namespace DataAccess
{
    public partial class ApplianceGroup : ILocalizable
    {
        #region GroupName i18n Wrapper
        private i18nString titleInternal;

        public i18nString GroupName
        {
            get
            {
                if (titleInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Name_i18n))
                    {
                        titleInternal = (i18nString)SerializationHelper.DeserializeObject(this.Name_i18n, typeof(i18nString));
                    }
                    else
                    {
                        titleInternal = new i18nString();
                    }
                }

                return titleInternal;
            }
        }

        public string GroupNameCurrentLanguage { get; set; }

        public void SetNameValue(string content, string code = null)
        {
            if (titleInternal == null)
                titleInternal = new i18nString();
            this.titleInternal.SetValue(code, content);
            this.Name_i18n = SerializationHelper.SerializeObject(titleInternal);
            this.GroupNameCurrentLanguage = this.GroupName.ToString();
        }
        #endregion

        public void PersistI18nValues()
        {
            this.GroupNameCurrentLanguage = this.GroupName.ToString();
        }
    }
}
