﻿using Common.Serialization;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public partial class PropertyStaticContent : ILocalizable
    {
        #region ContentValueInternational i18n Wrapper
        private i18nString contentValueInternal;

        public i18nString ContentValueInternational
        {
            get
            {
                if (contentValueInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.ContentValue_i18n))
                    {
                        contentValueInternal = (i18nString)SerializationHelper.DeserializeObject(this.ContentValue_i18n, typeof(i18nString));
                    }
                    else
                    {
                        contentValueInternal = new i18nString();
                    }
                }

                return contentValueInternal;
            }
        }

        public string ContentValueCurrentLanguage { get; set; }

        public void SetContentValue(string content, string code = null)
        {
            if (contentValueInternal == null)
                contentValueInternal = new i18nString();
            this.contentValueInternal.SetValue(code, content);
            this.ContentValue_i18n = SerializationHelper.SerializeObject(contentValueInternal);
            this.PersistI18nValues();
        }
        #endregion

        public void PersistI18nValues()
        {
            this.ContentValueCurrentLanguage = this.ContentValueInternational.ToString();
        }    
    }
}
