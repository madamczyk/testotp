﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Common.Serialization;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;

namespace DataAccess
{
    public partial class Unit : ILocalizable
    {
        #region UnitTitle i18n Wrapper
        private i18nString titleInternal;

        public i18nString UnitTitle
        {
            get
            {
                if (titleInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.UnitTitle_i18n.Trim()))
                    {
                        titleInternal = (i18nString)SerializationHelper.DeserializeObject(this.UnitTitle_i18n, typeof(i18nString));
                    }
                    else
                    {
                        titleInternal = new i18nString();
                    }
                }

                return titleInternal;
            }
        }

        public string UnitTitleCurrentLanguage { get; set; }

        public void SetTitleValue(string content, string code = null)
        {
            if (titleInternal == null)
                titleInternal = new i18nString();
            this.titleInternal.SetValue(code, content);
            this.UnitTitle_i18n = SerializationHelper.SerializeObject(titleInternal);
            this.UnitTitleCurrentLanguage = this.UnitTitle.ToString();
        }

        #endregion

        #region UnitDesc i18n Wrapper
        private i18nString descInternal;

        public i18nString UnitDesc
        {
            get
            {
                if (descInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.UnitDesc_i18n.Trim()))
                    {
                        descInternal = (i18nString)SerializationHelper.DeserializeObject(this.UnitDesc_i18n, typeof(i18nString));
                    }
                    else
                    {
                        descInternal = new i18nString();
                    }
                }

                return descInternal;
            }
        }

        public string UnitDescCurrentLanguage { get; set; }

        public void SetDescValue(string content, string code = null)
        {
            if (descInternal == null)
                descInternal = new i18nString();
            this.descInternal.SetValue(code, content);
            this.UnitDesc_i18n = SerializationHelper.SerializeObject(descInternal);
            this.UnitDescCurrentLanguage = this.UnitDesc.ToString();
        }
        #endregion

        public void PersistI18nValues()
        {
            this.UnitTitleCurrentLanguage = this.UnitTitle.ToString();
            this.UnitDescCurrentLanguage = this.UnitDesc.ToString();            
        }
    }
}
