﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Common.Serialization;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;

namespace DataAccess
{
    public partial class Amenity : ILocalizable
    {
        #region Title i18n Wrapper
        private i18nString titleInternal;

        public i18nString Title
        {
            get
            {
                if (titleInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.AmenityTitle_i18n))
                    {
                        titleInternal = (i18nString)SerializationHelper.DeserializeObject(this.AmenityTitle_i18n, typeof(i18nString));
                    }
                    else
                    {
                        titleInternal = new i18nString();
                    }
                }

                return titleInternal;
            }
        }

        public string TitleCurrentLanguage { get; set; }

        public void SetTitleValue(string content, string code = null)
        {
            if (titleInternal == null)
                titleInternal = new i18nString();
            this.titleInternal.SetValue(code, content);
            this.AmenityTitle_i18n = SerializationHelper.SerializeObject(titleInternal);
        }
        #endregion

        #region Description i18n Wrapper
        private i18nString descriptionInternal;

        public i18nString Description
        {
            get
            {
                if (descriptionInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Description_i18n))
                    {
                        descriptionInternal = (i18nString)SerializationHelper.DeserializeObject(this.Description_i18n, typeof(i18nString));
                    }
                    else
                    {
                        descriptionInternal = new i18nString();
                    }
                }

                return descriptionInternal;
            }
        }
        public string DescriptionCurrentLanguage { get; set; }

        public void SetDescriptionValue(string content, string code = null)
        {
            if (descriptionInternal == null)
                descriptionInternal = new i18nString();
            this.descriptionInternal.SetValue(code, content);
            this.Description_i18n = SerializationHelper.SerializeObject(descriptionInternal);
        }
        #endregion

        public void PersistI18nValues()
        {
            this.TitleCurrentLanguage = this.Title.ToString();
            this.DescriptionCurrentLanguage = this.Description.ToString();
        }
    }
}
