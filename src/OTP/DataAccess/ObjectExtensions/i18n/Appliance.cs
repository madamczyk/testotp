﻿using Common.Serialization;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public partial class Appliance : ILocalizable
    {
        #region Name i18n Wrapper
        private i18nString nameInternal;

        public i18nString Name
        {
            get
            {
                if (nameInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Name_i18n))
                    {
                        nameInternal = (i18nString)SerializationHelper.DeserializeObject(this.Name_i18n, typeof(i18nString));
                    }
                    else
                    {
                        nameInternal = new i18nString();
                    }
                }

                return nameInternal;
            }
        }

        public string NameCurrentLanguage { get; set; }

        public void SetNameValue(string content, string code = null)
        {
            if (nameInternal == null)
                nameInternal = new i18nString();
            this.nameInternal.SetValue(code, content);
            this.Name_i18n = SerializationHelper.SerializeObject(nameInternal);
            this.NameCurrentLanguage = this.Name.ToString();
        }
        #endregion

        public void PersistI18nValues()
        {
            this.NameCurrentLanguage = this.Name.ToString();
        }    
    }
}
