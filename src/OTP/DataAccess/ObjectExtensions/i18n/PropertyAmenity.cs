﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Common.Serialization;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;

namespace DataAccess
{
    public partial class PropertyAmenity : ILocalizable
    {
        #region Details i18n Wrapper
        private i18nString detailsInternal;

        public i18nString Details
        {
            get
            {
                if (detailsInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Details_i18n))
                    {
                        detailsInternal = (i18nString)SerializationHelper.DeserializeObject(this.Details_i18n, typeof(i18nString));
                    }
                    else
                    {
                        detailsInternal = new i18nString();
                    }
                }

                return detailsInternal;
            }
        }
        public string DetailsCurrentLanguage { get; set; }

        public void SetDetailsValue(string content, string code = null)
        {
            if (detailsInternal == null)
                detailsInternal = new i18nString();
            this.detailsInternal.SetValue(code, content);
            this.Details_i18n = SerializationHelper.SerializeObject(detailsInternal);
        }
        #endregion

        public void PersistI18nValues()
        {
            this.DetailsCurrentLanguage = this.Details.ToString();
        }
    }
}
