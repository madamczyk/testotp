﻿using Common.Serialization;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public partial class Property : ILocalizable
    {
        #region PropertyName i18n Wrapper
        private i18nString nameInternal;

        public i18nString PropertyName
        {
            get
            {
                if (nameInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.PropertyName_i18n))
                    {
                        nameInternal = (i18nString)SerializationHelper.DeserializeObject(this.PropertyName_i18n, typeof(i18nString));
                    }
                    else
                    {
                        nameInternal = new i18nString();
                    }
                }

                return nameInternal;
            }
        }

        public string PropertyNameCurrentLanguage { get; set; }

        public void SetPropertyNameValue(string content, string code = null)
        {
            if (nameInternal == null)
                nameInternal = new i18nString();
            this.nameInternal.SetValue(code, content);
            this.PropertyName_i18n = SerializationHelper.SerializeObject(nameInternal);
        }

        #endregion

        #region Short_Description i18n Wrapper
        private i18nString sdInternal;

        public i18nString Short_Description
        {
            get
            {
                if (sdInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Short_Description_i18n))
                    {
                        sdInternal = (i18nString)SerializationHelper.DeserializeObject(this.Short_Description_i18n, typeof(i18nString));
                    }
                    else
                    {
                        sdInternal = new i18nString();
                    }
                }

                return sdInternal;
            }
        }

        public string Short_DescriptionCurrentLanguage { get; set; }

        public void SetShortDescriptionValue(string content, string code = null)
        {
            if (sdInternal == null)
                sdInternal = new i18nString();
            this.sdInternal.SetValue(code, content);
            this.Short_Description_i18n = SerializationHelper.SerializeObject(sdInternal);
        }
        #endregion

        #region Long_Description i18n Wrapper
        private i18nString ldInternal;

        public i18nString Long_Description
        {
            get
            {
                if (ldInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Long_Description_i18n))
                    {
                        ldInternal = (i18nString)SerializationHelper.DeserializeObject(this.Long_Description_i18n, typeof(i18nString));
                    }
                    else
                    {
                        ldInternal = new i18nString();
                    }
                }

                return ldInternal;
            }
        }

        public string Long_DescriptionCurrentLanguage { get; set; }

        public void SetLongDescriptionValue(string content, string code = null)
        {
            if (ldInternal == null)
                ldInternal = new i18nString();
            this.ldInternal.SetValue(code, content);
            this.Long_Description_i18n = SerializationHelper.SerializeObject(ldInternal);
        }
        #endregion

        public void PersistI18nValues()
        {
            this.PropertyNameCurrentLanguage = this.PropertyName.ToString();
            this.Long_DescriptionCurrentLanguage = this.Long_Description.ToString();
            this.Short_DescriptionCurrentLanguage = this.Short_Description.ToString();
        }
    }
}
