﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.ObjectExtensions.i18n
{
    public interface ILocalizable
    {
        void PersistI18nValues();
    }
}
