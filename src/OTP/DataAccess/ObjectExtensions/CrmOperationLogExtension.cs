﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public partial class CrmOperationLog
    {
        public string UserFullName
        {
            get
            {
                if (this.User == null)
                {
                    return string.Empty;
                }

                return string.Format("{0} {1}", this.User.Firstname, this.User.Lastname);
            }
        }
    }
}
