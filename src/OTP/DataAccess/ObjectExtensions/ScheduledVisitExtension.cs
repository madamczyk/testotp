﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Serialization;

namespace DataAccess
{
    public partial class ScheduledVisit
    {
        #region Dates List<DateTime> Wrapper
        private List<DateTime> datesListInternal;

        public List<DateTime> DatesList
        {
            get
            {
                if ( datesListInternal == null )
                {
                    if (!String.IsNullOrEmpty(Dates))
                        datesListInternal = (List<DateTime>)SerializationHelper.DeserializeObject(Dates, typeof(List<DateTime>));
                    else
                        datesListInternal = new List<DateTime>();
                }

                datesListInternal.Sort();

                return datesListInternal; 
            }

            set
            {
                datesListInternal = value;
                Dates = SerializationHelper.SerializeObject(datesListInternal);
            }
        }
        #endregion
    }
}
