﻿using DataAccess.ObjectExtensions.i18n;
using System;

namespace DataAccess
{
    public partial class ZipCodeRange : ILocalizable
    {

        /// <summary>
        /// Zip code range formatted as two zip codes joined with dash
        /// </summary>
        public string ZipCodeRangeString { get; set; }

        public void PersistI18nValues()
        {
            this.ZipCodeRangeString = String.Format("{0} - {1}", ZipCodeStart, ZipCodeEnd);
        }
    }
}
