﻿using DataAccess.ObjectExtensions.i18n;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Converters;

namespace DataAccess
{
    public partial class User : ILocalizable
    {
        /// <summary>
        /// List of user roles in which user is logged in
        /// </summary>
        public List<Role> LoggedInRoles { get; set; }

        /// <summary>
        /// First name and Last name
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Preformatted full address of the user
        /// </summary>
        public string FullAddress { get; set; }

        /// <summary>
        /// Preformatted bank account data of the users
        /// </summary>
        public string BankAccountData { get; set; }

        public string FullNameAddress { get; set; }

        /// <summary>
        /// Role in which user is logged in
        /// </summary>
        public Role LoggedInRole 
        {
            get
            {
                return LoggedInRoles != null ? LoggedInRoles.FirstOrDefault() : null;
            }
            set
            {
                if (LoggedInRoles == null)
                {
                    LoggedInRoles = new List<Role>();
                }
                LoggedInRoles.Add(value);
            }
        }

        public void PersistI18nValues()
        {
            this.FullName = string.Format("{0} {1}", this.Firstname, this.Lastname);

            if (!String.IsNullOrWhiteSpace(this.Address1))
                this.FullAddress = string.Format(
                    "{0}, {1}, {2} {3}",
                    this.Address1 + (!String.IsNullOrWhiteSpace(this.Address2) ? ", " + this.Address2 : ""),
                    this.City,
                    this.State,
                    this.ZIPCode);
            else
                this.FullAddress = String.Empty;

            if (!String.IsNullOrWhiteSpace(this.BankName))
                this.BankAccountData = string.Format(
                    "{0}, {1}, {2}",
                    Base64.Decode(this.BankName),
                    Base64.Decode(this.RoutingNumber),
                    Base64.Decode(this.AccountNumber));
            else
                this.BankAccountData = String.Empty;

            this.FullNameAddress = this.FullName + (!String.IsNullOrWhiteSpace(this.FullAddress) ? ", " + this.FullAddress : "");
        }
    }
}
