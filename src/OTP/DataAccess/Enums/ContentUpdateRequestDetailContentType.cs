﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum ContentUpdateRequestDetailContentType
    {
        Image = 0,
        FloorPlan = 1
    }
}
