﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum AmenityIcon
    {
        None = 0,
        Signal = 1,
        CoffeeCup = 2,
        Sun = 3, 
        Swimmer = 4,
        Present = 5,
        Allowances = 6,
        Comfort = 7,
        Entertaiment = 8,
        Parking = 9,
        Sports = 10
    }
}
