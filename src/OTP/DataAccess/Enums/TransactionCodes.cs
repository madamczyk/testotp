﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum TransactionCodes
    {
        Payment = 1,
        DuePayment = 2,
        Tax = 3,
        Revenue = 4,
        SevenDayDiscount = 5
    }
}
