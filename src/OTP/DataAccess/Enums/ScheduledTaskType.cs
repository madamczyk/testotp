﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    [Serializable]
    public enum ScheduledTaskType
    {
        Email = 0,
        CreditCardRedact = 1,
        Reservation = 2,
        ReservationPayment = 3,
        EventLogSummary = 4,
        SessionCleaner = 5,
        CheckInEmail = 6,
        CheckOutEmail = 7,
        SetOfArrivalsForKeyManager = 8,
        KeyDropOffConfirmEmail = 9,
        KeyDropOffAlertEmail = 10,
        KeyDropOffSecondAlertEmail = 11,
        StorageCleaner = 12,
        SecurityDepositAquireRetry = 13
    }
}
