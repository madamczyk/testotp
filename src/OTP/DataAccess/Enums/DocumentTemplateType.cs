﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum DocumentType
    {
        ReservationAgreementForOwner = 1,
        ReservationAgreementForGuest = 2    
    }
}
