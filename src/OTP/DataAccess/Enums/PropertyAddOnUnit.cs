﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    [Serializable]
    public enum PropertyAddOnUnit
    {
        PerDay = 1,
        PerWeek = 2,
        PerPersonPerDay = 3,
        PerPersonPerWeek = 4,
        PerStay = 5
    }
}
