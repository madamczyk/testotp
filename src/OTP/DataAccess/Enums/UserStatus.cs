﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum UserStatus
    {
        Requested = 1,
        Inactive,
        Active,
        Blocked,
        Deleted
    }
}
