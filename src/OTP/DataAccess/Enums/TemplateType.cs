﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum TemplateType
    {
        //email template types
        UserSignupConfirmation = 0,
        UserSignupConfirmationPassword = 1,
        OwnerJoinLetter = 2,        
        OwnerActivationEmail = 4,
        PasswordResetEmail = 5,
        EVSServiceError = 6,
        EVSVerificationFailed = 7,
        ReservationConfirmationForOwner = 8,
        ReservationConfirmationForGuest = 9,
        AgeVerificationFailed = 10,
        PasswordResetBookingEmail = 11,
        ReservationCancelationConfirmation = 12,
        HomeownerContactEmail = 13,
        EventLogSummaryEmail = 14,
        TicketSystemJoinEmail = 15,
        UserAccountAdded = 16,
        CheckInLocboxEmail = 17,
        CheckInPickUpFromKeyHandlerEmail = 18,
        CheckOutLockboxEmail = 19,
        CheckOutPickUpFromKeyHandlerEmail = 20,
        SetOfArrivalsForKeyManagerEmail = 21,
        KeyDropOffLockboxEmail = 22,
        KeyDropOffPickUpFromKeyHandlerEmail = 23,
        KeyDropOffAlertEmail = 24,
        KeyDropOffSecondAlertEmail = 25,
        ReservationNoFunds = 26,
        CrmAddLeadOperationErrorEmail = 27,
        CrmConvertLeadOperationErrorEmail = 28,
        PropertySignOff = 29,


        //document templates types
        ReservationAgreement = 100,
        TripDetails = 101,
        Invoice = 102,
        ReservationAgreementOnDateChange = 103,

    }
}
