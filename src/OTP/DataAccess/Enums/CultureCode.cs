﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace DataAccess.Enums
{
    public static class CultureCode
    {
        public static readonly string en_US = "en-US";
        public static readonly string en_GB = "en-GB";
        public static readonly string de_DE = "de-DE";
        public static readonly string pl_PL = "pl-PL";
    }
}
