﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum TemplateType
    {
        UserSignupConfirmation = 0,
        OwnerJoinLetter = 1,
        OwnerActivationEmail = 2,
        PasswordResetEmail = 3,
        //document templates
        ReservationAgreementForOwner = 4,
        ReservationAgreementForGuest = 5 
    }
}
