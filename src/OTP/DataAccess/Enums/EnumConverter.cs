﻿using DataAccess.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public static class EnumConverter
    {
        public static string GetValue(UnitCleaningStatus cleaningStatus)
        {
            switch (cleaningStatus)
            {
                case UnitCleaningStatus.NotCleaned:
                    return EnumResources.Unit_CleaningStatus_NotCleaned;
                case UnitCleaningStatus.Cleaned:
                    return EnumResources.Unit_CleaningStatus_Cleaned;
                default:
                    return cleaningStatus.ToString();
            }
        }

        public static string GetValue(PropertyStaticContentType type)
        {
            switch (type)
            {
                case PropertyStaticContentType.FullScreen:
                    return EnumResources.StaticContent_FullScreen;
                case PropertyStaticContentType.FullScreen_Retina:
                    return EnumResources.StaticContent_FullScreenRetina;
                case PropertyStaticContentType.GalleryImage:
                    return EnumResources.StaticContent_GalleryImage;
                case PropertyStaticContentType.GalleryImage_Retina:
                    return EnumResources.StaticContent_GalleryImageRetina;
                case PropertyStaticContentType.ListView:
                    return EnumResources.StaticContent_ListView;
                case PropertyStaticContentType.ListView_Retina:
                    return EnumResources.StaticContent_ListViewRetina;
                case PropertyStaticContentType.Thumbnail:
                    return EnumResources.StaticContent_Thumbnail;
                case PropertyStaticContentType.Thumbnail_Retina:
                    return EnumResources.StaticContent_ThumbnailRetina;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(UnitBlockingType type)
        {
            switch (type)
            {
                case UnitBlockingType.Reservation:
                    return EnumResources.Unit_BlockingType_Reservation;
                case UnitBlockingType.OwnerStay:
                    return EnumResources.Unit_BlockingType_OwnerStay;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(UserStatus userStatus)
        {
            switch (userStatus)
            {
                case UserStatus.Active:
                    return EnumResources.UserStatus_Active;
                case UserStatus.Inactive:
                    return EnumResources.UserStatus_Inactive;
                case UserStatus.Blocked:
                    return EnumResources.UserStatus_Blocked;
                case UserStatus.Deleted:
                    return EnumResources.UserStatus_Deleted;
                case UserStatus.Requested:
                    return EnumResources.UserStatus_Requested;
                default:
                    return userStatus.ToString();
            }
        }

        public static string GetValue(RoleLevel roleLevel)
        {
            switch (roleLevel)
            {
                case RoleLevel.Administrator:
                    return EnumResources.RoleLevel_Administrator;
                case RoleLevel.Guest:
                    return EnumResources.RoleLevel_Guest;
                case RoleLevel.Owner:
                    return EnumResources.RoleLevel_Owner;
                case RoleLevel.Manager:
                    return EnumResources.RoleLevel_Manager;
                case RoleLevel.DataEntryManager:
                    return EnumResources.RoleLevel_DataEntryManager;
                case RoleLevel.KeyManager:
                    return EnumResources.RoleLevel_KeyManager;
                case RoleLevel.PropertyManager:
                    return EnumResources.RoleLevel_PropertyManager;
                default:
                    return roleLevel.ToString();
            }
        }

        public static string GetValue(PropertyAddOnUnit unit)
        {
            switch (unit)
            {
                case PropertyAddOnUnit.PerDay:
                    return EnumResources.AddOn_Unit_PerDay;
                case PropertyAddOnUnit.PerPersonPerDay:
                    return EnumResources.AddOn_Unit_PerPersonPerDay;
                case PropertyAddOnUnit.PerPersonPerWeek:
                    return EnumResources.AddOn_Unit_PerPersonPerWeek;
                case PropertyAddOnUnit.PerStay:
                    return EnumResources.AddOn_Unit_PerStay;
                case PropertyAddOnUnit.PerWeek:
                    return EnumResources.AddOn_Unit_PerWeek;
                default:
                    return unit.ToString();
            }            
        }

        public static string GetValue(MessageStatus status)
        {
            switch (status)
            {
                case MessageStatus.Completed:
                    return EnumResources.MessageStatus_Completed;
                case MessageStatus.Error:
                    return EnumResources.MessageStatus_Error;
                case MessageStatus.New:
                    return EnumResources.MessageStatus_New;
                case MessageStatus.Pending:
                    return EnumResources.MessageStatus_Pending;
                case MessageStatus.Rejected:
                    return EnumResources.MessageStatus_Rejected;
                default:
                    return status.ToString();
            }
        }

        public static string GetValue(TemplateType type)
        {
            switch (type)
            {
                case TemplateType.UserSignupConfirmation:
                    return EnumResources.TemplateType_UserSignup;
                case TemplateType.UserSignupConfirmationPassword:
                    return EnumResources.TemplateType_UserSignupConfirmationPassword;  
                case TemplateType.OwnerJoinLetter:
                    return EnumResources.TemplateType_OwnerJoin;
                case TemplateType.OwnerActivationEmail:
                    return EnumResources.TemplateType_OwnerActivation;
                case TemplateType.PasswordResetEmail:
                    return EnumResources.TemplateType_PasswordReset;
                case TemplateType.EVSServiceError:
                    return EnumResources.TemplateType_EVSError;
                case TemplateType.EVSVerificationFailed:
                    return EnumResources.TemplateType_EVSVerificationFailed;
                case TemplateType.ReservationConfirmationForOwner:
                    return EnumResources.TemplateType_ReservationConfirmationOwner;   
                case TemplateType.ReservationConfirmationForGuest:
                    return EnumResources.TemplateType_ReservationConfirmationGuest; 
                case TemplateType.AgeVerificationFailed:
                    return EnumResources.TemplateType_AgeVerificationFailed;
                case TemplateType.PasswordResetBookingEmail:
                    return EnumResources.TemplateType_PasswordResetBookingWizard; 

                case TemplateType.ReservationCancelationConfirmation:
                    return EnumResources.TemplateType_ReservationCancelationConfirmation;
                case TemplateType.HomeownerContactEmail:
                    return EnumResources.TemplateType_HomeownerContactEmail;
                case TemplateType.EventLogSummaryEmail:
                    return EnumResources.TemplateType_EventLogSummaryEmail;
                case TemplateType.TicketSystemJoinEmail:
                    return EnumResources.TemplateType_TicketSystemJoinEmail;
                case TemplateType.UserAccountAdded:
                    return EnumResources.TemplateType_UserAccountAdded;

      
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(BookingStatus type)
        {
            switch (type)
            {
                case BookingStatus.TentativeBooking:
                    return EnumResources.BookingStatus_TentativeBooking;
                case BookingStatus.ReceivedBooking:
                    return EnumResources.BookingStatus_ReceivedBooking;
                case BookingStatus.FinalizedBooking:
                    return EnumResources.BookingStatus_FinalizedBooking;
                case BookingStatus.CheckedIn:
                    return EnumResources.BookingStatus_CheckedIn;
                case BookingStatus.CheckedOut:
                    return EnumResources.BookingStatus_CheckedOut;
                case BookingStatus.Cancelled:
                    return EnumResources.BookingStatus_Cancelled;
                case BookingStatus.Withdrew:
                    return EnumResources.BookingStatus_Withdrew;
                case BookingStatus.DamageInvestigation:
                    return EnumResources.BookingStatus_DamageInvestigation;
                case BookingStatus.DatesChanged:
                    return EnumResources.BookingStatus_DatesChanged;
                case BookingStatus.NoFunds:
                    return EnumResources.BookingStatus_NoFunds;
                case BookingStatus.Completed:
                    return EnumResources.BookingStatus_Completed;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(EventCategory type)
        {
            switch (type)
            {
                case EventCategory.Error:
                    return EnumResources.EventCategory_Error;
                case EventCategory.Warning:
                    return EnumResources.EventCategory_Warning;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(EventLogSource type)
        {
            switch (type)
            {
                case EventLogSource.Intranet:
                    return EnumResources.EventLogSource_Intranet;
                case EventLogSource.IntranetProcessor:
                    return EnumResources.EventLogSource_IntranetProcessor;
                case EventLogSource.Extranet:
                    return EnumResources.EventLogSource_Extranet;
                case EventLogSource.PaymentGateway:
                    return EnumResources.EventLogSource_PaymentGateway;
                case EventLogSource.EVS:
                    return EnumResources.EventLogSource_EVS;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(PaymentGatewayOperationType type)
        {
            switch (type)
            {
                case PaymentGatewayOperationType.AddPaymentMethod:
                    return EnumResources.PaymentGatewayOperationType_AddPaymentMethod;
                case PaymentGatewayOperationType.Authorize:
                    return EnumResources.PaymentGatewayOperationType_Authorize;
                case PaymentGatewayOperationType.Capture:
                    return EnumResources.PaymentGatewayOperationType_Capture;
                case PaymentGatewayOperationType.Purchase:
                    return EnumResources.PaymentGatewayOperationType_Purchase;
                case PaymentGatewayOperationType.Redact:
                    return EnumResources.PaymentGatewayOperationType_Redact;
                case PaymentGatewayOperationType.Retain:
                    return EnumResources.PaymentGatewayOperationType_Retain;
                case PaymentGatewayOperationType.Void:
                    return EnumResources.PaymentGatewayOperationType_Void;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(ScheduleIntervalType type)
        {
            switch (type)
            {
                case ScheduleIntervalType.Seconds:
                    return EnumResources.ScheduleIntervalType_Seconds;
                case ScheduleIntervalType.Minutes:
                    return EnumResources.ScheduleIntervalType_Minutes;
                case ScheduleIntervalType.Hours:
                    return EnumResources.ScheduleIntervalType_Hours;
                case ScheduleIntervalType.Days:
                    return EnumResources.ScheduleIntervalType_Days;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(ScheduledTaskType type)
        {
            switch (type)
            {
                case ScheduledTaskType.CreditCardRedact:
                    return EnumResources.ScheduledTaskType_CreditCardRedact;
                case ScheduledTaskType.Email:
                    return EnumResources.ScheduledTaskType_Email;
                case ScheduledTaskType.EventLogSummary:
                    return EnumResources.ScheduledTaskType_EventLogSummary;
                case ScheduledTaskType.Reservation:
                    return EnumResources.ScheduledTaskType_Reservation;
                case ScheduledTaskType.ReservationPayment:
                    return EnumResources.ScheduledTaskType_ReservationPayment;
                case ScheduledTaskType.SessionCleaner:
                    return EnumResources.ScheduledTaskType_SessionCleaner;
                case ScheduledTaskType.CheckInEmail:
                    return EnumResources.ScheduledTaskType_CheckInEmail;
                case ScheduledTaskType.CheckOutEmail:
                    return EnumResources.ScheduledTaskType_CheckOutEmail;
                case ScheduledTaskType.SetOfArrivalsForKeyManager:
                    return EnumResources.ScheduledTaskType_SetOfArrivalsForKeyManager;
                case ScheduledTaskType.KeyDropOffConfirmEmail:
                    return EnumResources.ScheduledTaskType_KeyDropOffConfirmEmail;
                case ScheduledTaskType.KeyDropOffAlertEmail:
                    return EnumResources.ScheduledTaskType_KeyDropOffAlertEmail;
                case ScheduledTaskType.KeyDropOffSecondAlertEmail:
                    return EnumResources.ScheduledTaskType_KeyDropOffSecondAlertEmail;
                case ScheduledTaskType.StorageCleaner:
                    return EnumResources.ScheduledTaskType_StorageCleaner;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(ScheduleType type)
        {
            switch (type)
            {
                case ScheduleType.Every:
                    return EnumResources.ScheduleType_Every;
                case ScheduleType.EverydayAt:
                    return EnumResources.ScheduleType_EverydayAt;
                case ScheduleType.Once:
                    return EnumResources.ScheduleType_Once;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(TaskDetailsMessageType type)
        {
            switch (type)
            {
                case TaskDetailsMessageType.Error:
                    return EnumResources.TaskDetailsMessageType_Error;
                case TaskDetailsMessageType.Info:
                    return EnumResources.TaskDetailsMessageType_Info;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(TaskExecutionResult type)
        {
            switch (type)
            {
                case TaskExecutionResult.Failure:
                    return EnumResources.TaskExecutionResult_Failure;
                case TaskExecutionResult.Success:
                    return EnumResources.TaskExecutionResult_Success;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(PropertyRawRecordType type)
        {
            switch (type)
            {
                case PropertyRawRecordType.Image_FullScreen:
                    return EnumResources.RawRecordType_Image_FullScreen;
                case PropertyRawRecordType.Image_FullScreen_Retina:
                    return EnumResources.RawRecordType_Image_FullScreen_Retina;
                case PropertyRawRecordType.Image_GalleryImage:
                    return EnumResources.RawRecordType_Image_GalleryImage;
                case PropertyRawRecordType.Image_GalleryImage_Retina:
                    return EnumResources.RawRecordType_Image_GalleryImage_Retina;
                case PropertyRawRecordType.Image_ListView:
                    return EnumResources.RawRecordType_Image_ListView;
                case PropertyRawRecordType.Image_ListView_Retina:
                    return EnumResources.RawRecordType_Image_ListView_Retina;
                case PropertyRawRecordType.Image_Thumbnail:
                    return EnumResources.RawRecordType_Image_Thumbnail;
                case PropertyRawRecordType.Image_Thumbnail_Retina:
                    return EnumResources.RawRecordType_Image_Thumbnail_Retina;
                case PropertyRawRecordType.FloorPlan:
                    return EnumResources.RawRecordType_FloorPlan;
                case PropertyRawRecordType.Panorama:
                    return EnumResources.RawRecordType_Panorama;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(KeyProcessType type)
        {
            switch (type)
            {
                case KeyProcessType.Lockbox:
                    return EnumResources.KeyProcessType_Lockbox;
                case KeyProcessType.KeyHandler:
                    return EnumResources.KeyProcessType_KeyHandler;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(ContentUpdateRequestDetailContentType type)
        {
            switch (type)
            {
                case ContentUpdateRequestDetailContentType.Image:
                    return EnumResources.ContentUpdateRequestDetailContentType_Image;
                case ContentUpdateRequestDetailContentType.FloorPlan:
                    return EnumResources.ContentUpdateRequestDetailContentType_FloorPlan;
                default:
                    return type.ToString();
            }
        }

        public static string GetValue(CrmOparationLogsType type)
        {
            switch (type)
            {
                case CrmOparationLogsType.AddLead:
                    return EnumResources.CrmOparationLogsType_AddLead;
                case CrmOparationLogsType.ConvertLeadIntoAccount:
                    return EnumResources.CrmOparationLogsType_ConvertLead;
                case CrmOparationLogsType.DeleteAccount:
                    return EnumResources.CrmOparationLogsType_DeleteAccount;
                case CrmOparationLogsType.DeleteContact:
                    return EnumResources.CrmOparationLogsType_DeleteContact;
                case CrmOparationLogsType.DeleteLead:
                    return EnumResources.CrmOparationLogsType_DeleteLead;
                default:
                    return type.ToString();
            }
        }
    }
}
