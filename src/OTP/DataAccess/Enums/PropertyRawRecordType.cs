﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum PropertyRawRecordType
    {
        Image_Thumbnail = 0,
        Image_Thumbnail_Retina = 1,
        Image_FullScreen = 2,
        Image_FullScreen_Retina = 3,
        Image_ListView = 4,
        Image_ListView_Retina = 5,
        Image_GalleryImage = 6,
        Image_GalleryImage_Retina = 7,
        
        FloorPlan = 8,
        Panorama = 9
    }
}
