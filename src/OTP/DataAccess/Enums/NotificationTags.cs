﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum NotificationTag
    {
        LoggedUserLogin,
        LoggedUserFirstName,
        LoggedUserLastName,
        LoggedUserEmail,
        LoggedUserPhoneNumber,
        LoggedUserPassword,
        LoggedUserActivationLink,
    }
}
