﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum TaskDetailsMessageType
    {
        Info = 0,
        Error = 1
    }
}
