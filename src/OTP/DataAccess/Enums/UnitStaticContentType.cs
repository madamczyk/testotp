﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum UnitStaticContentType
    {
	    Bedrooms = 10,
	    Bathrooms = 11,
	    LivingAreasAndDining = 12,
        KitchenAndLaundry = 13,
        WifiName = 14,
        WifiPassword = 15
    }
}
