﻿using Common.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum ArrivalTimeRange
    {
        From2PM = 1,
        From4PM = 2,
        From6PM = 3,
        From8PM = 4,
        From10PM = 5,
    }

    public static class ArrivalTimeRangeExtensions
    {
        public static string GetTextRepresentation(this ArrivalTimeRange enumVal)
        {
            string textRepresentation = string.Empty;

            switch (enumVal)
            {
                case ArrivalTimeRange.From2PM:
                    textRepresentation = ArrivalTimeRangeResource.ArrivalPeriodFrom2PM;
                    break;

                case ArrivalTimeRange.From4PM:
                    textRepresentation = ArrivalTimeRangeResource.ArrivalPeriodFrom4PM;
                    break;

                case ArrivalTimeRange.From6PM:
                    textRepresentation = ArrivalTimeRangeResource.ArrivalPeriodFrom6PM;
                    break;

                case ArrivalTimeRange.From8PM:
                    textRepresentation = ArrivalTimeRangeResource.ArrivalPeriodFrom8PM;
                    break;

                case ArrivalTimeRange.From10PM:
                    textRepresentation = ArrivalTimeRangeResource.ArrivalPeriodFrom10PM;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();

            }

            return textRepresentation;
        }

        public static List<string> GetAllValues()
        {
            return Enum.GetValues(typeof(ArrivalTimeRange)).Cast<ArrivalTimeRange>().
                Select(s => s.GetTextRepresentation()).ToList();
        }

        public static Dictionary<int, string> GetAllValuesWithKeys()
        {
            return Enum.GetValues(typeof(ArrivalTimeRange)).Cast<ArrivalTimeRange>().ToDictionary(mc => (int)mc, mc => mc.GetTextRepresentation());
        }
    }
}
