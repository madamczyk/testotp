﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum PropertyStaticContentType
    { 
        //images
        Thumbnail = 0,          //for My Trips
        Thumbnail_Retina = 1,
        FullScreen = 2,         //for Landing Page
        FullScreen_Retina = 3,
        ListView = 4,           //for List View
        ListView_Retina = 5,
        GalleryImage = 6,       //for Gallery
        GalleryImage_Retina = 7,
        
        FloorPlan = 8,
        Panorama = 9,
        
        //text content
        SquareFootage = 10,
        Feature = 11,
        LocalAreaDescription = 12,
        DistanceToMainAttraction = 13,
        ArrivalInformation = 14,
        ParkingInformation = 15,
        DirectionToAirport = 16
    }
}
