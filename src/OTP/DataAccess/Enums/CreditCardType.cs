﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum CreditCardType
    {
        visa = 1,
        mastercard = 2,
        amex = 3
    }
}
