﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum RoleLevel
    {
        Guest =0 ,
        Owner =1,
        Manager =2,
        Administrator = 3,
        KeyManager = 4,
        PropertyManager = 5,
        DataEntryManager = 6
    }
}
