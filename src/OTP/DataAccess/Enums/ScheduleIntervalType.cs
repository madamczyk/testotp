﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum ScheduleIntervalType
    {
        Seconds = 0,
        Minutes = 1,
        Hours = 2,
        Days = 3
    }
}
