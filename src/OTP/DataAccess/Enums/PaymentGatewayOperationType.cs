﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum PaymentGatewayOperationType
    {
        AddPaymentMethod = 0,
        Retain = 1,
        Authorize = 2,
        Capture = 3 ,        
        Redact = 4,
        Void = 5,
        Purchase = 6
    }
}
