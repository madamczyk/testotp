﻿namespace DataAccess.Enums
{
    public enum CrmOparationLogsType
    {
        AddLead = 1,
        ConvertLeadIntoAccount = 2,
        DeleteAccount = 3,
        DeleteLead = 4,
        DeleteContact = 5
    }
}
