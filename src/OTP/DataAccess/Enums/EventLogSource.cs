﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum EventLogSource
    {
        Extranet,
        Intranet,
        EVS,
        PaymentGateway,
        IntranetProcessor
    }
}
