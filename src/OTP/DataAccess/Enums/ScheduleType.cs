﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum ScheduleType
    {
        Every = 0,
        EverydayAt = 1,
        Once = 2
    }
}
