﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum UnitBlockingType
    {
        Available = 0, //do not save unit blockade with this type
        Reservation = 1,
        OwnerStay = 2,
    }
}
