﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum TaskExecutionResult
    {
        Success = 0,
        Failure = 1
    }
}
