﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum BookingStatus
    {
        TentativeBooking = 0,
        ReceivedBooking = 1,
        FinalizedBooking = 2,
        CheckedIn = 3,
        CheckedOut = 4,
        Cancelled = 5,
        Completed = 6,
        Withdrew = 7,
        NoFunds = 8,
        DatesChanged = 9,
        DamageInvestigation = 10
    }
}
