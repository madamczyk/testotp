﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum ReservationDetailsBase
    {
        PerNight = 1,
        PerWeek = 2,
        PerStay = 3,
        PerNightPerson = 4,
        PerWeekPerson = 5
    }
}
