﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Enums
{
    public enum ReservationDetailsType
    {
        Stay = 1,
        AddOn = 2,
        ServiceProvider = 3
    }
}
