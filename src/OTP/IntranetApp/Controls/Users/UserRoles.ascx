﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserRoles.ascx.cs" Inherits="IntranetApp.Controls.Users.UserRoles" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="pnlData" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>
        
    <telerik:RadGrid
        AutoGenerateColumns="False" 
        ID="rgOwners" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="False" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="ownersLinqDS"
		ClientSettings-EnablePostBackOnRowClick="false"
        OnItemDataBound="rgOwners_ItemDataBound"
        OnDataBound="rgOwners_DataBound"
        OnInsertCommand="rgRoleLevels_InsertCommand"
        OnItemCommand="rgOwners_ItemCommand"
        ClientSettings-ClientEvents-OnColumnClick="disableWarning">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="UserRoleId" DataSourceID="ownersLinqDS" CommandItemDisplay="Bottom">
			<Columns>
				<telerik:GridBoundColumn DataField="Role.RoleLevel" FilterControlWidth="150px" UniqueName="RoleLevel" HeaderText="Role Level" SortExpression="Role.RoleLevel" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Status" UniqueName="Status" InitializeTemplatesFirst="false" SortExpression="Role.Status" ForceExtractValue="Always">
					<HeaderStyle Width="100px"></HeaderStyle>
					<ItemStyle CssClass="link" />
                    <ItemTemplate>
                        <telerik:RadComboBox runat="server" ID="rcbUserRoleStatus" AutoPostBack="true" EnableViewState="true" OnSelectedIndexChanged="rcbUserRoleStatus_SelectedIndexChanged" Width="90%" />
                     </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>
			</Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="UserRole"
                        HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                        EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblRoleLevel" meta:resourceKey="lblRoleLevel"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadComboBox ID="rcbRoleLevel" runat="server" OnPreRender="rcbRoleLevel_PreRender" ></telerik:RadComboBox>
                                <asp:RequiredFieldValidator ValidationGroup="UserRole" ID="RequiredFieldValidator1"
                                Text="*" ControlToValidate="rcbRoleLevel" runat="server" Display="Static"
                                CssClass="error" meta:resourceKey="ValRoleLevel" />
                            </td>
                        </tr>                                
                        <tr>
                            <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                <div style="position: relative; width: 415px;">
                                    <!-- workaround for issue with RadButtons not being aligned vertically -->
                                    <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="UserRole"
                                        VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        CausesValidation="true">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                    </telerik:RadButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>

			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="Role.RoleLevel" SortOrder="Ascending" />
            </SortExpressions>
		</MasterTableView>
		<HeaderContextMenu EnableImageSprites="True">
		</HeaderContextMenu>
	</telerik:RadGrid>
</asp:Panel>

<otpDS:SimpleDataSource ID="ownersLinqDS" runat="server" />