﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using IntranetApp.Enums;

namespace IntranetApp.Controls.Users
{
    public partial class UsersList : System.Web.UI.UserControl
    {
        public int? CountryFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["CountryFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["CountryFilterSelectedValue"] as int?;
            }
            set { ViewState["CountryFilterSelectedValue"] = value; }
        }
        
        public int? StatusFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["StatusFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["StatusFilterSelectedValue"] as int?;
            }
            set { ViewState["StatusFilterSelectedValue"] = value; }
        }

        public RoleLevel? UsersRoleLevel
        {
            get 
            {
                if (ViewState["UsersRoleLevel"] != null)
                    return (RoleLevel)ViewState["UsersRoleLevel"];
                else
                    return RoleLevel.Owner;
            }
            set { ViewState["UsersRoleLevel"] = value; }
        }

        #region Methods

        private void BindGrid()
        {
            this.ownersLinqDS.DataResolver = () =>
            {
                IQueryable<UserRole> result = RequestManager.Services.UsersService.GetUserRolesByRoleLevel(UsersRoleLevel.Value).AsQueryable();

                rgOwners.ApplyFilter<UserRole>(ref result, "Firstname", (q, f) => q.Where(p => p.User.Firstname.ToLower().Contains(f.ToLower())));
                rgOwners.ApplyFilter<UserRole>(ref result, "Lastname", (q, f) => q.Where(p => p.User.Lastname.ToLower().Contains(f.ToLower())));
                rgOwners.ApplyFilter<UserRole>(ref result, "email", (q, f) => q.Where(p => p.User.email.ToLower().Contains(f.ToLower())));
                rgOwners.ApplyFilter<UserRole>(ref result, "City", (q, f) => q.Where(p => !String.IsNullOrEmpty(p.User.City) && p.User.City.ToLower().Contains(f.ToLower())));

                if (this.StatusFilterSelectedValue.HasValue)
                    result = result.Where(u => u.Status == this.StatusFilterSelectedValue.Value);

                if (this.CountryFilterSelectedValue.HasValue)
                    result = result.Where(u => u.User.DictionaryCountry.CountryId == this.CountryFilterSelectedValue.Value);

                return result.ToList();
            };
        }

        private string ObjectName(bool plural = false)
        {
            if (UsersRoleLevel.HasValue)
            {
                return EnumConverter.GetValue(UsersRoleLevel.Value) + "s";
            }
            else
                return string.Empty;
        }

        private void SetNames()
        {
            this.lblFormTitle.Text = String.Format((string)GetLocalResourceObject("lblFormTitle.Text"), ObjectName());
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
            SetNames();
        }

        protected void rgOwners_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int userRoleId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserRoleId"].ToString());
                UserRole userRole = RequestManager.Services.UsersService.GetUserByUserRoleId(userRoleId);
                if (userRole == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_User"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_User"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    rgOwners.Rebind();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/Users/UserDetails.aspx");
                    sb.Append("?mode=");
                    sb.Append((int)FormMode.Edit);
                    sb.Append("&userId=");
                    sb.Append(userRole.User.UserID);
                    sb.Append("&role=");
                    sb.Append((int)UsersRoleLevel);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int userRoleId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserRoleId"].ToString());
                UserRole userRole = RequestManager.Services.UsersService.GetUserByUserRoleId(userRoleId);
                if (userRole == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_User"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_User"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    rgOwners.Rebind();
                }
                else if (RequestManager.Services.UsersService.HasAnyDependencies(userRole.User.UserID))
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_User"), userRole.User.FullName);
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_User"), userRole.User.FullName);

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");

                    return;
                }
                else
                    RequestManager.Services.UsersService.DeleteUserByRoleId(userRoleId);
            }
        }

        protected void rgOwners_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                UserRole obj = (UserRole)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgOwners.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_User"), obj.User.FullName), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_User"), obj.User.FullName));
            }
        }

        protected void rgOwners_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgOwners.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgOwners.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgOwners.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgOwners.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgOwners.MasterTableView.Rebind();
            }
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("~/Forms/Users/UserDetails.aspx");
            sb.Append("?mode=");
            sb.Append((int)FormMode.Add);
            sb.Append("&role=");
            sb.Append((int)UsersRoleLevel);
            Response.Redirect(sb.ToString());
        }

        protected void rcbFilterCountry_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;
                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (DictionaryCountry country in RequestManager.Services.DictionaryCountryService.GetCountries())
                    combo.Items.Add(new RadComboBoxItem(country.CountryName.ToString(), country.CountryId.ToString()));
            }
        }

        protected void rcbFilterCountry_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CountryFilterSelectedValue = int.Parse(e.Value);

            rgOwners.MasterTableView.Rebind();
        }

        protected void rcbFilterCountry_PreRender(object sender, EventArgs e)
        {
            if (CountryFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;
                combo.SelectedValue = CountryFilterSelectedValue.ToString();
            }
        }

        protected void rcbFilterStatus_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;
                combo.Items.Add(new RadComboBoxItem("", "-1"));
                
                foreach (UserStatus us in Enum.GetValues(typeof(UserStatus)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(us), ((int)us).ToString()));
            }
        }

        protected void rcbFilterStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            StatusFilterSelectedValue = int.Parse(e.Value);
            
            rgOwners.MasterTableView.Rebind();
        }

        protected void rcbFilterStatus_PreRender(object sender, EventArgs e)
        {
            if (StatusFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;
                combo.SelectedValue = StatusFilterSelectedValue.ToString();
            }
        }

        #endregion Event Handlers
    }
}