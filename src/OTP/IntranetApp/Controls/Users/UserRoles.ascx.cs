﻿using BusinessLogic.Enums;
using BusinessLogic.Managers;
using BusinessLogic.Objects.Templates.Email;
using Common;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Users
{
    public partial class UserRoles : System.Web.UI.UserControl
    {
        public int? UserId
        {
            get { return ViewState["UserId"] as int?; }
            set { ViewState["UserId"] = value; }
        }

        #region Methods

        private void BindGrid()
        {
            this.ownersLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.UserRole> result = RequestManager.Services.UsersService.GetUserRolesByUserId(this.UserId.Value).AsQueryable();
                return result.ToList();
            };
        }

        private void SendActivationEmail(User user, UserRole role)
        {
            if (role.Role.RoleLevel == (int)RoleLevel.Guest)
            {
                UserActivationEmail guestActivationEmail = new UserActivationEmail(user.email);
                guestActivationEmail.UserFirstName = user.Firstname;
                guestActivationEmail.ActivationLinkUrl = string.Format("{0}://{1}{2}/{3}",
                            Request.Url.Scheme,
                            RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.ExtranetURL),
                            "/Account/Activation",
                            role.ActivationToken);
                guestActivationEmail.ActivationLinkTitle = GetLocalResourceObject("ActivationLinkText").ToString();

                RequestManager.Services.MessageService.AddEmail(guestActivationEmail, user.DictionaryCulture.CultureCode); // should be (LanguageCode)Enum.Parse(typeof(LanguageCode), Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)
            }
            else if (role.Role.RoleLevel == (int)RoleLevel.Owner ||
                role.Role.RoleLevel == (int)RoleLevel.KeyManager ||
                role.Role.RoleLevel == (int)RoleLevel.PropertyManager
                )
            {
                string tempPassword = RequestManager.Services.AuthorizationService.GeneratePassword();

                user.Password = MD5Helper.Encode(tempPassword);
                user.IsGeneratedPassword = true;

                OwnerActivationEmail ownerActivationEmail = new OwnerActivationEmail(user.email);
                ownerActivationEmail.UserFirstName = user.Firstname;
                ownerActivationEmail.UserLogin = user.email;
                ownerActivationEmail.UserPassword = tempPassword;
                ownerActivationEmail.ActivationLinkTitle = GetLocalResourceObject("ActivationLinkText").ToString();
                ownerActivationEmail.ActivationLinkUrl = string.Format("{0}://{1}{2}/{3}",
                            Request.Url.Scheme,
                            RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.ExtranetURL),
                            "/Account/Activation",
                            role.ActivationToken);

                RequestManager.Services.MessageService.AddEmail(ownerActivationEmail, user.DictionaryCulture.CultureCode);
            }

        }

        private int ReadGridItemData(GridItem item)
        {
            return int.Parse((item.FindControl("rcbRoleLevel") as RadComboBox).SelectedValue);
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }
        
        protected void rgOwners_ItemDataBound(object sender, GridItemEventArgs e)
        {
            DataAccess.UserRole ur = e.Item.DataItem as DataAccess.UserRole;
            GridDataItem item = e.Item as GridDataItem;

            if (item != null)
            {
                RadComboBox rcb = item["Status"].FindControl("rcbUserRoleStatus") as RadComboBox;
                
                item["RoleLevel"].Text = EnumConverter.GetValue((RoleLevel)(ur.Role.RoleLevel));

                rcb.Attributes.Add("roleId", ur.UserRoleId.ToString());
                // status check
                if (ur.Status == (int)UserStatus.Requested)
                {
                    rcb.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(UserStatus.Requested), ((int)UserStatus.Requested).ToString()));
                    rcb.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(UserStatus.Inactive), ((int)UserStatus.Inactive).ToString()));
                }
                else if (ur.Status == (int)UserStatus.Inactive)
                {
                    rcb.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(UserStatus.Inactive), ((int)UserStatus.Inactive).ToString()));
                    rcb.Enabled = false;
                }
                else if (ur.Status == (int)UserStatus.Active)
                {
                    rcb.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(UserStatus.Active), ((int)UserStatus.Active).ToString()));
                    rcb.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(UserStatus.Blocked), ((int)UserStatus.Blocked).ToString()));
                    //rcb.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(UserStatus.Deleted), ((int)UserStatus.Deleted).ToString()));
                }
                else if (ur.Status == (int)UserStatus.Blocked)
                {
                    rcb.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(UserStatus.Blocked), ((int)UserStatus.Blocked).ToString()));
                    rcb.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(UserStatus.Active), ((int)UserStatus.Active).ToString()));
                    //rcb.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(UserStatus.Deleted), ((int)UserStatus.Deleted).ToString()));
                }
                else if (ur.Status == (int)UserStatus.Deleted)
                {
                    rcb.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(UserStatus.Deleted), ((int)UserStatus.Deleted).ToString()));
                    rcb.Enabled = false;
                }
                rcb.SelectedValue = ur.Status.ToString();
            }
        }

        protected void rgOwners_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgOwners.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgOwners.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgOwners.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgOwners.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgOwners.MasterTableView.Rebind();
            }
        }      

        protected void rcbRoleLevel_PreRender(object sender, EventArgs e)
        {
            RadComboBox rcbRoles = sender as RadComboBox;
            User user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);
            IEnumerable<RoleLevel> userRoles = RequestManager.Services.UsersService.GetRoleLevelsForUser(user.UserID);

            foreach (var rl in RequestManager.Services.RolesService.GetRoles())
            {
                if (!userRoles.Contains((RoleLevel)rl.RoleLevel))
                {
                    if ((((RoleLevel)rl.RoleLevel) == DataAccess.Enums.RoleLevel.Administrator &&
                            userRoles.Contains(RoleLevel.Manager)) ||
                            (((RoleLevel)rl.RoleLevel) == DataAccess.Enums.RoleLevel.Manager &&
                            userRoles.Contains(RoleLevel.Administrator)))
                        continue;

                    rcbRoles.Items.Add(new RadComboBoxItem(EnumConverter.GetValue((RoleLevel)rl.RoleLevel), ((int)rl.RoleLevel).ToString()));
                }
            }
        }

        protected void rgRoleLevels_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    User user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);
                    RoleLevel roleLevel = (RoleLevel)ReadGridItemData(args.Item);
                    UserRole userRole = RequestManager.Services.RolesService.AddUserToRole(user, roleLevel);
                    int status;

                    switch (roleLevel)
                    {
                        case RoleLevel.Guest:
                        case RoleLevel.Owner:
                        case RoleLevel.KeyManager:
                        case RoleLevel.PropertyManager:
                        default:
                            status = (int)UserStatus.Inactive;
                            break;

                        case RoleLevel.Administrator:
                        case RoleLevel.Manager:
                        case RoleLevel.DataEntryManager:
                            status = (int)UserStatus.Active;
                            break;
                    }

                    userRole.Status = status;
                    user.UserRoles.Add(userRole);

                    if (status == (int)UserStatus.Inactive)
                    {
                        userRole.ActivationToken = Guid.NewGuid().ToString();
                        SendActivationEmail(user, userRole);
                    }

                    RequestManager.Services.SaveChanges();
                    this.rgOwners.MasterTableView.Rebind();
                }
            }
        }

        protected void rcbUserRoleStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox rcb = sender as RadComboBox;
            int roleId = int.Parse(rcb.Attributes["roleId"]);
            int newStatus = int.Parse(rcb.SelectedValue);
            UserRole userRole = RequestManager.Services.UsersService.GetUserRolesByUserId(this.UserId.Value).Where(ur => ur.UserRoleId == roleId).Single();

            if (userRole.Status == (int)UserStatus.Requested && newStatus == (int)UserStatus.Inactive)
            {
                User user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);

                userRole.ActivationToken = Guid.NewGuid().ToString();
                SendActivationEmail(user, userRole);
            }

            userRole.Status = newStatus;

            RequestManager.Services.SaveChanges();
            this.rgOwners.MasterTableView.Rebind();
        }

        protected void rgOwners_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int userRoleId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserRoleId"].ToString());

                if (UserId.HasValue)
                {
                    DataAccess.User user;
                    if ((user = RequestManager.Services.UsersService.GetUserByUserId(this.UserId.Value)) != null)
                    {
                        if (RequestManager.Services.UsersService.HaseAnyDependenciesForUserRole(userRoleId))
                        {
                            var userRole = RequestManager.Services.UsersService.GetUserRolesByUserId(user.UserID).Where(r => r.UserRoleId == userRoleId).SingleOrDefault();
                            string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_UserRole"), string.Format("{0} - role {1}", user.FullName, EnumConverter.GetValue((RoleLevel)userRole.Role.RoleLevel)));
                            string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_UserRole"), string.Format("{0} - role {1}", user.FullName, EnumConverter.GetValue((RoleLevel)userRole.Role.RoleLevel)));

                            RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                        }
                        else
                        {
                            RequestManager.Services.UsersService.DeleteUserRoleByRoleId(userRoleId);
                            RequestManager.Services.SaveChanges();
                            this.rgOwners.Rebind();
                        }
                    }
                }
            }
        }

        #endregion Event Handlers
    }
}