﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UsersList.ascx.cs" Inherits="IntranetApp.Controls.Users.UsersList" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="pnlData" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>
    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgOwners" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="ownersLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgOwners_ItemCommand" 
        OnItemDataBound="rgOwners_ItemDataBound"
        OnDataBound="rgOwners_DataBound">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="UserRoleId" DataSourceID="ownersLinqDS">
			<Columns>
				<telerik:GridBoundColumn DataField="User.Firstname" FilterControlWidth="150px" UniqueName="Firstname" HeaderText="Firstname" SortExpression="User.Firstname" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="User.Lastname" FilterControlWidth="150px" UniqueName="Lastname" HeaderText="Lastname" SortExpression="User.Lastname" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="User.email" FilterControlWidth="150px" UniqueName="email" HeaderText="Email" SortExpression="User.email" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="User.DictionaryCountry.CountryNameCurrentLanguage" FilterControlWidth="150px" UniqueName="Country" HeaderText="Country" SortExpression="User.DictionaryCountry.CountryNameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                    <FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbFilterCountry" AutoPostBack="true" EnableViewState="true" OnInit="rcbFilterCountry_Init" OnSelectedIndexChanged="rcbFilterCountry_SelectedIndexChanged" OnPreRender="rcbFilterCountry_PreRender" MaxHeight="300px" />
					</FilterTemplate>
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="User.City" FilterControlWidth="150px" UniqueName="City" HeaderText="City" SortExpression="User.City" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="Status" HeaderText="Status" UniqueName="Status" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true">
					<HeaderStyle Width="100px"></HeaderStyle>
					<ItemStyle CssClass="link" />
                    <FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbFilterStatus" AutoPostBack="true" EnableViewState="true" OnInit="rcbFilterStatus_Init" OnSelectedIndexChanged="rcbFilterStatus_SelectedIndexChanged" OnPreRender="rcbFilterStatus_PreRender" Width="90%" />
					</FilterTemplate>
                    <ItemTemplate>
                        <%# DataAccess.Enums.EnumConverter.GetValue((DataAccess.Enums.UserStatus)Eval("Status")) %>
                     </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>	
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="User.Lastname" SortOrder="Ascending" />
            </SortExpressions>
		</MasterTableView>
		<HeaderContextMenu EnableImageSprites="True">
		</HeaderContextMenu>
	</telerik:RadGrid>
    <br />
    <telerik:RadButton runat="server" ID="btnAddUser" Text="Add" OnClick="btnAddUser_Click" />
</asp:Panel>

<otpDS:SimpleDataSource ID="ownersLinqDS" runat="server" />