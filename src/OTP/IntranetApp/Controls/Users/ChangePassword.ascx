﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.ascx.cs" Inherits="IntranetApp.Controls.Users.ChangePassword" %>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
       <telerik:RadWindow ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" Text="<%$ Resources: Controls, Dialog_Discard_Text %>" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="btnDiscard_Yes"></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClick="btnDiscard_No"></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<asp:Panel ID="pnlData" runat="server">
    <asp:ValidationSummary ID="vsChangePasswordSummary" ValidationGroup="ChangePassword" HeaderText=" "
        DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm"  />

    <asp:Label CssClass="error" ID="lblChangeError" Font-Size="14px" runat="server"/>
    <asp:Label CssClass="success" ID="lblChangeSuccess" Font-Size="14px" runat="server"/>
    
    <h1 class="FormHeader">
        <asp:Label runat="server" ID="lblHeader" meta:resourcekey="lblHeader"></asp:Label>
    </h1>
    <br />
    <table>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblOldPassword" meta:resourcekey="lblOldPassword" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" ID="rtbOldPassword" Enabled="true" MaxLength="50" TabIndex="1" TextMode="Password" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvOldPassword" ValidationGroup="ChangePassword" runat="server" ControlToValidate="rtbOldPassword"
                    Display="Dynamic" meta:resourcekey="valOldPassword" Text="*" CssClass="error"  />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblNewPassword" meta:resourcekey="lblNewPassword" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" ID="rtbNewPassword" Enabled="true" MaxLength="50" TabIndex="2" TextMode="Password" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvNewPassword" ValidationGroup="ChangePassword" runat="server" ControlToValidate="rtbNewPassword"
                    Display="Dynamic" meta:resourcekey="valNewPassword" Text="*" CssClass="error" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblConfirmNewPassword" meta:resourcekey="lblConfirmNewPassword" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" Enabled="true" ID="rtbConfirmNewPassword" MaxLength="50" TabIndex="3" TextMode="Password" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvConfirmNewPassword" ValidationGroup="ChangePassword" runat="server" ControlToValidate="rtbConfirmNewPassword"
                    Display="Dynamic" meta:resourcekey="valConfirmNewPassword" Text="*" CssClass="error" />
                <asp:CompareValidator runat="server" Enabled="true" ID="cvConfirmNewPassword" ValidationGroup="ChangePassword" ControlToValidate="rtbConfirmNewPassword"
                    ControlToCompare="rtbNewPassword" Display="Dynamic" meta:resourcekey="valPasswordsDoesnotMatch" Text="*" CssClass="error" />
            </td>
        </tr>
    </table>
</asp:Panel>

<table>
    <tr style="height: 20px;">
        <td></td>
    </tr>
    <tr>
        <td width="645px" align="right" style="padding-right: 3px">
            <telerik:RadButton runat="server" ID="btnSave" TabIndex="18" Text="<%$ Resources: Controls, Button_Save %>" CausesValidation="true" ValidationGroup="ChangePassword" OnClick="btnSave_Click" />
            <telerik:RadButton runat="server" ID="btnCancel" TabIndex="19" Text="<%$ Resources: Controls, Button_Cancel %>" CausesValidation="false" OnClick="btnCancel_Click" />
        </td>
    </tr>
</table>