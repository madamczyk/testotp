﻿using BusinessLogic.Managers;
using BusinessLogic.Objects.Session;
using System;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Users
{
    public partial class ChangePassword : System.Web.UI.UserControl
    {

        #region Methods

        private void AddScripts()
        {
            // enable warning before unloading the page
            RadScriptManager.RegisterStartupScript(this, GetType(),
                "startup_warn",
                "\twarnBeforeUnload = true;\n\tenableWarning();\n\twarningBeforeUnload = \"" + HttpUtility.JavaScriptStringEncode((string)GetGlobalResourceObject("Controls", "Dialog_Discard_ChangePassword")) + "\";\n",
                true);

            // disable warning before postback
            RadScriptManager.RegisterOnSubmitStatement(this, GetType(),
                "onsubmit_disable_warn",
                "disableWarning();");
        }

        #endregion

        #region Events Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rtbOldPassword.Focus();
            }

            Page.Form.DefaultButton = btnSave.UniqueID;
            lblChangeSuccess.Text = string.Empty;
            lblChangeError.Text = string.Empty;

            AddScripts();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SessionUser sessionUser = SessionManager.CurrentUser;

                if (sessionUser != null)
                {
                    try
                    {
                        RequestManager.Services.UsersService.ChangePassword(sessionUser.UserID, rtbOldPassword.Text, rtbNewPassword.Text);
                        lblChangeSuccess.Text = (string)GetLocalResourceObject("ChangePassword_SuccessMessage");
                    }
                    catch(Exception ex)
                    {
                        lblChangeError.Text = ex.Message;
                    }
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            rwConfirm.Title = (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption2");
            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        #endregion

    }
}