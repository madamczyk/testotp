﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="IntranetApp.Controls.Users.Login" %>
<div class="loginPanel">
	<div class="loginContent">
		<asp:Login runat="server" ID="login" OnLoggingIn="login_LoggingIn" 
            OnLoggedIn="login_LoggedIn" DestinationPageUrl="~/Default.aspx" 
            OnAuthenticate="login_Authenticate">
			<LayoutTemplate>
				<div class="loginPane">
					<div style="font-size: 14px; color: red; font-weight: bold; text-align: center"><asp:Literal ID="FailureText" runat="server"  /></div>
					<table>
						<tr>
							<td><asp:Label ID="Label1" runat="server" AssociatedControlID="userName" Text="E-mail" /></td>
							<td><asp:TextBox runat="server" MaxLength="64" ID="userName" /></td>
						</tr>
						<tr>
							<td><asp:Label ID="Label2" runat="server" AssociatedControlID="password" Text="Password"/></td>
							<td><asp:TextBox runat="server" ID="password" MaxLength="64" TextMode="Password" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<asp:CheckBox runat="server" ID="rememberPassword" CommandName="Login" Text="Remember me" />
							</td>
						</tr>
                        <tr>
							<td colspan="2">
							</td>
						</tr>
                        <tr>
                            <td colspan="2" class="buttonLoginCell">
                                <br />
                                <asp:Button ID="btnLogin" runat="server" CommandName="Login" Text="Login" />
                            </td>
                        </tr>
					</table>
				</div>
			</LayoutTemplate>
		</asp:Login>
        <br />
        
        
	</div>
</div>