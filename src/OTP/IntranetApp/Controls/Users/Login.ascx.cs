﻿using BusinessLogic.Managers;
using BusinessLogic.Objects;
using BusinessLogic.Objects.Session;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Controls.Users
{
    public partial class Login : System.Web.UI.UserControl
    {
        #region Event handlers

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the LoggingIn event of the login control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.LoginCancelEventArgs"/> instance containing the event data.</param>
        protected void login_LoggingIn(object sender, LoginCancelEventArgs e)
        {
            login.RememberMeSet = (login.FindControl("rememberPassword") as CheckBox).Checked;
        }

        /// <summary>
        /// Handles the LoggedIn event of the login control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void login_LoggedIn(object sender, EventArgs e)
        {
            SessionManager.CurrentUser = new SessionUser(RequestManager.Services.UsersService.GetUserByEmail(this.login.UserName));
            SessionManager.SessionToken = Guid.NewGuid().ToString();
            Response.Redirect("~/Default.aspx");            
        }

        /// <summary>
        /// Handles the Authenticate event- before LoggedIn /LoginError
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void login_Authenticate(object sender, AuthenticateEventArgs e)
        {
            bool authenticated = true;
            if ((string.IsNullOrWhiteSpace(this.login.UserName)) || (string.IsNullOrWhiteSpace(this.login.Password)))
            {
                this.login.FailureText = GetLocalResourceObject("lblLoginPasswordRequired").ToString();
                authenticated = false;
                return;
            }

            DataAccess.User user = RequestManager.Services.UsersService.GetUserByEmail(this.login.UserName);
            if (user == null)
            {
                this.login.FailureText = GetLocalResourceObject("lblLoginNotExist").ToString();
                authenticated = false;
                return;
            }
            else if (Membership.ValidateUser(this.login.UserName, this.login.Password))
            {
                //get current user role
                var currentUserRoles = user.UserRoles.Where(r =>
                    r.Role.RoleLevel == (int)RoleLevel.Manager
                    || r.Role.RoleLevel == (int)RoleLevel.DataEntryManager
                    || r.Role.RoleLevel == (int)RoleLevel.Administrator).ToList();

                if (currentUserRoles == null || !currentUserRoles.Any(ur => ur.Status == (int)UserStatus.Active))
                {
                    this.login.FailureText = GetLocalResourceObject("lblLoginNotValidRole").ToString();
                    authenticated = false;
                    return;
                }
                user.LoggedInRoles = currentUserRoles.Where(r => r.Status == (int)UserStatus.Active).Select(r => r.Role).ToList();
            }
            else
            {
                this.login.FailureText = GetLocalResourceObject("lblPasswordNotValid").ToString();
                authenticated = false;
                return;
            }
            e.Authenticated = authenticated;
            SessionManager.CurrentUser = new SessionUser(user);
        }

        #endregion
    }
}