﻿using BusinessLogic.Managers;
using BusinessLogic.Enums;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Common;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.Objects.StaticContentRepresentation;
using Common.Validation;
using Common.Converters;
using System.Text.RegularExpressions;

namespace IntranetApp.Controls.Users
{
    public partial class UserDetails : BaseUserControl
    {
        #region Fields

        public int? UserId
        {
            get { return ViewState["userId"] as int?; }
            set { ViewState["userId"] = value; }
        }

        public int? Role
        {
            get { return ViewState["role"] as int?; }
            set { ViewState["role"] = value; }
        }

        public string UserRoleName
        {
            get
            {
                if (Role.HasValue)
                {
                    return EnumConverter.GetValue((RoleLevel)Role.Value) + "s";
                }
                return string.Empty;
            }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["mode"]; }
            set { ViewState["mode"] = (int)value; }
        }

        #endregion Fields

        #region Methods

        private void LoadData()
        {
            Debug.Assert(UserId.HasValue);

            User user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);

            //textboxes
            txtFirstname.Text = user.Firstname;
            txtLastname.Text = user.Lastname;
            txtAddressLine1.Text = user.Address1;
            txtCity.Text = user.City;
            txtAddressLine2.Text = user.Address2;
            txtState.Text = user.State;
            txtZipCode.Text = user.ZIPCode;
            rcbCountry.SelectedValue = user.DictionaryCountry != null ? user.DictionaryCountry.CountryId.ToString() : "";
            rcbCulture.SelectedValue = user.DictionaryCulture.CultureId.ToString();
            txtEmail.Text = user.email;
            txtCellPhone.Text = user.CellPhone;
            txtLandLine.Text = user.LandLine;
            txtLicense.Text = user.DriverLicenseNbr;
            txtPassport.Text = user.PassportNbr;
            txtSocialSecurity.Text = user.SocialsecurityNbr;
            txtDirectDeposit.Text = user.DirectDepositInfo;
            txtBankName.Text = Base64.Decode(user.BankName);
            txtRoutingNumber.Text = Base64.Decode(user.RoutingNumber);
            txtAccountNumber.Text = Base64.Decode(user.AccountNumber);
            if ((RoleLevel)Role.Value == RoleLevel.Owner)
            {
                txtSignUpCode.Text = user.SignUpCode;
            }
        }

        private bool SaveData()
        {
            User user = new User();
            if (Mode == FormMode.Add)
                user = new User();
            else
                user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);

            user.Firstname = txtFirstname.Text;
            user.Lastname = txtLastname.Text;
            user.Address1 = txtAddressLine1.Text;
            user.Address2 = txtAddressLine2.Text;
            user.City = txtCity.Text;
            user.ZIPCode = txtZipCode.Text;
            user.State = txtState.Text;
            user.DictionaryCountry = RequestManager.Services.DictionaryCountryService.GetCountryById(int.Parse(this.rcbCountry.SelectedValue));
            user.email = txtEmail.Text;
            user.CellPhone = txtCellPhone.Text;
            user.LandLine = txtLandLine.Text;
            user.DriverLicenseNbr = txtLicense.Text;
            user.PassportNbr = txtPassport.Text;
            user.SocialsecurityNbr = txtSocialSecurity.Text;
            user.DirectDepositInfo = txtDirectDeposit.Text;
            user.DictionaryCulture = RequestManager.Services.DictionaryCultureService.GetCultureById(int.Parse(this.rcbCulture.SelectedValue));
            user.BankName = Base64.Encode(txtBankName.Text);
            user.RoutingNumber = Base64.Encode(txtRoutingNumber.Text);
            user.AccountNumber = Base64.Encode(txtAccountNumber.Text);
            if ((RoleLevel)Role.Value == RoleLevel.Owner)
            {
                user.SignUpCode = txtSignUpCode.Text;
            }
            if (Mode == FormMode.Add)
            {
                // Admin or manager creates user account in specific role
                UserRole userRole = RequestManager.Services.RolesService.AddUserToRole(user, (RoleLevel)Role);
                int status;

                switch ((RoleLevel)Role)
                {
                    case RoleLevel.Guest:
                    case RoleLevel.Owner:
                    case RoleLevel.KeyManager:
                    case RoleLevel.PropertyManager:
                    default:
                        status = (int)UserStatus.Inactive;
                        break;

                    case RoleLevel.Administrator:
                    case RoleLevel.Manager:
                    case RoleLevel.DataEntryManager:
                        status = (int)UserStatus.Active;
                        break;
                }

                userRole.Status = status;
                user.UserRoles.Add(userRole);

                if (status == (int)UserStatus.Inactive)
                {
                    userRole.ActivationToken = Guid.NewGuid().ToString();
                    SetNewPasswordAndSendActivationEmail(user, userRole);
                }
                else
                {
                    CreatePasswordAndSendEmail(user);
                }

                if ((RoleLevel)Role == RoleLevel.Owner && Mode == FormMode.Add)
                {
                    RequestManager.Services.UsersService.AddHomeowner(user);
                }
                else
                {
                    RequestManager.Services.UsersService.AddUser(user);
                }
            }

            return true;
        }

        private void SetNewPasswordAndSendActivationEmail(User user, UserRole role)
        {
            string tempPassword = RequestManager.Services.AuthorizationService.GeneratePassword();

            user.Password = MD5Helper.Encode(tempPassword);
            user.IsGeneratedPassword = true;

            OwnerActivationEmail ownerActivationEmail = new OwnerActivationEmail(user.email);
            ownerActivationEmail.UserFirstName = user.Firstname;
            ownerActivationEmail.UserLogin = user.email;
            ownerActivationEmail.UserPassword = tempPassword;
            ownerActivationEmail.ActivationLinkTitle = GetLocalResourceObject("ActivationLinkText").ToString();
            ownerActivationEmail.ActivationLinkUrl = string.Format("{0}://{1}{2}/{3}",
                        Request.Url.Scheme,
                        RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.ExtranetURL),
                        "/Account/Activation",
                        role.ActivationToken);

            RequestManager.Services.MessageService.AddEmail(ownerActivationEmail, user.DictionaryCulture.CultureCode);
        }

        private void CreatePasswordAndSendEmail(User user)
        {
            string tempPassword = RequestManager.Services.AuthorizationService.GeneratePassword();

            user.Password = MD5Helper.Encode(tempPassword);
            user.IsGeneratedPassword = true;

            UserAccountAdded newAccountAdded = new UserAccountAdded(user.email)
            {
                UserFirstName = user.Firstname,
                UserLogin = user.email,
                UserPassword = tempPassword
            };
            RequestManager.Services.MessageService.AddEmail(newAccountAdded, user.DictionaryCulture.CultureCode);
        }

        private void SetNewPasswordAndSendEmail(User user)
        {
            string tempPassword = RequestManager.Services.AuthorizationService.GeneratePassword();

            user.Password = MD5Helper.Encode(tempPassword);
            user.IsGeneratedPassword = true;

            PasswordResetEmail passwordResetEmail = new PasswordResetEmail(user.email)
            {
                UserFirstName = user.Firstname,
                TempPassword = tempPassword
            };

            RequestManager.Services.MessageService.AddEmail(passwordResetEmail, user.DictionaryCulture.CultureCode);
        }

        private bool CheckExists()
        {
            if (UserId.HasValue && RequestManager.Services.UsersService.GetUserByUserId(UserId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"),
                    (string)GetGlobalResourceObject("Controls", "Object_User"));

                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"),
                    (string)GetGlobalResourceObject("Controls", "Object_User"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }
            else
                return true;
        }

        private void BindControls()
        {
            // countries
            foreach (DictionaryCountry obj in RequestManager.Services.DictionaryCountryService.GetCountries())
            {
                rcbCountry.Items.Add(new RadComboBoxItem(obj.CountryName.ToString(), obj.CountryId.ToString()));
            }
            rcbCountry.SelectedValue = RequestManager.Services.DictionaryCountryService.GetCountryByCountryCode("us").CountryId.ToString();
            
            //cultures
            int defaultCulture = 0;
            foreach (DictionaryCulture obj in RequestManager.Services.DictionaryCultureService.GetCultures())
            {
                rcbCulture.Items.Add(new RadComboBoxItem(obj.CultureName.ToString(), obj.CultureId.ToString()));
                if (obj.CultureCode == "en-US")
                {
                    defaultCulture = obj.CultureId;
                }
            }

            if (this.Mode == FormMode.Add)
                this.rcbCulture.SelectedValue = defaultCulture.ToString();
            
            if (Role.Value == (int)RoleLevel.Owner)
            {
                this.rfvAccountNumber.Enabled = true;
                this.rfvBankName.Enabled = true;
                this.rfvRoutingNumber.Enabled = true;
            }

            if (UserId.HasValue && Mode == FormMode.Edit)
            {
                var user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);
                UserRole userOwnerRole = RequestManager.Services.UsersService.GetUserRoleByLevel(user, RoleLevel.Owner);
                

                btnResetPassword.Visible = true;
                pnlUserData.Visible = true;

                ctrlUserRoles.UserId = UserId.Value;

                if (userOwnerRole != null) // user has owner role
                {
                    ctrlProperties.userId = UserId.Value;

                    rpvProperties.Visible = true;
                    RadTab properties = new RadTab();
                    properties.PageViewID = this.rpvProperties.ID;
                    properties.Text = (string)GetLocalResourceObject("rtProperties.Text");
                    rtsUserData.Tabs.Add(properties);              
                    
                    if (userOwnerRole.Status == (int)UserStatus.Requested)
                    {
                        ctrlScheduledVisits.userId = UserId.Value;
                        rpvScheduledVisits.Visible = true;
                        RadTab scheduledVisits = new RadTab();
                        scheduledVisits.PageViewID = this.rpvScheduledVisits.ID;
                        scheduledVisits.Text = (string)GetLocalResourceObject("rtScheduledVisits.Text");
                        rtsUserData.Tabs.Add(scheduledVisits);                        
                    }                    
                }
                if ((RoleLevel)Role.Value == RoleLevel.Manager) //only for Upload manager and Property Manager tab with 'Zip code manager' will be displayed
                {
                    ctrlZipCodeManagers.UserId = UserId.Value;
                    ctrlZipCodeManagers.UserRoleLevel = Role.Value;
                }
                else
                {
                    RadTab tabZipCodeManagers = rtsUserData.Tabs.Where(tab => tab.Text == (string)GetLocalResourceObject("rtZipCodeManagers.Text")).SingleOrDefault();
                    tabZipCodeManagers.Visible = false;
                    ctrlZipCodeManagers.Visible = false;
                }
            }
            if ((RoleLevel)Role.Value == RoleLevel.Owner)
            {
                trSignUpCode.Visible = true;
            } 
        }

        private void SetNames()
        {
            if (Mode == FormMode.Edit)
            {
                User user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"),
                    (string)GetGlobalResourceObject("Controls", "Object_User"), user.FullName);

                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"),
                    EnumConverter.GetValue((RoleLevel)(Role.Value)),
                    user.FullName);
            }
            else
            {
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderAdd.Text"),
                    EnumConverter.GetValue((RoleLevel)(Role.Value)).ToLower());
            }
  
        }

        private void AddScripts()
        {
            string userName;

            if (Mode == FormMode.Edit)
                userName = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value).FullName;
            else
                userName = (txtFirstname.Text + " " + txtLastname.Text);

            userName = userName.Trim();

            // enable warning before unloading the page
            RadScriptManager.RegisterStartupScript(this, GetType(),
                "startup_warn",
                "\twarnBeforeUnload = true;\n\tenableWarning();\n\twarningBeforeUnload = \"" + HttpUtility.JavaScriptStringEncode(String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_LeaveConfrmation"), (string)GetGlobalResourceObject("Controls", "Object_User"), String.IsNullOrEmpty(userName) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : userName)) + "\";\n",
                true);

            // disable warning before postback
            RadScriptManager.RegisterOnSubmitStatement(this, GetType(),
                "onsubmit_disable_warn",
                "disableWarning();");
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindControls();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();

                    SetNames();
                }
            }

            //enter button
            Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                txtFirstname.Focus();
            }

            AddScripts();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"),
                        (string)GetGlobalResourceObject("Controls", "Object_User"));

                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"),
                        (string)GetGlobalResourceObject("Controls", "Object_User"));

                    rwError.VisibleOnPageLoad = true;
                }
                else
                    Response.Redirect("~/Forms/Users/" + ((RoleLevel)Role.Value).ToString() + "sList.aspx");
            }            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"),
                    (string)GetGlobalResourceObject("Controls", "Object_User"), 
                    String.IsNullOrEmpty(txtFirstname.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtFirstname.Text + " " + txtLastname.Text);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Users/" + ((RoleLevel)Role.Value).ToString() + "sList.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Users/" + ((RoleLevel)Role.Value).ToString() + "sList.aspx");
        }

        protected void cvEmailExist_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Mode == FormMode.Add)
            {
                CustomValidator cv = source as CustomValidator;

                bool valid = true;
                if (!string.IsNullOrWhiteSpace(this.txtEmail.Text))
                {
                    User user = RequestManager.Services.UsersService.GetUserByEmail(this.txtEmail.Text);
                    if (user != null)
                    {
                        valid = false;
                    }
                }
                args.IsValid = valid;
            }
        }

        protected void cvRoutingNumberFormat_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!String.IsNullOrWhiteSpace(this.txtRoutingNumber.Text))
                args.IsValid = BankData.ValidateRoutingNumber(this.txtRoutingNumber.Text);
            else
                args.IsValid = true;
        }

        protected void cvZipCodeFormat_ServerValidate(object source, ServerValidateEventArgs args)
        {
            var isUSASelected = RequestManager.Services.DictionaryCountryService.GetCountryByCountryCode("us").CountryId == int.Parse(rcbCountry.SelectedValue);

            if (!String.IsNullOrWhiteSpace(this.txtZipCode.Text) && isUSASelected)
                args.IsValid = txtZipCode.Text.Length == 5 && Regex.IsMatch(txtZipCode.Text, "^[0-9]{5}$");
            else
                args.IsValid = true;
        }

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            User user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);

            SetNewPasswordAndSendEmail(user);

            RequestManager.Services.SaveChanges();

            RadWindowManager1.RadAlert(String.Format((string)GetLocalResourceObject("ResetPasswordInfo_Text"), user.FullName, user.email), 
                430, 100, 
                (string)GetLocalResourceObject("ResetPasswordInfo_Title"), 
                "");
        }

        #endregion
    }
}