﻿using BusinessLogic.Enums;
using BusinessLogic.Managers;
using BusinessLogic.Objects.Templates.Email;
using Common;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Users
{
    public partial class ZipCodeManagers : System.Web.UI.UserControl
    {

        #region Fields

        public int? UserId
        {
            get { return ViewState["UserId"] as int?; }
            set { ViewState["UserId"] = value; }
        }

        public int? UserRoleLevel
        {
            get { return ViewState["UserRoleLevel"] as int?; }
            set { ViewState["UserRoleLevel"] = value; }
        }

        public int? UserRoleId
        {
            get { return ViewState["UserRoleId"] as int?; }
            set { ViewState["UserRoleId"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.zipCodeManagersLinqDS.DataResolver = () =>
            {
                if (!UserRoleId.HasValue)
                    return null;
                IQueryable<DataAccess.ManagerZipCodeRange> result = RequestManager.Services.UsersService.GetZipCodesRangesForUserRole(this.UserRoleId.Value).AsQueryable();
                var xx = result.ToList();
                return xx;
            };
        }        

        private int ReadGridItemData(GridItem item)
        {
            return int.Parse((item.FindControl("rcbZipCodeRanges") as RadComboBox).SelectedValue);
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
            if (!IsPostBack)
            {
                if (UserId.HasValue)
                {
                    UserRole userUploadRole = RequestManager.Services.UsersService.GetUserRolesByUserId(UserId.Value).Where(ur => ur.Role.RoleLevel == UserRoleLevel.Value).SingleOrDefault();
                    if (userUploadRole != null)
                    {
                        UserRoleId = userUploadRole.UserRoleId;
                    }
                }
            }
        }
        
        protected void rgOwners_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                ManagerZipCodeRange obj = (ManagerZipCodeRange)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                string title = obj.ZipCodeRange.ZipCodeRangeString;

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgOwners.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeManager"), title), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeManager"), title));
            }
        }

        protected void rgOwners_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgOwners.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgOwners.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgOwners.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgOwners.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgOwners.MasterTableView.Rebind();
            }
        }      

        protected void rcbRoleLevel_PreRender(object sender, EventArgs e)
        {
            RadComboBox rcbRoles = sender as RadComboBox;
            UserRole userUploadRole = RequestManager.Services.UsersService.GetUserRolesByUserId(UserId.Value).Where(ur => ur.Role.RoleLevel == UserRoleLevel.Value).SingleOrDefault();
            if (userUploadRole != null)
            {
                var zipCodeRanges = RequestManager.Services.UsersService.GetZipCodeRanges();

                foreach (var zipCodeRange in zipCodeRanges)
                {
                    if (!userUploadRole.ManagerZipCodeRanges.Any(zp => zp.ZipCodeRange.ZipCodeRangeId == zipCodeRange.ZipCodeRangeId))
                    {
                        rcbRoles.Items.Add(new RadComboBoxItem(zipCodeRange.ZipCodeRangeString, zipCodeRange.ZipCodeRangeId.ToString()));
                    }
                }
            }
        }

        protected void rgRoleLevels_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    User user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);
                    //UserRole userUploadRole = RequestManager.Services.UsersService.GetUserRoleByLevel(user, (RoleLevel)UserRoleLevel.Value);

                    if (UserRoleId.HasValue)
                    {
                        int rangeId = ReadGridItemData(args.Item);

                        RequestManager.Services.UsersService.AddZipCodeRangeToManager(rangeId, UserRoleId.Value);
                        RequestManager.Services.SaveChanges();
                        this.rgOwners.MasterTableView.Rebind();
                    }
                }
            }
        }

        protected void rgOwners_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int id = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ManagerZipCodeRangeId"].ToString());
            RequestManager.Services.UsersService.DeleteZipCodeManager(id);
            RequestManager.Services.SaveChanges();
            this.rgOwners.MasterTableView.Rebind();
        }

        #endregion Event Handlers
   
    }
}