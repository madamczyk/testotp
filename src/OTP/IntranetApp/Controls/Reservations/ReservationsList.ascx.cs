﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Calendar;
using System.Text;
using IntranetApp.Enums;
using Common.Helpers;
using Common.CultureInfo;
using Common.Extensions;

namespace IntranetApp.Controls.Reservations
{
    public partial class ReservationsList : System.Web.UI.UserControl
    {
        #region Fields

        public int? BookingStatusFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["BookingStatusFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["BookingStatusFilterSelectedValue"] as int?;
            }
            set { ViewState["BookingStatusFilterSelectedValue"] = value; }
        }

        public DateTime? DateArrivalFrom
        {
            get
            {
                if (ViewState["DateArrivalFrom"] != null)
                    return (DateTime)ViewState["DateArrivalFrom"];

                return null;
            }
            set
            {
                ViewState["DateArrivalFrom"] = value;
            }
        }

        public DateTime? DateArrivalTo
        {
            get
            {
                if (ViewState["DateArrivalTo"] != null)
                    return (DateTime)ViewState["DateArrivalTo"];

                return null;
            }
            set
            {
                ViewState["DateArrivalTo"] = value;
            }
        }

        public DateTime? DateDepartureFrom
        {
            get
            {
                if (ViewState["DateDepartureFrom"] != null)
                    return (DateTime)ViewState["DateDepartureFrom"];

                return null;
            }
            set
            {
                ViewState["DateDepartureFrom"] = value;
            }
        }

        public DateTime? DateDepartureTo
        {
            get
            {
                if (ViewState["DateDepartureTo"] != null)
                    return (DateTime)ViewState["DateDepartureTo"];

                return null;
            }
            set
            {
                ViewState["DateDepartureTo"] = value;
            }
        }

        public DateTime? BookingDateFrom
        {
            get
            {
                if (ViewState["BookingDateFrom"] != null)
                    return (DateTime)ViewState["BookingDateFrom"];

                return null;
            }
            set
            {
                ViewState["BookingDateFrom"] = value;
            }
        }

        public DateTime? BookingDateTo
        {
            get
            {
                if (ViewState["BookingDateTo"] != null)
                    return (DateTime)ViewState["BookingDateTo"];

                return null;
            }
            set
            {
                ViewState["BookingDateTo"] = value;
            }
        }

        public Decimal? TotalPriceFrom
        {
            get
            {
                if (ViewState["TotalPriceFrom"] != null)
                    return (Decimal)ViewState["TotalPriceFrom"];

                return null;
            }
            set
            {
                ViewState["TotalPriceFrom"] = value;
            }
        }

        public Decimal? TotalPriceTo
        {
            get
            {
                if (ViewState["TotalPriceTo"] != null)
                    return (Decimal)ViewState["TotalPriceTo"];

                return null;
            }
            set
            {
                ViewState["TotalPriceTo"] = value;
            }
        }

        public Decimal? PaymentReceivedFrom
        {
            get
            {
                if (ViewState["PaymentReceivedFrom"] != null)
                    return (Decimal)ViewState["PaymentReceivedFrom"];

                return null;
            }
            set
            {
                ViewState["PaymentReceivedFrom"] = value;
            }
        }

        public Decimal? PaymentReceivedTo
        {
            get
            {
                if (ViewState["PaymentReceivedTo"] != null)
                    return (Decimal)ViewState["PaymentReceivedTo"];

                return null;
            }
            set
            {
                ViewState["PaymentReceivedTo"] = value;
            }
        }

        public Decimal? TripBalanceFrom
        {
            get
            {
                if (ViewState["TripBalanceFrom"] != null)
                    return (Decimal)ViewState["TripBalanceFrom"];

                return null;
            }
            set
            {
                ViewState["TripBalanceFrom"] = value;
            }
        }

        public Decimal? TripBalanceTo
        {
            get
            {
                if (ViewState["TripBalanceTo"] != null)
                    return (Decimal)ViewState["TripBalanceTo"];

                return null;
            }
            set
            {
                ViewState["TripBalanceTo"] = value;
            }
        }

        public Decimal? TransactionFeeFrom
        {
            get
            {
                if (ViewState["TransactionFeeFrom"] != null)
                    return (Decimal)ViewState["TransactionFeeFrom"];

                return null;
            }
            set
            {
                ViewState["TransactionFeeFrom"] = value;
            }
        }

        public Decimal? TransactionFeeTo
        {
            get
            {
                if (ViewState["TransactionFeeTo"] != null)
                    return (Decimal)ViewState["TransactionFeeTo"];

                return null;
            }
            set
            {
                ViewState["TransactionFeeTo"] = value;
            }
        }

        public List<string> hiddenColumns
        {
            get
            {
                var list = ViewState["hiddenColumns"] as List<string>;

                if (list == null)
                    return new List<string>();
                else 
                    return list;
            }
            set { ViewState["hiddenColumns"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            reservationsLinqDS.DataResolver = () =>
            {
                IQueryable<Reservation> result = RequestManager.Services.ReservationsService.GetReservations().AsQueryable();

                rgReservations.ApplyFilter<Reservation>(ref result, "ConfirmationID",
                    (q, f) => q.Where(r => !String.IsNullOrEmpty(r.ConfirmationID) && r.ConfirmationID.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "UserFullName",
                    (q, f) => q.Where(r => r.User.FullName.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "UserFullAddress",
                    (q, f) => q.Where(r => r.User.FullAddress.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "BillingAddress",
                    (q, f) => q.Where(r => r.BillingAddress.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "PropertyCode",
                    (q, f) => q.Where(r => r.Property.PropertyCode.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "UnitTypeCode",
                    (q, f) => q.Where(r => r.UnitType.UnitTypeCode.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "UnitCode",
                    (q, f) => q.Where(r => r.Unit.UnitCode.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "HomeownerFullAddress",
                    (q, f) => q.Where(r => r.Property.User.FullNameAddress.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "HomeownerAccount",
                    (q, f) => q.Where(r => r.Property.User.BankAccountData.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "SalesPersonFullName",
                    (q, f) => q.Where(r => r.Property.SalesPerson != null && r.Property.SalesPerson.FullName.ToLower().Contains(f.ToLower())));

                if (this.BookingStatusFilterSelectedValue.HasValue)
                    result = result.Where(r => r.BookingStatus == this.BookingStatusFilterSelectedValue.Value);

                if (DateArrivalFrom.HasValue)
                    result = result.Where(r => r.DateArrival >= this.DateArrivalFrom.Value);
                if (DateArrivalTo.HasValue)
                    result = result.Where(r => r.DateArrival <= this.DateArrivalTo.Value);

                if (DateDepartureFrom.HasValue)
                    result = result.Where(r => r.DateDeparture >= this.DateDepartureFrom.Value);
                if (DateDepartureTo.HasValue)
                    result = result.Where(r => r.DateDeparture <= this.DateDepartureTo.Value);

                if (BookingDateFrom.HasValue)
                    result = result.Where(r => r.BookingDate >= this.BookingDateFrom.Value);
                if (BookingDateTo.HasValue)
                    result = result.Where(r => r.BookingDate <= this.BookingDateTo.Value);

                if (TotalPriceFrom.HasValue)
                    result = result.Where(r => r.TotalPrice >= this.TotalPriceFrom.Value);
                if (TotalPriceTo.HasValue)
                    result = result.Where(r => r.TotalPrice <= this.TotalPriceTo.Value);

                if (PaymentReceivedFrom.HasValue)
                    result = result.Where(r => r.PaymentReceived >= this.PaymentReceivedFrom.Value);
                if (PaymentReceivedTo.HasValue)
                    result = result.Where(r => r.PaymentReceived <= this.PaymentReceivedTo.Value);

                if (TripBalanceFrom.HasValue)
                    result = result.Where(r => r.TripBalance >= this.TripBalanceFrom.Value);
                if (TripBalanceTo.HasValue)
                    result = result.Where(r => r.TripBalance <= this.TripBalanceTo.Value);

                if (TransactionFeeFrom.HasValue)
                    result = result.Where(r => r.TransactionFee >= this.TransactionFeeFrom.Value);
                if (TransactionFeeTo.HasValue)
                    result = result.Where(r => r.TransactionFee <= this.TransactionFeeTo.Value);

                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void btnGenerateCSVFile_Click(object sender, EventArgs e)
        {
            GridColumn column;

            // save list of hidden columns and show all of them before exporting
            // hidden columns will be hidden before showing grid to user
            for (int i = 0; i < rgReservations.Columns.Count; ++i)
            {
                column = rgReservations.Columns[i];

                if (!column.Display)
                    hiddenColumns.Add(column.UniqueName);

                column.Display = true;
            }

            // prepare proper filename
            rgReservations.ExportSettings.FileName = String.Format(
                "reservationsList_{0}_{1}", 
                DateTime.Now.ToLocalizedDateString(), 
                DateTime.Now.ToLocalizedTimeString())
                .Replace(" ", "")
                .Replace("-", "")
                .Replace(":", "");

            rgReservations.MasterTableView.ExportToCSV();
        }

        protected void rgReservations_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgReservations_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgReservations.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgReservations.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgReservations.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgReservations.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgReservations.MasterTableView.Rebind();
            }
        }

        protected void rgReservations_PreRender(object sender, EventArgs e)
        {
            foreach (string columnName in hiddenColumns)
                rgReservations.MasterTableView.GetColumn(columnName).Display = false;

            hiddenColumns.Clear();
        }

        protected void rcbBookingStatus_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (BookingStatus bs in Enum.GetValues(typeof(BookingStatus)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(bs), ((int)bs).ToString()));
            }
        }

        protected void rcbBookingStatus_PreRender(object sender, EventArgs e)
        {
            if (BookingStatusFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = BookingStatusFilterSelectedValue.ToString();
            }
        }

        protected void rdpDateArrivalFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.DateArrivalFrom;
        }

        protected void rdpDateArrivalTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.DateArrivalTo;
        }

        protected void rdpDateDepartureFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.DateDepartureFrom;
        }

        protected void rdpDateDepartureTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.DateDepartureTo;
        }

        protected void rdpBookingDateFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.BookingDateFrom;
        }

        protected void rdpBookingDateTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.BookingDateTo;
        }

        protected void rntbTotalPriceFrom_PreRender(object sender, EventArgs e)
        {
            RadNumericTextBox rntb = sender as RadNumericTextBox;

            rntb.Value = (Double?)this.TotalPriceFrom;
        }

        protected void rntbTotalPriceTo_PreRender(object sender, EventArgs e)
        {
            RadNumericTextBox rntb = sender as RadNumericTextBox;

            rntb.Value = (Double?)this.TotalPriceTo;
        }

        protected void rntbPaymentReceivedFrom_PreRender(object sender, EventArgs e)
        {
            RadNumericTextBox rntb = sender as RadNumericTextBox;

            rntb.Value = (Double?)this.PaymentReceivedFrom;
        }

        protected void rntbPaymentReceivedTo_PreRender(object sender, EventArgs e)
        {
            RadNumericTextBox rntb = sender as RadNumericTextBox;

            rntb.Value = (Double?)this.PaymentReceivedTo;
        }

        protected void rntbTripBalanceFrom_PreRender(object sender, EventArgs e)
        {
            RadNumericTextBox rntb = sender as RadNumericTextBox;

            rntb.Value = (Double?)this.TripBalanceFrom;
        }

        protected void rntbTripBalanceTo_PreRender(object sender, EventArgs e)
        {
            RadNumericTextBox rntb = sender as RadNumericTextBox;

            rntb.Value = (Double?)this.TripBalanceTo;
        }

        protected void rntbTransactionFeeFrom_PreRender(object sender, EventArgs e)
        {
            RadNumericTextBox rntb = sender as RadNumericTextBox;

            rntb.Value = (Double?)this.TransactionFeeFrom;
        }

        protected void rntbTransactionFeeTo_PreRender(object sender, EventArgs e)
        {
            RadNumericTextBox rntb = sender as RadNumericTextBox;

            rntb.Value = (Double?)this.TransactionFeeTo;
        }

        protected void rcbBookingStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BookingStatusFilterSelectedValue = int.Parse(e.Value);
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpDateArrivalFrom_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            DateArrivalFrom = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpDateArrivalTo_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            DateArrivalTo = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpDateDepartureFrom_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            DateDepartureFrom = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpDateDepartureTo_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            DateDepartureTo = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpBookingDateFrom_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            BookingDateFrom = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpBookingDateTo_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            BookingDateTo = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void TotalPriceFilter(object sender, EventArgs e)
        {
            var header = rgReservations.MasterTableView.GetItems(GridItemType.FilteringItem)[0];
            var rntbFrom = header.FindControl("rntbTotalPriceFrom") as RadNumericTextBox;
            var rntbTo = header.FindControl("rntbTotalPriceTo") as RadNumericTextBox;

            TotalPriceFrom = (Decimal?)rntbFrom.Value;
            TotalPriceTo = (Decimal?)rntbTo.Value;

            rgReservations.MasterTableView.Rebind();
        }

        protected void PaymentReceivedFilter(object sender, EventArgs e)
        {
            var header = rgReservations.MasterTableView.GetItems(GridItemType.FilteringItem)[0];
            var rntbFrom = header.FindControl("rntbPaymentReceivedFrom") as RadNumericTextBox;
            var rntbTo = header.FindControl("rntbPaymentReceivedTo") as RadNumericTextBox;

            PaymentReceivedFrom = (Decimal?)rntbFrom.Value;
            PaymentReceivedTo = (Decimal?)rntbTo.Value;

            rgReservations.MasterTableView.Rebind();
        }

        protected void TripBalanceFilter(object sender, EventArgs e)
        {
            var header = rgReservations.MasterTableView.GetItems(GridItemType.FilteringItem)[0];
            var rntbFrom = header.FindControl("rntbTripBalanceFrom") as RadNumericTextBox;
            var rntbTo = header.FindControl("rntbTripBalanceTo") as RadNumericTextBox;

            TripBalanceFrom = (Decimal?)rntbFrom.Value;
            TripBalanceTo = (Decimal?)rntbTo.Value;

            rgReservations.MasterTableView.Rebind();
        }

        protected void TransactionFeeFilter(object sender, EventArgs e)
        {
            var header = rgReservations.MasterTableView.GetItems(GridItemType.FilteringItem)[0];
            var rntbFrom = header.FindControl("rntbTransactionFeeFrom") as RadNumericTextBox;
            var rntbTo = header.FindControl("rntbTransactionFeeTo") as RadNumericTextBox;

            TransactionFeeFrom = (Decimal?)rntbFrom.Value;
            TransactionFeeTo = (Decimal?)rntbTo.Value;

            rgReservations.MasterTableView.Rebind();
        }

        protected void rgReservations_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Reservation obj = (Reservation)item.DataItem;
                item.Attributes["allow-setstatus"] = "1";
                item.Attributes["allow-releasedeposit"] = (obj.BookingStatus == (int)BookingStatus.DamageInvestigation && obj.TakeSecurityDeposit.HasValue && obj.TakeSecurityDeposit.Value == true && !string.IsNullOrWhiteSpace(obj.SecurityDepositToken) && !obj.CapturedSecurityDeposit.HasValue) ? "1" : "0";
                item.Attributes["allow-capturedeposit"] = (obj.TakeSecurityDeposit.HasValue && obj.TakeSecurityDeposit.Value == true && !string.IsNullOrWhiteSpace(obj.SecurityDepositToken) && !obj.CapturedSecurityDeposit.HasValue) ? "1" : "0";
                item.Attributes["allow-authorizedeposit"] = (obj.TakeSecurityDeposit.HasValue && obj.TakeSecurityDeposit.Value == true && string.IsNullOrWhiteSpace(obj.SecurityDepositToken) && !obj.CapturedSecurityDeposit.HasValue) ? "1" : "0";
            }
        }

        #endregion Event Handlers
    }
}