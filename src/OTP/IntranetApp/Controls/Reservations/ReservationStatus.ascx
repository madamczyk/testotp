﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReservationStatus.ascx.cs" Inherits="IntranetApp.Controls.Reservations.ReservationStatus" %>

<telerik:RadWindowManager ID="rwmManager" runat="server">
    <Windows>
        <telerik:RadWindow Title="Change Reservation Status" ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Height="180px" Width="280px"  Behaviors="Move" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" meta:resourcekey="lblConfirmationText" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="wndBtnDiscard_Yes_Click" ></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClick="wndBtnDiscard_No_Click" ></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<asp:Panel runat="server" ID="pnlData">
    <asp:ValidationSummary ID="vsSummary" ValidationGroup="Reservation" HeaderText=" "
        DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
    <h1 class="FormHeader">
        <asp:Label runat="server" ID="lblHeader" meta:resourcekey="lblHeader"></asp:Label>
    </h1>
    <br />
    <table>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblCurrentStatus" meta:resourcekey="lblCurrentStatus" />
            </td>
            <td>
                <telerik:RadComboBox runat="server" TabIndex="1" ID="rcbCurrentStatus" Enabled="true" />
                <asp:RequiredFieldValidator
                    ID="rfvName"
                    ValidationGroup="Reservation"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="rcbCurrentStatus"
                    meta:resourcekey="ValTitle"
                    Text="*"
                    CssClass="error" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblNewStatus" meta:resourcekey="lblNewStatus" />
            </td>
            <td>
                <telerik:RadComboBox runat="server" TabIndex="1" ID="rcbNewStatus" Enabled="true" />
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidator1"
                    ValidationGroup="Reservation"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="rcbNewStatus"
                    meta:resourcekey="ValCode"
                    Text="*"
                    CssClass="error" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <telerik:RadButton Text="Change Status" runat="server" OnClick="btnChangeStatus_Click" ID="btnChangeStatus" />
            </td>
        </tr>
    </table>
</asp:Panel>