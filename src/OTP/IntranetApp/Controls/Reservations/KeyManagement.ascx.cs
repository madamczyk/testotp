﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Common.Extensions;

namespace IntranetApp.Controls.Reservations
{
    public partial class KeyManagement : System.Web.UI.UserControl
    {
        #region Fields

        #region Filter values - dates ranges - Start

        public DateTime? DateArrivalFrom
        {
            get
            {
                if (ViewState["DateArrivalFrom"] != null)
                    return (DateTime)ViewState["DateArrivalFrom"];

                return null;
            }
            set
            {
                ViewState["DateArrivalFrom"] = value;
            }
        }

        public DateTime? DateArrivalTo
        {
            get
            {
                if (ViewState["DateArrivalTo"] != null)
                    return (DateTime)ViewState["DateArrivalTo"];

                return null;
            }
            set
            {
                ViewState["DateArrivalTo"] = value;
            }
        }

        public DateTime? DateDepartureFrom
        {
            get
            {
                if (ViewState["DateDepartureFrom"] != null)
                    return (DateTime)ViewState["DateDepartureFrom"];

                return null;
            }
            set
            {
                ViewState["DateDepartureFrom"] = value;
            }
        }

        public DateTime? DateDepartureTo
        {
            get
            {
                if (ViewState["DateDepartureTo"] != null)
                    return (DateTime)ViewState["DateDepartureTo"];

                return null;
            }
            set
            {
                ViewState["DateDepartureTo"] = value;
            }
        }

        public DateTime? KeyReceivedFrom
        {
            get
            {
                if (ViewState["KeyReceivedFrom"] != null)
                    return (DateTime)ViewState["KeyReceivedFrom"];

                return null;
            }
            set
            {
                ViewState["KeyReceivedFrom"] = value;
            }
        }

        public DateTime? KeyReceivedTo
        {
            get
            {
                if (ViewState["KeyReceivedTo"] != null)
                    return (DateTime)ViewState["KeyReceivedTo"];

                return null;
            }
            set
            {
                ViewState["KeyReceivedTo"] = value;
            }
        }

        public DateTime? KeyDeliveredFrom
        {
            get
            {
                if (ViewState["KeyDeliveredFrom"] != null)
                    return (DateTime)ViewState["KeyDeliveredFrom"];

                return null;
            }
            set
            {
                ViewState["KeyDeliveredFrom"] = value;
            }
        }

        public DateTime? KeyDeliveredTo
        {
            get
            {
                if (ViewState["KeyDeliveredTo"] != null)
                    return (DateTime)ViewState["KeyDeliveredTo"];

                return null;
            }
            set
            {
                ViewState["KeyDeliveredTo"] = value;
            }
        }

        #endregion Filter values - dates ranges - End

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.reservationsLinqDS.DataResolver = () =>
            {
                IQueryable<Reservation> result = RequestManager.Services.ReservationsService.GetReservations().AsQueryable();
                if(SessionManager.CurrentUser.LoggedInRole.RoleLevel == (int)RoleLevel.Manager)
                {
                    //all properties managed by current upload manager
                    var properties = RequestManager.Services.PropertiesService.GetPropertiesByManager(SessionManager.CurrentUser.UserID, RoleLevel.Manager).ToList().Select(pr => pr.PropertyID);                    
                    result = result.Where(r => properties.Contains(r.Property.PropertyID)).AsQueryable();
                }

                rgReservations.ApplyFilter<Reservation>(ref result, "PropertyName", (q, f) => q.Where(p => p.Property.PropertyNameCurrentLanguage.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "UnitName", (q, f) => q.Where(p => p.Unit.UnitTitleCurrentLanguage.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "LastName", (q, f) => q.Where(p => p.User.Lastname.ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "ConfirmationID", (q, f) => q.Where(p => p.ConfirmationID.ToLower().Contains(f.ToLower())));

                if (DateArrivalFrom.HasValue)
                    result = result.Where(r => r.DateArrival >= this.DateArrivalFrom.Value);
                if (DateArrivalTo.HasValue)
                    result = result.Where(r => r.DateArrival <= this.DateArrivalTo.Value);

                if (DateDepartureFrom.HasValue)
                    result = result.Where(r => r.DateDeparture >= this.DateDepartureFrom.Value);
                if (DateDepartureTo.HasValue)
                    result = result.Where(r => r.DateDeparture <= this.DateDepartureTo.Value);

                if (KeyReceivedFrom.HasValue)
                    result = result.Where(r => r.KeysReceivedFromGuestDateTime.HasValue && r.KeysReceivedFromGuestDateTime.Value >= this.KeyReceivedFrom.Value);
                if (KeyReceivedTo.HasValue)
                    result = result.Where(r => r.KeysReceivedFromGuestDateTime.HasValue && r.KeysReceivedFromGuestDateTime <= this.KeyReceivedTo.Value);

                if (KeyDeliveredFrom.HasValue)
                    result = result.Where(r => r.KeysDeliveredToGuestDateTime.HasValue && r.KeysDeliveredToGuestDateTime.Value >= this.KeyDeliveredFrom.Value);
                if (KeyDeliveredTo.HasValue)
                    result = result.Where(r => r.KeysDeliveredToGuestDateTime.HasValue && r.KeysDeliveredToGuestDateTime <= this.KeyDeliveredTo.Value);

                rgReservations.ApplyFilter<Reservation>(ref result, "EstimatedArrivalTime", (q, f) => q.Where(p => p.EstimatedArrivalTime.HasValue && ArrivalTimeRangeExtensions.GetTextRepresentation((ArrivalTimeRange)p.EstimatedArrivalTime.Value).ToLower().Contains(f.ToLower())));
                rgReservations.ApplyFilter<Reservation>(ref result, "GuestConfirmedKeyDropOff", (q, f) => q.Where(p => (f.ToLower() == bool.TrueString.ToLower() && p.GuestConfirmedKeyDropOff.HasValue && p.GuestConfirmedKeyDropOff.Value == true) || (f.ToLower() == bool.FalseString.ToLower() && p.User != null) ));
                


                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgReservations_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgReservations_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Reservation obj = (Reservation)item.DataItem;

                if (obj.EstimatedArrivalTime.HasValue)
                {
                    item["EstimatedArrivalTime"].Text = ArrivalTimeRangeExtensions.GetTextRepresentation((ArrivalTimeRange)obj.EstimatedArrivalTime.Value);
                }

                if (obj.GuestConfirmedKeyDropOff.HasValue && obj.GuestConfirmedKeyDropOff.Value)
                {
                    item["GuestConfirmedKeyDropOff"].Text = "Yes";
                }

                if(!obj.KeysDeliveredToGuestDateTime.HasValue)
                {
                    RadButton btn = (RadButton)item.FindControl("btnConfirmDeliveredKeys");
                    btn.Attributes["obj-id"] = obj.ReservationID.ToString();
                    btn.Visible = true;

                    Label lbl = (Label)item.FindControl("lblConfirmDeliveredKeysDateTime");
                    lbl.Visible = false;
                }
                else
                {
                    RadButton btn = (RadButton)item.FindControl("btnConfirmDeliveredKeys");
                    btn.Visible = false;

                    Label lbl = (Label)item.FindControl("lblConfirmDeliveredKeysDateTime");
                    lbl.Text = obj.KeysDeliveredToGuestDateTime.Value.ToLocalizedDateTimeString();
                    lbl.Visible = true;
                }
                if (!obj.KeysReceivedFromGuestDateTime.HasValue)
                {
                    RadButton btn = (RadButton)item.FindControl("btnConfirmReceivedKeys");
                    btn.Attributes["obj-id"] = obj.ReservationID.ToString();
                    btn.CommandArgument = obj.ReservationID.ToString();
                    btn.Visible = true;

                    Label lbl = (Label)item.FindControl("lblConfirmReceivedKeysDateTime");
                    lbl.Visible = false;
                }
                else
                {
                    RadButton btn = (RadButton)item.FindControl("btnConfirmReceivedKeys");
                    btn.Visible = false;

                    Label lbl = (Label)item.FindControl("lblConfirmReceivedKeysDateTime");
                    lbl.Text = obj.KeysReceivedFromGuestDateTime.Value.ToLocalizedDateTimeString();
                    btn.CommandArgument = obj.ReservationID.ToString();
                    lbl.Visible = true;
                }
            }
        }

        protected void rgReservations_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgReservations.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgReservations.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgReservations.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgReservations.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgReservations.MasterTableView.Rebind();
            }
        }              

        protected void btnConfirmDeliveredKeys_Click(object sender, EventArgs e)
        {
            var btn = (sender as RadButton);
            int rId = int.Parse(btn.Attributes["obj-id"]);
            var reservation = RequestManager.Services.ReservationsService.GetReservationById(rId);
            reservation.KeysDeliveredToGuestDateTime = DateTime.Now;

            this.rgReservations.MasterTableView.Rebind();
        }

        protected void btnConfirmReceivedKeys_Click(object sender, EventArgs e)
        {
            var btn = (sender as RadButton);
            int rId = int.Parse(btn.Attributes["obj-id"]);
            var reservation = RequestManager.Services.ReservationsService.GetReservationById(rId);
            reservation.KeysReceivedFromGuestDateTime = DateTime.Now;

            this.rgReservations.MasterTableView.Rebind();
        }

        protected void rdpDateArrivalFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.DateArrivalFrom;
        }

        protected void rdpDateArrivalFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.DateArrivalFrom = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpDateArrivalTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.DateArrivalTo;
        }

        protected void rdpDateArrivalTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.DateArrivalTo = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpDateDepartureFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.DateDepartureFrom;

        }

        protected void rdpDateDepartureFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.DateDepartureFrom = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpDateDepartureTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.DateDepartureTo;
        }

        protected void rdpDateDepartureTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.DateDepartureTo = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpKeyDeliveredFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.KeyDeliveredFrom;
        }

        protected void rdpKeyDeliveredFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.KeyDeliveredFrom = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpKeyDeliveredTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.KeyDeliveredTo;
        }

        protected void rdpKeyDeliveredTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.KeyDeliveredTo = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpKeyReceivedFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.KeyReceivedFrom;
        }

        protected void rdpKeyReceivedFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.KeyReceivedFrom = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        protected void rdpKeyReceivedTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.KeyReceivedTo;
        }

        protected void rdpKeyReceivedTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.KeyReceivedTo = e.NewDate;
            rgReservations.MasterTableView.Rebind();
        }

        #endregion Event Handlers
    }
}