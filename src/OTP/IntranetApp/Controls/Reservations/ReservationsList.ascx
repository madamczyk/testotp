﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReservationsList.ascx.cs" Inherits="IntranetApp.Controls.Reservations.ReservationsList" %>
<%@ Import Namespace="Common.Extensions" %>
<%@ Import Namespace="Common.Helpers" %>
<%@ Import Namespace="System.Globalization" %>

<telerik:RadCodeBlock runat="server" ID="RadCodeBlock1">
    <script type="text/javascript">
        function RowContextMenu(sender, eventArgs) {
            var menu = $find("<%=rmReservations.ClientID %>");
            
            var evt = eventArgs.get_domEvent();

            if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                return;
            }
            var index = eventArgs.get_itemIndexHierarchical();
            document.getElementById("radGridClickedRowIndex").value = eventArgs.getDataKeyValue("ReservationID");
            sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);
            menu.enable();
            menu.show(evt);
            evt.cancelBubble = true;
            evt.returnValue = false;
            if (evt.stopPropagation) {
                evt.stopPropagation();
                evt.preventDefault();
            }

            var rowAttributes = sender.get_masterTableView().get_dataItems()[index]._element.attributes;

            DisableMenuItems(menu, 0, rowAttributes["allow-setstatus"]);
            DisableMenuItems(menu, 1, rowAttributes["allow-releasedeposit"]);
            DisableMenuItems(menu, 2, rowAttributes["allow-capturedeposit"]);
            DisableMenuItems(menu, 3, rowAttributes["allow-authorizedeposit"]);
        }

        function DisableMenuItems(menu, itemId, show) {
            var mI = menu.findItemByValue(itemId);
            if (parseInt(show.value) == 0) {
                var mI = menu.findItemByValue(itemId);
                mI.disable();
            }
        }

        function ContextMenuClick(sender, args) {
            if (args._item._properties._data.value == "0") {
                ShowEditorForm("ReservationStatus.aspx", null);
            }
            var securityDepositPage = "ReservationSecurityDeposit.aspx";
            if (args._item._properties._data.value == "1") {
                ShowEditorForm(securityDepositPage, 0);
            }
            if (args._item._properties._data.value == "2") {
                ShowEditorForm(securityDepositPage, 1);
            }
            if (args._item._properties._data.value == "3") {
                ShowEditorForm(securityDepositPage, 2);
            }
        }

        function ShowEditorForm(controlUrl, param) {
            var oWnd = $find("<%= EditStatusWindow.ClientID %>")
            var pageUrl = controlUrl + "?id=" + document.getElementById("radGridClickedRowIndex").value;
            if (param != null) {
                pageUrl += "&mode=" + param;
            }
            oWnd.setUrl(pageUrl);
            oWnd.show();
        }

        function RefreshReservationList(sender, args) {
            $find("<%= rgReservations.ClientID %>").get_masterTableView().rebind();            
        }
    </script>
</telerik:RadCodeBlock>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgReservations" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="FormWindows" runat="server">
        <Windows>
            <telerik:RadWindow OnClientClose="RefreshReservationList" ID="EditStatusWindow" runat="server" ShowContentDuringLoad="false" Width="370px" Height="290px" VisibleStatusbar="false"
                Title="Reservation Action" Modal="true" Behaviors="Move, Resize, Close">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>
    
    <div style="overflow-x: scroll; overflow-y: auto;">       
        <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex"/>
        <telerik:RadGrid 
            AutoGenerateColumns="False" 
            ID="rgReservations" 
            AllowSorting="True" 
            runat="server" 
            AllowFilteringByColumn="True" 
            AllowPaging="True"
            PageSize="20" 
            DataSourceID="reservationsLinqDS"
            OnItemCommand="rgReservations_ItemCommand" 
            OnDataBound="rgReservations_DataBound"
            OnPreRender="rgReservations_PreRender"
            OnItemDataBound="rgReservations_ItemDataBound"
            GroupingEnabled="false"
            EnableHeaderContextMenu="true">
            <ClientSettings EnableRowHoverStyle="true" Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="false" >
                <ClientEvents OnRowContextMenu="RowContextMenu" />
            </ClientSettings>
            <ExportSettings Csv-ColumnDelimiter="Comma" Csv-RowDelimiter="NewLine" Csv-FileExtension="csv" Csv-EncloseDataWithQuotes="true" IgnorePaging="true" />
		    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		    <MasterTableView TableLayout="Fixed" DataKeyNames="ReservationID" ClientDataKeyNames="ReservationID" DataSourceID="reservationsLinqDS">
                <SortExpressions>
                    <telerik:GridSortExpression FieldName="ConfirmationID" SortOrder="Descending" />
                </SortExpressions>
			    <Columns>
				    <telerik:GridBoundColumn DataField="ConfirmationID" UniqueName="ConfirmationID" HeaderText="No" SortExpression="ConfirmationID" HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />

				    <telerik:GridBoundColumn DataField="User.FullName" UniqueName="UserFullName" HeaderText="Guest" SortExpression="User.FullName" HeaderStyle-Width="150px" FilterControlWidth="120px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />
                
				    <telerik:GridBoundColumn DataField="User.FullAddress" UniqueName="UserFullAddress" HeaderText="Guest Address" SortExpression="User.FullAddress" HeaderStyle-Width="200px" FilterControlWidth="180px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />
                       
				    <telerik:GridBoundColumn DataField="BillingAddress" UniqueName="BillingAddress" HeaderText="Billing Address" SortExpression="BillingAddress" HeaderStyle-Width="200px" FilterControlWidth="180px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" Display="false" />
                
                    <telerik:GridTemplateColumn DataField="BookingStatus" HeaderText="Status" UniqueName="BookingStatus" SortExpression="BookingStatus" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="100px" ItemStyle-CssClass="link">
                        <ItemTemplate>
                            <%# DataAccess.Enums.EnumConverter.GetValue((DataAccess.Enums.BookingStatus)Eval("BookingStatus")) %>
                         </ItemTemplate>
                         <FilterTemplate>
						    <telerik:RadComboBox runat="server" ID="rcbBookingStatus" AutoPostBack="true" EnableViewState="true" OnInit="rcbBookingStatus_Init" OnSelectedIndexChanged="rcbBookingStatus_SelectedIndexChanged" OnPreRender="rcbBookingStatus_PreRender" MaxHeight="300px" Width="80%" DropDownWidth="150px" />
					    </FilterTemplate>
                    </telerik:GridTemplateColumn>
                
                    <telerik:GridTemplateColumn DataField="DateArrival" HeaderText="Arrival Date" UniqueName="DateArrival" SortExpression="DateArrival" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="150px" ItemStyle-CssClass="link">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("DateArrival")).ToLocalizedDateString() %>
                        </ItemTemplate>
                        <FilterTemplate>
                            <table style="border: 0; padding: 0; margin: 0;">
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpDateArrivalFrom" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpDateArrivalFrom_PreRender" OnSelectedDateChanged="rdpDateArrivalFrom_SelectedDateChanged">
                                            <DateInput ID="diDateArrivalFrom" runat="server" 
                                                DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                                DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                        </telerik:RadDatePicker></td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpDateArrivalTo" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpDateArrivalTo_PreRender" OnSelectedDateChanged="rdpDateArrivalTo_SelectedDateChanged" >
                                        <DateInput ID="diDateArrivalTo" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                                </tr>
                            </table>
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>
                
                    <telerik:GridTemplateColumn DataField="DateDeparture" HeaderText="Departure Date" UniqueName="DateDeparture" SortExpression="DateDeparture" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="150px" ItemStyle-CssClass="link">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("DateDeparture")).ToLocalizedDateString() %>
                         </ItemTemplate>
                        <FilterTemplate>
                            <table style="border: 0; padding: 0; margin: 0;">
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpDateDepartureFrom" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpDateDepartureFrom_PreRender" OnSelectedDateChanged="rdpDateDepartureFrom_SelectedDateChanged">
                                            <DateInput ID="diDateDepartureFrom" runat="server" 
                                                DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                                DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                        </telerik:RadDatePicker></td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpDateDepartureTo" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpDateDepartureTo_PreRender" OnSelectedDateChanged="rdpDateDepartureTo_SelectedDateChanged" >
                                        <DateInput ID="diDateDepartureTo" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                                </tr>
                            </table>
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>

				    <telerik:GridBoundColumn DataField="Property.PropertyCode" FilterControlWidth="120px" UniqueName="PropertyCode" HeaderText="Property Code" SortExpression="Property.PropertyCode" HeaderStyle-Width="150px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />

				    <telerik:GridBoundColumn DataField="UnitType.UnitTypeCode" FilterControlWidth="120px" UniqueName="UnitTypeCode" HeaderText="Unit Type Code" SortExpression="UnitType.UnitTypeCode" HeaderStyle-Width="150px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" Display="false" />

				    <telerik:GridBoundColumn DataField="Unit.UnitCode" FilterControlWidth="120px" UniqueName="UnitCode" HeaderText="Unit Code" SortExpression="Unit.UnitCode" HeaderStyle-Width="150px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" Display="false" />
                
                    <telerik:GridTemplateColumn DataField="BookingDate" HeaderText="Booking Date" UniqueName="BookingDate" SortExpression="BookingDate" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="150px" ItemStyle-CssClass="link" Display="false">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("BookingDate")).ToLocalizedDateTimeString() %>
                         </ItemTemplate>
                        <FilterTemplate>
                            <table style="border: 0; padding: 0; margin: 0;">
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpBookingDateFrom" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpBookingDateFrom_PreRender" OnSelectedDateChanged="rdpBookingDateFrom_SelectedDateChanged">
                                            <DateInput ID="diBookingDateFrom" runat="server" 
                                                DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                                DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                        </telerik:RadDatePicker></td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpBookingDateTo" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpBookingDateTo_PreRender" OnSelectedDateChanged="rdpBookingDateTo_SelectedDateChanged" >
                                        <DateInput ID="diBookingDateTo" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                                </tr>
                            </table>
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn DataField="TotalPrice" HeaderText="Full Amount" UniqueName="TotalPrice" SortExpression="TotalPrice" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="130px" ItemStyle-CssClass="link">
                        <ItemTemplate>
                            <%# ((decimal)Eval("TotalPrice")).ToString("c", (CultureInfo)Eval("DictionaryCulture.CultureInfo")) %>
                         </ItemTemplate>
                        <FilterTemplate>
                            <table style="border: 0; padding: 0; margin: 0;">
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Value_From %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadNumericTextBox ID="rntbTotalPriceFrom" runat="server" Width="40px" NumberFormat-DecimalDigits="2" OnPreRender="rntbTotalPriceFrom_PreRender" /><telerik:RadButton ID="btnTotalPriceFrom" runat="server" OnClick="TotalPriceFilter" Text="<%$ Resources: Controls, Button_Filter %>" /></td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Value_To %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadNumericTextBox ID="rntbTotalPriceTo" runat="server" Width="40px" NumberFormat-DecimalDigits="2" OnPreRender="rntbTotalPriceTo_PreRender" /><telerik:RadButton ID="btnTotalPriceTo" runat="server" OnClick="TotalPriceFilter" Text="<%$ Resources: Controls, Button_Filter %>" /></td>
                                </tr>
                            </table>
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>
                
                    <telerik:GridTemplateColumn DataField="PaymentReceived" HeaderText="Payment Received" UniqueName="PaymentReceived" SortExpression="PaymentReceived" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="130px" ItemStyle-CssClass="link">
                        <ItemTemplate>
                            <%#  ((decimal)Eval("PaymentReceived")).ToString("c", (CultureInfo)Eval("DictionaryCulture.CultureInfo")) %>
                         </ItemTemplate>
                        <FilterTemplate>
                            <table style="border: 0; padding: 0; margin: 0;">
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Value_From %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadNumericTextBox ID="rntbPaymentReceivedFrom" runat="server" Width="40px" NumberFormat-DecimalDigits="2" OnPreRender="rntbPaymentReceivedFrom_PreRender" /><telerik:RadButton ID="btnPaymentReceivedFrom" runat="server" OnClick="PaymentReceivedFilter" Text="<%$ Resources: Controls, Button_Filter %>" /></td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Value_To %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadNumericTextBox ID="rntbPaymentReceivedTo" runat="server" Width="40px" NumberFormat-DecimalDigits="2" OnPreRender="rntbPaymentReceivedTo_PreRender" /><telerik:RadButton ID="btnPaymentReceivedTo" runat="server" OnClick="PaymentReceivedFilter" Text="<%$ Resources: Controls, Button_Filter %>" /></td>
                                </tr>
                            </table>
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>
                
                    <telerik:GridTemplateColumn DataField="TripBalance" HeaderText="Balance" UniqueName="TripBalance" SortExpression="TripBalance" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="130px" ItemStyle-CssClass="link" Display="false">
                        <ItemTemplate>
                            <%#   ((decimal)Eval("TripBalance")).ToString("c", (CultureInfo)Eval("DictionaryCulture.CultureInfo")) %>
                         </ItemTemplate>
                        <FilterTemplate>
                            <table style="border: 0; padding: 0; margin: 0;">
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Value_From %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadNumericTextBox ID="rntbTripBalanceFrom" runat="server" Width="40px" NumberFormat-DecimalDigits="2" OnPreRender="rntbTripBalanceFrom_PreRender" /><telerik:RadButton ID="btnTripBalanceFrom" runat="server" OnClick="TripBalanceFilter" Text="<%$ Resources: Controls, Button_Filter %>" /></td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Value_To %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadNumericTextBox ID="rntbTripBalanceTo" runat="server" Width="40px" NumberFormat-DecimalDigits="2" OnPreRender="rntbTripBalanceTo_PreRender" /><telerik:RadButton ID="btnTripBalanceTo" runat="server" OnClick="TripBalanceFilter" Text="<%$ Resources: Controls, Button_Filter %>" /></td>
                                </tr>
                            </table>
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>
                
                    <telerik:GridTemplateColumn DataField="TransactionFee" HeaderText="Transaction Fee" UniqueName="TransactionFee" SortExpression="TransactionFee" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="130px" ItemStyle-CssClass="link">
                        <ItemTemplate>
                            <%#  ((decimal)Eval("TransactionFee")).ToString("c", (CultureInfo)Eval("DictionaryCulture.CultureInfo")) %>
                         </ItemTemplate>
                        <FilterTemplate>
                            <table style="border: 0; padding: 0; margin: 0;">
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Value_From %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadNumericTextBox ID="rntbTransactionFeeFrom" runat="server" Width="40px" NumberFormat-DecimalDigits="2" OnPreRender="rntbTransactionFeeFrom_PreRender" /><telerik:RadButton ID="btnTransactionFeeFrom" runat="server" OnClick="TransactionFeeFilter" Text="<%$ Resources: Controls, Button_Filter %>" /></td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Value_To %>" />&nbsp;</td>
                                    <td style="border: 0; padding: 0; margin: 0;"><telerik:RadNumericTextBox ID="rntbTransactionFeeTo" runat="server" Width="40px" NumberFormat-DecimalDigits="2" OnPreRender="rntbTransactionFeeTo_PreRender" /><telerik:RadButton ID="btnTransactionFeeTo" runat="server" OnClick="TransactionFeeFilter" Text="<%$ Resources: Controls, Button_Filter %>" /></td>
                                </tr>
                            </table>
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridBoundColumn DataField="Property.SalesPerson.FullName" FilterControlWidth="120px" UniqueName="SalesPersonFullName" HeaderText="Sales Person" SortExpression="Property.SalesPerson.FullName" HeaderStyle-Width="150px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />

				    <telerik:GridBoundColumn DataField="Property.User.FullNameAddress" UniqueName="HomeownerFullAddress" HeaderText="Homeowner" SortExpression="Property.User.FullNameAddress" HeaderStyle-Width="200px" FilterControlWidth="180px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" Display="false" />
                          
				    <telerik:GridBoundColumn DataField="User.BankAccountData" UniqueName="HomeownerAccount" HeaderText="Homeowner Account" SortExpression="User.BankAccountData" HeaderStyle-Width="150px" FilterControlWidth="120px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" Display="false" />
			    </Columns>
		    </MasterTableView>
	    </telerik:RadGrid>  
    </div>
    <br />
    <telerik:RadButton runat="server" ID="btnGenerateCSVFile" Text="Generate CSV File" OnClick="btnGenerateCSVFile_Click" />

    <telerik:RadContextMenu ID="rmReservations" runat="server" 
        EnableRoundedCorners="true" EnableShadows="true" OnClientItemClicked="ContextMenuClick" >
        <Items>
            <telerik:RadMenuItem Text="Reservation Status" Value="0">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem Text="Release Security Deposit" Value="1">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem Text="Capture Security Deposit" Value="2">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem Text="Authorize Security Deposit" Value="3">
            </telerik:RadMenuItem>
        </Items>
    </telerik:RadContextMenu>

</asp:Panel>

<otpDS:SimpleDataSource ID="reservationsLinqDS" runat="server" />