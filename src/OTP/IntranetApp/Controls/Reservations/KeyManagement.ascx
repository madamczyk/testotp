﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KeyManagement.ascx.cs" Inherits="IntranetApp.Controls.Reservations.KeyManagement" %>
<%@ Import Namespace="Common.Extensions" %>
<%@ Import Namespace="Common.Helpers" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlKeyManagement">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="pnlKeyManagement" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlKeyManagement">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgReservations" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="reservationsLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgReservations_ItemCommand" 
        OnItemDataBound="rgReservations_ItemDataBound" 
        OnDataBound="rgReservations_DataBound">
        <ClientSettings EnableRowHoverStyle="False">
            <Selecting AllowRowSelect="False" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="ReservationID" DataSourceID="reservationsLinqDS">
			<Columns>
				<telerik:GridBoundColumn DataField="Property.PropertyNameCurrentLanguage" UniqueName="PropertyName" HeaderText="Property Nickname" SortExpression="Property.PropertyNameCurrentLanguage"  ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="100px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Unit.UnitTitleCurrentLanguage"  UniqueName="UnitName" HeaderText="Unit Name" SortExpression="Unit.UnitTitleCurrentLanguage" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="100px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="User.LastName" UniqueName="LastName" HeaderText="Guest Last Name" SortExpression="User.LastName"  ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="100px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ConfirmationID"  UniqueName="ConfirmationID" HeaderText="Reservation Confirmation" SortExpression="ConfirmationID" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="100px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderStyle-Width="120px" DataField="DateArrival" UniqueName="DateArrival" HeaderText="Arrival Date" SortExpression="DateArrival" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<ItemTemplate>
                        <%# ((DateTime)Eval("DateArrival")).ToLocalizedDateString() %>
                    </ItemTemplate>
                    <FilterTemplate>
                        <table style="border: 0; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label ID="Label112341" runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpDateArrivalFrom" Width="80px" runat="server" AutoPostBack="true" OnPreRender="rdpDateArrivalFrom_PreRender" OnSelectedDateChanged="rdpDateArrivalFrom_SelectedDateChanged">
                                        <DateInput ID="diTimestampFrom" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label ID="Label2" runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpDateArrivalTo" Width="80px" runat="server" AutoPostBack="true" OnPreRender="rdpDateArrivalTo_PreRender" OnSelectedDateChanged="rdpDateArrivalTo_SelectedDateChanged">
                                    <DateInput ID="diTimestampTo" runat="server" 
                                        DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                </telerik:RadDatePicker></td>
                            </tr>
                        </table>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="EstimatedArrivalTime" HeaderStyle-Width="120px" UniqueName="EstimatedArrivalTime" HeaderText="Arrival Time" SortExpression="EstimatedArrivalTime"  ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="100px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridTemplateColumn  HeaderStyle-Width="120px" DataField="DateDeparture" UniqueName="DateDeparture" HeaderText="Departure Date" SortExpression="DateDeparture" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<ItemTemplate>
                        <%# ((DateTime)Eval("DateDeparture")).ToLocalizedDateString() %>
                    </ItemTemplate>
                    <FilterTemplate>
                        <table style="border: 0; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label ID="Label3" runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpDateDepartureFrom" Width="80px" runat="server" AutoPostBack="true" OnPreRender="rdpDateDepartureFrom_PreRender" OnSelectedDateChanged="rdpDateDepartureFrom_SelectedDateChanged" >
                                        <DateInput ID="DateInput1" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label ID="Label4" runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpDateDepartureTo" Width="80px" runat="server" AutoPostBack="true" OnPreRender="rdpDateDepartureTo_PreRender" OnSelectedDateChanged="rdpDateDepartureTo_SelectedDateChanged" >
                                    <DateInput ID="DateInput2" runat="server" 
                                        DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                </telerik:RadDatePicker></td>
                            </tr>
                        </table>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="GuestConfirmedKeyDropOff"  UniqueName="GuestConfirmedKeyDropOff" HeaderText="Guest Confirmed key drop-off" SortExpression="GuestConfirmedKeyDropOff"  ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="KeysDeliveredToGuestDateTime" HeaderText="Keys delivered to Guest" UniqueName="KeysDeliveredToGuestDateTime" SortExpression="KeysDeliveredToGuestDateTime" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true">  
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
                    <ItemTemplate>  
                        <asp:Label runat="server" ID="lblConfirmDeliveredKeysDateTime" />
                        <telerik:RadButton ID="btnConfirmDeliveredKeys" Text="Confirm" runat="server" OnClick="btnConfirmDeliveredKeys_Click" />
                     </ItemTemplate>
                    <FilterTemplate>
                        <table style="border: 0; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label ID="Label45341" runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpKeyDeliveredFrom" Width="80px" runat="server" AutoPostBack="true" OnPreRender="rdpKeyDeliveredFrom_PreRender" OnSelectedDateChanged="rdpKeyDeliveredFrom_SelectedDateChanged" >
                                        <DateInput ID="diTimestampFrom" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label ID="Label252" runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpKeyDeliveredTo" Width="80px" runat="server" AutoPostBack="true" OnPreRender="rdpKeyDeliveredTo_PreRender" OnSelectedDateChanged="rdpKeyDeliveredTo_SelectedDateChanged">
                                    <DateInput ID="diTimestampTo" runat="server" 
                                        DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                </telerik:RadDatePicker></td>
                            </tr>
                        </table>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="KeysReceivedFromGuestDateTime" HeaderText="Keys received from Guest" UniqueName="KeysReceivedFromGuestDateTime" SortExpression="KeysReceivedFromGuestDateTime" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true">  
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
                    <ItemTemplate>  
                        <asp:Label runat="server" ID="lblConfirmReceivedKeysDateTime" />
                        <telerik:RadButton ID="btnConfirmReceivedKeys" Text="Confirm" runat="server" OnClick="btnConfirmReceivedKeys_Click" />
                     </ItemTemplate>
                    <FilterTemplate>
                        <table style="border: 0; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label ID="Label2321" runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpKeyReceivedFrom" Width="80px" runat="server" AutoPostBack="true" OnPreRender="rdpKeyReceivedFrom_PreRender" OnSelectedDateChanged="rdpKeyReceivedFrom_SelectedDateChanged" >
                                        <DateInput ID="diTimestampFrom" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label ID="Labe2342431l2" runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpKeyReceivedTo" Width="80px" runat="server" AutoPostBack="true" OnPreRender="rdpKeyReceivedTo_PreRender" OnSelectedDateChanged="rdpKeyReceivedTo_SelectedDateChanged" >
                                    <DateInput ID="diTimestampTo" runat="server" 
                                        DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                </telerik:RadDatePicker></td>
                            </tr>
                        </table>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>                                			
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="Property.PropertyNameCurrentLanguage" SortOrder="Ascending" />
            </SortExpressions>
		</MasterTableView>
		<HeaderContextMenu EnableImageSprites="True">
		</HeaderContextMenu>
	</telerik:RadGrid>
</asp:Panel>

<otpDS:SimpleDataSource ID="reservationsLinqDS" runat="server" />