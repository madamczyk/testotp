﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReservationSecurityDeposit.ascx.cs" Inherits="IntranetApp.Controls.Reservations.ReservationSecurityDeposit" %>

<script type="text/javascript">
    function btn_Discard_No() {
        var oWnd = $find("<%= rwConfirm.ClientID %>")
        oWnd.argument = null;
        oWnd.close();
        return false;
    }

</script>

<telerik:RadWindowManager ID="rwmManager" runat="server">
    <Windows>
        <telerik:RadWindow Title="Change Reservation Status" ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Height="180px" Width="280px"  Behaviors="Move" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" meta:resourcekey="lblConfirmationText" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="wndBtnDiscard_Yes_Click" ></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClientClick="btn_Discard_No(); return false;" ></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<asp:Panel runat="server" ID="pnlData">
    <asp:ValidationSummary ID="vsSummary" ValidationGroup="Reservation" HeaderText=" "
        DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
    <h1 class="FormHeader">
        <asp:Label runat="server" ID="lblHeader" meta:resourcekey="lblHeader"></asp:Label>
    </h1>
    <br />
    <table style="width: 100%;">      
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="lblSecurityDeposit" meta:resourcekey="lblSecurityDeposit" />
            </td>
        </tr>
        <tr>
            <td style="height: 25px;"></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="lblCurrencyCode" />
                <telerik:RadNumericTextBox runat="server" TabIndex="1" ID="txtSecurityDeposit" Enabled="true" />
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidator1"
                    ValidationGroup="Reservation"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="txtSecurityDeposit"
                    meta:resourcekey="ValDeposit"
                    Text="*"
                    CssClass="error" />
            </td>
        </tr>
        <tr>
            <td style="height: 25px;"></td>
            <td>
                
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <asp:Button Text="Sumbit" runat="server" OnClick="btnSubmitAction_Click" ID="btnSubmitAction" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="lblActionResult" />
            </td>
        </tr>
    </table>
</asp:Panel>