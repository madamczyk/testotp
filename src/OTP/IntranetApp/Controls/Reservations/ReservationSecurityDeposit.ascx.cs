﻿using BusinessLogic.Managers;
using BusinessLogic.PaymentGateway;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Reservations
{
    public partial class ReservationSecurityDeposit : System.Web.UI.UserControl
    {
        public enum DepositAction
        {
            Release = 0,
            Capture = 1,
            Authorize = 2
        }

        public int? ReservationID
        {
            get { return ViewState["ReservationID"] as int?; }
            set { ViewState["ReservationID"] = value; }
        }

        public int? FormMode
        {
            get { return ViewState["FormMode"] as int?; }
            set { ViewState["FormMode"] = value; }
        }

        public DepositAction CurrentAction
        {
            get
            {
                return (DepositAction)FormMode.Value;
            }
        }

        public string CloseWindowScript
        {
            get
            {
                return "function GetRadWindow() {{" +
                    "var oWindow = null;" +
                    "if (window.radWindow) oWindow = window.radWindow;" +
                    "else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;" +
                    "return oWindow;" +
                    "}}";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            var reservation = RequestManager.Services.ReservationsService.GetReservationById(this.ReservationID.Value);
            this.lblCurrencyCode.Text = reservation.DictionaryCulture.ISOCurrencySymbol;
            if (reservation.SecurityDeposit.HasValue)
                this.txtSecurityDeposit.Text = reservation.SecurityDeposit.Value.ToString("N2", reservation.DictionaryCulture.CultureInfo);

            string headerText = string.Empty;
            switch (CurrentAction)
            {
                case DepositAction.Authorize:
                    headerText = GetLocalResourceObject("lblAuthorizeDeposit").ToString();
                    btnSubmitAction.Text = GetLocalResourceObject("btnAuthorize.Text").ToString();
                    lblSecurityDeposit.Text = GetLocalResourceObject("lblSecurityDeposit.Text").ToString();
                    break;
                case DepositAction.Capture:
                    headerText = GetLocalResourceObject("lblCaptureDeposit").ToString();
                    btnSubmitAction.Text = GetLocalResourceObject("btnCapture.Text").ToString();
                    lblSecurityDeposit.Text = string.Format(GetLocalResourceObject("lblCurrentSecurityDeposit.Text").ToString(),
                        reservation.SecurityDeposit.Value.ToString("c", reservation.DictionaryCulture.CultureInfo));
                    break;
                case DepositAction.Release:
                    headerText = GetLocalResourceObject("lblReleaseDeposit").ToString();
                    btnSubmitAction.Text = GetLocalResourceObject("btnRelease.Text").ToString();
                    this.txtSecurityDeposit.Enabled = false;
                    lblSecurityDeposit.Text = string.Format(GetLocalResourceObject("lblCurrentSecurityDeposit.Text").ToString(),
                        reservation.SecurityDeposit.Value.ToString("c", reservation.DictionaryCulture.CultureInfo));
                    break;
            }
            this.lblHeader.Text = string.Format(headerText, reservation.ConfirmationID);   
        }

        protected void btnSubmitAction_Click(object sender, EventArgs e)
        {
            var reservation = RequestManager.Services.ReservationsService.GetReservationById(this.ReservationID.Value);
            var confirmationText = GetLocalResourceObject("Confirmation.Text").ToString();
            confirmationText = string.Format(confirmationText + " " + reservation.DictionaryCulture.ISOCurrencySymbol, 
                GetLocalResourceObject("btn" + CurrentAction.ToString() + ".Text").ToString().ToLower(),
                this.txtSecurityDeposit.Text.ToString());
            lblConfirmation.Text = confirmationText;
            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void wndBtnDiscard_Yes_Click(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
            bool result = true;
            switch (CurrentAction)
            {
                case DepositAction.Authorize:
                    result = AuthorizeDeposit();
                    break;
                case DepositAction.Capture:
                    result = CaptureDeposit();
                    break;
                case DepositAction.Release:
                    result = ReleaseDeposit();
                    break;
            }

            if (result == true)
            {
                this.txtSecurityDeposit.Enabled = false;
                this.btnSubmitAction.Enabled = false;
                this.lblActionResult.ForeColor = Color.Green;
                this.lblActionResult.Text = GetLocalResourceObject("lblActionResult_Success.Text").ToString();
            }
            else
            {
                this.lblActionResult.ForeColor = Color.Red;
                this.btnSubmitAction.Enabled = true;
                this.lblActionResult.Text = GetLocalResourceObject("lblActionResult_Failed.Text").ToString();
            }
        }

        protected void wndBtnDiscard_No_Click(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        private TransactionRequest CreateTransactionRequest(Reservation rsv, decimal amount)
        {
            decimal amountInCents = amount * 100;
            TransactionRequest request = new TransactionRequest()
            {
                Amount = ((int)amountInCents).ToString(),
                CurrencyCode = rsv.DictionaryCulture.ISOCurrencySymbol,
                PaymentToken = rsv.GuestPaymentMethod.ReferenceToken
            };
            return request;
        }

        private bool AuthorizeDeposit()
        {
            decimal securityDeposit;
            if (decimal.TryParse(this.txtSecurityDeposit.Text, out securityDeposit))
            {
                var currentReservation = RequestManager.Services.ReservationsService.GetReservationById(this.ReservationID.Value);

                TransactionRequest tr = CreateTransactionRequest(currentReservation, securityDeposit);

                var authorizeResponse = RequestManager.Services.PaymentGatewayService.AuthorizePayment(tr, currentReservation.ReservationID);
                if (authorizeResponse != null && authorizeResponse.Succeded)
                {
                    currentReservation.SecurityDepositToken = authorizeResponse.Token;
                    currentReservation.SecurityDeposit = securityDeposit;
                    return true;
                }
            }
            return false;
        }

        private bool CaptureDeposit()
        {
            var currentReservation = RequestManager.Services.ReservationsService.GetReservationById(this.ReservationID.Value);
            
            decimal capturedDeposit;
            if (decimal.TryParse(this.txtSecurityDeposit.Text, out capturedDeposit))
            {
                if (capturedDeposit != currentReservation.SecurityDeposit)
                {
                    var response = RequestManager.Services.PaymentGatewayService.CancelPayment(currentReservation.TransactionToken, currentReservation.ReservationID);
                    if (response == null || !response.Succeded)
                        return false;
                    TransactionRequest tr = CreateTransactionRequest(currentReservation, capturedDeposit);

                    var authorizeResponse = RequestManager.Services.PaymentGatewayService.AuthorizePayment(tr, currentReservation.ReservationID);
                    if (authorizeResponse != null && authorizeResponse.Succeded)
                    {
                        currentReservation.SecurityDepositToken = authorizeResponse.Token;
                    }
                }
                var captureResponse = RequestManager.Services.PaymentGatewayService.CapturePayment(currentReservation.SecurityDepositToken, currentReservation.ReservationID);
                if (captureResponse != null && captureResponse.Succeded)
                {
                    currentReservation.CapturedSecurityDeposit = capturedDeposit;
                    currentReservation.SecurityDepositToken = null;
                    return true;
                }
            }
            return false;
        }

        private bool ReleaseDeposit()
        {
            var currentReservation = RequestManager.Services.ReservationsService.GetReservationById(this.ReservationID.Value);
            var response = RequestManager.Services.PaymentGatewayService.CancelPayment(currentReservation.TransactionToken, currentReservation.ReservationID);
            if (response != null && response.Succeded)
            {
                currentReservation.SecurityDepositToken = null;
                return true;
            }
            return false;
        }    
    }
}