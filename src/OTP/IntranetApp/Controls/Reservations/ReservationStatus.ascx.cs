﻿using BusinessLogic.Managers;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Reservations
{
    public partial class ReservationStatus : System.Web.UI.UserControl
    {
        public int? ReservationID
        {
            get { return ViewState["ReservationID"] as int?; }
            set { ViewState["ReservationID"] = value; }
        }

        public string CloseWindowScript
        {
            get
            {
                return "function GetRadWindow() {{" +
                    "var oWindow = null;" +
                    "if (window.radWindow) oWindow = window.radWindow;" +
                    "else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;" +
                    "return oWindow;" +
                    "}}";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            var reservation = RequestManager.Services.ReservationsService.GetReservationById(this.ReservationID.Value);
            this.lblHeader.Text = string.Format(GetLocalResourceObject("lblHeader.Text").ToString(), reservation.ConfirmationID);
            
            this.rcbCurrentStatus.Items.Add(new RadComboBoxItem(EnumConverter.GetValue((BookingStatus)reservation.BookingStatus), reservation.BookingStatus.ToString()));
        
            if(reservation.BookingStatus == (int)BookingStatus.CheckedOut)
            {
                this.rcbNewStatus.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(BookingStatus.DamageInvestigation), ((int)BookingStatus.DamageInvestigation).ToString()));
            }
            else if (reservation.BookingStatus == (int)BookingStatus.DamageInvestigation)
            {
                this.rcbNewStatus.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(BookingStatus.Completed), ((int)BookingStatus.Completed).ToString()));
            }
            else
            {
                this.rcbNewStatus.Items.Add(new RadComboBoxItem(EnumConverter.GetValue((BookingStatus)reservation.BookingStatus), reservation.BookingStatus.ToString()));
                this.btnChangeStatus.Enabled = false;
                this.btnChangeStatus.ToolTip = GetLocalResourceObject("lblNoManualTransition.Text").ToString();
            }
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            var newStatus = (BookingStatus)(int.Parse(this.rcbNewStatus.SelectedValue));
            var currentStatus = (BookingStatus)(int.Parse(this.rcbCurrentStatus.SelectedValue));


            lblConfirmation.Text = string.Format(GetLocalResourceObject("lblConfirmation.Text").ToString(),
                EnumConverter.GetValue(currentStatus), EnumConverter.GetValue(newStatus));
            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void wndBtnDiscard_Yes_Click(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
            var newStatus = (BookingStatus)(int.Parse(this.rcbNewStatus.SelectedValue));
            RequestManager.Services.ReservationsService.SetReservationStatus(newStatus, this.ReservationID.Value);
            string script = "<script language=JavaScript> " + CloseWindowScript + " GetRadWindow().close(1); </script>";
            Response.Write(script);
        }

        protected void wndBtnDiscard_No_Click(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }
    }
}