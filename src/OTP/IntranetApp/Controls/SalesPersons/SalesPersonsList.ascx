﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SalesPersonsList.ascx.cs" Inherits="IntranetApp.Controls.SalesPersons.SalesPersonsList" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgSalesPersons" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle"/>
    </h1>
    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgSalesPersons" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="salesPersonsLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgSalesPersons_ItemCommand" 
        OnItemDataBound="rgSalesPersons_ItemDataBound"
        OnDataBound="rgSalesPersons_DataBound">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="SalesPersonId" DataSourceID="salesPersonsLinqDS">
			<Columns>
				<telerik:GridBoundColumn DataField="FirstName" FilterControlWidth="150px" UniqueName="FirstName" HeaderText="First name" SortExpression="FirstName" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LastName" FilterControlWidth="150px" UniqueName="LastName" HeaderText="Last name" SortExpression="LastName" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="CommisionDate" FilterControlWidth="150px" UniqueName="CommisionDate" HeaderText="Commission date" SortExpression="CommisionDate" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
                    <ItemTemplate> 
                        <%# Common.Helpers.DateTimeHelper.GetLocalizedDateString((DateTime)Eval("CommisionDate"))%> 
                     </ItemTemplate> 
				</telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="Percentage" HeaderText="Commission" UniqueName="Percentage" SortExpression="Percentage" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="false">  
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
                    <ItemTemplate> 
                        <%# string.Format("{0:0.00} %", Eval("Percentage"))%> 
                     </ItemTemplate> 
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>		
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="LastName" SortOrder="Ascending" />
            </SortExpressions>
		</MasterTableView>
		<HeaderContextMenu EnableImageSprites="True">
		</HeaderContextMenu>
	</telerik:RadGrid>
    <br />
    <telerik:RadButton runat="server" ID="btnAddSalesPerson" Text="Add" OnClick="btnAddSalesPerson_Click" />
</asp:Panel>

<otpDS:SimpleDataSource ID="salesPersonsLinqDS" runat="server" />