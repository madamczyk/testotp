﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Diagnostics;
using System.Web;
using Telerik.Web.UI;
using System.Linq;
using System.Threading;

namespace IntranetApp.Controls.SalesPersons
{
    public partial class SalesPersonDetails : BaseUserControl
    {
        #region Fields

        public int? SalesPersonId
        {
            get { return ViewState["SalesPersonId"] as int?; }
            set { ViewState["SalesPersonId"] = value; }
        }

        public FormMode Mode
        {
            get
            {
                if (ViewState.Count > 0)
                {
                    try
                    {
                        return (FormMode)ViewState["Mode"];
                    }
                    catch
                    {
                        return FormMode.Add;
                    }
                }

                return FormMode.Add;
            }
            set { ViewState["Mode"] = (int)value; }
        }

        #endregion Fields


        #region Methods

        private void ParseQueryString()
        {
            int id = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out id))
            {
                SalesPersonId = id;
            }
        }

        private void LoadData()
        {
            Debug.Assert(SalesPersonId.HasValue);

            SalesPerson salesPerson = RequestManager.Services.SalesPersonsService.GetSalesPersonById(SalesPersonId.Value);

            // text boxes
            rtbFirstName.Text = salesPerson.FirstName;
            rtbLastName.Text = salesPerson.LastName;
            rdpCommission.SelectedDate = salesPerson.CommisionDate;

            string percentage = salesPerson.Percentage.ToString("0.00");

            if (string.Compare(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator, ",") == 0)
            {
                percentage = percentage.Replace(",", ".");
            }

            rntbCommission.Text = percentage;
        }

        private bool SaveData()
        {
            SalesPerson salesPerson = null;

            if (Mode == FormMode.Add)
            {
                salesPerson = new SalesPerson();
            }
            else
            {
                salesPerson = RequestManager.Services.SalesPersonsService.GetSalesPersonById(SalesPersonId.Value);
            }

            if (salesPerson == null)
            {
                return false;
            }

            salesPerson.FirstName = rtbFirstName.Text;
            salesPerson.LastName = rtbLastName.Text;
            salesPerson.CommisionDate = rdpCommission.SelectedDate.Value;

            if (string.Compare(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator, ",") == 0)
            {
                salesPerson.Percentage = decimal.Parse(rntbCommission.Text.Replace(".", ","), Thread.CurrentThread.CurrentCulture);
            }
            else
            {
                salesPerson.Percentage = decimal.Parse(rntbCommission.Text.Replace(",", "."), Thread.CurrentThread.CurrentCulture);
            }

            if (Mode == FormMode.Add)
            {
                RequestManager.Services.SalesPersonsService.AddSalesPerson(salesPerson);
            }
            else
            {
                RequestManager.Services.SaveChanges();
            }

            return true;
        }

        private bool CheckExists()
        {
            if (SalesPersonId.HasValue && RequestManager.Services.SalesPersonsService.GetSalesPersonById(SalesPersonId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void SetNames()
        {
            if (Mode == FormMode.Edit)
            {
                SalesPerson salesPerson = RequestManager.Services.SalesPersonsService.GetSalesPersonById(SalesPersonId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"), string.Format("{0} {1}", salesPerson.FirstName, salesPerson.LastName));
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"), string.Format("{0} {1}", rtbFirstName.Text, rtbLastName.Text));
            }
        }

        private void AddScripts()
        {
            // enable warning before unloading the page
            RadScriptManager.RegisterStartupScript(this, GetType(),
                "startup_warn",
                "\twarnBeforeUnload = true;\n\tenableWarning();\n\twarningBeforeUnload = \"" + HttpUtility.JavaScriptStringEncode(String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_LeaveConfrmation"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"), String.IsNullOrWhiteSpace(string.Format("{0} {1}", rtbFirstName.Text, rtbLastName.Text)) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : string.Format("{0} {1}", rtbFirstName.Text, rtbLastName.Text))) + "\";\n",
                true);

            // disable warning before postback
            RadScriptManager.RegisterOnSubmitStatement(this, GetType(),
                "onsubmit_disable_warn",
                "disableWarning();");
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ParseQueryString();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                    {
                        LoadData();
                    }

                    SetNames();
                }
            }

            //enter button
            Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                rtbFirstName.Focus();
            }

            AddScripts();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"));

                    rwError.VisibleOnPageLoad = true;
                }
                else
                {
                    Response.Redirect("~/Forms/SalesPersons/SalesPersonsList.aspx");
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
            {
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"), String.IsNullOrWhiteSpace(string.Format("{0} {1}", rtbFirstName.Text, rtbLastName.Text)) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : string.Format("{0} {1}", rtbFirstName.Text, rtbLastName.Text));
            }

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/SalesPersons/SalesPersonsList.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/SalesPersons/SalesPersonsList.aspx");
        }

        #endregion
    }
}