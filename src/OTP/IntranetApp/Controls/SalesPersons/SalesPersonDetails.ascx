﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SalesPersonDetails.ascx.cs" Inherits="IntranetApp.Controls.SalesPersons.SalesPersonDetails" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="pnlData">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlData" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="rwmMainWindow" runat="server">
    <Windows>
        <telerik:RadWindow ID="rwConfirm" Behaviors="None" Height="140px" Modal="true" runat="server" VisibleOnPageLoad="false" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" Text="<%$ Resources: Controls, Dialog_Discard_Text %>" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="btnDiscard_Yes"></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClick="btnDiscard_No"></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
        <telerik:RadWindow ID="rwError" Behaviors="None" Height="140px" Modal="true" runat="server" VisibleOnPageLoad="false" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblError" Font-Bold="true" runat="server" />
                        <br />
                        <br />
                        <asp:Button ID="wndBtnError_Ok" runat="server" Text="<%$ Resources: Controls, Dialog_Ok %>" OnClick="btnError_Ok" />
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<asp:Panel runat="server" ID="pnlData">
    <asp:ValidationSummary ID="vsSummary" ValidationGroup="SalesPerson" HeaderText=" "
        DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
    <h1 class="FormHeader">
        <asp:Label runat="server" ID="lblHeader" meta:resourcekey="lblHeaderAdd"></asp:Label>
    </h1>
    <br />
    <table>
        <tr>
            <td width="100px">
                <asp:Label runat="server" ID="lblFirstName" meta:resourcekey="lblFirstName" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" TabIndex="1" ID="rtbFirstName" Enabled="true" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvFirstName" ValidationGroup="SalesPerson" runat="server" ControlToValidate="rtbFirstName"
                    Display="Dynamic" meta:resourcekey="erFirstName" Text="*" CssClass="error" />
            </td>
        </tr>
        <tr>
            <td width="100px">
                <asp:Label runat="server" ID="lblLastName" meta:resourcekey="lblLastName" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" TabIndex="2" ID="rtbLastName" Enabled="true" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvLastName" ValidationGroup="SalesPerson" runat="server" ControlToValidate="rtbLastName"
                    Display="Dynamic" meta:resourcekey="erLastName" Text="*" CssClass="error" />
            </td>
        </tr>
        <tr>
            <td width="100px">
                <asp:Label runat="server" ID="lblCommission" meta:resourcekey="lblCommission" />
            </td>
            <td>
                <telerik:RadNumericTextBox ID="rntbCommission" TabIndex="3" MinValue="1" MaxValue="100" NumberFormat-DecimalDigits="2" NumberFormat-DecimalSeparator="<%# Common.CurrentCultureInfoSet.NumberSeparator %>" runat="server" Type="Percent" Value="1" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvCommission" ValidationGroup="SalesPerson" runat="server" ControlToValidate="rntbCommission"
                    Display="Dynamic" meta:resourcekey="erCommission" Text="*" CssClass="error" />
            </td>
        </tr>
        <tr>
            <td width="100px">
                <asp:Label runat="server" ID="lblCommissionDate" meta:resourcekey="lblCommissionDate" />
            </td>
            <td>
                <telerik:RadDatePicker ID="rdpCommission" TabIndex="4" runat="server" DateInput-DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" SelectedDate="<%# DateTime.Now %>" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvCommissionDate" ValidationGroup="SalesPerson" runat="server" ControlToValidate="rdpCommission"
                    Display="Dynamic" meta:resourcekey="erCommissionDate" Text="*" CssClass="error" />
            </td>
        </tr>
    </table>
</asp:Panel>

<table>
    <tr style="height: 20px;">
        <td></td>
    </tr>
    <tr>
        <td width="645px" align="right" style="padding-right: 3px">
            <telerik:RadButton runat="server" ID="btnSave" TabIndex="5" Text="<%$ Resources: Controls, Button_Save %>" CausesValidation="true" ValidationGroup="SalesPerson" OnClick="btnSave_Click" />
            <telerik:RadButton runat="server" ID="btnCancel" TabIndex="6" Text="<%$ Resources: Controls, Button_Cancel %>" CausesValidation="false" OnClick="btnCancel_Click" />
        </td>
    </tr>
</table>