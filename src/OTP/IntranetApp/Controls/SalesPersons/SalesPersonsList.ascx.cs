﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Enums;
using System;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.SalesPersons
{
    public partial class SalesPersonsList : System.Web.UI.UserControl
    {
        #region Methods

        private void BindGrid()
        {
            salesPersonsLinqDS.DataResolver = () =>
            {
                IQueryable<SalesPerson> result = RequestManager.Services.SalesPersonsService.GetSalesPersons().AsQueryable();
                rgSalesPersons.ApplyFilter<SalesPerson>(ref result, "LastName", (q, f) => q.Where(p => p.LastName.ToLower().Contains(f.ToLower())));
                rgSalesPersons.ApplyFilter<SalesPerson>(ref result, "FirstName", (q, f) => q.Where(p => p.FirstName.ToLower().Contains(f.ToLower())));

                return result.ToList();
            };
        }

        #endregion Methods

        #region Events Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgSalesPersons_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int salesPersonId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["SalesPersonId"].ToString());

                if (RequestManager.Services.SalesPersonsService.GetSalesPersonById(salesPersonId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/SalesPersons/SalesPersonDetails.aspx?id=");
                    sb.Append(salesPersonId);
                    sb.Append("&mode=");
                    sb.Append((int)FormMode.Edit);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int salesPersonId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["SalesPersonId"].ToString());

                if (RequestManager.Services.SalesPersonsService.GetSalesPersonById(salesPersonId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    RequestManager.Services.SalesPersonsService.DeleteSalesPerson(salesPersonId);
                }
            }
        }

        protected void rgSalesPersons_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                SalesPerson obj = (SalesPerson)item.DataItem;
                string salesPersonName = string.Format("{0} {1}", obj.FirstName, obj.LastName);
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgSalesPersons.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"), salesPersonName), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_SalesPerson"), salesPersonName));
            }
        }

        protected void rgSalesPersons_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgSalesPersons.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgSalesPersons.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgSalesPersons.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgSalesPersons.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgSalesPersons.MasterTableView.Rebind();
            }
        }

        protected void btnAddSalesPerson_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("~/Forms/SalesPersons/SalesPersonDetails.aspx?");
            sb.Append("mode=");
            sb.Append((int)FormMode.Add);

            Response.Redirect(sb.ToString());
        }

        #endregion
    }
}