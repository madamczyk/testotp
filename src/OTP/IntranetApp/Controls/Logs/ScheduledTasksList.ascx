﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScheduledTasksList.ascx.cs" Inherits="IntranetApp.Controls.Logs.ScheduledTasksList" %>
<%@ Import Namespace="Common.Extensions" %>
<%@ Import Namespace="Common.Helpers" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgScheduledTasks" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<style type="text/css">
    .taMessage {
        display: block;
        width: 90%;
        height: 90%;
        margin: auto;
    }
</style>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rwPreview" runat="server" VisibleOnPageLoad="false" Height="500px" Width="500px" Behaviors="Move, Resize, Close">
                <ContentTemplate>
                    <br />
                    <telerik:RadTextBox id="taMessage" runat="server" TextMode="MultiLine" Width="90%" Wrap="false" EnabledStyle-CssClass="taMessage" />
                    <br />
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgScheduledTasks" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="scheduledTasksLinqDS"
        OnItemDataBound="rgScheduledTasks_ItemDataBound"
        OnItemCommand="rgScheduledTasks_ItemCommand" 
        OnDataBound="rgScheduledTasks_DataBound">
        <ClientSettings EnableRowHoverStyle="true" Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="false" />
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="ScheduledTaskId" DataSourceID="scheduledTasksLinqDS">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="NextPlannedExecutionTime" SortOrder="Descending" />
            </SortExpressions>
			<Columns>      
                <telerik:GridTemplateColumn DataField="ScheduledTaskType" HeaderText="Task Type" UniqueName="ScheduledTaskType" SortExpression="ScheduledTaskType" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="100px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# DataAccess.Enums.EnumConverter.GetValue((DataAccess.Enums.ScheduledTaskType)Eval("ScheduledTaskType")) %>
                    </ItemTemplate>
                    <FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbScheduledTaskType" AutoPostBack="true" EnableViewState="true" OnInit="rcbScheduledTaskType_Init" OnSelectedIndexChanged="rcbScheduledTaskType_SelectedIndexChanged" OnPreRender="rcbScheduledTaskType_PreRender" MaxHeight="300px" Width="90%" DropDownWidth="150px" />
					</FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="ScheduleType" HeaderText="Schedule Type" UniqueName="ScheduleType" SortExpression="ScheduleType" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="100px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# DataAccess.Enums.EnumConverter.GetValue((DataAccess.Enums.ScheduleType)Eval("ScheduleType")) %>
                    </ItemTemplate>
                    <FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbScheduleType" AutoPostBack="true" EnableViewState="true" OnInit="rcbScheduleType_Init" OnSelectedIndexChanged="rcbScheduleType_SelectedIndexChanged" OnPreRender="rcbScheduleType_PreRender" MaxHeight="300px" Width="90%" DropDownWidth="150px" />
					</FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridBoundColumn DataField="ScheduleDetails" UniqueName="ScheduleDetails" HeaderText="Schedule Details" SortExpression="ScheduleDetails" HeaderStyle-Width="100px" FilterControlWidth="80%" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />

                <telerik:GridTemplateColumn DataField="LastExecutionTime" HeaderText="Last Execution Time" UniqueName="LastExecutionTime" SortExpression="LastExecutionTime" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="150px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%#  Eval("LastExecutionTime") != null ? ((DateTime)Eval("LastExecutionTime")).ToLocalizedDateTimeString() : "" %>
                    </ItemTemplate>
                    <FilterTemplate>
                        <table style="border: 0; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpLastExecutionTimeFrom" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpLastExecutionTimeFrom_PreRender" OnSelectedDateChanged="rdpLastExecutionTimeFrom_SelectedDateChanged">
                                        <DateInput ID="diLastExecutionTimeFrom" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpLastExecutionTimeTo" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpLastExecutionTimeTo_PreRender" OnSelectedDateChanged="rdpLastExecutionTimeTo_SelectedDateChanged" >
                                    <DateInput ID="diLastExecutionTimeTo" runat="server" 
                                        DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                </telerik:RadDatePicker></td>
                            </tr>
                        </table>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="NextPlannedExecutionTime" HeaderText="Next Planned Execution" UniqueName="NextPlannedExecutionTime" SortExpression="NextPlannedExecutionTime" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="150px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# Eval("NextPlannedExecutionTime") != null ? ((DateTime)Eval("NextPlannedExecutionTime")).ToLocalizedDateTimeString() : "" %>
                    </ItemTemplate>
                    <FilterTemplate>
                        <table style="border: 0; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpNextPlannedExecutionTimeFrom" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpNextPlannedExecutionTimeFrom_PreRender" OnSelectedDateChanged="rdpNextPlannedExecutionTimeFrom_SelectedDateChanged">
                                        <DateInput ID="diNextPlannedExecutionTimeFrom" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpNextPlannedExecutionTimeTo" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpNextPlannedExecutionTimeTo_PreRender" OnSelectedDateChanged="rdpNextPlannedExecutionTimeTo_SelectedDateChanged" >
                                    <DateInput ID="diNextPlannedExecutionTimeTo" runat="server" 
                                        DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                </telerik:RadDatePicker></td>
                            </tr>
                        </table>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn UniqueName="History" ShowFilterIcon="false" InitializeTemplatesFirst="false" AllowFiltering="false" ItemStyle-CssClass="link" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <telerik:RadButton ID="btnHistory" runat="server" Text="History" OnClick="btnHistory_Click" />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
			</Columns>
		</MasterTableView>
	</telerik:RadGrid>
</asp:Panel>

<otpDS:SimpleDataSource ID="scheduledTasksLinqDS" runat="server" />