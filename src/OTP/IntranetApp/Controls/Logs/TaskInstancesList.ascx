﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaskInstancesList.ascx.cs" Inherits="IntranetApp.Controls.Logs.TaskInstancesList" %>
<%@ Import Namespace="Common.Extensions" %>
<%@ Import Namespace="Common.Helpers" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgTaskInstances" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<style type="text/css">
    .taMessage {
        display: block;
        width: 90%;
        height: 90%;
        margin: auto;
    }

    .btnShow {
        float: right;
    }
</style>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rwPreview" runat="server" VisibleOnPageLoad="false" Height="500px" Width="500px" Behaviors="Move, Resize, Close">
                <ContentTemplate>
                    <br />
                    <telerik:RadTextBox id="taMessage" runat="server" TextMode="MultiLine" Width="90%" Wrap="false" EnabledStyle-CssClass="taMessage" />
                    <br />
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="rwError" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblError" Font-Bold="true" runat="server" />
                        <br />
                        <br />
                        <asp:Button ID="wndBtnError_Ok" runat="server" Text="<%$ Resources: Controls, Dialog_Ok %>" OnClick="btnError_Ok" />
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgTaskInstances" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="taskInstancesLinqDS"
        OnItemDataBound="rgTaskInstances_ItemDataBound">
        <ClientSettings EnableRowHoverStyle="true" Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="false" />
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="Id" DataSourceID="taskInstancesLinqDS">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="TaskInstance.ExecutionTimestamp" SortOrder="Descending" />
            </SortExpressions>
			<Columns>
                <telerik:GridBoundColumn DataField="TaskInstance.Id" UniqueName="TaskInstanceId" Display="false" />
                <telerik:GridBoundColumn DataField="TaskInstance.ScheduledTask.ScheduledTaskId" UniqueName="TaskId" Display="false" />
                <telerik:GridBoundColumn DataField="TaskInstance.ScheduledTask.ScheduledTaskType" UniqueName="TaskType" Display="false" />
                     
                <telerik:GridBoundColumn DataField="Id" UniqueName="Id" HeaderText="Id" SortExpression="Id" HeaderStyle-Width="60px" FilterControlWidth="80%" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />

                <telerik:GridTemplateColumn DataField="TaskInstance.ExecutionTimestamp" HeaderText="Execution Timestamp" UniqueName="ExecutionTimestamp" SortExpression="TaskInstance.ExecutionTimestamp" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="150px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%#  ((DateTime)Eval("TaskInstance.ExecutionTimestamp")).ToLocalizedDateTimeString() %>
                    </ItemTemplate>
                    <FilterTemplate>
                        <table style="border: 0; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpExecutionTimestampFrom" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpExecutionTimestampFrom_PreRender" OnSelectedDateChanged="rdpExecutionTimestampFrom_SelectedDateChanged">
                                        <DateInput ID="diExecutionTimestampFrom" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpExecutionTimestampTo" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpExecutionTimestampTo_PreRender" OnSelectedDateChanged="rdpExecutionTimestampTo_SelectedDateChanged">
                                    <DateInput ID="diExecutionTimestampTo" runat="server" 
                                        DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                </telerik:RadDatePicker></td>
                            </tr>
                        </table>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="TaskInstance.ExecutionResult" HeaderText="Execution Result" UniqueName="ExecutionResult" SortExpression="TaskInstance.ExecutionResult" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="100px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# Eval("TaskInstance.ExecutionResult") != null ? DataAccess.Enums.EnumConverter.GetValue((DataAccess.Enums.TaskExecutionResult)Eval("TaskInstance.ExecutionResult")) : "" %>
                    </ItemTemplate>
                    <FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbExecutionResult" AutoPostBack="true" EnableViewState="true" OnInit="rcbExecutionResult_Init" OnSelectedIndexChanged="rcbExecutionResult_SelectedIndexChanged" OnPreRender="rcbExecutionResult_PreRender" MaxHeight="300px" Width="90%" DropDownWidth="150px" />
					</FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="Category" HeaderText="Category" UniqueName="Category" SortExpression="Category" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="100px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# Eval("Category") != null ? DataAccess.Enums.EnumConverter.GetValue((DataAccess.Enums.TaskDetailsMessageType)Eval("Category")) : "" %>
                    </ItemTemplate>
                    <FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbCategory" AutoPostBack="true" EnableViewState="true" OnInit="rcbCategory_Init" OnSelectedIndexChanged="rcbCategory_SelectedIndexChanged" OnPreRender="rcbCategory_PreRender" MaxHeight="300px" Width="90%" DropDownWidth="150px" />
					</FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="Message" HeaderText="Message" UniqueName="Message" SortExpression="Message" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" AutoPostBackOnFilter="true" FilterControlWidth="70%" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# HttpUtility.HtmlEncode(((string)Eval("Message")).Limit(_previewLength)) %>
                        <telerik:RadButton ID="btnshowMessage" runat="server" Text="..." OnClick="btnshowMessage_Click" Visible="false" CssClass="btnShow" />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
			</Columns>
		</MasterTableView>
	</telerik:RadGrid>
</asp:Panel>

<otpDS:SimpleDataSource ID="taskInstancesLinqDS" runat="server" />