﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CrmOperationLogList.ascx.cs" Inherits="IntranetApp.Controls.Logs.CrmOperationLogList" %>
<%@ Import Namespace="Common.Extensions" %>
<%@ Import Namespace="Common.Helpers" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgPaymentGatewayOperationLogs" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<style type="text/css">
    .taMessage {
        display: block;
        width: 90%;
        height: 90%;
        margin: auto;
    }

    .btnShow {
        float: right;
    }
</style>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rwPreview" runat="server" VisibleOnPageLoad="false" Height="500px" Width="500px" Behaviors="Move, Resize, Close">
                <ContentTemplate>
                    <br />
                    <telerik:RadTextBox id="taMessage" runat="server" TextMode="MultiLine" Width="90%" Wrap="false" EnabledStyle-CssClass="taMessage" />
                    <br />
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgCrmOperationLogs" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="crmOperationLogsLinqDS"
        OnItemCommand="rgCrmOperationLogs_ItemCommand" 
        OnItemDataBound="rgCrmOperationLogs_ItemDataBound"
        OnDataBound="rgCrmOperationLogs_DataBound">
        <ClientSettings EnableRowHoverStyle="true" Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="false" />
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="Id" DataSourceID="crmOperationLogsLinqDS">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="RowDate" SortOrder="Descending" />
            </SortExpressions>
			<Columns>           
                <telerik:GridTemplateColumn DataField="OperationType" HeaderText="Operation Type" UniqueName="OperationType" SortExpression="OperationType" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="100px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# DataAccess.Enums.EnumConverter.GetValue((DataAccess.Enums.CrmOparationLogsType)Eval("OperationType")) %>
                    </ItemTemplate>
                    <FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbOperationType" AutoPostBack="true" EnableViewState="true" OnInit="rcbOperationType_Init" OnSelectedIndexChanged="rcbOperationType_SelectedIndexChanged" OnPreRender="rcbOperationType_PreRender" MaxHeight="300px" Width="90%" DropDownWidth="150px" />
					</FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="RowDate" HeaderText="Date" UniqueName="RowDate" SortExpression="RowDate" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="150px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# ((DateTime)Eval("RowDate")).ToLocalizedDateTimeString() %>
                    </ItemTemplate>
                    <FilterTemplate>
                        <table style="border: 0; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label ID="Label3" runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpRowDateFrom" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpRowDateFrom_PreRender" OnSelectedDateChanged="rdpRowDateFrom_SelectedDateChanged">
                                        <DateInput ID="diRowDateFrom" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label ID="Label4" runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpRowDateTo" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpRowDateTo_PreRender" OnSelectedDateChanged="rdpRowDateTo_SelectedDateChanged" >
                                    <DateInput ID="diRowDateTo" runat="server" 
                                        DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                </telerik:RadDatePicker></td>
                            </tr>
                        </table>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="Message" HeaderText="Message" UniqueName="Message" SortExpression="Message" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" AutoPostBackOnFilter="true" FilterControlWidth="70%" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# HttpUtility.HtmlEncode(((string)Eval("Message")).Limit(_previewLength)) %>
                        <telerik:RadButton ID="btnshowMessage" runat="server" Text="..." OnClick="btnshowMessage_Click" Visible="false" CssClass="btnShow" />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="CrmRequest" HeaderText="CRM Request" UniqueName="CrmRequest" SortExpression="CrmRequest" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" AutoPostBackOnFilter="true" FilterControlWidth="70%" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# HttpUtility.HtmlEncode(((string)Eval("CrmRequest")).Limit(_previewLength)) %>
                        <telerik:RadButton ID="btnShowRequest" runat="server" Text="..." OnClick="btnShowRequest_Click" Visible="false" CssClass="btnShow" />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                
                <telerik:GridTemplateColumn DataField="CrmResponse" HeaderText="CRM Response" UniqueName="CrmResponse" SortExpression="CrmResponse" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" AutoPostBackOnFilter="true" FilterControlWidth="70%" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# HttpUtility.HtmlEncode(((string)Eval("CrmResponse")).Limit(_previewLength)) %>
                        <telerik:RadButton ID="btnShowResponse" runat="server" Text="..." OnClick="btnShowResponse_Click" Visible="false" CssClass="btnShow" />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="UserFullName" HeaderText="User" UniqueName="UserFullName" SortExpression="UserFullName" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" AutoPostBackOnFilter="true" HeaderStyle-Width="150px" FilterControlWidth="70%" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# ((string)Eval("UserFullName")).Limit(_nameLength) %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
			</Columns>
		</MasterTableView>
	</telerik:RadGrid>
</asp:Panel>

<otpDS:SimpleDataSource ID="crmOperationLogsLinqDS" runat="server" />