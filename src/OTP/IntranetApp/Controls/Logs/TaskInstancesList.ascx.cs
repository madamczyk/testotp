﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Logs
{
    public partial class TaskInstancesList : System.Web.UI.UserControl
    {

        #region Fields

        public DateTime? ExecutionTimestampFrom
        {
            get
            {
                if (ViewState["ExecutionTimestampFrom"] != null)
                    return (DateTime)ViewState["ExecutionTimestampFrom"];

                return null;
            }
            set
            {
                ViewState["ExecutionTimestampFrom"] = value;
            }
        }

        public DateTime? ExecutionTimestampTo
        {
            get
            {
                if (ViewState["ExecutionTimestampTo"] != null)
                    return (DateTime)ViewState["ExecutionTimestampTo"];

                return null;
            }
            set
            {
                ViewState["ExecutionTimestampTo"] = value;
            }
        }

        public int? ExecutionResultFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["ExecutionResultFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["ExecutionResultFilterSelectedValue"] as int?;
            }
            set { ViewState["ExecutionResultFilterSelectedValue"] = value; }
        }

        public int? CategoryFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["CategoryFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["CategoryFilterSelectedValue"] as int?;
            }
            set { ViewState["CategoryFilterSelectedValue"] = value; }
        }

        public readonly int _previewLength = 100;

        public int? ScheduledTaskId
        {
            get { return ViewState["ScheduledTaskId"] as int?; }
            set { ViewState["ScheduledTaskId"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            taskInstancesLinqDS.DataResolver = () =>
            {
                IQueryable<TaskInstanceDetail> result;

                if (ScheduledTaskId.HasValue)
                    result = RequestManager.Services.TaskSchedulerEventLogService.GetTaskInstanceDetailsByTaskId(ScheduledTaskId.Value).AsQueryable();
                else
                    result = RequestManager.Services.TaskSchedulerEventLogService.GetTaskInstanceDetails().AsQueryable();

                rgTaskInstances.ApplyFilter<TaskInstanceDetail>(ref result, "Id",
                    (q, f) => q.Where(tid => tid.IdString.Contains(f.ToLower())));
                rgTaskInstances.ApplyFilter<TaskInstanceDetail>(ref result, "Message",
                    (q, f) => q.Where(tid => !String.IsNullOrEmpty(tid.Message) && tid.Message.ToLower().Contains(f.ToLower())));

                if (ExecutionTimestampFrom.HasValue)
                    result = result.Where(tid => tid.TaskInstance.ExecutionTimestamp >= this.ExecutionTimestampFrom.Value);
                if (ExecutionTimestampTo.HasValue)
                    result = result.Where(tid => tid.TaskInstance.ExecutionTimestamp <= this.ExecutionTimestampTo.Value);

                if (this.ExecutionResultFilterSelectedValue.HasValue)
                    result = result.Where(tid => tid.TaskInstance.ExecutionResult.HasValue && tid.TaskInstance.ExecutionResult.Value == this.ExecutionResultFilterSelectedValue.Value);

                if (this.CategoryFilterSelectedValue.HasValue)
                    result = result.Where(tid => tid.Category.HasValue && tid.Category.Value == this.CategoryFilterSelectedValue.Value);

                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Check if task with selected id exists
            if (ScheduledTaskId.HasValue && RequestManager.Services.ScheduledTasksService.GetScheduledTaskById(ScheduledTaskId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"),
                    (string)GetGlobalResourceObject("Controls", "Object_ScheduledTask"));

                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"),
                    (string)GetGlobalResourceObject("Controls", "Object_ScheduledTask"));

                rwError.VisibleOnPageLoad = true;
            }
            #endregion

            #region Grouping
            GridGroupByExpression expression;

            // group by ScheduledTaskId, if user didn't provide one
            if (!ScheduledTaskId.HasValue)
            {
                expression = new GridGroupByExpression();

                expression.SelectFields.Add(new GridGroupByField()
                {
                    FieldName = "TaskInstance.ScheduledTask.ScheduledTaskType",
                    HeaderText = "Scheduled Task",
                    FormatString = "#{0}"
                });
                expression.GroupByFields.Add(new GridGroupByField()
                {
                    FieldName = "TaskInstance.ScheduledTask.ScheduledTaskId",
                    SortOrder = GridSortOrder.Ascending
                });

                rgTaskInstances.MasterTableView.GroupByExpressions.Add(expression);
            }

            // always group by TaskInstanceId
            expression = new GridGroupByExpression();
            expression.SelectFields.Add(new GridGroupByField()
            {
                FieldName = "TaskInstance.Id",
                HeaderText = "Task Instance",
                FormatString = "#{0}"
            });
            expression.GroupByFields.Add(new GridGroupByField()
            {
                FieldName = "TaskInstance.Id",
                SortOrder = GridSortOrder.Ascending
            });

            rgTaskInstances.MasterTableView.GroupByExpressions.Add(expression);
            #endregion

            BindGrid();

            if (ScheduledTaskId.HasValue)
            {
                ScheduledTask task = RequestManager.Services.ScheduledTasksService.GetScheduledTaskById(ScheduledTaskId.Value);
                lblFormTitle.Text = String.Format((string)GetLocalResourceObject("lblFormTitle_Text_WithTask"), EnumConverter.GetValue((ScheduledTaskType)task.ScheduledTaskType));
            }
        }

        protected void rgTaskInstances_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                TaskInstanceDetail obj = (TaskInstanceDetail)item.DataItem;
                RadButton btnshowMessage = item["Message"].FindControl("btnshowMessage") as RadButton;

                btnshowMessage.Attributes.Add("id", obj.Id.ToString());
                btnshowMessage.Visible = !String.IsNullOrEmpty(obj.Message) && obj.Message.Length > _previewLength;
            }
            else if (e.Item is GridGroupHeaderItem)
            {
                GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;
                Match match = Regex.Match(item.DataCell.Text, "Scheduled Task: #([0-9]*)");

                if (match.Success)
                {
                    ScheduledTaskType taskType = (ScheduledTaskType)int.Parse(match.Groups[1].Value);

                    item.DataCell.Text = item.DataCell.Text.Replace("#" + match.Groups[1].Value, EnumConverter.GetValue(taskType));
                }
            }
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Logs/TaskInstancesList.aspx");
        }

        protected void btnshowMessage_Click(object sender, EventArgs e)
        {
            RadButton btn = sender as RadButton;

            if (btn == null || String.IsNullOrEmpty(btn.Attributes["id"]))
                return;

            int logId = int.Parse(btn.Attributes["id"]);

            rwPreview.Title = String.Format((string)GetLocalResourceObject("Dialog_Title_Message"), logId);
            taMessage.Text = RequestManager.Services.TaskSchedulerEventLogService.GetTaskInstanceDetailById(logId).Message;

            RadScriptManager.RegisterStartupScript(Page, Page.GetType(),
                "showRwPreviewFunction",
                "function showRwPreview() { $find(\"" + rwPreview.ClientID + "\").show(); Sys.Application.remove_load(showRwPreview); } Sys.Application.add_load(showRwPreview);",
                true);
        }
        
        protected void rdpExecutionTimestampFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.ExecutionTimestampFrom;
        }

        protected void rdpExecutionTimestampTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.ExecutionTimestampTo;
        }

        protected void rcbExecutionResult_PreRender(object sender, EventArgs e)
        {
            if (ExecutionResultFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = ExecutionResultFilterSelectedValue.Value.ToString();
            }
        }

        protected void rcbCategory_PreRender(object sender, EventArgs e)
        {
            if (CategoryFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = CategoryFilterSelectedValue.Value.ToString();
            }
        }

        protected void rcbExecutionResult_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (TaskExecutionResult ter in Enum.GetValues(typeof(TaskExecutionResult)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(ter), ((int)ter).ToString()));
            }
        }

        protected void rcbCategory_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (TaskDetailsMessageType tdmt in Enum.GetValues(typeof(TaskDetailsMessageType)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(tdmt), ((int)tdmt).ToString()));
            }
        }

        protected void rdpExecutionTimestampFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            ExecutionTimestampFrom = e.NewDate;
            rgTaskInstances.MasterTableView.Rebind();
        }

        protected void rdpExecutionTimestampTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            ExecutionTimestampTo = e.NewDate;
            rgTaskInstances.MasterTableView.Rebind();
        }

        protected void rcbExecutionResult_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ExecutionResultFilterSelectedValue = int.Parse(e.Value);
            rgTaskInstances.MasterTableView.Rebind();
        }

        protected void rcbCategory_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CategoryFilterSelectedValue = int.Parse(e.Value);
            rgTaskInstances.MasterTableView.Rebind();
        }

        #endregion Event Handlers

    }
}