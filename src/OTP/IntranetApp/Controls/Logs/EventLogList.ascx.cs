﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Linq;
using System.Web;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Logs
{
    public partial class EventLogList : System.Web.UI.UserControl
    {

        #region Fields

        public DateTime? TimestampFrom
        {
            get
            {
                if (ViewState["TimestampFrom"] != null)
                    return (DateTime)ViewState["TimestampFrom"];

                return null;
            }
            set
            {
                ViewState["TimestampFrom"] = value;
            }
        }

        public DateTime? TimestampTo
        {
            get
            {
                if (ViewState["TimestampTo"] != null)
                    return (DateTime)ViewState["TimestampTo"];

                return null;
            }
            set
            {
                ViewState["TimestampTo"] = value;
            }
        }

        public int? CategoryFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["CategoryFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["CategoryFilterSelectedValue"] as int?;
            }
            set { ViewState["CategoryFilterSelectedValue"] = value; }
        }

        public int? SourceFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["SourceFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["SourceFilterSelectedValue"] as int?;
            }
            set { ViewState["SourceFilterSelectedValue"] = value; }
        }

        public readonly int _previewLength = 80;

        #endregion

        #region Methods

        private void BindGrid()
        {
            eventLogsLinqDS.DataResolver = () =>
            {
                IQueryable<EventLog> result = RequestManager.Services.EventLogService.GetEventLogs().AsQueryable();

                rgEventLogs.ApplyFilter<EventLog>(ref result, "Id",
                    (q, f) => q.Where(el => el.IdString.Contains(f.ToLower())));
                rgEventLogs.ApplyFilter<EventLog>(ref result, "CorrelationId",
                    (q, f) => q.Where(el => el.CorrelationIdString.ToLower().Contains(f.ToLower())));
                rgEventLogs.ApplyFilter<EventLog>(ref result, "Message",
                    (q, f) => q.Where(el => el.Message.ToLower().Contains(f.ToLower())));

                if (TimestampFrom.HasValue)
                    result = result.Where(el => el.Timestamp >= this.TimestampFrom.Value);
                if (TimestampTo.HasValue)
                    result = result.Where(el => el.Timestamp <= this.TimestampTo.Value);

                if (this.CategoryFilterSelectedValue.HasValue)
                {
                    var category = ((EventCategory)this.CategoryFilterSelectedValue.Value).ToString();
                    result = result.Where(el => el.Category == category);
                }

                if (this.SourceFilterSelectedValue.HasValue)
                {
                    var source = ((EventLogSource)this.SourceFilterSelectedValue.Value).ToString();
                    result = result.Where(el => el.Source == source);
                }

                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgEventLogs_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int logId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());

                rwPreview.Title = String.Format((string)GetLocalResourceObject("Dialog_Title_Message"), logId);
                taMessage.Text = RequestManager.Services.EventLogService.GetEventLogById(logId).Message;

                RadScriptManager.RegisterStartupScript(Page, Page.GetType(), 
                    "showRwPreviewFunction",
                    "function showRwPreview() { $find(\"" + rwPreview.ClientID + "\").show(); Sys.Application.remove_load(showRwPreview); } Sys.Application.add_load(showRwPreview);", 
                    true);  
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgEventLogs_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgEventLogs.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgEventLogs.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgEventLogs.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgEventLogs.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgEventLogs.MasterTableView.Rebind();
            }
        }

        protected void rdpTimestampFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.TimestampFrom;
        }

        protected void rdpTimestampTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.TimestampTo;
        }

        protected void rcbCategory_PreRender(object sender, EventArgs e)
        {
            if (CategoryFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = CategoryFilterSelectedValue.Value.ToString();
            }
        }

        protected void rcbSource_PreRender(object sender, EventArgs e)
        {
            if (SourceFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = SourceFilterSelectedValue.Value.ToString();
            }
        }

        protected void rcbCategory_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (EventCategory ec in Enum.GetValues(typeof(EventCategory)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(ec), ((int)ec).ToString()));
            }
        }

        protected void rcbSource_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (EventLogSource els in Enum.GetValues(typeof(EventLogSource)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(els), ((int)els).ToString()));
            }
        }

        protected void rdpTimestampFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            TimestampFrom = e.NewDate;
            rgEventLogs.MasterTableView.Rebind();
        }

        protected void rdpTimestampTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            TimestampTo = e.NewDate;
            rgEventLogs.MasterTableView.Rebind();
        }

        protected void rcbCategory_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CategoryFilterSelectedValue = int.Parse(e.Value);
            rgEventLogs.MasterTableView.Rebind();
        }

        protected void rcbSource_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            SourceFilterSelectedValue = int.Parse(e.Value);
            rgEventLogs.MasterTableView.Rebind();
        }

        #endregion Event Handlers

    }
}