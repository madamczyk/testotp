﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventLogList.ascx.cs" Inherits="IntranetApp.Controls.Logs.EventLogList" %>
<%@ Import Namespace="Common.Extensions" %>
<%@ Import Namespace="Common.Helpers" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgEventLogs" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<style type="text/css">
    .taMessage {
        display: block;
        width: 90%;
        height: 90%;
        margin: auto;
    }
</style>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rwPreview" runat="server" VisibleOnPageLoad="false" Height="500px" Width="500px" Behaviors="Move, Resize, Close">
                <ContentTemplate>
                    <br />
                    <telerik:RadTextBox id="taMessage" runat="server" TextMode="MultiLine" Width="90%" Wrap="false" EnabledStyle-CssClass="taMessage" />
                    <br />
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgEventLogs" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="eventLogsLinqDS"
        OnItemCommand="rgEventLogs_ItemCommand" 
        OnDataBound="rgEventLogs_DataBound">
        <ClientSettings EnableRowHoverStyle="true" Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="true" />
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="Id" DataSourceID="eventLogsLinqDS">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="Timestamp" SortOrder="Descending" />
            </SortExpressions>
			<Columns>           
				<telerik:GridBoundColumn DataField="Id" UniqueName="Id" HeaderText="Id" SortExpression="Id" HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />	

				<telerik:GridBoundColumn DataField="CorrelationId" UniqueName="CorrelationId" HeaderText="Correlation Id" SortExpression="CorrelationId" HeaderStyle-Width="250px" FilterControlWidth="150px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />

                <telerik:GridTemplateColumn DataField="Timestamp" HeaderText="Timestamp" UniqueName="Timestamp" SortExpression="Timestamp" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="150px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# ((DateTime)Eval("Timestamp")).ToLocalizedDateTimeString() %>
                    </ItemTemplate>
                    <FilterTemplate>
                        <table style="border: 0; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpTimestampFrom" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpTimestampFrom_PreRender" OnSelectedDateChanged="rdpTimestampFrom_SelectedDateChanged">
                                        <DateInput ID="diTimestampFrom" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpTimestampTo" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpTimestampTo_PreRender" OnSelectedDateChanged="rdpTimestampTo_SelectedDateChanged" >
                                    <DateInput ID="diTimestampTo" runat="server" 
                                        DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                </telerik:RadDatePicker></td>
                            </tr>
                        </table>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>
                      
				<telerik:GridBoundColumn DataField="Category" UniqueName="Category" HeaderText="Category" SortExpression="Category" HeaderStyle-Width="100px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link">
                    <FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbCategory" AutoPostBack="true" EnableViewState="true" OnInit="rcbCategory_Init" OnSelectedIndexChanged="rcbCategory_SelectedIndexChanged" OnPreRender="rcbCategory_PreRender" MaxHeight="300px" Width="90%" DropDownWidth="150px" />
					</FilterTemplate>
                </telerik:GridBoundColumn>	                

				<telerik:GridBoundColumn DataField="Source" UniqueName="Source" HeaderText="Source" SortExpression="Source" HeaderStyle-Width="120px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link">
                    <FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbSource" AutoPostBack="true" EnableViewState="true" OnInit="rcbSource_Init" OnSelectedIndexChanged="rcbSource_SelectedIndexChanged" OnPreRender="rcbSource_PreRender" MaxHeight="300px" Width="90%" DropDownWidth="150px" />
					</FilterTemplate>
                </telerik:GridBoundColumn>	

                <telerik:GridTemplateColumn DataField="Message" HeaderText="Message" UniqueName="Message" SortExpression="Message" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" AutoPostBackOnFilter="true" FilterControlWidth="70%" ItemStyle-CssClass="link">
                        <ItemTemplate>
                            <%# HttpUtility.HtmlEncode(((string)Eval("Message")).Limit(_previewLength)) %>
                        </ItemTemplate>
                </telerik:GridTemplateColumn>
			</Columns>
		</MasterTableView>
	</telerik:RadGrid>
</asp:Panel>

<otpDS:SimpleDataSource ID="eventLogsLinqDS" runat="server" />