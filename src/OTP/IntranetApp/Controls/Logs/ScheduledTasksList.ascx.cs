﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Enums;
using System;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Logs
{
    public partial class ScheduledTasksList : System.Web.UI.UserControl
    {

        #region Fields

        public DateTime? LastExecutionTimeFrom
        {
            get
            {
                if (ViewState["LastExecutionTimeFrom"] != null)
                    return (DateTime)ViewState["LastExecutionTimeFrom"];

                return null;
            }
            set
            {
                ViewState["LastExecutionTimeFrom"] = value;
            }
        }

        public DateTime? LastExecutionTimeTo
        {
            get
            {
                if (ViewState["LastExecutionTimeTo"] != null)
                    return (DateTime)ViewState["LastExecutionTimeTo"];

                return null;
            }
            set
            {
                ViewState["LastExecutionTimeTo"] = value;
            }
        }

        public DateTime? NextPlannedExecutionTimeFrom
        {
            get
            {
                if (ViewState["NextPlannedExecutionTimeFrom"] != null)
                    return (DateTime)ViewState["NextPlannedExecutionTimeFrom"];

                return null;
            }
            set
            {
                ViewState["NextPlannedExecutionTimeFrom"] = value;
            }
        }

        public DateTime? NextPlannedExecutionTimeTo
        {
            get
            {
                if (ViewState["NextPlannedExecutionTimeTo"] != null)
                    return (DateTime)ViewState["NextPlannedExecutionTimeTo"];

                return null;
            }
            set
            {
                ViewState["NextPlannedExecutionTimeTo"] = value;
            }
        }

        public int? ScheduledTaskTypeFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["ScheduledTaskTypeFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["ScheduledTaskTypeFilterSelectedValue"] as int?;
            }
            set { ViewState["ScheduledTaskTypeFilterSelectedValue"] = value; }
        }

        public int? ScheduleTypeFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["ScheduleTypeFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["ScheduleTypeFilterSelectedValue"] as int?;
            }
            set { ViewState["ScheduleTypeFilterSelectedValue"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            scheduledTasksLinqDS.DataResolver = () =>
            {
                IQueryable<ScheduledTask> result = RequestManager.Services.ScheduledTasksService.GetScheduledTasks().AsQueryable();

                rgScheduledTasks.ApplyFilter<ScheduledTask>(ref result, "ScheduleDetails",
                    (q, f) => q.Where(st => st.ScheduleDetails.ToLower().Contains(f.ToLower())));

                if (LastExecutionTimeFrom.HasValue)
                    result = result.Where(st => st.LastExecutionTime.HasValue && st.LastExecutionTime >= this.LastExecutionTimeFrom.Value);
                if (LastExecutionTimeTo.HasValue)
                    result = result.Where(st => st.LastExecutionTime.HasValue && st.LastExecutionTime <= this.LastExecutionTimeTo.Value);

                if (NextPlannedExecutionTimeFrom.HasValue)
                    result = result.Where(st => st.NextPlannedExecutionTime.HasValue && st.NextPlannedExecutionTime >= this.NextPlannedExecutionTimeFrom.Value);
                if (NextPlannedExecutionTimeTo.HasValue)
                    result = result.Where(st => st.NextPlannedExecutionTime.HasValue && st.NextPlannedExecutionTime <= this.NextPlannedExecutionTimeTo.Value);

                if (this.ScheduledTaskTypeFilterSelectedValue.HasValue)
                    result = result.Where(st => st.ScheduledTaskType == this.ScheduledTaskTypeFilterSelectedValue.Value);

                if (this.ScheduleTypeFilterSelectedValue.HasValue)
                    result = result.Where(st => st.ScheduleType == this.ScheduleTypeFilterSelectedValue.Value);

                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgScheduledTasks_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgScheduledTasks_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                ScheduledTask obj = (ScheduledTask)item.DataItem;
                RadButton btnHistory = item["History"].FindControl("btnHistory") as RadButton;

                btnHistory.Attributes.Add("id", obj.ScheduledTaskId.ToString());
            }
        }

        protected void rgScheduledTasks_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgScheduledTasks.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgScheduledTasks.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgScheduledTasks.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgScheduledTasks.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgScheduledTasks.MasterTableView.Rebind();
            }
        }

        protected void btnHistory_Click(object sender, EventArgs e)
        {
            RadButton btn = sender as RadButton;

            if (btn == null || String.IsNullOrEmpty(btn.Attributes["id"]))
                return;

            int logId = int.Parse(btn.Attributes["id"]);
            StringBuilder sb = new StringBuilder();

            sb.Append("~/Forms/Logs/TaskInstancesList.aspx?id=");
            sb.Append(logId);

            Response.Redirect(sb.ToString());
        }

        protected void rdpLastExecutionTimeFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.LastExecutionTimeFrom;
        }

        protected void rdpLastExecutionTimeTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.LastExecutionTimeTo;
        }

        protected void rdpNextPlannedExecutionTimeFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.NextPlannedExecutionTimeFrom;
        }

        protected void rdpNextPlannedExecutionTimeTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.NextPlannedExecutionTimeTo;
        }

        protected void rcbScheduledTaskType_PreRender(object sender, EventArgs e)
        {
            if (ScheduledTaskTypeFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = ScheduledTaskTypeFilterSelectedValue.Value.ToString();
            }
        }

        protected void rcbScheduleType_PreRender(object sender, EventArgs e)
        {
            if (ScheduleTypeFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = ScheduleTypeFilterSelectedValue.Value.ToString();
            }
        }

        protected void rcbScheduledTaskType_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (ScheduledTaskType stt in Enum.GetValues(typeof(ScheduledTaskType)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(stt), ((int)stt).ToString()));
            }
        }

        protected void rcbScheduleType_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (ScheduleType st in Enum.GetValues(typeof(ScheduleType)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(st), ((int)st).ToString()));
            }
        }

        protected void rdpLastExecutionTimeFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            LastExecutionTimeFrom = e.NewDate;
            rgScheduledTasks.MasterTableView.Rebind();
        }

        protected void rdpLastExecutionTimeTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            LastExecutionTimeTo = e.NewDate;
            rgScheduledTasks.MasterTableView.Rebind();
        }

        protected void rdpNextPlannedExecutionTimeFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            NextPlannedExecutionTimeFrom = e.NewDate;
            rgScheduledTasks.MasterTableView.Rebind();
        }

        protected void rdpNextPlannedExecutionTimeTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            NextPlannedExecutionTimeTo = e.NewDate;
            rgScheduledTasks.MasterTableView.Rebind();
        }

        protected void rcbScheduledTaskType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ScheduledTaskTypeFilterSelectedValue = int.Parse(e.Value);
            rgScheduledTasks.MasterTableView.Rebind();
        }

        protected void rcbScheduleType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ScheduleTypeFilterSelectedValue = int.Parse(e.Value);
            rgScheduledTasks.MasterTableView.Rebind();
        }

        #endregion Event Handlers

    }
}