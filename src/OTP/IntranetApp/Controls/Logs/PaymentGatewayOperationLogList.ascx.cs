﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Helpers;
using System;
using System.Linq;
using System.Web;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Logs
{
    public partial class PaymentGatewayOperationLogList : System.Web.UI.UserControl
    {

        #region Fields

        public DateTime? BookingDateFrom
        {
            get
            {
                if (ViewState["BookingDateFrom"] != null)
                    return (DateTime)ViewState["BookingDateFrom"];

                return null;
            }
            set
            {
                ViewState["BookingDateFrom"] = value;
            }
        }

        public DateTime? BookingDateTo
        {
            get
            {
                if (ViewState["BookingDateTo"] != null)
                    return (DateTime)ViewState["BookingDateTo"];

                return null;
            }
            set
            {
                ViewState["BookingDateTo"] = value;
            }
        }

        public DateTime? RowDateFrom
        {
            get
            {
                if (ViewState["RowDateFrom"] != null)
                    return (DateTime)ViewState["RowDateFrom"];

                return null;
            }
            set
            {
                ViewState["RowDateFrom"] = value;
            }
        }

        public DateTime? RowDateTo
        {
            get
            {
                if (ViewState["RowDateTo"] != null)
                    return (DateTime)ViewState["RowDateTo"];

                return null;
            }
            set
            {
                ViewState["RowDateTo"] = value;
            }
        }

        public int? BookingStatusFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["BookingStatusFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["BookingStatusFilterSelectedValue"] as int?;
            }
            set { ViewState["BookingStatusFilterSelectedValue"] = value; }
        }

        public int? OperationTypeFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["OperationTypeFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["OperationTypeFilterSelectedValue"] as int?;
            }
            set { ViewState["OperationTypeFilterSelectedValue"] = value; }
        }

        public readonly int _previewLength = 35;

        #endregion

        #region Methods

        private void BindGrid()
        {
            paymentGatewayOperationLogsLinqDS.DataResolver = () =>
            {
                IQueryable<PaymentGatewayOperationsLog> result = RequestManager.Services.PaymentGatewayOperationLogService.GetPaymentGatewayOperationsLogs().AsQueryable();

                rgPaymentGatewayOperationLogs.ApplyFilter<PaymentGatewayOperationsLog>(ref result, "ReservationReservationId",
                    (q, f) => q.Where(pgol => pgol.Reservation != null && pgol.Reservation.ReservationIDString.Contains(f.ToLower())));
                rgPaymentGatewayOperationLogs.ApplyFilter<PaymentGatewayOperationsLog>(ref result, "ReservationConfirmationID",
                    (q, f) => q.Where(pgol => pgol.Reservation != null && pgol.Reservation.ConfirmationID.ToLower().Contains(f.ToLower())));
                rgPaymentGatewayOperationLogs.ApplyFilter<PaymentGatewayOperationsLog>(ref result, "Message",
                    (q, f) => q.Where(pgol => !String.IsNullOrEmpty(pgol.Message) && pgol.Message.ToLower().Contains(f.ToLower())));
                rgPaymentGatewayOperationLogs.ApplyFilter<PaymentGatewayOperationsLog>(ref result, "GatewayResponse",
                    (q, f) => q.Where(pgol => !String.IsNullOrEmpty(pgol.GatewayResponse) && pgol.GatewayResponse.ToLower().Contains(f.ToLower())));

                if (BookingDateFrom.HasValue)
                    result = result.Where(pgol => pgol.Reservation != null && pgol.Reservation.BookingDate >= this.BookingDateFrom.Value);
                if (BookingDateTo.HasValue)
                    result = result.Where(pgol => pgol.Reservation != null && pgol.Reservation.BookingDate <= this.BookingDateTo.Value);

                if (this.BookingStatusFilterSelectedValue.HasValue)
                    result = result.Where(pgol => pgol.Reservation != null && pgol.Reservation.BookingStatus == this.BookingStatusFilterSelectedValue.Value);

                if (this.OperationTypeFilterSelectedValue.HasValue)
                    result = result.Where(pgol => pgol.OperationType == this.OperationTypeFilterSelectedValue.Value);

                if (RowDateFrom.HasValue)
                    result = result.Where(pgol => pgol.RowDate >= this.RowDateFrom.Value);
                if (RowDateTo.HasValue)
                    result = result.Where(pgol => pgol.RowDate <= this.RowDateTo.Value);

                return result.ToList();
            };
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgPaymentGatewayOperationLogs_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            { 
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgPaymentGatewayOperationLogs_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                PaymentGatewayOperationsLog obj = (PaymentGatewayOperationsLog)item.DataItem;
                RadButton btnshowMessage = item["Message"].FindControl("btnshowMessage") as RadButton;
                RadButton btnShowResponse = item["GatewayResponse"].FindControl("btnShowResponse") as RadButton;

                btnshowMessage.Attributes.Add("id", obj.Id.ToString());
                btnShowResponse.Attributes.Add("id", obj.Id.ToString());

                btnshowMessage.Visible = !String.IsNullOrEmpty(obj.Message) && obj.Message.Length > _previewLength;
                btnShowResponse.Visible = !String.IsNullOrEmpty(obj.GatewayResponse) && obj.GatewayResponse.Length > _previewLength;
            }
        }

        protected void rgPaymentGatewayOperationLogs_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgPaymentGatewayOperationLogs.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgPaymentGatewayOperationLogs.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgPaymentGatewayOperationLogs.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgPaymentGatewayOperationLogs.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgPaymentGatewayOperationLogs.MasterTableView.Rebind();
            }
        }

        protected void btnshowMessage_Click(object sender, EventArgs e)
        {
            RadButton btn = sender as RadButton;
            if (btn == null || String.IsNullOrEmpty(btn.Attributes["id"]))
                return;

            int logId = int.Parse(btn.Attributes["id"]);

            rwPreview.Title = String.Format((string)GetLocalResourceObject("Dialog_Title_Message"), logId);
            taMessage.Text = RequestManager.Services.PaymentGatewayOperationLogService.GetPaymentGatewayOperationsLogById(logId).Message;

            RadScriptManager.RegisterStartupScript(Page, Page.GetType(),
                "showRwPreviewFunction",
                "function showRwPreview() { $find(\"" + rwPreview.ClientID + "\").show(); Sys.Application.remove_load(showRwPreview); } Sys.Application.add_load(showRwPreview);",
                true);
        }

        protected void btnShowResponse_Click(object sender, EventArgs e)
        {
            RadButton btn = sender as RadButton;
            if (btn == null || String.IsNullOrEmpty(btn.Attributes["id"]))
                return;

            int logId = int.Parse(btn.Attributes["id"]);

            rwPreview.Title = String.Format((string)GetLocalResourceObject("Dialog_Title_Response"), logId);
            string response = RequestManager.Services.PaymentGatewayOperationLogService.GetPaymentGatewayOperationsLogById(logId).GatewayResponse;
            taMessage.Text = XmlDisplayHelper.DisplayIndentedXML(response);

            RadScriptManager.RegisterStartupScript(Page, Page.GetType(),
                "showRwPreviewFunction",
                "function showRwPreview() { $find(\"" + rwPreview.ClientID + "\").show(); Sys.Application.remove_load(showRwPreview); } Sys.Application.add_load(showRwPreview);",
                true);
        }

        protected void rcbBookingStatus_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (BookingStatus bs in Enum.GetValues(typeof(BookingStatus)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(bs), ((int)bs).ToString()));
            }
        }

        protected void rcbOperationType_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (PaymentGatewayOperationType ot in Enum.GetValues(typeof(PaymentGatewayOperationType)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(ot), ((int)ot).ToString()));
            }
        }

        protected void rcbBookingStatus_PreRender(object sender, EventArgs e)
        {
            if (BookingStatusFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = BookingStatusFilterSelectedValue.ToString();
            }
        }

        protected void rcbOperationType_PreRender(object sender, EventArgs e)
        {
            if (OperationTypeFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = OperationTypeFilterSelectedValue.ToString();
            }
        }

        protected void rdpBookingDateFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.BookingDateFrom;
        }

        protected void rdpBookingDateTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.BookingDateTo;
        }

        protected void rdpRowDateFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.RowDateFrom;
        }

        protected void rdpRowDateTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.RowDateTo;
        }

        protected void rcbBookingStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BookingStatusFilterSelectedValue = int.Parse(e.Value);
            rgPaymentGatewayOperationLogs.MasterTableView.Rebind();
        }

        protected void rcbOperationType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            OperationTypeFilterSelectedValue = int.Parse(e.Value);
            rgPaymentGatewayOperationLogs.MasterTableView.Rebind();
        }

        protected void rdpBookingDateFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            BookingDateFrom = e.NewDate; 
            rgPaymentGatewayOperationLogs.MasterTableView.Rebind();
        }

        protected void rdpBookingDateTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            BookingDateTo = e.NewDate;
            rgPaymentGatewayOperationLogs.MasterTableView.Rebind();
        }

        protected void rdpRowDateFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            RowDateFrom = e.NewDate;
            rgPaymentGatewayOperationLogs.MasterTableView.Rebind();
        }

        protected void rdpRowDateTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            RowDateTo = e.NewDate;
            rgPaymentGatewayOperationLogs.MasterTableView.Rebind();
        }

        #endregion

    }
}