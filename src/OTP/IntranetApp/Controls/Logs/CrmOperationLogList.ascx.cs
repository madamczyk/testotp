﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Helpers;
using System;
using System.Linq;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Logs
{
    public partial class CrmOperationLogList : System.Web.UI.UserControl
    {
        #region Fields

        public DateTime? RowDateFrom
        {
            get
            {
                if (ViewState["RowDateFrom"] != null)
                    return (DateTime)ViewState["RowDateFrom"];

                return null;
            }
            set
            {
                ViewState["RowDateFrom"] = value;
            }
        }

        public DateTime? RowDateTo
        {
            get
            {
                if (ViewState["RowDateTo"] != null)
                    return (DateTime)ViewState["RowDateTo"];

                return null;
            }
            set
            {
                ViewState["RowDateTo"] = value;
            }
        }

        public int? OperationTypeFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["OperationTypeFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["OperationTypeFilterSelectedValue"] as int?;
            }
            set { ViewState["OperationTypeFilterSelectedValue"] = value; }
        }

        public readonly int _previewLength = 30;
        public readonly int _nameLength = 25;

        #endregion

        #region Methods

        private void BindGrid()
        {
            crmOperationLogsLinqDS.DataResolver = () =>
            {
                IQueryable<CrmOperationLog> result = RequestManager.Services.CrmOpertionLogsService.GetCrmOperationsLog().AsQueryable();

                rgCrmOperationLogs.ApplyFilter<CrmOperationLog>(ref result, "UserFullName",
                    (q, f) => q.Where(pgol => !String.IsNullOrEmpty(pgol.UserFullName) && pgol.UserFullName.ToLower().Contains(f.ToLower())));
                rgCrmOperationLogs.ApplyFilter<CrmOperationLog>(ref result, "Message",
                    (q, f) => q.Where(pgol => !String.IsNullOrEmpty(pgol.Message) && pgol.Message.ToLower().Contains(f.ToLower())));
                rgCrmOperationLogs.ApplyFilter<CrmOperationLog>(ref result, "CrmResponse",
                    (q, f) => q.Where(pgol => !String.IsNullOrEmpty(pgol.CrmResponse) && pgol.CrmResponse.ToLower().Contains(f.ToLower())));

                if (this.OperationTypeFilterSelectedValue.HasValue)
                {
                    result = result.Where(pgol => pgol.OperationType == this.OperationTypeFilterSelectedValue.Value);
                }

                if (RowDateFrom.HasValue)
                    result = result.Where(pgol => pgol.RowDate >= this.RowDateFrom.Value);
                if (RowDateTo.HasValue)
                    result = result.Where(pgol => pgol.RowDate <= this.RowDateTo.Value);

                return result.ToList();
            };
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgCrmOperationLogs_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgCrmOperationLogs_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                CrmOperationLog obj = (CrmOperationLog)item.DataItem;
                RadButton btnshowMessage = item["Message"].FindControl("btnshowMessage") as RadButton;
                RadButton btnShowResponse = item["CrmResponse"].FindControl("btnShowResponse") as RadButton;
                RadButton btnShowRequest = item["CrmRequest"].FindControl("btnShowRequest") as RadButton;

                btnshowMessage.Attributes.Add("id", obj.Id.ToString());
                btnShowResponse.Attributes.Add("id", obj.Id.ToString());
                btnShowRequest.Attributes.Add("id", obj.Id.ToString());

                btnshowMessage.Visible = !String.IsNullOrEmpty(obj.Message) && obj.Message.Length > _previewLength;
                btnShowResponse.Visible = !String.IsNullOrEmpty(obj.CrmResponse) && obj.CrmResponse.Length > _previewLength;
                btnShowRequest.Visible = !String.IsNullOrEmpty(obj.CrmRequest) && obj.CrmRequest.Length > _previewLength;
            }
        }

        protected void rgCrmOperationLogs_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgCrmOperationLogs.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgCrmOperationLogs.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgCrmOperationLogs.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgCrmOperationLogs.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgCrmOperationLogs.MasterTableView.Rebind();
            }
        }

        protected void btnshowMessage_Click(object sender, EventArgs e)
        {
            RadButton btn = sender as RadButton;
            if (btn == null || String.IsNullOrEmpty(btn.Attributes["id"]))
                return;

            int logId = int.Parse(btn.Attributes["id"]);

            rwPreview.Title = String.Format((string)GetLocalResourceObject("Dialog_Title_Message"), logId);
            taMessage.Text = RequestManager.Services.CrmOpertionLogsService.GetCrmOperationLogsById(logId).Message;

            RadScriptManager.RegisterStartupScript(Page, Page.GetType(),
                "showRwPreviewFunction",
                "function showRwPreview() { $find(\"" + rwPreview.ClientID + "\").show(); Sys.Application.remove_load(showRwPreview); } Sys.Application.add_load(showRwPreview);",
                true);
        }

        protected void btnShowRequest_Click(object sender, EventArgs e)
        {
            RadButton btn = sender as RadButton;
            if (btn == null || String.IsNullOrEmpty(btn.Attributes["id"]))
                return;

            int logId = int.Parse(btn.Attributes["id"]);

            rwPreview.Title = String.Format((string)GetLocalResourceObject("Dialog_Title_Request"), logId);
            string request = RequestManager.Services.CrmOpertionLogsService.GetCrmOperationLogsById(logId).CrmRequest;
            taMessage.Text = XmlDisplayHelper.DisplayIndentedXML(request);
            
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(),
                "showRwPreviewFunction",
                "function showRwPreview() { $find(\"" + rwPreview.ClientID + "\").show(); Sys.Application.remove_load(showRwPreview); } Sys.Application.add_load(showRwPreview);",
                true);
        }

        protected void btnShowResponse_Click(object sender, EventArgs e)
        {
            RadButton btn = sender as RadButton;
            if (btn == null || String.IsNullOrEmpty(btn.Attributes["id"]))
                return;

            int logId = int.Parse(btn.Attributes["id"]);

            rwPreview.Title = String.Format((string)GetLocalResourceObject("Dialog_Title_Response"), logId);
            string response = RequestManager.Services.CrmOpertionLogsService.GetCrmOperationLogsById(logId).CrmResponse;
            taMessage.Text = XmlDisplayHelper.DisplayIndentedXML(response);

            RadScriptManager.RegisterStartupScript(Page, Page.GetType(),
                "showRwPreviewFunction",
                "function showRwPreview() { $find(\"" + rwPreview.ClientID + "\").show(); Sys.Application.remove_load(showRwPreview); } Sys.Application.add_load(showRwPreview);",
                true);
        }

        protected void rcbOperationType_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (CrmOparationLogsType ot in Enum.GetValues(typeof(CrmOparationLogsType)))
                {
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(ot), ((int)ot).ToString()));
                }
            }
        }

        protected void rcbOperationType_PreRender(object sender, EventArgs e)
        {
            if (OperationTypeFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;
                combo.SelectedValue = OperationTypeFilterSelectedValue.ToString();
            }
        }

        protected void rdpRowDateFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.RowDateFrom;
        }

        protected void rdpRowDateTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.RowDateTo;
        }

        protected void rcbOperationType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            OperationTypeFilterSelectedValue = int.Parse(e.Value);
            rgCrmOperationLogs.MasterTableView.Rebind();
        }

        protected void rdpRowDateFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            RowDateFrom = e.NewDate;
            rgCrmOperationLogs.MasterTableView.Rebind();
        }

        protected void rdpRowDateTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            RowDateTo = e.NewDate;
            rgCrmOperationLogs.MasterTableView.Rebind();
        }

        #endregion

    }
}

