﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyCompletness.ascx.cs" Inherits="IntranetApp.Controls.Common.PropertyCompletness" %>

<script type="text/javascript">
    $(document).ready(function () {
        var progressValue = $('#<%=hfProgressValue.ClientID%>').val();
        progressValue = parseInt(progressValue);
        $("#progressbar").progressbar({ value: progressValue });
        $("#lblPercentage").html(progressValue);
    });

    function ShowModeDetails() {
        var progressValue = $('#<%=hfProgressValue.ClientID%>').val();
        $('#detailsPanel').dialog({
            autoOpen: false,
            modal: false,
            height: 220,
            resizable: true,
            title: "Property completness: " + progressValue + "%",
            open: function (event, ui) {
                $(event.target).parent().css('position', 'fixed');
                $(event.target).parent().css('top', '0px');
                $(event.target).parent().css('right', '0px');
                $(event.target).parent().css('left', 'auto');
                $(this).css({ 'overflow-y': 'auto' });
            }
        }).dialog('open');
    }

    function ShowConfirmDialog(sender, eventArgs) {
        var oWnd = $find("<%= rwConfirm.ClientID %>")        
        oWnd.show();
        return false;
    }

    function btnDiscard_No(sender, eventArgs) {
        var oWnd = $find("<%= rwConfirm.ClientID %>")
        oWnd.hide();
        return false;
    }
</script>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
        <telerik:RadWindow ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Width="340" Height="200px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" meta:resourcekey="dialogSignOffText" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="btnDiscard_Yes"></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClientClick="btnDiscard_No"></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>



<div style="width: 300px; margin-right: 10px;">
    <table>
        <tr>
            <td valign="top">
                <asp:Button runat="server" ID="btnSignOffProperty" meta:resourcekey="btnSignOffProperty" OnClientClick="return ShowConfirmDialog()" />
            </td>
            <td style="width: 200px;">
                <div id="progressbar"></div>
                <br />
                <div style="float: right;"><span id="lblPercentage"></span>%</div>
                <asp:Label ID="lblPropertyCompletness" meta:resourcekey="lblPropertyCompletness" runat="server" />
            </td>
            <td style="width: 80px; cursor: pointer;" align="right" valign="top">
                <span style="text-decoration: underline;" onclick="ShowModeDetails()">
                    <asp:Label runat="server" ID="lblShowMore" meta:resourcekey="lblShowMore" /></span>
            </td>
        </tr>
        <tr></tr>
    </table>
</div>

<div class="completnessDetails" id="detailsPanel" style="display: none;">
    <p>
        <asp:Label runat="server" ID="lblPropertyCompletnessInfo" meta:resourcekey="lblPropertyCompletnessInfo" /></p>
    <div id="validationErrorsList" runat="server">
    </div>
</div>

<asp:HiddenField ID="hfProgressValue" runat="server" />
