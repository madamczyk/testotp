﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Controls.Common
{
    public partial class BaseUserControl : System.Web.UI.UserControl
    {
        public void NavigateBack()
        {
            if (Session["navigationList"] != null)
            {
                Stack<string> navigationList = (Stack<string>)Session["navigationList"];
                navigationList.Pop();
                string redirectAddress = navigationList.Peek();
                Session["navigationList"] = navigationList;
                Response.Redirect(redirectAddress);
            }
        }
    }
}