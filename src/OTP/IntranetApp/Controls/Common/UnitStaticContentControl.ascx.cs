﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Common.Extensions;

namespace IntranetApp.Controls.Common
{
    public partial class UnitStaticContentControl : System.Web.UI.UserControl
    {
        #region Fields

        public string StaticContentInfo
        {
            get { return ViewState["StaticContentInfo" + this.ID] as string; }
            set { ViewState["StaticContentInfo" + this.ID] = value; }
        }

        public UnitStaticContentType ContentType
        {
            get 
            {
                if (ViewState["ContentType" + this.ID] != null)
                {
                    return (UnitStaticContentType)((ViewState["ContentType" + this.ID] as int?).Value);
                }
                return UnitStaticContentType.Bedrooms;

            }
            set { ViewState["ContentType" + this.ID] = (int)value; }
        }

        public int? UnitID
        {
            get { return ViewState["UnitID" + this.ID] as int?; }
            set { ViewState["UnitID" + this.ID] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemId" + this.ID] as int?; }
            set { ViewState["EditedItemId" + this.ID] = value; }
        }

        /// <summary>
        /// List will contain all the Discounts for the destination. Will be used for displaying the list to the user
        /// </summary>
        public List<UnitStaticContent> ElementsList
        {
            get { return ViewState["UnitStaticContentElementsList" + this.ID] as List<UnitStaticContent>; }
            set { ViewState["UnitStaticContentElementsList" + this.ID] = value; }
        }

        /// <summary>
        /// List will contain UnitStaticContents, that were added by the user. UnitStaticContents from this list should be added to the DB
        /// </summary>
        public List<UnitStaticContent> NewElements
        {
            get { return ViewState["UnitStaticContentNewElements" + this.ID] as List<UnitStaticContent>; }
            set { ViewState["UnitStaticContentNewElements" + this.ID] = value; }
        }

        /// <summary>
        /// List will contain UnitStaticContents, that were modified by the user and did not exist in the DB before - we don't need to update taxes, that do not exist in the DB
        /// </summary>
        public List<UnitStaticContent> ModifiedElementsList
        {
            get { return ViewState["UnitStaticContentModifiedElementsList" + this.ID] as List<UnitStaticContent>; }
            set { ViewState["UnitStaticContentModifiedElementsList" + this.ID] = value; }
        }

        /// <summary>
        /// List will containt IDs of taxes that were deleted by the user. UnitStaticContents from this list should be deleted from the DB
        /// </summary>
        public List<int> RemovedElementsList
        {
            get { return ViewState["UnitStaticContentRemovedElementsList" + this.ID] as List<int>; }
            set { ViewState["UnitStaticContentRemovedElementsList" + this.ID] = value; }
        }


        #endregion

        #region Methods

        private void BindGrid()
        {
            this.staticContentLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.UnitStaticContent> result = ElementsList.AsQueryable();                     
                
                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
            this.lblStaticContentInfo.Text = StaticContentInfo;
        }

        protected void rgStaticContent_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int itemId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitStaticContentId"].ToString());

                if (UnitID.HasValue)
                {
                    if (RequestManager.Services.UnitsService.GetUnitStaticContentById(itemId) == null)
                    {
                        string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_StaticContent"));
                        string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_StaticContent"));

                        RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                        BindGrid();
                    }
                }
                ElementsList.RemoveAll(t => t.UnitStaticContentId == itemId);
                ModifiedElementsList.RemoveAll(t => t.UnitStaticContentId == itemId);
                if (NewElements.RemoveAll(t => t.UnitStaticContentId == itemId) == 0)
                    RemovedElementsList.Add(itemId);
                
            }
            else if (e.CommandName == "Edit")
            {
                int editedId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitStaticContentId"].ToString());
                EditedItemId = editedId;
            }
            else if (e.CommandName == "Cancel")
            {
                EditedItemId = null;
            }
        }

        protected void rgStaticContent_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                DataAccess.UnitStaticContent obj = (DataAccess.UnitStaticContent)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                string title = obj.ContentValueInternational.ToString().Limit();

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgStaticContent.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_StaticContent"), title), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_StaticContent"), title));
            }
        }

        protected void rgStaticContent_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgStaticContent.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgStaticContent.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgStaticContent.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgStaticContent.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgStaticContent.MasterTableView.Rebind();
            }
        }

        protected void rgStaticContent_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    DataAccess.UnitStaticContent obj = new DataAccess.UnitStaticContent();
                    ReadGridItemData(args.Item, obj);
                    obj.UnitStaticContentId = ElementsList.Count > 0 ? ElementsList.Max(t => t.UnitStaticContentId) + 1 : 1;
                    ElementsList.Add(obj);
                    NewElements.Add(obj);
                    this.rgStaticContent.MasterTableView.Rebind();
                }
            }
        }

        protected void rgStaticContent_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("UnitStaticContentId"));
                    DataAccess.UnitStaticContent obj;
                    obj = ElementsList.Where(t => t.UnitStaticContentId == id).SingleOrDefault();

                    ReadGridItemData(editForm, obj);

                    if (NewElements.Where(t => t.UnitStaticContentId == id).Count() == 0) // if tax is not in the list of newly added
                    {
                        ModifiedElementsList.Add(obj); // make sure to update its data
                    }
                    else
                    {
                        NewElements.RemoveAll(t => t.UnitStaticContentId == id);
                        NewElements.Add(obj);
                    }
                    this.rgStaticContent.MasterTableView.Rebind();

                }
            }
            int reportId = int.Parse(args.Item.OwnerTableView.DataKeyValues[args.Item.ItemIndex]["UnitStaticContentId"].ToString());
            EditedItemId = reportId;
        }

        private void ReadGridItemData(GridItem item, DataAccess.UnitStaticContent obj)
        {
            obj.ContentCode = (int)ContentType;
            obj.SetContentValue((item.FindControl("txtFeature") as RadTextBox).Text);
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
        
        }

        #endregion Event Handlers
    }
}