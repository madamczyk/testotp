﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UnitStaticContentControl.ascx.cs" Inherits="IntranetApp.Controls.Common.UnitStaticContentControl" %>


<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="rgStaticContent">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgStaticContent" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData1">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rwError" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
                <ContentTemplate>
                    <div style="margin: 20px;">
                        <div style="float: left; width: 240px; text-align: center;">
                            <asp:Label ID="lblError" Font-Bold="true" runat="server" />
                            <br />
                            <br />
                            <asp:Button ID="wndBtnError_Ok" runat="server" Text="<%$ Resources: Controls, Dialog_Ok %>" OnClick="btnError_Ok" />
                        </div>
                        <div style="clear: both;">
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
<asp:Label runat="server" ID="lblStaticContentInfo" />
<br /><br />
    <telerik:RadGrid 
        Width="450px" 
        AutoGenerateColumns="False" 
        ID="rgStaticContent" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="false" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="staticContentLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgStaticContent_ItemCommand" 
        OnItemDataBound="rgStaticContent_ItemDataBound" 
        OnDataBound="rgStaticContent_DataBound"
        OnUpdateCommand="rgStaticContent_UpdateCommand"
        OnInsertCommand="rgStaticContent_InsertCommand">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="UnitStaticContentId" DataSourceID="staticContentLinqDS" CommandItemDisplay="Bottom">
			<Columns>
				<telerik:GridEditCommandColumn UniqueName="EditCommandColumn" HeaderText=" " HeaderStyle-Width="10px" >
                    <ItemStyle Width="10px"></ItemStyle>
                </telerik:GridEditCommandColumn>
                <telerik:GridBoundColumn meta:resourceKey="lblContentColumn" DataField="ContentValueCurrentLanguage" FilterControlWidth="150px" UniqueName="ContentValueCurrentLanguage" SortExpression="ContentValueCurrentLanguage" HeaderStyle-Width="300px" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>                
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>			
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="ContentValueCurrentLanguage" SortOrder="Ascending" />
            </SortExpressions>		
            <EditFormSettings EditFormType="Template">
                    <FormTemplate>
                        <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="Features"
                            HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                            EnableClientScript="true" runat="server" CssClass="errorInForm" />
                        <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                            <tr>
                                <td style='padding-top: 10px'>
                                    <asp:Label runat="server" ID="lblContent" meta:resourceKey="lblContent"></asp:Label>
                                </td>
                                <td class="paramInputCell" style='padding-top: 10px'>
                                    <telerik:RadTextBox Width="350px" runat="server" ID="txtFeature" TextMode="SingleLine" 
                                        Text='<%# Eval("ContentValueCurrentLanguage") %>'>
                                    </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="Features" ID="RequiredFieldValidator7"
                                        Text="*" ControlToValidate="txtFeature" runat="server" Display="Static"
                                        CssClass="error" meta:resourceKey="ValContent"></asp:RequiredFieldValidator>
                                </td>
                            </tr>                             
                            <tr>
                                <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                    <div style="position: relative; width: 415px;">
                                        <!-- workaround for issue with RadButtons not being aligned vertically -->
                                        <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="Features"
                                            VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                            CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' CausesValidation="true">
                                        </telerik:RadButton>
                                        <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                        </telerik:RadButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </FormTemplate>
                </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

</asp:Panel>

<otpDS:SimpleDataSource ID="staticContentLinqDS" runat="server" />