﻿using BusinessLogic.Enums;
using BusinessLogic.Managers;
using BusinessLogic.Objects.Templates.Email;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Helpers.ValidationRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Common
{
    public partial class PropertyCompletness : System.Web.UI.UserControl
    {
        private const string imagePlus = "/Images/plus-small.png";

        private List<ValidationResult> ValidationErrors;

        public int? PropertyId
        {
            get { return ViewState["CompletnessPropertyId"] as int?; }
            set { ViewState["CompletnessPropertyId"] = value; }
        }

        private int? _completness;

        public int? Completness
        {
            get
            {
                if (!_completness.HasValue)
                    PerformValidate();
                return _completness;
            }
            set
            {
                this.hfProgressValue.Value = value.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            btnSignOffProperty.Visible = false;

            if (!_completness.HasValue)
            {
                PerformValidate();
            }

            if (this.PropertyId.HasValue)
            {
                var property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyId.Value);

                /// Show this button only when property is completed on 100% , is not enabled and sign off email wasn't send yet
                if (_completness.HasValue && _completness.Value == 100 && !property.PropertyLive && string.IsNullOrEmpty(property.SignOffCode))
                {
                    btnSignOffProperty.Visible = true;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            string errors = "";
            foreach (var err in ValidationErrors)
            {
                errors += string.Format("<p rel-object-id='{3}' style='cursor: pointer;' onclick='{2}'><img width='11px' src='{1}' alt='-' /> {0}</p>",
                    err.ValidationMessage,
                    imagePlus,
                    err.MessageClientEventName,
                    err.ValidationObjectID.ToString());
            }
            validationErrorsList.InnerHtml = errors;
        }

        private void PerformValidate()
        {
            if (this.PropertyId.HasValue)
            {
                var property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyId.Value);
                _completness = PropertyValidationHelper.CalculatePropertyCompletness(property);
                ValidationErrors = PropertyValidationHelper.ValidationErrors;
                this.hfProgressValue.Value = _completness.ToString();
            }
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            if (this.PropertyId.HasValue)
            {
                string signOffToken = Guid.NewGuid().ToString();

                var property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyId.Value);
                property.SignOffCode = signOffToken;

                //SEND EMAIL TO HOMEOWNER
                string singOffPreviewLink = string.Format("{0}://{1}/{2}/{3}?token={4}",
                                            Request.Url.Scheme,
                                            RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.ExtranetURL),
                                            "PropertyDetail/PropertyDetails",
                                            property.PropertyID,
                                            signOffToken);

                string singOffConfirmLink = string.Format("{0}://{1}/{2}/{3}",
                            Request.Url.Scheme,
                            RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.ExtranetURL),
                            "PropertyDetail/SignOffConfirm",
                            signOffToken);

                PropertySignOffEmail email = new PropertySignOffEmail(property.User.email);
                email.PropertyName = property.PropertyNameCurrentLanguage;
                email.UserFirstName = property.User.Firstname;
                email.PreviewLink = singOffPreviewLink;
                email.PreviewLinkTitle = (string)GetLocalResourceObject("lblSignOffPreviewLinkTitle");
                email.ConfirmSignOffLink = singOffConfirmLink;
                email.ConfirmSignOffLinkTitle = (string)GetLocalResourceObject("lblSignOffConfirmLinkTitle");

                RequestManager.Services.MessageService.AddEmail(email, property.User.DictionaryCulture.CultureCode);
            }
        }
    }
}