﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace IntranetApp.Controls.Common
{
    /// <summary>
    /// Extends LinqDataSource by simplifying returning data -
    /// one should only provide delegate that will return data.
    /// </summary>
    public class SimpleDataSource : LinqDataSource
    {
        private class _DataContext
        {
            private SimpleDataSource _containingDS;

            /// <summary>
            /// Initializes a new instance of the <see cref="_DataContext"/> class.
            /// </summary>
            /// <param name="containingDS">The containing DS.</param>
            public _DataContext(SimpleDataSource containingDS)
            {
                _containingDS = containingDS;
            }

            /// <summary>
            /// Gets the data by resolving delegate from containing data source.
            /// </summary>
            /// <value>The data.</value>
            public object Data
            {
                get
                {
                    if (_containingDS.DataResolver == null)
                        throw new InvalidOperationException("Can't return data, property DataResolver is not initiated.");

                    return _containingDS.DataResolver();
                }
            }
        }

        /// <summary>
        /// Gets or sets the data resolver, that will be used for returning data.
        /// </summary>
        /// <value>The data resolver.</value>
        public Func<object> DataResolver { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinqSimpleDataSource"/> class.
        /// </summary>
        public SimpleDataSource()
            : base()
        {
            this.ContextCreating += new EventHandler<LinqDataSourceContextEventArgs>(LinqPropertyDataSource_ContextCreating);
            this.TableName = "Data";
        }

        private void LinqPropertyDataSource_ContextCreating(object sender, LinqDataSourceContextEventArgs e)
        {
            e.ObjectInstance = new _DataContext(this);
        }
    }
}