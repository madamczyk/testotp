﻿using BusinessLogic.Managers;
using DataAccess.Enums;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Properties
{
    public partial class Units : System.Web.UI.UserControl
    {
        public int? PropertyID
        {
            get { return ViewState["UnitId"] as int?; }
            set { ViewState["UnitId"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemId"] as int?; }
            set { ViewState["EditedItemId"] = value; }
        }

        public List<DataAccess.Unit> NewItems
        {
            get
            {
                if (ViewState["NewItems"] == null)
                {
                    ViewState["NewItems"] = new List<DataAccess.Unit>();
                }
                return ViewState["NewItems"] as List<DataAccess.Unit>;
            }
            set { ViewState["NewItems"] = value; }
        }

        private int _nextProductParameterId;

        public int NextProductParameterId
        {
            get
            {
                int result = _nextProductParameterId;
                _nextProductParameterId++;
                return result;
            }
            set
            {
                _nextProductParameterId = value;
            }
        }

        #region Methods

        private void BindGrid()
        {
            this.unitsLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.Unit> result = null;
                List<DataAccess.Unit> resultList;

                if (this.PropertyID.HasValue)
                    result = RequestManager.Services.UnitsService.GetUnitsByPropertyId(PropertyID.Value).AsQueryable();

                if (result != null)
                    resultList = new List<DataAccess.Unit>(result);
                else
                    resultList = new List<DataAccess.Unit>();

                resultList.AddRange(NewItems);

                if (resultList.Count != 0)
                    NextProductParameterId = resultList.Max(ob => ob.UnitID) + 1;
                else
                    NextProductParameterId = 1;

                return resultList.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgUnit_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int unitTypeID = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitID"].ToString());

                if (RequestManager.Services.UnitsService.GetUnitById(unitTypeID) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Unit"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_Unit"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/Units/UnitDetails.aspx?id=");
                    sb.Append(unitTypeID);
                    sb.Append("&mode=");
                    sb.Append((int)FormMode.Edit);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == "Edit")
            {
                int editedId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitID"].ToString());
                EditedItemId = editedId;
            }
            else if (e.CommandName == "Cancel")
            {
                EditedItemId = null;
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }    
            else if (e.CommandName == RadGrid.DeleteCommandName) // TODO: expand deletion
            {
                int unitID = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitID"].ToString());

                if (PropertyID.HasValue)
                {
                    DataAccess.Unit obj;
                    if ((obj = RequestManager.Services.UnitsService.GetUnitById(unitID)) != null)
                    {
                        if (obj.UnitAppliancies.Count != 0)
                        {
                            string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Unit"), obj.UnitTitleCurrentLanguage.ToString());
                            string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_Unit"), obj.UnitTitleCurrentLanguage.ToString());

                            RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                        }
                        else
                        {
                            RequestManager.Services.UnitsService.DeleteUnit(unitID);
                        }
                    }                    
                }     
                else                        
                    NewItems.Remove(NewItems.Where(u => u.UnitID == unitID).FirstOrDefault());          
            }
            else if (e.CommandName == "InitInsert")
            {
                e.Canceled = true;
            }
        }

        protected void rgUnit_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                DataAccess.Unit obj = (DataAccess.Unit)item.DataItem;
                Label lbl = item.FindControl("lblCleaningStatus") as Label;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                item.Attributes["unit-id"] = obj.UnitID.ToString();
                lbl.Text = EnumConverter.GetValue((UnitCleaningStatus)obj.CleaningStatus);

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgUnit.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Unit"), obj.UnitTitle.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Unit"), obj.UnitTitle.ToString()));
            }
            else if (e.Item is GridCommandItem)
            {
                if (PropertyID.HasValue)
                {
                    LinkButton lbAdd = e.Item.FindControl("InitInsertButton") as LinkButton;
                    Button btnAdd = e.Item.FindControl("AddNewRecordButton") as Button;
                    string onAddClick = "openUnitDialog(" + PropertyID.Value.ToString() + "," + ((int)FormMode.Add).ToString() + "); return false;";
                    lbAdd.Attributes["onclick"] = onAddClick;
                    btnAdd.Attributes["onclick"] = onAddClick;
                }
            }
        }

        protected void rgUnit_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgUnit.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgUnit.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgUnit.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgUnit.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgUnit.MasterTableView.Rebind();
            }
        }

        protected void rgUnit_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    DataAccess.Unit newUnit = new DataAccess.Unit();
                    ReadGridItemData(args.Item, newUnit);                    
                    if (PropertyID.HasValue)
                    {
                        newUnit.Property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyID.Value);
                        RequestManager.Services.UnitsService.AddUnit(newUnit);
                        RequestManager.Services.SaveChanges();
                    }
                    else
                    {
                        NewItems.Add(newUnit);
                    }
                    this.rgUnit.MasterTableView.Rebind();
                }
            }
        }

        protected void rgUnit_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    DataAccess.Unit unit = null;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("UnitID"));
                    if (this.PropertyID.HasValue)
                    {
                        unit = RequestManager.Services.UnitsService.GetUnitById(id);
                        ReadGridItemData(editForm, unit);
                        RequestManager.Services.SaveChanges();
                    }
                    else
                    {
                        unit = NewItems.Where(u => u.UnitID == id).FirstOrDefault();
                            ReadGridItemData(editForm, unit);
                    }
                    this.rgUnit.MasterTableView.Rebind();                    
                }
                int reportId = int.Parse(args.Item.OwnerTableView.DataKeyValues[args.Item.ItemIndex]["UnitID"].ToString());
                EditedItemId = reportId;
            }
        }

        private void ReadGridItemData(GridItem item, DataAccess.Unit unit)
        {
            unit.SetTitleValue((item.FindControl("txtTitle") as RadTextBox).Text);
            unit.CleaningStatus = int.Parse((item.FindControl("rcbCleaningStatus") as RadComboBox).SelectedValue);
            unit.MaxNumberOfBathrooms = int.Parse((item.FindControl("txtNumberOfBathrooms") as RadTextBox).Text);
            unit.MaxNumberOfBedRooms = int.Parse((item.FindControl("txtNumberOfBedrooms") as RadTextBox).Text);
            unit.MaxNumberOfGuests = int.Parse((item.FindControl("txtNumberOfGuests") as RadTextBox).Text);
            unit.Property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyID.Value);
            unit.UnitCode = (item.FindControl("txtUnitCode") as RadTextBox).Text;
            unit.SetDescValue((item.FindControl("txtUnitDesc") as RadTextBox).Text);
            unit.UnitType = RequestManager.Services.UnitTypesService.GetUnitTypeById(int.Parse((item.FindControl("rcbUnitType") as RadComboBox).SelectedValue));
        }

        #endregion Event Handlers

        protected void ddlCleaningStatus_PreRender(object sender, EventArgs e)
        {
            RadComboBox ddl = sender as RadComboBox;
            foreach(UnitCleaningStatus en in Enum.GetValues(typeof(UnitCleaningStatus)))
            {
                ddl.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(en), ((int)en).ToString()));
            }
            if (EditedItemId.HasValue && EditedItemId.Value != 0)
            {
                DataAccess.Unit unit = RequestManager.Services.UnitsService.GetUnitById(EditedItemId.Value);
                ddl.SelectedValue = unit.CleaningStatus.ToString();
            }
        }

        protected void ddlUnitType_PreRender(object sender, EventArgs e)
        {
            if (PropertyID.HasValue)
            {
                RadComboBox ddl = sender as RadComboBox;
                IEnumerable<DataAccess.UnitType> unitTypes = RequestManager.Services.UnitTypesService.GetUnitTypesByPropertyId(this.PropertyID.Value);
                foreach (DataAccess.UnitType ut in unitTypes)
                {
                    ddl.Items.Add(new RadComboBoxItem(ut.UnitTypeTitleCurrentLanguage, ut.UnitTypeID.ToString()));
                }
                if (EditedItemId.HasValue && EditedItemId.Value != 0)
                {
                    DataAccess.Unit unit = RequestManager.Services.UnitsService.GetUnitById(EditedItemId.Value);
                    ddl.SelectedValue = unit.UnitType.UnitTypeID.ToString();
                }
            }
        }
    }
}