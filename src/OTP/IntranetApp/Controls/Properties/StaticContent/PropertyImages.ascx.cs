﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using Telerik.Web.UI;
using System.IO;
using BusinessLogic.Enums;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Net;
using System.Drawing;
using System.Drawing.Drawing2D;
using Common.Extensions;

namespace IntranetApp.Controls.Properties
{
    public partial class PropertyImages : System.Web.UI.UserControl
    {
        public int? PropertyID
        {
            get { return ViewState["PropertyAddOnId"] as int?; }
            set { ViewState["PropertyAddOnId"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemId"] as int?; }
            set { ViewState["EditedItemId"] = value; }
        }

        public int? ContentType
        {
            get { return ViewState["ContentType"] as int?; }
            set { ViewState["ContentType"] = value; }
        }

        #region Methods

        private void BindGrid()
        {
            this.ImagesLinqDS.DataResolver = () =>
            {
                IQueryable<PropertyStaticContent> result = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(this.PropertyID.Value, new List<PropertyStaticContentType> 
                { 
                    PropertyStaticContentType.FullScreen, 
                    PropertyStaticContentType.FullScreen_Retina,
                    PropertyStaticContentType.GalleryImage,
                    PropertyStaticContentType.GalleryImage_Retina,
                    PropertyStaticContentType.ListView,
                    PropertyStaticContentType.ListView_Retina,
                    PropertyStaticContentType.Thumbnail_Retina,
                    PropertyStaticContentType.Thumbnail
                }).AsQueryable();
               
                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
            if(PropertyID.HasValue)
                BindControls();
            
        }

        private void BindControls()
        {
            if (!this.ContentType.HasValue)
            {
                BindImageTypes();
                
                this.ContentType = int.Parse(this.rcbImageType.SelectedValue);
            }
            if (this.PropertyID.HasValue)
            {
                this.rauImageUpload.HttpHandlerUrl = string.Format("~/Handlers/CustomHandler.ashx?pid={0}&", PropertyID.Value.ToString());
            }
            SetResInfo(this.ContentType.Value);
            rauImageUpload.MaxFileSize = int.Parse(RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.MaxFileSize));
        }

        private void BindImageTypes()
        {
            this.rcbImageType.Items.Clear();

            if (!PropertyID.HasValue)
                return;

            foreach (var type in Enum.GetValues(typeof(PropertyStaticContentType)))
            {
                if (((int)type) < 8)
                {
                    if (((PropertyStaticContentType)type == PropertyStaticContentType.FullScreen &&
                        RequestManager.Services.PropertiesService.
                        GetPropertyStaticContentByType(this.PropertyID.Value, (PropertyStaticContentType)type).Count() > 0)
                        ||
                        ((PropertyStaticContentType)type == PropertyStaticContentType.FullScreen_Retina &&
                        RequestManager.Services.PropertiesService.
                        GetPropertyStaticContentByType(this.PropertyID.Value, (PropertyStaticContentType)type).Count() > 0)
                        )
                    {
                        continue;
                    }
                    this.rcbImageType.Items.Add(new RadComboBoxItem(EnumConverter.GetValue((PropertyStaticContentType)type), ((int)type).ToString()));
                }
            }
            if (this.ContentType.HasValue)
            {
                this.rcbImageType.SelectedValue = this.ContentType.Value.ToString();
            }
        }

        private void SetResInfo(int imageContentType)
        {
            PropertyStaticContentType pscType = (PropertyStaticContentType)imageContentType;
            string minSize = RequestManager.Services.SettingsService.GetSettingValue("StaticContent.Image.Type_"  + pscType.ToString() + ".MinSize");
            string[] dimension = minSize.Split("x".ToArray());            
            minW.Value = dimension.GetValue(0).ToString();
            minH.Value = dimension.GetValue(1).ToString();

            string maxSize = RequestManager.Services.SettingsService.GetSettingValue("StaticContent.Image.Type_" + pscType.ToString() + ".MaxSize");
            dimension = maxSize.Split("x".ToArray());
            maxW.Value = dimension.GetValue(0).ToString();
            maxH.Value = dimension.GetValue(1).ToString();

            lblResolutionInfo.Text = string.Format(GetLocalResourceObject("lblResolutionInfo").ToString(), minSize, maxSize);
            if (minSize.ToLower() == maxSize.ToLower())
            {
                lblResolutionInfo.Text = string.Format(GetLocalResourceObject("lblResolutionInfoSimple").ToString(), maxSize);
            }
            lblPlaceOfDisplay.Text = string.Format(GetLocalResourceObject("lblPlaceOfDisplay.Text").ToString(), GetLocalResourceObject("lblImageDisplayOn_" + imageContentType.ToString()).ToString());
        }

        protected void rgImages_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgImages.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgImages.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgImages.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgImages.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgImages.MasterTableView.Rebind();
            }
        }

        protected void rgImages_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int itemId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyStaticContentId"].ToString());

                // TODO: update to support other cases
                if (RequestManager.Services.PropertiesService.GetPropertyStaticContentById(itemId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Image"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Image"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                    RequestManager.Services.PropertiesService.RemoveStaticContent(itemId);
            }
            BindImageTypes();
        }

        protected void rgImages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                PropertyStaticContent obj = (PropertyStaticContent)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                HtmlAnchor ctrl = (HtmlAnchor)item.FindControl("imgLink");
                string title = Path.GetFileNameWithoutExtension(obj.ContentValue).Limit();

                ctrl.HRef = string.Format("{0}/{1}", RequestManager.Services.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray()), obj.ContentValue.TrimStart("/".ToCharArray()));
                ctrl.InnerText = Path.GetFileNameWithoutExtension(ctrl.HRef.Replace("/", "\\"));
                item["ContentValue"].Text = Path.GetFileName(ctrl.HRef.Replace("/", "\\"));
                item["ContentCode"].Text = EnumConverter.GetValue((PropertyStaticContentType)obj.ContentCode);

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgImages.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Image"), title), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Image"), title));

                System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)item.FindControl("imageThumbnail");
                img.ImageUrl = ctrl.HRef;
                img.Width = 50;
                img.Attributes["onclick"] = string.Format("openImageDialog('{0}')", ctrl.HRef);
            }
        }

        protected void rauImageUpload_FileUploaded(object sender, EventArgs e)
        {
            if (!PropertyID.HasValue)
                return;
            foreach (UploadedFile file in this.rauImageUpload.UploadedFiles)
            {
                PropertyStaticContent psc = new PropertyStaticContent();
                psc.ContentCode = this.ContentType.Value;
                psc.ContentValue = file.FileName; // RequestManager.Services.BlobService.AddFile(file.InputStream, this.PropertyID.Value, file.GetName());
                
                psc.Property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyID.Value);
                RequestManager.Services.PropertiesService.AddPropertyStaticContent(psc);                
            }            

            RequestManager.Services.SaveChanges();
            this.BindGrid();
            this.rgImages.MasterTableView.Rebind();
            BindImageTypes();
            this.rcbImageType.SelectedValue = ((int)PropertyStaticContentType.Thumbnail).ToString();
            this.ContentType = (int)PropertyStaticContentType.Thumbnail;
            SetResInfo((int)PropertyStaticContentType.Thumbnail);
        }

        protected void rcbImageType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            this.ContentType = int.Parse(e.Value);
            this.SetResInfo(this.ContentType.Value);
        }

        private string TrimString(string floatValue)
        {
            if (floatValue.IndexOf(".") != -1)
            {
                return floatValue.Substring(0, floatValue.IndexOf("."));
            }
            return floatValue;
        }

        protected void Resize_Click(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                ////Get the Cordinates                
                int w = int.Parse(maxW.Value);
                int h = int.Parse(maxH.Value);

                string imgUrl = EditedImageUrl.Value.Substring(0, EditedImageUrl.Value.LastIndexOf("?"));

                //Load the Image from the Web Resource
                System.Drawing.Image image = Bitmap.FromStream(GetWebResponse(imgUrl));

                //Create a new image from the specified location to                
                //specified height and width                
                Bitmap bmp = new Bitmap(w, h, image.PixelFormat);
                Graphics g = Graphics.FromImage(bmp);
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.DrawImage(image, new Rectangle(0, 0, w, h));

                //Save the file and reload to the control
                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, image.RawFormat);
                ms.Position = 0;

                RequestManager.Services.BlobService.InitContainer(RequestManager.Services.SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersProperties));
                string fileName = RequestManager.Services.BlobService.AddFile(ms, PropertyID.Value, Path.GetFileName(imgUrl));
                btnCancel.Visible = false;
                btnOk.Visible = true;
                cropbox.Visible = true;
                //Resize.Visible = true;
                cropbox.Src = string.Format("{0}/{1}?ver={2}", RequestManager.Services.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray()), fileName.TrimStart("/".ToCharArray()), DateTime.Now.Ticks.ToString());
                Submit.Attributes.Add("style", "display: none;");
                Resize.Attributes.Add("style", "display: none;");
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                if (X.Value != "" && X.Value != "NaN")
                {
                    //Get the Cordinates                
                    int x = (int)float.Parse(TrimString(X.Value));
                    int y = (int)float.Parse(TrimString(Y.Value));
                    int w = (int)float.Parse(TrimString(W.Value));
                    int h = (int)float.Parse(TrimString(H.Value));

                    string imgUrl = EditedImageUrl.Value.Substring(0, EditedImageUrl.Value.LastIndexOf("?"));

                    //Load the Image from the Web Resource
                    System.Drawing.Image image = Bitmap.FromStream(GetWebResponse(imgUrl));

                    //Create a new image from the specified location to                
                    //specified height and width                
                    Bitmap bmp = new Bitmap(w, h, image.PixelFormat);
                    Graphics g = Graphics.FromImage(bmp);
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.DrawImage(image, new Rectangle(0, 0, w, h), new Rectangle(x, y, w, h), GraphicsUnit.Pixel);

                    //Save the file and reload to the control
                    MemoryStream ms = new MemoryStream();
                    bmp.Save(ms, image.RawFormat); 
                    ms.Position = 0 ;

                    RequestManager.Services.BlobService.InitContainer(RequestManager.Services.SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersProperties));
                    string fileName = RequestManager.Services.BlobService.AddFile(ms, PropertyID.Value, Path.GetFileName(imgUrl));
                    btnCancel.Visible = false;
                    btnOk.Visible = true;
                    cropbox.Visible = true;
                    cropbox.Src = string.Format("{0}/{1}?ver={2}", RequestManager.Services.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray()), fileName.TrimStart("/".ToCharArray()), DateTime.Now.Ticks.ToString());
                    Submit.Attributes.Add("style", "display: none;");
                    Resize.Attributes.Add("style", "display: none;");
                }
            }
        }

        private Stream GetWebResponse(string imgUrl)
        {
            // Create a 'WebRequest' object with the specified url. 
            WebRequest webRequest = WebRequest.Create(imgUrl);

            // Send the 'WebRequest' and wait for response.
            WebResponse webResponse = webRequest.GetResponse();

            // Obtain a 'Stream' object associated with the response object.
            Stream receiveStream = webResponse.GetResponseStream();

            return receiveStream;
        }

        #endregion Event Handlers
    }
}