﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using DataAccess;
using BusinessLogic.Managers;
using DataAccess.Enums;
using BusinessLogic.Objects;
using Common.Serialization;
using Common.ImageProcessing;
using System.IO;
using System.Drawing.Imaging;
using System.Web.UI.HtmlControls;
using BusinessLogic.Enums;
using IntranetApp.Helpers;
using BusinessLogic.Objects.StaticContentRepresentation;

namespace IntranetApp.Controls.Properties.StaticContent
{
    public partial class PropertyFloorPlan : System.Web.UI.UserControl
    {
        #region Properties

        public int? PropertyID
        {
            get { return ViewState["PropertyID"] as int?; }
            set { ViewState["PropertyID"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.FloorPlansLinqDS.DataResolver = () =>
            {
                IQueryable<PropertyStaticContent> result =
                    RequestManager.Services.PropertiesService
                        .GetPropertyStaticContentByType(this.PropertyID.Value,
                            new List<PropertyStaticContentType> { PropertyStaticContentType.FloorPlan })
                        .AsQueryable();

                return result.ToList();
            };
        }

        private void BindControls()
        {
            if (this.PropertyID.HasValue)
                this.rauFloorPlanUpload.HttpHandlerUrl = string.Format("~/Handlers/CustomHandler.ashx?pid={0}&fp=1&", PropertyID.Value.ToString());
            rauFloorPlanUpload.MaxFileSize = int.Parse(RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.MaxFileSize));
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
            BindControls();
        }

        protected void rgFloorPlans_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgFloorPlans.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgFloorPlans.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgFloorPlans.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgFloorPlans.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgFloorPlans.MasterTableView.Rebind();
            }
        }

        protected void rgFloorPlans_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.DeleteCommandName)
            {
                if (this.PropertyID.HasValue)
                    RequestManager.Services.PropertiesService.DeletePropertyFloorPlan(int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyStaticContentId"].ToString()));
            }
            RequestManager.SaveChanges();
        }

        protected void rgFloorPlans_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                PropertyStaticContent obj = (PropertyStaticContent)item.DataItem;
                FloorPlan fp = SerializationHelper.DeserializeObject<FloorPlan>(obj.ContentValue);

                e.Item.Attributes["floor-plan-id"] = obj.PropertyStaticContentId.ToString();

                //setting original image url
                HtmlAnchor ctrl = item.FindControl("imgOrigLink") as HtmlAnchor;
                ctrl.HRef = PropertyFloorPlanHelper.GetFloorPlanImageAbsolutePath(fp.OriginalImagePath);
                ctrl.InnerText = Path.GetFileName(ctrl.HRef.Replace("/", "\\"));
                item["originalImageAbsolutePath"].Text = ctrl.InnerText;
                ctrl.HRef = "";

                //setting display image url
                HtmlAnchor ctrlModified = item.FindControl("imgDispLink") as HtmlAnchor;
                ctrlModified.HRef = PropertyFloorPlanHelper.GetFloorPlanImageAbsolutePath(fp.DisplayImagePath);
                ctrlModified.InnerText = Path.GetFileName(ctrlModified.HRef.Replace("/", "\\"));
                item["displayImageAbsolutePath"].Text = ctrlModified.InnerText;
                ctrlModified.HRef = "";
                
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;",
                    rgFloorPlans.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"),
                        (string)GetGlobalResourceObject("Controls", "Object_Image"), fp.FloorPlanName.ToString()),
                    String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"),
                        (string)GetGlobalResourceObject("Controls", "Object_Image"), fp.FloorPlanName.ToString()));

                item["Name"].Text = fp.FloorPlanName.ToString();

                System.Web.UI.WebControls.Image imgOriginal = (System.Web.UI.WebControls.Image)item.FindControl("imageOriginalThumbnail");
                imgOriginal.ImageUrl = PropertyFloorPlanHelper.GetFloorPlanImageAbsolutePath(fp.OriginalImagePath);
                imgOriginal.Width = 50;
                imgOriginal.Attributes["onclick"] = string.Format("openImageDialog('{0}')", imgOriginal.ImageUrl);

                System.Web.UI.WebControls.Image imgModified = (System.Web.UI.WebControls.Image)item.FindControl("imageModifiedThumbnail");
                imgModified.ImageUrl = PropertyFloorPlanHelper.GetFloorPlanImageAbsolutePath(fp.DisplayImagePath);
                imgModified.Width = 50;
                imgModified.Attributes["onclick"] = string.Format("openImageDialog('{0}')", imgModified.ImageUrl);
            }
        }

        protected void rauFloorPlanUpload_FileUploaded(object sender, EventArgs e)
        {
            if (!PropertyID.HasValue || !Page.IsValid)
                return;

            var uploadedFile = this.rauFloorPlanUpload.UploadedFiles[0];
            if (uploadedFile != null)
            {
                var convertetFloorPlanStoragePath = 
                    PropertyFloorPlanHelper.UploadConvertedFloorPlan(PropertyID.Value, uploadedFile.FileName);

                FloorPlan floorPlanToAdd =  
                    new FloorPlan()
                        {
                            OriginalImagePath = uploadedFile.FileName,
                            DisplayImagePath = convertetFloorPlanStoragePath
                        };
                floorPlanToAdd.SetFloorPlanNameValue(rtxtFloorPlanName.Text);

                RequestManager.Services.PropertiesService.AddPropertyFloorPlan(this.PropertyID.Value, floorPlanToAdd);
            }

            this.BindGrid();
            this.rgFloorPlans.MasterTableView.Rebind();
        }

        protected void cvFloorPlanExists_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!PropertyID.HasValue)
                return;

            string chosenName = args.Value.ToLower();
            var floorPlans = RequestManager.Services.PropertiesService.GetPropertyFloorPlans(PropertyID.Value);

            args.IsValid = floorPlans.All(fp => fp.FloorPlanName.ToString(CultureCode.en_US).ToLower() != chosenName);
        }

        #endregion
    }
}