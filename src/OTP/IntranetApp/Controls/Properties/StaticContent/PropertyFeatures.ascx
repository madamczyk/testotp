﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyFeatures.ascx.cs" Inherits="IntranetApp.Controls.Properties.PropertyFeatures" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="rgFeatures">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgFeatures" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Label runat="server" ID="lblFeatureGuide" meta:resourceKey="lblFeaturesExample" />
<br /><br />
<asp:Panel runat="server" ID="pnlData1">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>
    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgFeatures" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="false" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="FeaturesLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgFeatures_ItemCommand" 
        OnItemDataBound="rgFeatures_ItemDataBound" 
        OnDataBound="rgFeatures_DataBound"
        OnUpdateCommand="rgFeatures_UpdateCommand"
        OnInsertCommand="rgFeatures_InsertCommand" Width="100%"
        ClientSettings-ClientEvents-OnColumnClick="disableWarning">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="PropertyStaticContentId" DataSourceID="FeaturesLinqDS" CommandItemDisplay="Bottom">
			<Columns>
				<telerik:GridEditCommandColumn UniqueName="EditCommandColumn" HeaderText=" " HeaderStyle-Width="10px" >
                    <ItemStyle Width="10px"></ItemStyle>
                </telerik:GridEditCommandColumn>
                <telerik:GridBoundColumn DataField="ContentValueCurrentLanguage" FilterControlWidth="150px" UniqueName="ContentValueCurrentLanguage" HeaderText="Content" SortExpression="ContentValueCurrentLanguage" HeaderStyle-Width="300px" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>                
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>			
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="ContentValueCurrentLanguage" SortOrder="Ascending" />
            </SortExpressions>		
            <EditFormSettings EditFormType="Template">
                    <FormTemplate>
                        <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="Features"
                            HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                            EnableClientScript="true" runat="server" CssClass="errorInForm" />
                        <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                            <tr>
                                <td class="paramLabelCell" style='padding-top: 10px'>
                                    <asp:Label runat="server" ID="lblContent" meta:resourceKey="lblContent"></asp:Label>
                                </td>
                                <td class="paramInputCell" style='padding-top: 10px'>
                                    <telerik:RadTextBox Width="350px" runat="server" ID="txtFeature" TextMode="SingleLine" 
                                        Text='<%# Eval("ContentValueCurrentLanguage") %>'>
                                    </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="Features" ID="RequiredFieldValidator7"
                                        Text="*" ControlToValidate="txtFeature" runat="server" Display="Static"
                                        CssClass="error" meta:resourceKey="ValContent"></asp:RequiredFieldValidator>
                                </td>
                            </tr>                             
                            <tr>
                                <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                    <div style="position: relative; width: 415px;">
                                        <!-- workaround for issue with RadButtons not being aligned vertically -->
                                        <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="Features"
                                            VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                            CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                            CausesValidation="true">
                                        </telerik:RadButton>
                                        <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                        </telerik:RadButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </FormTemplate>
                </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

</asp:Panel>

<otpDS:SimpleDataSource ID="FeaturesLinqDS" runat="server" />