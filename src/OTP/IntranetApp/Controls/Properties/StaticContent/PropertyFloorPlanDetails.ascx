﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyFloorPlanDetails.ascx.cs" Inherits="IntranetApp.Controls.Properties.StaticContent.PropertyFloorPlanDetails" %>

<link href="../../../Content/Styles/Site.css" rel="stylesheet" type="text/css" />
<link href="../../../Content/Styles/main.css" rel="stylesheet" type="text/css" />
<link href="../../../Content/Styles/loginPage.css" type="text/css" rel="stylesheet" />
<link href="../../../Content/Styles/Forms.css" type="text/css" rel="stylesheet" />
<link href="../../../Content/Styles/propertyFloorPlanDetails.css" type="text/css" rel="stylesheet" />

<script type="text/javascript" src="../../../Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../../../Scripts/jquery-ui-1.8.20.min.js"></script>
<script type="text/javascript" src="../../../Scripts/Pages/propertyFloorPlanDetails.js"></script>

<style>
    html {
        overflow: hidden;
    }
    .ruFileWrap {
        display: inline-block !important;
}
</style>

<script type="text/javascript">
    $(document).ready(function () {

        var panoramas = new Array();
        <% if (FloorPlanData.Panoramas != null)
           { %>
                <% foreach (var panorama in FloorPlanData.Panoramas)
                   { %>

        panoramas.push(
            {
                id: <%= panorama.Id %>,
                        offsetX: <%= panorama.OffsetX %>,
                        offsetY: <%= panorama.OffsetY %>, 
                        panoramaPath: "<%= panorama.PanoramaPath %>" 
                    });

        <% } %>
        <% } %>

        oth.propertyFloorPlanDetails.init(
            {
                imageDivContainterSelector: "#imageContainer",
                imageControlSelector: "#<%= FloorPlanImg.ClientID %>",
                hfPanoramaCordXSelector: "#<%= hfPanoramaCordX.ClientID %>",
                hfPanoramaCordYSelector: "#<%= hfPanoramaCordY.ClientID %>",
                hfPanoramaSelectedIDSelector: "#<%= hfPanoramaSelectedID.ClientID %>",
                btnShowPanoramaFormSelector: "#<%= btnShowPanoramaForm.ClientID %>",
                btnEditPanoramaFormSelector: "#<%= btnEditPanoramaForm.ClientID %>",
                btnEditNewPanoramaFormSelector: "#<%= btnEditHotSpot.ClientID %>",
                hfNewPanoramaSelectedIDSelector: "#<%= hfNewPanoramaSelectedID.ClientID %>",
                controlMode: "<%= hfFormMode.Value %>",
                panoramas: panoramas
            });
    });

    function GetRadWindow()  
    {  
        var oWindow = null;  
        if (window.radWindow) oWindow = window.radWindow;  
        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;  
        return oWindow;  
    }  
 
    function CloseWindow()  
    {  
        var oWindow = GetRadWindow();  
        oWindow.close();  
    }

    function cancelCallbackFn(args){
        if(args)
        {
            $find("#<%= btnEditPanoramaForm.ClientID %>").click();  
        }
    }
    function cancelCallbackFn(args){
        if(args)
        {
            $(senderName).click();  
            senderName = null;
        }
    }

    function cancelMainCallbackFn(args){
        if(args)
        {
            CloseWindow();
        }
    }

    function ConfirmCancel(sender, args)
    {
        senderName = sender;
        args.set_cancel(true);
        return radconfirm('Are you sure you want to cancel?', cancelCallbackFn, 250, 150, '', 'Cancel');
        
    }

    function ConfirmDelete(sender, args)
    {
        senderName = sender;
        args.set_cancel(true);
        return radconfirm('Are you sure you want to delete?', cancelCallbackFn, 250, 150, '', 'Delete');        
    }

    function ConfirmCancelMainWindow(sender, args)
    {
        args.set_cancel(true);
        return radconfirm('Are you sure you want to cancel?', cancelMainCallbackFn, 250, 150, '', 'Cancel');
    }

    var senderName;
    var uploadClientSt = '#CtrlPropertyFloorPlanDetails_rwEditPanorama_C_rauPanoramaUpload_ClientState';

    function validateNewUploadedFiles(sender, args) 
    {
        if($("#<%= rbAddNewPanorama.ClientID %>").is(':checked')) 
        {
            var isValid = true;
            var upload = $find("<%= rauPanoramaUpload.ClientID %>");
            ///var selItems = upload.getUploadedFiles().length != 0;
            var uploaded = $(uploadClientSt).val();
            var elements = getImageInfo(uploaded);
            
            if (elements.length == 0)
            {
                isValid = false;
            }
            else{
                var parts = elements[0].split("|");
                parts[0] = parseInt(parts[0]);
                parts[1] = parseInt(parts[1]);
                parts[2] = parseInt(parts[2]);
                parts[3] = parseInt(parts[3]);
                if(parts[0] == 0)
                {
                    //not valid extension
                    isValid = false;
                    sender.errormessage = "Only zip file is allowed";
                }
                if(parts[1] == 0 || parts[2] == 0)
                {
                    //not valid dir structure
                    isValid = false;
                    sender.errormessage = "Directory structure is not valid. See description below";
                }
                if (parts[3] == 0)
                {
                    isValid = false;
                    sender.errormessage = "Panorama with chosen name already exists";
                }
            }
            args.IsValid = isValid;
        }
        else
        {
            args.IsValid = true;
        }
    }

    function validateExistingPanorama(sender, args)
    {
        if($("#<%= rbAddNewPanorama.ClientID %>").is(':checked')) 
        {
            
        }
    }

    function getImageInfo(str) {
        var results = [], re = /#([^}]+)!/g, text;

        while (text = re.exec(str)) {
            results.push(text[1]);
        }
        return results;
    }

    function FileUploadFailedPanorama(sender, args) {
        var errorMessage = "File exceeds the size limit of 10 MB";
        if (args.get_fileName() != null && args.get_fileName() != "") {        
            var addedFileName = args.get_fileName();
            var fileExt = addedFileName.substr(addedFileName.lastIndexOf('.') + 1);       

            if ($.inArray(fileExt, sender._allowedFileExtensions) == -1)
            {
                errorMessage = "File of this type is not supported as a image";
            }
        }

        args.get_row().children[0].innerHTML = "<span class='ruUploadFailure failureFiles'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>" + errorMessage + "</span></span>";
    }

    //Telerik.Web.UI.RadAsyncUpload.Modules.Silverlight.isAvailable = function () { return false; };
    //Telerik.Web.UI.RadAsyncUpload.Modules.Flash.isAvailable = function () { return false; };
</script>

<form id="PropertyFloorPlanDetailsForm" runat="server" autocomplete="off">
    <telerik:RadScriptManager ID="RadScriptManager" AsyncPostBackTimeout="3600" runat="server" />
    <telerik:RadAjaxManager ID="radAjaxManager" runat="server" />
    <telerik:RadWindowManager ID="rwmFloorPlan" runat="server">
        <Windows>
            <telerik:RadWindow ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Height="150px" Width="250px"  Behaviors="Move" Modal="true" VisibleStatusbar="false">
                <ContentTemplate>
                    <div style="margin: 20px;">
                        <div style="float: left; width: 240px; text-align: center;">
                            <asp:Label ID="lblConfirmation" Font-Bold="true" Text="<%$ Resources: Controls, Dialog_Discard_Text %>" runat="server"></asp:Label>
                            <br />
                            <br />
                            <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" ></asp:Button>
                            <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" ></asp:Button>
                        </div>
                        <div style="clear: both;">
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>

            <telerik:RadWindow OnClientBeforeClose="ConfirmCancelMainWindow" ID="rwEditPanorama" VisibleTitlebar="false" runat="server" VisibleOnPageLoad="false" Height="280px" Width="420px" Behaviors="Close, Move, Resize" Modal="true" VisibleStatusbar="false">
                <ContentTemplate>
                    <asp:ValidationSummary ID="vsSummary" ValidationGroup="AddPanorama" HeaderText=" "
                        DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <div class="content" id="panoramaEditor" runat="server">
                        <% if (hfFormMode.Value == PropertyFloorDetailsModes.AddPanorama)
                            { %>
                        <h1>Add Panorama</h1>
                        <% }
                            else
                            { %>
                        <h1>Edit Panorama</h1>
                        <% } %> 
                        <table>
                            <tr>
                                <td><asp:Label CssClass="panoramaEditor" ID="Label1" Text="Panorama name" runat="server" /></td>
                                <td><asp:TextBox ID="txtPanoramaName" runat="server" />
                                    <asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator1"
                                        ValidationGroup="AddPanorama"
                                        runat="server"
                                        Display="Dynamic"
                                        ControlToValidate="txtPanoramaName"
                                        ErrorMessage="Panorama name is required"
                                        Text="*"
                                        CssClass="error" />
                                    <asp:CustomValidator
                                        ID="CustomValidator1"
                                        ValidationGroup="AddPanorama"
                                        runat="server"
                                        Display="Dynamic"
                                        ControlToValidate="txtPanoramaName"
                                        ErrorMessage="Panorama with chosen name already exists"
                                        Text="*"
                                        OnServerValidate="CustomValidator1_ServerValidate"
                                        CssClass="error" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton GroupName="PanoramaRadionButton" CssClass="panoramaEditor" ID="rbAddNewPanorama" runat="server" Text="Add new panorama" />
                                </td>
                                <td>
                                    <telerik:RadAsyncUpload runat="server" ID="rauPanoramaUpload" MultipleFileSelection="Disabled"
                                        InitialFileInputsCount="1" MaxFileInputsCount="1" AllowedFileExtensions="zip" OnClientValidationFailed="FileUploadFailedPanorama"/>
                                    <asp:CustomValidator runat="server" Display="Dynamic" ID="cvPanoramaUpload" ValidationGroup="AddPanorama" 
                                        ClientValidationFunction="validateNewUploadedFiles" Text="*" ErrorMessage="Select zip file with panorama" CssClass="error" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton GroupName="PanoramaRadionButton" CssClass="panoramaEditor" ID="rbAddExisting" runat="server" Text="Select existing panorama" />
                                </td>
                                <td>
                                     <telerik:RadComboBox CssClass="panoramaEditor" runat="server" ID="rcbPanoramas" Enabled="true" AutoPostBack="false" />
                                    <asp:CustomValidator runat="server" Display="Dynamic" ID="cvExistingPanorama" ValidationGroup="AddPanorama" 
                                        ClientValidationFunction="validateExistingPanorama" Text="*" ErrorMessage="Select existing panorama" CssClass="error">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>                                   
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 80px;">
                                    <asp:Label CssClass="panoramaEditor" runat="server" ID="lblZipStructure" Text="The file to upload need to be a zip file which includes the main directory with the following:<br />'Lib' directory, 'Media' directory, 'index.htm' file, 'scripts.js' file" />                               
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="SavePanoramaContainer" style="text-align:center;">
                                        <telerik:RadButton ID="btnPanoramaSave" Text="Save" OnClick="btnSavePanorama_Click" CausesValidation="True" ValidationGroup="AddPanorama" runat="server" />
                                        <% if (hfFormMode.Value == PropertyFloorDetailsModes.EditPanorama)
                                            { %>
                                        <telerik:RadButton ID="btnDeletePanorama" Text="Delete" OnClick="btnDeletePanorama_Click" runat="server" OnClientClicking="ConfirmDelete" />
                                        <% } %>
                                        <telerik:RadButton ID="btnPanoramaCancel" Text="Cancel" OnClick="btnCancelPanorama_Click" runat="server" OnClientClicking="ConfirmCancel" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="content" id="panoramaConfirmation" runat="server" visible="false">
                        <table>
                            <tr>
                                <td><asp:Label CssClass="panoramaEditor" ID="Label2" Text="Panorama successfully uploaded" runat="server" /></td>
                            </tr>
                            <tr>
                                <td><asp:HyperLink Font-Size="Small" Target="_blank" Text="Open Uploaded Panorama" runat="server" ID="hlPanoramaLink" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnConfirmationClick" Text="Close" runat="server" OnClick="btnConfirmationClick_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <!-- Hidden fields -->
    <asp:HiddenField ID="hfFormMode" runat="server" />
    <asp:HiddenField ID="hfAddPanoramaMode" runat="server" />
    <asp:HiddenField ID="hfPanoramaSelectedID" runat="server" />
    <asp:HiddenField ID="hfNewPanoramaSelectedID" runat="server" />
    <asp:HiddenField ID="hfPanoramaCordX" runat="server" />
    <asp:HiddenField ID="hfPanoramaCordY" runat="server" />

    <asp:Button ID="btnShowPanoramaForm" Style="display: none" runat="server" OnClick="btnShowPanoramaForm_Click" />
    <asp:Button ID="btnEditPanoramaForm" Style="display: none" runat="server" OnClick="btnEditPanoramaForm_Click" />
    <asp:Button ID="btnEditHotSpot" Style="display: none" runat="server" OnClick="btnEditHotSpot_Click" />


    <div id="floorPlanControl">
        <div id="imageContainer">
            <asp:Image ID="FloorPlanImg" Width="640" Height="480" runat="server" />
            <br />
            <asp:Label runat="server" Text="If you want to put 360 panorama into the floor plan, move your mouse there and click" ID="lblPanoramaInfo" />
        </div>
    </div>
    <div id="floorPlanEditForm">
        <asp:Panel ID="pnlfloorPlanProperties" runat="server">
            <asp:Label ID="lblFloorPlanName" AssociatedControlID="txtFloorPlanName" Text="Floor Plan Name" runat="server" />
            <asp:ValidationSummary ID="vsSummaryRawData" ValidationGroup="FloorPlanProperty" DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
            <br />
            <asp:TextBox ID="txtFloorPlanName" runat="server" />
            <asp:RequiredFieldValidator
                ID="rfqFloorPlanName"
                ValidationGroup="FloorPlanProperty"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtFloorPlanName"
                Text="*"
                ErrorMessage="Floor Plan name is required"
                CssClass="error" />
            <asp:CustomValidator
                ID="cvFloorPlanExists"
                ValidationGroup="FloorPlanProperty"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtFloorPlanName"
                Text="*"
                ErrorMessage="Floor Plan with chosen name already exists"
                OnServerValidate="cvFloorPlanExists_ServerValidate"
                CssClass="error" />
            <br />

            <div id="SavePropertyContainer">
                <telerik:RadButton ID="btnPropertySave" Text="Save" OnClick="btnSaveProperty_Click" CausesValidation="true" ValidationGroup="FloorPlanProperty" runat="server" />
                <telerik:RadButton ID="btnShowPanoramaEditor" Text="Cancel" CausesValidation="false" runat="server" OnClientClicking="ConfirmCancelMainWindow" />
            </div>
        </asp:Panel>

        
    </div>

<%--    <otpDS:SimpleDataSource ID="PanoramasLinqDS" runat="server" />--%>
</form>
