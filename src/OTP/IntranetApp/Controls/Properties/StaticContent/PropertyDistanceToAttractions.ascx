﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyDistanceToAttractions.ascx.cs" Inherits="IntranetApp.Controls.Properties.PropertyDistanceToAttractions" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="rgAttractions">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgAttractions" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData1">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>
    
    <asp:Label runat="server" ID="lblDistanceGuide" meta:resourceKey="lblDistanceGuide" />
    <br /><br />
    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgAttractions" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="false" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="AttractionsLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgAttractions_ItemCommand" 
        OnItemDataBound="rgAttractions_ItemDataBound" 
        OnDataBound="rgAttractions_DataBound"
        OnUpdateCommand="rgAttractions_UpdateCommand"
        OnInsertCommand="rgAttractions_InsertCommand" Width="100%"
        ClientSettings-ClientEvents-OnColumnClick="disableWarning">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="PropertyStaticContentId" DataSourceID="AttractionsLinqDS" CommandItemDisplay="Bottom">
			<Columns>
				<telerik:GridEditCommandColumn UniqueName="EditCommandColumn" HeaderText=" " HeaderStyle-Width="10px" >
                    <ItemStyle Width="10px"></ItemStyle>
                </telerik:GridEditCommandColumn>
                <telerik:GridBoundColumn DataField="ContentValueCurrentLanguage" FilterControlWidth="150px" UniqueName="ContentValueCurrentLanguage" HeaderText="Content" SortExpression="ContentValueCurrentLanguage" HeaderStyle-Width="300px" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>                
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>			
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="ContentValueCurrentLanguage" SortOrder="Ascending" />
            </SortExpressions>		
            <EditFormSettings EditFormType="Template">
                    <FormTemplate>
                        <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="Attractions"
                            HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                            EnableClientScript="true" runat="server" CssClass="errorInForm" />
                        <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                            <tr>
                                <td class="paramLabelCell" style='padding-top: 10px'>
                                    <asp:Label runat="server" ID="lblAttractions" meta:resourceKey="lblAttractions"></asp:Label>
                                </td>
                                <td class="paramInputCell" style='padding-top: 10px'>
                                    <telerik:RadTextBox Width="350px" runat="server" ID="txtAttractions" TextMode="SingleLine" 
                                        Text='<%# Eval("ContentValueCurrentLanguage") %>'>
                                    </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="Attractions" ID="RequiredFieldValidator7"
                                        Text="*" ControlToValidate="txtAttractions" runat="server" Display="Static"
                                        CssClass="error" meta:resourceKey="ValDistance"></asp:RequiredFieldValidator>
                                </td>
                            </tr>                             
                            <tr>
                                <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                    <div style="position: relative; width: 415px;">
                                        <!-- workaround for issue with RadButtons not being aligned vertically -->
                                        <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="Attractions"
                                            VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                            CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                            CausesValidation="true">
                                        </telerik:RadButton>
                                        <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                        </telerik:RadButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </FormTemplate>
                </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>
</asp:Panel>

<otpDS:SimpleDataSource ID="AttractionsLinqDS" runat="server" />