﻿using BusinessLogic.Managers;
using BusinessLogic.Objects;
using Common.Serialization;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using BusinessLogic.Objects.StaticContentRepresentation;
using BusinessLogic.Enums;

namespace IntranetApp.Controls.Properties.StaticContent
{
    public partial class PropertyFloorPlanDetails : System.Web.UI.UserControl
    {
        #region Consts

        public static class PropertyFloorDetailsModes
        {
            public const string Default = "Default";
            public const string AddPanorama = "AddPanorama";
            public const string EditPanorama = "EditPanorama";
            public const string UploadConfirmation = "UploadConfirmation";
        }

        public static class AddPanoramaModes
        {
            public const string AddNew = "AddNew";
            public const string AddExisting = "AddExisting";
        }

        #endregion

        #region Properties

        public string CloseWindowScript
        {
            get
            {
                return "function GetRadWindow() {{" +
                    "var oWindow = null;" +
                    "if (window.radWindow) oWindow = window.radWindow;" +
                    "else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;" +
                    "return oWindow;" +
                    "}}";
            }
        }

        public int? PropertyID
        {
            get { return ViewState["PropertyID"] as int?; }
            set { ViewState["PropertyID"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemId"] as int?; }
            set { ViewState["EditedItemId"] = value; }
        }

        public FloorPlan FloorPlanData
        {
            get { return ViewState["FloorPlanData"] as FloorPlan; }
            set { ViewState["FloorPlanData"] = value; }
        }

        public string SelectedPanorama
        {
            get { return ViewState["SelectedPanorama"] as string  ; }
            set { ViewState["SelectedPanorama"] = value; }
        }

        #endregion

        #region Methods

        private void BindControls()
        {
            ParseQueryString();
            LoadPropertyStaticContent();
            txtFloorPlanName.Text = FloorPlanData != null ? FloorPlanData.FloorPlanName.ToString() : "";
            FloorPlanImg.ImageUrl = 
                PropertyFloorPlanHelper.GetFloorPlanImageAbsolutePath(FloorPlanData.DisplayImagePath);

            if (this.PropertyID.HasValue)
                this.rauPanoramaUpload.HttpHandlerUrl = string.Format("~/Handlers/CustomHandler.ashx?pid={0}&", PropertyID.Value.ToString());
            rauPanoramaUpload.MaxFileSize = int.Parse(RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.MaxFileSize));
            LoadPanoramas();            
        }

        private void LoadPanoramas()
        {
            IQueryable<PropertyStaticContent> result = RequestManager.Services.PropertiesService
                        .GetPropertyStaticContentByType(this.PropertyID.Value, new List<PropertyStaticContentType> { PropertyStaticContentType.Panorama }).AsQueryable();

            //if (!IsPostBack)
            {
                this.rcbPanoramas.Items.Clear();
                foreach (var p in result)
                {
                    string name = p.ContentValue.Substring(p.ContentValue.LastIndexOf("/") + 1,
                                                            p.ContentValue.Length - p.ContentValue.LastIndexOf("/") - 1);
                    string value = p.ContentValue;
                    rcbPanoramas.Items.Add(new RadComboBoxItem(name, value));
                }
                if (!string.IsNullOrWhiteSpace(SelectedPanorama))
                    this.rcbPanoramas.SelectedValue = SelectedPanorama;
            }
        }

        private void ParseQueryString()
        {
            int ID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out ID))
            {
                this.EditedItemId = ID;
            }
        }

        private void LoadPropertyStaticContent()
        {
            PropertyStaticContent psc = RequestManager.Services.PropertiesService.GetPropertyStaticContentById(EditedItemId.Value);
            if (psc == null || psc.ContentCode != Convert.ToInt32(PropertyStaticContentType.FloorPlan))
                throw new ArgumentNullException("Floor plan does not exists");

            PropertyID = psc.Property.PropertyID;
            if(!IsPostBack)
                FloorPlanData = SerializationHelper.DeserializeObject<FloorPlan>(psc.ContentValue);
        }

        private void SwitchDetailsMode(string propertyFloorPlanDetailsMode)
        {
            LoadPropertyStaticContent();            

            hfFormMode.Value = propertyFloorPlanDetailsMode;

            switch(propertyFloorPlanDetailsMode)
            {
                case PropertyFloorDetailsModes.EditPanorama:                    
                    var selectedPanoramaID = int.Parse(hfPanoramaSelectedID.Value);
                    var selectedPanorama = FloorPlanData.Panoramas.FirstOrDefault(p => p.Id == selectedPanoramaID);

                    SwitchAddPanoramaMode(AddPanoramaModes.AddExisting);

                    rbAddNewPanorama.Checked = true;
                    rbAddExisting.Checked = false;
                    rcbPanoramas.SelectedValue = selectedPanorama.PanoramaPath;
                    hfPanoramaCordX.Value = selectedPanorama.OffsetX.ToString();
                    hfPanoramaCordY.Value = selectedPanorama.OffsetY.ToString();
                    txtPanoramaName.Text = selectedPanorama.PanoramaName.ToString();
                    rbAddExisting.Checked = true;
                    rcbPanoramas.SelectedValue = selectedPanorama.PanoramaPath;
                    this.panoramaConfirmation.Visible = false;
                    this.panoramaEditor.Visible = true;
                    return;
                case PropertyFloorDetailsModes.AddPanorama:                    
                    SwitchAddPanoramaMode(AddPanoramaModes.AddNew);

                    rbAddNewPanorama.Checked = true;
                    rbAddExisting.Checked = false;
                    break;
                case PropertyFloorDetailsModes.Default:
                    ClearControlFields();
                    rbAddNewPanorama.Checked = true;
                    rbAddExisting.Checked = false;
                    this.panoramaConfirmation.Visible = false;
                    this.panoramaEditor.Visible = true;
                    break;
                case PropertyFloorDetailsModes.UploadConfirmation:
                    ClearControlFields();
                    this.panoramaConfirmation.Visible = true;
                    this.panoramaEditor.Visible = false;
                    break;
            }
        }

        private void SwitchAddPanoramaMode(string addPanoramaMode)
        {
            hfAddPanoramaMode.Value = addPanoramaMode;

            switch (addPanoramaMode)
            {
                case AddPanoramaModes.AddNew:
                    rbAddNewPanorama.Checked = true;
                    rbAddExisting.Checked = false;
                    break;
                case AddPanoramaModes.AddExisting:
                    rbAddNewPanorama.Checked = false;
                    rbAddExisting.Checked = true;
                    break;
            }
        }

        private void ClearControlFields()
        {
            hfPanoramaCordX.Value = String.Empty;
            hfPanoramaCordY.Value = String.Empty;
            hfPanoramaSelectedID.Value = String.Empty;
            txtPanoramaName.Text = String.Empty;
        }

        #endregion

        #region EventHandlers

        protected void Page_Load(object sender, EventArgs e)
        {
            SelectedPanorama = this.rcbPanoramas.SelectedValue;
            BindControls();
            if (!Page.IsPostBack)
            {
                ParseQueryString();

                SwitchDetailsMode(PropertyFloorDetailsModes.Default);
                SwitchAddPanoramaMode(AddPanoramaModes.AddNew);                
            }
            this.rbAddExisting.Enabled = rcbPanoramas.Items.Count != 0;
        }

        protected void btnShowPanoramaForm_Click(object sender, EventArgs e)
        {
            SwitchDetailsMode(PropertyFloorDetailsModes.AddPanorama);
            this.rwEditPanorama.VisibleOnPageLoad = true;
        }

        protected void btnEditPanoramaForm_Click(object sender, EventArgs e)
        {
            var panoramaID = int.Parse(hfPanoramaSelectedID.Value);
            var selectedPanorama = FloorPlanData.Panoramas.FirstOrDefault(p => p.Id == panoramaID);
            selectedPanorama.OffsetX = Convert.ToInt32(!string.IsNullOrWhiteSpace(hfPanoramaCordX.Value) ? hfPanoramaCordX.Value : selectedPanorama.OffsetX.ToString());
            selectedPanorama.OffsetY = Convert.ToInt32(!string.IsNullOrWhiteSpace(hfPanoramaCordY.Value) ? hfPanoramaCordY.Value : selectedPanorama.OffsetY.ToString());

            SwitchDetailsMode(PropertyFloorDetailsModes.EditPanorama);
            this.rwEditPanorama.VisibleOnPageLoad = true;
        }

        protected void btnShowPanoramaEditor_Click(object sender, EventArgs e)
        {
            this.rwEditPanorama.VisibleOnPageLoad = true;
        }

        protected void btnEditHotSpot_Click(object sender, EventArgs e)
        {
            //save current panorama position
            var panoramaID = int.Parse(hfPanoramaSelectedID.Value);
            var selectedPanorama = FloorPlanData.Panoramas.FirstOrDefault(p => p.Id == panoramaID);
            GetPanoramaPosition(selectedPanorama);

            //load new position
            panoramaID = int.Parse(hfNewPanoramaSelectedID.Value);
            selectedPanorama = FloorPlanData.Panoramas.FirstOrDefault(p => p.Id == panoramaID);
            hfPanoramaCordX.Value = selectedPanorama.OffsetX.ToString();
            hfPanoramaCordY.Value = selectedPanorama.OffsetY.ToString();

            this.hfPanoramaSelectedID.Value = this.hfNewPanoramaSelectedID.Value;
        }

        protected void btnSaveProperty_Click(object sender, EventArgs e)
        {
            if (hfPanoramaSelectedID.Value != "")
            {
                var panoramaID = int.Parse(hfPanoramaSelectedID.Value);
                var selectedPanorama = FloorPlanData.Panoramas.FirstOrDefault(p => p.Id == panoramaID);
                GetPanoramaPosition(selectedPanorama);
            }
            if (!PropertyID.HasValue || !EditedItemId.HasValue || !Page.IsValid)
                return;

            FloorPlanData.SetFloorPlanNameValue(txtFloorPlanName.Text);
            RequestManager.Services.PropertiesService.EditPropertyFloorPlan(EditedItemId.Value, FloorPlanData);
            string script = "<script language=JavaScript> " + CloseWindowScript + " GetRadWindow().close(); </script>";
            Response.Write(script);
        }       


        protected void btnSavePanorama_Click(object sender, EventArgs e)
        {
            if (!PropertyID.HasValue || !Page.IsValid)
                return;

            string panoramaStoragePath = String.Empty;
            if (rbAddNewPanorama.Checked && this.rauPanoramaUpload.UploadedFiles[0] != null)
            {
                panoramaStoragePath = this.rauPanoramaUpload.UploadedFiles[0].FileName;
                PropertyFloorPlanHelper.AddPanorama(PropertyID.Value, panoramaStoragePath);
            }
            else if (rbAddExisting.Checked)
            {
                panoramaStoragePath = rcbPanoramas.SelectedValue;
            }
            else
            {
                return;
            }

            if (hfFormMode.Value == PropertyFloorDetailsModes.AddPanorama)
            {
                //PropertyFloorPlanHelper.AddPanorama(PropertyID.Value, panoramaStoragePath);
                Panorama pano = new Panorama()
                    {
                        Id = FloorPlanData.Panoramas == null ? 1 : FloorPlanData.Panoramas.Count() + 1,
                        PanoramaPath = panoramaStoragePath,
                        OffsetX = Convert.ToInt32(hfPanoramaCordX.Value),
                        OffsetY = Convert.ToInt32(hfPanoramaCordY.Value),
                    };
                pano.SetPanoramaNameValue(txtPanoramaName.Text);
                var panoramas = FloorPlanData.Panoramas == null ? new List<Panorama>() : FloorPlanData.Panoramas.ToList();
                panoramas.Add(pano);
                FloorPlanData.Panoramas = panoramas.ToArray();                    
            }
            else if(hfFormMode.Value == PropertyFloorDetailsModes.EditPanorama)
            {
                var panoramaID = int.Parse(hfPanoramaSelectedID.Value);
                var selectedPanorama = FloorPlanData.Panoramas.FirstOrDefault(p => p.Id == panoramaID);

                Panorama pano = new Panorama()
                    {
                        Id = int.Parse(hfPanoramaSelectedID.Value),
                        PanoramaPath = panoramaStoragePath
                        //OffsetX = Convert.ToInt32(hfPanoramaCordX.Value),
                        //OffsetY = Convert.ToInt32(hfPanoramaCordY.Value)
                    };
                GetPanoramaPosition(pano);

                pano.SetPanoramaNameValue(txtPanoramaName.Text);

                List<Panorama> Panoramas = FloorPlanData.Panoramas.ToList();
                Panoramas.Remove(selectedPanorama);
                Panoramas.Add(pano);
                FloorPlanData.Panoramas = Panoramas.ToArray();
            }
            hlPanoramaLink.NavigateUrl = PropertyFloorPlanHelper.GetFloorPlanImageAbsolutePath(panoramaStoragePath) + "/index.htm";
            SwitchDetailsMode(PropertyFloorDetailsModes.UploadConfirmation);
        }

        private void GetPanoramaPosition(Panorama pano)
        {
            pano.OffsetX = Convert.ToInt32(!string.IsNullOrWhiteSpace(hfPanoramaCordX.Value) ? hfPanoramaCordX.Value : pano.OffsetX.ToString());
            pano.OffsetY = Convert.ToInt32(!string.IsNullOrWhiteSpace(hfPanoramaCordY.Value) ? hfPanoramaCordY.Value : pano.OffsetY.ToString());
        }

        protected void btnDeletePanorama_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(hfPanoramaSelectedID.Value) || !EditedItemId.HasValue)
                return;

            List<Panorama> Panoramas = FloorPlanData.Panoramas.ToList();
            Panoramas.Remove(Panoramas.Where(p => p.Id == int.Parse(hfPanoramaSelectedID.Value)).SingleOrDefault());
            FloorPlanData.Panoramas = Panoramas.ToArray();
            SwitchDetailsMode(PropertyFloorDetailsModes.Default);

            this.rwEditPanorama.VisibleOnPageLoad = false;
        }

        protected void btnCancelPanorama_Click(object sender, EventArgs e)
        {
            SwitchDetailsMode(PropertyFloorDetailsModes.Default);
            this.rwEditPanorama.VisibleOnPageLoad = false;
        }

        protected void btnConfirmationClick_Click(object sender, EventArgs e)
        {
            SwitchDetailsMode(PropertyFloorDetailsModes.Default);
            this.rwEditPanorama.VisibleOnPageLoad = false;
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string chosenName = args.Value.ToLower();

            if (hfFormMode.Value == PropertyFloorDetailsModes.AddPanorama)
            {
                if ( FloorPlanData.Panoramas != null )
                    args.IsValid = FloorPlanData.Panoramas.All(p => p.PanoramaName.ToString(CultureCode.en_US).ToLower() != chosenName);
            }
            else if (hfFormMode.Value == PropertyFloorDetailsModes.EditPanorama)
            {
                var panoramaID = int.Parse(hfPanoramaSelectedID.Value);

                if (FloorPlanData.Panoramas != null)
                    args.IsValid = FloorPlanData.Panoramas.Where(p => p.Id != panoramaID).All(p => p.PanoramaName.ToString(CultureCode.en_US).ToLower() != chosenName);
            }
        }

        protected void cvFloorPlanExists_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!PropertyID.HasValue)
                return;

            string chosenName = args.Value.ToLower();
            var floorPlans = RequestManager.Services.PropertiesService.GetPropertyFloorPlans(PropertyID.Value);

            args.IsValid = floorPlans.Where(fp => fp.FloorPlanName.ToString(CultureCode.en_US).ToLower() != FloorPlanData.FloorPlanName.ToString(CultureCode.en_US).ToLower()).All(fp => fp.FloorPlanName.ToString(CultureCode.en_US).ToLower() != chosenName);
        }

        #endregion
    }
}