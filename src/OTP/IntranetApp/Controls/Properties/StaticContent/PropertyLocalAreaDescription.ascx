﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyLocalAreaDescription.ascx.cs" Inherits="IntranetApp.Controls.Properties.PropertyLocalAreaDescription" %>

<script type="text/javascript">
    function LocalAreaDescrValidationMarkEvent() {
        var obj = $find('<%= txtLocalAreaDesc.ClientID %>');
        MarkInvalid(obj)
        SelectTab("Local Area Description");
    }
</script>

<asp:Label runat="server" ID="lblDistanceGuide" meta:resourceKey="lblDistanceGuide" />
    <br /><br />
<table>
    <tr>
        <td width="100px">
            <asp:Label runat="server" ID="lblLocalAreaDescription" meta:resourcekey="lblLocalAreaDescription" />
        </td>
        <td width="500px">
            <telerik:RadTextBox runat="server" ID="txtLocalAreaDesc" TextMode="MultiLine" Rows="6" TabIndex="9" Width="450px" localArea="0" />
        </td>
    </tr>
</table>
