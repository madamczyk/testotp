﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertySquare.ascx.cs" Inherits="IntranetApp.Controls.Properties.PropertySquare" %>


<script type="text/javascript">
    function SquareFootageValidationMarkEvent() {
        var obj = $find('<%= txtPropertySquare.ClientID %>');   
        MarkInvalid(obj)
        SelectTab("Square footage");
    }
</script>

<asp:Label runat="server" ID="lblSquareFootageGuide" meta:resourceKey="lblSquareFootageGuide" />
<br /><br />
<table>
    <tr>
        <td width="100px">
            <asp:Label runat="server" ID="lblSquareFootage" meta:resourcekey="lblSquareFootage" />
        </td>
        <td width="500px">
            <telerik:RadTextBox runat="server" ID="txtPropertySquare" TextMode="SingleLine" TabIndex="9" Width="250px" squareFootage="0" />
            <asp:RequiredFieldValidator
                Text="*"
                ErrorMessage="Square Footage is required <a href='javascript:SelectTabSquareFootage()'> (Select tab)</a>"
                ControlToValidate="txtPropertySquare"
                ID="rfvSquareFootage"
                runat="server"
                ValidationGroup="Property"
                CssClass="error"
                Display="Dynamic"  SetFocusOnError="true" />
        </td>
    </tr>
</table>