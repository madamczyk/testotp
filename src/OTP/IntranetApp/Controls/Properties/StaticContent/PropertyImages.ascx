﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyImages.ascx.cs" Inherits="IntranetApp.Controls.Properties.PropertyImages" %>

<div id="codeBlocks" runat="server">
<script type="text/javascript">
    var jcrop_api;
    var uploadClientSt = '#ctl00_ctl00_MainContent_mainContent_PropertyDetailsCtrl_ctrlImages_rauImageUpload_ClientState';
    
    function Disable() {
        var btn = document.getElementById('<%=btnAddImage.ClientID%>');

        if (btn != null) {
            btn.disabled = true;
        }
    }
    function Enable() {
        var btn = document.getElementById('<%=btnAddImage.ClientID%>');

        if (btn != null) {
            btn.disabled = false;
        }
    }

    function ValidateImages() {
        var validationText = '';
        var uploaded = $(uploadClientSt).val();
        var elements = getImageInfo(uploaded);

        var maxH = $('#<%=maxH.ClientID%>').val();
        var minH = $('#<%=minH.ClientID%>').val();
        var maxW = $('#<%=maxW.ClientID%>').val();
        var minW = $('#<%=minW.ClientID%>').val();
        maxH = parseInt(maxH);
        minH = parseInt(minH);
        maxW = parseInt(maxW);
        minW = parseInt(minW);

        if (elements.length == 0) {
            validationText = 'Select images';
        }
        var validImageUrls = $('#<%=EditedValid.ClientID%>').val().split('^');

        for (i = 0; i < elements.length; i++) {
            var parts = elements[i].split("|");
            parts[0] = parseInt(parts[0]);
            parts[1] = parseInt(parts[1]);

            if (parts[0] == 0 && parts[1] == 0) {
                validationText += "<li>" + parts[2] + " File with that name already exist</li>";
                continue;
            }

            var fileName = parts[2].substring(parts[2].lastIndexOf('/') + 1);
            if (($.inArray(fileName, validImageUrls) > -1) || (parts[0] == -1 && parts[1] == -1) || (parts[0] >= minW && parts[0] <= maxW && parts[1] >= minH && parts[1] <= maxH))
                validationText += "";
            else {
                if (parts[0] < minW || parts[1] < minH)
                    validationText += "<li>" + fileName + " is too small</li>";
                else {
                    validationText += "<li>" + fileName + " is too large, <a onclick=\"CropImageForm('" + elements[i] + "', '" + parts[2] + "');\">crop or resize the image</a></li>";
                }
            }
        }
        if (validationText != '' || $('.failureFiles').length > 0) {
            if (validationText == '') {
                validationText += "<li>Selected files are not valid</li>";
            }
            $('#validationItems').html(validationText);
            $('#validationErrors').show();
        }
        else {
            $('#validationErrors').hide();
            __doPostBack('ctl00$ctl00$MainContent$mainContent$PropertyDetailsCtrl$ctrlImages$btnAddImage', '')
        }
    }

    function getMaxSize() {
        var maxH = $('#<%=maxH.ClientID%>').val();
        var maxW = $('#<%=maxW.ClientID%>').val();
        return [
            maxW = parseInt(maxW),
            maxH = parseInt(maxH)
        ];
    }

    function getMinSize() {
        var minW = $('#<%=minW.ClientID%>').val();
        var minH = $('#<%=minH.ClientID%>').val();
        return [            
            minW = parseInt(minW),
            minH = parseInt(minH)
        ];
    }

    function getSelection() {        
        var minW = $('#<%=minW.ClientID%>').val();
        var minH = $('#<%=minH.ClientID%>').val();
        return [
            0,
            0,
            minW = parseInt(minW),
            minH = parseInt(minH)            
        ];
    }

    function CropImageForm(metaData, url) {
        url = url + "?ver=" + new Date().getTime().toString()
        $('#<%=cropbox.ClientID%>').attr('src', url);
        $('#<%=EditedImage.ClientID%>').val(metaData);
        $('#<%=EditedImageUrl.ClientID%>').val(url);
        
        jcrop_api = $.Jcrop('#<%=cropbox.ClientID%>',
            {
                onSelect: updateCoords,
                onChange: updateCoords
            });
        jcrop_api.setImage(url);
        jcrop_api.setOptions({
            minSize: getMinSize(),
            maxSize: getMaxSize()
        });
        jcrop_api.setSelect(getSelection());
        //jcrop_api.focus();
        $('#cropImageContainer').dialog({
            autoOpen: false,
            modal: true,
            width: 500,
            height: 400,
            title: "Change size of image",
            overlay: { backgroundColor: "#000000", opacity: 0.5 },
            open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); }
        }).dialog('open');
        jcrop_api.setSelect(getSelection());
        jcrop_api.focus();
    }

    function updateCoords(c) {
        $('#<%=X.ClientID%>').val(c.x);
        $('#<%=Y.ClientID%>').val(c.y);
        $('#<%=W.ClientID%>').val(c.w);
        $('#<%=H.ClientID%>').val(c.h);
    };

    function SetAsValid() {
        $('#cropImageContainer').dialog().dialog('close');
        var uploadedFiles = $(uploadClientSt).val();
        var editedValue = $('#<%=EditedImage.ClientID%>').val();
        var editedValueUrl = $('#<%=EditedImageUrl.ClientID%>').val();

        var editedValid = $('#<%=EditedValid.ClientID%>').val();
        var fileName = editedValueUrl.substring(editedValueUrl.lastIndexOf('/') + 1);
        fileName = fileName.substring(0, fileName.lastIndexOf('?'));
        editedValid += '^' + fileName;
        $('#<%=EditedValid.ClientID%>').val(editedValid)

        uploadedFiles = uploadedFiles.replace(editedValue, "-1|-1|" + editedValueUrl);
        $('#<%=EditedImage.ClientID%>').val('');
        $(uploadClientSt).val(uploadedFiles)

        $('#<%=EditedImage.ClientID%>').val('');
        $('#<%=EditedImageUrl.ClientID%>').val('');
        jcrop_api.destroy();
        $('#<%=Submit.ClientID%>').show();
        $('#<%=Resize.ClientID%>').show();
    }

    function getImageInfo(str) {
        var results = [], re = /#([^}]+)!/g, text;

        while (text = re.exec(str)) {
            results.push(text[1]);
        }
        return results;
    }

    function FileRemoved(sender, args) {
        var removedFileName = args.get_fileName();
        var editedValid = $('#<%=EditedValid.ClientID%>').val();
        if (editedValid != "") {
            editedValid = editedValueUrl.replace('^' + removedFileName, '');
            $('#<%=EditedValid.ClientID%>').val(editedValid)
        }
    }

    function FileUploadFailed(sender, args) {
        var errorMessage = "File exceeds the size limit of 10 MB";
        if (args.get_fileName() != null && args.get_fileName() != "") {        
            var addedFileName = args.get_fileName();
            var fileExt = addedFileName.substr(addedFileName.lastIndexOf('.') + 1);       

            if ($.inArray(fileExt, sender._allowedFileExtensions) == -1)
            {
                errorMessage = "File of this type is not supported as a image";
            }
        }

        args.get_row().children[0].innerHTML = "<span class='ruUploadFailure failureFiles'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>" + errorMessage + "</span></span>";
    }

</script> 
</div>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="btnAddImage">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="pnlData1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="rcbImageType">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="pnlData1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="Submit">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="imagePanel" />
			</UpdatedControls>
		</telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="Resize">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="imagePanel" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData1">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>    
    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgImages" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="false" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="ImagesLinqDS"
		ClientSettings-EnablePostBackOnRowClick="false" 
        OnItemCommand="rgImages_ItemCommand" 
        OnItemDataBound="rgImages_ItemDataBound" 
        OnDataBound="rgImages_DataBound" Width="100%"
    ClientSettings-ClientEvents-OnColumnClick="disableWarning">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="PropertyStaticContentId" DataSourceID="ImagesLinqDS" CommandItemDisplay="None">
			<Columns>
				<telerik:GridBoundColumn DataField="ContentCode" FilterControlWidth="60px" UniqueName="ContentCode" HeaderText="Image Type" SortExpression="ContentCode" HeaderStyle-Width="300px" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="60px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn> 
                <telerik:GridBoundColumn DataField="ContentValue" FilterControlWidth="70px" UniqueName="ContentValue" HeaderText="Image Name" SortExpression="ContentValue" HeaderStyle-Width="300px" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="70px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridTemplateColumn FilterControlWidth="50px" Visible="false" UniqueName="ImgAbsolutePath" HeaderText="Image URL" HeaderStyle-Width="60px">  
                    <ItemTemplate> 
                        <a runat="server" id="imgLink" />
                    </ItemTemplate> 
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn FilterControlWidth="50px" UniqueName="Thumbnail" HeaderText="Image" HeaderStyle-Width="100px">  
                    <ItemTemplate> 
                        <asp:Image runat="server" id="imageThumbnail" />
                    </ItemTemplate> 
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>			
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="ContentCode" SortOrder="Ascending" />
            </SortExpressions>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

    <asp:Panel runat="server" ID="addingImgPanel">
    <br />
    <div ID="validationErrors" style="display:none;" class="errorInForm">
        <ul id="validationItems"></ul>
    </div>
        <h1><asp:Label runat="server" ID="lblAddImage" Text="Add Image" /></h1>
        <br />

        <telerik:RadComboBox runat="server" ID="rcbImageType" OnSelectedIndexChanged="rcbImageType_SelectedIndexChanged" AutoPostBack="true" />

        <asp:Label runat="server" ID="lblResolutionInfo" />
        <br />
        <asp:Label style="margin-left: 163px;" runat="server" ID="lblPlaceOfDisplay" meta:resourcekey="lblPlaceOfDisplay"  />

        <br /><br />
        <telerik:RadAsyncUpload InitialFileInputsCount="1" AllowedFileExtensions="jpg,jpeg,png" OnClientFileUploadRemoved="FileRemoved" 
            runat="server" ID="rauImageUpload" MultipleFileSelection="Disabled" OnClientFilesSelected="Disable" OnClientFileUploaded="Enable"  CssClass="" OnClientValidationFailed="FileUploadFailed"/>
        
        <asp:Button ID="btnAddImage" runat="server" Text="Upload" OnClientClick="ValidateImages(); return false;" OnClick="rauImageUpload_FileUploaded" />
    </asp:Panel>

    <asp:HiddenField ID="X" runat="server" />        
    <asp:HiddenField ID="Y" runat="server" />        
    <asp:HiddenField ID="W" runat="server" />        
    <asp:HiddenField ID="H" runat="server" />

    <asp:HiddenField ID="maxW" runat="server"/>        
    <asp:HiddenField ID="minW" runat="server" />        
    <asp:HiddenField ID="minH" runat="server" />        
    <asp:HiddenField ID="maxH" runat="server" />

    <asp:HiddenField ID="EditedImage" runat="server" />
    <asp:HiddenField ID="EditedImageUrl" runat="server" />
    <asp:HiddenField ID="EditedValid" runat="server" />
</asp:Panel>

<div id="cropImageContainer" style="display:none;">          
    <asp:Panel ID="imagePanel" runat="server">
    <asp:Button ID="Submit" runat="server" Text="Crop Image" onclick="Submit_Click" /> 
    <asp:Button ID="Resize" runat="server" Text="Resize" onclick="Resize_Click" /> 
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="$('#cropImageContainer').dialog().dialog('close'); jcrop_api.destroy(); return false;" />
    <asp:Button OnClientClick="SetAsValid(); return false;" ID="btnOk" Text="Close" runat="server" Visible="false"/>          
                           
    <br /> 
    <img id="cropbox" runat="server"/>
    <img id="croppedImage" runat="server" visible="false"/>
    </asp:Panel>
</div>
<otpDS:SimpleDataSource ID="ImagesLinqDS" runat="server" />

