﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Common.Extensions;

namespace IntranetApp.Controls.Properties
{
    public partial class PropertyFeatures : System.Web.UI.UserControl
    {

        #region Fields

        public PropertyStaticContentType ContentType
        {
            get { return PropertyStaticContentType.Feature; }
        }

        public int? PropertyID
        {
            get { return ViewState["PropertyAddOnId"] as int?; }
            set { ViewState["PropertyAddOnId"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemId"] as int?; }
            set { ViewState["EditedItemId"] = value; }
        }

        public List<DataAccess.PropertyStaticContent> NewItems
        {
            get
            {
                if (ViewState["NewItems"] == null)
                {
                    ViewState["NewItems"] = new List<DataAccess.PropertyStaticContent>();
                }
                return ViewState["NewItems"] as List<DataAccess.PropertyStaticContent>;
            }
            set { ViewState["NewItems"] = value; }
        }

        private int _nextProductParameterId;

        public int NextProductParameterId
        {
            get
            {
                int result = _nextProductParameterId;
                _nextProductParameterId++;
                return result;
            }
            set
            {
                _nextProductParameterId = value;
            }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.FeaturesLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.PropertyStaticContent> result = null;
                List<DataAccess.PropertyStaticContent> resultList;

                if (this.PropertyID.HasValue)
                    result = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(this.PropertyID.Value, ContentType).AsQueryable();

                if (result != null)
                    resultList = new List<DataAccess.PropertyStaticContent>(result);
                else
                    resultList = new List<DataAccess.PropertyStaticContent>();

                resultList.AddRange(NewItems);

                if (resultList.Count != 0)
                    NextProductParameterId = resultList.Max(ob => ob.PropertyStaticContentId) + 1;
                else
                    NextProductParameterId = 1;

                return resultList.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgFeatures_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int itemId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyStaticContentId"].ToString());
                if (PropertyID.HasValue)
                {
                    RequestManager.Services.PropertiesService.RemoveStaticContent(itemId);
                    RequestManager.Services.SaveChanges();
                }
                else
                    NewItems.Remove(NewItems.Where(u => u.PropertyStaticContentId == itemId).FirstOrDefault());                
            }
        }

        protected void rgFeatures_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                PropertyStaticContent obj = (PropertyStaticContent)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                string title = obj.ContentValueInternational.ToString().Limit();

                item.Attributes["feature-id"] = obj.PropertyStaticContentId.ToString();

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgFeatures.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_StaticContent"), title), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_StaticContent"), title));
            }
        }

        protected void rgFeatures_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgFeatures.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgFeatures.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgFeatures.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgFeatures.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgFeatures.MasterTableView.Rebind();
            }
        }

        protected void rgFeatures_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    DataAccess.PropertyStaticContent obj = new DataAccess.PropertyStaticContent();
                    ReadGridItemData(args.Item, obj);
                    obj.ContentCode = (int)ContentType;
                    if (PropertyID.HasValue)
                    {
                        obj.Property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyID.Value);
                        RequestManager.Services.PropertiesService.AddPropertyStaticContent(obj);
                        RequestManager.Services.SaveChanges();
                    }
                    else
                    {
                        NewItems.Add(obj);
                    }
                    this.rgFeatures.MasterTableView.Rebind();

                }
            }
        }

        protected void rgFeatures_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("PropertyStaticContentId"));


                    DataAccess.PropertyStaticContent obj;
                    if (PropertyID.HasValue)
                    {
                        obj = RequestManager.Services.PropertiesService.GetPropertyStaticContentById(id);
                        ReadGridItemData(editForm, obj);
                        RequestManager.Services.SaveChanges();
                    }
                    else
                    {
                        obj = NewItems.Where(ob => ob.PropertyStaticContentId == id).FirstOrDefault();
                        ReadGridItemData(editForm, obj);
                    } 
                    this.rgFeatures.MasterTableView.Rebind();

                }
            }
        }

        private void ReadGridItemData(GridItem item, DataAccess.PropertyStaticContent obj)
        {
            obj.ContentCode = (int)ContentType;
            obj.SetContentValue((item.FindControl("txtFeature") as RadTextBox).Text);
        }
        
        #endregion Event Handlers
    }
}