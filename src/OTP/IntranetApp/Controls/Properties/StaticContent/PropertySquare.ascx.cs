﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Controls.Properties
{
    public partial class PropertySquare : System.Web.UI.UserControl
    {
        public string Description
        {
            get { return this.txtPropertySquare.Text; }
            set { this.txtPropertySquare.Text = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}