﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyFloorPlan.ascx.cs" Inherits="IntranetApp.Controls.Properties.StaticContent.PropertyFloorPlan" %>

<link href="../../../Content/Styles/propertyFloorPlanDetails.css" type="text/css" rel="stylesheet" />

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData1">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="pnlData1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData1">
    <telerik:RadWindowManager ID="rwmFloorPlan" runat="server">
        <Windows>
            <telerik:RadWindow ID="EditFormWindow" runat="server" ShowContentDuringLoad="false" Width="933px" Height="550px" VisibleStatusbar="false"
                Title="The Property Floor Plan Edit" Modal="true" Behaviors="Move, Resize">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var uploadClientState = '#ctl00_ctl00_MainContent_mainContent_PropertyDetailsCtrl_ctrlFloorPlan_rauFloorPlanUpload_ClientState';

            function ShowEditForm(sender, eventArgs) {
                var oWnd = $find("<%= EditFormWindow.ClientID %>")
                oWnd.setUrl("PropertyFloorPlanDetails.aspx?id=" + eventArgs.getDataKeyValue("PropertyStaticContentId"));
                oWnd.show();
            }

            function validateUpload(sender, args) {
                var upload = $find("<%= rauFloorPlanUpload.ClientID %>");
                var uploadedState = $(uploadClientState).val();
                var elements = getFileInfo(uploadedState);
                var emptyFile = false;
                if(elements.length > 0)
                {
                    var parts = elements[i].split("|");
                    if(parseInt(parts[0]) == 0)
                        emptyFile = true;
                }
                if (elements.length == 0 || emptyFile || $('.failureFiles').length > 0)
                    args.IsValid = false;
            }

            function getFileInfo(str) {
                var results = [], re = /#([^}]+)!/g, text;

                while (text = re.exec(str)) {
                    results.push(text[1]);
                }
                return results;
            }

            function FileUploadFailedFloorPlans(sender, args) {
                var errorMessage = "File exceeds the size limit of 10 MB";
                if (args.get_fileName() != null && args.get_fileName() != "") {
                    var addedFileName = args.get_fileName();
                    var fileExt = addedFileName.substr(addedFileName.lastIndexOf('.') + 1);

                    if ($.inArray(fileExt, sender._allowedFileExtensions) == -1) {
                        errorMessage = "File of this type is not supported as a floor plan";
                    }
                }

                args.get_row().children[0].innerHTML = "<span class='ruUploadFailure failureFiles'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>" + errorMessage + "</span></span>";
            }
       </script>
    </telerik:RadCodeBlock>      
    <telerik:RadGrid 
        AutoGenerateColumns="False"
        ID="rgFloorPlans" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="false" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="FloorPlansLinqDS"
		ClientSettings-EnablePostBackOnRowClick="false" 
        OnItemCommand="rgFloorPlans_ItemCommand" 
        OnItemDataBound="rgFloorPlans_ItemDataBound" 
        OnDataBound="rgFloorPlans_DataBound" Width="100%"
        ClientSettings-ClientEvents-OnColumnClick="disableWarning">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
            <ClientEvents OnRowClick="ShowEditForm" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" ClientDataKeyNames="PropertyStaticContentId" DataKeyNames="PropertyStaticContentId" DataSourceID="FloorPlansLinqDS" CommandItemDisplay="None">
			<Columns>
                <telerik:GridBoundColumn DataField="Name" FilterControlWidth="70px" UniqueName="Name" HeaderText="Name" SortExpression="ContentValue" HeaderStyle-Width="300px" ShowSortIcon="false" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="70px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridTemplateColumn FilterControlWidth="50px" UniqueName="originalImageAbsolutePath" HeaderText="Original image" HeaderStyle-Width="150px">  
                    <ItemTemplate> 
                        <a runat="server" id="imgOrigLink" />
                    </ItemTemplate> 
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn FilterControlWidth="50px" UniqueName="ThumbnailOriginal" HeaderText="Thumbnail" HeaderStyle-Width="60px">  
                    <ItemTemplate> 
                        <asp:Image BackColor="Gray" runat="server" id="imageOriginalThumbnail" />
                    </ItemTemplate> 
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn FilterControlWidth="50px" UniqueName="displayImageAbsolutePath" HeaderText="Display image" HeaderStyle-Width="150px">  
                    <ItemTemplate> 
                        <a runat="server" id="imgDispLink" />
                    </ItemTemplate> 
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn FilterControlWidth="50px" UniqueName="ThumbnailModified" HeaderText="Thumbnail" HeaderStyle-Width="60px">  
                    <ItemTemplate> 
                        <asp:Image BackColor="Gray" runat="server" id="imageModifiedThumbnail" />
                    </ItemTemplate> 
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>		
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="ContentCode" SortOrder="Ascending" />
            </SortExpressions>
		    <PagerStyle AlwaysVisible="True"></PagerStyle>
	    </MasterTableView>
	    <HeaderContextMenu EnableImageSprites="True">
	    </HeaderContextMenu>
    </telerik:RadGrid>
    <br />
    <asp:Panel runat="server" ID="addingFloorPlanPanel" CssClass="upload-panel">
        <h1><asp:Label runat="server" ID="lblAddFloorPlan" Text="Add Floor Plan" /></h1>
        <asp:ValidationSummary ID="vsSummaryRawData" ValidationGroup="FloorPlan" DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
        <br />
        <asp:Label runat="server" ID="lblFloorPlanName" Text="Name" AssociatedControlID="rtxtFloorPlanName" /><br />
        <telerik:RadTextBox ID="rtxtFloorPlanName" runat="server" />
        <asp:RequiredFieldValidator ID="rfqFloorPlanName" ValidationGroup="FloorPlan" runat="server" Text="*" Display="Dynamic" ControlToValidate="rtxtFloorPlanName" meta:resourcekey="ValCode" CssClass="error" />
        <asp:CustomValidator ID="cvFloorPlanExists" ValidationGroup="FloorPlan" runat="server" Text="*" Display="Dynamic" ControlToValidate="rtxtFloorPlanName" meta:resourcekey="cvFloorPlanExists" CssClass="error" OnServerValidate="cvFloorPlanExists_ServerValidate" />
        <asp:Label ID="lblFloorPlanNameExample" meta:resourcekey="lblFloorPlanNameExample" runat="server" />
        <br /><br />
        <asp:Label runat="server" ID="lblFloorPlanImage" Text="Floor plan image" AssociatedControlID="rauFloorPlanUpload" />
        <telerik:RadAsyncUpload runat="server" ID="rauFloorPlanUpload" MultipleFileSelection="Disabled" 
            InitialFileInputsCount="1" MaxFileInputsCount="1" OnClientFilesSelected="Disable" OnClientFileUploaded="Enable" AllowedFileExtensions="jpg,png,jpeg,bmp" CssClass="" OnClientValidationFailed="FileUploadFailedFloorPlans" />
        <asp:CustomValidator runat="server" ID="cvFloorPlanUpload" ClientValidationFunction="validateUpload" meta:resourceKey="cvFloorPlanUpload" ValidationGroup="FloorPlan" Text="*" CssClass="error" />
        <br />
        <br />  
        <asp:Button ID="btnAddFloorPlan" runat="server" Text="Add" ValidationGroup="FloorPlan" OnClick="rauFloorPlanUpload_FileUploaded" />
    </asp:Panel>
</asp:Panel>
<otpDS:SimpleDataSource ID="FloorPlansLinqDS" runat="server" />
