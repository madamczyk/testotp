﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;
using Common.Helpers;
using Common.Extensions;

namespace IntranetApp.Controls.Properties
{
    public partial class ContentUpdateRequestDetails : BaseUserControl
    {      
        #region Fields

        public int? ContentUpdateRequestId
        {
            get { return ViewState["ContentUpdateRequestId"] as int?; }
            set { ViewState["ContentUpdateRequestId"] = value; }
        }   

        #endregion Fields

        #region Methods

        private void LoadData()
        {
            Debug.Assert(ContentUpdateRequestId.HasValue);

            ContentUpdateRequest contentUpdateRequest = RequestManager.Services.PropertiesService.GetContentUpdateRequestById(ContentUpdateRequestId.Value);

            // boxes
            txtPropertyName.Text = contentUpdateRequest.Property.PropertyNameCurrentLanguage;
            txtRequestDate.Text = contentUpdateRequest.RequestDateTime.ToLocalizedDateTimeString();
            txtDescription.Text = contentUpdateRequest.Description;
        }

        private bool SaveData()
        {
            return true;
        }

        private bool CheckExists()
        {
            if (ContentUpdateRequestId.HasValue && RequestManager.Services.PropertiesService.GetContentUpdateRequestById(ContentUpdateRequestId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ContentUpdateRequest"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_ContentUpdateRequest"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void BindControls()
        {
            DetailsListCtrl.ContentUpdateRequestId = ContentUpdateRequestId;
        }

        private void SetNames()
        {
            //if (Mode == FormMode.Edit)
            //{
            //    Amenity amenity = RequestManager.Services.AmenityService.GetAmenityById(ContentUpdateRequestId.Value);

            //    rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"), amenity.Title.ToString());
            //    lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"), txtName.Text);
            //}   

            lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeader.Text"), ContentUpdateRequestId.Value);
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindControls();

                if (CheckExists())
                {
                    LoadData();
                    SetNames();
                }
            }
        }

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    if (Page.IsValid)
        //    {
        //        if (!SaveData())
        //        {
        //            rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"));
        //            lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"));

        //            rwError.VisibleOnPageLoad = true;
        //        }
        //        else
        //            Response.Redirect("~/Forms/Amenities/AmenitiesList.aspx");
        //    }
        //}

        //protected void btnCancel_Click(object sender, EventArgs e)
        //{
        //    if (Mode == FormMode.Add)
        //        rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text);

        //    rwConfirm.VisibleOnPageLoad = true;
        //}

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Proeprties/ContentUpdateRequestsList.aspx");
        }

        #endregion
    }
}