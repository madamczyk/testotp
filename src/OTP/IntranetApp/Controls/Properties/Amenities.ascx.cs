﻿using BusinessLogic.Managers;
using Common;
using DataAccess;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Properties
{
    public partial class Amenities : System.Web.UI.UserControl
    {
        public global::Telerik.Web.UI.RadGrid rgAmenities;

        public int? PropertyID
        {
            get { return ViewState["UnitTypeId"] as int?; }
            set { ViewState["UnitTypeId"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemId"] as int?; }
            set { ViewState["EditedItemId"] = value; }
        }

        public List<PropertyAmenity> PropertyAmenities
        {
            get { return ViewState["PropertyAmenities"] as List<PropertyAmenity>; }
            set { ViewState["PropertyAmenities"] = value; }
        }

        #region Methods

        private void BindGrid()
        {
            this.amenitiesLinqDS.DataResolver = () =>
            {
                IQueryable<Amenity> result = RequestManager.Services.AmenityService.GetAmenities().AsQueryable();
                rgAmenities.ApplyFilter<Amenity>(ref result, "Name", (q, f) => q.Where(p => p.TitleCurrentLanguage.ToString().ToLower().Contains(f.ToLower())));
                rgAmenities.ApplyFilter<Amenity>(ref result, "Group", (q, f) => q.Where(p => p.AmenityGroup.NameCurrentLanguage.ToString().ToLower().Contains(f.ToLower())));

                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgAmenities_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgAmenities_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Amenity obj = (Amenity)item.DataItem;
                CheckBox cb = (CheckBox)item["IsConnected"].FindControl("cbAmenity");
                RadNumericTextBox rntb = (RadNumericTextBox)item["Quantity"].FindControl("txtQuantity");
                PropertyAmenity propertyAmenity = PropertyAmenities.Where(pa => pa.Amenity.AmenityID == obj.AmenityID).FirstOrDefault();
                RequiredFieldValidator rfv = (RequiredFieldValidator)item["Quantity"].FindControl("RequiredFieldValidatorQuantity");

                rntb.Visible = obj.Countable;
                rfv.ErrorMessage = String.Format((string)GetLocalResourceObject("Quantity_Required"), obj.TitleCurrentLanguage);

                if (propertyAmenity != null)
                {
                    cb.Checked = true;
                    rntb.Enabled = true;

                    if (obj.Countable)
                    {
                        rntb.Text = propertyAmenity.Quantity.HasValue ? propertyAmenity.Quantity.Value.ToString() : "";
                        rfv.Enabled = true;
                    }
                }

                cb.Attributes.Add("ItemID", item.ItemIndex.ToString());
                cb.Attributes.Add("AmenityID", obj.AmenityID.ToString());
            }
        }

        protected void rgAmenities_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgAmenities.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgAmenities.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgAmenities.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgAmenities.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgAmenities.MasterTableView.Rebind();
            }
        }

        protected void cbAmenities_CheckedChanged(object sender, EventArgs args)
        {
            CheckBox cb = (CheckBox)sender;
            RadNumericTextBox rntb;
            GridDataItem item;
            Amenity amenity;
            RequiredFieldValidator rfv;

            if (cb == null)
                return;

            if (String.IsNullOrEmpty(cb.Attributes["ItemID"]) || String.IsNullOrEmpty(cb.Attributes["AmenityID"]))
                return;

            item = rgAmenities.MasterTableView.Items[int.Parse(cb.Attributes["ItemID"])];
            amenity = RequestManager.Services.AmenityService.GetAmenityById(int.Parse(cb.Attributes["AmenityID"]));

            if (amenity == null)
            {
                rgAmenities.Rebind();
                return;
            }

            rntb = (RadNumericTextBox)item["Quantity"].FindControl("txtQuantity");
            rfv = (RequiredFieldValidator)item["Quantity"].FindControl("RequiredFieldValidatorQuantity");

            rntb.Enabled = cb.Checked;
            rfv.Enabled = cb.Checked && amenity.Countable;

            if (cb.Checked)
            {
                PropertyAmenities.Add(new PropertyAmenity()
                {
                    Amenity = amenity
                });
            }
            else
            {
                PropertyAmenities.RemoveAll(pa => pa.Amenity.AmenityID == amenity.AmenityID);
                rntb.Text = "";
            }
        }

        #endregion Event Handlers

        protected void rgAmenities_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            var gridItems = (sender as RadGrid).MasterTableView.Items;
            Page.Validate("Property");
            e.Canceled = !Page.IsValid;
            if (!Page.IsValid)
                return;

            foreach (GridDataItem itemGrid in gridItems)
            {
                //i obj = (Amenity)itemGrid.DataItem;
                CheckBox cb = (CheckBox)itemGrid["IsConnected"].FindControl("cbAmenity");
                if (cb.Checked)
                {
                    int amenityId = int.Parse(cb.Attributes["AmenityID"].ToString());
                    RadNumericTextBox rntb = (RadNumericTextBox)itemGrid["Quantity"].FindControl("txtQuantity");
                    Amenity obj = RequestManager.Services.AmenityService.GetAmenities().Where(a => a.AmenityID == amenityId).SingleOrDefault();
                    PropertyAmenity propertyAmenity = PropertyAmenities.Where(pa => pa.Amenity.AmenityID == amenityId).SingleOrDefault();
                    if (propertyAmenity == null)
                    {
                        propertyAmenity = new PropertyAmenity();
                        propertyAmenity.Amenity = obj;
                        PropertyAmenities.Add(propertyAmenity);
                    }
                    if (obj.Countable)
                    {
                        propertyAmenity.Quantity = (int)rntb.Value.Value;
                    }                    
                }
            }
        }
    }
}