﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tags.ascx.cs" Inherits="IntranetApp.Controls.Properties.Tags" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgtags">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgtags" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadGrid 
    AutoGenerateColumns="False" 
    ID="rgtags" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="tagsLinqDS"
	ClientSettings-EnablePostBackOnRowClick="false" 
    OnItemCommand="rgtags_ItemCommand" 
    OnDataBound="rgtags_DataBound"
    OnItemDataBound="rgtags_ItemDataBound" Width="100%"
    ClientSettings-ClientEvents-OnColumnClick="disableWarning">
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="True" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Fixed" DataKeyNames="TagID" DataSourceID="tagsLinqDS" CommandItemDisplay="None" >
		<Columns>            
            <telerik:GridBoundColumn DataField="NameCurrentLanguage" FilterControlWidth="200px" UniqueName="Tag" HeaderText="Tag" SortExpression="NameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="200px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText = " " UniqueName="Temp">  
                <ItemTemplate> 
                    <asp:CheckBox ID="cbTag" Checked="false" runat="server" OnCheckedChanged="cbTags_CheckedChanged" AutoPostBack="true" /> 
                </ItemTemplate> 
            </telerik:GridTemplateColumn> 
		</Columns>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<otpDS:SimpleDataSource ID="tagsLinqDS" runat="server" />