﻿using BusinessLogic.Enums;
using BusinessLogic.Managers;
using Common;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Properties
{
    public partial class PropertyDetails : System.Web.UI.UserControl
    {
        #region Fields

        public readonly string TimeZone = "Eastern Standard Time";

        public int? CountryUS
        {
            get { return ViewState["CountryUS"] as int?; }
            set { ViewState["CountryUS"] = value; }
        
        }

        public int? PropertyId
        {
            get { return ViewState["PropertyId"] as int?; }
            set { ViewState["PropertyId"] = value; }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["Mode"]; }
            set { ViewState["Mode"] = (int)value; }
        }

        #endregion Fields

        #region Methods

        private void ParseQueryString()
        {
            int ID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out ID))
            {
                this.PropertyId = ID;
            }
        }

        private void LoadData()
        {
            Debug.Assert(this.PropertyId.HasValue);

            Property property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyId.Value);

            //textboxes
            this.txtName.Text = property.PropertyName.ToString();
            this.cbPropertyEnabled.Checked = property.PropertyLive;
            this.rcbOwner.SelectedValue = property.User.UserID.ToString();
            this.rcbDestination.SelectedValue = property.Destination.DestinationID.ToString();
            this.rcbPropertyType.SelectedValue = property.PropertyType != null ? property.PropertyType.PropertyTypeId.ToString() : string.Empty;
            this.txtShortDescription.Text = property.Short_DescriptionCurrentLanguage;
            this.txtLongDescription.Text = property.Long_DescriptionCurrentLanguage;
            this.txtAddressLine1.Text = property.Address1;
            this.txtAddressLine2.Text = property.Address2;
            this.txtState.Text = property.State;
            this.txtZipCode.Text = property.ZIPCode;
            this.rcbCountry.SelectedValue = property.DictionaryCountry.CountryId.ToString();
            this.txtLatitude.Text = property.Latitude.ToString(".000000");
            this.txtLongitude.Text = property.Longitude.ToString(".000000");
            this.txtAltitude.Text = property.Altitude.ToString();
            this.rcbCulture.SelectedValue = property.DictionaryCulture.CultureId.ToString();
            this.txtCity.Text = property.City;
            this.txtCode.Text = property.PropertyCode;
            DateTime dt = DateTime.MinValue.AddYears(2000);
            
            dt = dt.AddHours(new DateTime(property.CheckIn.Ticks).Hour);
            dt = dt.AddMinutes(new DateTime(property.CheckIn.Ticks).Minute);
            
            this.rtpCheckInTime.SelectedDate = dt;
            this.txtStandardCommission.Text = property.StandardCommission.ToString("N2");
            this.rcbStarRating.SelectedValue = property.StarRating.ToString();

            dt = DateTime.MinValue.AddYears(2000);
            dt = dt.AddHours(new DateTime(property.CheckOut.Ticks).Hour);
            dt = dt.AddMinutes(new DateTime(property.CheckOut.Ticks).Minute);
            
            this.rtpCheckOutTime.DbSelectedDate = dt;
            
            var localDesc = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(property.PropertyID, PropertyStaticContentType.LocalAreaDescription).FirstOrDefault();
            var squareFootage = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(property.PropertyID, PropertyStaticContentType.SquareFootage).FirstOrDefault();

            this.ctrlLocalAreaDescr.Description = localDesc != null ? localDesc.ContentValueCurrentLanguage : "";
            this.ctrlSquare.Description = squareFootage != null ? squareFootage.ContentValueCurrentLanguage : "";
            this.rcbKeyProcessType.SelectedValue = ((int)KeyProcessType.Lockbox).ToString();

            if ((KeyProcessType)property.KeyProcessType == KeyProcessType.KeyHandler)
            {
                this.trResponsible.Attributes.Add("style", "display: ");
                this.rcbKeyProcessType.SelectedValue = property.KeyProcessType.ToString();
                var keyManager = property.PropertyAssignedManagers.Where(m => m.Role.RoleLevel == (int)RoleLevel.KeyManager).SingleOrDefault();
                this.rcbKeyHandlerManager.SelectedValue = keyManager != null ? keyManager.User.UserID.ToString() : rcbKeyHandlerManager.SelectedValue;
            }
            else
            {
                rfvKeyHandler.Enabled = false;
            }
            
            #region upload manager
            PropertyAssignedManager uploadManager = property.PropertyAssignedManagers.Where(pm => pm.Role.RoleLevel == (int)RoleLevel.Manager).SingleOrDefault();
            if(uploadManager != null)
            {
                this.rcbUploadManager.SelectedValue = uploadManager.User.UserID.ToString();
            }
            #endregion

            #region property manager
            PropertyAssignedManager propertyManager = property.PropertyAssignedManagers.Where(pm => pm.Role.RoleLevel == (int)RoleLevel.PropertyManager).SingleOrDefault();
            if (propertyManager != null)
            {
                this.rcbPropertyManager.SelectedValue = propertyManager.User.UserID.ToString();
            }
            #endregion

            this.rcbSalesPersons.SelectedValue = property.SalesPerson == null ? string.Empty : property.SalesPerson.SalesPersonId.ToString();

            //time zone
            this.rcbTimeZones.SelectedValue = property.TimeZone;

        }

        private int SaveData()
        {
            Property property = null;

            if (this.Mode == FormMode.Add)
                property = new Property();
            else
            {
                property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyId.Value);

                property.PropertyCode = null;
                property.Altitude = null;

                RequestManager.Services.LoadEntityProperty(property, "PropertyType");
                property.PropertyType = null;
            }

            if (property == null)
            {
                return -1;
            }

            property.Address1 = this.txtAddressLine1.Text;
            property.Address2 = this.txtAddressLine2.Text;

            if (!string.IsNullOrWhiteSpace(this.txtAltitude.Text))
            {
                property.Altitude = decimal.Parse(this.txtAltitude.Text);
            }

            property.Latitude = decimal.Parse(this.txtLatitude.Text);
            property.Longitude = decimal.Parse(this.txtLongitude.Text);

            property.DictionaryCountry = RequestManager.Services.DictionaryCountryService.GetCountryById(int.Parse(this.rcbCountry.SelectedValue));
            property.Destination = RequestManager.Services.DestinationService.GetDestinationById(int.Parse(this.rcbDestination.SelectedValue));
            
            property.SetLongDescriptionValue(this.txtLongDescription.Text);
            property.SetPropertyNameValue(this.txtName.Text);
            property.SetShortDescriptionValue(this.txtShortDescription.Text);
            property.State = this.txtState.Text;
            property.User = RequestManager.Services.UsersService.GetUserByUserId(int.Parse(this.rcbOwner.SelectedValue));
            property.ZIPCode = this.txtZipCode.Text;
            property.PropertyLive = this.cbPropertyEnabled.Checked;

            if (!string.IsNullOrWhiteSpace(this.rcbPropertyType.SelectedValue))
            {
                property.PropertyType = RequestManager.Services.PropertyTypeService.GetPropertyTypeById(int.Parse(this.rcbPropertyType.SelectedValue));
            }

            if (!string.IsNullOrWhiteSpace(this.txtCode.Text))
            {
                property.PropertyCode = this.txtCode.Text;
            }

            property.City = this.txtCity.Text;

            property.CheckIn = new TimeSpan(this.rtpCheckInTime.SelectedDate.Value.Hour, this.rtpCheckInTime.SelectedDate.Value.Minute, 0);
            property.CheckOut = new TimeSpan(this.rtpCheckOutTime.SelectedDate.Value.Hour, this.rtpCheckOutTime.SelectedDate.Value.Minute, 0);
            property.StarRating = int.Parse(this.rcbStarRating.SelectedValue);
            property.TimeZone = rcbTimeZones.SelectedValue;

            property.StandardCommission = decimal.Parse(this.txtStandardCommission.Text);
            property.KeyProcessType = int.Parse(this.rcbKeyProcessType.SelectedValue);
            if (property.KeyProcessType == (int)KeyProcessType.KeyHandler)
            {
                PropertyAssignedManager assignedManager = RequestManager.Services.PropertiesService.GetPropertyManagerByType(property.PropertyID, RoleLevel.KeyManager);

                if (assignedManager == null)
                {
                    assignedManager = new PropertyAssignedManager();
                    assignedManager.Role = RequestManager.Services.RolesService.GetRoleByRoleLevel(RoleLevel.KeyManager);
                    assignedManager.Property = property;
                    assignedManager.User = RequestManager.Services.UsersService.GetUserByUserId(int.Parse(this.rcbKeyHandlerManager.SelectedValue));
                    property.PropertyAssignedManagers.Add(assignedManager);
                }
                else
                    assignedManager.User = RequestManager.Services.UsersService.GetUserByUserId(int.Parse(this.rcbKeyHandlerManager.SelectedValue));
            }

            #region Upload Manager
            PropertyAssignedManager uploadManager = RequestManager.Services.PropertiesService.GetPropertyManagerByType(property.PropertyID, RoleLevel.Manager);
            if (uploadManager == null)
            {
                uploadManager = new PropertyAssignedManager();
                uploadManager.Role = RequestManager.Services.RolesService.GetRoleByRoleLevel(RoleLevel.Manager);
                uploadManager.Property = property;
                uploadManager.User = RequestManager.Services.UsersService.GetUserByUserId(int.Parse(this.rcbUploadManager.SelectedValue));
                property.PropertyAssignedManagers.Add(uploadManager);
            }
            else
                uploadManager.User = RequestManager.Services.UsersService.GetUserByUserId(int.Parse(this.rcbUploadManager.SelectedValue));
            #endregion

            #region Property Manager

            if (!string.IsNullOrWhiteSpace(this.rcbPropertyManager.SelectedValue))
            {
                PropertyAssignedManager propertyManager = RequestManager.Services.PropertiesService.GetPropertyManagerByType(property.PropertyID, RoleLevel.PropertyManager);
                if (propertyManager == null)
                {
                    propertyManager = new PropertyAssignedManager();
                    propertyManager.Role = RequestManager.Services.RolesService.GetRoleByRoleLevel(RoleLevel.PropertyManager);
                    propertyManager.Property = property;
                    propertyManager.User = RequestManager.Services.UsersService.GetUserByUserId(int.Parse(this.rcbPropertyManager.SelectedValue));
                    property.PropertyAssignedManagers.Add(propertyManager);
                }
                else
                    propertyManager.User = RequestManager.Services.UsersService.GetUserByUserId(int.Parse(this.rcbPropertyManager.SelectedValue));
            }
            else
            {
                PropertyAssignedManager managerToDelete = RequestManager.Services.PropertiesService.GetPropertyManagerByType(property.PropertyID, RoleLevel.PropertyManager);
                if (managerToDelete != null)
                {
                    RequestManager.Services.PropertiesService.DeleteManagerFromProperty(managerToDelete.PropertyAssignedManagerId);
                }
            }
            
            #endregion

            #region Sales Person

            if (rcbSalesPersons.SelectedValue == string.Empty)
            {
                if (property.SalesPerson != null)
                {
                    RequestManager.Services.LoadEntityProperty(property, "SalesPerson");
                    property.SalesPerson = null;
                }
            }
            else
            {
                int salesPersonId = int.Parse(rcbSalesPersons.SelectedValue);
                property.SalesPerson = RequestManager.Services.SalesPersonsService.GetSalesPersonById(salesPersonId);
            }

            #endregion

            property.DictionaryCulture = RequestManager.Services.DictionaryCultureService.GetCultureById(int.Parse(this.rcbCulture.SelectedValue));

            if (this.Mode == FormMode.Add)
                RequestManager.Services.PropertiesService.AddProperty(property);

            RequestManager.Services.SaveChanges();
            
            if (this.Mode == FormMode.Edit)
            {
                //local description area
                bool addNew = false;
                PropertyStaticContent psc = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(this.PropertyId.Value, PropertyStaticContentType.LocalAreaDescription).FirstOrDefault();
                if (psc == null)
                {
                    addNew = true;
                    psc = new PropertyStaticContent();
                }
                psc.ContentCode = (int)PropertyStaticContentType.LocalAreaDescription;
                psc.SetContentValue(this.ctrlLocalAreaDescr.Description);
                psc.Property = property;
                if(addNew) 
                    RequestManager.Services.PropertiesService.AddPropertyStaticContent(psc);

                //square footagea
                //local description area
                addNew = false;
                PropertyStaticContent psc1 = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(this.PropertyId.Value, PropertyStaticContentType.SquareFootage).FirstOrDefault();
                if (psc1 == null)
                {
                    addNew = true;
                    psc1 = new PropertyStaticContent();
                }
                psc1.ContentCode = (int)PropertyStaticContentType.SquareFootage;
                psc1.SetContentValue(this.ctrlSquare.Description);
                psc1.Property = property;
                if (addNew) 
                    RequestManager.Services.PropertiesService.AddPropertyStaticContent(psc1);

                //amenities
                Amenity amenity;
                PropertyAmenity propertyAmenity;
                CheckBox cb;
                int quantity;

                RequestManager.Services.PropertiesService.RemovePropertyAmenitiesByPropertyId(property.PropertyID); // remove all property amenitites

                foreach (GridDataItem item in ctrlAmenities.rgAmenities.MasterTableView.Items) // iterate through all amenities in the grid
                {
                    cb = (CheckBox)item["IsConnected"].FindControl("cbAmenity"); // find checkbox
                    if (cb == null || !cb.Checked || String.IsNullOrEmpty(cb.Attributes["AmenityID"]))
                        continue;

                    amenity = RequestManager.Services.AmenityService.GetAmenityById(int.Parse(cb.Attributes["AmenityID"]));

                    propertyAmenity = ctrlAmenities.PropertyAmenities.Where(a => a.Amenity.AmenityID == amenity.AmenityID).FirstOrDefault();
                    if (propertyAmenity == null)
                        continue;

                    if (!int.TryParse(((RadNumericTextBox)item["Quantity"].FindControl("txtQuantity")).Text, out quantity))
                        quantity = 1;

                    propertyAmenity = ctrlAmenities.PropertyAmenities.Where(pa => pa.Amenity.AmenityID == amenity.AmenityID).SingleOrDefault(); // retrieve property amenity associated with amenity
                    propertyAmenity.Amenity = amenity;
                    propertyAmenity.Property = property; // bind property
                    propertyAmenity.Quantity = amenity.Countable ? quantity : (int?)null; // set quantity if needed
                }
                foreach (PropertyAmenity pa in ctrlAmenities.PropertyAmenities)
                {
                    amenity = RequestManager.Services.AmenityService.GetAmenityById(pa.Amenity.AmenityID);
                    pa.Amenity = amenity;
                    pa.Property = property; // bind property
                    RequestManager.Services.PropertiesService.AddPropertyAmenity(pa);
                }

                RequestManager.Services.SaveChanges();
            }

            //DisableProperty(property);

            return property.PropertyID;
        }

        private void DisableProperty(Property property)
        {
            if (property != null)
            {
                if (propertyCompletness.Completness.HasValue && propertyCompletness.Completness.Value < 100)
                {
                    property.PropertyLive = false;
                }
                else if(propertyCompletness.Completness.Value == 100)
                {
                    property.PropertyLive = true;
                }
                RequestManager.Services.SaveChanges();
            }
        }

        private bool CheckExists()
        {
            if (PropertyId.HasValue && RequestManager.Services.PropertiesService.GetPropertyById(PropertyId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Property"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_Property"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void BindControls()
        {
            int countryUS = -1;
            int defaultCulture = -1;
            if (Mode == FormMode.Add)
            {
                this.txtStandardCommission.Text = decimal.Parse(RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.PropertyStandardCommision), CultureInfo.InvariantCulture).ToString(".00");
                //this.ctrlDistanceToAttractions.Visible = false;
                //this.ctrlFloorPlan.Visible = false;
                //this.ctrlImages.Visible = false;
                
            }
            //countries
            foreach (DictionaryCountry obj in RequestManager.Services.DictionaryCountryService.GetCountries())
            {
                this.rcbCountry.Items.Add(new RadComboBoxItem(obj.CountryName.ToString(), obj.CountryId.ToString()));
                if(obj.CountryCode2Letters.ToLower() == "us")
                {
                    countryUS = obj.CountryId;
                    CountryUS = obj.CountryId;
                }
            }
            //users
            var users = RequestManager.Services.UsersService.GetUsersByRole(RoleLevel.Owner).OrderBy(u => u.FullName);
            foreach (User obj in users)
            {
                this.rcbOwner.Items.Add(new RadComboBoxItem(obj.FullName.ToString(), obj.UserID.ToString()));
            }
            //load destinations
            foreach (Destination obj in RequestManager.Services.DestinationService.GetDestinations())
            {
                this.rcbDestination.Items.Add(new RadComboBoxItem(obj.DestinationName.ToString(), obj.DestinationID.ToString()));
            }
            //property types
            this.rcbPropertyType.Items.Add(new RadComboBoxItem());
            foreach (PropertyType obj in RequestManager.Services.PropertyTypeService.GetPropertyTypes())
            {
                this.rcbPropertyType.Items.Add(new RadComboBoxItem(obj.NameCurrentLanguage.ToString(), obj.PropertyTypeId.ToString()));
            }           

            //cultures
            var cultures = RequestManager.Services.DictionaryCultureService.GetCultures().OrderBy(c => c.CultureNameCurrentLanguage).ToList();
            foreach (DictionaryCulture obj in cultures)
            {
                this.rcbCulture.Items.Add(new RadComboBoxItem(obj.CultureName.ToString(), obj.CultureId.ToString()));
                if (obj.CultureCode == "en-US")
                {
                    defaultCulture = obj.CultureId;
                }
            }


            //property id
            if (PropertyId.HasValue)
            {
                this.ctrlUnitTypes.PropertyID = this.PropertyId.Value;
                this.ctrlUnits.PropertyID = this.PropertyId.Value;
                this.ctrlAddOns.PropertyID = this.PropertyId.Value;
                this.ctrlFeatures.PropertyID = this.PropertyId.Value;
                this.ctrlDistanceToAttractions.PropertyID = this.PropertyId.Value;
                this.ctrlImages.PropertyID = this.PropertyId.Value;
                this.ctrlAmenities.PropertyID = this.PropertyId.Value;
                this.ctrlTags.PropertyID = this.PropertyId.Value;
                this.ctrlFloorPlan.PropertyID = this.PropertyId.Value;
                this.ctrlRawData.PropertyID = this.PropertyId.Value;

                this.ctrlAmenities.PropertyAmenities = RequestManager.Services.PropertiesService.GetPropertyAmenitiesByPropertyId(this.PropertyId.Value).ToList();
            }
            if (this.Mode == FormMode.Add)
            {
                this.rcbCountry.SelectedValue = countryUS.ToString();
                this.rcbCulture.SelectedValue = defaultCulture.ToString();
            }
            this.lblLongitudeExample.Text = string.Format("00{0}000000", CurrentCultureInfoSet.NumberSeparator);
            this.lblLatitudeExample.Text = this.lblLongitudeExample.Text;
            this.lblAltitudeExample.Text = string.Format("000{0}00", CurrentCultureInfoSet.NumberSeparator);
            this.lblStandardCommissionExample.Text = string.Format("00{0}00", CurrentCultureInfoSet.NumberSeparator);

            if (this.PropertyId.HasValue)
            {
                this.propertyCompletness.PropertyId = this.PropertyId.Value;
                this.propertyCompletness.Visible = true;
                if (this.propertyCompletness.Completness != 100)
                {
                    this.cbPropertyEnabled.Enabled = false;
                }
                else
                    this.cbPropertyEnabled.Enabled = true;
            }
            else
            {
                this.propertyCompletness.Visible = false;
            }
            this.rcbKeyProcessType.Items.Add(new RadComboBoxItem("", ""));

            foreach(KeyProcessType kpType in Enum.GetValues(typeof(KeyProcessType)))
            {
                this.rcbKeyProcessType.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(kpType), ((int)kpType).ToString()));
                if (kpType == KeyProcessType.KeyHandler)
                {
                    this.hfKeyHandlerProcessType.Value = EnumConverter.GetValue(kpType);
                }
            }
            this.rcbKeyProcessType.SelectedIndex = -1; 
            this.trResponsible.Attributes.Add("style", "display: none");
            
            //responsbile manager
            var responsbileManagers = RequestManager.Services.UsersService.GetUsersByRole(RoleLevel.Owner).OrderBy(u => u.FullName).ToList();
            responsbileManagers.AddRange(RequestManager.Services.UsersService.GetUsersByRole(RoleLevel.KeyManager).OrderBy(u => u.FullName));
            responsbileManagers.AddRange(RequestManager.Services.UsersService.GetUsersByRole(RoleLevel.PropertyManager).OrderBy(u => u.FullName));
            this.rcbKeyHandlerManager.Items.Add(new RadComboBoxItem("", ""));
            foreach (User obj in responsbileManagers)
            {
                this.rcbKeyHandlerManager.Items.Add(new RadComboBoxItem(obj.FullName.ToString(), obj.UserID.ToString()));
            }

            //upload managers
            var uploadManagers = RequestManager.Services.UsersService.GetUsersByRole(RoleLevel.Manager).OrderBy(u => u.FullName);
            this.rcbUploadManager.Items.Add(new RadComboBoxItem("", ""));
            foreach (User obj in uploadManagers)
            {
                this.rcbUploadManager.Items.Add(new RadComboBoxItem(obj.FullName.ToString(), obj.UserID.ToString()));
            }

            //property managers
            var propertyManagers = RequestManager.Services.UsersService.GetUsersByRole(RoleLevel.PropertyManager).OrderBy(u => u.FullName);
            this.rcbPropertyManager.Items.Add(new RadComboBoxItem("", ""));
            foreach (User obj in propertyManagers)
            {
                this.rcbPropertyManager.Items.Add(new RadComboBoxItem(obj.FullName.ToString(), obj.UserID.ToString()));
            }

            // sales persons - already ordered in service
            var salesPersons = RequestManager.Services.SalesPersonsService.GetSalesPersons();
            this.rcbSalesPersons.Items.Add(new RadComboBoxItem(string.Empty, string.Empty));
            foreach (var salesPerson in salesPersons)
            {
                this.rcbSalesPersons.Items.Add(new RadComboBoxItem(string.Format("{0} {1}", salesPerson.FirstName, salesPerson.LastName), salesPerson.SalesPersonId.ToString()));
            }

            //time zones
            var timeZones = TimeZoneInfo.GetSystemTimeZones();

            foreach (var timeZone in timeZones)
            {
                this.rcbTimeZones.Items.Add(new RadComboBoxItem(timeZone.DisplayName, timeZone.Id));
            }
            this.rcbTimeZones.SelectedValue = this.TimeZone;
        }

        private void SetNames()
        {
            if (this.Mode == FormMode.Edit)
            {
                Property property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Property"), property.PropertyName.ToString());

                this.lblHeader.Text = string.Format(this.GetLocalResourceObject("lblHeaderEdit.Text").ToString(), this.txtName.Text);
                this.rcbOwner.Enabled = false;
                this.rcbDestination.Enabled = false;
                this.lblMoreDetails.Visible = false;
            }
            else
            {
                this.lblMoreDetails.Visible = true;
                
                foreach (RadTab tab in this.rtsPropertyDetails.Tabs)
                    tab.Visible = false;
                this.rtsPropertyDetails.Tabs[0].Visible = true;

                foreach (RadPageView rpv in rmpTabContent.PageViews)
                    rpv.Visible = false;
                this.rmpTabContent.PageViews[0].Visible = true;
            }
        }

        private void AddScripts()
        {
            // enable warning before unloading the page
            RadScriptManager.RegisterStartupScript(this, GetType(),
                "startup_warn",
                "\twarnBeforeUnload = true;\n\tenableWarning();\n\twarningBeforeUnload = \"" + HttpUtility.JavaScriptStringEncode(String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_LeaveConfrmation"), (string)GetGlobalResourceObject("Controls", "Object_Property"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text)) + "\";\n",
                true);

            // disable warning before postback
            RadScriptManager.RegisterOnSubmitStatement(this, GetType(),
                "onsubmit_disable_warn",
                "disableWarning();");
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.ParseQueryString();
                this.BindControls();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();

                    SetNames();
                }
            }

            //enter button
            this.Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                txtName.Focus();
            }

            AddScripts();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                int pid = this.SaveData();

                if (pid == -1)
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Property"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_Property"));

                    rwError.VisibleOnPageLoad = true;
                }
                else
                {
                    if (this.Mode == FormMode.Edit)
                    {
                        Response.Redirect("~/Forms/Properties/PropertyList.aspx");
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("~/Forms/Properties/PropertyDetails.aspx?id=");
                        sb.Append(pid);
                        sb.Append("&mode=");
                        sb.Append((int)FormMode.Edit);
                        Response.Redirect(sb.ToString());
                    }
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Property"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Properties/PropertyList.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Properties/PropertyList.aspx");
        }

        protected void ValidatePercentage(object source, ServerValidateEventArgs args)
        {
            decimal maxPercentage = 100.00M;
            string percentString = args.Value;
            decimal priceDecimal = 0;
            args.IsValid = decimal.TryParse(percentString, out priceDecimal);
            if (args.IsValid && (priceDecimal > maxPercentage || priceDecimal < 0))
            {
                args.IsValid = false;
                ((CustomValidator)source).ErrorMessage = this.GetLocalResourceObject("ValPercentageOverflow.ErrorMessage").ToString();
            }
        }        

        protected void RequiredFieldValidator_PreRender(object sender, EventArgs e)
        {
            RegularExpressionValidator regVal = sender as RegularExpressionValidator;
            string exp = @"^(\-)?\d{1,3}(\<NumberSeparator>\d{0,6})?";
            regVal.ValidationExpression = exp.Replace("<NumberSeparator>", CurrentCultureInfoSet.NumberSeparator);
        }

        protected void RequiredFieldValidatorCommission_PreRender(object sender, EventArgs e)
        {
            RegularExpressionValidator regVal = sender as RegularExpressionValidator;
            string exp = @"^\d{1,8}(\<NumberSeparator>\d{0,2})?";
            regVal.ValidationExpression = exp.Replace("<NumberSeparator>", CurrentCultureInfoSet.NumberSeparator);
        }

        protected void AltitudeRegexValidator_PreRender(object sender, EventArgs e)
        {
            RegularExpressionValidator regVal = sender as RegularExpressionValidator;
            string exp = @"^\d{1,8}(\<NumberSeparator>\d{0,2})?";
            regVal.ValidationExpression = exp.Replace("<NumberSeparator>", CurrentCultureInfoSet.NumberSeparator);
        }

        protected void txtZipCode_TextChanged(object sender, EventArgs e)
        {
            if ((Mode == FormMode.Add) && (this.rcbCountry.SelectedValue.ToLower() == CountryUS.Value.ToString()))
            {
                int zipCode;
                if (int.TryParse(txtZipCode.Text, out zipCode))
                {
                    UserRole uploadManager = RequestManager.Services.UsersService.GetManagerRoleForZipCode(txtZipCode.Text);
                    if (uploadManager != null)
                    {
                        rcbUploadManager.Items.Clear();
                        this.rcbUploadManager.Items.Add(new RadComboBoxItem(uploadManager.User.FullName.ToString(), uploadManager.User.UserID.ToString()));

                        var uploadManagers = RequestManager.Services.UsersService.GetUsersByRole(RoleLevel.Manager).OrderBy(u => u.FullName);
                        foreach (User obj in uploadManagers)
                        {
                            if (obj.UserID == uploadManager.User.UserID)
                                continue;
                            this.rcbUploadManager.Items.Add(new RadComboBoxItem(obj.FullName.ToString(), obj.UserID.ToString()));
                        }
                    }
                }
            }
        }

        protected void cvLockboxData_ServerValidate(object sender, ServerValidateEventArgs args)
        {
            args.IsValid = true;
            if(this.Mode == FormMode.Edit)
            {
                if (rcbKeyProcessType.SelectedValue == ((int)KeyProcessType.Lockbox).ToString())
                {
                    Property property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyId.Value);
                    if (property != null)
                    {
                        var units = RequestManager.Services.UnitsService.GetUnitsByPropertyId(property.PropertyID).ToList();
                        foreach (var unit in units)
                        {
                            if (string.IsNullOrWhiteSpace(unit.KeysLockboxCode) || string.IsNullOrWhiteSpace(unit.KeysLockboxLocation))
                            {
                                args.IsValid = false;
                                return;
                            }
                        }
                    }
                }
            }
        }

        protected void cvPropertyCode_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;
            var p = RequestManager.Services.PropertiesService.GetPropertyByCode(this.txtCode.Text);
            if (p == null)
                args.IsValid = true;
            else if (Mode == FormMode.Edit)
            {
                if (p.PropertyID == this.PropertyId.Value)
                    args.IsValid = true;
            }
        }

        #endregion       
    }
}