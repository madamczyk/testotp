﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Properties
{
    public partial class Tags : System.Web.UI.UserControl
    {

        public int? PropertyID
        {
            get { return ViewState["UnitTypeId"] as int?; }
            set { ViewState["UnitTypeId"] = value; }
        }
        public List<DataAccess.PropertyTag> NewItems
        {
            get
            {
                if (ViewState["NewItems"] == null)
                {
                    ViewState["NewItems"] = new List<DataAccess.PropertyTag>();
                }
                return ViewState["NewItems"] as List<DataAccess.PropertyTag>;
            }
            set { ViewState["NewItems"] = value; }
        }

        #region Methods

        private void BindGrid()
        {
            this.tagsLinqDS.DataResolver = () =>
            {
                IQueryable<Tag> result = RequestManager.Services.TagsService.GetTags().AsQueryable();
                //rgUnitTypes.ApplyFilter<UnitType>(ref result, "Name", (q, f) => q.Where(p => p.Name.ToString().Contains(f)));
                //rgUnitTypes.ApplyFilter<UnitType>(ref result, "Destination.DestinationName", (q, f) => q.Where(p => p.Destination.DestinationName.ToString().Contains(f)));

                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgtags_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgtags_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Tag obj = (Tag)item.DataItem;
                CheckBox cb = e.Item.FindControl("cbTag") as CheckBox;

                cb.Attributes.Add("tagID", obj.TagID.ToString());

                if (PropertyID.HasValue)
                {
                    if (RequestManager.Services.PropertiesService.GetPropertyTag(this.PropertyID.Value, obj.TagID) != null)
                    {
                        cb.Checked = true;
                    }
                }
            }
        }

        protected void rgtags_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgtags.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgtags.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgtags.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgtags.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgtags.MasterTableView.Rebind();
            }
        }

        protected void cbTags_CheckedChanged(object sender, EventArgs args)
        {
            CheckBox chbox = sender as CheckBox;

            if (chbox == null)
                return;

            if (String.IsNullOrEmpty(chbox.Attributes["tagID"]))
                return;

            int tagId = int.Parse(chbox.Attributes["tagID"]);

            if (RequestManager.Services.TagsService.GetTagById(tagId) == null)
            {
                BindGrid();
                return;
            }

            if (chbox.Checked)
            {
                PropertyTag pt = new PropertyTag();
                pt.Tag = RequestManager.Services.TagsService.GetTagById(tagId);
                if (!PropertyID.HasValue)
                {
                    NewItems.Add(pt);
                    return;
                }
                pt.Property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyID.Value);
                RequestManager.Services.PropertiesService.AddPropertyTag(pt);
            }
            else
            {
                if (!this.PropertyID.HasValue)
                {
                    NewItems.Remove(NewItems.Where(p => p.Tag.TagID == tagId).SingleOrDefault());
                    return;
                }
                PropertyTag pt = RequestManager.Services.PropertiesService.GetPropertyTag(this.PropertyID.Value, tagId);
                RequestManager.Services.PropertiesService.RemovePropertyTag(pt.PropertyTagID);
            }
            RequestManager.Services.SaveChanges();
        }

        #endregion Event Handlers
    }
}