﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyRawDatas.ascx.cs" Inherits="IntranetApp.Controls.Properties.PropertyRawDatas" %>
<%@ Import Namespace="Common.Extensions" %>

<script type="text/javascript">
    function ValidateRawData() {
        return true;
    }

    function Disable() {
        var btn = document.getElementById('<%= btnUploadData.ClientID %>');

        if (btn != null) {
            btn.disabled = true;
        }
    }

    function Enable(radAsyncUpload, args) {
        var btn = document.getElementById('<%= btnUploadData.ClientID %>');

        if (btn != null) {
            btn.disabled = false;
        }
    }

    function cvFilePresentValidation(source, args) {
        var upload = $find('<%= rauImageUpload.ClientID %>');

        args.IsValid = upload.getUploadedFiles().length > 0;
    }

    function cvFileExistsValidation(source, args) {
        var upload = $find('<%= rauImageUpload.ClientID %>');
        var inputs = upload._uploadedFiles;

        if (inputs.length > 0) {
            args.IsValid = inputs[0].fileInfo.ContentLength > 0;
        } else
            args.IsValid = true;
    }
</script>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgRawData">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgRawData" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    <telerik:RadGrid
        AutoGenerateColumns="False"
        ID="rgRawData"
        AllowSorting="True"
        runat="server"
        AllowFilteringByColumn="True"
        AllowPaging="True"
        PageSize="20"
        DataSourceID="rawDataLinqDS"
        ClientSettings-EnablePostBackOnRowClick="false"
        OnItemCommand="rgRawData_ItemCommand"
        OnItemDataBound="rgRawData_ItemDataBound"
        OnDataBound="rgRawData_DataBound" Width="100%"
        ClientSettings-ClientEvents-OnColumnClick="disableWarning">
        <ClientSettings EnableRowHoverStyle="true" Selecting-AllowRowSelect="true" />
        <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
        <MasterTableView TableLayout="Fixed" DataKeyNames="PropertyRawDataRecordID" DataSourceID="rawDataLinqDS" CommandItemDisplay="None">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="RawRecordType" SortOrder="Ascending" />
            </SortExpressions>
            <Columns>
                <telerik:GridTemplateColumn DataField="RawRecordType" HeaderText="Record Type" UniqueName="RawRecordType" SortExpression="RawRecordType" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="200px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# DataAccess.Enums.EnumConverter.GetValue((DataAccess.Enums.PropertyRawRecordType)Eval("RawRecordType")) %>
                    </ItemTemplate>
                    <FilterTemplate>
                        <telerik:RadComboBox runat="server" ID="rcbRawRecordType" AutoPostBack="true" EnableViewState="true" OnInit="rcbRawRecordType_Init" OnSelectedIndexChanged="rcbRawRecordType_SelectedIndexChanged" OnPreRender="rcbRawRecordType_PreRender" MaxHeight="300px" Width="90%" />
                    </FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridBoundColumn DataField="RecordName" UniqueName="RecordName" HeaderText="Name" SortExpression="RecordName" HeaderStyle-Width="200px" ShowFilterIcon="false" FilterControlWidth="80%" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />

                <telerik:GridTemplateColumn DataField="Description" HeaderText="Description" UniqueName="Description" SortExpression="Description" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" AutoPostBackOnFilter="true" FilterControlWidth="70%" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# (string)Eval("Description") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="RawRecordContent" HeaderText="Content" UniqueName="RawRecordContent" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="false" AutoPostBackOnFilter="true" HeaderStyle-Width="150px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <a runat="server" id="rawRecordContentLink" />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png" HeaderStyle-Width="30px" />
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <br />
    <asp:Panel runat="server" ID="addRawDataPanel">
        <h1><asp:Label runat="server" ID="lblAddRawData" meta:resourcekey="lblAddRawData" /></h1>
        <asp:ValidationSummary ID="vsSummaryRawData" ValidationGroup="RawDataEntry" DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
        <br />
        <table width="100%">
            <tr>
                <td><asp:Label runat="server" ID="lblRecordName" meta:resourcekey="lblRecordName" /></td>
                <td><telerik:RadTextBox runat="server" TabIndex="1" ID="txtRecordName" Enabled="true" Width="450px" MaxLength="50" /><asp:RequiredFieldValidator ID="rfvRecordName" ValidationGroup="RawDataEntry" runat="server" Display="Dynamic" ControlToValidate="txtRecordName" meta:resourcekey="rfvRecordName" Text="*" CssClass="error" /></td>
            </tr>
            <tr>
                <td width="100px"><asp:Label runat="server" ID="lblRecordType" meta:resourcekey="lblRecordType" /></td>
                <td><telerik:RadComboBox runat="server" ID="rcbRecordType" TabIndex="2" AutoPostBack="false" Enabled="true" Width="250px" OnPreRender="rcbRecordType_PreRender" /></td>
            </tr>
            <tr>
                <td><asp:Label runat="server" ID="lblDescription" meta:resourcekey="lblDescription" /></td>
                <td><telerik:RadTextBox runat="server" TabIndex="3" ID="txtDescription" Enabled="true" Width="450px" Rows="4" TextMode="MultiLine" /><asp:RequiredFieldValidator ID="rfvDescription" ValidationGroup="RawDataEntry" runat="server" Display="Dynamic" ControlToValidate="txtDescription" meta:resourcekey="rfvDescription" Text="*" CssClass="error" /></td>
            </tr>
            <tr>
                <td><asp:Label runat="server" ID="lblFile" meta:resourcekey="lblFile" /></td>
                <td><telerik:RadAsyncUpload runat="server" ID="rauImageUpload" MultipleFileSelection="Disabled" OnClientFilesSelected="Disable" OnClientFileUploaded="Enable" MaxFileInputsCount="1" TabIndex="4" OnClientValidationFailed="FileUploadFailed" />
                    <asp:CustomValidator ID="cvFilePresent" runat="server" Text="*" CssClass="error" Display="Dynamic" meta:resourcekey="cvFilePresent" ValidationGroup="RawDataEntry" ClientValidationFunction="cvFilePresentValidation" />
                    <asp:CustomValidator ID="cvFileExists" runat="server" Text="*" CssClass="error" Display="Dynamic" meta:resourcekey="cvFileExists" ValidationGroup="RawDataEntry" ClientValidationFunction="cvFileExistsValidation" /></td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btnUploadData" runat="server" meta:resourcekey="btnUploadData" OnClick="btnUploadData_Click" ValidationGroup="RawDataEntry" />
    </asp:Panel>
</asp:Panel>

<otpDS:SimpleDataSource ID="rawDataLinqDS" runat="server" />