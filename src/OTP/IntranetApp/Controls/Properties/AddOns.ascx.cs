﻿using BusinessLogic.Managers;
using Common;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Properties
{
    public partial class AddOns : System.Web.UI.UserControl
    {
        public int? PropertyID
        {
            get { return ViewState["UnitId"] as int?; }
            set { ViewState["UnitId"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemId"] as int?; }
            set { ViewState["EditedItemId"] = value; }
        }

        public List<DataAccess.PropertyAddOn> NewItems
        {
            get
            {
                if (ViewState["NewItems"] == null)
                {
                    ViewState["NewItems"] = new List<DataAccess.PropertyAddOn>();
                }
                return ViewState["NewItems"] as List<DataAccess.PropertyAddOn>;
            }
            set { ViewState["NewItems"] = value; }
        }

        private int _nextProductParameterId;

        public int NextProductParameterId
        {
            get
            {
                int result = _nextProductParameterId;
                _nextProductParameterId++;
                return result;
            }
            set
            {
                _nextProductParameterId = value;
            }
        }

        #region Methods

        private void BindGrid()
        {
            this.addOnsLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.PropertyAddOn> result = null;
                List<DataAccess.PropertyAddOn> resultList;

                if (this.PropertyID.HasValue)
                    result = RequestManager.Services.PropertyAddOnsService.GetAddOnsByPropertyId(PropertyID.Value).AsQueryable();
                
                if (result != null)
                    resultList = new List<DataAccess.PropertyAddOn>(result);
                else
                    resultList = new List<DataAccess.PropertyAddOn>();

                resultList.AddRange(NewItems);

                if (resultList.Count != 0)
                    NextProductParameterId = resultList.Max(ob => ob.PropertyAddOnID) + 1;
                else
                    NextProductParameterId = 1;

                return resultList.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgAddOns_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int propertyAddonId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyAddOnID"].ToString());

                if (RequestManager.Services.PropertyAddOnsService.GetById(propertyAddonId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyAddOn"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_PropertyAddOn"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
            }
            else if (e.CommandName == "Edit")
            {
                int editedId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyAddOnID"].ToString());
                EditedItemId = editedId;
            }
            else if (e.CommandName == "Cancel")
            {
                EditedItemId = null;
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int propertyAddonId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyAddOnID"].ToString());

                if (this.PropertyID.HasValue)
                {
                    RequestManager.Services.PropertyAddOnsService.DeleteAddOn(propertyAddonId);
                }
                else
                    NewItems.Remove(NewItems.Where(u => u.PropertyAddOnID == propertyAddonId).FirstOrDefault()); 

            }
        }

        protected void rgAddOns_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                PropertyAddOn obj = (PropertyAddOn)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                CheckBox cbMandatory = item.FindControl("cbIsMandatory") as CheckBox;

                cbMandatory.Checked = obj.IsMandatory;
                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgAddOns.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_PropertyAddOn"), obj.AddOnTitle.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyAddOn"), obj.AddOnTitle.ToString()));

                item["Unit"].Text = EnumConverter.GetValue((PropertyAddOnUnit)obj.Unit).ToString();
            }
        }

        protected void rgAddOns_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgAddOns.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgAddOns.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgAddOns.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgAddOns.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgAddOns.MasterTableView.Rebind();
            }
        }

        protected void rgAddOns_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    DataAccess.PropertyAddOn addOn = new DataAccess.PropertyAddOn();
                    ReadGridItemData(args.Item, addOn);
                    if (PropertyID.HasValue)
                    {
                        addOn.Property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyID.Value);
                        RequestManager.Services.PropertyAddOnsService.Add(addOn);
                    }
                    else
                    {
                        NewItems.Add(addOn);
                    }
                    this.rgAddOns.Rebind();
                }
            }
        }

        protected void rgAddOns_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("PropertyAddOnID"));
                    DataAccess.PropertyAddOn addOn = null;
                    if (PropertyID.HasValue)
                    {
                        addOn = RequestManager.Services.PropertyAddOnsService.GetById(id);
                        ReadGridItemData(editForm, addOn);
                        RequestManager.Services.SaveChanges();
                    }
                    else
                    {
                        addOn = NewItems.Where(ob => ob.PropertyAddOnID == id).FirstOrDefault();
                        ReadGridItemData(editForm, addOn);
                    }
                    
                    this.BindGrid();
                    this.rgAddOns.Rebind();
                }
                int reportId = int.Parse(args.Item.OwnerTableView.DataKeyValues[args.Item.ItemIndex]["PropertyAddOnID"].ToString());
                EditedItemId = reportId;
            }
        }

        private void ReadGridItemData(GridItem item, DataAccess.PropertyAddOn addOn)
        {
            addOn.SetTitleValue((item.FindControl("txtAddOnTitle") as RadTextBox).Text);
            addOn.SetDescValue((item.FindControl("txtAddOnDesc") as RadTextBox).Text);
            addOn.IsMandatory = ((item.FindControl("cbIsMandatory") as CheckBox).Checked);
            addOn.Unit = int.Parse((item.FindControl("rcbUnit") as RadComboBox).SelectedValue);
            addOn.Price = decimal.Parse((item.FindControl("txtPrice") as RadTextBox).Text);
            addOn.Tax = RequestManager.Services.TaxesService.GetTaxById(int.Parse((item.FindControl("rcbTax") as RadComboBox).SelectedValue));
            addOn.PersistI18nValues();
        }

        #endregion Event Handlers

        protected void ddlUnit_PreRender(object sender, EventArgs e)
        {
            RadComboBox ddl = sender as RadComboBox;
            ddl.Items.Add(new RadComboBoxItem("", "-1"));

            foreach (PropertyAddOnUnit us in Enum.GetValues(typeof(PropertyAddOnUnit)))
                ddl.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(us), ((int)us).ToString()));
            if (PropertyID.HasValue)
            {
                              
                if (EditedItemId.HasValue && EditedItemId.Value != 0)
                {
                    DataAccess.PropertyAddOn addOn = RequestManager.Services.PropertyAddOnsService.GetById(EditedItemId.Value);
                    ddl.SelectedValue = addOn.Unit.ToString();
                }
            }
        }

        protected void ddlTax_PreRender(object sender, EventArgs e)
        {
            RadComboBox ddl = sender as RadComboBox;
            DataAccess.Property property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyID.Value);
            IEnumerable<Tax> taxes = RequestManager.Services.TaxesService.GetTaxes().Where(t => t.Destination.DestinationID == property.Destination.DestinationID);
            foreach (Tax ut in taxes)
            {
                ddl.Items.Add(new RadComboBoxItem(ut.NameCurrentLanguage, ut.TaxId.ToString()));
            }
            if (EditedItemId.HasValue && EditedItemId.Value != 0)
            {
                DataAccess.PropertyAddOn addon = RequestManager.Services.PropertyAddOnsService.GetById(EditedItemId.Value);
                ddl.SelectedValue = addon.Tax.TaxId.ToString();
            }
        }

        protected void RequiredFieldValidator_PreRender(object sender, EventArgs e)
        {
            RegularExpressionValidator regVal = sender as RegularExpressionValidator;
            string exp = @"^\d*(\<NumberSeparator>\d*)?";
            regVal.ValidationExpression = exp.Replace("<NumberSeparator>", CurrentCultureInfoSet.NumberSeparator);
        }
    }
}