﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UnitTypes.ascx.cs" Inherits="IntranetApp.Controls.Properties.UnitTypes" %>

<script type="text/javascript">
    function RowClick(sender, args) {
        var rowId = args.getDataKeyValue("UnitTypeID");
        openUnitTypeDialog(rowId, 1);
        args.stopPropagation();
        return false;
    }
</script>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgUnitTypes">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgUnitTypes" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<telerik:RadGrid 
    AutoGenerateColumns="False" 
    SaveScrollPosition="True"
    ID="rgUnitTypes" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="unitTypesLinqDS"
	ClientSettings-EnablePostBackOnRowClick="true" 
    OnItemCommand="rgUnitTypes_ItemCommand" 
    OnDataBound="rgUnitTypes_DataBound"
    OnUpdateCommand="rgUnitTypes_UpdateCommand"
    OnItemDataBound="rgUnitTypes_ItemDataBound"
    OnInsertCommand="rgUnitTypes_InsertCommand" Width="100%"
    ClientSettings-ClientEvents-OnColumnClick="disableWarning">
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelecting="RowClick" />
        <Scrolling SaveScrollPosition="true" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView Width="100%" TableLayout="Fixed" DataKeyNames="UnitTypeID" ClientDataKeyNames="UnitTypeID" DataSourceID="unitTypesLinqDS" CommandItemDisplay="Bottom" >
        <Columns>
            <telerik:GridBoundColumn DataField="UnitTypeTitle" FilterControlWidth="150px" UniqueName="UnitTypeTitle" HeaderText="Title" SortExpression="UnitTypeTitleCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="UnitTypeDesc" FilterControlWidth="150px" UniqueName="UnitTypeDesc" HeaderText="Description" SortExpression="UnitTypeDescCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="UnitTypeCode" FilterControlWidth="150px" UniqueName="UnitTypeCode" HeaderText="Code" SortExpression="UnitTypeCode" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridButtonColumn>			
		</Columns>
       <%-- <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="UnitTypes"
                        HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                        EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblTitle" meta:resourceKey="lblTitle"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtUnitTypeTitle" MaxLength="50"
                                    Text='<%# Eval("UnitTypeTitleCurrentLanguage") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="UnitTypes" ID="rfvNameProductParam"
                                    Text="*" ControlToValidate="txtUnitTypeTitle" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValTitle"></asp:RequiredFieldValidator>
                            </td>
                        </tr> 
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblCode" meta:resourceKey="lblCode"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtUnitTypeCode" MaxLength="50"
                                    Text='<%# Eval("UnitTypeCode") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="UnitTypes" ID="RequiredFieldValidator1"
                                    Text="*" ControlToValidate="txtUnitTypeCode" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValCode"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDescription" meta:resourceKey="lblDescription"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtUnitTypeDesc" MaxLength="50" TextMode="MultiLine" Rows="6"
                                    Text='<%# Eval("UnitTypeDescCurrentLanguage") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="UnitTypes" ID="RequiredFieldValidator2"
                                    Text="*" ControlToValidate="txtUnitTypeDesc" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDescription"></asp:RequiredFieldValidator>
                            </td>
                        </tr>                                   
                        <tr>
                            <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                <div style="position: relative; width: 415px;">
                                    <!-- workaround for issue with RadButtons not being aligned vertically -->
                                    <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="UnitTypes"
                                        VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        CausesValidation="true">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                    </telerik:RadButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>--%>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<otpDS:SimpleDataSource ID="unitTypesLinqDS" runat="server" />