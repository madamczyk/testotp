﻿using BusinessLogic.Managers;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Properties
{
    public partial class UnitTypes : System.Web.UI.UserControl
    {
        public int? PropertyID
        {
            get { return ViewState["UnitTypeId"] as int?; }
            set { ViewState["UnitTypeId"] = value; }
        }

        public List<DataAccess.UnitType> NewItems
        {
            get
            {
                if (ViewState["NewItems"] == null)
                {
                    ViewState["NewItems"] = new List<DataAccess.UnitType>();
                }
                return ViewState["NewItems"] as List<DataAccess.UnitType>;
            }
            set { ViewState["NewItems"] = value; }
        }

        private int _nextProductParameterId;

        public int NextProductParameterId
        {
            get
            {
                int result = _nextProductParameterId;
                _nextProductParameterId++;
                return result;
            }
            set
            {
                _nextProductParameterId = value;
            }
        }

        #region Methods

        private void BindGrid()
        {
            this.unitTypesLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.UnitType> result = null;
                List<DataAccess.UnitType> resultList;

                if(this.PropertyID.HasValue)
                    result = RequestManager.Services.UnitTypesService.GetUnitTypesByPropertyId(PropertyID.Value).AsQueryable();

                if (result != null)
                    resultList = new List<DataAccess.UnitType>(result);
                else
                    resultList = new List<DataAccess.UnitType>();

                resultList.AddRange(NewItems);

                if (resultList.Count != 0)
                    NextProductParameterId = resultList.Max(ob => ob.UnitTypeID) + 1;
                else
                    NextProductParameterId = 1;

                return resultList.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgUnitTypes_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int unitTypeID = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitTypeID"].ToString());

                if (RequestManager.Services.UnitTypesService.GetUnitTypeById(unitTypeID) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int unitTypeID = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitTypeID"].ToString());

                if (PropertyID.HasValue)
                {
                    DataAccess.UnitType obj;
                    if ((obj = RequestManager.Services.UnitTypesService.GetUnitTypeById(unitTypeID)) != null)
                    {
                        if (obj.Units.Count != 0)
                        {
                            string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"), obj.UnitTypeTitleCurrentLanguage.ToString());
                            string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"), obj.UnitTypeTitleCurrentLanguage.ToString());

                            RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                        }
                        else
                        {
                            RequestManager.Services.UnitTypesService.DeleteUnitType(unitTypeID);
                        }
                    }                    
                }
                else
                    NewItems.Remove(NewItems.Where(u => u.UnitTypeID == unitTypeID).FirstOrDefault());
            }
            else if (e.CommandName == "InitInsert")
            {
                e.Canceled = true; 
            }
        }

        protected void rgUnitTypes_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                DataAccess.UnitType obj = (DataAccess.UnitType)item.DataItem;
                e.Item.Attributes["unit-type-id"] = obj.UnitTypeID.ToString();
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgUnitTypes.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"), obj.UnitTypeTitle.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"), obj.UnitTypeTitle.ToString()));
            }
            else if (e.Item is GridCommandItem)
            {
                if (PropertyID.HasValue)
                {
                    LinkButton lbAdd = e.Item.FindControl("InitInsertButton") as LinkButton;
                    Button btnAdd = e.Item.FindControl("AddNewRecordButton") as Button;
                    string onAddClick = "openUnitTypeDialog(" + PropertyID.Value.ToString() + "," + ((int)FormMode.Add).ToString() + "); return false;";
                    lbAdd.Attributes["onclick"] = onAddClick;
                    btnAdd.Attributes["onclick"] = onAddClick;
                }
            }
        }

        protected void rgUnitTypes_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgUnitTypes.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgUnitTypes.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgUnitTypes.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgUnitTypes.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgUnitTypes.MasterTableView.Rebind();
            }
        }

        protected void rgUnitTypes_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    DataAccess.UnitType newUnit = new DataAccess.UnitType();
                    ReadGridItemData(args.Item, newUnit);
                    if (PropertyID.HasValue)
                    {
                        newUnit.Property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyID.Value);
                        RequestManager.Services.UnitTypesService.AddUnitType(newUnit);
                        RequestManager.Services.SaveChanges();
                    }
                    else
                    {
                        NewItems.Add(newUnit);
                    }
                    this.rgUnitTypes.MasterTableView.Rebind();
                }
            }
        }

        protected void rgUnitTypes_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("UnitTypeID"));
                    
                    DataAccess.UnitType unitEdit;
                    if (PropertyID.HasValue)
                    {
                        unitEdit = RequestManager.Services.UnitTypesService.GetUnitTypeById(id);
                        ReadGridItemData(editForm, unitEdit);
                        RequestManager.Services.SaveChanges();
                    }
                    else
                    {
                        unitEdit = NewItems.Where(ob => ob.UnitTypeID == id).FirstOrDefault();
                        ReadGridItemData(editForm, unitEdit);
                    }         
                }
            }
        }

        private void ReadGridItemData(GridItem item, DataAccess.UnitType unitType)
        {
            unitType.UnitTypeCode = (item.FindControl("txtUnitTypeCode") as RadTextBox).Text;
            unitType.SetTitleValue((item.FindControl("txtUnitTypeTitle") as RadTextBox).Text);
            unitType.SetDescValue((item.FindControl("txtUnitTypeDesc") as RadTextBox).Text);
        }

        #endregion Event Handlers
    }
}