﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyList.ascx.cs" Inherits="IntranetApp.Controls.Properties.PropertyList" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgTaxes" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgProperties" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="propertiesLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgProperties_ItemCommand" 
        OnItemDataBound="rgProperties_ItemDataBound" 
        OnDataBound="rgProperties_DataBound">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="PropertyID" DataSourceID="propertiesLinqDS">
			<Columns>
				<telerik:GridBoundColumn DataField="PropertyNameCurrentLanguage" FilterControlWidth="150px" UniqueName="PropertyName" HeaderText="Name" SortExpression="PropertyNameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Destination.DestinationNameCurrentLanguage" FilterControlWidth="150px" UniqueName="DestinationName" HeaderText="Market" SortExpression="Destination.DestinationNameCurrentLanguage" HeaderStyle-Width="300px" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DictionaryCountry.CountryNameCurrentLanguage" FilterControlWidth="150px" UniqueName="Country" HeaderText="Country" SortExpression="DictionaryCountry.CountryNameCurrentLanguage" HeaderStyle-Width="300px" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="City" FilterControlWidth="150px" UniqueName="City" HeaderText="City" SortExpression="City" HeaderStyle-Width="300px" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="User.FullName" FilterControlWidth="150px" UniqueName="User.FullName" HeaderText="Owner" SortExpression="User.FullName" HeaderStyle-Width="300px" ShowSortIcon="true" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="PropertyLive" HeaderText="Is Activated" UniqueName="PropertyLive" SortExpression="PropertyLive" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="false">  
					<HeaderStyle Width="100px"></HeaderStyle>
					<ItemStyle CssClass="link" />
                    <ItemTemplate> 
                        <%# (Boolean.Parse(Eval("PropertyLive").ToString())) ? "Yes" : "No" %> 
                     </ItemTemplate> 
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                        <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>			
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="PropertyNameCurrentLanguage" SortOrder="Ascending" />
            </SortExpressions>
		</MasterTableView>
		<HeaderContextMenu EnableImageSprites="True">
		</HeaderContextMenu>
	</telerik:RadGrid>
    <br />
    <telerik:RadButton runat="server" ID="btnAddProperty" Text="Add" OnClick="btnAddProperty_Click" />
</asp:Panel>

<otpDS:SimpleDataSource ID="propertiesLinqDS" runat="server" />