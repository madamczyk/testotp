﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Properties
{
    public partial class PropertyList : System.Web.UI.UserControl
    {
        #region Methods

        private void BindGrid()
        {
            this.propertiesLinqDS.DataResolver = () =>
            {
                IQueryable<Property> result = RequestManager.Services.PropertiesService.GetProperties().AsQueryable();
                rgProperties.ApplyFilter<Property>(ref result, "PropertyName", (q, f) => q.Where(p => p.PropertyNameCurrentLanguage.ToLower().Contains(f.ToLower())));
                rgProperties.ApplyFilter<Property>(ref result, "DestinationName", (q, f) => q.Where(p => p.Destination.DestinationNameCurrentLanguage.ToLower().Contains(f.ToLower())));
                rgProperties.ApplyFilter<Property>(ref result, "Country", (q, f) => q.Where(p => p.DictionaryCountry.CountryNameCurrentLanguage.ToLower().Contains(f.ToLower())));
                rgProperties.ApplyFilter<Property>(ref result, "City", (q, f) => q.Where(p => p.City.ToLower().Contains(f.ToLower())));
                rgProperties.ApplyFilter<Property>(ref result, "User.FullName", (q, f) => q.Where(p => p.User.FullName.ToLower().Contains(f.ToLower())));
                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgProperties_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int propertyId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyID"].ToString());

                if (RequestManager.Services.PropertiesService.GetPropertyById(propertyId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Property"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_Property"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/Properties/PropertyDetails.aspx?id=");
                    sb.Append(propertyId);
                    sb.Append("&mode=");
                    sb.Append((int)FormMode.Edit);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int propertyId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyID"].ToString());

                if (RequestManager.Services.PropertiesService.GetPropertyById(propertyId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Property"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Property"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else if (RequestManager.Services.PropertiesService.HasAnyReservation(propertyId))
                {
                    Property property = RequestManager.Services.PropertiesService.GetPropertyById(propertyId);

                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Property"), property.PropertyName.ToString());
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_Property"), property.PropertyName.ToString());

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                }
                else
                {
                    RequestManager.Services.PropertiesService.RemoveProperty(propertyId);
                }
            }
        }

        protected void rgProperties_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Property obj = (Property)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgProperties.ClientID, String.Format((string)GetLocalResourceObject("Dialog_Delete_Confirm_Text"), obj.PropertyName.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Property"), obj.PropertyName.ToString()));
            }
        }

        protected void rgProperties_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgProperties.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgProperties.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgProperties.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgProperties.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgProperties.MasterTableView.Rebind();
            }
        }

        protected void btnAddProperty_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("~/Forms/Properties/PropertyDetails.aspx?");
            sb.Append("mode=");
            sb.Append((int)FormMode.Add);

            Response.Redirect(sb.ToString());
        }

        #endregion Event Handlers
    }
}