﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentUpdateRequestsList.ascx.cs" Inherits="IntranetApp.Controls.Properties.ContentUpdateRequestsList" %>
<%@ Import Namespace="Common.Extensions" %>
<%@ Import Namespace="Common.Helpers" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgContentUpdateRequests" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgContentUpdateRequests" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="contentUpdateRequestsLinqDS"
        OnItemCommand="rgContentUpdateRequests_ItemCommand" 
        OnDataBound="rgContentUpdateRequests_DataBound">
        <ClientSettings EnableRowHoverStyle="true" Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="true" />
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="ContentUpdateRequestId" DataSourceID="contentUpdateRequestsLinqDS">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="RequestDateTime" SortOrder="Descending" />
            </SortExpressions>
			<Columns>           
				<telerik:GridBoundColumn DataField="Property.PropertyNameCurrentLanguage" UniqueName="PropertyName" HeaderText="Property Name" SortExpression="Property.PropertyNameCurrentLanguage" HeaderStyle-Width="300px" FilterControlWidth="80%" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />	

                <telerik:GridTemplateColumn DataField="RequestDateTime" HeaderText="Request Date" UniqueName="RequestDate" SortExpression="RequestDateTime" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="150px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# ((DateTime)Eval("RequestDateTime")).ToLocalizedDateTimeString() %>
                    </ItemTemplate>
                    <FilterTemplate>
                        <table style="border: 0; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_From %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpRequestDateFrom" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpRequestDateFrom_PreRender" OnSelectedDateChanged="rdpRequestDateFrom_SelectedDateChanged">
                                        <DateInput ID="diRequestDateFrom" runat="server" 
                                            DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                            DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td style="border: 0; padding: 0; margin: 0;"><asp:Label runat="server" Text="<%$ Resources: Controls, Label_Date_To %>" />&nbsp;</td>
                                <td style="border: 0; padding: 0; margin: 0;"><telerik:RadDatePicker ID="rdpRequestDateTo" Width="100px" runat="server" AutoPostBack="true" OnPreRender="rdpRequestDateTo_PreRender" OnSelectedDateChanged="rdpRequestDateTo_SelectedDateChanged" >
                                    <DateInput ID="diRequestDateTo" runat="server" 
                                        DateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# DateTimeHelper.GetLocalizedDatePattern() %>" />
                                </telerik:RadDatePicker></td>
                            </tr>
                        </table>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="Description" HeaderText="Description" UniqueName="Description" SortExpression="Description" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" AutoPostBackOnFilter="true" FilterControlWidth="70%" ItemStyle-CssClass="link">
                        <ItemTemplate>
                            <%# HttpUtility.HtmlEncode(((string)Eval("Description")).Limit(_previewLength)) %>
                        </ItemTemplate>
                </telerik:GridTemplateColumn>
			</Columns>
		</MasterTableView>
	</telerik:RadGrid>
</asp:Panel>

<otpDS:SimpleDataSource ID="contentUpdateRequestsLinqDS" runat="server" />