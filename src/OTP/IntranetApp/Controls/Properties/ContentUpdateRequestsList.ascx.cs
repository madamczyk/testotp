﻿using BusinessLogic.Managers;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Properties
{
    public partial class ContentUpdateRequestsList : System.Web.UI.UserControl
    {

        #region Fields

        public DateTime? RequestDateFrom
        {
            get
            {
                if (ViewState["RequestDateFrom"] != null)
                    return (DateTime)ViewState["RequestDateFrom"];

                return null;
            }
            set
            {
                ViewState["RequestDateFrom"] = value;
            }
        }

        public DateTime? RequestDateTo
        {
            get
            {
                if (ViewState["RequestDateTo"] != null)
                    return (DateTime)ViewState["RequestDateTo"];

                return null;
            }
            set
            {
                ViewState["RequestDateTo"] = value;
            }
        }

        public readonly int _previewLength = 100;

        #endregion

        #region Methods

        private void BindGrid()
        {
            contentUpdateRequestsLinqDS.DataResolver = () =>
            {
                IQueryable<ContentUpdateRequest> result = RequestManager.Services.PropertiesService.GetContentUpdateRequests().AsQueryable();

                rgContentUpdateRequests.ApplyFilter<ContentUpdateRequest>(ref result, "PropertyName",
                    (q, f) => q.Where(el => el.Property.PropertyNameCurrentLanguage.ToLower().Contains(f.ToLower())));
                rgContentUpdateRequests.ApplyFilter<ContentUpdateRequest>(ref result, "Description",
                    (q, f) => q.Where(el => el.Description.ToLower().Contains(f.ToLower())));

                if (RequestDateFrom.HasValue)
                    result = result.Where(el => el.RequestDateTime >= this.RequestDateFrom.Value);
                if (RequestDateTo.HasValue)
                    result = result.Where(el => el.RequestDateTime <= this.RequestDateTo.Value);

                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgContentUpdateRequests_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int requestId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ContentUpdateRequestId"].ToString());

                if (RequestManager.Services.PropertiesService.GetContentUpdateRequestById(requestId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ContentUpdateRequest"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_ContentUpdateRequest"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");

                    rgContentUpdateRequests.MasterTableView.Rebind();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/Properties/ContentUpdateRequestDetails.aspx?id=");
                    sb.Append(requestId);

                    Response.Redirect(sb.ToString());
                }

            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgContentUpdateRequests_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgContentUpdateRequests.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgContentUpdateRequests.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgContentUpdateRequests.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgContentUpdateRequests.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgContentUpdateRequests.MasterTableView.Rebind();
            }
        }

        protected void rdpRequestDateFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.RequestDateFrom;
        }

        protected void rdpRequestDateTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;

            picker.SelectedDate = this.RequestDateTo;
        }

        protected void rdpRequestDateFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            RequestDateFrom = e.NewDate;
            rgContentUpdateRequests.MasterTableView.Rebind();
        }

        protected void rdpRequestDateTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            RequestDateTo = e.NewDate;
            rgContentUpdateRequests.MasterTableView.Rebind();
        }

        #endregion Event Handlers

    }
}