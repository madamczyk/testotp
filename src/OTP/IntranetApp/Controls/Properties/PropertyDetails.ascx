﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyDetails.ascx.cs" Inherits="IntranetApp.Controls.Properties.PropertyDetails" %>
<%@ Register TagPrefix="uc" TagName="UnitTypes" Src="~/Controls/Properties/UnitTypes.ascx" %>
<%@ Register TagPrefix="uc" TagName="Amenities" Src="~/Controls/Properties/Amenities.ascx" %>
<%@ Register TagPrefix="uc" TagName="Tags" Src="~/Controls/Properties/Tags.ascx" %>
<%@ Register TagPrefix="uc" TagName="Units" Src="~/Controls/Properties/Units.ascx" %>
<%@ Register TagPrefix="uc" TagName="AddOns" Src="~/Controls/Properties/AddOns.ascx" %>
<%@ Register TagPrefix="uc" TagName="Images" Src="~/Controls/Properties/StaticContent/PropertyImages.ascx" %>
<%@ Register TagPrefix="uc" TagName="RawData" Src="~/Controls/Properties/PropertyRawDatas.ascx" %>
<%@ Register TagPrefix="uc" TagName="Features" Src="~/Controls/Properties/StaticContent/PropertyFeatures.ascx" %>
<%@ Register TagPrefix="uc" TagName="Square" Src="~/Controls/Properties/StaticContent/PropertySquare.ascx" %>
<%@ Register TagPrefix="uc" TagName="AttrDistance" Src="~/Controls/Properties/StaticContent/PropertyDistanceToAttractions.ascx" %>
<%@ Register TagPrefix="uc" TagName="LocalAreaDescr" Src="~/Controls/Properties/StaticContent/PropertyLocalAreaDescription.ascx" %>
<%@ Register TagPrefix="uc" TagName="FloorPlan" Src="~/Controls/Properties/StaticContent/PropertyFloorPlan.ascx" %>
<%@ Register TagPrefix="uc" TagName="UnitTypeDetails" Src="~/Controls/UnitTypes/UnitTypeDetails.ascx" %>
<%@ Register TagPrefix="uc" TagName="Completness" Src="~/Controls/Common/PropertyCompletness.ascx" %>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script type="text/javascript">

    Telerik.Web.UI.RadAsyncUpload.Modules.Silverlight.isAvailable = function () { return false; };
    Telerik.Web.UI.RadAsyncUpload.Modules.Flash.isAvailable = function () { return false; };

    function initialize(lat, lng, zoomValue) {
        var latlng = new google.maps.LatLng(lat, lng);
        var settings = {
            zoom: zoomValue,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU },
            navigationControl: true,
            navigationControlOptions: { style: google.maps.NavigationControlStyle.SMALL },
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

        var companyImage = new google.maps.MarkerImage('../../Images/google-map-pointer.png',
            new google.maps.Size(100, 50),
            new google.maps.Point(0, 0),
            new google.maps.Point(50, 50)
        );


        var companyPos = new google.maps.LatLng(lat, lng);

        var companyMarker = new google.maps.Marker({
            position: companyPos,
            map: map,
            icon: companyImage,
            zIndex: 3
        });

        google.maps.event.addListener(companyMarker, 'click', function () {
            infowindow.open(map, companyMarker);
        });
    }

    function initMap() {
        var lat = document.getElementById('ctl00_ctl00_MainContent_mainContent_PropertyDetailsCtrl_txtLatitude').value.replace(',', '.');
        var lng = document.getElementById('ctl00_ctl00_MainContent_mainContent_PropertyDetailsCtrl_txtLongitude').value.replace(',', '.');
        if (lat != "" && lng != "") {
            document.getElementById('tdGoogleMap').style.display = '';
            initialize(lat, lng, 12);
        }
    }

    function openImageDialog(imgUrl) {
        window.open(
            imgUrl,
            '_blank'
            );
    }

    function openUnitTypeDialog(propertyId, formMode) {
        var divModal = "<div id='modalDialog'><iframe width='670' height='570' frameBorder='0' src='/Forms/UnitTypes/UnitTypeDetails.aspx?id=" + propertyId + " &mode=" + formMode + "'></iframe></div>";
        $(divModal).dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            width: 670,
            height: 600,
            title: "Unit Types",
            overlay: { backgroundColor: "#000000", opacity: 0.5 },
            open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
            close: function (ev, ui) {
                $(".ui-dialog-titlebar-close").show();
                __doPostBack('ctl00$ctl00$MainContent$mainContent$PropertyDetailsCtrl$ctrlUnitTypes$rgUnitTypes$ctl00$ctl03$ctl01$RebindGridButton', '');

            },
        }).dialog('open');
    }

    function openUnitDialog(propertyId, formMode) {

        var lockbox = "1";
        var keyHandlerType = $('#<%= hfKeyHandlerProcessType.ClientID %>').val();
        var selectedKeyType = $('#<%= rcbKeyProcessType.ClientID %>').val();
        if (keyHandlerType == selectedKeyType) {
            lockbox = "0";
        }
        var divModal = "<div id='modalDialog'><iframe width='670' height='650'  frameBorder='0' src='/Forms/Units/UnitDetails.aspx?id=" + propertyId + " &mode=" + formMode + "&lockbox=" + lockbox + "'></iframe></div>";
        $(divModal).dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            width: 670,
            height: 680,
            title: "Units",
            overlay: { backgroundColor: "#000000", opacity: 0.5 },
            open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
            close: function (ev, ui) {
                $(".ui-dialog-titlebar-close").show();
                __doPostBack('ctl00$ctl00$MainContent$mainContent$PropertyDetailsCtrl$ctrlUnits$rgUnit$ctl00$ctl03$ctl01$RebindGridButton', '');

            },
        }).dialog('open');
    }

    function SelectTab() {
        var radTab = $find('<%= rtsPropertyDetails.ClientID %>').findTabByText("Square footage");
    radTab.select();
}


//////Validation helpers
function PropertyNameValidationMarkEvent() {
    var txtName = $find('<%= txtName.ClientID %>');
    MarkInvalid(txtName);
    SelectTab("Basic Data");
}

function PropertyLongDescValidationMarkEvent() {
    var txtName = $find('<%= txtLongDescription.ClientID %>');
    MarkInvalid(txtName);
    SelectTab("Basic Data");
}

function PropertyShortDescValidationMarkEvent() {
    var txtName = $find('<%= txtShortDescription.ClientID %>');
    MarkInvalid(txtName);
    SelectTab("Basic Data");
}

function PropertyCodeValidationMarkEvent() {
    var txtPropertyCode = $find('<%= txtCode.ClientID %>');
    MarkInvalid(txtPropertyCode);
    SelectTab("Basic Data");
}

function PropertyTypeValidationMarkEvent() {
    var rcbPropertyType = $find('<%= rcbPropertyType.ClientID %>');
    SelectTab("Basic Data");
    MarkRadComboBoxInvalid(rcbPropertyType);
}

function PropertyAltitudeValidationMarkEvent() {
    var txtAltitude = $find('<%= txtAltitude.ClientID %>');
    MarkInvalid(txtAltitude);
    SelectTab("Basic Data");
}

function AmenitiesValidationMarkEvent() {
    SelectTab("Amenities");
}

function TagsValidationMarkEvent() {
    SelectTab("Tags");
}

function UnitCountValidationMarkEvent() {
    SelectTab("Units");
}

function UnitFieldValidationMarkEvent(elemId) {
    MarkRadGridRow("unit-id", elemId);
    SelectTab("Units");
}

function UnitTypesCountValidationMarkEvent() {
    SelectTab("Unit Types");
}

function UnitTypesDescValidationMarkEvent(elemId) {
    MarkRadGridRow("unit-type-id", elemId);
    SelectTab("Unit Types");
}


//Floor plans
function FloorPlansValidationMarkEvent() {
    SelectTab("Floor Plans");
}

function FloorPlansHotSpotsValidationMarkEvent(elemId) {
    MarkRadGridRow("floor-plan-id", elemId);
    SelectTab("Floor Plans");
}

function ImagesValidationMarkEvent() {
    SelectTab("Images");
}

//features
function FeaturesValidationMarkEvent() {
    SelectTab("Features");
}

function FeaturesWordsValidationMarkEvent(elemId) {
    MarkRadGridRow("feature-id", elemId);
    SelectTab("Features");
}
//distance to attractions
function DistanceToAttractionsValidationMarkEvent() {
    SelectTab("Distance to Attractions");
}
//
function LocalAreaDescrValidationMarkEvent() {
    var obj = $('[localArea="0"]');
    MarkInvalid(obj)
    SelectTab("Local Area Description");
}

function InputValueChanged(sender, args) {
    sender.get_styles().EnabledStyle[0] = "NaN";
    sender.updateCssClass();
}

function MarkInvalid(obj) {
    InputValueChanged(obj, null);
    obj.get_styles().EnabledStyle[0] = "border: 1px solid #f00;";
    obj.updateCssClass();
}

function MarkRadComboBoxInvalid(obj) {
    obj._element.style.border = "solid 1px #f00";
    obj.updateCssClass();
}

function SelectTab(tabName) {
    var radTab = $find('<%= rtsPropertyDetails.ClientID %>').findTabByText(tabName);
    radTab.select();
}

function MarkRadGridRow(entityName, dataId) {
    var row = $('[' + entityName + '="' + dataId + '"]');
    $(row).contents('td').css({ 'border': '1px solid red', 'border-left': 'none', 'border-right': 'none' });
}

function ShowResponsibleSection(sender, args) {    
    var responsibleRow = $('#<%= trResponsible.ClientID %>');
    var keyHandlerType = $('#<%= hfKeyHandlerProcessType.ClientID %>').val();
    if (keyHandlerType == sender._selectedItem._text) {
        responsibleRow.show();
    }
    else {
        responsibleRow.hide();
    }
}

function ValidateResponsbile(sender, args) {
    var keyHandlerType = $('#<%= hfKeyHandlerProcessType.ClientID %>').val();
    var selectedKeyType = $('#<%= rcbKeyProcessType.ClientID %>').val();
    if (keyHandlerType == selectedKeyType) {
        var keyHandler = $('#<%= rcbKeyHandlerManager.ClientID %>').val();
        if (keyHandler == "") {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }
    else {
        args.IsValid = true;
    }
}

function SelectTabSquareFootage() {
    SelectTab("Square footage");
}
</script>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="txtZipCode">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rcbUploadManager" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
        <telerik:RadWindow ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" Text="<%$ Resources: Controls, Dialog_Discard_Text %>" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="btnDiscard_Yes"></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClick="btnDiscard_No"></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
        <telerik:RadWindow ID="rwError" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblError" Font-Bold="true" runat="server" />
                        <br />
                        <br />
                        <asp:Button ID="wndBtnError_Ok" runat="server" Text="<%$ Resources: Controls, Dialog_Ok %>" OnClick="btnError_Ok" />
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<asp:Panel runat="server" ID="pnlDataTextBoxes">
    <asp:ValidationSummary ID="vsSummary" ValidationGroup="Property" HeaderText=" "
        DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
    <h1 class="FormHeader">
        <asp:Label runat="server" ID="lblHeader" meta:resourcekey="lblHeaderAdd"></asp:Label>
    </h1>
    <div style="float: right;">
        <uc:Completness ID="propertyCompletness" runat="server" />
    </div>
    <div style="clear: both;"></div>

        <asp:Panel runat="server" ID="pnlData">
        <table>
            <tr>
                <td colspan="5">
                <telerik:RadTabStrip ID="rtsPropertyDetails" runat="server"
                    SelectedIndex="0" MultiPageID="rmpTabContent">
                    <Tabs>
                        <telerik:RadTab Text="Basic Data" />
                        <telerik:RadTab Text="Unit Types"/>
                        <telerik:RadTab Text="Units" />
                        <telerik:RadTab Text="Amenities" />
                        <telerik:RadTab Text="Tags" />
                        <telerik:RadTab Text="Add-Ons" />
                        <telerik:RadTab Text="Images" />
                        <telerik:RadTab Text="Features" />
                        <telerik:RadTab Text="Square footage" />
                        <telerik:RadTab Text="Distance to Attractions" />
                        <telerik:RadTab Text="Local Area Description" />
                        <telerik:RadTab Text="Floor Plans" />
                        <telerik:RadTab Text="Raw Data" />
                    </Tabs>
                </telerik:RadTabStrip>
                
                 <telerik:RadMultiPage ID="rmpTabContent" runat="server" SelectedIndex="0" >
                     <telerik:RadPageView ID="rpvBasicData" runat="server" CssClass="radTab">                        
                        <table>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblName" meta:resourcekey="lblName" />
                                </td>
                                <td style="width: 270px;">
                                    <telerik:RadTextBox runat="server" TabIndex="1" ID="txtName" Enabled="true" Width="250px">
                                            <ClientEvents OnValueChanged="InputValueChanged" />
                                    </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator 
                                        ID="rfvName" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtName"                     
                                        meta:resourcekey="ValName" 
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblPropertyEnabled" meta:resourcekey="lblPropertyEnabled" />
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" ID="cbPropertyEnabled" TabIndex="2" Enabled="false" meta:resourcekey="cbPropertyEnabled"/>
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label4" meta:resourcekey="lblCheckInTime" />
                                </td>
                                <td>
                                    <telerik:RadTimePicker ID="rtpCheckInTime" runat="server" ZIndex="30001"></telerik:RadTimePicker>
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator4" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="rtpCheckInTime"                     
                                        meta:resourcekey="ValCheckIn" 
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label5" meta:resourcekey="lblCheckOutTime" />
                                </td>
                                <td>
                                    <telerik:RadTimePicker ID="rtpCheckOutTime" runat="server" ZIndex="30001"></telerik:RadTimePicker>
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator12" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="rtpCheckOutTime"                     
                                        meta:resourcekey="ValCheckOut" 
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblStarRating" meta:resourcekey="lblStarRating" />
                                </td>
                                <td>
                                   <telerik:RadComboBox runat="server" ID="rcbStarRating" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" >
                                       <Items>
                                           <telerik:RadComboBoxItem Text="" Value="0" />
                                           <telerik:RadComboBoxItem Text="1" Value="1" />
                                           <telerik:RadComboBoxItem Text="2" Value="2" />
                                           <telerik:RadComboBoxItem Text="3" Value="3" />
                                           <telerik:RadComboBoxItem Text="4" Value="4" />
                                           <telerik:RadComboBoxItem Text="5" Value="5" />
                                       </Items></telerik:RadComboBox>
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator15"
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="rcbStarRating"                     
                                        meta:resourcekey="ValStarRating" 
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblKeyProcessType" meta:resourcekey="lblKeyProcessType" />
                                    <asp:HiddenField runat="server" ID="hfKeyHandlerProcessType" />
                                </td>
                                <td>
                                   <telerik:RadComboBox runat="server" ID="rcbKeyProcessType" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" OnClientSelectedIndexChanged="ShowResponsibleSection" />
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator13" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="rcbKeyProcessType"                     
                                        meta:resourcekey="ValProcessType" 
                                        Text="*" 
                                        CssClass="error" />
                                    <asp:CustomValidator
                                        OnServerValidate="cvLockboxData_ServerValidate"
                                        ID="cvLockboxData" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic"                    
                                        meta:resourcekey="ValLockboxStrategy_Units" 
                                        ControlToValidate="rcbKeyProcessType"
                                        ValidateEmptyText="false"
                                        EnableClientScript="true"
                                        Enabled="true"
                                        Text="*"
                                        CssClass="error" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr id="trResponsible" runat="server">
                                <td>
                                    <asp:Label ID="lblKeyHandler" meta:resourcekey="lblKeyHandler" runat="server"/>
                                </td>
                                <td>
                                   <telerik:RadComboBox runat="server" ID="rcbKeyHandlerManager" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" />
                                    <asp:CustomValidator
                                        ClientValidationFunction="ValidateResponsbile"
                                        ID="rfvKeyHandler" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic"                    
                                        meta:resourcekey="ValKeyHandler" 
                                        ControlToValidate="rcbKeyHandlerManager"
                                        ValidateEmptyText="true"
                                        EnableClientScript="true"
                                        Enabled="true"
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblCode" meta:resourcekey="lblCode" />
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" TabIndex="1" ID="txtCode" Enabled="true" Width="250px" MaxLength="250"/>
                                    <asp:CustomValidator
                                        ID="cvPropertyCode" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic"                    
                                        meta:resourcekey="ValPropertyCode" 
                                        ControlToValidate="txtCode"
                                        ValidateEmptyText="false"
                                        EnableClientScript="true"
                                        Enabled="true"
                                        Text="*" 
                                        OnServerValidate="cvPropertyCode_ServerValidate"
                                        CssClass="error" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblOwner" meta:resourcekey="lblOwner" />
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="rcbOwner" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblDestination" meta:resourcekey="lblDestination" />
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="rcbDestination" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblPropertyType" meta:resourcekey="lblPropertyType" />
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="rcbPropertyType" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblShortDescription" meta:resourcekey="lblShortDescription" />
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtShortDescription" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" TextMode="SingleLine" >
                                            <ClientEvents OnValueChanged="InputValueChanged" />
                                    </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator 
                                        ID="rfvShortDesc" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtShortDescription"                     
                                        meta:resourcekey="ValShortDesc" 
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                                <td colspan="3">
                                    <div style="width: 300px">Example: <asp:Label ID="lblInfo1" runat="server" meta:resourceKey="txtShortDescription" /></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblLongDescription" meta:resourcekey="lblLongDescription" />
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtLongDescription" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" TextMode="MultiLine" Rows="6" >
                                            <ClientEvents OnValueChanged="InputValueChanged" />
                                    </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator1" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtLongDescription"                     
                                        meta:resourcekey="ValLongDesc" 
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                                <td colspan="3">
                                    <div style="width: 300px">Example: <asp:Label ID="Label3" runat="server" meta:resourceKey="txtLongDescription" /></div>
                                </td>    
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <asp:Label runat="server" ID="Label1" meta:resourcekey="lblAddressAndLocation" Font-Bold="true" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                            <tr>
                                <td style="width: 150px">
                                    <asp:Label runat="server" ID="lblAddressLine1" meta:resourcekey="lblAddressLine1" />
                                </td>
                                <td style="width: 260px">
                                    <telerik:RadTextBox runat="server" ID="txtAddressLine1" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" TextMode="SingleLine" MaxLength="250" />
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator2" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtAddressLine1"                     
                                        meta:resourcekey="ValAddressLine1" 
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                                <td style="width: 50px">
                                    &nbsp;
                                </td>
                                <td style="width: 150px">
                                    <asp:Label runat="server" ID="lblState" meta:resourcekey="lblState" />
                                </td>
                                <td style="width: 260px">
                                    <telerik:RadTextBox runat="server" ID="txtState" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" TextMode="SingleLine" MaxLength="250"/>
                                     <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator3" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtState"                     
                                        meta:resourcekey="ValState" 
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblAddressLine2" meta:resourcekey="lblAddressLine2" />
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtAddressLine2" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" TextMode="SingleLine" MaxLength="250" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCountry" meta:resourcekey="lblCountry" />
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="rcbCountry" TabIndex="9" Width="250px" MaxHeight="200px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblCity" meta:resourcekey="lblCity" />
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtCity" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" TextMode="SingleLine" MaxLength="250" />
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator9" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtCity"                     
                                        meta:resourcekey="ValCity" 
                                        Text="*" 
                                        CssClass="error" />
                                </td> 
                                <td colspan="3">
                                    &nbsp;
                                </td>           
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblZipCode" meta:resourcekey="lblZipCode" />
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtZipCode" TabIndex="9"
                                        AutoPostBack="true" OnTextChanged="txtZipCode_TextChanged" Enabled="true" Width="250px" TextMode="SingleLine" MaxLength="250" />
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator5" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtZipCode"                     
                                        meta:resourcekey="ValZipCode" 
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr >
                                <td>
                                    <asp:Label ID="lblUploadManager" meta:resourcekey="lblUploadManager" runat="server"/>
                                </td>
                                <td>
                                   <telerik:RadComboBox runat="server" ID="rcbUploadManager" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" />
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator14" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="rcbUploadManager"                     
                                        meta:resourcekey="ValUploadManager" 
                                        Text="*" 
                                        CssClass="error" />
                                </td>
                            </tr>
                            <tr >
                                <td>
                                    <asp:Label ID="lblPropertyManager" meta:resourcekey="lblPropertyManager" runat="server"/>
                                </td>
                                <td>
                                   <telerik:RadComboBox runat="server" ID="rcbPropertyManager" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblLatitude" meta:resourcekey="lblLatitude" />
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtLatitude" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" TextMode="SingleLine" onchange="javascript:initMap()">
                                        </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator6" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtLatitude"                     
                                        meta:resourcekey="ValLatitude" 
                                        Text="*" 
                                        CssClass="error" />
                                    <asp:RegularExpressionValidator                  
                                        ID="RequiredFieldValidator456" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtLatitude"                     
                                        meta:resourcekey="ValFormatLatitude" 
                                        Text="*" 
                                        CssClass="error" OnPreRender="RequiredFieldValidator_PreRender" />
                                </td> 
                                <td colspan="3">
                                    Example: <asp:Label ID="lblLatitudeExample" runat="server" />
                                </td>           
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblLongitude" meta:resourcekey="lblLongitude" />
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtLongitude" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" TextMode="SingleLine" onchange="javascript:initMap()">
                                        </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator7" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtLongitude"                     
                                        meta:resourcekey="ValLongitude" 
                                        Text="*" 
                                        CssClass="error" />
                                    <asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator1" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtLongitude"                     
                                        meta:resourcekey="ValFormatLongitude" 
                                        Text="*" 
                                        CssClass="error" OnPreRender="RequiredFieldValidator_PreRender"/>
                                </td>
                                <td colspan="3">
                                    Example: <asp:Label ID="lblLongitudeExample" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblAltitude" meta:resourcekey="lblAltitude" />
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtAltitude" TabIndex="9"
                                        AutoPostBack="false" Enabled="true" Width="250px" TextMode="SingleLine">
                         
                                        </telerik:RadTextBox>
                                    <asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator2" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtAltitude"                     
                                        meta:resourcekey="ValFormatAltitude" 
                                        Text="*" 
                                        CssClass="error" OnPreRender="AltitudeRegexValidator_PreRender" />
                                </td>
                                <td colspan="3">
                                    Example: <asp:Label ID="lblAltitudeExample" runat="server" />&nbsp;(meters)
                                </td>
                            </tr>
                            <tr id="tdGoogleMap" style="display: none; height: 450px;">
                                <td colspan="5" style="height: 450px; width: 100%;">
                                    <div class="googlemap">
                                        <div id="googlemap">
                                        </div>
                                    </div>
                                    <div id="map_canvas" style="height: 450px;"></div>
                                </td>   
                            </tr>    
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <asp:Label runat="server" ID="lblOtherSettings" meta:resourcekey="lblOtherSettings" Font-Bold="true" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblCulture" meta:resourcekey="lblCulture" />
                                </td>
                                <td>
                                    <telerik:RadComboBox Width="300" runat="server" ID="rcbCulture" TabIndex="9" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblTimeZones" meta:resourcekey="lblTimeZones" />
                                </td>
                                <td colspan="3">
                                    <telerik:RadComboBox Width="300" runat="server" ID="rcbTimeZones" TabIndex="9" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblStandardCommision" meta:resourcekey="lblStandardCommision" />
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtStandardCommission" TabIndex="9" /> %
                                    <asp:RequiredFieldValidator 
                                        ID="RequiredFieldValidator10" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtStandardCommission"                     
                                        meta:resourcekey="ValStandardComminssion" 
                                        Text="*" 
                                        CssClass="error" />
                                    <asp:RegularExpressionValidator 
                                        ValidationExpression=""
                                        ID="RegularExpressionValidator3" 
                                        ValidationGroup="Property" 
                                        runat="server" 
                                        Display="Dynamic" 
                                        ControlToValidate="txtStandardCommission"                     
                                        meta:resourcekey="ValStandardCommission" 
                                        Text="*" 
                                        CssClass="error" OnPreRender="RequiredFieldValidatorCommission_PreRender"/>
                
                                </td>
                                <td colspan="3">
                                    Example: <asp:Label ID="lblStandardCommissionExample" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblSalesPersons" meta:resourcekey="lblSalesPersons" />
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="rcbSalesPersons" TabIndex="9" />
                                </td>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                            </table>
                    </telerik:RadPageView>
                    
                    <telerik:RadPageView ID="rpvContacts" runat="server" CssClass="radTab">
                        <uc:UnitTypes ID="ctrlUnitTypes" runat="server" />
                    </telerik:RadPageView>

                    <telerik:RadPageView ID="RadPageView1" runat="server" CssClass="radTab">
                        <uc:Units ID="ctrlUnits" runat="server" />
                    </telerik:RadPageView>

                    <telerik:RadPageView ID="RadPageView2" runat="server" CssClass="radTab">
                        <uc:Amenities ID="ctrlAmenities" runat="server" />
                    </telerik:RadPageView>

                    <telerik:RadPageView ID="RadPageView3" runat="server" CssClass="radTab">
                        <uc:Tags ID="ctrlTags" runat="server" />
                    </telerik:RadPageView>

                    <telerik:RadPageView ID="RadPageView4" runat="server" CssClass="radTab">
                        <uc:AddOns ID="ctrlAddOns" runat="server" />
                    </telerik:RadPageView>

                     <telerik:RadPageView ID="RadPageView6" runat="server" CssClass="radTab">
                        <uc:Images ID="ctrlImages" runat="server" />
                    </telerik:RadPageView>

                    <telerik:RadPageView ID="RadPageView7" runat="server" CssClass="radTab">
                        <uc:Features ID="ctrlFeatures" runat="server" />
                    </telerik:RadPageView>

                    <telerik:RadPageView ID="RadPageView8" runat="server" CssClass="radTab">
                        <uc:Square ID="ctrlSquare" runat="server" />
                    </telerik:RadPageView>                            

                    <telerik:RadPageView ID="RadPageView11" runat="server" CssClass="radTab">
                        <uc:AttrDistance ID="ctrlDistanceToAttractions" runat="server" />
                    </telerik:RadPageView>

                    <telerik:RadPageView ID="RadPageView12" runat="server" CssClass="radTab">
                        <uc:LocalAreaDescr ID="ctrlLocalAreaDescr" runat="server" />
                    </telerik:RadPageView>

                    <telerik:RadPageView ID="RadPageView13" runat="server" CssClass="radTab">
                        <uc:FloorPlan ID="ctrlFloorPlan" runat="server" />
                    </telerik:RadPageView>

                    <telerik:RadPageView ID="RadPageView5" runat="server" CssClass="radTab">
                        <uc:RawData ID="ctrlRawData" runat="server" />
                    </telerik:RadPageView>

                </telerik:RadMultiPage>
                   
            </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>

<table>
    <tr style="height: 20px;">
        <td></td>
    </tr>
    <tr>
        <td width="645px" align="right" style="padding-right: 3px">
            <telerik:RadButton runat="server" ID="btnSave" TabIndex="10" Text="<%$ Resources: Controls, Button_Save %>" CausesValidation="true" ValidationGroup="Property" OnClick="btnSave_Click" />
            <telerik:RadButton runat="server" ID="btnCancel" TabIndex="11" Text="<%$ Resources: Controls, Button_Cancel %>" CausesValidation="false" OnClick="btnCancel_Click" />
        </td>
    </tr>
</table>

<table>
    <tr></tr>
    <tr>
        <td>
            <asp:Label ID="lblMoreDetails" runat="server" meta:resourceKey="lblMoreDetails" />
        </td>
    </tr>
</table>

<script type="text/javascript">
    initMap();
</script>

