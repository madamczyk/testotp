﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentUpdateRequestDetailsList.ascx.cs" Inherits="IntranetApp.Controls.Properties.ContentUpdateRequestDetailsList" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgDetails">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgDetails" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadGrid 
    AutoGenerateColumns="False" 
    ID="rgDetails" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="detailsLinqDS"
    OnItemCommand="rgDetails_ItemCommand" 
    OnDataBound="rgDetails_DataBound"
    OnItemDataBound="rgDetails_ItemDataBound" Width="100%"
    ClientSettings-ClientEvents-OnColumnClick="disableWarning">
    <ClientSettings EnableRowHoverStyle="false" Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="false" />
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
	<MasterTableView TableLayout="Fixed" DataKeyNames="ContentUpdateRequestDetailId" DataSourceID="detailsLinqDS" CommandItemDisplay="None" >
		<Columns>            
                <telerik:GridTemplateColumn DataField="ContentCode" HeaderText="Content Type" UniqueName="ContentCode" SortExpression="ContentCode" ShowFilterIcon="false" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="true" HeaderStyle-Width="100px" ItemStyle-CssClass="link">
                    <ItemTemplate>
                        <%# DataAccess.Enums.EnumConverter.GetValue((DataAccess.Enums.ContentUpdateRequestDetailContentType)Eval("ContentCode")) %>
                    </ItemTemplate>
                    <FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbContentType" AutoPostBack="true" EnableViewState="true" OnInit="rcbContentType_Init" OnSelectedIndexChanged="rcbContentType_SelectedIndexChanged" OnPreRender="rcbContentType_PreRender" MaxHeight="300px" Width="90%" DropDownWidth="150px" />
					</FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn UniqueName="ImageThumbnail" HeaderStyle-Width="100px">  
                    <ItemTemplate> 
                        <asp:Image runat="server" id="imageThumbnail" Visible="false" />
                    </ItemTemplate> 
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn UniqueName="ContentLink" HeaderText="Download" HeaderStyle-Width="60px">  
                    <ItemTemplate> 
                        <a runat="server" id="downloadLink" />
                    </ItemTemplate> 
                </telerik:GridTemplateColumn>
		</Columns>
	</MasterTableView>
</telerik:RadGrid>

<otpDS:SimpleDataSource ID="detailsLinqDS" runat="server" />