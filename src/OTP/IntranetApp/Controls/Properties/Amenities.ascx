﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Amenities.ascx.cs" Inherits="IntranetApp.Controls.Properties.Amenities" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgAmenities">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgAmenities" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<telerik:RadGrid
    AutoGenerateColumns="False" 
    ID="rgAmenities" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="amenitiesLinqDS"
	ClientSettings-EnablePostBackOnRowClick="false" 
    OnItemCommand="rgAmenities_ItemCommand" 
    OnDataBound="rgAmenities_DataBound"
    OnItemDataBound="rgAmenities_ItemDataBound"
    OnPageIndexChanged="rgAmenities_PageIndexChanged" 
    ClientSettings-ClientEvents-OnColumnClick="disableWarning">
	<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView Width="100%" TableLayout="Fixed" DataKeyNames="AmenityID" DataSourceID="amenitiesLinqDS">
		<Columns>         
            <telerik:GridBoundColumn DataField="TitleCurrentLanguage" FilterControlWidth="200px" UniqueName="Name" HeaderText="Name" SortExpression="TitleCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="200px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>    
            <telerik:GridBoundColumn DataField="AmenityGroup.NameCurrentLanguage" FilterControlWidth="200px" UniqueName="Group" HeaderText="Group" SortExpression="AmenityGroup.NameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="200px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText=" " UniqueName="IsConnected">
				<HeaderStyle Width="50px"></HeaderStyle>  
                <ItemTemplate> 
                    <asp:CheckBox ID="cbAmenity" Checked="false" runat="server" OnCheckedChanged="cbAmenities_CheckedChanged" AutoPostBack="true" /> 
                </ItemTemplate> 
            </telerik:GridTemplateColumn>             	
            <telerik:GridTemplateColumn HeaderText="Quantity" UniqueName="Quantity">  
				<HeaderStyle Width="400px"></HeaderStyle>  
                <ItemTemplate> 
                    <telerik:RadNumericTextBox Width="50px" runat="server" ID="txtQuantity" MaxLength="5" Enabled="false" MinValue="1" NumberFormat-DecimalDigits="0" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorQuantity" ValidationGroup="Property" Enabled="false" runat="server" Display="Dynamic" ControlToValidate="txtQuantity" Text="*" CssClass="error" />
                </ItemTemplate> 
            </telerik:GridTemplateColumn>
		</Columns>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="AmenityGroup.NameCurrentLanguage" SortOrder="Ascending" />
        </SortExpressions>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<otpDS:SimpleDataSource ID="amenitiesLinqDS" runat="server" />
