﻿using BusinessLogic.Enums;
using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Properties
{
    public partial class ContentUpdateRequestDetailsList : System.Web.UI.UserControl
    {
        #region Fields

        public int? ContentUpdateRequestId
        {
            get { return ViewState["ContentUpdateRequestId"] as int?; }
            set { ViewState["ContentUpdateRequestId"] = value; }
        }

        public int? ContentTypeFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["ContentTypeFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["ContentTypeFilterSelectedValue"] as int?;
            }
            set { ViewState["ContentTypeFilterSelectedValue"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.detailsLinqDS.DataResolver = () =>
            {
                IQueryable<ContentUpdateRequestDetail> result = RequestManager.Services.PropertiesService.GetContentUpdateRequestDetailsByRequestId(ContentUpdateRequestId.Value).AsQueryable();

                if (ContentTypeFilterSelectedValue.HasValue)
                    result = result.Where(d=> d.ContentCode == ContentTypeFilterSelectedValue);

                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgDetails_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgDetails_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                ContentUpdateRequestDetail obj = (ContentUpdateRequestDetail)item.DataItem;
                HtmlAnchor downloadLink = (HtmlAnchor)item["ContentLink"].FindControl("downloadLink");
                Image image = (Image)item["ImageThumbnail"].FindControl("imageThumbnail");

                if (!String.IsNullOrEmpty(obj.ContentValue))
                {
                    downloadLink.HRef = string.Format("{0}/{1}", RequestManager.Services.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray()), obj.ContentValue.TrimStart("/".ToCharArray()));
                    downloadLink.InnerText = (string)GetGlobalResourceObject("Controls", "Link_Download");

                    if (obj.ContentCode == (int)ContentUpdateRequestDetailContentType.Image)
                    {
                        image.ImageUrl = downloadLink.HRef;
                        image.Width = 100;
                        image.Visible = true;
                    }
                }
            }
        }

        protected void rgDetails_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgDetails.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgDetails.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgDetails.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgDetails.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgDetails.MasterTableView.Rebind();
            }
        }

        protected void rcbContentType_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (ContentUpdateRequestDetailContentType ct in Enum.GetValues(typeof(ContentUpdateRequestDetailContentType)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(ct), ((int)ct).ToString()));
            }
        }

        protected void rcbContentType_PreRender(object sender, EventArgs e)
        {
            if (ContentTypeFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = ContentTypeFilterSelectedValue.ToString();
            }
        }

        protected void rcbContentType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ContentTypeFilterSelectedValue = int.Parse(e.Value);
            rgDetails.MasterTableView.Rebind();
        }

        #endregion Event Handlers
    }
}