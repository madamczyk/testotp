﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddOns.ascx.cs" Inherits="IntranetApp.Controls.Properties.AddOns" %>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgAddOns">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgAddOns" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadGrid 
    AutoGenerateColumns="False" 
    ID="rgAddOns" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="addOnsLinqDS"
	ClientSettings-EnablePostBackOnRowClick="false" 
    OnItemCommand="rgAddOns_ItemCommand" 
    OnDataBound="rgAddOns_DataBound"
    OnItemDataBound="rgAddOns_ItemDataBound"
    OnUpdateCommand="rgAddOns_UpdateCommand"
    OnInsertCommand="rgAddOns_InsertCommand" Width="100%"
    ClientSettings-ClientEvents-OnColumnClick="disableWarning">
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="True" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Fixed" DataKeyNames="PropertyAddOnID" DataSourceID="addOnsLinqDS" CommandItemDisplay="Bottom" >
		<Columns>
            <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" HeaderText=" " HeaderStyle-Width="50px" >
                <ItemStyle Width="50px"></ItemStyle>
                </telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn DataField="AddOnTitleCurrentLanguage" FilterControlWidth="150px" UniqueName="AddOnTitleCurrentLanguage" HeaderText="Title" SortExpression="AddOnTitleCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AddOnDescCurrentLanguage" FilterControlWidth="150px" UniqueName="AddOnDescCurrentLanguage" HeaderText="Description" SortExpression="AddOnDescCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Unit" FilterControlWidth="150px" UniqueName="Unit" HeaderText="Unit" SortExpression="Unit" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Price" FilterControlWidth="150px" UniqueName="Price" HeaderText="Price" SortExpression="Price" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="Is mandatory"> 
                <ItemTemplate> 
                <asp:CheckBox Enabled="false" ID="cbIsMandatory" runat="server" /> 
                </ItemTemplate> 
            </telerik:GridTemplateColumn> 
            <telerik:GridBoundColumn DataField="Tax.NameCurrentLanguage" FilterControlWidth="150px" UniqueName="Tax.NameCurrentLanguage" HeaderText="Tax" SortExpression="Tax.NameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>           
            <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridButtonColumn>			
		</Columns>
        <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="AddOns"
                        HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                        EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">

                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblTitle" meta:resourceKey="lblTitle"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtAddOnTitle" MaxLength="50" TextMode="SingleLine"  Rows="11"
                                    Text='<%# Eval("AddOnTitleCurrentLanguage") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="AddOns" ID="RequiredFieldValidator7"
                                    Text="*" ControlToValidate="txtAddOnTitle" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValTitle"></asp:RequiredFieldValidator>
                            </td>
                        </tr> 
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblUnit" meta:resourceKey="lblUnit"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadComboBox Width="220px" runat="server" ID="rcbUnit" MaxLength="50" OnPreRender="ddlUnit_PreRender" />
                                <asp:RequiredFieldValidator ValidationGroup="AddOns" ID="RequiredFieldValidator1"
                                    Text="*" ControlToValidate="rcbUnit" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValUnit"></asp:RequiredFieldValidator>
                                <%--<asp:RegularExpressionValidator 
                                    ValidationExpression="^\s*[0-9]{1,10}\s*$"
                                    ID="RegularExpressionValidator3" 
                                    ValidationGroup="AddOns"
                                    runat="server" 
                                    Display="Dynamic" 
                                    ControlToValidate="txtUnit"                     
                                    meta:resourcekey="ValUnitFormat" 
                                    Text="*" 
                                    CssClass="error" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDescription" meta:resourceKey="lblDescription"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtAddOnDesc" MaxLength="50" TextMode="MultiLine"  Rows="5"
                                    Text='<%# Eval("AddOnDescCurrentLanguage") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="AddOns" ID="RequiredFieldValidator2"
                                    Text="*" ControlToValidate="txtAddOnDesc" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDescription"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblMandatory" meta:resourceKey="lblMandatory"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <asp:CheckBox runat="server" ID="cbIsMandatory" MaxLength="50" Checked='<%# Eval("IsMandatory") is DBNull ? false : Eval("IsMandatory") %>'  />
                            </td>
                        </tr>
                        

                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblrice" meta:resourceKey="lblPrice"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtPrice" MaxLength="50" Text='<%# Eval("Price") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="AddOns" ID="RequiredFieldValidator3"
                                    Text="*" ControlToValidate="txtPrice" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValPrice"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator
                                    ID="RequiredFieldValidator456" 
                                    ValidationGroup="AddOns" 
                                    runat="server" 
                                    Display="Dynamic" 
                                    ControlToValidate="txtPrice"                     
                                    meta:resourcekey="ValPriceFormat" 
                                    Text="*" 
                                    CssClass="error" OnPreRender="RequiredFieldValidator_PreRender"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblTax" meta:resourceKey="lblTax"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadComboBox Width="220px" runat="server" ID="rcbTax" MaxLength="50" OnPreRender="ddlTax_PreRender" />
                                <asp:RequiredFieldValidator ValidationGroup="AddOns" ID="RequiredFieldValidator4"
                                    Text="*" ControlToValidate="rcbTax" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValTax"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        
                                                   
                        <tr>
                            <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                <div style="position: relative; width: 415px;">
                                    <!-- workaround for issue with RadButtons not being aligned vertically -->
                                    <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="AddOns"
                                        VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        CausesValidation="true">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                    </telerik:RadButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<otpDS:SimpleDataSource ID="addOnsLinqDS" runat="server" />