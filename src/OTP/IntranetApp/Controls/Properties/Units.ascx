﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Units.ascx.cs" Inherits="IntranetApp.Controls.Properties.Units" %>

<script type="text/javascript">
    function RowUnitClick(sender, args) {
        var rowId = args.getDataKeyValue("UnitID");
        openUnitDialog(rowId, 1);
        args.stopPropagation();
        return false;
    }
</script>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgUnit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgUnit" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<telerik:RadGrid 
    AutoGenerateColumns="False" 
    ID="rgUnit" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="unitsLinqDS"
	ClientSettings-EnablePostBackOnRowClick="true" 
    OnItemCommand="rgUnit_ItemCommand" 
    OnDataBound="rgUnit_DataBound"
    OnItemDataBound="rgUnit_ItemDataBound"
    OnUpdateCommand="rgUnit_UpdateCommand"
    OnInsertCommand="rgUnit_InsertCommand" Width="100%"
    ClientSettings-ClientEvents-OnColumnClick="disableWarning">
   <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelecting="RowUnitClick" />
        <Scrolling SaveScrollPosition="true" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Auto"  DataKeyNames="UnitID" ClientDataKeyNames="UnitID" DataSourceID="unitsLinqDS" CommandItemDisplay="Bottom" >
		<Columns>
            <telerik:GridBoundColumn DataField="UnitTitleCurrentLanguage" FilterControlWidth="150px" UniqueName="UnitTitleCurrentLanguage" HeaderText="Title" SortExpression="UnitTitleCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="UnitType.UnitTypeTitleCurrentLanguage" FilterControlWidth="150px" UniqueName="UnitType.UnitTypeTitleCurrentLanguage" HeaderText="Unit Type" SortExpression="UnitType.UnitTypeTitleCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="UnitDescCurrentLanguage" FilterControlWidth="150px" UniqueName="UnitDescCurrentLanguage" HeaderText="Description" SortExpression="UnitDescCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="UnitCode" FilterControlWidth="150px" UniqueName="UnitCode" HeaderText="Code" SortExpression="UnitCode" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="Cleaning Status"> 
                <ItemTemplate> 
                <asp:Label ID="lblCleaningStatus" runat="server" /> 
                </ItemTemplate> 
            </telerik:GridTemplateColumn> 
            <telerik:GridBoundColumn DataField="MaxNumberOfGuests" FilterControlWidth="150px" UniqueName="MaxNumberOfGuests" HeaderText="No. of guests" SortExpression="MaxNumberOfGuests" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MaxNumberOfBedrooms" FilterControlWidth="150px" UniqueName="MaxNumberOfBedrooms" HeaderText="No. of bedrooms" SortExpression="MaxNumberOfBedrooms" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MaxNumberOfBathrooms" FilterControlWidth="150px" UniqueName="MaxNumberOfBathrooms" HeaderText="No. of bathrooms" SortExpression="MaxNumberOfBathrooms" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridButtonColumn>			
		</Columns>
            <%--<EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="Unit"
                        HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                        EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">

                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblTitle" meta:resourceKey="lblTitle"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtTitle" MaxLength="50"
                                    Text='<%# Eval("UnitTitleCurrentLanguage") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="Unit" ID="RequiredFieldValidator7"
                                    Text="*" ControlToValidate="txtTitle" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValTitle"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblUnitType" meta:resourceKey="lblUnitType"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadComboBox Width="220px" runat="server" ID="rcbUnitType" MaxLength="50" OnPreRender="ddlUnitType_PreRender" />
                                <asp:RequiredFieldValidator ValidationGroup="Unit" ID="rfvNameProductParam"
                                    Text="*" ControlToValidate="rcbUnitType" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValUnitType"></asp:RequiredFieldValidator>
                            </td>
                        </tr> 
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblCode" meta:resourceKey="lblCode"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtUnitCode" MaxLength="50"
                                    Text='<%# Eval("UnitCode") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="Unit" ID="RequiredFieldValidator1"
                                    Text="*" ControlToValidate="txtUnitCode" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValCode"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDescription" meta:resourceKey="lblDescription"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtUnitDesc" MaxLength="50" TextMode="MultiLine"  Rows="5"
                                    Text='<%# Eval("UnitDescCurrentLanguage") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="Unit" ID="RequiredFieldValidator2"
                                    Text="*" ControlToValidate="txtUnitDesc" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDescription"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblCleaningStatus" meta:resourceKey="lblCleaningStatus" ></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadComboBox Width="220px" runat="server" ID="rcbCleaningStatus" MaxLength="50" OnPreRender="ddlCleaningStatus_PreRender" />
                                <asp:RequiredFieldValidator ValidationGroup="Unit" ID="RequiredFieldValidator3"
                                    Text="*" ControlToValidate="rcbCleaningStatus" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValCleaningStatus"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblNumberOfGuests" meta:resourceKey="lblNumberOfGuests"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtNumberOfGuests" MaxLength="50" TextMode="SingleLine"
                                    Text='<%# Eval("MaxNumberOfGuests") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="Unit" ID="RequiredFieldValidator4"
                                    Text="*" ControlToValidate="txtNumberOfGuests" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValNumberOfGuests"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator 
                                    ValidationExpression="^\d{1,4}\+?"
                                    ID="RequiredFieldValidator456" 
                                    ValidationGroup="Unit" 
                                    runat="server" 
                                    Display="Dynamic" 
                                    ControlToValidate="txtNumberOfGuests"                     
                                    meta:resourcekey="ValFormatNumberOfGuests" 
                                    Text="*" 
                                    CssClass="error" />
                            </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblBaths"  meta:resourceKey="lblBaths"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtNumberOfBathrooms" MaxLength="50" TextMode="SingleLine"
                                    Text='<%# Eval("MaxNumberOfBathrooms") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="Unit" ID="RequiredFieldValidator5"
                                    Text="*" ControlToValidate="txtNumberOfBathrooms" runat="server" Display="Static"
                                    CssClass="error" meta:resourcekey="ValNumberOfBathrooms"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator 
                                    ValidationExpression="^\d{1,4}\+?"
                                    ID="RegularExpressionValidator1" 
                                    ValidationGroup="Unit" 
                                    runat="server" 
                                    Display="Dynamic" 
                                    ControlToValidate="txtNumberOfBathrooms"                     
                                    meta:resourcekey="ValFormatNumberOfGuests" 
                                    Text="*" 
                                    CssClass="error" />
                            </td>
                        </tr>    
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblNumberOfBedrooms" meta:resourceKey="lblNumberOfBedrooms"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtNumberOfBedrooms" MaxLength="50" TextMode="SingleLine"
                                    Text='<%# Eval("MaxNumberOfBedrooms") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="Unit" ID="RequiredFieldValidator6"
                                    Text="*" ControlToValidate="txtNumberOfBedrooms" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValNumberOfBedrooms"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator 
                                    ValidationExpression="^\d{1,4}\+?"
                                    ID="RegularExpressionValidator2" 
                                    ValidationGroup="Unit" 
                                    runat="server" 
                                    Display="Dynamic" 
                                    ControlToValidate="txtNumberOfBedrooms"                     
                                    meta:resourcekey="ValFormatNumberOfGuests" 
                                    Text="*" 
                                    CssClass="error" />
                            </td>
                        </tr>                            
                        <tr>
                            <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                <div style="position: relative; width: 415px;">
                                    <!-- workaround for issue with RadButtons not being aligned vertically -->
                                    <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="Unit"
                                        VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        CausesValidation="true">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                    </telerik:RadButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>--%>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<otpDS:SimpleDataSource ID="unitsLinqDS" runat="server" />