﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using Telerik.Web.UI;
using System.IO;
using BusinessLogic.Enums;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Net;
using System.Drawing;
using System.Drawing.Drawing2D;


namespace IntranetApp.Controls.Properties
{
    public partial class PropertyRawDatas : System.Web.UI.UserControl
    {
        #region Fields

        public int? PropertyID
        {
            get { return ViewState["PropertyRawDataId"] as int?; }
            set { ViewState["PropertyRawDataId"] = value; }
        }

        public int? RawRecordTypeFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["RawRecordTypeFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["RawRecordTypeFilterSelectedValue"] as int?;
            }
            set { ViewState["RawRecordTypeFilterSelectedValue"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.rawDataLinqDS.DataResolver = () =>
            {
                if (!PropertyID.HasValue)
                    return new List<PropertyRawData>();

                IQueryable<PropertyRawData> result = RequestManager.Services.PropertiesService.GetPropertyRawDatas(PropertyID.Value).AsQueryable();
                        
                rgRawData.ApplyFilter<PropertyRawData>(ref result, "Description", (q, f) => q.Where(prd => prd.Description.ToLower().Contains(f.ToLower())));

                if (RawRecordTypeFilterSelectedValue.HasValue)
                    result = result.Where(prd => prd.RawRecordType == RawRecordTypeFilterSelectedValue.Value);
               
                return result.ToList();
            };
        }

        private void BindControls()
        {
            if (PropertyID.HasValue)
                rauImageUpload.HttpHandlerUrl = string.Format("~/Handlers/CustomHandler.ashx?pid={0}&rawData=1&", PropertyID.Value);
            rauImageUpload.MaxFileSize = int.Parse(RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.MaxFileSize));
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
            BindControls();
        }

        protected void rgRawData_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgRawData.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgRawData.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgRawData.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgRawData.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgRawData.MasterTableView.Rebind();
            }
        }

        protected void rgRawData_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int rawDataId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyRawDataRecordID"].ToString());

                if (RequestManager.Services.PropertiesService.GetPropertyRawDataById(rawDataId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyRawData"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_PropertyRawData"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                    RequestManager.Services.PropertiesService.DeletePropertyRawData(rawDataId);
            }
        }

        protected void rgRawData_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                PropertyRawData obj = (PropertyRawData)item.DataItem;
                HtmlAnchor imgLink = (HtmlAnchor)item["RawRecordContent"].FindControl("rawRecordContentLink");
                ImageButton deleteImage = (ImageButton)item["DeleteColumn"].Controls[0];
                
                imgLink.HRef = string.Format("{0}/{1}", RequestManager.Services.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray()), obj.RawRecordContent.TrimStart("/".ToCharArray()));
                imgLink.InnerText = (string)GetGlobalResourceObject("Controls", "Link_Download");
                imgLink.Attributes["onClick"] = "disableWarning();";

                deleteImage.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgRawData.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_PropertyRawData"), obj.RecordName), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyRawData"), obj.RecordName));
            }
        }

        protected void rcbRawRecordType_Init(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Add(new RadComboBoxItem("", "-1"));

                foreach (PropertyRawRecordType rt in Enum.GetValues(typeof(PropertyRawRecordType)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(rt), ((int)rt).ToString()));
            }
        }

        protected void rcbRawRecordType_PreRender(object sender, EventArgs e)
        {
            if (RawRecordTypeFilterSelectedValue.HasValue)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.SelectedValue = RawRecordTypeFilterSelectedValue.Value.ToString();
            }
        }

        protected void rcbRawRecordType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RawRecordTypeFilterSelectedValue = int.Parse(e.Value);
            rgRawData.MasterTableView.Rebind();
        }

        protected void rcbRecordType_PreRender(object sender, EventArgs e)
        {
            if (sender is RadComboBox)
            {
                RadComboBox combo = sender as RadComboBox;

                combo.Items.Clear();

                foreach (PropertyRawRecordType rt in Enum.GetValues(typeof(PropertyRawRecordType)))
                    combo.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(rt), ((int)rt).ToString()));
            }
        }

        protected void btnUploadData_Click(object sender, EventArgs e)
        {
            #region will be used with new telerik lib
            //if (!PropertyID.HasValue)
            //    return;

            //Property property = RequestManager.Services.PropertiesService.GetPropertyById(PropertyID.Value);

            //if (property == null)
            //    return;

            //PropertyRawData prd;
            //int parsedType;

            //foreach (UploadedFile file in rauImageUpload.UploadedFiles)
            //{
            //    prd = new PropertyRawData();

            //    parsedType = 0;

            //    prd.Property = property;
            //    prd.RawRecordType = int.TryParse(file.GetFieldValue("recordType"), out parsedType) ? parsedType : 0;
            //    prd.Description = file.GetFieldValue("description");
            //    prd.RawRecordContent = file.FileName;

            //    RequestManager.Services.PropertiesService.AddPropertyRawData(prd);
            //}

            //RequestManager.Services.SaveChanges();
            //rgRawData.MasterTableView.Rebind();
            #endregion

            if (!PropertyID.HasValue || !Page.IsValid)
                return;

            Property property = RequestManager.Services.PropertiesService.GetPropertyById(PropertyID.Value);

            if (property == null)
                return;

            if (rauImageUpload.UploadedFiles.Count == 0)
                return;

            PropertyRawData prd = new PropertyRawData()
            {
                Property = property,
                Description = txtDescription.Text,
                RecordName = txtRecordName.Text,
                RawRecordType = int.Parse(rcbRecordType.SelectedValue),
                RawRecordContent = rauImageUpload.UploadedFiles[0].FileName
            };

            RequestManager.Services.PropertiesService.AddPropertyRawData(prd);
            RequestManager.Services.SaveChanges();
            rgRawData.MasterTableView.Rebind();

            rcbRecordType.SelectedValue = "0";
            txtDescription.Text = "";
            txtRecordName.Text = "";
        }

        #endregion

    }
}