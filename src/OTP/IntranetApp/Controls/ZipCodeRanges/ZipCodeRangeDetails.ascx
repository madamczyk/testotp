﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZipCodeRangeDetails.ascx.cs" Inherits="IntranetApp.Controls.ZipCodeRanges.ZipCodeRangeDetails" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="pnlData">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlData" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
        <telerik:RadWindow ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" Text="<%$ Resources: Controls, Dialog_Discard_Text %>" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="btnDiscard_Yes"></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClick="btnDiscard_No"></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
        <telerik:RadWindow ID="rwError" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblError" Font-Bold="true" runat="server" />
                        <br />
                        <br />
                        <asp:Button ID="wndBtnError_Ok" runat="server" Text="<%$ Resources: Controls, Dialog_Ok %>" OnClick="btnError_Ok" />
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<asp:Panel runat="server" ID="pnlData">
    <asp:ValidationSummary ID="vsSummary" ValidationGroup="ZipCodeRange" DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
    <h1 class="FormHeader">
        <asp:Label runat="server" ID="lblHeader" meta:resourcekey="lblHeaderAdd"></asp:Label>
    </h1>
    <br />
    <table>
        <tr>
            <td width="100px">
                <asp:Label runat="server" ID="lblZipCodeStart" meta:resourcekey="lblZipCodeStart" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" TabIndex="1" ID="txtZipCodeStart" Enabled="true" Width="250px" MaxLength="5" />
                <asp:RequiredFieldValidator ID="rfvZipCodeStart" ValidationGroup="ZipCodeRange" runat="server" ControlToValidate="txtZipCodeStart" Display="Dynamic" meta:resourcekey="rfvZipCodeStart" Text="*" CssClass="error" />
                <asp:CustomValidator ID="cvZipCodeStart" ValidationGroup="ZipCodeRange" meta:resourcekey="cvZipCodeStart" runat="server" ControlToValidate="txtZipCodeStart" Text="*" CssClass="error" OnServerValidate="cvZipCodeStart_ServerValidate" ValidateEmptyText="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblZipCodeEnd" meta:resourcekey="lblZipCodeEnd" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" TabIndex="2" ID="txtZipCodeEnd" Enabled="true" Width="250px" MaxLength="5" />
                <asp:RequiredFieldValidator ID="rfvZipCodeEnd" ValidationGroup="ZipCodeRange" runat="server" ControlToValidate="txtZipCodeEnd" Display="Dynamic" meta:resourcekey="rfvZipCodeEnd" Text="*" CssClass="error" />
                <asp:CustomValidator ID="cvZipCodeEnd" ValidationGroup="ZipCodeRange" meta:resourcekey="cvZipCodeEnd" runat="server" ControlToValidate="txtZipCodeEnd" Text="*" CssClass="error" OnServerValidate="cvZipCodeEnd_ServerValidate" ValidateEmptyText="false" />
            </td>
        </tr>
    </table>
</asp:Panel>

<table>
    <tr style="height: 20px;">
        <td></td>
    </tr>
    <tr>
        <td width="645px" align="right" style="padding-right: 3px">
            <telerik:RadButton runat="server" ID="btnSave" TabIndex="3" Text="<%$ Resources: Controls, Button_Save %>" CausesValidation="true" ValidationGroup="ZipCodeRange" OnClick="btnSave_Click" />
            <telerik:RadButton runat="server" ID="btnCancel" TabIndex="4" Text="<%$ Resources: Controls, Button_Cancel %>" CausesValidation="false" OnClick="btnCancel_Click" />
        </td>
    </tr>
</table>
