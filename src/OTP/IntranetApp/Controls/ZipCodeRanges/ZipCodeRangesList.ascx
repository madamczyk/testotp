﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZipCodeRangesList.ascx.cs" Inherits="IntranetApp.Controls.ZipCodeRanges.ZipCodeRangesList" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgZipCodeRanges" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgZipCodeRanges" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="zipCodeRangesLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgZipCodeRanges_ItemCommand" 
        OnItemDataBound="rgZipCodeRanges_ItemDataBound"
        OnDataBound="rgZipCodeRanges_DataBound">
        <ClientSettings EnableRowHoverStyle="true" Selecting-AllowRowSelect="true" />
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="ZipCodeRangeId" DataSourceID="zipCodeRangesLinqDS">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="ZipCodeStart" SortOrder="Ascending" />
            </SortExpressions>
			<Columns>
				<telerik:GridBoundColumn DataField="ZipCodeStart" UniqueName="ZipCodeStart" HeaderText="Zip Code Start" SortExpression="ZipCodeStart" FilterControlWidth="100px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />

				<telerik:GridBoundColumn DataField="ZipCodeEnd" UniqueName="ZipCodeEnd" HeaderText="Zip Code End" SortExpression="ZipCodeEnd" FilterControlWidth="100px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />	

                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png" HeaderStyle-Width="40px" />
			</Columns>
		</MasterTableView>
	</telerik:RadGrid>
    <br />
    <telerik:RadButton runat="server" ID="btnAddZipCodeRange" Text="Add" OnClick="btnAddZipCodeRange_Click" />
</asp:Panel>

<otpDS:SimpleDataSource ID="zipCodeRangesLinqDS" runat="server" />