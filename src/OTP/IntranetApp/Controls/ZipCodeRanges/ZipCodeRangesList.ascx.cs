﻿using BusinessLogic.Managers;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using IntranetApp.Enums;

namespace IntranetApp.Controls.ZipCodeRanges
{
    public partial class ZipCodeRangesList : System.Web.UI.UserControl
    {
        #region Methods

        private void BindGrid()
        {
            zipCodeRangesLinqDS.DataResolver = () =>
            {
                IQueryable<ZipCodeRange> result = RequestManager.Services.UsersService.GetZipCodeRanges().AsQueryable();

                rgZipCodeRanges.ApplyFilter<ZipCodeRange>(ref result, "ZipCodeStart", (q, f) => q.Where(p => p.ZipCodeStart.ToLower().Contains(f.ToLower())));
                rgZipCodeRanges.ApplyFilter<ZipCodeRange>(ref result, "ZipCodeEnd", (q, f) => q.Where(p => p.ZipCodeEnd.ToLower().Contains(f.ToLower())));
                
                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgZipCodeRanges_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int rangeId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ZipCodeRangeId"].ToString());

                if (RequestManager.Services.UsersService.GetZipCodeRangeById(rangeId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/ZipCodeRanges/ZipCodeRangesDetails.aspx?id=");
                    sb.Append(rangeId);
                    sb.Append("&mode=");
                    sb.Append((int)FormMode.Edit);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int rangeId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ZipCodeRangeId"].ToString());

                if (RequestManager.Services.UsersService.GetZipCodeRangeById(rangeId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else if (RequestManager.Services.UsersService.ZipCodeRangeHasAnyDependencies(rangeId))
                {
                    ZipCodeRange range = RequestManager.Services.UsersService.GetZipCodeRangeById(rangeId);

                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"), range.ZipCodeRangeString);
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"), range.ZipCodeRangeString);

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                }
                else
                    RequestManager.Services.UsersService.DeleteZipCodeRangeById(rangeId);
            }
        }

        protected void rgZipCodeRanges_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                ZipCodeRange obj = (ZipCodeRange)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgZipCodeRanges.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"), obj.ZipCodeRangeString), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"), obj.ZipCodeRangeString));
            }
        }

        protected void rgZipCodeRanges_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgZipCodeRanges.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgZipCodeRanges.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgZipCodeRanges.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgZipCodeRanges.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgZipCodeRanges.MasterTableView.Rebind();
            }
        }

        protected void btnAddZipCodeRange_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("~/Forms/ZipCodeRanges/ZipCodeRangesDetails.aspx?");
            sb.Append("mode=");
            sb.Append((int)FormMode.Add);

            Response.Redirect(sb.ToString());
        }

        #endregion Event Handlers

    }
}