﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text.RegularExpressions;

namespace IntranetApp.Controls.ZipCodeRanges
{
    public partial class ZipCodeRangeDetails : BaseUserControl
    {      
        #region Fields

        public int? ZipCodeRangeId
        {
            get { return ViewState["ZipCodeRangeId"] as int?; }
            set { ViewState["ZipCodeRangeId"] = value; }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["Mode"]; }
            set { ViewState["Mode"] = (int)value; }
        }             

        #endregion Fields

        #region Methods

        private void LoadData()
        {
            Debug.Assert(ZipCodeRangeId.HasValue);

            ZipCodeRange range = RequestManager.Services.UsersService.GetZipCodeRangeById(ZipCodeRangeId.Value);

            // boxes
            txtZipCodeStart.Text = range.ZipCodeStart;
            txtZipCodeEnd.Text = range.ZipCodeEnd;
        }

        private bool SaveData()
        {
            ZipCodeRange range = null;

            if (Mode == FormMode.Add)
                range = new ZipCodeRange();
            else
                range = RequestManager.Services.UsersService.GetZipCodeRangeById(ZipCodeRangeId.Value);

            if (range == null)
                return false;

            range.ZipCodeEnd = txtZipCodeStart.Text;
            range.ZipCodeStart = txtZipCodeEnd.Text;

            if (Mode == FormMode.Add)
                RequestManager.Services.UsersService.AddZipCodeRange(range);
            else
                RequestManager.Services.SaveChanges();

            return true;
        }

        private bool CheckExists()
        {
            if (ZipCodeRangeId.HasValue && RequestManager.Services.UsersService.GetZipCodeRangeById(ZipCodeRangeId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void BindControls()
        {
        }

        private void SetNames()
        {
            if (Mode == FormMode.Edit)
            {
                ZipCodeRange range = RequestManager.Services.UsersService.GetZipCodeRangeById(ZipCodeRangeId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"), range.ZipCodeRangeString);
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"), range.ZipCodeRangeString);
            }            
        }

        private void AddScripts()
        {
            string range = String.Format("{0} - {1}", txtZipCodeStart.Text, txtZipCodeEnd.Text).Trim("- ".ToCharArray());

            // enable warning before unloading the page
            RadScriptManager.RegisterStartupScript(this, GetType(),
                "startup_warn",
                "\twarnBeforeUnload = true;\n\tenableWarning();\n\twarningBeforeUnload = \"" + HttpUtility.JavaScriptStringEncode(String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_LeaveConfrmation"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"), String.IsNullOrEmpty(txtZipCodeStart.Text) && String.IsNullOrEmpty(txtZipCodeEnd.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : range)) + "\";\n",
                true);

            // disable warning before postback
            RadScriptManager.RegisterOnSubmitStatement(this, GetType(),
                "onsubmit_disable_warn",
                "disableWarning();");
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindControls();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();

                    SetNames();
                }
            }

            //enter button
            Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                txtZipCodeStart.Focus();
            }

            AddScripts();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"));

                    rwError.VisibleOnPageLoad = true;
                }
                else
                    Response.Redirect("~/Forms/ZipCodeRanges/ZipCodeRangesList.aspx");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string range = String.Format("{0} - {1}", txtZipCodeStart.Text, txtZipCodeEnd.Text).Trim("- ".ToCharArray());

            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_ZipCodeRange"), String.IsNullOrEmpty(txtZipCodeStart.Text) && String.IsNullOrEmpty(txtZipCodeEnd.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : range);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/ZipCodeRanges/ZipCodeRangesList.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/ZipCodeRanges/ZipCodeRangesList.aspx");
        }

        protected void cvZipCodeStart_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = Regex.IsMatch(txtZipCodeStart.Text, "^[0-9]*$") && txtZipCodeStart.Text.Length == 5;
        }

        protected void cvZipCodeEnd_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = Regex.IsMatch(txtZipCodeEnd.Text, "^[0-9]*$") && txtZipCodeEnd.Text.Length == 5;
        }

        #endregion
    }
}