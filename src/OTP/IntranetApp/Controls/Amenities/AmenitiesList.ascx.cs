﻿using BusinessLogic.Managers;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using IntranetApp.Enums;

namespace IntranetApp.Controls.Amenities
{
    public partial class AmenitiesList : System.Web.UI.UserControl
    {
        #region Methods

        private void BindGrid()
        {
            amenitiesLinqDS.DataResolver = () =>
            {
                IQueryable<Amenity> result = RequestManager.Services.AmenityService.GetAmenities().AsQueryable();
                rgAmenities.ApplyFilter<Amenity>(ref result, "Title", (q, f) => q.Where(p => p.TitleCurrentLanguage.ToLower().Contains(f.ToLower())));
                rgAmenities.ApplyFilter<Amenity>(ref result, "Group", (q, f) => q.Where(p => p.AmenityGroup.NameCurrentLanguage.ToLower().Contains(f.ToLower())));
                
                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgAmenities_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int amenityId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AmenityId"].ToString());

                if (RequestManager.Services.AmenityService.GetAmenityById(amenityId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/Amenities/AmenitiesDetails.aspx?id=");
                    sb.Append(amenityId);
                    sb.Append("&mode=");
                    sb.Append((int)FormMode.Edit);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int amenityId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AmenityId"].ToString());

                if (RequestManager.Services.AmenityService.GetAmenityById(amenityId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else if (RequestManager.Services.PropertiesService.HasAnyUseAmenity(amenityId))
                {
                    Amenity amenity = RequestManager.Services.AmenityService.GetAmenityById(amenityId);

                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"), amenity.Title.ToString());
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"), amenity.Title.ToString());

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                }
                else
                    RequestManager.Services.AmenityService.DeleteAmenity(amenityId);
            }
        }

        protected void rgAmenities_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Amenity obj = (Amenity)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgAmenities.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"), obj.Title.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"), obj.Title.ToString()));
            }
        }

        protected void rgAmenities_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgAmenities.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgAmenities.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgAmenities.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgAmenities.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgAmenities.MasterTableView.Rebind();
            }
        }

        protected void btnAddAmenity_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("~/Forms/Amenities/AmenitiesDetails.aspx?");
            sb.Append("mode=");
            sb.Append((int)FormMode.Add);

            Response.Redirect(sb.ToString());
        }

        #endregion Event Handlers
    }
}