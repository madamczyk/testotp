﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AmenityDetails.ascx.cs" Inherits="IntranetApp.Controls.Amenities.AmenityDetails" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="pnlData">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlData" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
        <telerik:RadWindow ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" Text="<%$ Resources: Controls, Dialog_Discard_Text %>" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="btnDiscard_Yes"></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClick="btnDiscard_No"></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
        <telerik:RadWindow ID="rwError" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblError" Font-Bold="true" runat="server" />
                        <br />
                        <br />
                        <asp:Button ID="wndBtnError_Ok" runat="server" Text="<%$ Resources: Controls, Dialog_Ok %>" OnClick="btnError_Ok" />
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<asp:Panel runat="server" ID="pnlData">
    <asp:ValidationSummary ID="vsSummary" ValidationGroup="Amenity" HeaderText=" "
        DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
    <h1 class="FormHeader">
        <asp:Label runat="server" ID="lblHeader" meta:resourcekey="lblHeaderAdd"></asp:Label>
    </h1>
    <br />
    <table>
        <tr>
            <td width="100px">
                <asp:Label runat="server" ID="lblName" meta:resourcekey="lblName" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" TabIndex="1" ID="txtName" Enabled="true" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvName" ValidationGroup="Amenity" runat="server" ControlToValidate="txtName"
                    Display="Dynamic" meta:resourcekey="ValName" Text="*" CssClass="error" />
            </td>
        </tr>
        <tr>
            <td width="100px">
                <asp:Label runat="server" ID="lblGroup" meta:resourcekey="lblGroup" />
            </td>
            <td>
                <telerik:RadComboBox runat="server" TabIndex="2" ID="rcbGroup" Enabled="true" AutoPostBack="false" Width="250px" />
            </td>
        </tr>
        <tr>
            <td width="100px">
                <asp:Label runat="server" ID="lblCountable" meta:resourcekey="lblCountable" />
            </td>
            <td>
                <asp:CheckBox runat="server" TabIndex="3" ID="cbCountable" Enabled="true" AutoPostBack="false" />
            </td>
        </tr>
    </table>
</asp:Panel>

<table>
    <tr style="height: 20px;">
        <td></td>
    </tr>
    <tr>
        <td width="645px" align="right" style="padding-right: 3px">
            <telerik:RadButton runat="server" ID="btnSave" TabIndex="4" Text="<%$ Resources: Controls, Button_Save %>" CausesValidation="true" ValidationGroup="Amenity" OnClick="btnSave_Click" />
            <telerik:RadButton runat="server" ID="btnCancel" TabIndex="5" Text="<%$ Resources: Controls, Button_Cancel %>" CausesValidation="false" OnClick="btnCancel_Click" />
        </td>
    </tr>
</table>
