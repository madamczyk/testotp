﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AmenitiesList.ascx.cs" Inherits="IntranetApp.Controls.Amenities.AmenitiesList" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgAmenities" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgAmenities" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="amenitiesLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgAmenities_ItemCommand" 
        OnItemDataBound="rgAmenities_ItemDataBound"
        OnDataBound="rgAmenities_DataBound">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="AmenityId" DataSourceID="amenitiesLinqDS">
			<Columns>
				<telerik:GridBoundColumn DataField="TitleCurrentLanguage" FilterControlWidth="150px" UniqueName="Title" HeaderText="Title" SortExpression="TitleCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="AmenityGroup.NameCurrentLanguage" FilterControlWidth="150px" UniqueName="Group" HeaderText="Group" SortExpression="AmenityGroup.NameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="Countable" HeaderText="Countable" UniqueName="Countable" SortExpression="Countable" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="false">  
					<HeaderStyle Width="100px"></HeaderStyle>
					<ItemStyle CssClass="link" />
                    <ItemTemplate> 
                        <%# (Boolean.Parse(Eval("Countable").ToString())) ? "Yes" : "No" %> 
                     </ItemTemplate> 
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>		
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="AmenityGroup.NameCurrentLanguage" SortOrder="Ascending" />
            </SortExpressions>
		</MasterTableView>
		<HeaderContextMenu EnableImageSprites="True">
		</HeaderContextMenu>
	</telerik:RadGrid>
    <br />
    <telerik:RadButton runat="server" ID="btnAddAmenity" Text="Add" OnClick="btnAddAmenity_Click" />
</asp:Panel>

<otpDS:SimpleDataSource ID="amenitiesLinqDS" runat="server" />