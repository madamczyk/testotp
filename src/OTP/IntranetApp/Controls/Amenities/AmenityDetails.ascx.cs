﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Amenities
{
    public partial class AmenityDetails : BaseUserControl
    {      
        #region Fields

        public int? AmenityId
        {
            get { return ViewState["AmenityId"] as int?; }
            set { ViewState["AmenityId"] = value; }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["Mode"]; }
            set { ViewState["Mode"] = (int)value; }
        }             

        #endregion Fields

        #region Methods

        private void ParseQueryString()
        {
            int id = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out id))
            {
                AmenityId = id;
            }
        }

        private void LoadData()
        {
            Debug.Assert(AmenityId.HasValue);

            Amenity amenity = RequestManager.Services.AmenityService.GetAmenityById(AmenityId.Value);

            // boxes
            txtName.Text = amenity.Title.ToString();
            rcbGroup.SelectedValue = amenity.AmenityGroup.AmenityGroupId.ToString();
            cbCountable.Checked = amenity.Countable;
        }

        private bool SaveData()
        {
            Amenity amenity = null;

            if (Mode == FormMode.Add)
                amenity = new Amenity();
            else
                amenity = RequestManager.Services.AmenityService.GetAmenityById(AmenityId.Value);

            if (amenity == null)
                return false;

            //TODO (ver2): set text depending on the language
            amenity.SetTitleValue(txtName.Text);
            amenity.AmenityGroup = RequestManager.Services.AmenityGroupsService.GetAmenityGroupById(int.Parse(rcbGroup.SelectedValue));
            amenity.AmenityCode = " ";
            amenity.Countable = cbCountable.Checked;

            if (Mode == FormMode.Add)
                RequestManager.Services.AmenityService.AddAmenity(amenity);
            else
                RequestManager.Services.SaveChanges();

            return true;
        }

        private bool CheckExists()
        {
            if (AmenityId.HasValue && RequestManager.Services.AmenityService.GetAmenityById(AmenityId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void BindControls()
        {
            //load amenity groups
            foreach (AmenityGroup ag in RequestManager.Services.AmenityGroupsService.GetAmenityGroups())
            {
                rcbGroup.Items.Add(new RadComboBoxItem(ag.Name.ToString(), ag.AmenityGroupId.ToString()));
            }
        }

        private void SetNames()
        {
            if (Mode == FormMode.Edit)
            {
                Amenity amenity = RequestManager.Services.AmenityService.GetAmenityById(AmenityId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"), amenity.Title.ToString());
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"), txtName.Text);
            }            
        }

        private void AddScripts()
        {
            // enable warning before unloading the page
            RadScriptManager.RegisterStartupScript(this, GetType(),
                "startup_warn",
                "\twarnBeforeUnload = true;\n\tenableWarning();\n\twarningBeforeUnload = \"" + HttpUtility.JavaScriptStringEncode(String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_LeaveConfrmation"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text)) + "\";\n",
                true);

            // disable warning before postback
            RadScriptManager.RegisterOnSubmitStatement(this, GetType(),
                "onsubmit_disable_warn",
                "disableWarning();");
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ParseQueryString();
                BindControls();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();

                    SetNames();
                }
            }

            //enter button
            Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                txtName.Focus();
            }

            AddScripts();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"));

                    rwError.VisibleOnPageLoad = true;
                }
                else
                    Response.Redirect("~/Forms/Amenities/AmenitiesList.aspx");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Amenity"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Amenities/AmenitiesList.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Amenities/AmenitiesList.aspx");
        }

        #endregion
    }
}