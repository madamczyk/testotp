﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Tags
{
    public partial class TagDetails : BaseUserControl
    {      
        #region Fields

        public int? TagId
        {
            get { return ViewState["TagId"] as int?; }
            set { ViewState["TagId"] = value; }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["Mode"]; }
            set { ViewState["Mode"] = (int)value; }
        }             

        #endregion Fields

        #region Methods

        private void ParseQueryString()
        {
            int userID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out userID))
            {
                TagId = userID;
            }
        }

        private void LoadData()
        {
            Debug.Assert(TagId.HasValue);

            Tag tag = RequestManager.Services.TagsService.GetTagById(TagId.Value);

            //textboxes
            txtName.Text = tag.Name.ToString();
            rcbGroup.SelectedValue = tag.TagGroup.TagGroupId.ToString();
        }

        private bool SaveData()
        {
            Tag tag = null;
            if (Mode == FormMode.Add)
                tag = new Tag();
            else
                tag = RequestManager.Services.TagsService.GetTagById(TagId.Value);

            if (tag == null)
                return false;

            //TODO (ver2): set text depending on the language
            tag.SetNameValue(txtName.Text);
            tag.TagGroup = RequestManager.Services.TagGroupsService.GetTagGroupById(int.Parse(rcbGroup.SelectedValue));
            
            if (Mode == FormMode.Add)
                RequestManager.Services.TagsService.AddTag(tag);
            else
                RequestManager.Services.SaveChanges();

            return true;
        }

        private bool CheckExists()
        {
            if (TagId.HasValue && RequestManager.Services.TagsService.GetTagById(TagId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Tag"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_Tag"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void BindControls()
        {
            //load amenity groups
            foreach (TagGroup tg in RequestManager.Services.TagGroupsService.GetTagGroups())
            {
                rcbGroup.Items.Add(new RadComboBoxItem(tg.Name.ToString(), tg.TagGroupId.ToString()));
            }
        }

        private void SetNames()
        {
            if (Mode == FormMode.Edit)
            {
                Tag tag = RequestManager.Services.TagsService.GetTagById(TagId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Tag"), tag.Name.ToString());
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"), txtName.Text);
            }             
        }

        private void AddScripts()
        {
            // enable warning before unloading the page
            RadScriptManager.RegisterStartupScript(this, GetType(),
                "startup_warn",
                "\twarnBeforeUnload = true;\n\tenableWarning();\n\twarningBeforeUnload = \"" + HttpUtility.JavaScriptStringEncode(String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_LeaveConfrmation"), (string)GetGlobalResourceObject("Controls", "Object_Tag"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text)) + "\";\n",
                true);

            // disable warning before postback
            RadScriptManager.RegisterOnSubmitStatement(this, GetType(),
                "onsubmit_disable_warn",
                "disableWarning();");
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ParseQueryString();
                BindControls();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();

                    SetNames();
                }
            }

            //enter button
            Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                txtName.Focus();
            }

            AddScripts();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Tag"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_Tag"));

                    rwError.VisibleOnPageLoad = true;
                }
                else
                    Response.Redirect("~/Forms/Tags/TagsList.aspx");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Tag"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Tags/TagsList.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Tags/TagsList.aspx");
        }

        #endregion
    }
}