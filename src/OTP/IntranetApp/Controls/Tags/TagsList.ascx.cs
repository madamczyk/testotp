﻿using BusinessLogic.Managers;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using IntranetApp.Enums;

namespace IntranetApp.Controls.Tags
{
    public partial class TagsList : System.Web.UI.UserControl
    {
        #region Methods

        private void BindGrid()
        {
            this.tagsLinqDS.DataResolver = () =>
            {
                IQueryable<Tag> result = RequestManager.Services.TagsService.GetTags().AsQueryable();
                rgTags.ApplyFilter<Tag>(ref result, "Name", (q, f) => q.Where(p => p.NameCurrentLanguage.ToLower().Contains(f.ToLower())));
                rgTags.ApplyFilter<Tag>(ref result, "Group", (q, f) => q.Where(p => p.TagGroup.NameCurrentLanguage.ToLower().Contains(f.ToLower())));
                
                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgTags_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int tagId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["TagID"].ToString());

                if (RequestManager.Services.TagsService.GetTagById(tagId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Tag"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_Tag"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/Tags/TagsDetails.aspx?id=");
                    sb.Append(tagId);
                    sb.Append("&mode=");
                    sb.Append((int)FormMode.Edit);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int tagId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["TagID"].ToString());

                if (RequestManager.Services.TagsService.GetTagById(tagId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Tag"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Tag"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else if (RequestManager.Services.PropertiesService.HasAnyUseTag(tagId))
                {
                    Tag tag = RequestManager.Services.TagsService.GetTagById(tagId);

                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Tag"), tag.Name.ToString());
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_Tag"), tag.Name.ToString());

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                }
                else
                    RequestManager.Services.TagsService.DeleteTag(tagId);
            }
        }

        protected void rgTags_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Tag obj = (Tag)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgTags.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Tag"), obj.Name.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Tag"), obj.Name.ToString()));
            }
        }

        protected void rgTags_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgTags.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgTags.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgTags.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgTags.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgTags.MasterTableView.Rebind();
            }
        }

        protected void btnAddTag_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("~/Forms/Tags/TagsDetails.aspx?");
            sb.Append("mode=");
            sb.Append((int)FormMode.Add);

            Response.Redirect(sb.ToString());
        }

        #endregion Event Handlers
    }
}