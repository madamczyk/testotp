﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.PropertyTypes
{
    public partial class PropertyTypeDetails : BaseUserControl
    {      
        #region Fields

        public int? PropertyTypeId
        {
            get { return ViewState["PropertyTypeId"] as int?; }
            set { ViewState["PropertyTypeId"] = value; }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["Mode"]; }
            set { ViewState["Mode"] = (int)value; }
        }             

        #endregion Fields

        #region Methods

        private void ParseQueryString()
        {
            int userID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out userID))
            {
                PropertyTypeId = userID;
            }
        }

        private void LoadData()
        {
            Debug.Assert(PropertyTypeId.HasValue);

            PropertyType propertyType = RequestManager.Services.PropertyTypeService.GetPropertyTypeById(PropertyTypeId.Value);

            //textboxes
            txtName.Text = propertyType.Name.ToString();
        }

        private bool SaveData()
        {
            PropertyType propertyType = null;
            if (Mode == FormMode.Add)
                propertyType = new PropertyType();
            else
                propertyType = RequestManager.Services.PropertyTypeService.GetPropertyTypeById(PropertyTypeId.Value);

            if (propertyType == null)
                return false;

            //TODO (ver2): set text depending on the language
            propertyType.SetNameValue(txtName.Text);

            if (Mode == FormMode.Add)
                RequestManager.Services.PropertyTypeService.AddPropertyType(propertyType);
            else
                RequestManager.Services.SaveChanges();

            return true;
        }

        private bool CheckExists()
        {
            if (PropertyTypeId.HasValue && RequestManager.Services.PropertyTypeService.GetPropertyTypeById(PropertyTypeId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void BindControls()
        {
        }

        private void SetNames()
        {
            if (Mode == FormMode.Edit)
            {
                PropertyType propertyType = RequestManager.Services.PropertyTypeService.GetPropertyTypeById(PropertyTypeId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"), propertyType.Name.ToString());
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"), txtName.Text);
            }          
        }

        private void AddScripts()
        {
            // enable warning before unloading the page
            RadScriptManager.RegisterStartupScript(this, GetType(),
                "startup_warn",
                "\twarnBeforeUnload = true;\n\tenableWarning();\n\twarningBeforeUnload = \"" + HttpUtility.JavaScriptStringEncode(String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_LeaveConfrmation"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text)) + "\";\n",
                true);

            // disable warning before postback
            RadScriptManager.RegisterOnSubmitStatement(this, GetType(),
                "onsubmit_disable_warn",
                "disableWarning();");
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ParseQueryString();
                BindControls();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();

                    SetNames();
                }
            }

            //enter button
            Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                txtName.Focus();
            }

            AddScripts();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"));

                    rwError.VisibleOnPageLoad = true;
                }
                else
                    Response.Redirect("~/Forms/PropertyTypes/PropertyTypesList.aspx");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/PropertyTypes/PropertyTypesList.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/PropertyTypes/PropertyTypesList.aspx");
        }

        #endregion
    }
}