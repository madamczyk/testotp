﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyTypesList.ascx.cs" Inherits="IntranetApp.Controls.PropertyTypes.PropertyTypesList" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgPropertyTypes" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgPropertyTypes" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="propertyTypesLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgPropertyTypes_ItemCommand" 
        OnItemDataBound="rgPropertyTypes_ItemDataBound"
        OnDataBound="rgPropertyTypes_DataBound">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="PropertyTypeId" DataSourceID="propertyTypesLinqDS">
			<Columns>
				<telerik:GridBoundColumn DataField="NameCurrentLanguage" FilterControlWidth="150px" UniqueName="Name" HeaderText="Name" SortExpression="NameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>				
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="NameCurrentLanguage" SortOrder="Ascending" />
            </SortExpressions>
		</MasterTableView>
		<HeaderContextMenu EnableImageSprites="True">
		</HeaderContextMenu>
	</telerik:RadGrid>
    <br />
    <telerik:RadButton runat="server" ID="btnAddPropertyType" Text="Add" OnClick="btnAddPropertyType_Click" />
</asp:Panel>

<otpDS:SimpleDataSource ID="propertyTypesLinqDS" runat="server" />