﻿using BusinessLogic.Managers;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using IntranetApp.Enums;

namespace IntranetApp.Controls.PropertyTypes
{
    public partial class PropertyTypesList : System.Web.UI.UserControl
    {
        #region Methods

        private void BindGrid()
        {
            propertyTypesLinqDS.DataResolver = () =>
            {
                IQueryable<PropertyType> result = RequestManager.Services.PropertyTypeService.GetPropertyTypes().AsQueryable();
                rgPropertyTypes.ApplyFilter<PropertyType>(ref result, "Name", (q, f) => q.Where(p => p.NameCurrentLanguage.ToLower().Contains(f.ToLower())));
                
                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgPropertyTypes_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int propertyTypeId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyTypeId"].ToString());

                if (RequestManager.Services.PropertyTypeService.GetPropertyTypeById(propertyTypeId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/PropertyTypes/PropertyTypesDetails.aspx?id=");
                    sb.Append(propertyTypeId);
                    sb.Append("&mode=");
                    sb.Append((int)FormMode.Edit);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int propertyTypeId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyTypeId"].ToString());

                if (RequestManager.Services.PropertyTypeService.GetPropertyTypeById(propertyTypeId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else if (RequestManager.Services.PropertiesService.HasAnyUsePropertyType(propertyTypeId))
                {
                    PropertyType propertyType = RequestManager.Services.PropertyTypeService.GetPropertyTypeById(propertyTypeId);

                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"), propertyType.Name.ToString());
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"), propertyType.Name.ToString());

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                }
                else
                    RequestManager.Services.PropertyTypeService.DeletePropertyType(propertyTypeId);
            }
        }

        protected void rgPropertyTypes_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgPropertyTypes.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgPropertyTypes.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgPropertyTypes.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgPropertyTypes.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgPropertyTypes.MasterTableView.Rebind();
            }
        }

        protected void rgPropertyTypes_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                PropertyType obj = (PropertyType)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgPropertyTypes.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"), obj.Name.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_PropertyType"), obj.Name.ToString()));
            }
        }

        protected void btnAddPropertyType_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("~/Forms/PropertyTypes/PropertyTypesDetails.aspx?");
            sb.Append("mode=");
            sb.Append((int)FormMode.Add);

            Response.Redirect(sb.ToString());
        }

        #endregion Event Handlers
    }
}