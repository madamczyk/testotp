﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Common.Extensions;

namespace IntranetApp.Controls.Notifications
{
    public partial class NotificationsList : System.Web.UI.UserControl
    {
        #region Fields

        public int? StatusFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["StatusFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["StatusFilterSelectedValue"] as int?;
            }
            set { ViewState["StatusFilterSelectedValue"] = (int)value; }
        }

        public int? TypeFilterSelectedValue
        {
            get
            {
                if (((int?)ViewState["TypeFilterSelectedValue"]) == -1)
                    return (int?)null;
                else
                    return ViewState["TypeFilterSelectedValue"] as int?;
            }
            set { ViewState["TypeFilterSelectedValue"] = (int)value; }
        }

        public DateTime? CompletedDateFilterDateFrom
        {
            get
            {
                if (ViewState["CompletedDateFilterDateFrom"] != null)
                    return (DateTime)ViewState["CompletedDateFilterDateFrom"];
                return null;
            }
            set
            {
                ViewState["CompletedDateFilterDateFrom"] = value;
            }
        }
        
        public DateTime? CompletedDateFilterDateTo
        {
            get
            {
                if (ViewState["CompletedDateFilterDateTo"] != null)
                    return ((DateTime)ViewState["CompletedDateFilterDateTo"]).AddDays(1).AddMilliseconds(-1);
                return null;
            }
            set
            {
                ViewState["CompletedDateFilterDateTo"] = value;
            }
        }

        public DateTime? CreatedDateFilterDateFrom
        {
            get
            {
                if (ViewState["CreatedDateFilterDateFrom"] != null)
                    return (DateTime)ViewState["CreatedDateFilterDateFrom"];
                return null;
            }
            set
            {
                ViewState["CreatedDateFilterDateFrom"] = value;
            }
        }
        
        public DateTime? CreatedDateFilterDateTo
        {
            get
            {
                if (ViewState["CreatedDateFilterDateTo"] != null)
                    return ((DateTime)ViewState["CreatedDateFilterDateTo"]).AddDays(1).AddMilliseconds(-1);
                return null;
            }
            set
            {
                ViewState["CreatedDateFilterDateTo"] = value;
            }
        }

        public List<int> SelectedStatusFilterValue
        {
            get { return (List<int>)ViewState["SelectedStatusFilterValue"]; }
            set { ViewState["SelectedStatusFilterValue"] = value; }
        }

        #endregion Fields

        #region Methods

        private void BindGrid()
        {
            this.notificationsLinqDS.DataResolver = () =>
            {
                IQueryable<Message> result = RequestManager.Services.MessageService.GetMessages().AsQueryable();
                rgNotifications.ApplyFilter<Message>(ref result, "Recipients", (q, f) => q.Where(p => p.Recipients.ToLower().Contains(f.ToLower())));
                rgNotifications.ApplyFilter<Message>(ref result, "Subject", (q, f) => q.Where(p => p.Subject.ToLower().Contains(f.ToLower())));
                
                //created date
                //rgNotifications.ApplyFilter<Message>(ref result, "Country", (q, f) => q.Where(p => p.DictionaryCountry.CountryNameCurrentLanguage.ToLower().Contains(f.ToLower())));
                if (CreatedDateFilterDateFrom.HasValue)
                    result = result.Where(ob => ob.Created >= this.CreatedDateFilterDateFrom.Value);
                if (CreatedDateFilterDateTo.HasValue)
                    result = result.Where(ob => ob.Created <= this.CreatedDateFilterDateTo.Value);

                //completed
                if (CompletedDateFilterDateFrom.HasValue)
                    result = result.Where(ob => ob.CompleteDate.HasValue && ob.CompleteDate.Value >= this.CompletedDateFilterDateFrom.Value);
                if (CompletedDateFilterDateTo.HasValue)
                    result = result.Where(ob => ob.CompleteDate.HasValue && ob.CompleteDate.Value <= this.CompletedDateFilterDateTo.Value);
                
                //status ddl
                if(StatusFilterSelectedValue.HasValue)
                    result = result.Where(ob => ob.Status == StatusFilterSelectedValue.Value);

                //type
                if (TypeFilterSelectedValue.HasValue)
                    result = result.Where(ob => ob.Type == TypeFilterSelectedValue.Value);
                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void rdpFilterCreatedDateFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.CreatedDateFilterDateFrom = e.NewDate;

            this.BindGrid();
            this.rgNotifications.MasterTableView.Rebind();
        }

        protected void rdpFilterCreatedDateTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.CreatedDateFilterDateTo = e.NewDate;

            this.BindGrid();
            this.rgNotifications.MasterTableView.Rebind();
        }

        protected void rdpFilterCompletedDateFrom_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.CompletedDateFilterDateFrom = e.NewDate;

            this.BindGrid();
            this.rgNotifications.MasterTableView.Rebind();
        }

        protected void rdpFilterCompletedDateTo_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.CompletedDateFilterDateTo = e.NewDate;

            this.BindGrid();
            this.rgNotifications.MasterTableView.Rebind();
        }

        protected void rdpFilterCreatedDateFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.CreatedDateFilterDateFrom;
        }

        protected void rdpFilterCreatedDateTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.CreatedDateFilterDateTo;
        }

        protected void rdpFilterCompletedDateFrom_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.CompletedDateFilterDateFrom;
        }

        protected void rdpFilterCompletedDateTo_PreRender(object sender, EventArgs e)
        {
            RadDatePicker picker = sender as RadDatePicker;
            picker.SelectedDate = this.CompletedDateFilterDateTo;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgNotifications_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == "Resend")
            {
                int messageId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["MessageId"].ToString());
                Message message = RequestManager.Services.MessageService.GetMessageById(messageId);

                if (message == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Property"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Property"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    message.Status = (int)MessageStatus.New;
                    RequestManager.Services.SaveChanges();
                    this.rgNotifications.MasterTableView.Rebind();
                }                
            }
        }

        protected void rgNotifications_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Message obj = (Message)item.DataItem;
                item["Status"].Text = EnumConverter.GetValue((MessageStatus)obj.Status);
                item["Type"].Text = EnumConverter.GetValue((TemplateType)obj.Type);
                item["Created"].Text = obj.Created.ToLocalizedDateTimeString();
                item["CompleteDate"].Text = 
                    obj.CompleteDate.HasValue ? obj.CompleteDate.Value.ToLocalizedDateTimeString() : String.Empty;

                ImageButton image = (ImageButton)item["ResendColumn"].Controls[0];
                if (obj.Status == (int)MessageStatus.New || obj.Status == (int)MessageStatus.Pending)
                {
                    image.Visible = false;
                }
                else
                {
                    string title = String.Format((string)GetLocalResourceObject("lblResendTitle.Text"));
                    string body = String.Format((string)GetLocalResourceObject("lblResendBody.Text"), obj.Subject , obj.Recipients);

                    image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", 
                        rgNotifications.ClientID, 
                        body,
                        title
                        );

                }
            }
        }

        protected void rgNotifications_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgNotifications.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgNotifications.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgNotifications.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgNotifications.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgNotifications.MasterTableView.Rebind();
            }
        }

        protected void rcbFilterStatus_PreRender(object sender, EventArgs e)
        {
            RadComboBox combo = sender as RadComboBox;
            if (this.StatusFilterSelectedValue.HasValue)
                combo.SelectedValue = this.StatusFilterSelectedValue.ToString();
            else
                combo.SelectedValue = "-1";
        }

        protected void rcbFilterStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            int value = int.Parse(e.Value);
            //save selected filter value
            this.StatusFilterSelectedValue = value;
            this.BindGrid();
            this.rgNotifications.MasterTableView.Rebind();           
        }

        protected void rcbStatus_OnInit(object sender, EventArgs args)
        {
            RadComboBox rcbStatus = (RadComboBox)sender;
            //load payment types
            rcbStatus.Items.Add(new RadComboBoxItem("", "-1"));
            Array productType = Enum.GetValues(typeof(MessageStatus));
            foreach (int item in productType)
                rcbStatus.Items.Add(new RadComboBoxItem(EnumConverter.GetValue((MessageStatus)item), item.ToString()));
        }

        //type
        protected void rcbFilterType_PreRender(object sender, EventArgs e)
        {
            RadComboBox combo = sender as RadComboBox;
            if (this.TypeFilterSelectedValue.HasValue)
                combo.SelectedValue = this.TypeFilterSelectedValue.ToString();
            else
                combo.SelectedValue = "-1";
        }

        protected void rcbFilterType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            int value = int.Parse(e.Value);
            //save selected filter value
            this.TypeFilterSelectedValue = value;
            this.BindGrid();
            this.rgNotifications.MasterTableView.Rebind();
        }

        protected void rcbType_OnInit(object sender, EventArgs args)
        {
            RadComboBox rcbType= (RadComboBox)sender;
            //load payment types
            rcbType.Items.Add(new RadComboBoxItem("", "-1"));
            Array messageType = Enum.GetValues(typeof(TemplateType));
            foreach (int item in messageType)
                if(item < 100)
                    rcbType.Items.Add(new RadComboBoxItem(EnumConverter.GetValue((TemplateType)item), item.ToString()));
        }

        #endregion Event Handlers
    }
}