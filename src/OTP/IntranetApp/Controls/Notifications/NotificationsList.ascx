﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotificationsList.ascx.cs" Inherits="IntranetApp.Controls.Notifications.NotificationsList" %>


<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgNotifications" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgNotifications" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="notificationsLinqDS"
		ClientSettings-EnablePostBackOnRowClick="false" 
        OnItemCommand="rgNotifications_ItemCommand" 
        OnItemDataBound="rgNotifications_ItemDataBound"
        OnDataBound="rgNotifications_DataBound">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="False" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="MessageId" DataSourceID="notificationsLinqDS">
			<Columns>

				<telerik:GridBoundColumn DataField="Recipients" FilterControlWidth="150px" UniqueName="Recipients" HeaderText="Recipients" SortExpression="Recipients" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>

				<telerik:GridBoundColumn DataField="Subject" FilterControlWidth="150px" UniqueName="Subject" HeaderText="Subject" SortExpression="Subject" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                
                <telerik:GridBoundColumn DataField="Created" FilterControlWidth="150px" UniqueName="Created" HeaderText="Created" SortExpression="Created" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
                    <FilterTemplate>
                        <span style="width: 30px;"><asp:Label runat="server" ID="lblDateFrom" meta:resourceKey="lblDateFrom" />&nbsp;</span>
                        <telerik:RadDatePicker OnPreRender="rdpFilterCreatedDateFrom_PreRender" DateInput-Width="75px" Width="90px" runat="server" AutoPostBack="true" ID="rdpFilterCreatedDateFrom" OnSelectedDateChanged="rdpFilterCreatedDateFrom_SelectedDateChanged" >
                            <DateInput 
                                DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                        </telerik:RadDatePicker><br />
                        <span style="width: 30px; margin-right: 13px;"><asp:Label runat="server" CssClass="dateTo" ID="lblDateTo" meta:resourceKey="lblDateTo" />&nbsp;</span>
                        <telerik:RadDatePicker OnPreRender="rdpFilterCreatedDateTo_PreRender" DateInput-Width="75px" Width="90px" runat="server" AutoPostBack="true" ID="rdpFilterCreatedDateTo" OnSelectedDateChanged="rdpFilterCreatedDateTo_SelectedDateChanged" >
                            <DateInput 
                                DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                        </telerik:RadDatePicker>
                    </FilterTemplate>
				</telerik:GridBoundColumn>

                <telerik:GridBoundColumn DataField="CompleteDate" FilterControlWidth="150px" UniqueName="CompleteDate" HeaderText="Complete Date" SortExpression="CompleteDate" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
                    <FilterTemplate>
                        <span style="width: 30px;"><asp:Label runat="server" ID="lblDateFrom1" meta:resourceKey="lblDateFrom" />&nbsp;</span>
                        <telerik:RadDatePicker OnPreRender="rdpFilterCompletedDateFrom_PreRender" DateInput-Width="75px" Width="90px" runat="server" AutoPostBack="true" ID="rdpFilterCompletedDateFrom" OnSelectedDateChanged="rdpFilterCompletedDateFrom_SelectedDateChanged" >
                            <DateInput 
                                DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                        </telerik:RadDatePicker><br />
                        <span style="width: 30px; margin-right: 13px;"><asp:Label runat="server" CssClass="dateTo" ID="lblDateTo1" meta:resourceKey="lblDateTo" />&nbsp;</span>
                        <telerik:RadDatePicker OnPreRender="rdpFilterCompletedDateTo_PreRender" DateInput-Width="75px" Width="90px" runat="server" AutoPostBack="true" ID="rdpFilterCompletedDateTo" OnSelectedDateChanged="rdpFilterCompletedDateTo_SelectedDateChanged" >
                            <DateInput 
                                DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                        </telerik:RadDatePicker>
                    </FilterTemplate>
				</telerik:GridBoundColumn>

                <telerik:GridBoundColumn DataField="Status" FilterControlWidth="150px" UniqueName="Status" HeaderText="Status" SortExpression="Status" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbStatus" AutoPostBack="true" EnableViewState="true"  OnSelectedIndexChanged="rcbFilterStatus_SelectedIndexChanged" OnPreRender="rcbFilterStatus_PreRender" OnInit="rcbStatus_OnInit" />
					 </FilterTemplate>
                    <HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>

                <telerik:GridBoundColumn DataField="Status" FilterControlWidth="150px" UniqueName="Type" HeaderText="Type" SortExpression="Type" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbType" AutoPostBack="true" EnableViewState="true"  OnSelectedIndexChanged="rcbFilterType_SelectedIndexChanged" OnPreRender="rcbFilterType_PreRender" OnInit="rcbType_OnInit" />
					 </FilterTemplate>
                    <HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>

                <telerik:GridButtonColumn CommandName="Resend" ButtonType="ImageButton" UniqueName="ResendColumn" HeaderText="Resend" ImageUrl="/Images/resend.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>
                		
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="Created" SortOrder="Descending" />
            </SortExpressions>
		</MasterTableView>
		<HeaderContextMenu EnableImageSprites="True">
		</HeaderContextMenu>
	</telerik:RadGrid>
</asp:Panel>

<otpDS:SimpleDataSource ID="notificationsLinqDS" runat="server" />