﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DestinationsList.ascx.cs" Inherits="IntranetApp.Controls.Destinations.DestinationsList" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgDestinations" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgDestinations" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="destinationsLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgDestinations_ItemCommand" 
        OnItemDataBound="rgDestinations_ItemDataBound"
        OnDataBound="rgDestinations_DataBound">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="DestinationID" DataSourceID="destinationsLinqDS">
			<Columns>
				<telerik:GridBoundColumn DataField="DestinationNameCurrentLanguage" FilterControlWidth="150px" UniqueName="Name" HeaderText="Name" SortExpression="DestinationNameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Properties.Count" FilterControlWidth="150px" UniqueName="Properties.Count" HeaderText="Properties Number" SortExpression="Properties.Count" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Active" FilterControlWidth="150px" UniqueName="Active" HeaderText="Is active" SortExpression="Active" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
					<FilterTemplate>
						<telerik:RadComboBox runat="server" ID="rcbStatus" AutoPostBack="true" EnableViewState="true"  OnSelectedIndexChanged="rcbFilterStatus_SelectedIndexChanged" OnPreRender="rcbFilterStatus_PreRender" OnInit="rcbStatus_OnInit" />
					 </FilterTemplate>
                    <HeaderStyle Width="120px"></HeaderStyle>
					<ItemStyle CssClass="link" />
				</telerik:GridBoundColumn>
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridButtonColumn>			
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="DestinationNameCurrentLanguage" SortOrder="Ascending" />
            </SortExpressions>
		</MasterTableView>
		<HeaderContextMenu EnableImageSprites="True">
		</HeaderContextMenu>
	</telerik:RadGrid>
    <br />
    <telerik:RadButton runat="server" ID="btnAddDestination" Text="Add" OnClick="btnAddDestination_Click" />
</asp:Panel>

<otpDS:SimpleDataSource ID="destinationsLinqDS" runat="server" />