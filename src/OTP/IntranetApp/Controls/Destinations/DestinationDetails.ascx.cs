﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Destinations
{
    public partial class DestinationDetails : BaseUserControl
    {      
        #region Fields

        public int? DestinationId
        {
            get { return ViewState["DestinationId"] as int?; }
            set { ViewState["DestinationId"] = value; }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["Mode"]; }
            set { ViewState["Mode"] = (int)value; }
        }             

        #endregion Fields

        #region Methods

        private void ParseQueryString()
        {
            int userID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out userID))
            {
                DestinationId = userID;
            }
        }

        private void LoadData()
        {
            Debug.Assert(DestinationId.HasValue);

            Destination destination = RequestManager.Services.DestinationService.GetDestinationById(DestinationId.Value);

            //textboxes
            txtName.Text = destination.DestinationName.ToString();
            txtDescription.Text = destination.DestinationDescription.ToString();
            chbActive.Checked = destination.Active;
        }

        private bool SaveData()
        {
            Destination destination = null;
            List<Tax>.Enumerator taxListEnumerator;
            Tax tax = null;

            if (Mode == FormMode.Add)
                destination = new Destination();
            else
                destination = RequestManager.Services.DestinationService.GetDestinationById(DestinationId.Value);

            if (destination == null)
                return false;

            destination.Active = chbActive.Checked;
            destination.SetDestinationNameValue(txtName.Text);
            destination.SetDestinationDescriptionValue(txtDescription.Text);

            if (Mode == FormMode.Add)
                RequestManager.Services.DestinationService.AddDestination(destination);

            RequestManager.Services.SaveChanges();

            foreach (var id in ctrlTaxes.removedTaxesList) // delete removed taxes
                RequestManager.Services.TaxesService.DeleteTax(id);

            taxListEnumerator = ctrlTaxes.newTaxesList.GetEnumerator();
            while (taxListEnumerator.MoveNext()) // add new taxes, but update their destination first
            {
                taxListEnumerator.Current.Destination = destination;
                RequestManager.Services.TaxesService.AddTax(taxListEnumerator.Current);
            }

            foreach (var t in ctrlTaxes.modifiedTaxesList)
            {
                tax = RequestManager.Services.TaxesService.GetTaxById(t.TaxId);
                
                tax.Destination = destination;
                tax.Percentage = t.Percentage;
                tax.SetNameValue(t.NameCurrentLanguage);
            }

            RequestManager.Services.SaveChanges();

            return true;
        }

        private bool CheckExists()
        {
            if (DestinationId.HasValue && RequestManager.Services.DestinationService.GetDestinationById(DestinationId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Destination"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_Destination"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void BindControls()
        {
            ctrlTaxes.taxesList = new List<Tax>();
            ctrlTaxes.removedTaxesList = new List<int>();
            ctrlTaxes.newTaxesList = new List<Tax>();
            ctrlTaxes.modifiedTaxesList = new List<Tax>();

            if (Mode == FormMode.Edit)
            {
                Tax tax = null;
                foreach (var t in RequestManager.Services.TaxesService.GetTaxesForDestination(DestinationId.Value))
                {
                    tax = new Tax();
                    tax.TaxId = t.TaxId;
                    tax.Destination = RequestManager.Services.DestinationService.GetDestinationById(DestinationId.Value);
                    tax.Percentage = t.Percentage;
                    tax.SetNameValue(t.NameCurrentLanguage);
                    tax.PersistI18nValues();

                    ctrlTaxes.taxesList.Add(tax);
                }
            }            
        }

        private void SetNames()
        {
            if (Mode == FormMode.Edit)
            {
                Destination destination = RequestManager.Services.DestinationService.GetDestinationById(DestinationId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Destination"), destination.DestinationName.ToString());
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"), txtName.Text);
            }            
        }

        private void AddScripts()
        {
            // enable warning before unloading the page
            RadScriptManager.RegisterStartupScript(this, GetType(),
                "startup_warn",
                "\twarnBeforeUnload = true;\n\tenableWarning();\n\twarningBeforeUnload = \"" + HttpUtility.JavaScriptStringEncode(String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_LeaveConfrmation"), (string)GetGlobalResourceObject("Controls", "Object_Destination"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text)) + "\";\n",
                true);

            // disable warning before postback
            RadScriptManager.RegisterOnSubmitStatement(this, GetType(),
                "onsubmit_disable_warn",
                "disableWarning();");
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ParseQueryString();
                BindControls();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();

                    SetNames();
                }
            }

            //enter button
            Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                txtName.Focus();
            }

            AddScripts();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Destination"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_Destination"));

                    rwError.VisibleOnPageLoad = true;
                }
                else
                    Response.Redirect("~/Forms/Destinations/DestinationsList.aspx");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Destination"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Destinations/DestinationsList.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Destinations/DestinationsList.aspx");
        }

        #endregion
    }
}