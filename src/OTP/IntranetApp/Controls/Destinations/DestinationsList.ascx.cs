﻿using BusinessLogic.Managers;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using IntranetApp.Enums;

namespace IntranetApp.Controls.Destinations
{
    public partial class DestinationsList : System.Web.UI.UserControl
    {
        public string StatusFilterSelectedValue
        {
            get
            {
                if (ViewState["StatusFilterSelectedValue"] == null)
                {
                    ViewState["StatusFilterSelectedValue"] = "-1";
                }
                if (((string)ViewState["StatusFilterSelectedValue"]) == "-1")
                    return null;
                else
                    return ViewState["StatusFilterSelectedValue"] as string;
            }
            set { ViewState["StatusFilterSelectedValue"] = value; }
        }

        #region Methods

        private void BindGrid()
        {
            destinationsLinqDS.DataResolver = () =>
            {
                IQueryable<Destination> result = RequestManager.Services.DestinationService.GetDestinations().AsQueryable();
                rgDestinations.ApplyFilter<Destination>(ref result, "Name", (q, f) => q.Where(p => p.DestinationNameCurrentLanguage.ToLower().Contains(f.ToLower())));
                rgDestinations.ApplyFilter<Destination>(ref result, "Properties.Count", (q, f) => q.Where(p => p.Properties.Count.ToString() == f));

                bool active;
                if (bool.TryParse(StatusFilterSelectedValue, out active))
                {
                    result = result.Where(ob => ob.Active == active);
                }

                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgDestinations_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int destinationId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["DestinationID"].ToString());

                if (RequestManager.Services.DestinationService.GetDestinationById(destinationId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Destination"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_Destination"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/Destinations/DestinationsDetails.aspx?id=");
                    sb.Append(destinationId);
                    sb.Append("&mode=");
                    sb.Append((int)FormMode.Edit);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int destinationId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["DestinationID"].ToString());

                if (RequestManager.Services.DestinationService.GetDestinationById(destinationId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Destination"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Destination"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else if (RequestManager.Services.DestinationService.HasAnyDependencies(destinationId))
                {
                    Destination destination = RequestManager.Services.DestinationService.GetDestinationById(destinationId);

                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Destination"), destination.DestinationName.ToString());
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_Destination"), destination.DestinationName.ToString());

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                }
                else
                    RequestManager.Services.DestinationService.DeleteDestination(destinationId);
            }
        }

        protected void rgDestinations_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Destination obj = (Destination)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgDestinations.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Destination"), obj.DestinationName.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Destination"), obj.DestinationName.ToString()));
                item["Active"].Text = obj.Active ? "Yes" : "No";
            }
        }

        protected void rgDestinations_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgDestinations.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgDestinations.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgDestinations.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgDestinations.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgDestinations.MasterTableView.Rebind();
            }
        }

        protected void btnAddDestination_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("~/Forms/Destinations/DestinationsDetails.aspx?");
            sb.Append("mode=");
            sb.Append((int)FormMode.Add);

            Response.Redirect(sb.ToString());
        }

        protected void rcbFilterStatus_PreRender(object sender, EventArgs e)
        {
            RadComboBox combo = sender as RadComboBox;
            bool active;
            if (bool.TryParse(StatusFilterSelectedValue, out active))
            {
                combo.SelectedValue = active.ToString();
            }
            else
                combo.SelectedValue = "-1";
        }

        protected void rcbFilterStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //save selected filter value
            this.StatusFilterSelectedValue = e.Value;
            this.BindGrid();
            this.rgDestinations.MasterTableView.Rebind();
        }

        protected void rcbStatus_OnInit(object sender, EventArgs args)
        {
            RadComboBox rcbStatus = (RadComboBox)sender;
            //load payment types
            rcbStatus.Items.Add(new RadComboBoxItem("", "-1"));
            rcbStatus.Items.Add(new RadComboBoxItem("Yes", "True"));
            rcbStatus.Items.Add(new RadComboBoxItem("No", "False"));

        }

        #endregion Event Handlers
    }
}