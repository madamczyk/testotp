﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Taxes.ascx.cs" Inherits="IntranetApp.Controls.Destinations.Taxes" %>

<telerik:RadGrid 
    AutoGenerateColumns="False" 
    ID="rgTaxes" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="True" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="taxesLinqDS"
	ClientSettings-EnablePostBackOnRowClick="false" 
    OnItemCommand="rgTaxes_ItemCommand" 
    OnDataBound="rgTaxes_DataBound"
    OnItemDataBound="rgTaxes_ItemDataBound"
    OnUpdateCommand="rgTaxes_UpdateCommand"
    OnInsertCommand="rgTaxes_InsertCommand"
    ClientSettings-ClientEvents-OnColumnClick="disableWarning" >
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="True" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Fixed" DataKeyNames="TaxId" DataSourceID="taxesLinqDS" CommandItemDisplay="Bottom" meta:resourceKey="tblTaxes">
		<Columns>           
			<telerik:GridBoundColumn DataField="NameCurrentLanguage" FilterControlWidth="150px" UniqueName="Name" HeaderText="Name" SortExpression="NameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Percentage" FilterControlWidth="150px" UniqueName="Percentage" HeaderText="Percentage" SortExpression="Percentage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" HeaderText=" " HeaderStyle-Width="50px" meta:resourceKey="gecTaxes">
                <ItemStyle Width="50px"></ItemStyle>
            </telerik:GridEditCommandColumn> 
            <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                 <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridButtonColumn>	
		</Columns>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="NameCurrentLanguage" SortOrder="Ascending" />
        </SortExpressions>
        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <asp:ValidationSummary ID="vsTaxesSummary" ValidationGroup="Tax"
                    HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                    EnableClientScript="true" runat="server" CssClass="errorInForm" />
                <table runat="server" id="tblData" class="module">
                    <tr>
                        <td class="paramLabelCell" style='padding-top: 10px'>
                            <asp:Label runat="server" ID="lblName" meta:resourceKey="lblName"></asp:Label>
                        </td>
                        <td class="paramInputCell" style='padding-top: 10px'>
                            <telerik:RadTextBox Width="220px" runat="server" ID="txtName" MaxLength="50"
                                Text='<%# Eval("NameCurrentLanguage") %>'>
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ValidationGroup="Tax" ID="rfvName"
                                Text="*" ControlToValidate="txtName" runat="server" Display="Static"
                                CssClass="error" meta:resourceKey="ValName"></asp:RequiredFieldValidator>
                        </td>
                    </tr> 
                    <tr>
                        <td class="paramLabelCell" style='padding-top: 10px'>
                            <asp:Label runat="server" ID="lblPercentage" meta:resourceKey="lblPercentage"></asp:Label>
                        </td>
                        <td class="paramInputCell" style='padding-top: 10px'>
                            <telerik:RadTextBox Width="100px" runat="server" ID="txtPercentage" MaxLength="20"
                                Text='<%# Eval("Percentage") %>'>
                            </telerik:RadTextBox>%
                            <asp:RequiredFieldValidator ID="rfvPercentage" ValidationGroup="Tax" runat="server"
                                ControlToValidate="txtPercentage" Display="Static" meta:resourcekey="ValPercentage"
                                Text="*" CssClass="error" />
                            <asp:CustomValidator runat="server" ID="cvPercentage" ValidationGroup="Tax" OnServerValidate="ValidatePercentage"
                                ControlToValidate="txtPercentage" CssClass="error" Text="*" meta:resourcekey="ValPercentageIncorrectDecimal"
                                ValidateEmptyText="false" />
                        </td>
                    </tr>                                   
                    <tr>
                        <td colspan="2" style='text-align: right; padding-top: 4px;'>
                            <div style="position: relative">
                                <!-- workaround for issue with RadButtons not being aligned vertically -->
                                <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="Tax"
                                    VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                    CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                    CausesValidation="true">
                                </telerik:RadButton>
                                <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                </telerik:RadButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
    </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<otpDS:SimpleDataSource ID="taxesLinqDS" runat="server" />