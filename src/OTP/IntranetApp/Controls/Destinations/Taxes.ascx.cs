﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Destinations
{
    public partial class Taxes : System.Web.UI.UserControl
    {
        /// <summary>
        /// List will contain all the taxes for the destination. Will be used for displaying the list to the user
        /// </summary>
        public List<Tax> taxesList
        {
            get { return ViewState["taxesList"] as List<Tax>; }
            set { ViewState["taxesList"] = value; }
        }

        /// <summary>
        /// List will contain taxes, that were added by the user. Taxes from this list should be added to the DB
        /// </summary>
        public List<Tax> newTaxesList
        {
            get { return ViewState["newTaxesList"] as List<Tax>; }
            set { ViewState["newTaxesList"] = value; }
        }

        /// <summary>
        /// List will contain taxes, that were modified by the user and did not exist in the DB before - we don't need to update taxes, that do not exist in the DB
        /// </summary>
        public List<Tax> modifiedTaxesList
        {
            get { return ViewState["modifiedTaxesList"] as List<Tax>; }
            set { ViewState["modifiedTaxesList"] = value; }
        }

        /// <summary>
        /// List will containt IDs of taxes that were deleted by the user. Taxes from this list should be deleted from the DB
        /// </summary>
        public List<int> removedTaxesList
        {
            get { return ViewState["removedTaxesList"] as List<int>; }
            set { ViewState["removedTaxesList"] = value; }
        }

        #region Methods

        private void BindGrid()
        {
            taxesLinqDS.DataResolver = () =>
            {
                IQueryable<Tax> result = taxesList.AsQueryable();
                rgTaxes.ApplyFilter<Tax>(ref result, "Name", (q, f) => q.Where(p => p.NameCurrentLanguage.ToLower().Contains(f.ToLower())));

                return result.ToList();
            };
        }

        protected void ValidatePercentage(object source, ServerValidateEventArgs args)
        {
            decimal maxPercentage = 100.00M;
            string percentString = args.Value;
            decimal priceDecimal = 0;
            args.IsValid = decimal.TryParse(percentString, out priceDecimal);
            if (args.IsValid && (priceDecimal > maxPercentage || priceDecimal < 0))
            {
                args.IsValid = false;
                ((CustomValidator)source).ErrorMessage = GetLocalResourceObject("ValPercentageOverflow.ErrorMessage").ToString();
            }
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgTaxes_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int taxId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["TaxId"].ToString());

                taxesList.RemoveAll(t => t.TaxId == taxId); // remove deleted tax from the list of taxes
                modifiedTaxesList.RemoveAll(t => t.TaxId == taxId); // remove deleted tax from the list of modified taxes
                if ( newTaxesList.RemoveAll(t => t.TaxId == taxId) == 0 ) // remove deleted tax from the list of new taxes. if number of deleted taxes equals 0, it means, that tax existed in the db and we need to delete it
                    removedTaxesList.Add(taxId); // add id of tax to the list of removed taxes

                rgTaxes.MasterTableView.Rebind();
            }
        }

        protected void rgTaxes_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Tax obj = (Tax)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                item["Percentage"].Text = obj.Percentage + "%";

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgTaxes.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Tax"), obj.Name.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Tax"), obj.Name.ToString()));
            }
        }

        protected void rgTaxes_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgTaxes.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgTaxes.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgTaxes.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgTaxes.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgTaxes.MasterTableView.Rebind();
            }
        }


        protected void rgTaxes_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    Tax tax = new Tax();

                    ReadGridItemData(args.Item, tax);
                    tax.TaxId = taxesList.Count > 0 ? taxesList.Max(t => t.TaxId) + 1 : 1;

                    taxesList.Add(tax);
                    newTaxesList.Add(tax);

                    rgTaxes.MasterTableView.Rebind();
                }
            }
        }

        protected void rgTaxes_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("TaxId"));
                    Tax tax = taxesList.Where(t => t.TaxId == id).SingleOrDefault();

                    ReadGridItemData(editForm, tax);

                    if ( newTaxesList.Where(t => t.TaxId == id).Count() == 0 ) // if tax is not in the list of newly added
                        modifiedTaxesList.Add(tax); // make sure to update its data

                    rgTaxes.MasterTableView.Rebind();
                }
            }
        }

        private void ReadGridItemData(GridItem item, Tax tax)
        {
            tax.SetNameValue((item.FindControl("txtName") as RadTextBox).Text);
            tax.Percentage = decimal.Parse((item.FindControl("txtPercentage") as RadTextBox).Text);
            tax.PersistI18nValues();
        }

        #endregion Event Handlers
    }
}