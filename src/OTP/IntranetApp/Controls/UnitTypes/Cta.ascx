﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Cta.ascx.cs" Inherits="IntranetApp.Controls.UnitTypes.Cta" %>


<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgUnitTypes">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgUnitTypes" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<div runat="server" id="scriptBlock">
    <script type="text/javascript">
        function onCTADateSelected(sender, args) {
            var dateField = $('#<%=hfSelectedDateFrom.ClientID%>');
            dateField.val(sender.get_selectedDate());
        }
        function onCTAUntilDatePopupOpening(sender, args) {
            var dateField = $('#<%=hfSelectedDateFrom.ClientID%>');
            if (dateField.val() != '') {
                var dateFrom = new Date(dateField.val());
                sender.set_focusedDate(dateFrom);
            }
            else {
                sender.set_focusedDate(new Date());
            }
        }
    </script>
</div>

<telerik:RadGrid
    Width="550px"
    AutoGenerateColumns="False" 
    ID="rgMlos" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="mlosLinqDS"
	ClientSettings-EnablePostBackOnRowClick="true" 
    OnItemCommand="rgMlos_ItemCommand" 
    OnDataBound="rgMlos_DataBound"
    OnItemDataBound="rgMlos_ItemDataBound"
    OnUpdateCommand="rgMlos_UpdateCommand"
    OnInsertCommand="rgMlos_InsertCommand" >
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Auto" DataKeyNames="UnitTypeCTAID" DataSourceID="mlosLinqDS" CommandItemDisplay="Bottom" >
		<Columns>
            <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" HeaderText=" " HeaderStyle-Width="50px" >
                    <ItemStyle Width="50px"></ItemStyle>
                </telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn DataField="DateFrom" FilterControlWidth="150px" UniqueName="DateFrom" HeaderText="Date From" SortExpression="DateFrom" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DateUntil" FilterControlWidth="150px" UniqueName="DateUntil" HeaderText="Date Until" SortExpression="DateUntil" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
			<%--<telerik:GridBoundColumn DataField="MLOS" FilterControlWidth="150px" UniqueName="MLOS" HeaderText="Min. lenght of stay" SortExpression="MLOS" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>--%>
            <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridButtonColumn>		
		</Columns>
        <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="CTA"
                        HeaderText=" " DisplayMode="BulletList"
                        EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDateFrom" meta:resourceKey="lblDateFrom"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadDatePicker ID="rcDateFrom" runat="server" SelectedDate='<%# (Eval("DateFrom") != null && Eval("DateFrom") is DateTime) ? Convert.ToDateTime(Eval("DateFrom")) : (DateTime?)null %>'>
                                    <DateInput runat="server"
                                        DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    <ClientEvents OnDateSelected="onCTADateSelected" />
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ValidationGroup="CTA" ID="rfvNameProductParam"
                                    Text="*" ControlToValidate="rcDateFrom" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDateFrom"></asp:RequiredFieldValidator>
                                <asp:CustomValidator 
                                    ID="cvDateFrom"
                                    ValidationGroup="CTA"
                                    Display="Dynamic"
                                    runat="server"
                                    OnServerValidate="cvInvalidPeriod_ServerValidate"
                                    meta:resourcekey="ValCtaPeriod"
                                    ControlToValidate="rcDateFrom"
                                    Text="*"
                                    CssClass="error" />
                            </td>
                        </tr> 
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDateUntil" meta:resourceKey="lblDateUntil"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadDatePicker ID="rcDateUntil" runat="server"  SelectedDate='<%# (Eval("DateUntil") != null && Eval("DateUntil") is DateTime) ? Convert.ToDateTime(Eval("DateUntil")) : (DateTime?)null %>'>
                                    <DateInput runat="server"
                                        DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    <ClientEvents OnPopupOpening="onCTAUntilDatePopupOpening" />    
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ValidationGroup="CTA" ID="RequiredFieldValidator1"
                                    Text="*" ControlToValidate="rcDateUntil" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDateFrom"></asp:RequiredFieldValidator>
                                <br />
                                <asp:CompareValidator ID="cmpEndDate" runat="server"
                                    meta:resourceKey="ValDates" CssClass="error" ValidationGroup="CTA"
                                    ControlToCompare="rcDateFrom" ControlToValidate="rcDateUntil"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                            </td>
                        </tr>                        
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblCTADays" meta:resourceKey="lblCTADays"></asp:Label>
                            </td>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:CheckBox ID="cbMonday" runat="server" meta:resourcekey="cbMonday" OnInit="cbDay_Init" /><br />
                                <asp:CheckBox ID="cbTuesday" runat="server" meta:resourcekey="cbTuesday" OnInit="cbDay_Init" /><br />
                                <asp:CheckBox ID="cbWednesday" runat="server" meta:resourcekey="cbWednesday" OnInit="cbDay_Init" /><br />
                                <asp:CheckBox ID="cbThursday" runat="server" meta:resourcekey="cbThursday" OnInit="cbDay_Init" /><br />
                                <asp:CheckBox ID="cbFriday" runat="server" meta:resourcekey="cbFriday" OnInit="cbDay_Init" /><br />
                                <asp:CheckBox ID="cbSaturday" runat="server" meta:resourcekey="cbSaturday" OnInit="cbDay_Init" /><br />
                                <asp:CheckBox ID="cbSunday" runat="server" meta:resourcekey="cbSunday" OnInit="cbDay_Init" />
                            </td>
                        </tr>                       
                        <tr>
                            <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                <div style="position: relative; width: 415px;">
                                    <!-- workaround for issue with RadButtons not being aligned vertically -->
                                    <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="CTA"
                                        VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        CausesValidation="true">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                    </telerik:RadButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>
<asp:HiddenField ID="hfSelectedDateFrom" runat="server" />

<otpDS:SimpleDataSource ID="mlosLinqDS" runat="server" />