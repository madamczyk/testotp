﻿using BusinessLogic.Managers;
using System;
using System.Linq;
using Telerik.Web.UI;
using Common.CultureInfo;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using DataAccess;
using Common.Extensions;

namespace IntranetApp.Controls.UnitTypes
{
    public partial class Mlos : System.Web.UI.UserControl
    {

        #region Fields

        public int? UnitTypeId
        {
            get { return ViewState["UnitTypeId"] as int?; }
            set { ViewState["UnitTypeId"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemId"] as int?; }
            set { ViewState["EditedItemId"] = value; }
        }

        /// <summary>
        /// List will contain all the Discounts for the destination. Will be used for displaying the list to the user
        /// </summary>
        public List<UnitTypeMLO> ElementsList
        {
            get { return ViewState["UnitTypeMLOElementsList"] as List<UnitTypeMLO>; }
            set { ViewState["UnitTypeMLOElementsList"] = value; }
        }

        /// <summary>
        /// List will contain Discounts, that were added by the user. Discounts from this list should be added to the DB
        /// </summary>
        public List<UnitTypeMLO> NewElements
        {
            get { return ViewState["UnitTypeMLONewElements"] as List<UnitTypeMLO>; }
            set { ViewState["UnitTypeMLONewElements"] = value; }
        }

        /// <summary>
        /// List will contain Discounts, that were modified by the user and did not exist in the DB before - we don't need to update taxes, that do not exist in the DB
        /// </summary>
        public List<UnitTypeMLO> ModifiedElementsList
        {
            get { return ViewState["UnitTypeMLOModifiedElementsList"] as List<UnitTypeMLO>; }
            set { ViewState["UnitTypeMLOModifiedElementsList"] = value; }
        }

        /// <summary>
        /// List will containt IDs of taxes that were deleted by the user. Discounts from this list should be deleted from the DB
        /// </summary>
        public List<int> RemovedElementsList
        {
            get { return ViewState["UnitTypeMLORemovedElementsList"] as List<int>; }
            set { ViewState["UnitTypeMLORemovedElementsList"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.mlosLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.UnitTypeMLO> result = ElementsList.AsQueryable();
                return result.ToList();
            };            
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgMlos_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int id = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitTypeMLOSID"].ToString());
                ElementsList.RemoveAll(t => t.UnitTypeMLOSID == id); // remove deleted tax from the list of taxes
                ModifiedElementsList.RemoveAll(t => t.UnitTypeMLOSID == id); // remove deleted tax from the list of modified taxes
                if (NewElements.RemoveAll(t => t.UnitTypeMLOSID == id) == 0) // remove deleted tax from the list of new taxes. if number of deleted taxes equals 0, it means, that tax existed in the db and we need to delete it
                    RemovedElementsList.Add(id); // add id of tax to the list of removed taxes

            }
            else if (e.CommandName == "Edit")
            {
                int editedId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitTypeMLOSID"].ToString());
                EditedItemId = editedId;
            }
            else if (e.CommandName == "Cancel")
            {
                EditedItemId = null;
            }
        }

        protected void rgMlos_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                UnitTypeMLO obj = (UnitTypeMLO)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                string title = String.Format("{0} - {1}", obj.DateFrom.ToLocalizedDateString(), obj.DateUntil.ToLocalizedDateString());

                item["DateFrom"].Text = obj.DateFrom.ToLocalizedDateString();
                item["DateUntil"].Text = obj.DateUntil.ToLocalizedDateString();

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgMlos.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_MLOS"), title), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_MLOS"), title));
            }
        }

        protected void rgMlos_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgMlos.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgMlos.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgMlos.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgMlos.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgMlos.MasterTableView.Rebind();
            }
        }

        protected void rgMlos_InsertCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (e.Item is GridEditFormInsertItem)
                {
                    DataAccess.UnitTypeMLO newUnit = new DataAccess.UnitTypeMLO();
                    ReadGridItemData(e.Item, newUnit);
                    newUnit.UnitTypeMLOSID = ElementsList.Count > 0 ? ElementsList.Max(t => t.UnitTypeMLOSID) + 1 : 1;
                    ElementsList.Add(newUnit);
                    NewElements.Add(newUnit);
                    this.rgMlos.MasterTableView.Rebind();
                }
            }
        }

        protected void rgMlos_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    DataAccess.UnitTypeMLO unit = null;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("UnitTypeMLOSID"));
                    unit = ElementsList.Where(t => t.UnitTypeMLOSID == id).SingleOrDefault();

                    ReadGridItemData(editForm, unit);

                    if (NewElements.Where(t => t.UnitTypeMLOSID == id).Count() == 0) // if tax is not in the list of newly added
                    {
                        ModifiedElementsList.Add(unit); // make sure to update its data
                    }
                    else
                    {
                        NewElements.RemoveAll(t => t.UnitTypeMLOSID == id);
                        NewElements.Add(unit);
                    }
                    this.rgMlos.MasterTableView.Rebind();
                }
                int reportId = int.Parse(args.Item.OwnerTableView.DataKeyValues[args.Item.ItemIndex]["UnitTypeMLOSID"].ToString());
                EditedItemId = reportId;
            }
        }

        private void ReadGridItemData(GridItem item, DataAccess.UnitTypeMLO unitDiscount)
        {
            unitDiscount.DateFrom = (item.FindControl("rcDateFrom") as RadDatePicker).SelectedDate.Value;
            unitDiscount.DateUntil = (item.FindControl("rcDateUntil") as RadDatePicker).SelectedDate.Value;
            unitDiscount.MLOS = int.Parse((item.FindControl("txtMlos") as RadTextBox).Text.Replace(".", ","));
        }

        protected void cvInvalidPeriod_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool valid = true;
            DateTime DateFrom = DateTime.Parse(args.Value);
            DateTime? DateUntil = ((RadDatePicker)((CustomValidator)source).FindControl("rcDateUntil")).SelectedDate;
            IQueryable<UnitTypeMLO> result = ElementsList == null ? new List<UnitTypeMLO>().AsQueryable() : ElementsList.AsQueryable();

            if (!EditedItemId.HasValue)
            {
                if (result.Any(o => o.DateFrom <= DateFrom && o.DateUntil >= DateFrom)) // does DateFrom fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.DateFrom <= DateUntil && o.DateUntil >= DateUntil)) // does DateUntil fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.DateFrom >= DateFrom && o.DateUntil <= DateUntil))// does DateFrom - DateUntil contain any other period?
                    valid = false;
            }
            else
            {
                if (result.Any(o => o.UnitTypeMLOSID != EditedItemId.Value && o.DateFrom <= DateFrom && o.DateUntil >= DateFrom)) // does DateFrom fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.UnitTypeMLOSID != EditedItemId.Value && o.DateFrom <= DateUntil && o.DateUntil >= DateUntil)) // does DateUntil fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.UnitTypeMLOSID != EditedItemId.Value && o.DateFrom >= DateFrom && o.DateUntil <= DateUntil))// does DateFrom - DateUntil contain any other period?
                    valid = false;
            }

            args.IsValid = valid;
        }

        #endregion Event Handlers

    }
}