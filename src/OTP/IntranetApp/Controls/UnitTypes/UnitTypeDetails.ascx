﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UnitTypeDetails.ascx.cs" Inherits="IntranetApp.Controls.UnitTypes.UnitTypeDetails" %>
<%@ Register TagPrefix="uc" TagName="Mlos" Src="~/Controls/UnitTypes/Mlos.ascx" %>
<%@ Register TagPrefix="uc" TagName="Cta" Src="~/Controls/UnitTypes/Cta.ascx" %>
<%@ Register TagPrefix="uc" TagName="Discount" Src="~/Controls/UnitTypes/Discount.ascx" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="pnlData">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlData" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
        <telerik:RadWindow ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" Text="<%$ Resources: Controls, Dialog_Discard_Text %>" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="btnDiscard_Yes"></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClick="btnDiscard_No"></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
        <telerik:RadWindow ID="rwError" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblError" Font-Bold="true" runat="server" />
                        <br />
                        <br />
                        <asp:Button ID="wndBtnError_Ok" runat="server" Text="<%$ Resources: Controls, Dialog_Ok %>" OnClick="btnError_Ok" />
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<asp:Panel runat="server" ID="pnlData">
    <asp:ValidationSummary ID="vsSummary" ValidationGroup="UnitType" HeaderText=" "
        DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
    <h1 class="FormHeader">
        <asp:Label runat="server" ID="lblHeader" meta:resourcekey="lblHeaderAdd"></asp:Label>
    </h1>
    <br />
    <table>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblTitle" meta:resourcekey="lblTitle" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" TabIndex="1" ID="txtTitle" Enabled="true" Width="250px" />
                <asp:RequiredFieldValidator
                    ID="rfvName"
                    ValidationGroup="UnitType"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="txtTitle"
                    meta:resourcekey="ValTitle"
                    Text="*"
                    CssClass="error" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblCode" meta:resourcekey="lblCode" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" TabIndex="1" ID="txtCode" Enabled="true" Width="250px" />
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidator1"
                    ValidationGroup="UnitType"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="txtCode"
                    meta:resourcekey="ValCode"
                    Text="*"
                    CssClass="error" />
                <asp:CustomValidator
                    ID="cvUnitCode" 
                    ValidationGroup="UnitType" 
                    runat="server" 
                    Display="Dynamic"                    
                    meta:resourcekey="ValUnitTypeCode" 
                    ControlToValidate="txtCode"
                    ValidateEmptyText="false"
                    EnableClientScript="true"
                    Enabled="true"
                    Text="*" 
                    OnServerValidate="cvUnitCode_ServerValidate"
                    CssClass="error" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblDescription" meta:resourcekey="lblDescription" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" TabIndex="1" ID="txtDescription" Enabled="true" Width="250px" />
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidator2"
                    ValidationGroup="UnitType"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="txtDescription"
                    meta:resourcekey="ValDescription"
                    Text="*"
                    CssClass="error" />
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <tr style="height: 20px;">
                <td></td>
            </tr>
            <td>
              <telerik:RadTabStrip ID="rtsPropertyDetails" runat="server"
                        SelectedIndex="0" MultiPageID="rmpTabContent">
                        <Tabs>
                            <telerik:RadTab Text="7 day discount" />
                            <telerik:RadTab Text="Minimum length of stay" />
                            <telerik:RadTab Text="Close To Arrival" />
                        </Tabs>
                    </telerik:RadTabStrip>

                    <telerik:RadMultiPage ID="rmpTabContent" runat="server" SelectedIndex="0">

                        <telerik:RadPageView ID="rpvContacts" runat="server" CssClass="radTabSmall">
                            <uc:Discount ID="ctrlDiscount" runat="server" />
                        </telerik:RadPageView>

                        <telerik:RadPageView ID="RadPageView1" runat="server" CssClass="radTabSmall">
                            <uc:Mlos ID="ctrlMlos" runat="server" />
                        </telerik:RadPageView>

                        <telerik:RadPageView ID="RadPageView2" runat="server" CssClass="radTabSmall">
                            <uc:Cta ID="ctrlCta" runat="server" />
                        </telerik:RadPageView>

                    </telerik:RadMultiPage>


            </td>

        </tr>
      
    </table>
</asp:Panel>

<table>
    <tr style="height: 20px;">
        <td></td>
    </tr>
    <tr>
        <td width="645px" align="right" style="padding-right: 3px">
            <telerik:RadButton runat="server" ID="btnSave" TabIndex="4" Text="<%$ Resources: Controls, Button_Save %>" CausesValidation="true" ValidationGroup="UnitType" OnClick="btnSave_Click" />
            <telerik:RadButton runat="server" ID="btnCancel" TabIndex="5" Text="<%$ Resources: Controls, Button_Cancel %>" CausesValidation="false" OnClick="btnCancel_Click" />
        </td>
    </tr>
</table>
