﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Discount.ascx.cs" Inherits="IntranetApp.Controls.UnitTypes.Discount" %>


<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgUnitTypes">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgUnitTypes" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<div runat="server" id="scriptBlock">
    <script type="text/javascript">
        function onDiscountDateSelected(sender, args) {
            var dateField = $('#<%=hfSelectedDateFrom.ClientID%>');
            dateField.val(sender.get_selectedDate());
        }
        function onDiscountUntilDatePopupOpening(sender, args) {
            var dateField = $('#<%=hfSelectedDateFrom.ClientID%>');
            if (dateField.val() != '') {
                var dateFrom = new Date(dateField.val());
                sender.set_focusedDate(dateFrom);
            }
            else {
                sender.set_focusedDate(new Date());
            }
        }
    </script>
</div>

<telerik:RadGrid 
    Width="550px"
    AutoGenerateColumns="False" 
    ID="rgDiscounts" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="discountsLinqDS"
	ClientSettings-EnablePostBackOnRowClick="true" 
    OnItemCommand="rgDiscounts_ItemCommand" 
    OnDataBound="rgDiscounts_DataBound"
    OnItemDataBound="rgDiscounts_ItemDataBound"
    OnUpdateCommand="rgDiscounts_UpdateCommand"
    OnInsertCommand="rgDiscounts_InsertCommand" >
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView  TableLayout="Auto" DataKeyNames="UnitType7dayID" DataSourceID="discountsLinqDS" CommandItemDisplay="Bottom" >
		<Columns>
            <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" HeaderText=" " HeaderStyle-Width="50px" >
                    <ItemStyle Width="50px"></ItemStyle>
                </telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn DataField="DateFrom" FilterControlWidth="150px" UniqueName="DateFrom" HeaderText="Date From" SortExpression="DateFrom" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DateUntil" FilterControlWidth="150px" UniqueName="DateUntil" HeaderText="Date Until" SortExpression="DateUntil" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="Discount" FilterControlWidth="150px" UniqueName="Discount" HeaderText="Discount" SortExpression="Discount" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridButtonColumn>		
		</Columns>
        <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="Discount"
                        HeaderText=" " DisplayMode="BulletList"
                        EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDateFrom" meta:resourceKey="lblDateFrom"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadDatePicker ID="rcDateFrom" runat="server" SelectedDate='<%# (Eval("DateFrom") != null && Eval("DateFrom") is DateTime) ? Convert.ToDateTime(Eval("DateFrom")) : (DateTime?)null %>'>
                                    <DateInput runat="server"
                                        DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    <ClientEvents OnDateSelected="onDiscountDateSelected" />
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ValidationGroup="Discount" ID="rfvNameProductParam"
                                    Text="*" ControlToValidate="rcDateFrom" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDateFrom"></asp:RequiredFieldValidator>
                                <asp:CustomValidator 
                                    ID="cvDateFrom"
                                    ValidationGroup="Discount"
                                    Display="Dynamic"
                                    runat="server"
                                    OnServerValidate="cvInvalidPeriod_ServerValidate"
                                    meta:resourcekey="ValDiscountPeriod"
                                    ControlToValidate="rcDateFrom"
                                    Text="*"
                                    CssClass="error" />
                            </td>
                        </tr> 
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDateUntil" meta:resourceKey="lblDateUntil"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadDatePicker ID="rcDateUntil" runat="server" SelectedDate='<%# (Eval("DateUntil") != null && Eval("DateUntil") is DateTime) ? Convert.ToDateTime(Eval("DateUntil")) : (DateTime?)null %>'>
                                    <DateInput runat="server"
                                        DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    <ClientEvents OnPopupOpening="onDiscountUntilDatePopupOpening" />
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ValidationGroup="Discount" ID="RequiredFieldValidator1"
                                    Text="*" ControlToValidate="rcDateUntil" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDateFrom"></asp:RequiredFieldValidator>
                                <br />
                                <asp:CompareValidator ID="cmpEndDate" runat="server"
                                    meta:resourceKey="ValDates" CssClass="error" ValidationGroup="Discount"
                                    ControlToCompare="rcDateFrom" ControlToValidate="rcDateUntil"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                        </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDiscount" meta:resourceKey="lblDiscount"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="120px" runat="server" ID="txtDiscount" MaxLength="20" TextMode="SingleLine" Text='<%# Eval("Discount") %>'>
                                </telerik:RadTextBox>%
                                <asp:RequiredFieldValidator ValidationGroup="Discount" ID="RequiredFieldValidator2"
                                    Text="*" ControlToValidate="txtDiscount" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDiscount"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator3" 
                                    ValidationGroup="Discount"
                                    runat="server" 
                                    Display="Dynamic" 
                                    ControlToValidate="txtDiscount"                     
                                    meta:resourcekey="ValDiscountFormat" 
                                    Text="*" 
                                    CssClass="error" OnPreRender="RequiredFieldValidator_PreRender" />
                            </td>
                        </tr>                                   
                        <tr>
                            <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                <div style="position: relative; width: 415px;">
                                    <!-- workaround for issue with RadButtons not being aligned vertically -->
                                    <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="Discount"
                                        VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        CausesValidation="true">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CausesValidation="false">
                                    </telerik:RadButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>
<asp:HiddenField ID="hfSelectedDateFrom" runat="server" />

<otpDS:SimpleDataSource ID="discountsLinqDS" runat="server" />