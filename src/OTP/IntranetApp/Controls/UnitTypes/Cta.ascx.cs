﻿using BusinessLogic.Managers;
using System;
using System.Linq;
using Telerik.Web.UI;
using Common.CultureInfo;
using System.Collections.Generic;
using DataAccess;
using Common.Extensions;
using System.Web.UI.WebControls;

namespace IntranetApp.Controls.UnitTypes
{
    public partial class Cta : System.Web.UI.UserControl
    {

        #region Fields

        public int? UnitTypeId
        {
            get { return ViewState["UnitTypeId"] as int?; }
            set { ViewState["UnitTypeId"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["CtaEditedItemId"] as int?; }
            set { ViewState["CtaEditedItemId"] = value; }
        }

        /// <summary>
        /// List will contain all the Discounts for the destination. Will be used for displaying the list to the user
        /// </summary>
        public List<UnitTypeCTA> ElementsList
        {
            get { return ViewState["UnitTypeCTAElementsList"] as List<UnitTypeCTA>; }
            set { ViewState["UnitTypeCTAElementsList"] = value; }
        }

        /// <summary>
        /// List will contain UnitTypeCTAs, that were added by the user. UnitTypeCTAs from this list should be added to the DB
        /// </summary>
        public List<UnitTypeCTA> NewElements
        {
            get { return ViewState["UnitTypeCTANewElements"] as List<UnitTypeCTA>; }
            set { ViewState["UnitTypeCTANewElements"] = value; }
        }

        /// <summary>
        /// List will contain UnitTypeCTAs, that were modified by the user and did not exist in the DB before - we don't need to update taxes, that do not exist in the DB
        /// </summary>
        public List<UnitTypeCTA> ModifiedElementsList
        {
            get { return ViewState["UnitTypeCTAModifiedElementsList"] as List<UnitTypeCTA>; }
            set { ViewState["UnitTypeCTAModifiedElementsList"] = value; }
        }

        /// <summary>
        /// List will containt IDs of taxes that were deleted by the user. UnitTypeCTAs from this list should be deleted from the DB
        /// </summary>
        public List<int> RemovedElementsList
        {
            get { return ViewState["UnitTypeCTARemovedElementsList"] as List<int>; }
            set { ViewState["UnitTypeCTARemovedElementsList"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.mlosLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.UnitTypeCTA> result = ElementsList.AsQueryable(); 
                return result.ToList();
            };            
        }

        #endregion
        
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgMlos_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int id = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitTypeCTAID"].ToString());
                ElementsList.RemoveAll(t => t.UnitTypeCTAID == id); // remove deleted tax from the list of taxes
                ModifiedElementsList.RemoveAll(t => t.UnitTypeCTAID == id); // remove deleted tax from the list of modified taxes
                if (NewElements.RemoveAll(t => t.UnitTypeCTAID == id) == 0) // remove deleted tax from the list of new taxes. if number of deleted taxes equals 0, it means, that tax existed in the db and we need to delete it
                    RemovedElementsList.Add(id); // add id of tax to the list of removed taxes
            }            
            else if (e.CommandName == "Edit")
            {
                int editedId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitTypeCTAID"].ToString());
                EditedItemId = editedId;
            }
            else if (e.CommandName == "Cancel")
            {
                EditedItemId = null;
            }
        }

        protected void rgMlos_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                UnitTypeCTA obj = (UnitTypeCTA)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                string title = String.Format("{0} - {1}", obj.DateFrom.ToLocalizedDateString(), obj.DateUntil.ToLocalizedDateString());

                item["DateFrom"].Text = obj.DateFrom.ToLocalizedDateString();
                item["DateUntil"].Text = obj.DateUntil.ToLocalizedDateString();

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgMlos.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_CTA"), title), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_CTA"), title));
            }
        }

        protected void rgMlos_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgMlos.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgMlos.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgMlos.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgMlos.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgMlos.MasterTableView.Rebind();
            }
        }

        protected void rgMlos_InsertCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (e.Item is GridEditFormInsertItem)
                {
                    DataAccess.UnitTypeCTA newUnit = new DataAccess.UnitTypeCTA();
                    ReadGridItemData(e.Item, newUnit);
                    newUnit.UnitTypeCTAID = ElementsList.Count > 0 ? ElementsList.Max(t => t.UnitTypeCTAID) + 1 : 1;
                    ElementsList.Add(newUnit);
                    NewElements.Add(newUnit);
                    this.rgMlos.MasterTableView.Rebind();
                }
            }
        }

        protected void rgMlos_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    DataAccess.UnitTypeCTA unit = null;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("UnitTypeCTAID"));
                    unit = ElementsList.Where(t => t.UnitTypeCTAID == id).SingleOrDefault();

                    ReadGridItemData(editForm, unit);

                    if (NewElements.Where(t => t.UnitTypeCTAID == id).Count() == 0) // if tax is not in the list of newly added
                    {
                        ModifiedElementsList.Add(unit); // make sure to update its data
                    }
                    else
                    {
                        NewElements.RemoveAll(t => t.UnitTypeCTAID == id);
                        NewElements.Add(unit);
                    }
                    this.rgMlos.MasterTableView.Rebind();
                }
                int reportId = int.Parse(args.Item.OwnerTableView.DataKeyValues[args.Item.ItemIndex]["UnitTypeCTAID"].ToString());
                EditedItemId = reportId;
            }
        }

        private void ReadGridItemData(GridItem item, DataAccess.UnitTypeCTA unitDiscount)
        {
            unitDiscount.DateFrom = (item.FindControl("rcDateFrom") as RadDatePicker).SelectedDate.Value;
            unitDiscount.DateUntil = (item.FindControl("rcDateUntil") as RadDatePicker).SelectedDate.Value;
            unitDiscount.Monday = (item.FindControl("cbMonday") as CheckBox).Checked;
            unitDiscount.Tuesday = (item.FindControl("cbTuesday") as CheckBox).Checked;
            unitDiscount.Wednesday = (item.FindControl("cbWednesday") as CheckBox).Checked;
            unitDiscount.Thursday = (item.FindControl("cbThursday") as CheckBox).Checked;
            unitDiscount.Friday = (item.FindControl("cbFriday") as CheckBox).Checked;
            unitDiscount.Saturday = (item.FindControl("cbSaturday") as CheckBox).Checked;
            unitDiscount.Sunday = (item.FindControl("cbSunday") as CheckBox).Checked;
        }

        protected void cbDay_Init(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;

            if (EditedItemId.HasValue)
            {
                var ctaItem = ElementsList.Where(c => c.UnitTypeCTAID == EditedItemId.Value).SingleOrDefault();
                var name = cb.ID.Replace("cb", "");
                switch (name)
                {
                    case "Monday" :
                        cb.Checked = ctaItem.Monday;
                        break;
                    case "Tuesday":
                        cb.Checked = ctaItem.Tuesday;
                        break;
                    case "Wednesday":
                        cb.Checked = ctaItem.Wednesday;
                        break;
                    case "Thursday":
                        cb.Checked = ctaItem.Thursday;
                        break;
                    case "Friday":
                        cb.Checked = ctaItem.Friday;
                        break;
                    case "Saturday":
                        cb.Checked = ctaItem.Saturday;
                        break;
                    case "Sunday":
                        cb.Checked = ctaItem.Sunday;
                        break;
                }
            }
            else
            {
                cb.Checked = true;
            }
        }
        
        protected void cvInvalidPeriod_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool valid = true;
            DateTime DateFrom = DateTime.Parse(args.Value);
            DateTime? DateUntil = ((RadDatePicker)((CustomValidator)source).FindControl("rcDateUntil")).SelectedDate;
            IQueryable<UnitTypeCTA> result = ElementsList == null ? new List<UnitTypeCTA>().AsQueryable() : ElementsList.AsQueryable();

            if (!EditedItemId.HasValue)
            {
                if (result.Any(o => o.DateFrom <= DateFrom && o.DateUntil >= DateFrom)) // does DateFrom fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.DateFrom <= DateUntil && o.DateUntil >= DateUntil)) // does DateUntil fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.DateFrom >= DateFrom && o.DateUntil <= DateUntil))// does DateFrom - DateUntil contain any other period?
                    valid = false;
            }
            else
            {
                if (result.Any(o => o.UnitTypeCTAID != EditedItemId.Value && o.DateFrom <= DateFrom && o.DateUntil >= DateFrom)) // does DateFrom fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.UnitTypeCTAID != EditedItemId.Value && o.DateFrom <= DateUntil && o.DateUntil >= DateUntil)) // does DateUntil fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.UnitTypeCTAID != EditedItemId.Value && o.DateFrom >= DateFrom && o.DateUntil <= DateUntil))// does DateFrom - DateUntil contain any other period?
                    valid = false;
            }

            args.IsValid = valid;
        }

        #endregion Event Handlers
    }
}