﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Web.UI;
using Telerik.Web.UI;

namespace IntranetApp.Controls.UnitTypes
{
    public partial class UnitTypeDetails : System.Web.UI.UserControl
    {
        #region Fields

        private int? PropertyId
        {
            get { return ViewState["PropertyId"] as int?; }
            set { ViewState["PropertyId"] = value; }
        }

        public int? UnitTypeId
        {
            get { return ViewState["UnitTypeId"] as int?; }
            set { ViewState["UnitTypeId"] = value; }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["Mode"]; }
            set { ViewState["Mode"] = (int)value; }
        }

        #endregion Fields

        #region Methods

        private void ParseQueryString()
        {
            int id = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out id))
            {
                if (Mode == FormMode.Edit)
                {
                    UnitTypeId = id;
                    PropertyId = RequestManager.Services.UnitTypesService.GetUnitTypeById(id).Property.PropertyID;
                }
                else
                {
                    PropertyId = id;                
                }
            }
        }

        private void LoadData()
        {
            if (Mode == FormMode.Edit)
            {
                DataAccess.UnitType ut = RequestManager.Services.UnitTypesService.GetUnitTypeById(UnitTypeId.Value);

                //textboxes
                txtTitle.Text = ut.UnitTypeTitleCurrentLanguage;
                txtCode.Text = ut.UnitTypeCode;
                txtDescription.Text = ut.UnitTypeDescCurrentLanguage;
            }
        }

        private bool SaveData()
        {
            DataAccess.UnitType ut = null;

            if (Mode == FormMode.Add)
                ut = new UnitType();
            else
                ut = RequestManager.Services.UnitTypesService.GetUnitTypeById(UnitTypeId.Value);

            if (ut == null)
                return false;

            ut.SetTitleValue(txtTitle.Text);
            ut.SetDescValue(txtDescription.Text);
            ut.UnitTypeCode = txtCode.Text;
            ut.Property = RequestManager.Services.PropertiesService.GetPropertyById(PropertyId.Value);

            if (Mode == FormMode.Add)
            {
                RequestManager.Services.UnitTypesService.AddUnitType(ut);
                RequestManager.Services.SaveChanges();
            }

            #region 7 days discount - save to db

            foreach (var id in ctrlDiscount.RemovedElementsList)
                RequestManager.Services.UnitTypesService.DeleteUnitType7DaysDiscount(id);

            List<UnitType7dayDiscounts>.Enumerator ListEnumerator;
            ListEnumerator = ctrlDiscount.NewElements.GetEnumerator();
            while (ListEnumerator.MoveNext()) // add new UnitType7dayDiscounts, but update their Unit Type first
            {
                ListEnumerator.Current.UnitType = ut;
                RequestManager.Services.UnitTypesService.AddUnitType7DayDiscount(ListEnumerator.Current);
            }
            UnitType7dayDiscounts discount;
            foreach (var t in ctrlDiscount.ModifiedElementsList)
            {
                discount = RequestManager.Services.UnitTypesService.Get7DayUnitTypeDiscountById(t.UnitType7dayID);

                discount.DateFrom = t.DateFrom;
                discount.DateUntil = t.DateUntil;
                discount.Discount = t.Discount;
                discount.UnitType = ut;
            }
            #endregion

            #region MLOS - save to db

            foreach (var id in ctrlMlos.RemovedElementsList)
                RequestManager.Services.UnitTypesService.DeleteUnitTypeMlos(id);

            List<UnitTypeMLO>.Enumerator ListMLOSEnumerator;
            ListMLOSEnumerator = ctrlMlos.NewElements.GetEnumerator();
            while (ListMLOSEnumerator.MoveNext())
            {
                ListMLOSEnumerator.Current.UnitType = ut;
                RequestManager.Services.UnitTypesService.AddUnitTypeMlos(ListMLOSEnumerator.Current);
            }
            UnitTypeMLO mlos;
            foreach (var t in ctrlMlos.ModifiedElementsList)
            {
                mlos = RequestManager.Services.UnitTypesService.GetUnitTypeMlosById(t.UnitTypeMLOSID);
                mlos.DateFrom = t.DateFrom;
                mlos.DateUntil = t.DateUntil;
                mlos.UnitType = ut;
                mlos.MLOS = t.MLOS;
            }
            #endregion

            #region CTA - save to db

            foreach (var id in ctrlCta.RemovedElementsList)
                RequestManager.Services.UnitTypesService.DeleteUnitTypeCta(id);

            List<UnitTypeCTA>.Enumerator ListCTAEnumerator;
            ListCTAEnumerator = ctrlCta.NewElements.GetEnumerator();
            while (ListCTAEnumerator.MoveNext())
            {
                ListCTAEnumerator.Current.UnitType = ut;
                RequestManager.Services.UnitTypesService.AddUnitTypeCta(ListCTAEnumerator.Current);
            }
            UnitTypeCTA cta;
            foreach (var t in ctrlCta.ModifiedElementsList)
            {
                cta = RequestManager.Services.UnitTypesService.GetUnitTypeCtaById(t.UnitTypeCTAID);
                cta.DateFrom = t.DateFrom;
                cta.DateUntil = t.DateUntil;
                cta.Monday = t.Monday;
                cta.Tuesday = t.Tuesday;
                cta.Wednesday = t.Wednesday;
                cta.Thursday = t.Thursday;
                cta.Friday = t.Friday;
                cta.Saturday = t.Saturday;
                cta.Sunday = t.Sunday;
                cta.UnitType = ut;
            }
            #endregion
            
            RequestManager.Services.SaveChanges();

            return true;
        }

        private bool CheckExists()
        {
            if (UnitTypeId.HasValue && RequestManager.Services.UnitTypesService.GetUnitTypeById(UnitTypeId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void BindControls()
        {
            ctrlDiscount.ElementsList = new List<UnitType7dayDiscounts>();
            ctrlDiscount.RemovedElementsList = new List<int>();
            ctrlDiscount.NewElements = new List<UnitType7dayDiscounts>();
            ctrlDiscount.ModifiedElementsList = new List<UnitType7dayDiscounts>();

            ctrlMlos.ElementsList = new List<UnitTypeMLO>();
            ctrlMlos.RemovedElementsList = new List<int>();
            ctrlMlos.NewElements = new List<UnitTypeMLO>();
            ctrlMlos.ModifiedElementsList = new List<UnitTypeMLO>();

            ctrlCta.ElementsList = new List<UnitTypeCTA>();
            ctrlCta.RemovedElementsList = new List<int>();
            ctrlCta.NewElements = new List<UnitTypeCTA>();
            ctrlCta.ModifiedElementsList = new List<UnitTypeCTA>();

            if (Mode == FormMode.Edit)            
            {
                #region 7 Days Discount

                UnitType7dayDiscounts discount = null;
                var discounts = RequestManager.Services.UnitTypesService.Get7DayUnitTypeDiscounts(this.UnitTypeId.Value);
                foreach (var t in discounts)
                {
                    discount = new UnitType7dayDiscounts();
                    discount.DateFrom = t.DateFrom;
                    discount.DateUntil = t.DateUntil;
                    discount.Discount = t.Discount;
                    discount.UnitType = RequestManager.Services.UnitTypesService.GetUnitTypeById(this.UnitTypeId.Value);
                    discount.UnitType7dayID = t.UnitType7dayID;

                    ctrlDiscount.ElementsList.Add(discount);                    
                }
                
                #endregion

                #region MLOS

                UnitTypeMLO mlos = null;
                var mloses = RequestManager.Services.UnitTypesService.GetUnitTypeMlos(this.UnitTypeId.Value);
                foreach (var t in mloses)
                {
                    mlos = new UnitTypeMLO();
                    mlos.DateFrom = t.DateFrom;
                    mlos.DateUntil = t.DateUntil;
                    mlos.MLOS = t.MLOS;
                    mlos.UnitType = RequestManager.Services.UnitTypesService.GetUnitTypeById(this.UnitTypeId.Value);
                    mlos.UnitTypeMLOSID = t.UnitTypeMLOSID;

                    ctrlMlos.ElementsList.Add(mlos);
                }

                #endregion

                #region CTA

                UnitTypeCTA cta = null;
                var ctas = RequestManager.Services.UnitTypesService.GetUnitTypeCta(this.UnitTypeId.Value);
                foreach (var t in ctas)
                {
                    cta = new UnitTypeCTA();
                    cta.DateFrom = t.DateFrom;
                    cta.DateUntil = t.DateUntil;
                    cta.UnitType = RequestManager.Services.UnitTypesService.GetUnitTypeById(this.UnitTypeId.Value);
                    cta.UnitTypeCTAID = t.UnitTypeCTAID;
                    cta.Monday = t.Monday;
                    cta.Tuesday = t.Tuesday;
                    cta.Wednesday = t.Wednesday;
                    cta.Thursday = t.Thursday;
                    cta.Friday = t.Friday;
                    cta.Saturday = t.Saturday;
                    cta.Sunday = t.Sunday;
                    ctrlCta.ElementsList.Add(cta);
                }

                #endregion
            }
        }

        private void SetNames()
        {
            if (Mode == FormMode.Edit)
            {
                UnitType ut = RequestManager.Services.UnitTypesService.GetUnitTypeById(UnitTypeId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"), ut.UnitTypeTitle.ToString());
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"), txtCode.Text, ut.Property.PropertyName);
            }  
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ParseQueryString();
                BindControls();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();

                    SetNames();
                }
            }

            //enter button
            Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                txtTitle.Focus();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"));

                    rwError.VisibleOnPageLoad = true;
                }
                string script = "<script language=JavaScript> window.parent.$('#modalDialog').dialog('close').remove(); </script>";
                Response.Write(script);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_UnitType"), String.IsNullOrEmpty(txtTitle.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtTitle.Text);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            string script = "<script language=JavaScript> window.parent.$('#modalDialog').dialog('close').remove(); </script>";
            Response.Write(script);
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            string script = "<script language=JavaScript> window.parent.$('#modalDialog').dialog('close').remove(); </script>";
            Response.Write(script);
        }

        protected void cvUnitCode_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            args.IsValid = false;
            var p = RequestManager.Services.UnitTypesService.GetUnitTypeForPropertyByCode(this.PropertyId.Value, this.txtCode.Text);
            if (p == null)
                args.IsValid = true;
            else if (Mode == FormMode.Edit)
            {
                if (p.UnitTypeID == this.UnitTypeId.Value)
                    args.IsValid = true;
            }
        }

        #endregion        
    }
}