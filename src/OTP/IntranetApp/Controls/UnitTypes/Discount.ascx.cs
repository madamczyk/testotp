﻿using BusinessLogic.Managers;
using Common;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Common.CultureInfo;
using DataAccess;
using Common.Helpers;
using Common.Extensions;

namespace IntranetApp.Controls.UnitTypes
{
    public partial class Discount : System.Web.UI.UserControl
    {

        #region Fields

        public int? UnitTypeId
        {
            get { return ViewState["UnitTypeId"] as int?; }
            set { ViewState["UnitTypeId"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemId"] as int?; }
            set { ViewState["EditedItemId"] = value; }
        }
        
        /// <summary>
        /// List will contain all the Discounts for the destination. Will be used for displaying the list to the user
        /// </summary>
        public List<UnitType7dayDiscounts> ElementsList
        {
            get { return ViewState["DiscountElementsList"] as List<UnitType7dayDiscounts>; }
            set { ViewState["DiscountElementsList"] = value; }
        }

        /// <summary>
        /// List will contain Discounts, that were added by the user. Discounts from this list should be added to the DB
        /// </summary>
        public List<UnitType7dayDiscounts> NewElements
        {
            get { return ViewState["DiscountNewElements"] as List<UnitType7dayDiscounts>; }
            set { ViewState["DiscountNewElements"] = value; }
        }

        /// <summary>
        /// List will contain Discounts, that were modified by the user and did not exist in the DB before - we don't need to update taxes, that do not exist in the DB
        /// </summary>
        public List<UnitType7dayDiscounts> ModifiedElementsList
        {
            get { return ViewState["DiscountModifiedElementsList"] as List<UnitType7dayDiscounts>; }
            set { ViewState["DiscountModifiedElementsList"] = value; }
        }

        /// <summary>
        /// List will containt IDs of taxes that were deleted by the user. Discounts from this list should be deleted from the DB
        /// </summary>
        public List<int> RemovedElementsList
        {
            get { return ViewState["DiscountRemovedElementsList"] as List<int>; }
            set { ViewState["DiscountRemovedElementsList"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.discountsLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.UnitType7dayDiscounts> result = ElementsList.AsQueryable();
                return ElementsList.ToList();
            };            
        }

        #endregion
        
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgDiscounts_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int id = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitType7dayID"].ToString());
                ElementsList.RemoveAll(t => t.UnitType7dayID == id); // remove deleted tax from the list of taxes
                ModifiedElementsList.RemoveAll(t => t.UnitType7dayID == id); // remove deleted tax from the list of modified taxes
                if (NewElements.RemoveAll(t => t.UnitType7dayID == id) == 0) // remove deleted tax from the list of new taxes. if number of deleted taxes equals 0, it means, that tax existed in the db and we need to delete it
                    RemovedElementsList.Add(id); // add id of tax to the list of removed taxes

            }
            else if (e.CommandName == "Edit")
            {
                int editedId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitType7dayID"].ToString());
                EditedItemId = editedId;
            }
            else if (e.CommandName == "Cancel")
            {
                EditedItemId = null;
            }
        }

        protected void rgDiscounts_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                UnitType7dayDiscounts obj = (UnitType7dayDiscounts)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                string title = String.Format("{0} - {1}, {2}%", obj.DateFrom.ToLocalizedDateString(), obj.DateUntil.ToLocalizedDateString(), obj.Discount.ToString("N2"));

                item["DateFrom"].Text = obj.DateFrom.ToLocalizedDateString();
                item["DateUntil"].Text = obj.DateUntil.ToLocalizedDateString();
                item["Discount"].Text = obj.Discount + "%";

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgDiscounts.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Discount"), title), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Discount"), title));
            }
        }

        protected void rgDiscounts_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgDiscounts.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgDiscounts.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgDiscounts.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgDiscounts.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgDiscounts.MasterTableView.Rebind();
            }
        }

        protected void rgDiscounts_InsertCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditFormInsertItem)
            {
                DataAccess.UnitType7dayDiscounts newUnit = new DataAccess.UnitType7dayDiscounts();
                ReadGridItemData(e.Item, newUnit);
                newUnit.UnitType7dayID = ElementsList.Count > 0 ? ElementsList.Max(t => t.UnitType7dayID) + 1 : 1;
                ElementsList.Add(newUnit);
                NewElements.Add(newUnit);
                this.rgDiscounts.MasterTableView.Rebind();
            }
        }

        protected void rgDiscounts_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    DataAccess.UnitType7dayDiscounts unit = null;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("UnitType7dayID"));
                    unit = ElementsList.Where(t => t.UnitType7dayID == id).SingleOrDefault();

                    ReadGridItemData(editForm, unit);

                    if (NewElements.Where(t => t.UnitType7dayID == id).Count() == 0) // if tax is not in the list of newly added
                    {
                        ModifiedElementsList.Add(unit); // make sure to update its data
                    }
                    else
                    {
                        NewElements.RemoveAll(t => t.UnitType7dayID == id);
                        NewElements.Add(unit);
                    }
                    this.rgDiscounts.MasterTableView.Rebind();
                }
                int reportId = int.Parse(args.Item.OwnerTableView.DataKeyValues[args.Item.ItemIndex]["UnitType7dayID"].ToString());
                EditedItemId = reportId;
            }
        }

        private void ReadGridItemData(GridItem item, DataAccess.UnitType7dayDiscounts unitDiscount)
        {
            unitDiscount.DateFrom = (item.FindControl("rcDateFrom") as RadDatePicker).SelectedDate.Value;
            unitDiscount.DateUntil = (item.FindControl("rcDateUntil") as RadDatePicker).SelectedDate.Value;
            unitDiscount.Discount = decimal.Parse((item.FindControl("txtDiscount") as RadTextBox).Text);
        }

        protected void RequiredFieldValidator_PreRender(object sender, EventArgs e)
        {
            RegularExpressionValidator regVal = sender as RegularExpressionValidator;
            string exp = @"^\d{1,8}(\<NumberSeparator>\d{0,2})?";
            regVal.ValidationExpression = exp.Replace("<NumberSeparator>", CurrentCultureInfoSet.NumberSeparator);
        }

        protected void cvInvalidPeriod_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool valid = true;
            DateTime DateFrom = DateTime.Parse(args.Value);
            DateTime? DateUntil = ((RadDatePicker)((CustomValidator)source).FindControl("rcDateUntil")).SelectedDate;
            IQueryable<UnitType7dayDiscounts> result = ElementsList == null ? new List<UnitType7dayDiscounts>().AsQueryable() : ElementsList.AsQueryable();

            if (!EditedItemId.HasValue)
            {
                if (result.Any(o => o.DateFrom <= DateFrom && o.DateUntil >= DateFrom)) // does DateFrom fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.DateFrom <= DateUntil && o.DateUntil >= DateUntil)) // does DateUntil fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.DateFrom >= DateFrom && o.DateUntil <= DateUntil))// does DateFrom - DateUntil contain any other period?
                    valid = false;
            }
            else
            {
                if (result.Any(o => o.UnitType7dayID != EditedItemId.Value && o.DateFrom <= DateFrom && o.DateUntil >= DateFrom)) // does DateFrom fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.UnitType7dayID != EditedItemId.Value && o.DateFrom <= DateUntil && o.DateUntil >= DateUntil)) // does DateUntil fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.UnitType7dayID != EditedItemId.Value && o.DateFrom >= DateFrom && o.DateUntil <= DateUntil))// does DateFrom - DateUntil contain any other period?
                    valid = false;
            }

            args.IsValid = valid;
        }

        #endregion Event Handlers
    }
}