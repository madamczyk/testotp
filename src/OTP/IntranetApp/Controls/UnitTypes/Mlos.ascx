﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mlos.ascx.cs" Inherits="IntranetApp.Controls.UnitTypes.Mlos" %>


<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgUnitTypes">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgUnitTypes" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<div runat="server" id="scriptBlock">
    <script type="text/javascript">
        function onMLOSDateSelected(sender, args) {
            var dateField = $('#<%=hfSelectedDateFrom.ClientID%>');
            dateField.val(sender.get_selectedDate());
        }
        function onMLOSUntilDatePopupOpening(sender, args) {
            var dateField = $('#<%=hfSelectedDateFrom.ClientID%>');
            if (dateField.val() != '') {
                var dateFrom = new Date(dateField.val());
                sender.set_focusedDate(dateFrom);
            }
            else {
                sender.set_focusedDate(new Date());
            }
        }
    </script>
</div>

<telerik:RadGrid
    Width="550px"
    AutoGenerateColumns="False" 
    ID="rgMlos" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="mlosLinqDS"
	ClientSettings-EnablePostBackOnRowClick="true" 
    OnItemCommand="rgMlos_ItemCommand" 
    OnDataBound="rgMlos_DataBound"
    OnItemDataBound="rgMlos_ItemDataBound"
    OnUpdateCommand="rgMlos_UpdateCommand"
    OnInsertCommand="rgMlos_InsertCommand" >
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Auto" DataKeyNames="UnitTypeMLOSID" DataSourceID="mlosLinqDS" CommandItemDisplay="Bottom" >
		<Columns>
            <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" HeaderText=" " HeaderStyle-Width="50px" >
                    <ItemStyle Width="50px"></ItemStyle>
            </telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn DataField="DateFrom" FilterControlWidth="150px" UniqueName="DateFrom" HeaderText="Date From" SortExpression="DateFrom" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DateUntil" FilterControlWidth="150px" UniqueName="DateUntil" HeaderText="Date Until" SortExpression="DateUntil" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="MLOS" FilterControlWidth="150px" UniqueName="MLOS" HeaderText="Min. length of stay" SortExpression="MLOS" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridButtonColumn>		
		</Columns>
        <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="Mlos"
                        HeaderText=" " DisplayMode="BulletList"
                        EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDateFrom" meta:resourceKey="lblDateFrom"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadDatePicker ID="rcDateFrom" runat="server" SelectedDate='<%# (Eval("DateFrom") != null && Eval("DateFrom") is DateTime) ? Convert.ToDateTime(Eval("DateFrom")) : (DateTime?)null %>'>
                                    <DateInput runat="server"
                                        DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    <ClientEvents OnDateSelected="onMLOSDateSelected" />
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ValidationGroup="Mlos" ID="rfvNameProductParam"
                                    Text="*" ControlToValidate="rcDateFrom" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDateFrom"></asp:RequiredFieldValidator>
                                <asp:CustomValidator 
                                    ID="cvDateFrom"
                                    ValidationGroup="Mlos"
                                    Display="Dynamic"
                                    runat="server"
                                    OnServerValidate="cvInvalidPeriod_ServerValidate"
                                    meta:resourcekey="ValMlosPeriod"
                                    ControlToValidate="rcDateFrom"
                                    Text="*"
                                    CssClass="error" />
                            </td>
                        </tr> 
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDateUntil" meta:resourceKey="lblDateUntil"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadDatePicker ID="rcDateUntil" runat="server"  SelectedDate='<%# (Eval("DateUntil") != null && Eval("DateUntil") is DateTime) ? Convert.ToDateTime(Eval("DateUntil")) : (DateTime?)null %>'>
                                    <DateInput runat="server"
                                        DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    <ClientEvents OnPopupOpening="onMLOSUntilDatePopupOpening" />
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ValidationGroup="Mlos" ID="RequiredFieldValidator1"
                                    Text="*" ControlToValidate="rcDateUntil" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDateFrom"></asp:RequiredFieldValidator>                               
                                <br />
                                <asp:CompareValidator ID="cmpEndDate" runat="server"
                                    meta:resourceKey="ValDates" CssClass="error" ValidationGroup="Mlos"
                                    ControlToCompare="rcDateFrom" ControlToValidate="rcDateUntil"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                        </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblMlos" meta:resourceKey="lblMlos"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtMlos" MaxLength="50" Text='<%# Eval("MLOS") %>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="Mlos" ID="RequiredFieldValidator2"
                                    Text="*" ControlToValidate="txtMlos" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValMlos"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator 
                                    ValidationExpression="^\s*[0-9]{1,10}\s*$"
                                    ID="RegularExpressionValidator3" 
                                    ValidationGroup="Mlos"
                                    runat="server" 
                                    Display="Dynamic" 
                                    ControlToValidate="txtMlos"                     
                                    meta:resourcekey="ValMlosFormat" 
                                    Text="*" 
                                    CssClass="error" />
                                 <asp:RangeValidator
                                    ID="RegularExpressionValidator1" 
                                    ValidationGroup="Mlos"
                                    runat="server" 
                                    Display="Dynamic" 
                                    ControlToValidate="txtMlos"                     
                                    meta:resourcekey="ValMlosFormat" 
                                    Text="*"
                                    MinimumValue="1"
                                    MaximumValue="999999999"
                                    CssClass="error" />
                            </td>
                        </tr>                                   
                        <tr>
                            <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                <div style="position: relative; width: 415px;">
                                    <!-- workaround for issue with RadButtons not being aligned vertically -->
                                    <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="Mlos"
                                        VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        CausesValidation="true">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                    </telerik:RadButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>
<asp:HiddenField ID="hfSelectedDateFrom" runat="server" />

<otpDS:SimpleDataSource ID="mlosLinqDS" runat="server" />