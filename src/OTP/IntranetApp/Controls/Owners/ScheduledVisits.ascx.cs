﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Common.CultureInfo;
using Common.Extensions;

namespace IntranetApp.Controls.Owners
{
    public partial class ScheduledVisits : System.Web.UI.UserControl
    {
        public int? userId
        {
            get { return ViewState["userId"] as int?; }
            set { ViewState["userId"] = value; }
        }

        #region Methods
        
        private void BindGrid()
        {
            scheduledVisitsLinqDS.DataResolver = () =>
            {
                if (this.userId.HasValue)
                {
                    IQueryable<ScheduledVisit> result = RequestManager.Services.ScheduledVisitService.GetVisitsByUserId(this.userId.Value).AsQueryable();
                    rgScheduledVisits.ApplyFilter<ScheduledVisit>(ref result, "DateCreated", (q, f) => q.Where(p => p.DateCreated.ToString(StringFormats.LongDate, System.Globalization.CultureInfo.InvariantCulture).ToLower().Contains(f.ToLower())));

                    return result.ToList();
                }
                else
                    return new List<ScheduledVisit>();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgScheduledVisits_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgScheduledVisits_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                ScheduledVisit obj = (ScheduledVisit)item.DataItem;

                item["Dates"].Text = String.Join(", ", obj.DatesList.Select(d => d.ToLocalizedDateString()));
                item["Hours"].Text = DateTime.Parse(obj.HourFrom + ":00").ToLocalizedTimeString() + " - " + DateTime.Parse(obj.HourUntil + ":00").ToLocalizedTimeString();
                item["DateCreated"].Text = obj.DateCreated.ToLocalizedDateTimeString();
            }
        }

        protected void rgScheduledVisits_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgScheduledVisits.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgScheduledVisits.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgScheduledVisits.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgScheduledVisits.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgScheduledVisits.MasterTableView.Rebind();
            }
        }

        #endregion Event Handlers
    }
}