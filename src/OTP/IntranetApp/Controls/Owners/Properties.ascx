﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Properties.ascx.cs" Inherits="IntranetApp.Controls.Owners.Properties" %>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<telerik:RadGrid 
    AutoGenerateColumns="False" 
    ID="rgProperties" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="True" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="propertiesLinqDS"
	ClientSettings-EnablePostBackOnRowClick="true" 
    OnItemCommand="rgProperties_ItemCommand" 
    OnDataBound="rgProperties_DataBound" 
    ClientSettings-ClientEvents-OnRowClick="forceWarning"
    ClientSettings-ClientEvents-OnColumnClick="disableWarning">
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="True" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Fixed" DataKeyNames="PropertyID" DataSourceID="propertiesLinqDS" CommandItemDisplay="None">
		<Columns>           
			<telerik:GridBoundColumn DataField="PropertyNameCurrentLanguage" FilterControlWidth="150px" UniqueName="Name" HeaderText="Name" SortExpression="PropertyNameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Destination.DestinationNameCurrentLanguage" FilterControlWidth="150px" UniqueName="Market" HeaderText="Market" SortExpression="Destination.DestinationNameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DictionaryCountry.CountryNameCurrentLanguage" FilterControlWidth="150px" UniqueName="Country" HeaderText="Country" SortExpression="DictionaryCountry.CountryNameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="City" FilterControlWidth="150px" UniqueName="City" HeaderText="City" SortExpression="City" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
		</Columns>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="PropertyNameCurrentLanguage" SortOrder="Ascending" />
        </SortExpressions>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<otpDS:SimpleDataSource ID="propertiesLinqDS" runat="server" />