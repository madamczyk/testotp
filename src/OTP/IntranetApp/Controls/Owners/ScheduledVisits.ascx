﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScheduledVisits.ascx.cs" Inherits="IntranetApp.Controls.Owners.ScheduledVisits" %>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<telerik:RadGrid 
    AutoGenerateColumns="False" 
    ID="rgScheduledVisits" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="scheduledVisitsLinqDS"
	ClientSettings-EnablePostBackOnRowClick="false" 
    OnItemCommand="rgScheduledVisits_ItemCommand"
    OnItemDataBound="rgScheduledVisits_ItemDataBound" 
    OnDataBound="rgScheduledVisits_DataBound"
    ClientSettings-ClientEvents-OnColumnClick="disableWarning" >
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="True" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Fixed" DataKeyNames="VisitId" DataSourceID="scheduledVisitsLinqDS" CommandItemDisplay="None">
		<Columns>           
            <telerik:GridTemplateColumn HeaderText="Dates" UniqueName="Dates" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="false">  
				<ItemStyle CssClass="link" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Hours" UniqueName="Hours" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="false" HeaderStyle-Width="25%">  
				<ItemStyle CssClass="link" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Request date" UniqueName="DateCreated" InitializeTemplatesFirst="false" SortExpression="DateCreated" ForceExtractValue="Always" AllowFiltering="false" HeaderStyle-Width="25%">
				<ItemStyle CssClass="link" />
            </telerik:GridTemplateColumn>
		</Columns>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="DateCreated" SortOrder="Descending" />
        </SortExpressions>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<otpDS:SimpleDataSource ID="scheduledVisitsLinqDS" runat="server" />