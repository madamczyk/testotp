﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Owners
{
    public partial class Properties : System.Web.UI.UserControl
    {
        public int? userId
        {
            get { return ViewState["userId"] as int?; }
            set { ViewState["userId"] = value; }
        }

        #region Methods
        
        private void BindGrid()
        {
            propertiesLinqDS.DataResolver = () =>
            {
                if (this.userId.HasValue)
                {
                    IQueryable<Property> result = RequestManager.Services.PropertiesService.GetPropertiesByOwnerId(this.userId.Value).AsQueryable();
                    rgProperties.ApplyFilter<Property>(ref result, "Name", (q, f) => q.Where(p => p.PropertyNameCurrentLanguage.ToLower().Contains(f.ToLower())));
                    rgProperties.ApplyFilter<Property>(ref result, "Market", (q, f) => q.Where(p => p.Destination.DestinationNameCurrentLanguage.ToLower().Contains(f.ToLower())));
                    rgProperties.ApplyFilter<Property>(ref result, "Country", (q, f) => q.Where(p => p.DictionaryCountry.CountryNameCurrentLanguage.ToLower().Contains(f.ToLower())));
                    rgProperties.ApplyFilter<Property>(ref result, "City", (q, f) => q.Where(p => p.City.ToLower().Contains(f.ToLower())));

                    return result.ToList();
                }
                else 
                    return new List<Property>();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgProperties_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int propertyId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PropertyID"].ToString());

                if (RequestManager.Services.PropertiesService.GetPropertyById(propertyId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Property"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_Property"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    BindGrid();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/Properties/PropertyDetails.aspx?id=");
                    sb.Append(propertyId);
                    sb.Append("&mode=");
                    sb.Append((int)FormMode.Edit);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
        }

        protected void rgProperties_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Property obj = (Property)item.DataItem;
            }
        }

        protected void rgProperties_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgProperties.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgProperties.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgProperties.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgProperties.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgProperties.MasterTableView.Rebind();
            }
        }

        #endregion Event Handlers
    }
}