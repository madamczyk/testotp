﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OwnerDetails.ascx.cs" Inherits="IntranetApp.Controls.Owners.OwnerDetails" %>
<%@ Register TagPrefix="uc" TagName="Properties" Src="~/Controls/Owners/Properties.ascx" %>
<%@ Register TagPrefix="uc" TagName="ScheduledVisits" Src="~/Controls/Owners/ScheduledVisits.ascx" %>
<%@ Register TagPrefix="uc" TagName="UserRoles" Src="~/Controls/Users/UserRoles.ascx" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="pnlData">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlData" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
       <telerik:RadWindow ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" Text="<%$ Resources: Controls, Dialog_Discard_Text %>" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="btnDiscard_Yes"></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClick="btnDiscard_No"></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
        <telerik:RadWindow ID="rwError" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblError" Font-Bold="true" runat="server" />
                        <br />
                        <br />
                        <asp:Button ID="wndBtnError_Ok" runat="server" Text="<%$ Resources: Controls, Dialog_Ok %>" OnClick="btnError_Ok" />
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<asp:Panel runat="server" ID="pnlData">
    <asp:ValidationSummary ID="vsSummary" ValidationGroup="Owner" HeaderText=" "
        DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
    <h1 class="FormHeader">
        <asp:Label runat="server" ID="lblHeader"></asp:Label>
    </h1>
    <br />
    <table>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblFirstname" meta:resourcekey="lblFirstname" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" TabIndex="1" ID="txtFirstname" MaxLength="250" Enabled="true" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvFirstname" ValidationGroup="Owner" runat="server" ControlToValidate="txtFirstname"
                    Display="Dynamic" meta:resourcekey="ValFirstname" Text="*" CssClass="error" />
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblLastname" meta:resourcekey="lblLastname" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" TabIndex="2" ID="txtLastname" MaxLength="250" Enabled="true" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvLastname" ValidationGroup="Owner" runat="server" ControlToValidate="txtLastname"
                    Display="Dynamic" meta:resourcekey="ValLastname" Text="*" CssClass="error" />
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblStatus" meta:resourcekey="lblStatus" />
            </td>
            <td colspan="4">
                <telerik:RadComboBox runat="server" TabIndex="3" ID="rcbStatus" Enabled="true" AutoPostBack="false" Width="250px" />
            </td>
        </tr>
        <tr style="height: 20px;">
            <td colspan="5"></td>
        </tr>
        <tr>
            <td colspan="5">
                <h3><asp:Literal runat="server" meta:resourceKey="hAddress" /></h3>
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblAddressLine1" meta:resourcekey="lblAddressLine1" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" ID="txtAddressLine1" TabIndex="4" Enabled="true" Width="250px" MaxLength="250" />
            </td>
            <td width="30px">
            </td>
            <td width="150px">
                <asp:Label runat="server" ID="lblCity" meta:resourcekey="lblCity" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" ID="txtCity" TabIndex="5" Enabled="true" Width="250px"  MaxLength="250" />
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblAddressLine2" meta:resourcekey="lblAddressLine2" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" ID="txtAddressLine2" TabIndex="6" Enabled="true" Width="250px" MaxLength="250" />
            </td>
            <td width="30px">
            </td>
            <td width="150px">
                <asp:Label runat="server" ID="lblState" meta:resourcekey="lblState" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" ID="txtState" TabIndex="7" Enabled="true" Width="250px" MaxLength="250"/>
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblZipCode" meta:resourcekey="lblZipCode" />
            </td>
            <td>
                <telerik:RadTextBox runat="server" ID="txtZipCode" TabIndex="8" Enabled="true" Width="250px" MaxLength="250" />
            </td>
            <td width="30px">
            </td>
            <td width="150px">
                <asp:Label runat="server" ID="lblCountry" meta:resourcekey="lblCountry" />
            </td>
            <td>
                <telerik:RadComboBox runat="server" ID="rcbCountry" TabIndex="9" Width="250px" MaxHeight="200px" />
            </td>
        </tr>
        <tr style="height: 20px;">
            <td colspan="5"></td>
        </tr>
        <tr>
            <td colspan="5">
                <h3><asp:Literal runat="server" meta:resourceKey="hContact" /></h3>
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblEmail" meta:resourcekey="lblEmail" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" TabIndex="10" ID="txtEmail" MaxLength="250" Enabled="true" Width="250px" />
                <asp:RequiredFieldValidator ID="rfvEmail" ValidationGroup="Owner" runat="server" ControlToValidate="txtEmail"
                    Display="Dynamic" meta:resourcekey="ValEmail" Text="*" CssClass="error" />
                <asp:RegularExpressionValidator ID="revEmail" ValidationGroup="Owner" runat="server" ControlToValidate="txtEmail"
                    Display="Dynamic" meta:resourcekey="FormatEmail" Text="*" CssClass="error" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                <asp:CustomValidator ID="cvEmailExist" runat="server" Text="*" CssClass="error" ControlToValidate="txtEmail"
                    Display="Dynamic" meta:resourcekey="EmailExists" ValidateEmptyText="false" ValidationGroup="Owner" OnServerValidate="cvEmailExist_ServerValidate" />
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblCellPhone" meta:resourcekey="lblCellPhone" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" TabIndex="11" ID="txtCellPhone" MaxLength="250" Enabled="true" Width="250px" />
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblLandLine" meta:resourcekey="lblLandLine" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" TabIndex="12" ID="txtLandLine" MaxLength="250" Enabled="true" Width="250px" />
            </td>
        </tr>
        <tr style="height: 20px;">
            <td colspan="5"></td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblLicense" meta:resourcekey="lblLicense" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" TabIndex="13" ID="txtLicense" MaxLength="250" Enabled="true" Width="250px" />
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblPassport" meta:resourcekey="lblPassport" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" TabIndex="14" ID="txtPassport" MaxLength="250" Enabled="true" Width="250px" />
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblSocialSecurity" meta:resourcekey="lblSocialSecurity" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" TabIndex="15" ID="txtSocialSecurity" MaxLength="250" Enabled="true" Width="250px" />
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblDirectDeposit" meta:resourcekey="lblDirectDeposit" />
            </td>
            <td colspan="4">
                <telerik:RadTextBox runat="server" TabIndex="16" ID="txtDirectDeposit" MaxLength="250" Enabled="true" Width="250px" />
            </td>
        </tr>
        <tr>
            <td width="150px">
                <asp:Label runat="server" ID="lblLanguage" meta:resourcekey="lblLanguage" />
            </td>
            <td width="250px">
                <telerik:RadComboBox runat="server" ID="rcbLanguage" TabIndex="17" Enabled="true" AutoPostBack="false" Width="250px" />
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlOwnerData" Visible="false">
        <br />
        <telerik:RadTabStrip ID="rtsOwnerData" runat="server" SelectedIndex="0" MultiPageID="rmpTabContent">
            <Tabs>
                <telerik:RadTab meta:resourceKey="rtProperties" />
                <telerik:RadTab meta:resourceKey="rtUserRoles" />
            </Tabs>
        </telerik:RadTabStrip>     
        <telerik:RadMultiPage ID="rmpTabContent" runat="server" SelectedIndex="0" >
            <telerik:RadPageView ID="rpvProperties" runat="server" CssClass="radTab">          
                <uc:Properties ID="ctrlProperties" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="rpvUserRoles" runat="server" CssClass="radTab" >
                <uc:UserRoles ID="ctrlUserRoles" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="rpvScheduledVisits" runat="server" CssClass="radTab" Visible="false">
                <uc:ScheduledVisits ID="ctrlScheduledVisits" runat="server" />
            </telerik:RadPageView>
            
        </telerik:RadMultiPage>    
    </asp:Panel>    
</asp:Panel>

<table>
    <tr style="height: 20px;">
        <td></td>
    </tr>
    <tr>
        <td width="645px" align="right" style="padding-right: 3px">
            <telerik:RadButton runat="server" ID="btnSave" TabIndex="18" Text="<%$ Resources: Controls, Button_Save %>" CausesValidation="true" ValidationGroup="Owner" OnClick="btnSave_Click" />
            <telerik:RadButton runat="server" ID="btnCancel" TabIndex="19" Text="<%$ Resources: Controls, Button_Cancel %>" CausesValidation="false" OnClick="btnCancel_Click" />
        </td>
    </tr>
</table>
