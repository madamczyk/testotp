﻿using BusinessLogic.Managers;
using BusinessLogic.Enums;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Common;
using BusinessLogic.Objects.Email;

namespace IntranetApp.Controls.Owners
{
    public partial class OwnerDetails : BaseUserControl
    {
        #region Fields

        public int? UserId
        {
            get { return ViewState["userId"] as int?; }
            set { ViewState["userId"] = value; }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["Mode"]; }
            set { ViewState["Mode"] = (int)value; }
        }

        public int? CurrentRoleLevel
        {
            get { return ViewState["RoleLevel"] as int?; }
            set { ViewState["RoleLevel"] = value; }
        }

        #endregion Fields

        #region Methods

        private void LoadData()
        {
            Debug.Assert(UserId.HasValue);

            User user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);
            UserRole userRole = RequestManager.Services.UsersService.GetUserRoleByLevel(user, (RoleLevel)CurrentRoleLevel);

            // status check
            if (userRole.Status == (int)UserStatus.Requested)
            {
                rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Requested.ToString(), ((int)UserStatus.Requested).ToString()));
                rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Inactive.ToString(), ((int)UserStatus.Inactive).ToString()));
            }
            else if (userRole.Status == (int)UserStatus.Inactive)
            {
                rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Inactive.ToString(), ((int)UserStatus.Inactive).ToString()));
                rcbStatus.Enabled = false;
            }
            else if (userRole.Status == (int)UserStatus.Active)
            {
                rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Active.ToString(), ((int)UserStatus.Active).ToString()));
                rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Blocked.ToString(), ((int)UserStatus.Blocked).ToString()));
                rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Deleted.ToString(), ((int)UserStatus.Deleted).ToString()));
            }
            else if (userRole.Status == (int)UserStatus.Blocked)
            {
                rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Blocked.ToString(), ((int)UserStatus.Blocked).ToString()));
                rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Active.ToString(), ((int)UserStatus.Active).ToString()));
                rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Deleted.ToString(), ((int)UserStatus.Deleted).ToString()));
            }
            else if (userRole.Status == (int)UserStatus.Deleted)
            {
                rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Deleted.ToString(), ((int)UserStatus.Deleted).ToString()));
                rcbStatus.Enabled = false;
            }
            rcbStatus.SelectedValue = userRole.Status.ToString();

            //textboxes
            txtFirstname.Text = user.Firstname;
            txtLastname.Text = user.Lastname;
            txtAddressLine1.Text = user.Address1;
            txtCity.Text = user.City;
            txtAddressLine2.Text = user.Address2;
            txtState.Text = user.State;
            txtZipCode.Text = user.ZIPCode;
            rcbCountry.SelectedValue = user.DictionaryCountry.CountryId.ToString();
            txtEmail.Text = user.email;
            txtCellPhone.Text = user.CellPhone;
            txtLandLine.Text = user.LandLine;
            txtLicense.Text = user.DriverLicenseNbr;
            txtPassport.Text = user.PassportNbr;
            txtSocialSecurity.Text = user.SocialsecurityNbr;
            txtDirectDeposit.Text = user.DirectDepositInfo;
            rcbLanguage.SelectedValue = user.DictionaryLanguage.LanguageId.ToString();

            //user roles
            this.ctrlUserRoles.UserId = this.UserId.Value;
        }

        private bool SaveData()
        {
            User user = new User();
            if (Mode == FormMode.Add)
            {
                user = new User();
            }
            else
            {
                user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);
            }
            user.Firstname = txtFirstname.Text;
            user.Lastname = txtLastname.Text;
            user.Address1 = txtAddressLine1.Text;
            user.Address2 = txtAddressLine2.Text;
            user.City = txtCity.Text;
            user.ZIPCode = txtZipCode.Text;
            user.State = txtState.Text;
            user.DictionaryCountry = RequestManager.Services.DictionaryCountryService.GetCountryById(int.Parse(this.rcbCountry.SelectedValue));
            user.email = txtEmail.Text;
            user.CellPhone = txtCellPhone.Text;
            user.LandLine = txtLandLine.Text;
            user.DriverLicenseNbr = txtLicense.Text;
            user.PassportNbr = txtPassport.Text;
            user.SocialsecurityNbr = txtSocialSecurity.Text;
            user.DirectDepositInfo = txtDirectDeposit.Text;
            user.DictionaryLanguage = RequestManager.Services.DictionaryLanguageService.GetLanguageById(int.Parse(this.rcbLanguage.SelectedValue));

            if (Mode == FormMode.Add)
            {
                // Admin or manager creates owner account
                UserRole ownerRole = RequestManager.Services.RolesService.AddUserToRole(user, (RoleLevel)CurrentRoleLevel);
                ownerRole.Status = (int)UserStatus.Inactive;
                ownerRole.ActivationToken = Guid.NewGuid().ToString();
                user.UserRoles.Add(ownerRole);

                user.Password = RequestManager.Services.AuthorizationService.GeneratePassword();
                SendActivationEmailWithPassword(user, ownerRole);
                user.Password = MD5Helper.Encode(user.Password);
                RequestManager.Services.UsersService.AddUser(user);
            }
            else 
            {
                int newStatus = int.Parse(this.rcbStatus.SelectedValue);
                UserRole userRole = user.UserRoles.Where(ur => ur.Role.RoleLevel == CurrentRoleLevel.Value).SingleOrDefault();
                if (userRole.Status == (int)UserStatus.Requested && newStatus == (int)UserStatus.Inactive)
                {
                    userRole.ActivationToken = Guid.NewGuid().ToString();
                    SendActivationEmail(user, userRole);
                }
                userRole.Status = newStatus;
            }

            return true;
        }

        private void SendActivationEmailWithPassword(User user, UserRole role)
        {
            OwnerActivationEmail ownerActivationEmail = new OwnerActivationEmail(user.email);
            ownerActivationEmail.UserFirstName = user.Firstname;
            ownerActivationEmail.UserLogin = user.email;
            ownerActivationEmail.UserPassword = user.Password;
            ownerActivationEmail.ActivationLinkTitle = GetLocalResourceObject("ActivationLinkText").ToString();
            ownerActivationEmail.ActivationLinkUrl = string.Format("{0}://{1}{2}/{3}",
                        Request.Url.Scheme,
                        RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.ExtranetURL),
                        "/Account/Activation",
                        role.ActivationToken);

            RequestManager.Services.EmailService.SendEmail(ownerActivationEmail, LanguageCode.en);
        }

        private void SendActivationEmail(User user, UserRole role)
        {
            UserActivationEmail guestActivationEmail = new UserActivationEmail(user.email);
            guestActivationEmail.UserFirstName = user.Firstname;
            guestActivationEmail.ActivationLinkUrl = string.Format("{0}://{1}{2}/{3}",
                        Request.Url.Scheme,
                        RequestManager.Services.SettingsService.GetSettingValue(SettingKeyName.ExtranetURL),
                        "/Account/Activation",
                        role.ActivationToken);
            guestActivationEmail.ActivationLinkTitle = GetLocalResourceObject("ActivationLinkText").ToString();

            RequestManager.Services.EmailService.SendEmail(guestActivationEmail, LanguageCode.en); // should be (LanguageCode)Enum.Parse(typeof(LanguageCode), Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)
        }

        private bool CheckExists()
        {
            if (UserId.HasValue && RequestManager.Services.UsersService.GetUserByUserId(UserId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Owner"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_Owner"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void InitControls()
        {
            rcbStatus.Items.Add(new RadComboBoxItem(UserStatus.Inactive.ToString(), ((int)UserStatus.Inactive).ToString()));
            rcbStatus.SelectedValue = ((int)UserStatus.Inactive).ToString();
            rcbStatus.Enabled = false;
        }

        private void BindControls()
        {
            // countries
            foreach (DictionaryCountry obj in RequestManager.Services.DictionaryCountryService.GetCountries())
            {
                rcbCountry.Items.Add(new RadComboBoxItem(obj.CountryName.ToString(), obj.CountryId.ToString()));
            }
            rcbCountry.SelectedValue = RequestManager.Services.DictionaryCountryService.GetCountryByCountryCode("us").CountryId.ToString();

            // languages
            foreach (DictionaryLanguage obj in RequestManager.Services.DictionaryLanguageService.GetLanguages())
            {
                rcbLanguage.Items.Add(new RadComboBoxItem(obj.LanguageNameCurrentLanguage.ToString(), obj.LanguageId.ToString()));
            }
            rcbLanguage.SelectedValue = RequestManager.Services.DictionaryLanguageService.GetLanguageByCode("en").LanguageId.ToString();

            if (UserId.HasValue)
            {
                User user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);

                pnlOwnerData.Visible = true;
                ctrlProperties.userId = UserId.Value;
                ctrlScheduledVisits.userId = UserId.Value;

                UserRole userRole = RequestManager.Services.UsersService.GetUserRoleByLevel(user, (RoleLevel)CurrentRoleLevel);

                if (userRole != null && userRole.Status == (int)UserStatus.Requested)
                {
                    rtsOwnerData.Tabs.Add(new RadTab((string)GetLocalResourceObject("rtScheduledVisits.Text")));
                    rpvScheduledVisits.Visible = true;
                }
            }                
        }

        private void SetNames()
        {
            if (Mode == FormMode.Edit)
            {
                User user = RequestManager.Services.UsersService.GetUserByUserId(UserId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Owner"), user.FullName);
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"),
                    EnumConverter.GetValue((RoleLevel)(CurrentRoleLevel.Value)),
                    user.FullName);
            }
            else
            {
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderAdd.Text"),
                    EnumConverter.GetValue((RoleLevel)(CurrentRoleLevel.Value)));
            }
  
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindControls();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();
                    else
                        InitControls();

                    SetNames();
                }
            }

            //enter button
            Page.Form.DefaultButton = btnSave.UniqueID;
            txtFirstname.Focus();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Owner"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_Owner"));

                    rwError.VisibleOnPageLoad = true;
                }
                else
                    Response.Redirect("~/Forms/Owners/OwnersList.aspx");
            }            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Owner"), String.IsNullOrEmpty(txtFirstname.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtFirstname.Text + " " + txtLastname.Text);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            
            Response.Redirect("~/Forms/Owners/OwnersList.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Owners/OwnersList.aspx");
        }

        protected void cvEmailExist_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Mode == FormMode.Add)
            {
                CustomValidator cv = source as CustomValidator;

                bool valid = true;
                if (!string.IsNullOrWhiteSpace(this.txtEmail.Text))
                {
                    User user = RequestManager.Services.UsersService.GetUserByEmail(this.txtEmail.Text);
                    if (user != null)
                    {
                        valid = false;
                    }
                }
                args.IsValid = valid;
            }
        }

        #endregion
    }
}