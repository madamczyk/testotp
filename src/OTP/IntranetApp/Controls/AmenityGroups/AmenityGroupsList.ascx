﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AmenityGroupsList.ascx.cs" Inherits="IntranetApp.Controls.AmenityGroups.AmenityGroupsList" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="pnlData">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rgTags" />
			</UpdatedControls>
		</telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="pnlData">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

    <h1 class="FormHeader" style="text-transform:none" >
        <asp:Label ID="lblFormTitle" runat="server" meta:resourceKey="lblFormTitle" />
    </h1>

    <telerik:RadGrid 
        AutoGenerateColumns="False" 
        ID="rgAmenityGroups" 
        AllowSorting="True" 
        runat="server" 
        AllowFilteringByColumn="True" 
        AllowPaging="True"
        PageSize="20" 
        DataSourceID="amenityGroupsLinqDS"
		ClientSettings-EnablePostBackOnRowClick="true" 
        OnItemCommand="rgAmenityGroups_ItemCommand" 
        OnItemDataBound="rgAmenityGroups_ItemDataBound"
        OnDataBound="rgAmenityGroups_DataBound"
        OnPreRender="rgAmenityGroups_PreRender"
        OnSortCommand="rgAmenityGroups_SortCommand">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
		<GroupingSettings CaseSensitive="false" />
		<MasterTableView TableLayout="Fixed" DataKeyNames="AmenityGroupId" DataSourceID="amenityGroupsLinqDS">
			<Columns>
				<telerik:GridBoundColumn DataField="Position" UniqueName="No" HeaderText="No" SortExpression="Position" HeaderStyle-Width="5%" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" AllowFiltering="false" />
				<telerik:GridBoundColumn DataField="NameCurrentLanguage" UniqueName="Name" HeaderText="Name" SortExpression="NameCurrentLanguage" HeaderStyle-Width="45%" FilterControlWidth="200px" ShowFilterIcon="false" AutoPostBackOnFilter="true" ItemStyle-CssClass="link" />
                <telerik:GridTemplateColumn DataField="AmenityIcon" UniqueName="Icon" HeaderText="Icon" HeaderStyle-Width="40%" InitializeTemplatesFirst="false" ForceExtractValue="Always" AllowFiltering="false" ItemStyle-CssClass="link">
                    <ItemTemplate> 
                        <%# ((DataAccess.Enums.AmenityIcon)Eval("AmenityIcon")).ToString() %> 
                     </ItemTemplate> 
                </telerik:GridTemplateColumn> 
                <telerik:GridButtonColumn CommandName="MoveUp" ButtonType="ImageButton" UniqueName="MoveUpColumn" ImageUrl="/Images/arrow_up.png" ItemStyle-HorizontalAlign="Center" />	
                <telerik:GridButtonColumn CommandName="MoveDown" ButtonType="ImageButton" UniqueName="MoveDownColumn" ImageUrl="/Images/arrow_down.png" ItemStyle-HorizontalAlign="Center" />
                <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png" ItemStyle-HorizontalAlign="Center" />		
			</Columns>
			<PagerStyle AlwaysVisible="True"></PagerStyle>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="Position" SortOrder="Ascending" />
            </SortExpressions>
		</MasterTableView>
		<HeaderContextMenu EnableImageSprites="True">
		</HeaderContextMenu>
	</telerik:RadGrid>
    <br />
    <telerik:RadButton runat="server" ID="btnAddAmenityGroup" Text="Add" OnClick="btnAddAmenityGroup_Click" />
</asp:Panel>

<otpDS:SimpleDataSource ID="amenityGroupsLinqDS" runat="server" />