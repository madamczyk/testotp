﻿using BusinessLogic.Managers;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using IntranetApp.Enums;

namespace IntranetApp.Controls.AmenityGroups
{
    public partial class AmenityGroupsList : System.Web.UI.UserControl
    {
        public bool AllowReordering
        {
            get
            {
                bool? ar = ViewState["AllowReordering"] as bool?;
                return ar.HasValue ? ar.Value : true;
            }
            set { ViewState["AllowReordering"] = value; }
        }

        public bool AllItemsPresent
        {
            get
            {
                bool? aip = ViewState["AllItemsPresent"] as bool?;
                return aip.HasValue ? aip.Value : true;
            }
            set { ViewState["AllItemsPresent"] = value; }
        }

        #region Methods

        private void BindGrid()
        {
            this.amenityGroupsLinqDS.DataResolver = () =>
            {
                int groupCount;
                IQueryable<AmenityGroup> result = RequestManager.Services.AmenityGroupsService.GetAmenityGroups().AsQueryable();
                groupCount = result.Count();

                rgAmenityGroups.ApplyFilter<AmenityGroup>(ref result, "Name", (q, f) => q.Where(p => p.NameCurrentLanguage.ToLower().Contains(f.ToLower())));

                AllItemsPresent = groupCount == result.Count();

                return result.ToList();
            };
        }

        #endregion Methods

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgAmenityGroups_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                int amenityGroupId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AmenityGroupId"].ToString());

                if (RequestManager.Services.AmenityGroupsService.GetAmenityGroupById(amenityGroupId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Caption"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Access_Text"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                    rgAmenityGroups.Rebind();
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("~/Forms/AmenityGroups/AmenityGroupsDetails.aspx?id=");
                    sb.Append(amenityGroupId);
                    sb.Append("&mode=");
                    sb.Append((int)FormMode.Edit);
                    Response.Redirect(sb.ToString());
                }
            }
            else if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == "MoveUp" || e.CommandName == "MoveDown")
            {
                int amenityGroupId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AmenityGroupId"].ToString());

                if (RequestManager.Services.AmenityGroupsService.GetAmenityGroupById(amenityGroupId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                }
                else
                    RequestManager.Services.AmenityGroupsService.ChangeAmenityGroupPosition(amenityGroupId, e.CommandName == "MoveUp" ? -1 : 1);

                rgAmenityGroups.Rebind();
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int amenityGroupId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AmenityGroupId"].ToString());

                if (RequestManager.Services.AmenityGroupsService.GetAmenityGroupById(amenityGroupId) == null)
                {
                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"));
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"));

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                }
                else if (RequestManager.Services.AmenityGroupsService.HasAnyDependencies(amenityGroupId))
                {
                    AmenityGroup amenityGroup = RequestManager.Services.AmenityGroupsService.GetAmenityGroupById(amenityGroupId);

                    string title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Caption"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"), amenityGroup.Name.ToString());
                    string body = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_CannotDelete_InUse_Text"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"), amenityGroup.Name.ToString());

                    RadWindowManager1.RadAlert(Server.HtmlEncode(body), 330, 100, title, "null");
                }
                else
                    RequestManager.Services.AmenityGroupsService.DeleteAmenityGroup(amenityGroupId);

                rgAmenityGroups.MasterTableView.CurrentPageIndex = 0;
                rgAmenityGroups.Rebind();
            }
        }

        protected void rgAmenityGroups_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                AmenityGroup obj = (AmenityGroup)item.DataItem;
                ImageButton buttonDelete = (ImageButton)item["DeleteColumn"].Controls[0];
                
                buttonDelete.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgAmenityGroups.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"), obj.Name.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"), obj.Name.ToString()));
            }
        }

        protected void rgAmenityGroups_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = rgAmenityGroups.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgAmenityGroups.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (rgAmenityGroups.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                rgAmenityGroups.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                rgAmenityGroups.MasterTableView.Rebind();
            }
        }

        protected void btnAddAmenityGroup_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("~/Forms/AmenityGroups/AmenityGroupsDetails.aspx?");
            sb.Append("mode=");
            sb.Append((int)FormMode.Add);

            Response.Redirect(sb.ToString());
        }

        protected void rgAmenityGroups_PreRender(object sender, EventArgs e)
        {
            foreach (GridDataItem item in rgAmenityGroups.MasterTableView.Items)
            {
                ImageButton buttonMoveUp = (ImageButton)item["MoveUpColumn"].Controls[0];
                ImageButton buttonMoveDown = (ImageButton)item["MoveDownColumn"].Controls[0];
                int currentPage = rgAmenityGroups.MasterTableView.CurrentPageIndex;

                buttonMoveUp.Visible = (currentPage > 0 || item.ItemIndex > 0) && AllowReordering && AllItemsPresent;
                buttonMoveDown.Visible = (currentPage < (rgAmenityGroups.MasterTableView.PageCount - 1) || (item.ItemIndex < (rgAmenityGroups.MasterTableView.Items.Count - 1))) && AllowReordering && AllItemsPresent;
            }
        }

        protected void rgAmenityGroups_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            AllowReordering = e.SortExpression == "Position" && e.NewSortOrder == GridSortOrder.Ascending;
        }

        #endregion Event Handlers
    }
}