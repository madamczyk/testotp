﻿using BusinessLogic.Managers;
using DataAccess;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.AmenityGroups
{
    public partial class AmenityGroupDetails : BaseUserControl
    {      
        #region Fields

        public int? AmenityGroupId
        {
            get { return ViewState["AmenityGroupId"] as int?; }
            set { ViewState["AmenityGroupId"] = value; }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["Mode"]; }
            set { ViewState["Mode"] = (int)value; }
        }             

        #endregion Fields

        #region Methods

        private void ParseQueryString()
        {
            int userID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out userID))
            {
                AmenityGroupId = userID;
            }
        }

        private void LoadData()
        {
            Debug.Assert(AmenityGroupId.HasValue);

            AmenityGroup amenityGroup = RequestManager.Services.AmenityGroupsService.GetAmenityGroupById(AmenityGroupId.Value);

            //textboxes
            txtName.Text = amenityGroup.Name.ToString();
            rcbAmenityIcon.SelectedValue = amenityGroup.AmenityIcon.ToString();
        }

        private bool SaveData()
        {
            AmenityGroup amenityGroup = null;
            if (Mode == FormMode.Add)
                amenityGroup = new AmenityGroup();
            else
                amenityGroup = RequestManager.Services.AmenityGroupsService.GetAmenityGroupById(AmenityGroupId.Value);

            if (amenityGroup == null)
                return false;

            //TODO (ver2): set text depending on the language
            amenityGroup.SetNameValue(txtName.Text);
            amenityGroup.AmenityIcon = int.Parse(rcbAmenityIcon.SelectedValue);

            if (Mode == FormMode.Add)
                RequestManager.Services.AmenityGroupsService.AddAmenityGroup(amenityGroup);
            else
                RequestManager.Services.SaveChanges();

            return true;
        }

        private bool CheckExists()
        {
            if (AmenityGroupId.HasValue && RequestManager.Services.AmenityGroupsService.GetAmenityGroupById(AmenityGroupId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void BindControls()
        {
            // amenity icons
            foreach (DataAccess.Enums.AmenityIcon ai in Enum.GetValues(typeof(DataAccess.Enums.AmenityIcon)))
            {
                rcbAmenityIcon.Items.Add(new RadComboBoxItem(Enum.GetName(typeof(DataAccess.Enums.AmenityIcon), ai), ((int)ai).ToString()));
            }
        }

        private void SetNames()
        {
            if (Mode == FormMode.Edit)
            {
                AmenityGroup amenityGroup = RequestManager.Services.AmenityGroupsService.GetAmenityGroupById(AmenityGroupId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"), amenityGroup.NameCurrentLanguage);
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"), txtName.Text);
            }            
        }

        private void AddScripts()
        {
            // enable warning before unloading the page
            RadScriptManager.RegisterStartupScript(this, GetType(),
                "startup_warn",
                "\twarnBeforeUnload = true;\n\tenableWarning();\n\twarningBeforeUnload = \"" + HttpUtility.JavaScriptStringEncode(String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_LeaveConfrmation"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text)) + "\";\n",
                true);

            // disable warning before postback
            RadScriptManager.RegisterOnSubmitStatement(this, GetType(),
                "onsubmit_disable_warn",
                "disableWarning();");
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ParseQueryString();
                BindControls();

                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();

                    SetNames();
                }
            }

            //enter button
            Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                txtName.Focus();
            }

            AddScripts();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"));

                    rwError.VisibleOnPageLoad = true;
                }
                else
                    Response.Redirect("~/Forms/AmenityGroups/AmenityGroupsList.aspx");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_AmenityGroup"), String.IsNullOrEmpty(txtName.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtName.Text);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/AmenityGroups/AmenityGroupsList.aspx");
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/AmenityGroups/AmenityGroupsList.aspx");
        }

        #endregion
    }
}