﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Appliances.ascx.cs" Inherits="IntranetApp.Controls.Units.Appliances" %>



<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgAppliances">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgAppliances" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<telerik:RadGrid
    Width="550px"
    AutoGenerateColumns="False" 
    ID="rgAppliances" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="10" 
    DataSourceID="appliancesLinqDS"
	ClientSettings-EnablePostBackOnRowClick="false" 
    OnItemCommand="rgAppliances_ItemCommand" 
    OnDataBound="rgAppliances_DataBound"
    OnItemDataBound="rgAppliances_ItemDataBound"
    OnUpdateCommand="rgAppliances_UpdateCommand"
    OnInsertCommand="rgAppliances_InsertCommand"
    OnPreRender="rgAppliances_PreRender" >
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Fixed" DataKeyNames="UnitApplianceId" DataSourceID="appliancesLinqDS" CommandItemDisplay="Bottom" >
		<Columns>
            <telerik:GridBoundColumn DataField="Appliance.ApplianceGroup.GroupNameCurrentLanguage" FilterControlWidth="150px" UniqueName="Appliance.ApplianceGroup.GroupNameCurrentLanguage" HeaderText="Appliance Group" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Appliance.NameCurrentLanguage" FilterControlWidth="150px" UniqueName="Appliance.NameCurrentLanguage" HeaderText="Appliance" SortExpression="Appliance.NameCurrentLanguage" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Appliance.Quantity" FilterControlWidth="50px" UniqueName="Appliance.Quantity" HeaderText="Quantity" SortExpression="Appliance.Quantity" HeaderStyle-Width="50px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridButtonColumn>		
		</Columns>
        <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="Appliance"
                        HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                        EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblApplianceGroup" meta:resourceKey="lblApplianceGroup"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadComboBox ID="rcbApplianceGroup" runat="server" OnPreRender="rcbApplianceGroup_PreRender" OnSelectedIndexChanged="rcbApplianceGroup_SelectedIndexChanged" AutoPostBack="true"></telerik:RadComboBox>
                                <asp:RequiredFieldValidator ValidationGroup="Appliance" ID="rfvNameProductParam"
                                    Text="*" ControlToValidate="rcbApplianceGroup" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValApplianceGroup"></asp:RequiredFieldValidator>
                            </td>
                        </tr> 
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblAppliance" meta:resourceKey="lblAppliance"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadComboBox ID="rcbAppliance" runat="server" OnPreRender="rcbAppliance_PreRender" ></telerik:RadComboBox>
                                <asp:RequiredFieldValidator ValidationGroup="Appliance" ID="RequiredFieldValidator1"
                                    Text="*" ControlToValidate="rcbApplianceGroup" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValAppliance"></asp:RequiredFieldValidator>
                            </td>
                        </tr>        
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblQuantity" meta:resourceKey="lblQuantity"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadNumericTextBox ID="rntbQuantity" runat="server" NumberFormat-DecimalDigits="0" OnPreRender="rntbQuantity_PreRender" Type="Number" Width="150px"  />
                            </td>
                        </tr>                           
                        <tr>
                            <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                <div style="position: relative; width: 415px;">
                                    <!-- workaround for issue with RadButtons not being aligned vertically -->
                                    <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="Appliance"
                                        VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        CausesValidation="true">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                    </telerik:RadButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<otpDS:SimpleDataSource ID="appliancesLinqDS" runat="server" />