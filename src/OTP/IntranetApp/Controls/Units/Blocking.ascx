﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Blocking.ascx.cs" Inherits="IntranetApp.Controls.Units.Blocking" %>


<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgBlockages">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgBlockages" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<div runat="server" id="scriptBlock">
    <script type="text/javascript">
        function onBlockDateSelected(sender, args) {
            var dateField = $('#<%=hfSelectedDateFrom.ClientID%>');
            dateField.val(sender.get_selectedDate());
        }
        function onBlockUntilDatePopupOpening(sender, args) {
            var dateField = $('#<%=hfSelectedDateFrom.ClientID%>');
            if (dateField.val() != '') {
                var dateFrom = new Date(dateField.val());
                sender.set_focusedDate(dateFrom);
            }
            else {
                sender.set_focusedDate(new Date());
            }
        }
    </script>
</div>

<asp:Label runat="server" ID="lblNotEditable" meta:resourceKey="lblNotEditable" />
<br /><br />

<telerik:RadGrid
    Width="550px"
    AutoGenerateColumns="False" 
    ID="rgBlockages" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="blockingsLinqDS"
	ClientSettings-EnablePostBackOnRowClick="false" 
    OnItemCommand="rgBlockings_ItemCommand" 
    OnDataBound="rgBlockings_DataBound"
    OnItemDataBound="rgBlockings_ItemDataBound"
    OnUpdateCommand="rgBlockings_UpdateCommand"
    OnInsertCommand="rgBlockings_InsertCommand" >
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Fixed" DataKeyNames="UnitInvBlockingID" DataSourceID="blockingsLinqDS" CommandItemDisplay="Bottom" >
		<Columns>
            <%--<telerik:GridEditCommandColumn UniqueName="EditCommandColumn" HeaderText=" " HeaderStyle-Width="50px" >
                    <ItemStyle Width="50px"></ItemStyle>
                </telerik:GridEditCommandColumn>--%>
            <telerik:GridBoundColumn meta:resourceKey="colDateFrom" DataField="DateFrom" FilterControlWidth="150px" UniqueName="DateFrom" SortExpression="DateFrom" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn  meta:resourceKey="colDateUntil" DataField="DateUntil" FilterControlWidth="150px" UniqueName="DateUntil" SortExpression="DateUntil" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
			<telerik:GridBoundColumn  meta:resourceKey="colType" DataField="Type" FilterControlWidth="150px" UniqueName="Type" SortExpression="Type" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridButtonColumn>	
		</Columns>
        <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="Blockings"
                        HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                        EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDateFrom" meta:resourceKey="lblDateFrom"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadDatePicker ID="rcDateFrom" runat="server" SelectedDate='<%# (Eval("DateFrom") != null && Eval("DateFrom") is DateTime) ? Convert.ToDateTime(Eval("DateFrom")) : (DateTime?)null %>'>
                                    <DateInput 
                                        DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    <ClientEvents OnDateSelected="onBlockDateSelected" />
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ValidationGroup="Blockings" ID="rfvNameProductParam"
                                    Text="*" ControlToValidate="rcDateFrom" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDateFrom"></asp:RequiredFieldValidator>
                                 <asp:CustomValidator 
                                    ID="cvDateFrom"
                                    ValidationGroup="Blockings"
                                    Display="Dynamic"
                                    runat="server"
                                    OnServerValidate="cvInvalidPeriod_ServerValidate"
                                    meta:resourcekey="ValPeriodOverlaps"
                                    ControlToValidate="rcDateFrom"
                                    Text="*"
                                    CssClass="error" />
                            </td>
                        </tr> 
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDateUntil" meta:resourceKey="lblDateUntil"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadDatePicker ID="rcDateUntil" runat="server"  SelectedDate='<%# (Eval("DateUntil") != null && Eval("DateUntil") is DateTime) ? Convert.ToDateTime(Eval("DateUntil")) : (DateTime?)null %>'>
                                    <DateInput 
                                        DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                    />             
                                    <ClientEvents OnPopupOpening="onBlockUntilDatePopupOpening" />                       
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ValidationGroup="Blockings" ID="RequiredFieldValidator1"
                                    Text="*" ControlToValidate="rcDateUntil" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDateFrom"></asp:RequiredFieldValidator>
                                <br /><asp:CompareValidator ID="cmpEndDate" runat="server"
                                    meta:resourceKey="ValDates" CssClass="error" ValidationGroup="Blockings"
                                    ControlToCompare="rcDateFrom" ControlToValidate="rcDateUntil"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                        </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblType" meta:resourceKey="lblType"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadComboBox ID="rcbType" runat="server" OnPreRender="rcbType_PreRender"/>
                            </td>
                        </tr>                                   
                        <tr>
                            <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                <div style="position: relative; width: 415px;">
                                    <!-- workaround for issue with RadButtons not being aligned vertically -->
                                    <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="Blockings"
                                        VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        CausesValidation="true">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                    </telerik:RadButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<asp:HiddenField ID="hfSelectedDateFrom" runat="server" />

<otpDS:SimpleDataSource ID="blockingsLinqDS" runat="server" />