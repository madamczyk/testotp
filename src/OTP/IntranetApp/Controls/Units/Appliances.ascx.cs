﻿using BusinessLogic.Managers;
using Common;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Units
{
    public partial class Appliances : System.Web.UI.UserControl
    {

        #region Fields

        public int? UnitId
        {
            get { return ViewState["UnitIdAppliances"] as int?; }
            set { ViewState["UnitIdAppliances"] = value; }
        }

        public int? SelectedAppliancyGroup
        {
            get { return ViewState["SelectedAppliancyGroupAppliances"] as int?; }
            set { ViewState["SelectedAppliancyGroupAppliances"] = value; }
        }

        /// <summary>
        /// List will contain all the Discounts for the destination. Will be used for displaying the list to the user
        /// </summary>
        public List<UnitAppliancy> ElementsList
        {
            get { return ViewState["UnitAppliancyElementsList"] as List<UnitAppliancy>; }
            set { ViewState["UnitAppliancyElementsList"] = value; }
        }

        /// <summary>
        /// List will contain UnitAppliancys, that were added by the user. UnitAppliancys from this list should be added to the DB
        /// </summary>
        public List<UnitAppliancy> NewElements
        {
            get { return ViewState["UnitAppliancyNewElements"] as List<UnitAppliancy>; }
            set { ViewState["UnitAppliancyNewElements"] = value; }
        }

        /// <summary>
        /// List will contain UnitAppliancys, that were modified by the user and did not exist in the DB before - we don't need to update taxes, that do not exist in the DB
        /// </summary>
        public List<UnitAppliancy> ModifiedElementsList
        {
            get { return ViewState["UnitAppliancyModifiedElementsList"] as List<UnitAppliancy>; }
            set { ViewState["UnitAppliancyModifiedElementsList"] = value; }
        }

        /// <summary>
        /// List will containt IDs of taxes that were deleted by the user. UnitAppliancys from this list should be deleted from the DB
        /// </summary>
        public List<int> RemovedElementsList
        {
            get { return ViewState["UnitAppliancyRemovedElementsList"] as List<int>; }
            set { ViewState["UnitAppliancyRemovedElementsList"] = value; }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.appliancesLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.UnitAppliancy> result = ElementsList.AsQueryable();
                return result.ToList();
            };
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgAppliances_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int id = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitApplianceId"].ToString());
                ElementsList.RemoveAll(t => t.UnitApplianceId == id); // remove deleted tax from the list of taxes
                ModifiedElementsList.RemoveAll(t => t.UnitApplianceId == id); // remove deleted tax from the list of modified taxes
                if (NewElements.RemoveAll(t => t.UnitApplianceId == id) == 0) // remove deleted tax from the list of new taxes. if number of deleted taxes equals 0, it means, that tax existed in the db and we need to delete it
                    RemovedElementsList.Add(id); // add id of tax to the list of removed taxes

                rgAppliances.MasterTableView.Rebind();
            }
            else if (e.CommandName == "Cancel")
            {
            }
        }

        protected void rgAppliances_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                UnitAppliancy obj = (UnitAppliancy)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];

                item["Appliance.ApplianceGroup.GroupNameCurrentLanguage"].Text = obj.Appliance.ApplianceGroup.GroupNameCurrentLanguage;

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgAppliances.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Appliance"), obj.Appliance.Name.ToString()), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Appliance"), obj.Appliance.Name.ToString()));
            }
        }

        protected void rgAppliances_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgAppliances.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgAppliances.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgAppliances.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgAppliances.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgAppliances.MasterTableView.Rebind();
            }
        }

        protected void rgAppliances_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    DataAccess.UnitAppliancy appliance = new DataAccess.UnitAppliancy();
                    ReadGridItemData(args.Item, appliance);
                    appliance.UnitApplianceId = ElementsList.Count > 0 ? ElementsList.Max(t => t.UnitApplianceId) + 1 : 1;
                    ElementsList.Add(appliance);
                    NewElements.Add(appliance);
                    this.rgAppliances.MasterTableView.Rebind();
                }
            }
        }

        protected void rgAppliances_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    DataAccess.UnitAppliancy unit = null;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("UnitApplianceId"));
                    unit = ElementsList.Where(t => t.UnitApplianceId == id).SingleOrDefault();

                    ReadGridItemData(editForm, unit);

                    if (NewElements.Where(t => t.UnitApplianceId == id).Count() == 0) // if tax is not in the list of newly added
                    {
                        ModifiedElementsList.Add(unit); // make sure to update its data
                    }
                    else
                    {
                        NewElements.RemoveAll(t => t.UnitApplianceId == id);
                        NewElements.Add(unit);
                    }
                    this.rgAppliances.MasterTableView.Rebind();
                }
            }
        }

        private void ReadGridItemData(GridItem item, DataAccess.UnitAppliancy ua)
        {
            ua.Appliance = RequestManager.Services.AppliancesService.GetApplianceById(int.Parse((item.FindControl("rcbAppliance") as RadComboBox).SelectedValue));

            if (item.FindControl("rntbQuantity") is RadNumericTextBox)
            {
                int quantity;
                string quantityString = (item.FindControl("rntbQuantity") as RadNumericTextBox).Text;

                bool quantityHasText = !string.IsNullOrWhiteSpace(quantityString);
                bool quantityIsInteger = int.TryParse(quantityString, out quantity);
                bool applianceNotNull = ua.Appliance != null;

                if (applianceNotNull)
                {
                    if (quantityHasText && quantityIsInteger)
                    {
                        ua.Quantity = quantity;
                    }
                    else
                    {
                        ua.Quantity = null;
                    }
                }
            }
        }

        protected void rcbApplianceGroup_PreRender(object sender, EventArgs e)
        {
            RadComboBox groups = sender as RadComboBox;

            if (groups != null)
            {
                if (groups.Items.Count == 0)
                {
                    var applianceGroups = RequestManager.Services.AppliancesService.GetApplianceGroups();

                    SelectedAppliancyGroup = null;

                    foreach (var appliance in applianceGroups)
                    {
                        if (ElementsList.Where(ap => ap.Appliance.ApplianceGroup.ApplianceGroupId == appliance.ApplianceGroupId).Count() < appliance.Appliances.Count)
                            groups.Items.Add(new RadComboBoxItem(appliance.GroupNameCurrentLanguage, appliance.ApplianceGroupId.ToString()));
                    }

                    if (groups.Items.Count > 0)
                    {
                        SelectedAppliancyGroup = int.Parse(groups.Items.First().Value);
                        groups.SelectedValue = SelectedAppliancyGroup.Value.ToString();
                    }
                }
            }

        }

        protected void rcbAppliance_PreRender(object sender, EventArgs e)
        {
            RadComboBox appliancesComboBox = sender as RadComboBox;

            if (appliancesComboBox != null)
            {
                appliancesComboBox.ClearSelection();
                appliancesComboBox.Items.Clear();

                if (SelectedAppliancyGroup.HasValue)
                {
                    var appliances = RequestManager.Services.AppliancesService.GetApplianceByGroupId(SelectedAppliancyGroup.Value);

                    foreach (var appliance in appliances)
                    {
                        if (!ElementsList.Any(ap => ap.Appliance.ApplianceId == appliance.ApplianceId))
                            appliancesComboBox.Items.Add(new RadComboBoxItem(appliance.NameCurrentLanguage, appliance.ApplianceId.ToString()));
                    }
                }
            }
        }


        protected void rcbApplianceGroup_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            SelectedAppliancyGroup = int.Parse(e.Value);
        }

        protected void rgAppliances_PreRender(object sender, EventArgs e)
        {
            if (!UnitId.HasValue)
                return;

            var allApliances = RequestManager.Services.AppliancesService.GetAppliances();
            bool notRelatedAppliancesPresent = allApliances.Where(a => ElementsList.All(ua => ua.Appliance.ApplianceId != a.ApplianceId)).Count() > 0;

            if (rgAppliances.MasterTableView.GetItems(GridItemType.CommandItem).Length > 0)
            {
                GridCommandItem commandItem = rgAppliances.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem;
                Control button;

                button = commandItem.FindControl("AddNewRecordButton");
                if (button != null)
                    button.Visible = notRelatedAppliancesPresent;

                button = commandItem.FindControl("InitInsertButton");
                if (button != null)
                    button.Visible = notRelatedAppliancesPresent;
            }
        }

        protected void rntbQuantity_PreRender(object sender, EventArgs e)
        {
            RadNumericTextBox textBox = sender as RadNumericTextBox;

            if (textBox != null)
            {
                textBox.Text = string.Empty;
            }
        }

        #endregion Event Handlers

    }
}