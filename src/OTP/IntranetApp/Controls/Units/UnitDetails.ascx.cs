﻿using BusinessLogic.Managers;
using Common.Converters;
using DataAccess;
using DataAccess.Enums;
using IntranetApp.Controls.Common;
using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.UI;
using Telerik.Web.UI;

namespace IntranetApp.Controls.Units
{
    public partial class UnitDetails : System.Web.UI.UserControl
    {
        #region Fields

        public int? LockBoxStrategy
        {
            get { return ViewState["LockBoxStrategy"] as int?; }
            set { ViewState["LockBoxStrategy"] = value; }
        }

        private int? PropertyId
        {
            get { return ViewState["PropertyIdUnitDetails"] as int?; }
            set { ViewState["PropertyIdUnitDetails"] = value; }
        }

        public int? UnitId
        {
            get { return ViewState["UnitTypeIdUnitDetails"] as int?; }
            set { ViewState["UnitTypeIdUnitDetails"] = value; }
        }

        public FormMode Mode
        {
            get { return (FormMode)ViewState["ModeUnitDetails"]; }
            set { ViewState["ModeUnitDetails"] = (int)value; }
        }

        #endregion Fields

        #region Methods

        private void ParseQueryString()
        {
            int id = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out id))
            {
                if (Mode == FormMode.Edit)
                {
                    UnitId = id;
                    PropertyId = RequestManager.Services.UnitsService.GetUnitById(id).Property.PropertyID;
                }
                else
                {
                    PropertyId = id;
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["lockbox"]) && int.TryParse(Request.QueryString["lockbox"], out id))
            {
                LockBoxStrategy = id;
            }
        }

        private void LoadData()
        {
            Debug.Assert(this.UnitId.HasValue);

            DataAccess.Unit unit = RequestManager.Services.UnitsService.GetUnitById(this.UnitId.Value);

            //textboxes
            this.txtTitle.Text = unit.UnitTitleCurrentLanguage;
            this.txtCode.Text = unit.UnitCode;
            this.txtDescription.Text = unit.UnitDescCurrentLanguage;
            this.txtNoOfGuests.Text = unit.MaxNumberOfGuests.ToString();
            this.txtNoOfBaths.Text = unit.MaxNumberOfBathrooms.ToString();
            this.txtNoOfBedrooms.Text = unit.MaxNumberOfBedRooms.ToString();
            this.rcbCleaningStatus.SelectedValue = unit.CleaningStatus.ToString();
            this.rcbUnitType.SelectedValue = unit.UnitType.UnitTypeID.ToString();
            this.txtAlarmCode.Text = unit.AlarmCode;
            this.txtAlarmLocation.Text = unit.AlarmLocation;
        }

        private bool SaveData()
        {
            DataAccess.Unit unit = null;

            if (this.Mode == FormMode.Add)
            {
                unit = new DataAccess.Unit();
            }
            else
            {
                unit = RequestManager.Services.UnitsService.GetUnitById(this.UnitId.Value);
                unit.AlarmCode = null;
                unit.AlarmLocation = null;
            }

            if (unit == null)
            {
                return false;
            }

            unit.UnitType = RequestManager.Services.UnitTypesService.GetUnitTypeById(int.Parse(this.rcbUnitType.SelectedValue));
            unit.SetTitleValue(this.txtTitle.Text);
            unit.SetDescValue(this.txtDescription.Text);
            unit.UnitCode = this.txtCode.Text;
            unit.MaxNumberOfGuests = int.Parse(this.txtNoOfGuests.Text);
            unit.MaxNumberOfBathrooms = int.Parse(this.txtNoOfBaths.Text);
            unit.MaxNumberOfBedRooms = int.Parse(this.txtNoOfBedrooms.Text);
            unit.CleaningStatus = int.Parse(this.rcbCleaningStatus.SelectedValue);
            unit.Property = RequestManager.Services.PropertiesService.GetPropertyById(PropertyId.Value);
            if (Mode == FormMode.Add)
            {
                RequestManager.Services.UnitsService.AddUnit(unit);
                RequestManager.Services.SaveChanges();
            }

            #region Unit Rates
            foreach (var id in ctrlRates.RemovedElementsList)
                RequestManager.Services.UnitsService.DeleteUnitRate(id);

            List<UnitRate>.Enumerator RateListEnumerator = ctrlRates.NewElements.GetEnumerator();
            while (RateListEnumerator.MoveNext())
            {
                RateListEnumerator.Current.Unit = unit;
                RequestManager.Services.UnitsService.AddUnitRate(RateListEnumerator.Current);
            }
            UnitRate rate;
            foreach (var t in ctrlRates.ModifiedElementsList)
            {
                rate = RequestManager.Services.UnitsService.GetUnitRateById(t.UnitRateID);

                rate.DateFrom = t.DateFrom;
                rate.DateUntil = t.DateUntil;
                rate.DailyRate = t.DailyRate;
                rate.Unit = unit;
            }
            #endregion

            #region Unit Appliances

            foreach (var id in ctrlAppliances.RemovedElementsList)
                RequestManager.Services.UnitsService.DeleteUnitAppliance(id);

            List<UnitAppliancy>.Enumerator ApplianceListEnumerator = ctrlAppliances.NewElements.GetEnumerator();
            while (ApplianceListEnumerator.MoveNext())
            {
                ApplianceListEnumerator.Current.Unit = unit;
                ApplianceListEnumerator.Current.Appliance = RequestManager.Services.AppliancesService.GetApplianceById(ApplianceListEnumerator.Current.Appliance.ApplianceId);
                RequestManager.Services.UnitsService.AddUnitAppliance(ApplianceListEnumerator.Current);
            }
            UnitAppliancy ua;
            foreach (var t in ctrlAppliances.ModifiedElementsList)
            {
                ua = RequestManager.Services.UnitsService.GetUnitApplianceById(t.UnitApplianceId);

                ua.Appliance = RequestManager.Services.AppliancesService.GetApplianceById(t.Appliance.ApplianceId);
                ua.Unit = unit;
            }
            #endregion

            #region Blockings

            foreach (var id in ctrlBlockings.RemovedElementsList)
                RequestManager.Services.UnitsService.DeleteUnitBlockage(id);

            List<UnitInvBlocking>.Enumerator BlockingsListEnumerator = ctrlBlockings.NewElements.GetEnumerator();
            while (BlockingsListEnumerator.MoveNext())
            {
                BlockingsListEnumerator.Current.Unit = unit;
                RequestManager.Services.UnitsService.AddUnitBlockade(BlockingsListEnumerator.Current);
            }
            UnitInvBlocking bl;
            foreach (var t in ctrlBlockings.ModifiedElementsList)
            {
                bl = RequestManager.Services.UnitsService.GetUnitBlockageById(t.UnitInvBlockingID);

                bl.DateFrom = t.DateFrom;
                bl.DateUntil = t.DateUntil;
                bl.Reservation = t.Reservation;
                bl.Type = t.Type;
                bl.Unit = unit;
            }
            #endregion

            #region StaticContent
            SaveToDB(ctrlKitchen, unit);
            SaveToDB(ctrlLiving, unit);
            SaveToDB(ctrlSCBaths, unit);
            SaveToDB(ctrlSCBedrooms, unit);

            #endregion

            #region Wifi Data

            bool addNew = false;
            if (!string.IsNullOrWhiteSpace(this.ctrlWifiData.AccessPointName))
            {
                UnitStaticContent usc = null;
                if (this.UnitId.HasValue)
                    usc = RequestManager.Services.UnitsService.GetUnitStaticContentByType(this.UnitId.Value, UnitStaticContentType.WifiName).FirstOrDefault();
                if (usc == null)
                {
                    addNew = true;
                    usc = new UnitStaticContent();
                }
                usc.ContentCode = (int)UnitStaticContentType.WifiName;
                usc.ContentValue = this.ctrlWifiData.AccessPointName;
                usc.Unit = unit;
                if (addNew)
                    RequestManager.Services.UnitsService.AddUnitStaticContent(usc);
            }
            else
            {
                UnitStaticContent usc = null;
                if (this.UnitId.HasValue)
                    usc = RequestManager.Services.UnitsService.GetUnitStaticContentByType(this.UnitId.Value, UnitStaticContentType.WifiName).SingleOrDefault();
                if (usc != null)
                {
                    RequestManager.Services.UnitsService.RemoveStaticContent(usc.UnitStaticContentId);
                }
            }
            if (!string.IsNullOrWhiteSpace(this.ctrlWifiData.Password))
            {
                UnitStaticContent usc = null;
                if (this.UnitId.HasValue)
                    usc = RequestManager.Services.UnitsService.GetUnitStaticContentByType(this.UnitId.Value, UnitStaticContentType.WifiPassword).FirstOrDefault();
                if (usc == null)
                {
                    addNew = true;
                    usc = new UnitStaticContent();
                }
                usc.ContentCode = (int)UnitStaticContentType.WifiPassword;
                usc.ContentValue = Base64.Encode(this.ctrlWifiData.Password);
                usc.Unit = unit;
                if (addNew)
                    RequestManager.Services.UnitsService.AddUnitStaticContent(usc);
            }
            else
            {
                UnitStaticContent usc = null;
                if (this.UnitId.HasValue)
                    usc = RequestManager.Services.UnitsService.GetUnitStaticContentByType(this.UnitId.Value, UnitStaticContentType.WifiPassword).SingleOrDefault(); ;
                if (usc != null)
                {
                    RequestManager.Services.UnitsService.RemoveStaticContent(usc.UnitStaticContentId);
                }
            }

            #endregion

            #region Lockbox data
            if (LockBoxStrategy == 1)
            {
                unit.KeysLockboxCode = this.txtLockBoxCode.Text;
                unit.KeysLockboxLocation = this.txtLockBoxLocation.Text;

                if (!string.IsNullOrWhiteSpace(this.txtAlarmCode.Text))
                {
                    unit.AlarmCode = this.txtAlarmCode.Text;
                }

                if (!string.IsNullOrWhiteSpace(this.txtAlarmLocation.Text))
                {
                    unit.AlarmLocation = this.txtAlarmLocation.Text;
                }

                unit.OtherAccessInformation = this.txtAccessInfo.Text;
            }
            #endregion

            RequestManager.Services.SaveChanges();

            return true;
        }

        private void SaveToDB(UnitStaticContentControl ctrl, Unit unit)
        {
            foreach (var id in ctrl.RemovedElementsList)
                RequestManager.Services.UnitsService.RemoveStaticContent(id);

            List<UnitStaticContent>.Enumerator ListEnumerator = ctrl.NewElements.GetEnumerator();
            while (ListEnumerator.MoveNext())
            {
                ListEnumerator.Current.Unit = unit;
                RequestManager.Services.UnitsService.AddUnitStaticContent(ListEnumerator.Current);
            }
            UnitStaticContent sc;
            foreach (var t in ctrl.ModifiedElementsList)
            {
                sc = RequestManager.Services.UnitsService.GetUnitStaticContentById(t.UnitStaticContentId);
                sc.ContentCode = t.ContentCode;
                sc.ContentValue = t.ContentValue;
                sc.SetContentValue(t.ContentValueCurrentLanguage);
                sc.Unit = unit;
            }
        }

        private bool CheckExists()
        {
            if (UnitId.HasValue &&  RequestManager.Services.UnitsService.GetUnitById(this.UnitId.Value) == null)
            {
                rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Unit"));
                lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_DoesntExist_Text"), (string)GetGlobalResourceObject("Controls", "Object_Unit"));

                rwError.VisibleOnPageLoad = true;

                return false;
            }

            return true;
        }

        private void BindControls()
        {
            foreach (UnitCleaningStatus en in Enum.GetValues(typeof(UnitCleaningStatus)))
            {
                this.rcbCleaningStatus.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(en), ((int)en).ToString()));
            }

            IEnumerable<DataAccess.UnitType> unitTypes = RequestManager.Services.UnitTypesService.GetUnitTypesByPropertyId(this.PropertyId.Value);
            foreach (DataAccess.UnitType ut in unitTypes)
            {
                rcbUnitType.Items.Add(new RadComboBoxItem(ut.UnitTypeTitleCurrentLanguage, ut.UnitTypeID.ToString()));
            }
            
            #region Unit Rates
            ctrlRates.ElementsList = new List<UnitRate>();
            ctrlRates.RemovedElementsList = new List<int>();
            ctrlRates.NewElements = new List<UnitRate>();
            ctrlRates.ModifiedElementsList = new List<UnitRate>();
            #endregion 

            #region Unit Appliances
            ctrlAppliances.ElementsList = new List<UnitAppliancy>();
            ctrlAppliances.RemovedElementsList = new List<int>();
            ctrlAppliances.NewElements = new List<UnitAppliancy>();
            ctrlAppliances.ModifiedElementsList = new List<UnitAppliancy>();
            #endregion 

            #region Blockings
            ctrlBlockings.ElementsList = new List<UnitInvBlocking>();
            ctrlBlockings.RemovedElementsList = new List<int>();
            ctrlBlockings.NewElements = new List<UnitInvBlocking>();
            ctrlBlockings.ModifiedElementsList = new List<UnitInvBlocking>();
            #endregion

            #region static content
            ctrlKitchen.ElementsList = new List<UnitStaticContent>();
            ctrlKitchen.RemovedElementsList = new List<int>();
            ctrlKitchen.NewElements = new List<UnitStaticContent>();
            ctrlKitchen.ModifiedElementsList = new List<UnitStaticContent>();

            ctrlLiving.ElementsList = new List<UnitStaticContent>();
            ctrlLiving.RemovedElementsList = new List<int>();
            ctrlLiving.NewElements = new List<UnitStaticContent>();
            ctrlLiving.ModifiedElementsList = new List<UnitStaticContent>();

            ctrlSCBaths.ElementsList = new List<UnitStaticContent>();
            ctrlSCBaths.RemovedElementsList = new List<int>();
            ctrlSCBaths.NewElements = new List<UnitStaticContent>();
            ctrlSCBaths.ModifiedElementsList = new List<UnitStaticContent>();

            ctrlSCBedrooms.ElementsList = new List<UnitStaticContent>();
            ctrlSCBedrooms.RemovedElementsList = new List<int>();
            ctrlSCBedrooms.NewElements = new List<UnitStaticContent>();
            ctrlSCBedrooms.ModifiedElementsList = new List<UnitStaticContent>();           

            #endregion

            this.ctrlKitchen.StaticContentInfo = GetLocalResourceObject("lblKitchen").ToString() + GetLocalResourceObject("lblKitchenExamples").ToString();
            this.ctrlLiving.StaticContentInfo = GetLocalResourceObject("lblLivingAreas").ToString() + GetLocalResourceObject("lblLivingAreasExamples").ToString();
            this.ctrlSCBaths.StaticContentInfo = GetLocalResourceObject("lblBathrooms").ToString() + GetLocalResourceObject("lblBathroomsExamples").ToString();
            this.ctrlSCBedrooms.StaticContentInfo = GetLocalResourceObject("lblBedrooms").ToString() + GetLocalResourceObject("lblBedroomsExamples").ToString();
            this.ctrlKitchen.ContentType = UnitStaticContentType.KitchenAndLaundry;
            this.ctrlLiving.ContentType = UnitStaticContentType.LivingAreasAndDining;
            this.ctrlSCBaths.ContentType = UnitStaticContentType.Bathrooms;
            this.ctrlSCBedrooms.ContentType = UnitStaticContentType.Bedrooms;

            var property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyId.Value);
            if (property != null)
            {
                if(LockBoxStrategy == 1)
                //if (property.KeyProcessType == (int)KeyProcessType.Lockbox)
                {
                    tableLockboxStrategy.Visible = true;
                }
            }
            this.ctrlRates.PropertyId = PropertyId.Value;
            if (Mode == FormMode.Edit)
            {
                DataAccess.Unit unit = RequestManager.Services.UnitsService.GetUnitById(this.UnitId.Value);
                
                //lockbox data
                this.txtLockBoxCode.Text = unit.KeysLockboxCode;
                this.txtLockBoxLocation.Text = unit.KeysLockboxLocation;
                this.txtAlarmCode.Text = unit.AlarmCode;
                this.txtAlarmLocation.Text = unit.AlarmLocation;
                this.txtAccessInfo.Text = unit.OtherAccessInformation;
                
                ////grid views
                this.ctrlKitchen.UnitID = unit.UnitID;
                this.ctrlLiving.UnitID = unit.UnitID;
                this.ctrlSCBaths.UnitID = unit.UnitID;
                this.ctrlSCBedrooms.UnitID = unit.UnitID;

                //daily rates
                this.ctrlRates.UnitId = unit.UnitID;
                

                //appliances
                this.ctrlAppliances.UnitId = unit.UnitID;

                ////blockings
                this.ctrlBlockings.UnitId = unit.UnitID;

                #region Unit Rates

                UnitRate rate = null;
                var discounts = RequestManager.Services.UnitsService.GetUnitRates(this.UnitId.Value);
                foreach (var t in discounts)
                {
                    rate = new UnitRate();
                    rate.DateFrom = t.DateFrom;
                    rate.DateUntil = t.DateUntil;
                    rate.DailyRate = t.DailyRate;
                    rate.Unit = unit;
                    rate.UnitRateID = t.UnitRateID;

                    ctrlRates.ElementsList.Add(rate);
                }

                #endregion

                #region Unit Appliances

                UnitAppliancy unitAppliances = null;
                var appliances = RequestManager.Services.UnitsService.GetUnitApplianciesByUnitId(this.UnitId.Value);
                var applianceGroups = RequestManager.Services.AppliancesService.GetApplianceGroups().AsQueryable();
                foreach (var t in appliances)
                {
                    unitAppliances = new UnitAppliancy();
                    unitAppliances.Appliance = RequestManager.Services.AppliancesService.GetApplianceById(t.Appliance.ApplianceId);
                    unitAppliances.Appliance.ApplianceGroup = applianceGroups.Where(g => g.ApplianceGroupId == t.Appliance.ApplianceGroup.ApplianceGroupId).FirstOrDefault();
                    unitAppliances.Unit = unit;
                    unitAppliances.UnitApplianceId = t.UnitApplianceId;

                    ctrlAppliances.ElementsList.Add(unitAppliances);
                }

                #endregion

                #region Blockings

                UnitInvBlocking blocking = null;
                var blocks = RequestManager.Services.UnitsService.GetUnitBlockings(this.UnitId.Value);
                foreach (var t in blocks)
                {
                    blocking = new UnitInvBlocking();
                    blocking.DateFrom = t.DateFrom;
                    blocking.DateUntil = t.DateUntil;
                    blocking.Unit = unit;
                    blocking.Type = t.Type;
                    blocking.Reservation = t.Reservation;
                    blocking.UnitInvBlockingID = t.UnitInvBlockingID;

                    ctrlBlockings.ElementsList.Add(blocking);
                }

                #endregion
                SetStaticContent(ctrlSCBedrooms, unit);
                SetStaticContent(ctrlSCBaths, unit);
                SetStaticContent(ctrlLiving, unit);
                SetStaticContent(ctrlKitchen, unit);

                #region Wifi Data

                var wifiName = RequestManager.Services.UnitsService.GetUnitStaticContentByType(this.UnitId.Value, UnitStaticContentType.WifiName);
                var wifiPassword = RequestManager.Services.UnitsService.GetUnitStaticContentByType(this.UnitId.Value, UnitStaticContentType.WifiPassword);

                if (wifiName.SingleOrDefault() != null)
                {
                    ctrlWifiData.AccessPointName = wifiName.SingleOrDefault().ContentValue;
                }
                if (wifiPassword.SingleOrDefault() != null)
                {
                    ctrlWifiData.Password = Base64.Decode(wifiPassword.SingleOrDefault().ContentValue);
                }
                #endregion
            }
        }

        private void SetStaticContent(UnitStaticContentControl ctrl, Unit unit)
        {
            UnitStaticContent usc = null;
            var blocks = RequestManager.Services.UnitsService.GetUnitStaticContentByType(this.UnitId.Value, ctrl.ContentType);
            foreach (var t in blocks)
            {
                usc = new UnitStaticContent();
                usc.ContentCode = t.ContentCode;
                usc.ContentValue = t.ContentValue;
                usc.ContentValue_i18n = t.ContentValue_i18n;
                usc.ContentValueCurrentLanguage = t.ContentValueCurrentLanguage;
                usc.SetContentValue(usc.ContentValueCurrentLanguage);
                usc.Unit = unit;
                usc.UnitStaticContentId = t.UnitStaticContentId;

                ctrl.ElementsList.Add(usc);
            }
        }

        private void SetNames()
        {
            if (this.Mode == FormMode.Edit)
            {
                Unit unit = RequestManager.Services.UnitsService.GetUnitById(this.UnitId.Value);

                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Unit"), unit.UnitTitle.ToString());
                lblHeader.Text = String.Format((string)GetLocalResourceObject("lblHeaderEdit.Text"), txtCode.Text, unit.Property.PropertyNameCurrentLanguage);
            }  
        }

        #endregion Methods

        #region Events Handling

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.ParseQueryString();
                this.BindControls();
                if (CheckExists())
                {
                    if (Mode == FormMode.Edit)
                        LoadData();
                    SetNames();
                }                
            }
            //enter button
            this.Page.Form.DefaultButton = btnSave.UniqueID;
            if (!IsPostBack)
            {
                txtTitle.Focus();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!SaveData())
                {
                    rwError.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Unit"));
                    lblError.Text = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_NoLongerExists_Edit_Text"), (string)GetGlobalResourceObject("Controls", "Object_Unit"));

                    rwError.VisibleOnPageLoad = true;
                }
                string script = "<script language=JavaScript> window.parent.$('#modalDialog').dialog('close').remove(); </script>";
                Response.Write(script);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Mode == FormMode.Add)
                rwConfirm.Title = String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Unit"), String.IsNullOrEmpty(txtTitle.Text) ? (string)GetGlobalResourceObject("Controls", "Dialog_Discard_Caption_New") : txtTitle.Text);

            rwConfirm.VisibleOnPageLoad = true;
        }

        protected void btnDiscard_Yes(object sender, EventArgs e)
        {
            string script = "<script language=JavaScript> window.parent.$('#modalDialog').dialog('close').remove(); </script>";
            Response.Write(script);
        }

        protected void btnDiscard_No(object sender, EventArgs e)
        {
            rwConfirm.VisibleOnPageLoad = false;
        }

        protected void btnError_Ok(object sender, EventArgs e)
        {
            string script = "<script language=JavaScript> window.parent.$('#modalDialog').dialog('close').remove(); </script>";
            Response.Write(script);
        }

        protected void cvUnitCode_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            args.IsValid = false;
            var p = RequestManager.Services.UnitsService.GetUnitForPropertyByCode(this.PropertyId.Value, this.txtCode.Text);
            if (p == null)
                args.IsValid = true;
            else if (Mode == FormMode.Edit)
            {
                if (p.UnitID == this.UnitId.Value)
                    args.IsValid = true;
            }
        }

        #endregion        
    }
}