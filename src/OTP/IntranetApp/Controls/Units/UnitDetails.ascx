﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UnitDetails.ascx.cs" Inherits="IntranetApp.Controls.Units.UnitDetails" %>
<%@ Register TagPrefix="uc" TagName="StaticContent" Src="~/Controls/Common/UnitStaticContentControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="Rates" Src="~/Controls/Units/Rates.ascx" %>
<%@ Register TagPrefix="uc" TagName="Appliances" Src="~/Controls/Units/Appliances.ascx" %>
<%@ Register TagPrefix="uc" TagName="Blockings" Src="~/Controls/Units/Blocking.ascx" %>
<%@ Register TagPrefix="uc" TagName="WifiData" Src="~/Controls/Units/WifiData.ascx" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="pnlData">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlData" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
        <telerik:RadWindow ID="rwConfirm" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblConfirmation" Font-Bold="true" Text="<%$ Resources: Controls, Dialog_Discard_Text %>" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="wndBtnDiscard_Yes" runat="server" Text="<%$ Resources: Controls, Dialog_Yes %>" OnClick="btnDiscard_Yes"></asp:Button>
                        <asp:Button ID="wndBtnDiscard_No" runat="server" Text="<%$ Resources: Controls, Dialog_No %>" OnClick="btnDiscard_No"></asp:Button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
        <telerik:RadWindow ID="rwError" runat="server" VisibleOnPageLoad="false" Height="140px" Behaviors="None" Modal="true" VisibleStatusbar="false">
            <ContentTemplate>
                <div style="margin: 20px;">
                    <div style="float: left; width: 240px; text-align: center;">
                        <asp:Label ID="lblError" Font-Bold="true" runat="server" />
                        <br />
                        <br />
                        <asp:Button ID="wndBtnError_Ok" runat="server" Text="<%$ Resources: Controls, Dialog_Ok %>" OnClick="btnError_Ok" />
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<div id="mainPanel">

    <asp:Panel runat="server" ID="pnlData">
        <asp:ValidationSummary ID="vsSummary" ValidationGroup="Unit" HeaderText=" "
            DisplayMode="BulletList" EnableClientScript="true" runat="server" CssClass="errorInForm" />
        <h1 class="FormHeader">
            <asp:Label runat="server" ID="lblHeader" meta:resourcekey="lblHeaderAdd"></asp:Label>
        </h1>
    

        <table>
            <tr style="height: 20px;">
                <td></td>
            </tr>
            <tr>
                <td>
                    <telerik:RadTabStrip ID="rtsPropertyDetails" runat="server"
                        SelectedIndex="0" MultiPageID="rmpTabContent">
                        <Tabs>
                            <telerik:RadTab meta:resourcekey="tabBasicData"/>
                            <telerik:RadTab meta:resourcekey="tabRates"/>
                            <telerik:RadTab meta:resourcekey="tabAppliances"/>
                            <telerik:RadTab meta:resourcekey="tabBlockings"/>
                            <telerik:RadTab meta:resourcekey="tabStaticContet"/>
                        </Tabs>
                    </telerik:RadTabStrip>

                    <telerik:RadMultiPage ID="rmpTabContent" runat="server" SelectedIndex="0">

    <telerik:RadPageView ID="RadPageView1" runat="server" CssClass="radTabSmall">
        <table>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblTitle" meta:resourcekey="lblTitle" />
                </td>
                <td>
                    <telerik:RadTextBox runat="server" TabIndex="1" ID="txtTitle" Width="250px" />
                    <asp:RequiredFieldValidator
                        ID="rfvName"
                        ValidationGroup="Unit"
                        runat="server"
                        Display="Dynamic"
                        ControlToValidate="txtTitle"
                        meta:resourcekey="ValTitle"
                        Text="*"
                        CssClass="error" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblUnitType" meta:resourceKey="lblUnitType"></asp:Label>
                </td>
                <td>
                    <telerik:RadComboBox Width="220px" runat="server" ID="rcbUnitType"/>
                    <asp:RequiredFieldValidator ValidationGroup="Unit" ID="rfvNameProductParam"
                        Text="*" ControlToValidate="rcbUnitType" runat="server" Display="Dynamic"
                        CssClass="error" meta:resourceKey="ValUnitType"></asp:RequiredFieldValidator>
                </td>
            </tr> 
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblCode" meta:resourcekey="lblCode" />
                </td>
                <td>
                    <telerik:RadTextBox runat="server" TabIndex="1" ID="txtCode" Enabled="true" Width="250px" />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator1"
                        ValidationGroup="Unit"
                        runat="server"
                        Display="Dynamic"
                        ControlToValidate="txtCode"
                        meta:resourcekey="ValCode"
                        Text="*"
                        CssClass="error" />
                    <asp:CustomValidator
                        ID="cvUnitCode" 
                        ValidationGroup="Unit" 
                        runat="server" 
                        Display="Dynamic"                    
                        meta:resourcekey="ValUnitCode" 
                        ControlToValidate="txtCode"
                        ValidateEmptyText="false"
                        EnableClientScript="true"
                        Enabled="true"
                        Text="*" 
                        OnServerValidate="cvUnitCode_ServerValidate"
                        CssClass="error" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblNoOfGuests" meta:resourcekey="lblNoOfGuests" />
                </td>
                <td>
                    <telerik:RadNumericTextBox AllowOutOfRangeAutoCorrect="false" runat="server" TabIndex="1" ID="txtNoOfGuests" Enabled="true" Width="250px" MinValue="1" MaxValue="100" NumberFormat-DecimalDigits="0" />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator3"
                        ValidationGroup="Unit"
                        runat="server"
                        Display="Dynamic"
                        ControlToValidate="txtNoOfGuests"
                        meta:resourcekey="ValNoOfGuests"
                        Text="*"
                        CssClass="error" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblNoOfBaths" meta:resourcekey="lblNoOfBaths" />
                </td>
                <td>
                    <telerik:RadNumericTextBox AllowOutOfRangeAutoCorrect="false" runat="server" TabIndex="1" ID="txtNoOfBaths" Enabled="true" Width="250px" MinValue="1" MaxValue="100" NumberFormat-DecimalDigits="0" />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator4"
                        ValidationGroup="Unit"
                        runat="server"
                        Display="Dynamic"
                        ControlToValidate="txtNoOfBaths"
                        meta:resourcekey="ValNoOfBaths"
                        Text="*"
                        CssClass="error" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblNoOfBedrooms" meta:resourcekey="lblNoOfBedrooms" />
                </td>
                <td>
                    <telerik:RadNumericTextBox AllowOutOfRangeAutoCorrect="false" runat="server" TabIndex="1" ID="txtNoOfBedrooms" Enabled="true" Width="250px" MinValue="1" MaxValue="100" NumberFormat-DecimalDigits="0" />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator5"
                        ValidationGroup="Unit"
                        runat="server"
                        Display="Dynamic"
                        ControlToValidate="txtNoOfBedrooms"
                        meta:resourcekey="ValNoOfBedrooms"
                        Text="*"
                        CssClass="error" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDescription" meta:resourcekey="lblDescription" />
                </td>
                <td>
                    <telerik:RadTextBox runat="server" TabIndex="1" ID="txtDescription" Enabled="true" Width="250px" TextMode="MultiLine" Rows="1" />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator2"
                        ValidationGroup="Unit"
                        runat="server"
                        Display="Dynamic"
                        ControlToValidate="txtDescription"
                        meta:resourcekey="ValDescription"
                        Text="*"
                        CssClass="error" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblCleaningStatus" meta:resourcekey="lblCleaningStatus" />
                </td>
                <td>
                    <telerik:RadComboBox ID="rcbCleaningStatus" runat="server" />
                </td>
            </tr>
        </table>
         <table runat="server" id="tableLockboxStrategy" visible="false">
            <tr>
                <td>
                    <asp:Label runat="server" ID="LblLockboxCode" meta:resourcekey="LblLockboxCode" />
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtLockBoxCode" MaxLength="20" Width="250px"/>
                     <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator6"
                        ValidationGroup="Unit"
                        runat="server"
                        Display="Dynamic"
                        ControlToValidate="txtLockBoxCode"
                        meta:resourcekey="ValLockBoxCode"
                        Text="*"
                        CssClass="error" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblLockboxLocation" meta:resourcekey="lblLockboxLocation" />
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtLockBoxLocation" MaxLength="100" TextMode="MultiLine" Rows="1" Width="250px"/>
                     <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator7"
                        ValidationGroup="Unit"
                        runat="server"
                        Display="Dynamic"
                        ControlToValidate="txtLockBoxLocation"
                        meta:resourcekey="ValLockBoxLocation"
                        Text="*"
                        CssClass="error" />
                </td>
            </tr>
            <tr>
                 <td>
                    <asp:Label runat="server" ID="lblAlarmCode" meta:resourcekey="lblAlarmCode" />
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtAlarmCode" MaxLength="50" Width="250px"/>
                </td>
            </tr>
             <tr>
                 <td>
                    <asp:Label runat="server" ID="lblAlarmLocation" meta:resourcekey="lblAlarmLocation" />
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtAlarmLocation" MaxLength="300" TextMode="MultiLine" Rows="1" Width="250px"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblAccessInfo" meta:resourcekey="lblAccessInfo" />
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtAccessInfo" MaxLength="300" TextMode="MultiLine" Rows="1" Width="250px"/>
                </td>
            </tr>

        </table>
                        </telerik:RadPageView>

                        <telerik:RadPageView ID="rpvContacts" runat="server" CssClass="radTabSmall">
                                <uc:Rates id="ctrlRates" runat="server" />
                        </telerik:RadPageView>

                        <telerik:RadPageView ID="RadPageView122" runat="server" CssClass="radTabSmall">
                            <uc:Appliances id="ctrlAppliances" runat="server" />
                        </telerik:RadPageView>

                        <telerik:RadPageView ID="RadPageView2" runat="server" CssClass="radTabSmall">
                            <uc:Blockings id="ctrlBlockings" runat="server" />
                        </telerik:RadPageView>

                       <telerik:RadPageView ID="RadPageView3" runat="server" CssClass="radTabSmall">
                            <telerik:RadTabStrip ID="RadTabStrip1" runat="server"
                                SelectedIndex="0" MultiPageID="RadMultiPage1">
                                <Tabs>
                                    <telerik:RadTab meta:resourcekey="tabBedrooms"/>
                                    <telerik:RadTab meta:resourcekey="tabBathrooms"/>
                                    <telerik:RadTab meta:resourcekey="tabLivingAreas"/>
                                    <telerik:RadTab meta:resourcekey="tabKitchen"/>
                                    <telerik:RadTab meta:resourcekey="tabWifiData"/>
                                </Tabs>
                            </telerik:RadTabStrip>

                            <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
                                <telerik:RadPageView ID="RadPageView4" runat="server" CssClass="radTabSmall2">
                                    <uc:StaticContent id="ctrlSCBedrooms" runat="server" />
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="RadPageView5" runat="server" CssClass="radTabSmall2">
                                    <uc:StaticContent id="ctrlSCBaths" runat="server" />
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="RadPageView6" runat="server" CssClass="radTabSmall2">
                                    <uc:StaticContent id="ctrlLiving" runat="server" />
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="RadPageView7" runat="server" CssClass="radTabSmall2">
                                    <uc:StaticContent id="ctrlKitchen" runat="server" />
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="RadPageView8" runat="server" CssClass="radTabSmall2">
                                    <uc:WifiData id="ctrlWifiData" runat="server" />
                                </telerik:RadPageView>
                            </telerik:RadMultiPage>

                        </telerik:RadPageView>

                    </telerik:RadMultiPage>


                </td>
            </tr>

        </table>
    </asp:Panel>

    <table>
        <tr style="height: 20px;">
            <td></td>
        </tr>
        <tr>
            <td width="645px" align="right" style="padding-right: 3px">
                <telerik:RadButton runat="server" ID="btnSave" TabIndex="4" Text="<%$ Resources: Controls, Button_Save %>" CausesValidation="true" ValidationGroup="Unit" OnClick="btnSave_Click" />
                <telerik:RadButton runat="server" ID="btnCancel" TabIndex="5" Text="<%$ Resources: Controls, Button_Cancel %>" CausesValidation="false" OnClick="btnCancel_Click" />
            </td>
        </tr>
    </table>

</div>
