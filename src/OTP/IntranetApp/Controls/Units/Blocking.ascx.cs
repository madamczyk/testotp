﻿using BusinessLogic.Managers;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Common.CultureInfo;
using DataAccess;
using Common.Extensions;

namespace IntranetApp.Controls.Units
{
    public partial class Blocking : System.Web.UI.UserControl
    {

        #region Fields

        public int? UnitId
        {
            get { return ViewState["UnitIdBlocking"] as int?; }
            set { ViewState["UnitIdBlocking"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemIdBlocking"] as int?; }
            set { ViewState["EditedItemIdBlocking"] = value; }
        }

        /// <summary>
        /// List will contain all the Discounts for the destination. Will be used for displaying the list to the user
        /// </summary>
        public List<UnitInvBlocking> ElementsList
        {
            get { return ViewState["UnitInvBlockingElementsList"] as List<UnitInvBlocking>; }
            set { ViewState["UnitInvBlockingElementsList"] = value; }
        }

        /// <summary>
        /// List will contain UnitInvBlockings, that were added by the user. UnitInvBlockings from this list should be added to the DB
        /// </summary>
        public List<UnitInvBlocking> NewElements
        {
            get { return ViewState["UnitInvBlockingNewElements"] as List<UnitInvBlocking>; }
            set { ViewState["UnitInvBlockingNewElements"] = value; }
        }

        /// <summary>
        /// List will contain UnitInvBlockings, that were modified by the user and did not exist in the DB before - we don't need to update taxes, that do not exist in the DB
        /// </summary>
        public List<UnitInvBlocking> ModifiedElementsList
        {
            get { return ViewState["UnitInvBlockingModifiedElementsList"] as List<UnitInvBlocking>; }
            set { ViewState["UnitInvBlockingModifiedElementsList"] = value; }
        }

        /// <summary>
        /// List will containt IDs of taxes that were deleted by the user. UnitInvBlockings from this list should be deleted from the DB
        /// </summary>
        public List<int> RemovedElementsList
        {
            get { return ViewState["UnitInvBlockingRemovedElementsList"] as List<int>; }
            set { ViewState["UnitInvBlockingRemovedElementsList"] = value; }
        }
        
        #endregion

        #region Methods

        private void BindGrid()
        {
            this.blockingsLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.UnitInvBlocking> result = ElementsList.AsQueryable();
                return result.ToList();
            };
        }
        
        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void rgBlockings_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int id = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitInvBlockingID"].ToString());
                ElementsList.RemoveAll(t => t.UnitInvBlockingID == id); // remove deleted tax from the list of taxes
                ModifiedElementsList.RemoveAll(t => t.UnitInvBlockingID == id); // remove deleted tax from the list of modified taxes
                if (NewElements.RemoveAll(t => t.UnitInvBlockingID == id) == 0) // remove deleted tax from the list of new taxes. if number of deleted taxes equals 0, it means, that tax existed in the db and we need to delete it
                    RemovedElementsList.Add(id); // add id of tax to the list of removed taxes
            }
            else if (e.CommandName == "Edit")
            {
                int editedId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitInvBlockingID"].ToString());
                EditedItemId = editedId;
            }
            else if (e.CommandName == "Cancel")
            {
                EditedItemId = null;
            }
        }

        protected void rgBlockings_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                UnitInvBlocking obj = (UnitInvBlocking)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                string title = String.Format("{0} - {1}, {2}", obj.DateFrom.ToLocalizedDateString(), obj.DateUntil.ToLocalizedDateString(), EnumConverter.GetValue((UnitBlockingType)obj.Type));

                item["DateFrom"].Text = obj.DateFrom.ToLocalizedDateString();
                item["DateUntil"].Text = obj.DateUntil.ToLocalizedDateString();
                item["Type"].Text = EnumConverter.GetValue((UnitBlockingType)obj.Type);
                
                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgBlockages.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Blocking"), title), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Blocking"), title));
            }
        }

        protected void rgBlockings_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgBlockages.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgBlockages.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgBlockages.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgBlockages.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgBlockages.MasterTableView.Rebind();
            }
        }

        protected void rgBlockings_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    DataAccess.UnitInvBlocking newUnit = new DataAccess.UnitInvBlocking();
                    ReadGridItemData(args.Item, newUnit);
                    newUnit.UnitInvBlockingID = ElementsList.Count > 0 ? ElementsList.Max(t => t.UnitInvBlockingID) + 1 : 1;
                    ElementsList.Add(newUnit);
                    NewElements.Add(newUnit);
                    this.rgBlockages.MasterTableView.Rebind();
                }
            }
        }

        protected void rgBlockings_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    DataAccess.UnitInvBlocking unit = null;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("UnitInvBlockingID"));
                    unit = ElementsList.Where(t => t.UnitInvBlockingID == id).SingleOrDefault();

                    ReadGridItemData(editForm, unit);

                    if (NewElements.Where(t => t.UnitInvBlockingID == id).Count() == 0) // if tax is not in the list of newly added
                    {
                        ModifiedElementsList.Add(unit); // make sure to update its data
                    }
                    else
                    {
                        NewElements.RemoveAll(t => t.UnitInvBlockingID == id);
                        NewElements.Add(unit);
                    }
                    this.rgBlockages.MasterTableView.Rebind();
                }
                int reportId = int.Parse(args.Item.OwnerTableView.DataKeyValues[args.Item.ItemIndex]["UnitInvBlockingID"].ToString());
                EditedItemId = reportId;
            }
        }

        private void ReadGridItemData(GridItem item, DataAccess.UnitInvBlocking unit)
        {
            unit.DateFrom = (item.FindControl("rcDateFrom") as RadDatePicker).SelectedDate.Value;
            unit.DateUntil = (item.FindControl("rcDateUntil") as RadDatePicker).SelectedDate.Value;
            unit.Type = int.Parse((item.FindControl("rcbType") as RadComboBox).SelectedValue);
            unit.Reservation = null;
        }

        protected void rcbType_PreRender(object sender, EventArgs e)
        {
            RadComboBox ddl = sender as RadComboBox;            
            foreach (UnitBlockingType type in Enum.GetValues(typeof(UnitBlockingType)))
            {
                if (type != UnitBlockingType.OwnerStay)
                    continue;
                ddl.Items.Add(new RadComboBoxItem(EnumConverter.GetValue(type), ((int)type).ToString()));
            }
            if (EditedItemId.HasValue && EditedItemId.Value != 0)
            {
                DataAccess.UnitInvBlocking ublock = ElementsList.Where(el => el.UnitInvBlockingID == EditedItemId.Value).SingleOrDefault();
                ddl.SelectedValue = ublock.Type.ToString();
            }
        }

        protected void cvInvalidPeriod_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool valid = true;
            DateTime DateFrom = DateTime.Parse(args.Value);
            DateTime? DateUntil = ((RadDatePicker)((CustomValidator)source).FindControl("rcDateUntil")).SelectedDate;
            IQueryable<UnitInvBlocking> result = ElementsList == null ? new List<UnitInvBlocking>().AsQueryable() : ElementsList.AsQueryable();

            if (!EditedItemId.HasValue)
            {
                if (result.Any(o => o.DateFrom <= DateFrom && o.DateUntil >= DateFrom)) // does DateFrom fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.DateFrom <= DateUntil && o.DateUntil >= DateUntil)) // does DateUntil fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.DateFrom >= DateFrom && o.DateUntil <= DateUntil))// does DateFrom - DateUntil contain any other period?
                    valid = false;
            }
            else
            {
                if (result.Any(o => o.UnitInvBlockingID != EditedItemId.Value && o.DateFrom <= DateFrom && o.DateUntil >= DateFrom)) // does DateFrom fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.UnitInvBlockingID != EditedItemId.Value && o.DateFrom <= DateUntil && o.DateUntil >= DateUntil)) // does DateUntil fall within range of any other period?
                    valid = false;
                else if (DateUntil.HasValue && result.Any(o => o.UnitInvBlockingID != EditedItemId.Value && o.DateFrom >= DateFrom && o.DateUntil <= DateUntil))// does DateFrom - DateUntil contain any other period?
                    valid = false;
            }

            args.IsValid = valid;
        }

        #endregion Event Handlers
    }
}