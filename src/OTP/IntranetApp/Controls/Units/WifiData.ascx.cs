﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Controls.Units
{
    public partial class WifiData : System.Web.UI.UserControl
    {
        public string AccessPointName
        {
            get { return this.txtAPName.Text; }
            set { this.txtAPName.Text = value; }
        }

        public string Password
        {
            get { return this.txtPassword.Text; }
            set { this.txtPassword.Text = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}