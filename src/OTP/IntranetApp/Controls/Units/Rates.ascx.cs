﻿using BusinessLogic.Managers;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Common.CultureInfo;
using DataAccess;
using Common.Extensions;

namespace IntranetApp.Controls.Units
{
    public partial class Rates : System.Web.UI.UserControl
    {

        #region Fields

        public int? PropertyId
        {
            get { return ViewState["PropertyId"] as int?; }
            set { ViewState["PropertyId"] = value; }
        }

        public int? UnitId
        {
            get { return ViewState["UnitIdRates"] as int?; }
            set { ViewState["UnitIdRates"] = value; }
        }

        public int? EditedItemId
        {
            get { return ViewState["EditedItemIdRates"] as int?; }
            set { ViewState["EditedItemIdRates"] = value; }
        }

        /// <summary>
        /// List will contain all the Discounts for the destination. Will be used for displaying the list to the user
        /// </summary>
        public List<UnitRate> ElementsList
        {
            get { return ViewState["UnitRateElementsList"] as List<UnitRate>; }
            set { ViewState["UnitRateElementsList"] = value; }
        }

        /// <summary>
        /// List will contain UnitRates, that were added by the user. UnitRates from this list should be added to the DB
        /// </summary>
        public List<UnitRate> NewElements
        {
            get { return ViewState["UnitRateNewElements"] as List<UnitRate>; }
            set { ViewState["UnitRateNewElements"] = value; }
        }

        /// <summary>
        /// List will contain UnitRates, that were modified by the user and did not exist in the DB before - we don't need to update taxes, that do not exist in the DB
        /// </summary>
        public List<UnitRate> ModifiedElementsList
        {
            get { return ViewState["UnitRateModifiedElementsList"] as List<UnitRate>; }
            set { ViewState["UnitRateModifiedElementsList"] = value; }
        }

        /// <summary>
        /// List will containt IDs of taxes that were deleted by the user. UnitRates from this list should be deleted from the DB
        /// </summary>
        public List<int> RemovedElementsList
        {
            get { return ViewState["UnitRateRemovedElementsList"] as List<int>; }
            set { ViewState["UnitRateRemovedElementsList"] = value; }
        }

        private Property property;

        #endregion

        #region Methods

        private void BindGrid()
        {
            this.ratesLinqDS.DataResolver = () =>
            {
                IQueryable<DataAccess.UnitRate> result = ElementsList.AsQueryable();
                return result.ToList();
            };
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
            property = RequestManager.Services.PropertiesService.GetPropertyById(this.PropertyId.Value);
        }

        protected void rgRates_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                e.Item.OwnerTableView.OwnerGrid.HandleCustomFiltering(e);
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                int id = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitRateID"].ToString());
                ElementsList.RemoveAll(t => t.UnitRateID == id); // remove deleted tax from the list of taxes
                ModifiedElementsList.RemoveAll(t => t.UnitRateID == id); // remove deleted tax from the list of modified taxes
                if (NewElements.RemoveAll(t => t.UnitRateID == id) == 0) // remove deleted tax from the list of new taxes. if number of deleted taxes equals 0, it means, that tax existed in the db and we need to delete it
                    RemovedElementsList.Add(id); // add id of tax to the list of removed taxes
            }
            else if (e.CommandName == "Edit")
            {
                int editedId = int.Parse(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UnitRateID"].ToString());
                EditedItemId = editedId;
            }
            else if (e.CommandName == "Cancel")
            {
                EditedItemId = null;
            }
        }

        protected void rgRates_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                UnitRate obj = (UnitRate)item.DataItem;
                ImageButton image = (ImageButton)item["DeleteColumn"].Controls[0];
                string title = String.Format("{0} - {1}, {2}", obj.DateFrom.ToLocalizedDateString(), obj.DateUntil.ToLocalizedDateString(), obj.DailyRate.ToString("c", property.DictionaryCulture.CultureInfo));

                item["DateFrom"].Text = obj.DateFrom.ToLocalizedDateString();
                item["DateUntil"].Text = obj.DateUntil.ToLocalizedDateString();
                item["DailyRate"].Text = obj.DailyRate.ToString("c", property.DictionaryCulture.CultureInfo);

                image.Attributes["onClick"] = String.Format("if(!$find('{0}').confirm('{1}', event, '{2}'))return false;", rgRates.ClientID, String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Text"), (string)GetGlobalResourceObject("Controls", "Object_Rate"), title), String.Format((string)GetGlobalResourceObject("Controls", "Dialog_Delete_Caption"), (string)GetGlobalResourceObject("Controls", "Object_Rate"), title));
            }
        }

        protected void rgRates_DataBound(object sender, EventArgs args)
        {
            //current itemCount in grid
            int currentPageItemCount = this.rgRates.MasterTableView.VirtualItemCount;

            //max pageCount
            double? maxPageCount = System.Convert.ToDouble(currentPageItemCount) / System.Convert.ToDouble(rgRates.PageSize);
            maxPageCount = Math.Floor(maxPageCount.Value + 1) - 1;

            //if current index page > max Page Count
            if (this.rgRates.MasterTableView.CurrentPageIndex > maxPageCount)
            {
                this.rgRates.MasterTableView.CurrentPageIndex = (int)maxPageCount;
                this.rgRates.MasterTableView.Rebind();
            }
        }

        protected void rgRates_InsertCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.PerformInsertCommandName)
            {
                if (args.Item is GridEditFormInsertItem)
                {
                    DataAccess.UnitRate newUnit = new DataAccess.UnitRate();
                    ReadGridItemData(args.Item, newUnit);
                    newUnit.UnitRateID = ElementsList.Count > 0 ? ElementsList.Max(t => t.UnitRateID) + 1 : 1;
                    ElementsList.Add(newUnit);
                    NewElements.Add(newUnit);
                    this.rgRates.MasterTableView.Rebind();
                }
            }
        }

        protected void rgRates_UpdateCommand(object sender, GridCommandEventArgs args)
        {
            if (args.CommandName == RadGrid.UpdateCommandName)
            {
                if (args.Item is GridEditFormItem)
                {
                    GridEditFormItem editForm = (GridEditFormItem)args.Item;
                    DataAccess.UnitRate unit = null;
                    int id = Convert.ToInt32(editForm.GetDataKeyValue("UnitRateID"));
                    unit = ElementsList.Where(t => t.UnitRateID == id).SingleOrDefault();

                    ReadGridItemData(editForm, unit);

                    if (NewElements.Where(t => t.UnitRateID == id).Count() == 0) // if tax is not in the list of newly added
                    {
                        ModifiedElementsList.Add(unit); // make sure to update its data
                    }
                    else
                    {
                        NewElements.RemoveAll(t => t.UnitRateID == id);
                        NewElements.Add(unit);
                    }
                    this.rgRates.MasterTableView.Rebind();
                }
                int reportId = int.Parse(args.Item.OwnerTableView.DataKeyValues[args.Item.ItemIndex]["UnitRateID"].ToString());
                EditedItemId = reportId;
            }
        }

        private void ReadGridItemData(GridItem item, DataAccess.UnitRate unit)
        {
            unit.DateFrom = (item.FindControl("rcDateFrom") as RadDatePicker).SelectedDate.Value;
            unit.DateUntil = (item.FindControl("rcDateUntil") as RadDatePicker).SelectedDate.Value;
            unit.DailyRate = decimal.Parse((item.FindControl("txtRate") as RadTextBox).Text);
        }

        protected void RequiredFieldValidator_PreRender(object sender, EventArgs e)
        {
            RegularExpressionValidator regVal = sender as RegularExpressionValidator;
            string exp = @"^\d{1,8}(\<NumberSeparator>\d{0,2})?";
            regVal.ValidationExpression = exp.Replace("<NumberSeparator>", CurrentCultureInfoSet.NumberSeparator);
        }

        protected void CustomValidator_ValidateDataRange(object sender, ServerValidateEventArgs  e)
        {
            CustomValidator custValidator = (CustomValidator)sender;
            GridEditableItem urItem = (GridEditableItem)custValidator.NamingContainer;

            DateTime rangeFrom = (urItem.FindControl("rcDateFrom") as RadDatePicker).SelectedDate.Value;
            DateTime rangeUntil = (urItem.FindControl("rcDateUntil") as RadDatePicker).SelectedDate.Value;

            var unitRates = ElementsList == null ? new List<UnitRate>().AsQueryable() : ElementsList.AsQueryable();
            foreach (var unitRate in unitRates)
            {
                if (this.rgRates.EditIndexes.Count > 0)
                {
                    var unitRateDataKey = Convert.ToInt32(urItem.GetDataKeyValue("UnitRateID"));
                    if (unitRateDataKey > 0 && Convert.ToUInt32(unitRateDataKey) == unitRate.UnitRateID)
                        continue;
                }

                if ((rangeFrom <= unitRate.DateUntil) && (rangeUntil >= unitRate.DateFrom))
                {
                    e.IsValid = false;
                    return;
                }
            }

            e.IsValid = true;
        }

        #endregion Event Handlers
    }
}