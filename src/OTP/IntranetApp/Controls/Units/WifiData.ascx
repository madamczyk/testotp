﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WifiData.ascx.cs" Inherits="IntranetApp.Controls.Units.WifiData" %>

<table>
    <tr>
        <td width="100px">
            <asp:Label runat="server" ID="lblAPNme" meta:resourcekey="lblAPNme" />
        </td>
        <td width="500px">
            <telerik:RadTextBox runat="server" ID="txtAPName" TextMode="SingleLine" Width="250px" />
            <asp:RequiredFieldValidator
                Text="*"
                ControlToValidate="txtAPName"
                ID="rfvAPName"
                runat="server"
                ValidationGroup="Property"
                CssClass="error"
                Display="Dynamic"
                meta:resourcekey="ValAPName"
                Enabled="false"
                />
        </td>
    </tr>
    <tr>
        <td width="100px">
            <asp:Label runat="server" ID="lblPassword" meta:resourcekey="lblPassword" />
        </td>
        <td width="500px">
            <telerik:RadTextBox runat="server" ID="txtPassword" TextMode="SingleLine" Width="250px" />
            <asp:RequiredFieldValidator
                Text="*"
                ControlToValidate="txtPassword"
                ID="RequiredFieldValidator1"
                runat="server"
                ValidationGroup="Property"
                CssClass="error"
                Display="Dynamic"  
                meta:resourcekey="ValPassword"
                Enabled="false"
                />
        </td>
    </tr>
</table>