﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Rates.ascx.cs" Inherits="IntranetApp.Controls.Units.Rates" %>


<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgRates">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgRates" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>

<div runat="server" id="scriptBlock">
    <script type="text/javascript">
        function onRatesDateSelected(sender, args) {
            var dateField = $('#<%=hfSelectedDateFrom.ClientID%>');
            dateField.val(sender.get_selectedDate());
        }
        function onRatesUntilDatePopupOpening(sender, args) {
            var dateField = $('#<%=hfSelectedDateFrom.ClientID%>');
            if (dateField.val() != '') {
                var dateFrom = new Date(dateField.val());
                sender.set_focusedDate(dateFrom);
            }
            else {
                sender.set_focusedDate(new Date());
            }
        }
    </script>
</div>

<telerik:RadGrid
    Width="550px"
    AutoGenerateColumns="False" 
    ID="rgRates" 
    AllowSorting="True" 
    runat="server" 
    AllowFilteringByColumn="False" 
    AllowPaging="True"
    PageSize="20" 
    DataSourceID="ratesLinqDS"
	ClientSettings-EnablePostBackOnRowClick="true" 
    OnItemCommand="rgRates_ItemCommand" 
    OnDataBound="rgRates_DataBound"
    OnItemDataBound="rgRates_ItemDataBound"
    OnUpdateCommand="rgRates_UpdateCommand"
    OnInsertCommand="rgRates_InsertCommand" >
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>
	<GroupingSettings CaseSensitive="false" />
	<MasterTableView TableLayout="Fixed" DataKeyNames="UnitRateID" DataSourceID="ratesLinqDS" CommandItemDisplay="Bottom" >
		<Columns>
            <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" HeaderText=" " HeaderStyle-Width="50px" >
                    <ItemStyle Width="50px"></ItemStyle>
                </telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn DataField="DateFrom" FilterControlWidth="150px" UniqueName="DateFrom" HeaderText="Date From" SortExpression="DateFrom" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DateUntil" FilterControlWidth="150px" UniqueName="DateUntil" HeaderText="Date Until" SortExpression="DateUntil" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="DailyRate" FilterControlWidth="150px" UniqueName="DailyRate" HeaderText="Daily Rate" SortExpression="DailyRate" HeaderStyle-Width="300px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
				<HeaderStyle Width="120px"></HeaderStyle>
				<ItemStyle CssClass="link" />
			</telerik:GridBoundColumn>
            <telerik:GridButtonColumn CommandName="Delete" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete.png">
                <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridButtonColumn>		
		</Columns>
        <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <asp:ValidationSummary ID="vsProductParamsSummary" ValidationGroup="Discount"
                        HeaderText="<%$ Resources: Validation, ValidationSummary %>" DisplayMode="BulletList"
                        EnableClientScript="true" runat="server" CssClass="errorInForm" />
                    <table runat="server" id="tblData" cellspacing="1" cellpadding="1" border="0" class="module">
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDateFrom" meta:resourceKey="lblDateFrom"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadDatePicker ID="rcDateFrom" runat="server" SelectedDate='<%# (Eval("DateFrom") != null && Eval("DateFrom") is DateTime) ? Convert.ToDateTime(Eval("DateFrom")) : (DateTime?)null %>'>
                                    <DateInput 
                                        DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                                     <ClientEvents OnDateSelected="onRatesDateSelected" />
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ValidationGroup="Discount" ID="rfvNameProductParam"
                                    Text="*" ControlToValidate="rcDateFrom" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDateFrom"></asp:RequiredFieldValidator>

                                <asp:CustomValidator runat="server" 
                                        ID="CustomValidatorDataRange"
                                        ControlToValidate="rcDateFrom" 
                                        ValidationGroup="Discount" 
                                        OnServerValidate="CustomValidator_ValidateDataRange"
                                        CssClass="error"
                                        Display="None"
                                        ErrorMessage="<%$ resources:Validation, DataRangeOverlap %>" />
                            </td>
                        </tr> 
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDateUntil" meta:resourceKey="lblDateUntil"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadDatePicker ID="rcDateUntil" runat="server"  SelectedDate='<%# (Eval("DateUntil") != null && Eval("DateUntil") is DateTime) ? Convert.ToDateTime(Eval("DateUntil")) : (DateTime?)null %>'>
                                    <DateInput 
                                        DateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" 
                                        DisplayDateFormat="<%# Common.Helpers.DateTimeHelper.GetLocalizedDatePattern() %>" />
                                    <ClientEvents OnPopupOpening="onRatesUntilDatePopupOpening" /> 
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ValidationGroup="Discount" ID="RequiredFieldValidator1"
                                    Text="*" ControlToValidate="rcDateUntil" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValDateFrom"></asp:RequiredFieldValidator>
                                <br /><asp:CompareValidator ID="cmpEndDate" runat="server"
                                    meta:resourceKey="ValDates" CssClass="error" ValidationGroup="Discount"
                                    ControlToCompare="rcDateFrom" ControlToValidate="rcDateUntil"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                        </td>
                        </tr>
                        <tr>
                            <td class="paramLabelCell" style='padding-top: 10px'>
                                <asp:Label runat="server" ID="lblDiscount" meta:resourceKey="lblRate"></asp:Label>
                            </td>
                            <td class="paramInputCell" style='padding-top: 10px'>
                                <telerik:RadTextBox Width="220px" runat="server" ID="txtRate" MaxLength="50" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container.DataItem, "DailyRate", "{0:##0.00}")%>'>
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ValidationGroup="Discount" ID="RequiredFieldValidator2"
                                    Text="*" ControlToValidate="txtRate" runat="server" Display="Static"
                                    CssClass="error" meta:resourceKey="ValRate"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator3" 
                                    ValidationGroup="Discount"
                                    runat="server" 
                                    Display="Dynamic" 
                                    ControlToValidate="txtRate"                     
                                    meta:resourcekey="ValRateFormat" 
                                    Text="*" 
                                    CssClass="error" OnPreRender="RequiredFieldValidator_PreRender" />
                            </td>
                        </tr>                                   
                        <tr>
                            <td colspan="2" style='text-align: right; padding-top: 4px;'>
                                <div style="position: relative; width: 415px;">
                                    <!-- workaround for issue with RadButtons not being aligned vertically -->
                                    <telerik:RadButton CssClass="bottomAlignedButton" ValidationGroup="Discount"
                                        VerticalAlignment="Bottom" ID="btnProceed" runat="server" Text='<%# (Container is GridEditFormInsertItem) ? "Add" : "Save" %>'
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        CausesValidation="true">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel">
                                    </telerik:RadButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
		<PagerStyle AlwaysVisible="True"></PagerStyle>
	</MasterTableView>
	<HeaderContextMenu EnableImageSprites="True">
	</HeaderContextMenu>
</telerik:RadGrid>

<asp:HiddenField ID="hfSelectedDateFrom" runat="server" />

<otpDS:SimpleDataSource ID="ratesLinqDS" runat="server" />