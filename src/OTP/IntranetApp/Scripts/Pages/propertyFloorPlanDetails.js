﻿var oth = oth || {};

oth.propertyFloorPlanDetails = (function (oth, $) {

    //view model
    var ViewModel = function() {
        var self = this;

        //Properties
        self.imageFloorPlanMaxWidth = 640;
        self.imageFloorPlanMaxHeight = 480;
        self.imageMarkerWidth = 40;
        self.imageMarkerHeight = 40;

        self.imageDivContainterSelector = null;
        self.imageControlSelector = null;
        self.hfPanoramaCordXSelector = null;
        self.hfPanoramaCordYSelector = null;
        self.hfPanoramaSelectedIDSelector = null;
        self.btnShowPanoramaFormSelector = null;
        self.btnEditPanoramaFormSelector = null;
        self.btnEditNewPanoramaFormSelector = null;
        self.hfNewPanoramaSelectedIDSelector = null;

        self.controlMode = null;
        self.panoramas = null;
        self.editedPanorama = null;

        //Methods
        //view activation function
        self.init = function (options) {
            $.extend(self, options);

            switch (self.controlMode) {
                case "AddPanorama":
                    _initAddPanoramaMode();
                    break;
                case "Default":
                    _initDefaultMode();
                    break;
            }

            _createFloorPlanMarkers();
        }

        //private functions
        function _initDefaultMode() {
            $(self.imageControlSelector).click(function (event) {
                var currentPos = $(this).offset();
                var imageOffset =
                    {
                        'x': event.pageX - currentPos.left - (self.imageMarkerWidth / 2),
                        'y': event.pageY - currentPos.top - (self.imageMarkerHeight / 2)
                    };

                if (imageOffset.x < 0)
                    imageOffset.x = 0;
                if (imageOffset.y < 0)
                    imageOffset.y = 0;

                if (imageOffset.x > (self.imageFloorPlanMaxWidth - self.imageMarkerWidth))
                    imageOffset.x = self.imageFloorPlanMaxWidth - self.imageMarkerWidth;
                if (imageOffset.y > (self.imageFloorPlanMaxHeight - self.imageMarkerHeight))
                    imageOffset.y = self.imageFloorPlanMaxHeight - self.imageMarkerHeight;

                $(self.imageDivContainterSelector).append(_createMarker(imageOffset.x, imageOffset.y));

                $(self.hfPanoramaCordXSelector).val(imageOffset.x);
                $(self.hfPanoramaCordYSelector).val(imageOffset.y);
                $(self.btnShowPanoramaFormSelector).click();
            });

            $(self.imageControlSelector).css("cursor", "crosshair");
        }

        function _initAddPanoramaMode() {
            var newPanoramaMarker = _createMarker(parseInt($(self.hfPanoramaCordXSelector).val(), 10),
                                                 parseInt($(self.hfPanoramaCordYSelector).val(), 10))
            newPanoramaMarker.addClass("selected");
            newPanoramaMarker.css("cursor", "move");
            _makeMarkerDraggable(newPanoramaMarker);

            $(self.imageDivContainterSelector).append(newPanoramaMarker);
        }

        //renders all floor plan's markers
        function _createFloorPlanMarkers() {
            var selectedPanorama = parseInt($(self.hfPanoramaSelectedIDSelector).val(), 10);

            $.each(self.panoramas, function (index, panorama) {
                var marker = _createMarker(panorama.offsetX, panorama.offsetY);
                marker.attr("panoramaId", panorama.id);

                if (!isNaN(selectedPanorama) && panorama.id === selectedPanorama) {
                    marker.addClass("selected");
                    marker.css("cursor", "move");
                    self.editedPanorama = marker;
                    _makeMarkerDraggable(marker);
                }
                else {
                    $(marker).dblclick(function (event) {
                        $(self.hfPanoramaSelectedIDSelector).val($(this).attr("panoramaId"));
                        $(self.btnEditPanoramaFormSelector).click();
                    });
                    $(marker).click(function (event) {
                        if (self.editedPanorama != null && self.editedPanorama != marker)
                        {
                            self.editedPanorama.draggable("destroy");
                            self.editedPanorama.removeClass("selected");
                            self.editedPanorama.css("cursor", "pointer");

                            $(self.hfNewPanoramaSelectedIDSelector).val($(this).attr("panoramaId"));
                            $(self.btnEditNewPanoramaFormSelector).click();
                        }
                        $(self.hfPanoramaSelectedIDSelector).val($(this).attr("panoramaId"));
                        marker.attr("panoramaId");
                        //$(self.btnShowPanoramaFormSelector).click();
                        marker.addClass("selected");
                        marker.css("cursor", "move");
                        self.editedPanorama = marker;
                        _makeMarkerDraggable(marker);
                    });                    
                }

                $(self.imageDivContainterSelector).append(marker);
            });
        }

        //creates marker element for display on image
        function _createMarker(offsetX, offsetY) {
            var imageMarker = $("<div />");
            imageMarker.addClass("marker");

            imageMarker.css("cursor", "pointer");
            imageMarker.css("left", offsetX);
            imageMarker.css("top", offsetY);

            return imageMarker;
        }

        //applies draggable interface to marker element
        function _makeMarkerDraggable(element) {
            $(element).draggable(
            {
                containment: self.imageDivContainterSelector,
                scroll: false,
                cursor: 'move',
                zIndex: 150,
                drag: function (event, ui) {
                    _updateMarkerPosition(ui.position);
                }
            });
        }
        
        //updates selected marker position
        function _updateMarkerPosition(position) {
            $(self.hfPanoramaCordXSelector).val(position.left);
            $(self.hfPanoramaCordYSelector).val(position.top);
        }
        
    }
    
    return new ViewModel();

}(oth || {}, jQuery));