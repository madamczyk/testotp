﻿using IntranetApp.Helpers;
using System;
using System.Web;
using Telerik.Web.UI;
namespace IntranetApp.Handlers
{
    /// <summary>
    /// Summary description for CustomHandler
    /// </summary>
    public class CustomHandler : AsyncUploadHandler 
    {
        protected override IAsyncUploadResult Process(UploadedFile file, HttpContext context, IAsyncUploadConfiguration configuration, string tempFileName)
        {
            UploadResult upload = new UploadResult();
            int propertyId = int.Parse(context.Request.QueryString["pid"]);
            bool rawData = int.Parse(String.IsNullOrWhiteSpace(context.Request.QueryString["rawData"]) ? "0" : context.Request.QueryString["rawData"]) > 0;
            bool floorPlan = int.Parse(String.IsNullOrWhiteSpace(context.Request.QueryString["fp"]) ? "0" : context.Request.QueryString["fp"]) > 0;
            string encodedFileName = Common.UrlHelper.Encode(file.FileName);

            if (rawData)
                return upload.UploadRawData(encodedFileName, file.InputStream, file.ContentLength, file.ContentType, propertyId);

            if(floorPlan)
                return upload.UploadFloorPlanFile(encodedFileName, file.InputStream, file.ContentLength, file.ContentType, propertyId);  

            switch (file.GetExtension())
            {
                case ".zip":
                    return PropertyFloorPlanHelper.UploadPanorama(propertyId, file, encodedFileName);              
                default:
                    return upload.UploadFile(encodedFileName, file.InputStream, file.ContentLength, file.ContentType, propertyId);
            }
        }
    }
}