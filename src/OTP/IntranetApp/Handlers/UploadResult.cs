﻿using BusinessLogic.Enums;
using BusinessLogic.Managers;
using BusinessLogic.Services;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using Telerik.Web.UI;

namespace IntranetApp.Handlers
{
    public class UploadResult : IAsyncUploadResult
    {
        public int ContentLength
        {
            get;
            set;
        }

        public string ContentType
        {
            get;
            set;
        }

        public string FileName
        {
            get;
            set;
        }

        public string Extended
        {
            get;
            set;
        }

        public IAsyncUploadResult UploadRawData(string fileName, Stream fileContent, int contentLength, string contentType, int propertyId)
        {
            //save in azure blob as temp files
            RequestManager.Services.BlobService.InitContainer(RequestManager.Services.SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersPropertyDawData));

            if (RequestManager.Services.BlobService.FileExists(propertyId, fileName))
            {
                return new UploadResult()
                {
                    ContentLength = 0,
                    ContentType = contentType,
                    FileName = fileName
                };
            }

            fileName = RequestManager.Services.BlobService.AddFile(fileContent, propertyId, fileName);

            return new UploadResult()
            {
                ContentLength = contentLength,
                ContentType = contentType,
                FileName = fileName,
                Extended = String.Format("{0}/{1}", RequestManager.Services.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray()), fileName.TrimStart("/".ToCharArray()))
            };
        }

        public IAsyncUploadResult UploadFloorPlanFile(string fileName, Stream fileContent, int contentLength, string contentType, int propertyId)
        {
            //save in azure blob as temp files
            RequestManager.Services.BlobService.InitContainer(RequestManager.Services.SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersProperties));

            var content = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(propertyId,
                new List<PropertyStaticContentType>()
                {
                    PropertyStaticContentType.FloorPlan
                });
            if(content.Any(c => c.ContentValue.Contains(string.Format("/{0}", fileName))))
            {
                return new UploadResult()
                {
                    ContentLength = 0,
                    ContentType = contentType,
                    FileName = fileName,
                    Extended = string.Format("#{0}|{1}|{2}!", 0, 0, fileName)
                };
            }
            fileName = RequestManager.Services.BlobService.AddFile(fileContent, propertyId, fileName);
            
            Image img = Image.FromStream(fileContent);

            return new UploadResult()
            {
                ContentLength = contentLength,
                ContentType = contentType,
                FileName = fileName,
                Extended = string.Format("#{0}|{1}|{2}!", img.Width, img.Height, 
                string.Format("{0}/{1}", RequestManager.Services.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray()), fileName.TrimStart("/".ToCharArray())))
            };
        }

        public IAsyncUploadResult UploadFile(string fileName, Stream fileContent, int contentLength, string contentType, int propertyId)
        {
            //save in azure blob as temp files
            RequestManager.Services.BlobService.InitContainer(RequestManager.Services.SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersProperties));

            var content = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(propertyId,
                new List<PropertyStaticContentType>()
                {
                    PropertyStaticContentType.FullScreen,
                    PropertyStaticContentType.FullScreen_Retina,
                    PropertyStaticContentType.GalleryImage,
                    PropertyStaticContentType.GalleryImage_Retina,
                    PropertyStaticContentType.ListView,
                    PropertyStaticContentType.ListView_Retina,
                    PropertyStaticContentType.Thumbnail,
                    PropertyStaticContentType.Thumbnail_Retina
                });
            if(content.Any(c => c.ContentValue.Contains(string.Format("/{0}", fileName))))
            {
                return new UploadResult()
                {
                    ContentLength = 0,
                    ContentType = contentType,
                    FileName = fileName,
                    Extended = string.Format("#{0}|{1}|{2}!", 0, 0, fileName)
                };
            }
            fileName = RequestManager.Services.BlobService.AddFile(fileContent, propertyId, fileName);
            
            Image img = Image.FromStream(fileContent);

            return new UploadResult()
            {
                ContentLength = contentLength,
                ContentType = contentType,
                FileName = fileName,
                Extended = string.Format("#{0}|{1}|{2}!", img.Width, img.Height, 
                string.Format("{0}/{1}", RequestManager.Services.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray()), fileName.TrimStart("/".ToCharArray())))
            };
        }
    }
}