﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.UI;
using System.Web.UI;

namespace IntranetApp
{
    public static class RadGridExtensions
    {
        /// <summary>
        /// While applying filter, remember to check, whether string value is not empty or null.
        /// </summary>
        public static void ApplyFilter<T>(this RadGrid grid, ref IQueryable<T> sequence, string columnName, Func<IQueryable<T>, string, IQueryable<T>> filterAction)
        {
            string stringValue = grid.Columns.FindByUniqueName(columnName).CurrentFilterValue;
            if (!string.IsNullOrEmpty(stringValue))
                sequence = filterAction(sequence, stringValue.Trim()).AsQueryable();
        }

        public static void ApplyDecimalFilter<T>(this RadGrid grid, ref IQueryable<T> sequence, string columnName, Func<IQueryable<T>, decimal, IQueryable<T>> filterAction)
        {
            string stringValue = grid.Columns.FindByUniqueName(columnName).CurrentFilterValue;
            decimal value;
            if (!string.IsNullOrEmpty(stringValue) && decimal.TryParse(stringValue, out value))
                sequence = filterAction(sequence, value).AsQueryable();
        }

        public static void ApplyDateTimeFilter<T>(this RadGrid grid, ref IQueryable<T> sequence, string columnName, Func<IQueryable<T>, DateTime, IQueryable<T>> filterAction)
        {
            string stringValue = grid.Columns.FindByUniqueName(columnName).CurrentFilterValue;
            DateTime value;
            if (!string.IsNullOrEmpty(stringValue) && DateTime.TryParse(stringValue, out value))
                sequence = filterAction(sequence, value).AsQueryable();
        }

        public static void ApplyIntegerFilter<T>(this RadGrid grid, ref IQueryable<T> sequence, string columnName, Func<IQueryable<T>, int, IQueryable<T>> filterAction)
        {
            string stringValue = grid.Columns.FindByUniqueName(columnName).CurrentFilterValue;
            int value;
            if (!string.IsNullOrEmpty(stringValue) && int.TryParse(stringValue, out value))
                sequence = filterAction(sequence, value);
        }

        public static void SetColumnFilterValue(this RadGrid grid, string columnName, string value)
        {
            grid.Columns.FindByUniqueName(columnName).CurrentFilterValue = value;
        }

        public static string GetColumnFilterValue(this RadGrid grid, string columnName)
        {
            return grid.Columns.FindByUniqueName(columnName).CurrentFilterValue;
        }

        public static void HandleCustomFiltering(this RadGrid grid, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                string colName = filterPair.Second.ToString();
                e.Item.OwnerTableView.Columns.FindByUniqueName(colName).CurrentFilterFunction = (GridKnownFunction)Enum.Parse(typeof(GridKnownFunction), filterPair.First.ToString());

                foreach (GridColumn column in grid.Columns)
                {
                    column.CurrentFilterValue = column.CurrentFilterValue.Trim();
                }

                e.Canceled = true;
                e.Item.OwnerTableView.Rebind();
            }
        }

        //checks whether any of built in filters is set
        public static bool IsFiltered(this RadGrid grid)
        {
            foreach (GridColumn column in grid.Columns)
            {
                if (!string.IsNullOrWhiteSpace(column.CurrentFilterValue))
                {
                    return true;
                }
            }
            return false;
        }
    }
}