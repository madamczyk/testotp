﻿using BusinessLogic.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace IntranetApp
{
    public partial class MainLogged : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated && SessionManager.CurrentUser == null)
            {
                if (HttpContext.Current.User != null)
                {
                    var currentUser = RequestManager.Services.UsersService.GetUserByEmail(HttpContext.Current.User.Identity.Name);

                    if (currentUser != null)
                    {
                        SessionManager.CurrentUser = new BusinessLogic.Objects.Session.SessionUser(currentUser);
                        SessionManager.SessionToken = Guid.NewGuid().ToString();
                    }
                }
            }
            if (SessionManager.CurrentUser != null)
            {
                this.lblFullName.Text = BusinessLogic.Managers.SessionManager.CurrentUser.FullName.Trim();
                this.lblLogin.Text = BusinessLogic.Managers.SessionManager.CurrentUser.Email;
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx", true);
            }
        }

        /// <summary>
        /// Handles the Click event of the logoutLink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void logoutLink_Click(object sender, EventArgs e)
        {
            SessionManager.SessionToken = null;
            FormsAuthentication.SignOut();
            Response.Redirect("~/Account/Login.aspx", true);
        }

        /// <summary>
        /// Handles Click event from ChangePassword link button control.
        /// </summary>
        /// <param name="sender">Event's sender</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lbChangePassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Forms/Users/ChangePassword.aspx");
        }

        protected void BackOfficeMenu_ItemDataBound(object sender, RadMenuEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Item.NavigateUrl))
            {
                e.Item.Attributes["NavigateUrl"] = e.Item.NavigateUrl;
                e.Item.NavigateUrl = "";
            }
        }

        protected void BackOfficeMenu_OnItemClick(object sender, RadMenuEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Item.NavigateUrl) && e.Item.Attributes["NavigateUrl"] != null)
            {
                Session["navigationList"] = null;
                Response.Redirect(e.Item.Attributes["NavigateUrl"].ToString());
            }
        }
    }
}