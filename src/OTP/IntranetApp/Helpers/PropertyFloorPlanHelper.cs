﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Telerik.Web.UI;
using BusinessLogic.Managers;
using Common.ImageProcessing;
using System.Drawing.Imaging;
using BusinessLogic.Objects;
using DataAccess;
using DataAccess.Enums;
using Common.Serialization;
using BusinessLogic.Enums;
using ICSharpCode.SharpZipLib.Zip;
using IntranetApp.Handlers;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.WindowsAzure.ServiceRuntime;


namespace IntranetApp.Helpers
{
    public static class PropertyFloorPlanHelper
    {        
        /// <summary>
        /// Convert original floor plan and uploads it to azure storage
        /// </summary>
        /// <param name="propertyID">Property id</param>
        /// <param name="floorPlanStoragePath">Absolute storage path to original floor plan image</param>
        /// <returns></returns>
        public static string UploadConvertedFloorPlan(int propertyID, string floorPlanStoragePath)
        {
            using (var floorPlanStream = RequestManager.Services.BlobService.DownloadFileByUrl(floorPlanStoragePath, RequestManager.Services.SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersProperties)))
            {
                if (floorPlanStream == null)
                {
                    throw new ArgumentNullException("Uploaded floor plan was deleted immediately after upload");
                }
                var convertedFloorPlanImage = FloorPlanImage.Convert(floorPlanStream);
                using (var convertedImageStream = new MemoryStream())
                {
                    convertedFloorPlanImage.Save(convertedImageStream, ImageFormat.Png);
                    convertedImageStream.Seek(0, SeekOrigin.Begin);

                    string fileName = GetFileNameFromStoragePath(propertyID, floorPlanStoragePath);
                    string fileNameWithoutExt = fileName;
                    int fileExtPos = fileName.LastIndexOf(".");
                    if (fileExtPos >= 0)
                        fileNameWithoutExt = fileName.Substring(0, fileExtPos);

                    var displayImageFileName = String.Format("disp_{0}.png", fileNameWithoutExt);

                    RequestManager.Services.BlobService.InitContainer(RequestManager.Services.SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersProperties));
                    var convertedImageStoragePath =
                        RequestManager.Services.BlobService.AddFile(convertedImageStream, propertyID, displayImageFileName);

                    return convertedImageStoragePath;
                }
            }
        }

        /// <summary>
        /// Unzip and upload panorama to azure storage
        /// </summary>
        /// <param name="propertyID">Property id</param>
        /// <param name="zippedPanoramaFile">Panorama zip package</param>
        /// <returns></returns>
        public static IAsyncUploadResult UploadPanorama(int propertyID, UploadedFile zippedPanoramaFile, string fileName)
        {
            RequestManager.Services.BlobService.InitContainer(RequestManager.Services.SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersProperties));

            LocalResource localStorage = RoleEnvironment.GetLocalResource("TemporaryLocalStorage");
            string panoramaPackage = "panoramaFile_" + DateTime.Now.Ticks+ ".zip";

            zippedPanoramaFile.SaveAs(localStorage.RootPath + panoramaPackage, true);

            bool format;
            bool structure;
            bool mainDir;

            string fileZipName = Path.GetFileNameWithoutExtension(fileName);

            string idPath = "/id" + propertyID + "/";

            if (RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(propertyID, PropertyStaticContentType.Panorama).Any(p => p.ContentValue.Substring(p.ContentValue.IndexOf(idPath) + idPath.Length).ToLower() == fileZipName.ToLower()))
                return new UploadResult()
                {
                    ContentLength = 0,
                    ContentType = "panoramaFile",
                    FileName = string.Empty,
                    Extended = "#1|1|1|0!"
                };

            if (!ValidatePanoramaPackage(zippedPanoramaFile, out format, out structure, out mainDir))
            {
                return new UploadResult()
                {
                    ContentLength = 0,
                    ContentType = "panoramaFile",
                    FileName = string.Empty,
                    Extended = String.Format("#{0}|{1}|{2}|1!", format ? "1" : "0", structure ? "1" : "0", mainDir ? "1" : "0")
                };
            }
            using (ZipInputStream zipStream = new ZipInputStream(File.Open(localStorage.RootPath + panoramaPackage, FileMode.Open)))
            {
                ZipEntry entry;
                while ((entry = zipStream.GetNextEntry()) != null)
                {
                    if (entry.IsFile)
                    {
                        string newFilePath = entry.Name;
                        newFilePath = entry.Name.Substring(entry.Name.IndexOf('/'));
                        newFilePath = fileZipName + newFilePath;

                        if (newFilePath.EndsWith("index.htm") || newFilePath.EndsWith("index.html"))
                        {
                            RequestManager.Services.BlobService.AddFile(zipStream, propertyID, newFilePath, "html");
                        }
                        else
                        {
                            RequestManager.Services.BlobService.AddFile(zipStream, propertyID, newFilePath);
                        }
                    }
                }
            }           

            return new UploadResult()
            {
                ContentLength = 1,
                ContentType = "zip",
                FileName = String.Format("/{0}/id{1}/{2}", RequestManager.Services.SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersProperties), propertyID, fileZipName),
                Extended = "#1|1|1|1!"
            };
        }
        
        private static bool ValidatePanoramaPackage(UploadedFile zippedPanoramaFile, out bool format, out bool structure, out bool mainDir)
        {
            string PanoramaContent = ";/lib/;/index.htm;/media/;/script.js";
            string MediaDir = "media";

            format = false;
            structure = false;
            mainDir = false;

            if (zippedPanoramaFile != null && zippedPanoramaFile.GetExtension() == ".zip")
            {
                format = true;
            }
            ZipInputStream zipStream = new ZipInputStream(zippedPanoramaFile.InputStream);            
            ZipEntry entry;
            while ((entry = zipStream.GetNextEntry()) != null)
            {
                if (entry.Name.IndexOf("/") != -1)
                {
                    PanoramaContent = PanoramaContent.Replace(";" + entry.Name.Substring(entry.Name.IndexOf("/")).ToLower(), "");
                }
                if (entry.Name.ToLower().Contains(MediaDir))
                {
                    if (!(entry.Name.ToLower().StartsWith("/")) && !(entry.Name.ToLower().StartsWith(MediaDir)))
                    {
                        mainDir = true;
                    }
                }
            }            
            if (PanoramaContent.Equals(string.Empty))
            {
                structure = true;
            }
            if (!format || !structure || !mainDir)
                return false;
            return true;
        }

        /// <summary>
        /// Adds panorama static content to database
        /// </summary>
        /// <param name="propertyID"></param>
        /// <param name="panoramaStoragePath"></param>
        public static void AddPanorama(int propertyID, string panoramaStoragePath)
        {
            var existingPanorama = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(propertyID, PropertyStaticContentType.Panorama)
            .Where(ps => ps.ContentValue == panoramaStoragePath).FirstOrDefault();
            if (existingPanorama == null)
            {
                PropertyStaticContent psc = new PropertyStaticContent()
                    {
                        ContentCode = Convert.ToInt32(PropertyStaticContentType.Panorama),
                        ContentValue = panoramaStoragePath,
                        Property = RequestManager.Services.PropertiesService.GetPropertyById(propertyID)
                    };

                RequestManager.Services.PropertiesService.AddPropertyStaticContent(psc);
                RequestManager.Services.SaveChanges();
            }
        }

        /// <summary>
        /// Retusn web addres to specific azure blob
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static String GetFloorPlanImageAbsolutePath(string storagePath)
        {
            var storageBlobUrl =
                RequestManager.Services.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray());

            return String.Format("{0}/{1}", storageBlobUrl.TrimEnd("/".ToCharArray()), storagePath.TrimStart("/".ToCharArray()));
        }

        /// <summary>
        /// Returns file name from absolute storage path
        /// </summary>
        /// <param name="propertyID">Property id</param>
        /// <param name="storagePath">Absolute storage path</param>
        /// <returns></returns>
        public static String GetFileNameFromStoragePath(int propertyID, string storagePath)
        {
            return storagePath.Replace(String.Format("/{0}/id{1}/", RequestManager.Services.SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersProperties), propertyID), "");
        }
    }
}