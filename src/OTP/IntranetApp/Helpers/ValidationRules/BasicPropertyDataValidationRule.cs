﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Helpers;
using Resources;
using BusinessLogic.Managers;
using Common.Serialization;
using BusinessLogic.Objects.StaticContentRepresentation;
using DataAccess.Enums;

namespace IntranetApp.Helpers.ValidationRules
{
    public class BasicPropertyDataValidationRule : BasePropertyRule
    {
        public override bool Execute()
        {
            ValidatePropertyName();
            ValidateLongDescription();
            ValidateShortDescription();
            ValidatePropertyCode();
            ValidatePropertyType();
            ValidatePropertyAltitude();
            ValidateAmenities();
            ValidateTags();
            
            ValidateFloorPlans();
            ValidateImages();
            ValidateFeatures();
            ValidateDistanceToAttractions();
            ValidateDescription();
            ValidateSquareFootage();

            return base.Execute();            
        }

        private void ValidatePropertyName()
        {
            if (base.Property.PropertyNameCurrentLanguage.CountWords() < 2)
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.BasicData_PropertyName, "PropertyNameValidationMarkEvent()"));
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidatePropertyCode()
        {
            if (string.IsNullOrWhiteSpace(base.Property.PropertyCode))
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.BasicData_PropertyCode, "PropertyCodeValidationMarkEvent()"));
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidatePropertyType()
        {
            if (base.Property.PropertyType == null)
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.BasicData_PropertyType, "PropertyTypeValidationMarkEvent()"));
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidatePropertyAltitude()
        {
            if (!base.Property.Altitude.HasValue)
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.BasicData_PropertyAltitude, "PropertyAltitudeValidationMarkEvent()"));
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidateLongDescription()
        {
            int charsNumber = base.Property.Long_DescriptionCurrentLanguage.ToString().Length;
            if (charsNumber < 100 || charsNumber > 500)
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.BasicData_LongDescription, "PropertyLongDescValidationMarkEvent()"));
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidateShortDescription()
        {
            int charsNumber = base.Property.Short_DescriptionCurrentLanguage.ToString().Length;
            if (charsNumber < 20 || charsNumber > 80)
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.BasicData_ShortDescription, "PropertyShortDescValidationMarkEvent()"));
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidateAmenities()
        {
            var amenities = RequestManager.Services.PropertiesService.GetPropertyAmenitiesByPropertyId(Property.PropertyID);
            var amenityGroups = amenities.Select(gr => gr.Amenity.AmenityGroup.AmenityGroupId).Distinct().ToList();
            if (amenities.Select(am => am.Amenity.AmenityID).Distinct().Count() < 2 || amenityGroups.Count < 2)
            {
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.BasicData_Amenities, "AmenitiesValidationMarkEvent()"));
            }
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidateTags()
        {
            var tags = RequestManager.Services.PropertiesService.GetPropertyTagsByPropertyId(Property.PropertyID);
            var tagGroups = tags.Select(gr => gr.Tag.TagGroup.TagGroupId).Distinct().ToList();

            if (tags.Select(am => am.Tag.TagGroup).Distinct().Count() < 2 || tagGroups.Count < 2)
            {
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.BasicData_Tags, "TagsValidationMarkEvent()"));
            }
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidateFloorPlans()
        {
            var floorPlans = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(Property.PropertyID, DataAccess.Enums.PropertyStaticContentType.FloorPlan).ToList();
            if (floorPlans.Count == 0)
            {
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.Property_StaticContent_FloorPlanCount, "FloorPlansValidationMarkEvent()"));
            }
            else
                ValidationResults.Add(new ValidationResult());

            foreach (var floorPlan in floorPlans)
            {
                var panoramas = SerializationHelper.DeserializeObject<FloorPlan>(floorPlan.ContentValue);
                if (panoramas.Panoramas == null)
                {
                    ValidationResults.Add(new ValidationResult(floorPlan.PropertyStaticContentId, PropertyValidations.Property_StaticContent_FloorPlanHotSpot, "FloorPlansHotSpotsValidationMarkEvent(" + floorPlan.PropertyStaticContentId.ToString() + ")"));
                }
                else
                {
                    ValidationResults.Add(new ValidationResult());
                }
            }            
        }

        private void ValidateImages()
        {
            var images = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(Property.PropertyID,
                new List<PropertyStaticContentType>()
                {
                    PropertyStaticContentType.FullScreen,
                    PropertyStaticContentType.FullScreen_Retina,
                    PropertyStaticContentType.GalleryImage,
                    PropertyStaticContentType.GalleryImage_Retina,
                    PropertyStaticContentType.ListView,
                    PropertyStaticContentType.ListView_Retina,
                    PropertyStaticContentType.Thumbnail,
                    PropertyStaticContentType.Thumbnail_Retina
 
                }).ToList();
            int distinctImagesTypes = images.Select(im => im.ContentCode).Distinct().Count();
            if (distinctImagesTypes != 8)
            {
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.Property_StaticContent_Images, "ImagesValidationMarkEvent()"));
            }
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidateFeatures()
        {
            var features = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(Property.PropertyID,
                new List<PropertyStaticContentType>()
                {
                    PropertyStaticContentType.Feature
 
                }).ToList();
            if (features.Count < 3)
            {
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.Property_StaticContent_FeaturesCount, "FeaturesValidationMarkEvent()"));
            }
            else
                ValidationResults.Add(new ValidationResult());
            foreach (var f in features)
            {
                if (f.ContentValueCurrentLanguage.CountWords() > 3 && f.ContentValueCurrentLanguage.CountWords() < 9)
                {
                    ValidationResults.Add(new ValidationResult());
                }
                else
                {
                    ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.Property_StaticContent_FeaturesWordsCount, "FeaturesWordsValidationMarkEvent(" + f.PropertyStaticContentId.ToString() + ")"));
                }
            }
        }

        private void ValidateDistanceToAttractions()
        {
            var distance = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(Property.PropertyID,
                new List<PropertyStaticContentType>()
                {
                    PropertyStaticContentType.DistanceToMainAttraction
 
                }).ToList();
            if (distance.Count == 0)
            {
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.Property_StaticContent_DistanceToAttractions, "DistanceToAttractionsValidationMarkEvent()"));
            }
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidateDescription()
        {
            var distance = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(Property.PropertyID,
                new List<PropertyStaticContentType>()
                {
                    PropertyStaticContentType.LocalAreaDescription
 
                }).ToList();
            if (distance.Count == 1 && distance.SingleOrDefault().ContentValueCurrentLanguage.Length > 100 && distance.SingleOrDefault().ContentValueCurrentLanguage.Length < 500)
            {
                ValidationResults.Add(new ValidationResult());
            }
            else
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.Property_StaticContent_LocalAreaDesctription, "LocalAreaDescrValidationMarkEvent()"));
        }

        private void ValidateSquareFootage()
        {
            int minSquareFootage = 1000;
            string sqFtSuffix = PropertyValidations.Property_SquareFootageSuffix;
            var sqFt = RequestManager.Services.PropertiesService.GetPropertyStaticContentByType(Property.PropertyID,
                new List<PropertyStaticContentType>()
                {
                    PropertyStaticContentType.SquareFootage
 
                }).ToList();
            if (sqFt.Count == 1)
            {
                int sqftValue;
                if (int.TryParse(sqFt.SingleOrDefault().ContentValueCurrentLanguage.Replace(sqFtSuffix, string.Empty), out sqftValue) && sqftValue > minSquareFootage)
                {
                    ValidationResults.Add(new ValidationResult());
                    return;
                }                
            }
            ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.Property_StaticContent_SquareFootage, "SquareFootageValidationMarkEvent()"));
        }
    }
}