﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Helpers;
using Resources;
using BusinessLogic.Managers;

namespace IntranetApp.Helpers.ValidationRules
{
    /// <summary>
    /// Contains validation methods for checking unit types 
    /// </summary>
    public class UnitTypesValidationRule : BasePropertyRule
    {
        public override bool Execute()
        {
            ValidateUnitTypesCount();
            ValidateUnitTypeDescription();            

            return base.Execute();            
        }

        /// <summary>
        /// Validate if property has at least 1 unit type
        /// </summary>
        private void ValidateUnitTypesCount()
        {
            if (base.Property.UnitTypes.Count == 0)
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.UnitTypes_UnitTypesCount, "UnitTypesCountValidationMarkEvent()"));
            else
                ValidationResults.Add(new ValidationResult());
        }

        /// <summary>
        /// Validate if unit type description has more than 40 letters and less than 200
        /// </summary>
        private void ValidateUnitTypeDescription()
        {
            foreach (var unitType in base.Property.UnitTypes)
            {
                int charsNumber = unitType.UnitTypeDescCurrentLanguage.Length;
                if (charsNumber < 40 || charsNumber > 200)
                    ValidationResults.Add(new ValidationResult(unitType.UnitTypeID, 
                        string.Format(PropertyValidations.UnitTypes_Description, unitType.UnitTypeTitleCurrentLanguage), "UnitTypesDescValidationMarkEvent(" + unitType.UnitTypeID.ToString() + ")"));
                else
                    ValidationResults.Add(new ValidationResult());
            }            
        }        
    }
}