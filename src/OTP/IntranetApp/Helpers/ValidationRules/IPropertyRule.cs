﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetApp.Helpers.ValidationRules
{
    public interface IPropertyRule
    {
        bool Execute();
    }
}