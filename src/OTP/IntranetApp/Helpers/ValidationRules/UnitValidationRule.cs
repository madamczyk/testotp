﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Helpers;
using Resources;
using BusinessLogic.Managers;
using DataAccess.Enums;
using System.Text.RegularExpressions;

namespace IntranetApp.Helpers.ValidationRules
{
    public class UnitValidationRule : BasePropertyRule
    {
        public override bool Execute()
        {
            ValidateUnitsCount();
            ValidateUnitsDescription();
            ValidateNoOfBedrooms();
            ValidateNoOfBathrooms();
            ValidateAppliances();
            ValidateRates();
            ValidateStaticContentBathrooms();
            ValidateStaticContentBedrooms();
            ValidateStaticContentLivingArea();
            ValidateStaticContentWifi();

            return base.Execute();            
        }

        private void ValidateUnitsCount()
        {
            int unitTypes = base.Property.UnitTypes.Count;
            int units = base.Property.Units.Select(ut => ut.UnitType).Distinct().Count();

            if (unitTypes != units)
                ValidationResults.Add(new ValidationResult(base.Property.PropertyID, PropertyValidations.Units_UnitsCount, "UnitCountValidationMarkEvent()"));
            else
                ValidationResults.Add(new ValidationResult());
        }

        private void ValidateUnitsDescription()
        {
            foreach (var unit in base.Property.Units)
            {
                int charsNumber = unit.UnitDescCurrentLanguage.Length;
                if (charsNumber < 40 || charsNumber > 200)
                    ValidationResults.Add(new ValidationResult(unit.UnitID, 
                        string.Format(PropertyValidations.UnitTypes_Description, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                else
                    ValidationResults.Add(new ValidationResult());
            }            
        }

        private void ValidateNoOfBedrooms()
        {
            foreach (var unit in base.Property.Units)
            {
                if (unit.MaxNumberOfBedRooms > 20)
                    ValidationResults.Add(new ValidationResult(unit.UnitID,
                        string.Format(PropertyValidations.Units_NumberOfBedrooms, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                else
                    ValidationResults.Add(new ValidationResult());
            }
        }

        private void ValidateNoOfBathrooms()
        {
            foreach (var unit in base.Property.Units)
            {
                if (unit.MaxNumberOfBathrooms > 20)
                    ValidationResults.Add(new ValidationResult(unit.UnitID,
                        string.Format(PropertyValidations.Units_NumberOfBathrooms, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                else
                    ValidationResults.Add(new ValidationResult());
            }
        }

        private void ValidateAppliances()
        {
            var appliances = RequestManager.Services.AppliancesService.GetAppliances().ToList();

            ///Required appliances
            string deluxeKitchen = PropertyValidations.Unit_DeluxeKitchen;
            string cookingAndbaking = PropertyValidations.Unit_CookingAndBaking;
            string majorAppliance = PropertyValidations.Unit_MajorAppliances;
            string minorAppliance = PropertyValidations.Unit_MinorAppliances;
            string platingSets = PropertyValidations.Unit_PlatingSets;

            int deluxeKitchenID = appliances.Where(ap => ap.ApplianceGroup.GroupNameCurrentLanguage == deluxeKitchen).FirstOrDefault().ApplianceGroup.ApplianceGroupId;
            int cookingAndbakingID = appliances.Where(ap => ap.ApplianceGroup.GroupNameCurrentLanguage == cookingAndbaking).FirstOrDefault().ApplianceGroup.ApplianceGroupId;
            int majorApplianceID = appliances.Where(ap => ap.ApplianceGroup.GroupNameCurrentLanguage == majorAppliance).FirstOrDefault().ApplianceGroup.ApplianceGroupId;
            int minorApplianceID = appliances.Where(ap => ap.ApplianceGroup.GroupNameCurrentLanguage == minorAppliance).FirstOrDefault().ApplianceGroup.ApplianceGroupId;
            int platingSetsID = appliances.Where(ap => ap.ApplianceGroup.GroupNameCurrentLanguage == platingSets).FirstOrDefault().ApplianceGroup.ApplianceGroupId;

            foreach (var unit in base.Property.Units)
            {
                var unitAppliances = unit.UnitAppliancies.ToList();

                ValidationResults.Add(unitAppliances.Where(ua => ua.Appliance.ApplianceGroup.ApplianceGroupId == deluxeKitchenID).FirstOrDefault() != null ?
                    new ValidationResult() : new ValidationResult(unit.UnitID, string.Format(PropertyValidations.Units_Appliances_DeluxeKitchen, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));

                ValidationResults.Add(unitAppliances.Where(ua => ua.Appliance.ApplianceGroup.ApplianceGroupId == cookingAndbakingID).FirstOrDefault() != null ?
                    new ValidationResult() : new ValidationResult(unit.UnitID, string.Format(PropertyValidations.Units_Appliances_CookingBaking, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));

                ValidationResults.Add(unitAppliances.Where(ua => ua.Appliance.ApplianceGroup.ApplianceGroupId == majorApplianceID).ToList().Count > 4 ?
                    new ValidationResult() : new ValidationResult(unit.UnitID, string.Format(PropertyValidations.Units_Appliances_Major, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));

                ValidationResults.Add(unitAppliances.Where(ua => ua.Appliance.ApplianceGroup.ApplianceGroupId == minorApplianceID).ToList().Count > 2 ?
                    new ValidationResult() : new ValidationResult(unit.UnitID, string.Format(PropertyValidations.Units_Appliances_Minor, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));


                DataAccess.UnitAppliancy unitAppliancy = unitAppliances.Where(ua => ua.Appliance.ApplianceGroup.ApplianceGroupId == platingSetsID).FirstOrDefault();

                if (unitAppliancy != null)
                {
                    if (unitAppliancy.Quantity == null || unitAppliancy.Quantity < 6)
                    {
                        ValidationResults.Add(new ValidationResult(unit.UnitID, string.Format(PropertyValidations.Units_Appliances_PlatingSets_MinimumQuantity, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                    }
                    else
                    {
                        ValidationResults.Add(new ValidationResult());
                    }
                }
                else
                {
                    ValidationResults.Add(new ValidationResult(unit.UnitID, string.Format(PropertyValidations.Units_Appliances_PlatingSets, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                }
            }
        }

        private void ValidateRates()
        {
            foreach (var unit in base.Property.Units)
            {
                bool isValid = true;
                DateTime today = unit.Created != null ? unit.Created.Value.Date : DateTime.Now.Date;
                DateTime dayAfterYear = today.AddYears(1);

                //all ranges
                var rates = RequestManager.Services.UnitsService.GetUnitRates(unit.UnitID).OrderBy(ur => ur.DateFrom).ToList();
                
                //start range
                var rateToday = rates.Where(ur => today >= ur.DateFrom && today <= ur.DateUntil).FirstOrDefault();
                
                //last matched range
                var rateAfterYear = rates.Where(ur => dayAfterYear >= ur.DateFrom && dayAfterYear <= ur.DateUntil).FirstOrDefault();
                
                bool continuosRanges = true;
                if (rateToday != null && rateAfterYear != null)
                {
                    //ranges onyly for next year
                    rates = rates.Where(r => r.DateFrom >= rateToday.DateFrom && r.DateUntil <= rateAfterYear.DateUntil).ToList();

                    DateTime? lastDayFromPrevRange = null;
                    foreach (var range in rates)
                    {
                        if (lastDayFromPrevRange.HasValue)
                        {
                            if (lastDayFromPrevRange.Value.AddDays(1) != range.DateFrom)
                            {
                                continuosRanges = false;
                                break;
                            }
                            lastDayFromPrevRange = range.DateUntil;
                        }
                        else
                        {
                            lastDayFromPrevRange = range.DateUntil;
                        }
                    }
                }
                else
                {
                    isValid = false;
                }
                if (!continuosRanges)
                    isValid = false;

                if (!isValid)
                    ValidationResults.Add(new ValidationResult(unit.UnitID,
                        string.Format(PropertyValidations.Units_UnitRatesGaps, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                else
                    ValidationResults.Add(new ValidationResult());

            }
        }

        private void ValidateStaticContentBathrooms()
        {
            string bath = PropertyValidations.Unit_Bath.ToLower();
            UnitStaticContentType type = UnitStaticContentType.Bathrooms;

            foreach (var unit in base.Property.Units)
            {
                var bathrooomStaticContents = unit.UnitStaticContents.Where(sc => sc.ContentCode == (int)type).ToList();

                // check, whether is there at least one bath
                if (bathrooomStaticContents.Where(sc => sc.ContentValueCurrentLanguage.ToLower().Contains(bath)).Count() == 0)
                    ValidationResults.Add(new ValidationResult(unit.UnitID,
                        string.Format(PropertyValidations.Units_StaticContent_Bathrooms, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                else
                    ValidationResults.Add(new ValidationResult());
            }
        }

        private void ValidateStaticContentBedrooms()
        {
            string masterBedroom = PropertyValidations.Unit_MasterBedroom.ToLower();
            UnitStaticContentType type = UnitStaticContentType.Bedrooms;
            string contentformat = "^({{room}}) / ({{bed}}) " + PropertyValidations.Unit_Bedroom_Bed + "$";
            string[] roomTypes = PropertyValidations.Unit_Bedroom_TypesOfRoom.Split(','),
                     bedTypes = PropertyValidations.Unit_Bedroom_TypesOfBed.Split(',');

            contentformat = contentformat
                .Replace("{{room}}", String.Join("|", roomTypes.Select(t => String.Format("({0})", t.Trim())).ToArray()))
                .Replace("{{bed}}", String.Join("|", bedTypes.Select(t => String.Format("({0})", t.Trim())).ToArray()));

            foreach (var unit in base.Property.Units)
            {
                var bedrooomStaticContents = unit.UnitStaticContents.Where(sc => sc.ContentCode == (int)type).ToList();

                // check, whether is there at least one Master Beddrom
                if (bedrooomStaticContents.Where(sc => sc.ContentValueCurrentLanguage.ToLower().Contains(masterBedroom)).Count() == 0)
                    ValidationResults.Add(new ValidationResult(unit.UnitID,
                        string.Format(PropertyValidations.Units_StaticContent_Bedrooms, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                else
                    ValidationResults.Add(new ValidationResult());

                // check, whether all records follow the specified format
                if (bedrooomStaticContents.Any(sc => !Regex.IsMatch(sc.ContentValueCurrentLanguage, contentformat, RegexOptions.IgnoreCase)))
                    ValidationResults.Add(new ValidationResult(unit.UnitID,
                        string.Format(PropertyValidations.Units_StaticContent_Bedrooms_Format, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                else
                    ValidationResults.Add(new ValidationResult());
            }
        }
        
        private void ValidateStaticContentLivingArea()
        {
            string diningTable = PropertyValidations.Unit_DinigTable;
            string seats = PropertyValidations.Unit_Seats;
            
            foreach (var unit in base.Property.Units)
            {
                UnitStaticContentType type = UnitStaticContentType.LivingAreasAndDining;
                var staticContents = unit.UnitStaticContents.Where(sc => sc.ContentCode == (int)type).ToList();

                if (staticContents.Where(sc => sc.ContentValueCurrentLanguage.Contains(diningTable)).ToList().Count == 0)
                {
                    ValidationResults.Add(new ValidationResult(unit.UnitID,
                        string.Format(PropertyValidations.Units_StaticContent_LivingAreas, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                }
                else
                    ValidationResults.Add(new ValidationResult());

                if (staticContents.Where(sc => sc.ContentValueCurrentLanguage.Contains(seats)).ToList().Count < 2)
                {
                    ValidationResults.Add(new ValidationResult(unit.UnitID,
                        string.Format(PropertyValidations.Units_StaticContent_LivingAreas_Seats, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                }
                else
                    ValidationResults.Add(new ValidationResult());
            }
        }

        private void ValidateStaticContentWifi()
        {
            int nameType = (int)UnitStaticContentType.WifiName,
                passwordType = (int)UnitStaticContentType.WifiPassword;

            foreach (var unit in base.Property.Units)
            {
                var staticContents = unit.UnitStaticContents;

                if (staticContents.Where(sc => sc.ContentCode == nameType).Count() == 0 || staticContents.Where(sc => sc.ContentCode == passwordType).Count() == 0)
                {
                    ValidationResults.Add(new ValidationResult(unit.UnitID,
                        string.Format(PropertyValidations.Units_StaticContent_Wifi, unit.UnitTitleCurrentLanguage), string.Format("UnitFieldValidationMarkEvent({0})", unit.UnitID.ToString())));
                }
                else
                    ValidationResults.Add(new ValidationResult());
            }
        }

    }
}