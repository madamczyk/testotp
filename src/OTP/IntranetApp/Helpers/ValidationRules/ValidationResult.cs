﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetApp.Helpers.ValidationRules
{
    public class ValidationResult
    {
        public bool IsValid { get; set; }
        public int ValidationObjectID { get; set; }
        public string ValidationMessage { get; set; }
        public string MessageClientEventName { get; set; }

        public ValidationResult()
        {
            IsValid = true;
        }

        public ValidationResult(int objectId, string message, string clientEventName)
        {
            IsValid = false;
            ValidationObjectID = objectId;
            ValidationMessage = message;
            MessageClientEventName = clientEventName;
        }
    }
}