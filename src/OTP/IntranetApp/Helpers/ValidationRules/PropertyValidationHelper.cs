﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetApp.Helpers.ValidationRules
{
    public static class PropertyValidationHelper
    {
        /// <summary>
        /// List of validation errors
        /// </summary>
        public static List<ValidationResult> ValidationErrors;

        /// <summary>
        /// Calculate completness for property
        /// </summary>
        /// <param name="property"></param>
        /// <returns>Percentage value of property completness</returns>
        public static int CalculatePropertyCompletness(Property property)
        {
            List<Type> RulesList = new List<Type>()
            {
                typeof(BasicPropertyDataValidationRule),
                typeof(UnitTypesValidationRule),
                typeof(UnitValidationRule)

            };

            int rulesCount = 0;
            int validationSuccedded = 0;
            ValidationErrors = new List<ValidationResult>();
            foreach (Type type in RulesList)
            {
                BasePropertyRule rule = (BasePropertyRule)Activator.CreateInstance(type, null);
                rule.Property = property;
                rule.Execute();
                rulesCount += rule.ValidationResults.Count;
                validationSuccedded += rule.ValidationResults.Where(s => s.IsValid == true).ToList().Count;
                ValidationErrors.AddRange(rule.ValidationResults.Where(s => s.IsValid == false).ToList());
            }
            return decimal.ToInt32(((decimal)(validationSuccedded / (decimal)rulesCount)) * 100.00M);
        }
    }
}