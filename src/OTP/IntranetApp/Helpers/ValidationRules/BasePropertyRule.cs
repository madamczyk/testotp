﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetApp.Helpers.ValidationRules
{
    public abstract class BasePropertyRule : IPropertyRule
    {
        private Property _property;

        public List<ValidationResult> ValidationResults
        {
            get;
            set;
        }

        public Property Property
        {
            get { return _property; }
            set { _property = value; }
        }

        public virtual bool Execute()
        {
            //all validation results are valid
            if (ValidationResults.Where(s => s.IsValid == false).ToList().Count == 0)
                return true;
            return false;
        }

        public BasePropertyRule()
        {
            this.ValidationResults = new List<ValidationResult>();
        }

    }
}