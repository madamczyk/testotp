﻿using System.IO;
using System.Text;
using System.Xml;

namespace IntranetApp.Helpers
{
    public static class XmlDisplayHelper
    {
        /// <summary>
        /// Returns indented formatted XML
        /// </summary>
        /// <param name="xml">XML data</param>
        /// <returns>Indented formatted XML</returns>
        public static string DisplayIndentedXML(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            using (MemoryStream ms = new MemoryStream())
            using (XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.Unicode))
            using (StreamReader sr = new StreamReader(ms))
            {
                xtw.Formatting = Formatting.Indented;
                doc.WriteContentTo(xtw);
                xtw.Flush();
                ms.Seek(0, SeekOrigin.Begin);
                return sr.ReadToEnd();
            }
        }
    }
}