﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Permissions;
using DataAccess;
using BusinessLogic.Managers;

namespace IntranetApp.SiteMap
{
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class CustomSiteMapProvider : System.Web.StaticSiteMapProvider
    {
        private SiteMapNode rootNode = null;

        public CustomSiteMapProvider()
        {
        }

        protected override SiteMapNode GetRootNodeCore()
        {
            if (rootNode == null)
                BuildSiteMap();
            return rootNode;
        }

        /// <summary>
        /// Builds site map object 
        /// </summary>
        /// <returns></returns>
        public override SiteMapNode BuildSiteMap()
        {
            lock (this)
            {
                if (this.rootNode == null)
                {
                    IntranetSiteMapAction rootAction = RequestManager.Services.SiteMapService.GetRootAction();
                    if (rootAction == null)
                        return null;

                    Stack<IntranetSiteMapAction> stack = new Stack<IntranetSiteMapAction>();
                    stack.Push(rootAction);

                    IntranetSiteMapAction siteMapAction;
                    SiteMapNode newSiteMapNode, parentSiteMapNode;
                    List<string> roles;
                    Dictionary<int, SiteMapNode> nodes = new Dictionary<int, SiteMapNode>(); // used to access nodes already added to the site map

                    while (stack.Count != 0)
                    {
                        siteMapAction = stack.Pop();

                        foreach (IntranetSiteMapAction item in siteMapAction.ChildIntranetSiteMapActions)
                        {
                            stack.Push(item);
                        }

                        newSiteMapNode = new SiteMapNode(this, siteMapAction.Id.ToString(), siteMapAction.Url, siteMapAction.DisplayNameCurrentLanguage, siteMapAction.DescriptionCurrentLanguage);
                        roles = GetRoles(siteMapAction);
                        newSiteMapNode.Roles = roles;

                        if (siteMapAction.ParentIntranetSiteMapAction != null)
                        {
                            parentSiteMapNode = nodes[siteMapAction.ParentIntranetSiteMapAction.Id];
                            if (parentSiteMapNode != null)
                            {
                                AddNode(newSiteMapNode, parentSiteMapNode);
                                foreach (string role in roles)
                                    AddAndPropagateRole(parentSiteMapNode, role);
                            }
                        }
                        else
                        {
                            AddNode(newSiteMapNode, null);
                            this.rootNode = newSiteMapNode;
                        }
                        nodes.Add(siteMapAction.Id, newSiteMapNode);
                    }
                }
                return rootNode;
            }
        }

        private List<string> GetRoles(IntranetSiteMapAction siteMapAction)
        {
            List<string> roles = new List<string>();

            foreach (RoleIntranetSiteMapAction roleSiteMapAction in siteMapAction.RoleIntranetSiteMapActions)
                roles.Add(roleSiteMapAction.Role.Name);

            return roles;
        }

        private void AddAndPropagateRole(SiteMapNode node, string role)
        {
            SiteMapNode temp = node;
            List<string> roles;

            while (temp != null)
            {
                roles = (List<string>)temp.Roles;
                if (roles == null)
                {
                    roles = new List<string>();
                    roles.Add(role);
                    temp.Roles = roles;
                }
                else if (!roles.Contains(role))
                {
                    roles.Add(role);
                }
                temp = temp.ParentNode;
            }
        }

    }
}