﻿<%@ Page Language="C#" MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="IntranetApp.Forms.Users.ChangePassword" %>
<%@ Register TagPrefix="uc" TagName="ChangePassword" Src="~/Controls/Users/ChangePassword.ascx" %>

<asp:Content ID="ctlMainContent" ContentPlaceHolderID="mainContent" runat="server">
    <uc:ChangePassword ID="ctlChangePassword" runat="server" />
</asp:Content>
