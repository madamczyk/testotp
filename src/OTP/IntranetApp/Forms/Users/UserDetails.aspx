﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master"  AutoEventWireup="true" CodeBehind="UserDetails.aspx.cs" Inherits="IntranetApp.Forms.Users.UserDetails" %>
<%@ Register TagPrefix="uc" TagName="UserDetails" Src="~/Controls/Users/UserDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:UserDetails ID="UserDetailsCtrl" runat="server" />
</asp:Content>

