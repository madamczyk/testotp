﻿using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Forms.Users
{
    public partial class UserDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int outInt = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["mode"]) && int.TryParse(Request.QueryString["mode"], out outInt))
            {
                this.UserDetailsCtrl.Mode = (FormMode)outInt;
            }

            if (!string.IsNullOrEmpty(Request.QueryString["role"]) && int.TryParse(Request.QueryString["role"], out outInt))
            {
                this.UserDetailsCtrl.Role = outInt;
            }

            if (!string.IsNullOrEmpty(Request.QueryString["userId"]) && int.TryParse(Request.QueryString["userId"], out outInt))
            {
                this.UserDetailsCtrl.UserId = outInt;
            }
        }
    }
}