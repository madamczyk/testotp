﻿using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Forms.Users
{
    public partial class DataEntryManagersList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.UsersListCtrl.UsersRoleLevel = RoleLevel.DataEntryManager;
        }
    }
}