﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="GuestsList.aspx.cs" Inherits="IntranetApp.Forms.Users.GuestsList" %>
<%@ Register TagPrefix="uc" TagName="UsersList" Src="~/Controls/Users/UsersList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:UsersList ID="UsersListCtrl" runat="server" />
</asp:Content>