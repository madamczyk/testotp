﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="OwnersList.aspx.cs" Inherits="IntranetApp.Forms.Users.OwnersList" %>
<%@ Register TagPrefix="uc" TagName="UsersList" Src="~/Controls/Users/UsersList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:UsersList ID="UsersListCtrl" runat="server" />
</asp:Content>