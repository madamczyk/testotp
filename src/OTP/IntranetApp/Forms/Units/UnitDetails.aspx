﻿<%@ Page Language="C#" MasterPageFile="~/Simple.master" AutoEventWireup="true" CodeBehind="UnitDetails.aspx.cs" Inherits="IntranetApp.Forms.Units.UnitDetails" EnableViewStateMac="false" %>

<%@ Register TagPrefix="uc" TagName="UnitDetails" Src="~/Controls/Units/UnitDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:UnitDetails ID="ctrlUnitDetails" runat="server" />
</asp:Content>
