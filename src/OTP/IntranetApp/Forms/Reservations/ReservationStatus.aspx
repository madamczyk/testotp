﻿<%@ Page MasterPageFile="~/Simple.master" Language="C#" AutoEventWireup="true" CodeBehind="ReservationStatus.aspx.cs" Inherits="IntranetApp.Forms.Reservations.ReservationStatus" %>
<%@ Register TagPrefix="uc" TagName="ResStatus" Src="~/Controls/Reservations/ReservationStatus.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
.centerWrapperSmall {
    min-height: 0px !important;
    width: inherit !important;
}
.content {
        min-height: 0px !important;
}
</style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:ResStatus runat="server" ID="ctrlReservationStatus" />
</asp:Content>