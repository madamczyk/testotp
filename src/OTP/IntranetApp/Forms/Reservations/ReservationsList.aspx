﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="ReservationsList.aspx.cs" Inherits="IntranetApp.Forms.DestinationsList" %>
<%@ Register TagPrefix="uc" TagName="ReservationsList" Src="~/Controls/Reservations/ReservationsList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:ReservationsList ID="ReservationsListCtrl" runat="server" />
</asp:Content>