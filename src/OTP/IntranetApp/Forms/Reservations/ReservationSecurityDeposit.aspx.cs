﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Forms.Reservations
{
    public partial class ReservationSecurityDeposit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int outInt = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out outInt))
            {
                this.ctrlReservationAction.ReservationID = outInt;
            }
            if (!string.IsNullOrEmpty(Request.QueryString["mode"]) && int.TryParse(Request.QueryString["mode"], out outInt))
            {
                this.ctrlReservationAction.FormMode = outInt;
            }
        }
    }
}