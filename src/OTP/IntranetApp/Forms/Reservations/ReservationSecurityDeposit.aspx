﻿<%@ Page MasterPageFile="~/Simple.master" Language="C#" AutoEventWireup="true" CodeBehind="ReservationSecurityDeposit.aspx.cs" Inherits="IntranetApp.Forms.Reservations.ReservationSecurityDeposit" %>
<%@ Register TagPrefix="uc" TagName="ResAction" Src="~/Controls/Reservations/ReservationSecurityDeposit.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
.centerWrapperSmall {
    min-height: 0px !important;
    width: inherit !important;
}
.content {
        min-height: 0px !important;
}
</style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:ResAction runat="server" ID="ctrlReservationAction" />
</asp:Content>