﻿<%@ Page Language="C#" MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="KeyManagement.aspx.cs" Inherits="IntranetApp.Forms.Reservations.KeyManagement" %>
<%@ Register TagPrefix="uc" TagName="KeyManagement" Src="~/Controls/Reservations/KeyManagement.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:KeyManagement ID="KeyManagemenCtrl" runat="server" />
</asp:Content>
