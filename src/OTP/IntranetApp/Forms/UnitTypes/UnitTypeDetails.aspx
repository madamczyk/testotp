﻿<%@ Page Language="C#" MasterPageFile="~/Simple.Master" AutoEventWireup="true" CodeBehind="UnitTypeDetails.aspx.cs" Inherits="IntranetApp.Forms.UnitTypes.UnitTypeDetails" %>

<%@ Register TagPrefix="uc" TagName="UnitTypeDetails" Src="~/Controls/UnitTypes/UnitTypeDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:UnitTypeDetails ID="ctrlUnitTypeDetails" runat="server" />
</asp:Content>