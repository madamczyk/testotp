﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="PaymentGatewayOperationLogList.aspx.cs" Inherits="IntranetApp.Forms.PaymentGatewayOperationLogList" %>
<%@ Register TagPrefix="uc" TagName="PaymentGatewayOperationLogList" Src="~/Controls/Logs/PaymentGatewayOperationLogList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:PaymentGatewayOperationLogList ID="PaymentGatewayOperationLogListCtrl" runat="server" />
</asp:Content>