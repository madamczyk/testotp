﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="TaskInstancesList.aspx.cs" Inherits="IntranetApp.Forms.TaskInstancesList" %>
<%@ Register TagPrefix="uc" TagName="TaskInstancesList" Src="~/Controls/Logs/TaskInstancesList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:TaskInstancesList ID="TaskInstancesListCtrl" runat="server" />
</asp:Content>