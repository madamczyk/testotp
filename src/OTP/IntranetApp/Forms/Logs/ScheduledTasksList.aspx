﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="ScheduledTasksList.aspx.cs" Inherits="IntranetApp.Forms.ScheduledTasksList" %>
<%@ Register TagPrefix="uc" TagName="ScheduledTasksList" Src="~/Controls/Logs/ScheduledTasksList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:ScheduledTasksList ID="ScheduledTasksListCtrl" runat="server" />
</asp:Content>