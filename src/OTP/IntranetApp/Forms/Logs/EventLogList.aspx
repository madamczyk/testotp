﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="EventLogList.aspx.cs" Inherits="IntranetApp.Forms.EventLogList" %>
<%@ Register TagPrefix="uc" TagName="EventLogList" Src="~/Controls/Logs/EventLogList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:EventLogList ID="EventLogListCtrl" runat="server" />
</asp:Content>