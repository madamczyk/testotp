﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="CrmOperationLogList.aspx.cs" Inherits="IntranetApp.Forms.CrmOperationLogList" %>
<%@ Register TagPrefix="uc" TagName="CrmOperationLogList" Src="~/Controls/Logs/CrmOperationLogList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:CrmOperationLogList ID="CrmOperationLogListCtrl" runat="server" />
</asp:Content>