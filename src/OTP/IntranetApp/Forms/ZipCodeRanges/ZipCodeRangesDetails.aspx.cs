﻿using IntranetApp.Enums;
using System;

namespace IntranetApp.Forms.ZipCodeRanges
{
    public partial class ZipCodeRangeDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int intOut = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out intOut))
            {
                this.ZipCodeRangeDetailsCtrl.ZipCodeRangeId = intOut;
            }

            if (!string.IsNullOrEmpty(Request.QueryString["mode"]) && int.TryParse(Request.QueryString["mode"], out intOut))
            {
                this.ZipCodeRangeDetailsCtrl.Mode = (FormMode)intOut;
            }
        }
    }
}