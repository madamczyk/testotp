﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master"  AutoEventWireup="true" CodeBehind="ZipCodeRangesList.aspx.cs" Inherits="IntranetApp.Forms.ZipCodeRanges.ZipCodeRangesList" %>
<%@ Register TagPrefix="uc" TagName="ZipCodeRangesList" Src="~/Controls/ZipCodeRanges/ZipCodeRangesList.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:ZipCodeRangesList ID="ZipCodeRangesListCtrl" runat="server" />
</asp:Content>

