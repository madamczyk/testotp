﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="ZipCodeRangesDetails.aspx.cs" Inherits="IntranetApp.Forms.ZipCodeRanges.ZipCodeRangeDetails" %>
<%@ Register TagPrefix="uc" TagName="ZipCodeRangeDetails" Src="~/Controls/ZipCodeRanges/ZipCodeRangeDetails.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:ZipCodeRangeDetails ID="ZipCodeRangeDetailsCtrl" runat="server" />
</asp:Content>