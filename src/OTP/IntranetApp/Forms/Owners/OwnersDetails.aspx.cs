﻿using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Forms.Owners
{
    public partial class OwnersDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int mode = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["mode"]) && int.TryParse(Request.QueryString["mode"], out mode))
            {
                this.OwnerDetailsCtrl.Mode = (FormMode)mode;
            }
            if (!string.IsNullOrEmpty(Request.QueryString["type"]) && int.TryParse(Request.QueryString["type"], out mode))
            {
                this.OwnerDetailsCtrl.CurrentRoleLevel = mode;
            }
            if (!string.IsNullOrEmpty(Request.QueryString["userId"]) && int.TryParse(Request.QueryString["userId"], out mode))
            {
                this.OwnerDetailsCtrl.UserId = mode;
            }
        }
    }
}