﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="OwnersList.aspx.cs" Inherits="IntranetApp.Forms.OwnersList" %>
<%@ Register TagPrefix="uc" TagName="OwnersList" Src="~/Controls/Owners/OwnersList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:OwnersList ID="OwnersListCtrl" runat="server" />
</asp:Content>