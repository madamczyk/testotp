﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master"  AutoEventWireup="true" CodeBehind="OwnersDetails.aspx.cs" Inherits="IntranetApp.Forms.Owners.OwnersDetails" %>
<%@ Register TagPrefix="uc" TagName="OwnerDetails" Src="~/Controls/Owners/OwnerDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:OwnerDetails ID="OwnerDetailsCtrl" runat="server" />
</asp:Content>

