﻿<%@ Page Language="C#" MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="PropertyList.aspx.cs" Inherits="IntranetApp.Forms.Properties.PropertyList" %>
<%@ Register TagPrefix="uc" TagName="PropertyList" Src="~/Controls/Properties/PropertyList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:PropertyList ID="PropertyListCtrl" runat="server" />
</asp:Content>
