﻿using IntranetApp.Enums;
using System;

namespace IntranetApp.Forms.Properties
{
    public partial class ContentUpdateRequestDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int outInt = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && int.TryParse(Request.QueryString["id"], out outInt))
            {
                this.ContentUpdateRequestDetailsCtrl.ContentUpdateRequestId = outInt;
            }
        }
    }
}