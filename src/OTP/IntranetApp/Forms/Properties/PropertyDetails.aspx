﻿<%@ Page MaintainScrollPositionOnPostback="true" MasterPageFile="~/MainLogged.Master" Language="C#" AutoEventWireup="true" CodeBehind="PropertyDetails.aspx.cs" Inherits="IntranetApp.Forms.Properties.PropertyDetails" EnableViewStateMac="false" %>
<%@ Register TagPrefix="uc" TagName="PropertyDetails" Src="~/Controls/Properties/PropertyDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:PropertyDetails ID="PropertyDetailsCtrl" runat="server" />
</asp:Content>

