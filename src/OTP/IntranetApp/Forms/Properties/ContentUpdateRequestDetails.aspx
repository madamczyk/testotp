﻿<%@ Page MaintainScrollPositionOnPostback="true" MasterPageFile="~/MainLogged.Master" Language="C#" AutoEventWireup="true" CodeBehind="ContentUpdateRequestDetails.aspx.cs" Inherits="IntranetApp.Forms.Properties.ContentUpdateRequestDetails" %>
<%@ Register TagPrefix="uc" TagName="ContentUpdateRequestDetails" Src="~/Controls/Properties/ContentUpdateRequestDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:ContentUpdateRequestDetails ID="ContentUpdateRequestDetailsCtrl" runat="server" />
</asp:Content>

