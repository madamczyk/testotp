﻿<%@ Page Language="C#" MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="ContentUpdateRequestsList.aspx.cs" Inherits="IntranetApp.Forms.Properties.ContentUpdateRequestsList" %>
<%@ Register TagPrefix="uc" TagName="ContentUpdateRequestsList" Src="~/Controls/Properties/ContentUpdateRequestsList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:ContentUpdateRequestsList ID="ContentUpdateRequestsListCtrl" runat="server" />
</asp:Content>
