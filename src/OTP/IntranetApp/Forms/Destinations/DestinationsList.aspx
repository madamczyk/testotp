﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="DestinationsList.aspx.cs" Inherits="IntranetApp.Forms.DestinationsList" %>
<%@ Register TagPrefix="uc" TagName="DestinationsList" Src="~/Controls/Destinations/DestinationsList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:DestinationsList ID="DestinationsListCtrl" runat="server" />
</asp:Content>