﻿using IntranetApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Forms.Destinations
{
    public partial class DestinationsDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int mode = 0;
            
            if (!string.IsNullOrEmpty(Request.QueryString["mode"]) && int.TryParse(Request.QueryString["mode"], out mode))
            {
                this.DestinationDetailsCtrl.Mode = (FormMode)mode;
            }
        }
    }
}