﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master"  AutoEventWireup="true" CodeBehind="DestinationsDetails.aspx.cs" Inherits="IntranetApp.Forms.Destinations.DestinationsDetails" %>
<%@ Register TagPrefix="uc" TagName="DestinationDetails" Src="~/Controls/Destinations/DestinationDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:DestinationDetails ID="DestinationDetailsCtrl" runat="server" />
</asp:Content>

