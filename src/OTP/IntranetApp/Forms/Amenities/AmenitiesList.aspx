﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="AmenitiesList.aspx.cs" Inherits="IntranetApp.Forms.Amenities.AmenitiesList" %>
<%@ Register TagPrefix="uc" TagName="AmenitiesList" Src="~/Controls/Amenities/AmenitiesList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:AmenitiesList ID="AmenitiesListCtrl" runat="server" />
</asp:Content>