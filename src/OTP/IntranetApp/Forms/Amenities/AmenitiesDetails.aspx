﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master"  AutoEventWireup="true" CodeBehind="AmenitiesDetails.aspx.cs" Inherits="IntranetApp.Forms.Amenities.AmenitiesDetails" %>
<%@ Register TagPrefix="uc" TagName="AmenityDetails" Src="~/Controls/Amenities/AmenityDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:AmenityDetails ID="AmenityDetailsCtrl" runat="server" />
</asp:Content>

