﻿<%@ Page MasterPageFile="~/MainLogged.Master"  Language="C#" AutoEventWireup="true" CodeBehind="NotificationsList.aspx.cs" Inherits="IntranetApp.Forms.Notifications.NotificationsList" %>
<%@ Register TagPrefix="uc" TagName="Notifications" Src="~/Controls/Notifications/NotificationsList.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:Notifications ID="NotificationsCtrl" runat="server" />
</asp:Content>
