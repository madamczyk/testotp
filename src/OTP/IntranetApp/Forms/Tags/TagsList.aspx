﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="TagsList.aspx.cs" Inherits="IntranetApp.Forms.TagsList" %>
<%@ Register TagPrefix="uc" TagName="TagsList" Src="~/Controls/Tags/TagsList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:TagsList ID="TagsListCtrl" runat="server" />
</asp:Content>