﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master"  AutoEventWireup="true" CodeBehind="TagsDetails.aspx.cs" Inherits="IntranetApp.Forms.Tags.TagsDetails" %>
<%@ Register TagPrefix="uc" TagName="TagDetails" Src="~/Controls/Tags/TagDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:TagDetails ID="TagDetailsCtrl" runat="server" />
</asp:Content>

