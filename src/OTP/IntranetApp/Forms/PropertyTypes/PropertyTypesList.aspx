﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="PropertyTypesList.aspx.cs" Inherits="IntranetApp.Forms.PropertyTypesList" %>
<%@ Register TagPrefix="uc" TagName="PropertyTypesList" Src="~/Controls/PropertyTypes/PropertyTypesList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:PropertyTypesList ID="PropertyTypesListCtrl" runat="server" />
</asp:Content>