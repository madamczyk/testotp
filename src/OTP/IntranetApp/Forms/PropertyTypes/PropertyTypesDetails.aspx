﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master"  AutoEventWireup="true" CodeBehind="PropertyTypesDetails.aspx.cs" Inherits="IntranetApp.Forms.PropertyTypes.PropertyTypesDetails" %>
<%@ Register TagPrefix="uc" TagName="PropertyTypeDetails" Src="~/Controls/PropertyTypes/PropertyTypeDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:PropertyTypeDetails ID="PropertyTypeDetailsCtrl" runat="server" />
</asp:Content>

