﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master" AutoEventWireup="true" CodeBehind="AmenityGroupsList.aspx.cs" Inherits="IntranetApp.Forms.AmenityGroupsList" %>
<%@ Register TagPrefix="uc" TagName="AmenityGroupsList" Src="~/Controls/AmenityGroups/AmenityGroupsList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:AmenityGroupsList ID="AmenityGroupsListCtrl" runat="server" />
</asp:Content>