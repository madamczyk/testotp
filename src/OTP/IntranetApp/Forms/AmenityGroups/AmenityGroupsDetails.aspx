﻿<%@ Page Language="C#"  MasterPageFile="~/MainLogged.Master"  AutoEventWireup="true" CodeBehind="AmenityGroupsDetails.aspx.cs" Inherits="IntranetApp.Forms.AmenityGroups.AmenityGroupsDetails" %>
<%@ Register TagPrefix="uc" TagName="AmenityGroupsDetails" Src="~/Controls/AmenityGroups/AmenityGroupDetails.ascx" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <uc:AmenityGroupsDetails ID="AmenityGroupsDetailsCtrl" runat="server" />
</asp:Content>

