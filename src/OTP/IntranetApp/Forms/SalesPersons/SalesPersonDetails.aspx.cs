﻿using IntranetApp.Enums;
using System;

namespace IntranetApp.Forms.SalesPersons
{
    public partial class SalesPersonDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int mode = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["mode"]) && int.TryParse(Request.QueryString["mode"], out mode))
            {
                this.ucSalesPersonDetails.Mode = (FormMode)mode;
            }
        }
    }
}