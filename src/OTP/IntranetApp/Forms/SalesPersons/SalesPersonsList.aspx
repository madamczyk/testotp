﻿<%@ Page Language="C#" MasterPageFile="~/MainLogged.master" AutoEventWireup="true" CodeBehind="SalesPersonsList.aspx.cs" Inherits="IntranetApp.Forms.SalesPersons.SalesPersonsList" %>
<%@ Register TagPrefix="uc" TagName="SalesPersonsList" Src="~/Controls/SalesPersons/SalesPersonsList.ascx" %>

<asp:Content ID="cphMainContent" ContentPlaceHolderID="mainContent" runat="server">
    <uc:SalesPersonsList ID="ucSalesPersonsList" runat="server" />
</asp:Content>

