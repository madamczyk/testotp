﻿<%@ Page Language="C#" MasterPageFile="~/MainLogged.master" AutoEventWireup="true" CodeBehind="SalesPersonDetails.aspx.cs" Inherits="IntranetApp.Forms.SalesPersons.SalesPersonDetails" %>
<%@ Register TagPrefix="uc" TagName="SalesPersonDetails" Src="~/Controls/SalesPersons/SalesPersonDetails.ascx" %>

<asp:Content ID="cphMainContent" ContentPlaceHolderID="mainContent" runat="server">
    <uc:SalesPersonDetails ID="ucSalesPersonDetails" runat="server" />
</asp:Content>
