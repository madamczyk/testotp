﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using IntranetApp;
using BusinessLogic.Managers;
using DataAccess.Enums;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using Common.Exceptions;
using BusinessLogic.Services;

namespace IntranetApp
{
    public class Global : HttpApplication
    {
        /// <summary>
        /// Page not found error code
        /// </summary>
        private const int ERROR_404 = 404;

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            //AuthConfig.RegisterOpenAuth();
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
        }

        void Application_Error(object sender, EventArgs e)
        {
            //Attempt to prevent re-entry of invalid values ​​to the database
            //Services will be re-created in next use
            RequestManager.Services = null;

            Exception exception = Server.GetLastError();

            string errorMessage = ExceptionHelper.FormatErrorMessage(exception);
            try
            {
                if (exception != null && exception is HttpException)
                {
                    HttpException httpException = (HttpException)exception;
                    if (httpException.GetHttpCode().Equals(ERROR_404)) // log only as warning on page not found error (404)
                    {
                        Trace.Write(errorMessage, EventCategory.Warning.ToString());
                    }
                    else
                    {
                        Trace.Write(errorMessage, EventCategory.Error.ToString());
                    }
                }
                else
                {
                    Trace.Write(errorMessage, EventCategory.Error.ToString());
                }
            }
            catch (Exception)
            {
                RequestManager.Services.EventLogService.LogError(exception.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.Intranet);
            }   

            RequestManager.Services.SaveChanges();
        }

        protected void Application_BeginRequest()
        {
            string currentCulture = string.Empty;
            if (SessionManager.IsAuthenticated())
                currentCulture = SessionManager.CurrentUser.CultureCode;
            else
            {
                if (HttpContext.Current.Request.UserLanguages != null && !string.IsNullOrEmpty(HttpContext.Current.Request.UserLanguages[0]))
                    currentCulture = HttpContext.Current.Request.UserLanguages[0];
                else
                    currentCulture = "en-US";
            }

            CultureInfo ci = new CultureInfo(currentCulture);
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            RequestManager.Services.SaveChanges();
        }
    }
}
