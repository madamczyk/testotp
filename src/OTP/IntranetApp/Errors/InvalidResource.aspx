﻿<%@ Page MasterPageFile="~/MainNotLogged.master" Language="C#" AutoEventWireup="true" CodeBehind="InvalidResource.aspx.cs" Inherits="IntranetApp.Errors.InvalidResource" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <h1>
        <asp:Label ID="lblErrorHeader" Text="<%$ Resources: ErrorPage, InvalidResourceHeader %>" runat="server"></asp:Label>
    </h1>
    <br />
    <br />
    <asp:Label ID="lblErrorIDInfo" runat="server"></asp:Label>
</asp:Content>
