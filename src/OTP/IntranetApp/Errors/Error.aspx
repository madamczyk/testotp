﻿<%@ Page MasterPageFile="~/MainNotLogged.master" Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="IntranetApp.Errors.Error" %>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <h1>
        <asp:Label ID="lblErrorHeader" text="<%$ Resources: ErrorPage, Header %>" runat="server"></asp:Label>
    </h1>
    <br />
    <br />
    <asp:Label ID="lblErrorIDInfo" runat="server"></asp:Label>
</asp:Content>
