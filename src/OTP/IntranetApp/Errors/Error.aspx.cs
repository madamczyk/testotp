﻿using BusinessLogic.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Errors
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ClearError();
            string errorId = RequestManager.Services.EventLogService.GetLastError().CorrelationId.ToString();
            lblErrorIDInfo.Text = string.Format(GetGlobalResourceObject("ErrorPage", "ErrorIDInfo").ToString(), errorId);
        }
    }
}