﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntranetApp.Errors
{
    public partial class InvalidResource : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblErrorIDInfo.Text = GetGlobalResourceObject("ErrorPage", "Error404").ToString();
        }
    }
}