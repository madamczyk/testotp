﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="IntranetApp.Account.Login" %>

<%@ Register TagPrefix="uc" TagName="Login" Src="~/Controls/Users/Login.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <uc:Login ID="LoginCtrl" runat="server" />
</asp:Content>