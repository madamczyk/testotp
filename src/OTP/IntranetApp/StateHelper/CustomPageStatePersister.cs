﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using BusinessLogic.Managers;

namespace IntranetApp.StateHelper
{
    public class CustomPageStatePersister : HiddenFieldPageStatePersister
    {
        // A Prefix used for Cache Key.
        private const string VIEWSTATE_CACHEKEY_PREFIX = "__VIEWSTATE-";
        private const string VIEWSTATE_SQLCONNECTION_NAME = "COE_ViewStatePersister";

        // Load the Configuration from web.config
        CustomPageStatePersisterConfiguration configuration = new CustomPageStatePersisterConfiguration();


        // Call base constructor
        public CustomPageStatePersister(Page page) : base(page)
        {
        
        }

        /// <summary>
        /// Method that loads the ViewState from various sources and plugs it back on the page.
        /// </summary>
        public override void Load()
        {
            // Load Whatever is there in the __VIEWSTATE hidden variables.
            base.Load();

            // Check if the configuration is set to be switched OFF or none of the Custom persisters are set.
            if (configuration.IsSwitchOff || (!configuration.IsSqlPersisted && !configuration.IsCached && !configuration.IsCompressed))
                return;                        
            
            // Check if the Page configuration demands that only flaged pages need to be custom persisted 
            // & the current page implements ICustomStatePersistedPage
            if ((configuration.IsOnCustomPageOnly && !(base.Page is ICustomStatePersistedPage)) ||  (base.Page is IDefaultStatePersistedPage))
                return;
         

            string currentViewState = base.ViewState.ToString();

            // Lets check if the ViewState is a GUID. 
            // Dont need to check anything except the fact that the String is exactly 36 char.
            if (currentViewState.Length == 36)
            {
                CustomViewState vs = null;

                // If Caching is switch on then try to retrieve from cache.
                if (configuration.IsSqlPersisted)
                {
                    // Form a Cache Key.
                    string cacheKey = VIEWSTATE_CACHEKEY_PREFIX + base.ViewState;
                    try
                    {
                        vs = GetFromSession(cacheKey);
                    }
                    catch
                    {
                        return;
                    }
                }
                // Deserialize the cache using the Formatter. 
                Deserialize(vs.Data);
            }
        }

        /// <summary>
        /// Saves the current ViewState into the database/cache as per configuration.
        /// </summary>
        public override void Save()
        {
            // Check if the configuration is set to be switched OFF or none of the Custom persisters are set.
            if (configuration.IsSwitchOff || (!configuration.IsSqlPersisted && !configuration.IsCached && !configuration.IsCompressed))
            {
                base.Save();
                return;
            }

            // Check if the Page configuration demands that only flaged pages need to be custom persisted 
            // & the current page implements ICustomStatePersistedPage
            if ((configuration.IsOnCustomPageOnly && !(base.Page is ICustomStatePersistedPage)) || (base.Page is IDefaultStatePersistedPage))
            {
                base.Save();
                return;
            }
            // Create the CustomViewState with data needed to save.
            CustomViewState vs = new CustomViewState
            {
                Id = Guid.NewGuid().ToString(),
                Data = Serialize(),
                TimeStamp = DateTime.Now
            };

            // Save to SQL if IsSqlPersisted is True
            if (configuration.IsSqlPersisted)
                SaveToSession(vs, VIEWSTATE_CACHEKEY_PREFIX + vs.Id);
                
            // Replace the actual ViewState going to the page with the GUID.
            base.ViewState = vs.Id;
            base.ControlState = "";
            
            // Call base class method to do its job.
            base.Save();
        }

        public string Serialize()
        {
            Pair vsPair = new Pair(base.ViewState, base.ControlState);
            return base.StateFormatter.Serialize(vsPair);
        }

        public void Deserialize(string serializedViewState)
        {
            object vsPair =  base.StateFormatter.Deserialize(serializedViewState) as Pair;
            if (vsPair != null && (vsPair is Pair))
            {
                Pair myPair = vsPair as Pair;
                base.ViewState = myPair.First;
                base.ControlState = myPair.Second;
            }
            else
            {
                base.ViewState = vsPair;
            }
        }

        public static void SaveToSession(CustomViewState vs, string key)
        {
            if (vs != null && !string.IsNullOrEmpty(vs.Id) && !string.IsNullOrEmpty(vs.Data))
            {
                RequestManager.Services.StateService.AddToSession(key, vs);
            }
        }

        public static CustomViewState GetFromSession(string key)
        {
            if (string.IsNullOrEmpty(key) || RequestManager.Services.StateService.IsNullInSession(key))
            {
                return null;
            }

            return RequestManager.Services.StateService.GetFromSession<CustomViewState>(key);
        }
    }
}
