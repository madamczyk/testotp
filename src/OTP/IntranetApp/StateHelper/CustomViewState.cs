﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetApp.StateHelper
{
    [Serializable()]
    public class CustomViewState
    {
        public string Id { get; set; }
        public string Data { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}