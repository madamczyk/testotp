﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.Adapters;

namespace IntranetApp.StateHelper
{
    public class CustomPageAdapter : PageAdapter
    {
        public override System.Web.UI.PageStatePersister GetStatePersister()
        {
            return new CustomPageStatePersister(Page);
           //return base.GetStatePersister();
        }
    }
}