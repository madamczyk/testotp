﻿using ExtranetApp.Extensions;
using ExtranetApp.Resources.Models;
using ModelMetadataExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Models.Guest
{
    // All localization strings are taken from Common resx file.
    // Localization Names with HomeownerModel_<PropertyName>_<Attribute> 
    // (HomeownerModel_ConfirmPassword_Compare)
    [Serializable]
    [MetadataConventionsAttribute(ResourceType = typeof(Resources.Models.UserProfile))]
    public class GuestProfileModel
    {
        public int UserId { get; set; }

        [Required]
        [StringLength(250)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(250)]
        public string LastName { get; set; }

        [StringLength(250)]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        public string Email { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(250)]
        public string AddressLine1 { get; set; }

        [StringLength(250)]
        public string AddressLine2 { get; set; }

        [StringLength(250)]
        public string State { get; set; }

        [StringLength(250)]
        public string City { get; set; }

        [StringLength(250)]
        public string ZIPCode { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        public int Country { get; set; }

        public string CountryName { get; set; }

        public List<DataAccess.GuestPaymentMethod> CreditCards { get; set; }

        public string FullName { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        public int UserCulture { get; set; }

        public string UserCultureName { get; set; }

        public int CreditCardToDelete { get; set; }

        public IEnumerable<int> CreditCardsInUse { get; set; }

        public bool SendInfoFavoritePlaces { get; set; }

        public bool SendNews { get; set; }

        public bool SendMePromotions { get; set; }
    }
}