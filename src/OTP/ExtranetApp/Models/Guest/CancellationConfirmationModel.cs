﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.Guest
{
    public class CancellationConfirmationModel
    {
        public String userEmail;
        public String message;
        public String cancellationCode;
    }
}