﻿using DataAccess;
using ModelMetadataExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.CustomObjects;

namespace ExtranetApp.Models.Guest
{
    [Serializable]
    [MetadataConventionsAttribute(ResourceType = typeof(Resources.Models.UserProfile))]
    public class MyTripsModel
    {
        public IEnumerable<ReservationForUser> reservations { get; set; }
        public User User { get; set; }
    }
}
