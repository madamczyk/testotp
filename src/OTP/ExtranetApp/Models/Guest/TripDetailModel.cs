﻿using BusinessLogic.Objects.StaticContentRepresentation;
using DataAccess;
using System;
using System.Collections.Generic;

namespace ExtranetApp.Models.Guest
{
    [Serializable]
    public class TripDetailModel
    {
        public string AlertCode { get; set; }
        public string AlertLocation { get; set; }
        public IEnumerable<PropertyStaticContent> ArrivalInformations { get; set; }
        public IEnumerable<DirectionToAirport> Directions { get; set; }
        public User KeyMenager { get; set; }
        public int KeyProcess { get; set; }
        public int LengthOfStay { get; set; }
        public string LockBoxCode { get; set; }
        public string LockBoxLocation { get; set; }
        public IEnumerable<PropertyStaticContent> Parkings { get; set; }
        public decimal PricePerNight { get; set; }
        public Reservation Reservation { get; set; }
        public int ReservationId { get; set; }
        public User User { get; set; }
        public IEnumerable<UnitStaticContent> WifiName { get; set; }
        public IEnumerable<UnitStaticContent> WifiPass { get; set; }

        public Reservation mostExpensiveReservation { get; set; }
    }
}
