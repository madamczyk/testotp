﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Models.Guest
{
    [Serializable]
    public class InvoiceDetailsModel
    {
        public DataAccess.CustomObjects.PropertyDetailsInvoice invoiceDetail;
        public decimal originalReservationTotalPrice;
    }
}
