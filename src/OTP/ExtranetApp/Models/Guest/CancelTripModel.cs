﻿using DataAccess;
using DataAccess.CustomObjects;
using ModelMetadataExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ExtranetApp.Models.Guest
{
    [Serializable]
    [MetadataConventionsAttribute(ResourceType = typeof(Resources.Models.UserProfile))]
    public class CancelTripModel
    {
        public Reservation reservation { get; set; }
        public IEnumerable<String> Thumbnails { get; set; }
        public User User { get; set; }
        public Int32 ReservationId { get; set; }
        public String errorMessage { get; set; }
    }
}