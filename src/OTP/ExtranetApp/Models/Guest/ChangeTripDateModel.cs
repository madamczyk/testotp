﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.Guest
{
    [Serializable]
    public class ChangeTripDateModel
    {
        public int reservationId { get; set; }
        public Reservation reservation { get; set; }
        public IEnumerable<String> Thumbnails { get; set; }
        public User user { get; set; }
        public DateTime DateArrival { get; set; }
        public DateTime DateDeparture { get; set; }

    }
}