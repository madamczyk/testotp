﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models
{
    public class GroupedObjectItem
    {
        public string GroupName { get; set; }
        public int GroupId { get; set; }
        public List<ObjectItem> Items { get; set; }
    }
}