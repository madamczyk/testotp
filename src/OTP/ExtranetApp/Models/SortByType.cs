﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models
{
    public enum SortByType
    {
        PriceDesc,
        PriceAsc,
        SleepDesc,
        SleepAsc,
        RatingDesc,
        RatingAsc,
        TimeDesc
    }
}