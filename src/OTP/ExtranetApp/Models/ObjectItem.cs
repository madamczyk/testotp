﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models
{
    public class ObjectItem
    {
        public int ID { get; set; }
        public string Value { get; set; }
        public bool Selected { get; set; }                
    }
}