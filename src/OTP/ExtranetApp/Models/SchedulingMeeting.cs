﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models
{
    [Serializable()]
    public class SchedulingMeeting
    {
        [Required]
        public string Dates { get; set; }

        [Required]
        public string DateFrom { get; set; }

        [Required]
        public string DateTo { get; set; }
    }
}