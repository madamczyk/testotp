﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models
{
    public class ChangeMlosesModel
    {
        public int UnitId { get; set; }

        public UnitTypeMLO[] UnitObjectsToUpdate { get; set; }

        public int[] UnitObjectsToDelete { get; set; }
    }
}