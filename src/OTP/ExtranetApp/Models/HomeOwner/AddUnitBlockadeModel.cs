﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess.Enums;
using System.ComponentModel.DataAnnotations;

namespace ExtranetApp.Models.HomeOwner
{
    public class AddUnitBlockadeModel
    {
        [Required]
        public int UnitId { get; set; }

        [Required]
        public DateTime DateFrom { get; set; }

        [Required]
        public DateTime DateTo { get; set; }

        [Required]
        public UnitBlockingType UnitBlockingType { get; set; }

        public string Comment { get; set; }
    }
}