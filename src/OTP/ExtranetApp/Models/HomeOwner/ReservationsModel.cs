﻿using DataAccess;
using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.HomeOwner
{
    public class ReservationsModel
    {
        public int PropertyId { get; set; }
        public Property Property { get; set; }
        public Unit Unit { get; set; }
        public IEnumerable<ReservationForHomeOwner> Reservations { get; set; }

        public ReservationsModel()
        {
            Reservations = new List<ReservationForHomeOwner>();
        }
    }
}