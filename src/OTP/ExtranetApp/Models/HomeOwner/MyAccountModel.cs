﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.HomeOwner
{
    public class MyAccountModel
    {
        public OwnerUpdateProfileModel Owner { get; set; }
        public IEnumerable<PropertyWithImages> PropertyWithImages { get; set; }

        public MyAccountModel()
        {
            PropertyWithImages = new List<PropertyWithImages>();
        }


    }
}