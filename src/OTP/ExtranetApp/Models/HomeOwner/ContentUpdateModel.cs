﻿using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.HomeOwner
{
    public class ContentUpdateModel
    {
        public PropertyWithImages propWithImages { get; set; }
        public ContentUpdateRequestDetailContentType updateContentCode { get; set; }
        public String message { get; set; }
        public String storageLinks { get; set; }
        public int propertyId { get; set; }


        public enum ContentUpdateType
        {
            images = 0,
            other = 1,
            both = 2
        }
    }
}