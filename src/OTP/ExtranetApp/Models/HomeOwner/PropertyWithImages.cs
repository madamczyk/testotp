﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.HomeOwner
{
    public class PropertyWithImages
    {
        public Property Property { get; set; }
        public string FullScreenImage { get; set; }
        public string FullScreenImageRetina { get; set; }
    }
}