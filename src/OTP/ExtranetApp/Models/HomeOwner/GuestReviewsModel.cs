﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.HomeOwner
{
    public class GuestReviewsModel
    {
        public int PropertyRating { get; set; }

        public DataAccess.Property Property { get; set; }

        public IEnumerable<GuestReview> GuestReviews { get; set; }

        public GuestReviewsModel()
        {
            GuestReviews = new List<GuestReview>();
        }
    }
}