﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.HomeOwner
{
    public class KeyManagementModel
    {
        public IEnumerable<KeyManagementData> KeyManagementList { get; set; }

        public KeyManagementModel()
        {
            KeyManagementList = new List<KeyManagementData>();
        }
    }
}