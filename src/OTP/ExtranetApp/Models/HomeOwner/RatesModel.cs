﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.HomeOwner
{
    public class RatesModel
    {
        public int PropertyId { get; set; }

        public Property Property { get; set; }

        public Unit Unit { get; set; }

        public string PropertyCurrency { get; set; }
    }
}