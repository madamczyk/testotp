﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.HomeOwner
{
    public class TransactionModel
    {
        public IEnumerable<DataAccess.CustomObjects.ReservationForHomeOwner> ReservationsList { get; set; }

        public TransactionModel()
        {
            ReservationsList = new List<ReservationForHomeOwner>();
        }
    }
}