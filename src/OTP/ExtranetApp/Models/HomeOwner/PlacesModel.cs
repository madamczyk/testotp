﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.HomeOwner
{
    public class PlacesModel
    {
        public Property Property { get; set; }
        public Unit Unit { get; set; }
        public IEnumerable<string> Thumbnails { get; set; }

        public PlacesModel()
        {
            Thumbnails = new List<string>();
        }
    }
}