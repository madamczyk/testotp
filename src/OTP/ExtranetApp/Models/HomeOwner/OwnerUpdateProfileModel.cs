﻿using ExtranetApp.Extensions;
using ExtranetApp.Resources.Booking;
using ExtranetApp.Resources.Models;
using ModelMetadataExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Models.HomeOwner
{
    [Serializable]
    [MetadataConventionsAttribute(ResourceType=typeof(Resources.Models.UserProfile))]
    public class OwnerUpdateProfileModel : IValidatableObject
    {
        public int UserId { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250)]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250)]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250)]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(50)]
        public string PhoneNumber { get; set; }       

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250)]
        public string AddressLine1 { get; set; }

        [StringLength(250)]
        public string AddressLine2 { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250)]
        public string State { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250)]
        public string City { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250)]
        public string ZIPCode { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]       
        public int Country { get; set; }
        
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]       
        public string CountryCode2Letters { get; set; }

        public string CountryName { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250)]
        public string BankName { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [RegularExpression(@"^\d+$", ErrorMessageResourceName = "Validation_WrongFormat", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(9, MinimumLength = 9, ErrorMessageResourceName = "Validation_StringLengthExact", ErrorMessageResourceType = typeof(ValidationShared))]
        [RoutingNumberChecksum]
        public string RoutingNumber { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250)]
        public string AccountNumber { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (CountryCode2Letters.Equals("us"))
            {
                if (ZIPCode.Length != 5 || !Regex.IsMatch(ZIPCode, "^[0-9]{5}$"))
                    yield return new ValidationResult(BookingProcess.Validation_UsZip, new string[] { "ZIPCode" });
            }
        }
    }
}