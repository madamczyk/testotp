﻿using ExtranetApp.Extensions;
using ExtranetApp.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Models.HomeOwner
{
    /// <summary>
    /// Model class for Homeowner activation form - it gathers all homeowners bank account related data
    /// </summary>
    public class ActivateOwnerBankAccountRelatedModel
    {
        /// <summary>
        /// Activation token value - only hidden input
        /// </summary>
        [Required]        
        [HiddenInput(DisplayValue=false)]
        public string ActivationToken { get; set; }

        /// <summary>
        /// Name of the homeowners bank
        /// </summary>
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(255)]
        public string BankName { get; set; }

        /// <summary>
        /// Account routing number (swift)
        /// </summary>
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [RegularExpression(@"^\d+$", ErrorMessageResourceName = "Validation_WrongFormat", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(9, MinimumLength = 9, ErrorMessageResourceName = "Validation_StringLengthExact", ErrorMessageResourceType = typeof(ValidationShared))]
        [RoutingNumberChecksum]
        public string RoutingNumber { get; set; }

        /// <summary>
        /// Account number
        /// </summary>
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(255)]
        public string AccountNumber { get; set; }
    }
}