﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models
{
    public class SearchFilterModel
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? Guests { get; set; }
        public int? Bedrooms { get; set; }
        public int? Bathrooms { get; set; }
        public ObjectItem Location { get; set; }
        public IEnumerable<ObjectItem> Destinations  { get; set; }
        public IEnumerable<GroupedObjectItem> Amenities { get; set; }
        public IEnumerable<ObjectItem> PropertyTypes { get; set; }
        public IEnumerable<GroupedObjectItem> Experiences { get; set; }
        public IEnumerable<PropertiesSearchResult> SearchResults { get; set; }    
    }
}