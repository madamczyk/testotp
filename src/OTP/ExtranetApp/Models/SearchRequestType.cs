﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtranetApp.Models
{
    public enum SearchRequestType
    {
        Unknown, 
        Scroll, 
        Sort
    }
}
