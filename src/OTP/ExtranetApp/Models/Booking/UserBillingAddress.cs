﻿using ExtranetApp.Resources.Booking;
using ExtranetApp.Resources.Models;
using ModelMetadataExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ExtranetApp.Models.Booking
{
    [Serializable]
    [MetadataConventionsAttribute(ResourceType = typeof(Resources.Models.UserProfile))]
    public class UserBillingAddress : IValidatableObject
    {
        public int UserId { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250, ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250, ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250, ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string AddressLine1 { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250, ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string State { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250, ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string City { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(250, ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string ZIPCode { get; set; }

        /// <summary>
        /// Country Code (2 letters)
        /// </summary>
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        public string Country { get; set; }
                
        public virtual DateTime? BirthDay { get; set; }
        public string CountryName { get; set; }

        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Country.Equals("us"))
            {
                if (ZIPCode.Length != 5 || !Regex.IsMatch(ZIPCode, "^[0-9]{5}$"))
                    yield return new ValidationResult(BookingProcess.Validation_UsZip, new string[] { "ZIPCode" });
            }
        }
    }
}