﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.Booking
{
    [Serializable]
    public class IdentityVerificationAnswerModel
    {
        public String AnswerText { get; set; }
        public Boolean IsCorrect { get; set; }
        public Int32 AnswerId { get; set; }
    }
}