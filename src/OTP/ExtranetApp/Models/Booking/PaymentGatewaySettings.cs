﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtranetApp.Models.Booking
{
    public class PaymentGatewaySettings
    {
        public string RedirectURL { get; set; }
        public string ApiAuthKey { get; set; }
        public string ApiLogin { get; set; }
        public string ApiSecret { get; set; }
    }
}
