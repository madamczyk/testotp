﻿using ExtranetApp.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.Booking
{
    [Serializable]
    public class IdentityVerificationQuestionModel
    {
        public String QuestionText { get; set; }
        public Int32 QuestionType { get; set; }
        [Required(ErrorMessageResourceName = "Answer_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        public Int32 SelectedAnswer { set; get; }
        public List<IdentityVerificationAnswerModel> Answers { get; set; }

        public IdentityVerificationQuestionModel()
        {
            Answers = new List<IdentityVerificationAnswerModel>();
        }
    }
}