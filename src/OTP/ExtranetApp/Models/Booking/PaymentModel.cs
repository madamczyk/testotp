﻿using ExtranetApp.Extensions;
using ExtranetApp.Resources.Booking;
using ExtranetApp.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.Booking
{
    public class PaymentModel
    {
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        public int? GuestPaymentMethodId { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        public int? CreditCardholderId { get; set; }

        [Display(Name = "Payment_CreditCardNumber", ResourceType = typeof(BookingProcess))]
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(16, MinimumLength = 15, ErrorMessageResourceName = "Validation_StringLengthCustom", ErrorMessageResourceType = typeof(ValidationShared))]
        public string CreditCardNumber { get; set; }
        
        public string CreditCardLast4Digits { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(3, MinimumLength = 3, ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string PinCode { get; set; }

        public string ReferenceToken { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        public int ExpirationMonthDate { get; set; }
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        public int ExpirationYearDate { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        public CreditCardSelection? CreditCardSelection { get; set; }

        public int UserId { get; set; }
    }

    public enum CreditCardSelection
    {
        New = 0,
        Existing = 1
    }
}