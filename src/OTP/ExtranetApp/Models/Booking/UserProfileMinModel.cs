﻿using ExtranetApp.Extensions;
using ExtranetApp.Resources.Booking;
using ExtranetApp.Resources.Models;
using ModelMetadataExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Resources;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Models.Booking
{
    // All localization strings are taken from Common resx file.
    // Localization Names with HomeownerModel_<PropertyName>_<Attribute> 
    // (HomeownerModel_ConfirmPassword_Compare)
    [Serializable]
    [MetadataConventionsAttribute(ResourceType=typeof(Resources.Models.UserProfile))]
    public class UserProfileMinModel : UserBillingAddress, IValidatableObject
    {       
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))] 
        [StringLength(250,ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))] 
        [StringLength(50,ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string PhoneNumber { get; set; }

        [StringLength(250, ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string AddressLine2 { get; set; }        

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))] 
        public string Gender { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]        
        public override DateTime? BirthDay { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Country.Equals("us"))
            {
                if (ZIPCode.Length != 5 || !Regex.IsMatch(ZIPCode, "^[0-9]{5}$"))
                    yield return new ValidationResult(BookingProcess.Validation_UsZip, new string[] { "ZIPCode" });
                if (PhoneNumber.Length != 10 || !Regex.IsMatch(PhoneNumber, "^[0-9]{10}$"))
                    yield return new ValidationResult(BookingProcess.Validation_UsPhone, new string[] { "PhoneNumber" });
            }
        }
    }
}