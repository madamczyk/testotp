﻿using DataAccess;
using DataAccess.CustomObjects;
using ExtranetApp.Resources.Booking;
using ExtranetApp.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.Booking
{
    public class BookingProcessModel
    {
        public Property Property { get; set; }
        public Unit Unit { get; set; }
        public IEnumerable<String> PropertyImages { get; set; }
        public string PropertyUnitName { get; set; }


        public BookingProcessModel()
        {            
            PropertyImages = new List<string>();
            VerificationData = new PaymentAgeTermsVerification();
        }

        public BookingInfoModel BookingInfoData { get; set; }

        public Destination DestinationDetails { get; set; }

        public Account.LoginModel LoginModel { get; set; }

        public Account.JoinModel JoinModel { get; set; }

        public UserProfileMinModel GuestProfile { get; set; }

        public UserBillingAddress BillingAddress { get; set; }

        public PaymentModel PaymentData { get; set; }

        public PaymentAgeTermsVerification VerificationData { get; set; }

        public IList<IdentityVerificationQuestionModel> IdentityVerification { get; set; }

        public PropertyDetailsInvoice InvoiceData { get; set; }

        public BusinessLogic.Objects.PaymentResult PaymentGateWayResult { get; set; }

        public PaymentGatewaySettings PaymentGatewaySettings { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [RegularExpression(@"^\d+$", ErrorMessageResourceName = "Validation_WrongFormat", ErrorMessageResourceType = typeof(ValidationShared))]
        [Range(1, 9, ErrorMessageResourceName = "Validation_Range", ErrorMessageResourceType = typeof(ValidationShared))]
        public int? NumberOfGuests { get; set; }
    }
}