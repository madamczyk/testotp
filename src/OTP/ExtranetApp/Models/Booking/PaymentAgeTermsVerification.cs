﻿using ExtranetApp.Extensions;
using ModelMetadataExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ExtranetApp.Resources.Booking;
using ExtranetApp.Resources.Models;

namespace ExtranetApp.Models.Booking
{
    [Serializable]
    [MetadataConventionsAttribute(ResourceType = typeof(Resources.Models.PaymentAgeTermsVerification))]
    public class PaymentAgeTermsVerification
    {
        [BooleanMustBeTrueValidation(ErrorMessageResourceName="PaymentAgeTermsVerification_AgeVerification", ErrorMessageResourceType=typeof(ExtranetApp.Resources.Models.PaymentAgeTermsVerification))]
        [AgeValidation]
        public bool AgeVerification { get; set; }
        [BooleanMustBeTrueValidation(ErrorMessageResourceName = "PaymentAgeTermsVerification_TermsReadVerificaction", ErrorMessageResourceType = typeof(ExtranetApp.Resources.Models.PaymentAgeTermsVerification))]
        public bool TermsReadVerificaction { get; set; }
        [BooleanMustBeTrueValidation(ErrorMessageResourceName = "PaymentAgeTermsVerification_CancellationPolicy", ErrorMessageResourceType = typeof(ExtranetApp.Resources.Models.PaymentAgeTermsVerification))]
        public bool CancellationPolicyAgree { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(4, MinimumLength = 2, ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string UserInitials { get; set; }

    }
}