﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.PropertyDetails
{
    public class SignOffModel
    {
        public Property Property { get; set; }
        public string SignOffToken { get; set; }
    }
}