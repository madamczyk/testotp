﻿using BusinessLogic.Objects;
using BusinessLogic.Objects.StaticContentRepresentation;
using DataAccess;
using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.PropertyDetails
{
    public class PropertyDetailsModel
    {        
        public Property Property { get; set; }
        public Unit Unit { get; set; }
        public IEnumerable<String> PropertyImages { get; set; }
        public IEnumerable<String> GalleryImages { get; set; }
        public IEnumerable<String> BedroomsDesc { get; set; }
        public IEnumerable<String> KitchenDesc { get; set; }
        public IEnumerable<String> BathroomsDesc { get; set; }
        public IEnumerable<String> LivingArea { get; set; }
        public IEnumerable<String> Features { get; set; }
        public IEnumerable<string> DistanceFormAttractions { get; set; }
        public IEnumerable<Appliance> Appliances { get; set; }
        public IEnumerable<ApplianceGroup> ApplianceGroups { get; set; }
        public IEnumerable<PropertyAmenities> Amenities { get; set; }
        public IEnumerable<FloorPlan> FloorPlans { get; set; }

        public Int32 PropertyRating { get; set; }
        public string SquareFootage { get; set; }

        public IEnumerable<GuestReview> GuestReviews { get; set; }
        public Destination DestinationDetails { get; set; }
        public Int32 NumberOfPropertyUnits { get; set; }
        public string PropertyUnitName { get; set; }
        public string StorageUrl { get; set; }

        public PropertyDetailsModel()
        {            
            PropertyImages = new List<string>();
            GalleryImages = new List<string>();
            Appliances = new List<Appliance>();
            GuestReviews = new List<GuestReview>();
            PropertyRating = 0;
        }

        
    }
}