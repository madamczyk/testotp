﻿using BusinessLogic.Objects;
using BusinessLogic.Objects.StaticContentRepresentation;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.PropertyDetails
{
    public class PanoramaModel
    {
        public Property Property { get; set; }
        public Panorama Panorama { get; set; }
        public FloorPlan FloorPlan { get; set; }

        public string StorageUrl { get; set; }
    }
}