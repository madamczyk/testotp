﻿using DataAccess.Enums;
using ModelMetadataExtensions;
using ExtranetApp.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Models.Account
{
    [MetadataConventionsAttribute(ResourceType = typeof(Resources.Models.Login))]
    public class LoginModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public RoleLevel LoginRole { get; set; }
    }
}