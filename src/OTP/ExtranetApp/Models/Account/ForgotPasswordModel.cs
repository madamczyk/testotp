﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using ModelMetadataExtensions;

namespace ExtranetApp.Models.Account
{
    [MetadataConventionsAttribute(ResourceType = typeof(Resources.Models.ForgotPassword))]
    public class ForgotPasswordModel
    {
        [Required]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        public string Email { get; set; }
    }
}