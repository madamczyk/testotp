﻿using DataAnnotationsExtensions;
using ModelMetadataExtensions;
using ExtranetApp.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Models.Account
{
    [MetadataConventionsAttribute(ResourceType = typeof(Resources.Models.Join))]
    public class JoinModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        public string Email { get; set; }
    }
}