﻿using BusinessLogic.ServiceContracts;
using DataAccess;
using ExtranetApp.Extensions;
using ExtranetApp.Resources.Booking;
using ExtranetApp.Resources.Models;
using ModelMetadataExtensions;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Models
{
    // All localization strings are taken from Common resx file.
    // Localization Names with HomeownerModel_<PropertyName>_<Attribute> 
    // (HomeownerModel_ConfirmPassword_Compare)
    [Serializable]
    [MetadataConventionsAttribute(ResourceType=typeof(Resources.Models.UserProfile))]
    public class UserProfileModel : IValidatableObject
    {
        public int UserId { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(255)]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(255)]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        [ExistingUser]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(255)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(4, MinimumLength = 2, ErrorMessageResourceName = "Validation_StringLength", ErrorMessageResourceType = typeof(ValidationShared))]
        public string TCInitials { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(255)]
        public string AddressLine1 { get; set; }

        [StringLength(255)]
        public string AddressLine2 { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(255)]
        public string State { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(255)]
        public string City { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        [StringLength(255)]
        public string ZIPCode { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]       
        public int Country { get; set; }

        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(ValidationShared))]
        public string CountryCode2Letters { get; set; }

        [StringLength(255)]
        public string SignUpCode { get; set; }

        public string CountryName { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (CountryCode2Letters.Equals("us"))
            {
                if (ZIPCode.Length != 5 || !Regex.IsMatch(ZIPCode, "^[0-9]{5}$"))
                    yield return new ValidationResult(BookingProcess.Validation_UsZip, new string[] { "ZIPCode" });
            }        
        }
    }
}