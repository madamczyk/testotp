﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.Home
{
    public class ListViewModel
    {
        public IEnumerable<PropertiesSearchResult> PropertiesResult;
        public int TotalResults { get; set; }
        public int CurrentResultCount { get; set; }
    }
}