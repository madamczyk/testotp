﻿using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtranetApp.Models.Home
{
    public class FinalScoreDetailsModel
    {
        public SearchFinalScoreDetails Details;
        public int? SearchFilterGuests;
        public int? SearchFilterBedrooms;
        public int? SearchFilterBathrooms;
    }
}