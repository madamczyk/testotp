﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExtranetApp.Resources.Booking {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class BookingProcess {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal BookingProcess() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ExtranetApp.Resources.Booking.BookingProcess", typeof(BookingProcess).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your credit card has been authorized and will be charged in 24 hours..
        /// </summary>
        public static string Confirmation_ChargedInfo {
            get {
                return ResourceManager.GetString("Confirmation_ChargedInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Check in:.
        /// </summary>
        public static string Confirmation_CheckIn {
            get {
                return ResourceManager.GetString("Confirmation_CheckIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Check out:.
        /// </summary>
        public static string Confirmation_CheckOut {
            get {
                return ResourceManager.GetString("Confirmation_CheckOut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirmation code: {0}.
        /// </summary>
        public static string Confirmation_ConfirmationCode {
            get {
                return ResourceManager.GetString("Confirmation_ConfirmationCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reservation made {0}.
        /// </summary>
        public static string Confirmation_DateOfMade {
            get {
                return ResourceManager.GetString("Confirmation_DateOfMade", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Below are your Trip Details. A confirmation has been sent to {0}..
        /// </summary>
        public static string Confirmation_EmailSend {
            get {
                return ResourceManager.GetString("Confirmation_EmailSend", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invoice.
        /// </summary>
        public static string Confirmation_Invoice {
            get {
                return ResourceManager.GetString("Confirmation_Invoice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ohtheplaces_Invoice_{0}.pdf.
        /// </summary>
        public static string Confirmation_InvoiceFileName {
            get {
                return ResourceManager.GetString("Confirmation_InvoiceFileName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Itinerary.
        /// </summary>
        public static string Confirmation_Itinerary {
            get {
                return ResourceManager.GetString("Confirmation_Itinerary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to per night.
        /// </summary>
        public static string Confirmation_PerNight {
            get {
                return ResourceManager.GetString("Confirmation_PerNight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save File.
        /// </summary>
        public static string Confirmation_SaveFile {
            get {
                return ResourceManager.GetString("Confirmation_SaveFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ohtheplaces_TripDetails_{0}.pdf.
        /// </summary>
        public static string Confirmation_SaveFileName {
            get {
                return ResourceManager.GetString("Confirmation_SaveFileName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Share.
        /// </summary>
        public static string Confirmation_Share {
            get {
                return ResourceManager.GetString("Confirmation_Share", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your Reservation has been confirmed!.
        /// </summary>
        public static string Confirmation_Subtitle {
            get {
                return ResourceManager.GetString("Confirmation_Subtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thank you for booking with Oh The Places!.
        /// </summary>
        public static string Confirmation_ThankYou {
            get {
                return ResourceManager.GetString("Confirmation_ThankYou", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirmation.
        /// </summary>
        public static string Confirmation_Title {
            get {
                return ResourceManager.GetString("Confirmation_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to total.
        /// </summary>
        public static string Confirmation_Total {
            get {
                return ResourceManager.GetString("Confirmation_Total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trip.
        /// </summary>
        public static string Confirmation_Trip {
            get {
                return ResourceManager.GetString("Confirmation_Trip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trip Details.
        /// </summary>
        public static string Confirmation_TripDetails {
            get {
                return ResourceManager.GetString("Confirmation_TripDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to to.
        /// </summary>
        public static string Confirmation_TripTo {
            get {
                return ResourceManager.GetString("Confirmation_TripTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View at My trips.
        /// </summary>
        public static string Confirmation_ViewAtMyTrips {
            get {
                return ResourceManager.GetString("Confirmation_ViewAtMyTrips", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Oh the places Reservation Confirmation for {0}, {1}.
        /// </summary>
        public static string ConfirmationEmail_Subject {
            get {
                return ResourceManager.GetString("ConfirmationEmail_Subject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to YYYY/MM/DD.
        /// </summary>
        public static string Contact_DateFormat {
            get {
                return ResourceManager.GetString("Contact_DateFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Next.
        /// </summary>
        public static string Contact_Next {
            get {
                return ResourceManager.GetString("Contact_Next", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Guests.
        /// </summary>
        public static string Contact_NumberOfGuests {
            get {
                return ResourceManager.GetString("Contact_NumberOfGuests", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please tell us how many people will be travelling.
        /// </summary>
        public static string Contact_NumerOfGuestsTitle {
            get {
                return ResourceManager.GetString("Contact_NumerOfGuestsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to * Any changes made will be saved when you click “NEXT”.
        /// </summary>
        public static string Contact_SaveChanges {
            get {
                return ResourceManager.GetString("Contact_SaveChanges", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please confirm your contact information:.
        /// </summary>
        public static string Contact_ThankYou {
            get {
                return ResourceManager.GetString("Contact_ThankYou", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact.
        /// </summary>
        public static string Contact_Title {
            get {
                return ResourceManager.GetString("Contact_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submit.
        /// </summary>
        public static string IdentityVerification_Submit {
            get {
                return ResourceManager.GetString("IdentityVerification_Submit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We just need to ask you a few questions to complete the booking process:.
        /// </summary>
        public static string IdentityVerification_Subtitle {
            get {
                return ResourceManager.GetString("IdentityVerification_Subtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Identity Verification.
        /// </summary>
        public static string IdentityVerification_Title {
            get {
                return ResourceManager.GetString("IdentityVerification_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Supplied wrong answer for the validation questions, please try again..
        /// </summary>
        public static string IdentityVerification_WrongAnswer {
            get {
                return ResourceManager.GetString("IdentityVerification_WrongAnswer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BACK TO HOME PAGE.
        /// </summary>
        public static string IdentityVerificationFailure_BtnBack {
            get {
                return ResourceManager.GetString("IdentityVerificationFailure_BtnBack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your identity cannot be verified by our service. Your account has been blocked to prevent further attempts. Please contact our office to get more informations..
        /// </summary>
        public static string IdentityVerificationFailure_Message {
            get {
                return ResourceManager.GetString("IdentityVerificationFailure_Message", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We&apos;re sorry..
        /// </summary>
        public static string IdentityVerificationFailure_Sorry {
            get {
                return ResourceManager.GetString("IdentityVerificationFailure_Sorry", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Identity Verification Failure.
        /// </summary>
        public static string IdentityVerificationFailure_Title {
            get {
                return ResourceManager.GetString("IdentityVerificationFailure_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invoice.pdf.
        /// </summary>
        public static string Invoice_FileName {
            get {
                return ResourceManager.GetString("Invoice_FileName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Property cannot be booked on selected period. There are no rates set for given period or total invoice value is 0..
        /// </summary>
        public static string InvoiceRateNotSet_Description {
            get {
                return ResourceManager.GetString("InvoiceRateNotSet_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Property rates are not set..
        /// </summary>
        public static string InvoiceRateNotSet_Title {
            get {
                return ResourceManager.GetString("InvoiceRateNotSet_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirmation.
        /// </summary>
        public static string Nav_Confirmation {
            get {
                return ResourceManager.GetString("Nav_Confirmation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact.
        /// </summary>
        public static string Nav_Contact {
            get {
                return ResourceManager.GetString("Nav_Contact", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payment.
        /// </summary>
        public static string Nav_Payment {
            get {
                return ResourceManager.GetString("Nav_Payment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome.
        /// </summary>
        public static string Nav_Welcome {
            get {
                return ResourceManager.GetString("Nav_Welcome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I am at least 25 years old..
        /// </summary>
        public static string Payment_AgeCheck {
            get {
                return ResourceManager.GetString("Payment_AgeCheck", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Provided birth date and the selected checkbox option are not consistent. Booking process cannot be finished..
        /// </summary>
        public static string Payment_AgeTermsValidationMessage {
            get {
                return ResourceManager.GetString("Payment_AgeTermsValidationMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I have read and agree to.
        /// </summary>
        public static string Payment_AgreeTerms {
            get {
                return ResourceManager.GetString("Payment_AgreeTerms", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Billing Address:.
        /// </summary>
        public static string Payment_BillingAddr {
            get {
                return ResourceManager.GetString("Payment_BillingAddr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Card Holder Name.
        /// </summary>
        public static string Payment_CardHolder {
            get {
                return ResourceManager.GetString("Payment_CardHolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Card Number.
        /// </summary>
        public static string Payment_CardNumber {
            get {
                return ResourceManager.GetString("Payment_CardNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose your existing credit card.
        /// </summary>
        public static string Payment_ChooseExisting {
            get {
                return ResourceManager.GetString("Payment_ChooseExisting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CONFIRM &amp; PAY.
        /// </summary>
        public static string Payment_Confirm {
            get {
                return ResourceManager.GetString("Payment_Confirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Credit Card Information:.
        /// </summary>
        public static string Payment_CreditCard {
            get {
                return ResourceManager.GetString("Payment_CreditCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Credit card number.
        /// </summary>
        public static string Payment_CreditCardNumber {
            get {
                return ResourceManager.GetString("Payment_CreditCardNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There was problem with saving your credit card data. You will need to input credit card data next time on booking process..
        /// </summary>
        public static string Payment_CreditSavingError {
            get {
                return ResourceManager.GetString("Payment_CreditSavingError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Expiration Date:.
        /// </summary>
        public static string Payment_ExpirationDate {
            get {
                return ResourceManager.GetString("Payment_ExpirationDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Initials.
        /// </summary>
        public static string Payment_Initials {
            get {
                return ResourceManager.GetString("Payment_Initials", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please confirm:.
        /// </summary>
        public static string Payment_LegalAgreement {
            get {
                return ResourceManager.GetString("Payment_LegalAgreement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter your initials here:.
        /// </summary>
        public static string Payment_LegalAgreemntConfirmation {
            get {
                return ResourceManager.GetString("Payment_LegalAgreemntConfirmation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New credit card.
        /// </summary>
        public static string Payment_NewCreditCard {
            get {
                return ResourceManager.GetString("Payment_NewCreditCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PIN CODE.
        /// </summary>
        public static string Payment_PinCode {
            get {
                return ResourceManager.GetString("Payment_PinCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT.
        /// </summary>
        public static string Payment_SelectCreditCard {
            get {
                return ResourceManager.GetString("Payment_SelectCreditCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to * Oh The Places will store your credit card information to use in the case of future bookings or additional services purchased..
        /// </summary>
        public static string Payment_StoreCreditCard {
            get {
                return ResourceManager.GetString("Payment_StoreCreditCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please provide valid payment information.
        /// </summary>
        public static string Payment_Subtitle {
            get {
                return ResourceManager.GetString("Payment_Subtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payment.
        /// </summary>
        public static string Payment_Title {
            get {
                return ResourceManager.GetString("Payment_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use alternate card.
        /// </summary>
        public static string Payment_UseAnotherCard {
            get {
                return ResourceManager.GetString("Payment_UseAnotherCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Existing credit card.
        /// </summary>
        public static string Payment_UseExistingCreditCard {
            get {
                return ResourceManager.GetString("Payment_UseExistingCreditCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We are sorry, there was an error on processing your payment. Booking process cannot be completed.&lt;br /&gt; Please make sure you input valid credit card data and try again later..
        /// </summary>
        public static string PaymentFailure_Subtitle {
            get {
                return ResourceManager.GetString("PaymentFailure_Subtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payment process error.
        /// </summary>
        public static string PaymentFailure_Title {
            get {
                return ResourceManager.GetString("PaymentFailure_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We&apos;re sorry, selected property is no longer available. Choose another property or select different travel time..
        /// </summary>
        public static string PropertyNotAvailable_Subtitle {
            get {
                return ResourceManager.GetString("PropertyNotAvailable_Subtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selected property is not available.
        /// </summary>
        public static string PropertyNotAvailable_Title {
            get {
                return ResourceManager.GetString("PropertyNotAvailable_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to to.
        /// </summary>
        public static string TripTo {
            get {
                return ResourceManager.GetString("TripTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone number must have 10  digits.
        /// </summary>
        public static string Validation_UsPhone {
            get {
                return ResourceManager.GetString("Validation_UsPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zip code must have 5 digits.
        /// </summary>
        public static string Validation_UsZip {
            get {
                return ResourceManager.GetString("Validation_UsZip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Forgot password?.
        /// </summary>
        public static string Welcome_Forgot {
            get {
                return ResourceManager.GetString("Welcome_Forgot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login.
        /// </summary>
        public static string Welcome_LoginButtonText {
            get {
                return ResourceManager.GetString("Welcome_LoginButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you a member, please login:.
        /// </summary>
        public static string Welcome_LoginText {
            get {
                return ResourceManager.GetString("Welcome_LoginText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not a member? Please join:.
        /// </summary>
        public static string Welcome_NotAMember {
            get {
                return ResourceManager.GetString("Welcome_NotAMember", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to privacy policy.
        /// </summary>
        public static string Welcome_Policy {
            get {
                return ResourceManager.GetString("Welcome_Policy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to * By joining, you accept the.
        /// </summary>
        public static string Welcome_Privacy {
            get {
                return ResourceManager.GetString("Welcome_Privacy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to and.
        /// </summary>
        public static string Welcome_Privacy_And {
            get {
                return ResourceManager.GetString("Welcome_Privacy_And", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We have reserved your accomodation, you have 15 minutes to complete your reservation..
        /// </summary>
        public static string Welcome_TemporatyBookTime {
            get {
                return ResourceManager.GetString("Welcome_TemporatyBookTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to terms and conditions.
        /// </summary>
        public static string Welcome_Terms {
            get {
                return ResourceManager.GetString("Welcome_Terms", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome.
        /// </summary>
        public static string Welcome_Title {
            get {
                return ResourceManager.GetString("Welcome_Title", resourceCulture);
            }
        }
    }
}
