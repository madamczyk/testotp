﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExtranetApp.Resources.Home {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class PropertyDetails {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PropertyDetails() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ExtranetApp.Resources.Home.PropertyDetails", typeof(PropertyDetails).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have selected too short period. Please choose at least 1 night..
        /// </summary>
        public static string DialogAtLeastOneNightDescription {
            get {
                return ResourceManager.GetString("DialogAtLeastOneNightDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selected period is too short.
        /// </summary>
        public static string DialogAtLeastOneNightTitle {
            get {
                return ResourceManager.GetString("DialogAtLeastOneNightTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minimum Stay {0} nights.
        /// </summary>
        public static string MinimumStayLength {
            get {
                return ResourceManager.GetString("MinimumStayLength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Book now.
        /// </summary>
        public static string NavigationButtonBook {
            get {
                return ResourceManager.GetString("NavigationButtonBook", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Amenities.
        /// </summary>
        public static string Section_AdditionInformation_Amenities {
            get {
                return ResourceManager.GetString("Section_AdditionInformation_Amenities", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Availability.
        /// </summary>
        public static string Section_AdditionInformation_Availability {
            get {
                return ResourceManager.GetString("Section_AdditionInformation_Availability", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description &amp; Features.
        /// </summary>
        public static string Section_AdditionInformation_Description {
            get {
                return ResourceManager.GetString("Section_AdditionInformation_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gallery.
        /// </summary>
        public static string Section_AdditionInformation_Galery {
            get {
                return ResourceManager.GetString("Section_AdditionInformation_Galery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Guest Reviews.
        /// </summary>
        public static string Section_AdditionInformation_Reviews {
            get {
                return ResourceManager.GetString("Section_AdditionInformation_Reviews", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Share Property &amp; Trip Details.
        /// </summary>
        public static string Section_AdditionInformation_Share {
            get {
                return ResourceManager.GetString("Section_AdditionInformation_Share", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street View &amp; Local Info.
        /// </summary>
        public static string Section_AdditionInformation_StreetView {
            get {
                return ResourceManager.GetString("Section_AdditionInformation_StreetView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Additional Information.
        /// </summary>
        public static string Section_AdditionInformation_Title {
            get {
                return ResourceManager.GetString("Section_AdditionInformation_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Floor Plan &amp; Virtual Tours.
        /// </summary>
        public static string Section_AdditionInformation_Tour {
            get {
                return ResourceManager.GetString("Section_AdditionInformation_Tour", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Amenities.
        /// </summary>
        public static string Section_Amenities_Title {
            get {
                return ResourceManager.GetString("Section_Amenities_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Available.
        /// </summary>
        public static string Section_AvailabilityAvailable {
            get {
                return ResourceManager.GetString("Section_AvailabilityAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No arrivals at this day.
        /// </summary>
        public static string Section_AvailabilityCTA {
            get {
                return ResourceManager.GetString("Section_AvailabilityCTA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minimum Stay.
        /// </summary>
        public static string Section_AvailabilityMLOS {
            get {
                return ResourceManager.GetString("Section_AvailabilityMLOS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Past date.
        /// </summary>
        public static string Section_AvailabilityPast {
            get {
                return ResourceManager.GetString("Section_AvailabilityPast", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to / night.
        /// </summary>
        public static string Section_AvailabilityPerNight {
            get {
                return ResourceManager.GetString("Section_AvailabilityPerNight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selection.
        /// </summary>
        public static string Section_AvailabilitySelection {
            get {
                return ResourceManager.GetString("Section_AvailabilitySelection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unavailable.
        /// </summary>
        public static string Section_AvailabilityUnvailable {
            get {
                return ResourceManager.GetString("Section_AvailabilityUnvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Book This Property.
        /// </summary>
        public static string Section_Book_Button {
            get {
                return ResourceManager.GetString("Section_Book_Button", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 7 days discount.
        /// </summary>
        public static string Section_Invoice_7DaysDiscount {
            get {
                return ResourceManager.GetString("Section_Invoice_7DaysDiscount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accomodation.
        /// </summary>
        public static string Section_Invoice_Accomodation {
            get {
                return ResourceManager.GetString("Section_Invoice_Accomodation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Length of stay.
        /// </summary>
        public static string Section_Invoice_LengthOfStay {
            get {
                return ResourceManager.GetString("Section_Invoice_LengthOfStay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nights.
        /// </summary>
        public static string Section_Invoice_Nights {
            get {
                return ResourceManager.GetString("Section_Invoice_Nights", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to nights.
        /// </summary>
        public static string Section_Invoice_NumberOfNights {
            get {
                return ResourceManager.GetString("Section_Invoice_NumberOfNights", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price per night .
        /// </summary>
        public static string Section_Invoice_PricePerNight {
            get {
                return ResourceManager.GetString("Section_Invoice_PricePerNight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subtotal.
        /// </summary>
        public static string Section_Invoice_SubTotal {
            get {
                return ResourceManager.GetString("Section_Invoice_SubTotal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tax.
        /// </summary>
        public static string Section_Invoice_Tax {
            get {
                return ResourceManager.GetString("Section_Invoice_Tax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invoice Summary.
        /// </summary>
        public static string Section_Invoice_Title {
            get {
                return ResourceManager.GetString("Section_Invoice_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total.
        /// </summary>
        public static string Section_Invoice_Total {
            get {
                return ResourceManager.GetString("Section_Invoice_Total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total accomodation.
        /// </summary>
        public static string Section_Invoice_TotalAccomodation {
            get {
                return ResourceManager.GetString("Section_Invoice_TotalAccomodation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cleaning fees.
        /// </summary>
        public static string Section_Other_CleaningFees {
            get {
                return ResourceManager.GetString("Section_Other_CleaningFees", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other items.
        /// </summary>
        public static string Section_Other_Title {
            get {
                return ResourceManager.GetString("Section_Other_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total other items.
        /// </summary>
        public static string Section_Other_Total {
            get {
                return ResourceManager.GetString("Section_Other_Total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to From.
        /// </summary>
        public static string Section_TripDetails_DateFrom {
            get {
                return ResourceManager.GetString("Section_TripDetails_DateFrom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Until.
        /// </summary>
        public static string Section_TripDetails_DateTo {
            get {
                return ResourceManager.GetString("Section_TripDetails_DateTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trip Details.
        /// </summary>
        public static string Section_TripDetails_Title {
            get {
                return ResourceManager.GetString("Section_TripDetails_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm signing off.
        /// </summary>
        public static string SignOffButton {
            get {
                return ResourceManager.GetString("SignOffButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are about to signing off the property. After your confirmation the property will be available in the system for booking. If you reviewed the content of the property please confirm it clicking the button below..
        /// </summary>
        public static string SignOffQuestion {
            get {
                return ResourceManager.GetString("SignOffQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sign off property.
        /// </summary>
        public static string SignOffTitle {
            get {
                return ResourceManager.GetString("SignOffTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Property Details.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Availability.
        /// </summary>
        public static string ViewAvailability_Title {
            get {
                return ResourceManager.GetString("ViewAvailability_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bathroom(s).
        /// </summary>
        public static string ViewDescriptionFeatures_Bathroom {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_Bathroom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bedroom(s).
        /// </summary>
        public static string ViewDescriptionFeatures_Bedroom {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_Bedroom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Appliances.
        /// </summary>
        public static string ViewDescriptionFeatures_DeluxeAppliance {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_DeluxeAppliance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        public static string ViewDescriptionFeatures_Description {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Features.
        /// </summary>
        public static string ViewDescriptionFeatures_Features {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_Features", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kitchen &amp; Laundry.
        /// </summary>
        public static string ViewDescriptionFeatures_KitchenLaundry {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_KitchenLaundry", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deluxe.
        /// </summary>
        public static string ViewDescriptionFeatures_KitchenLaundryDeluxe {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_KitchenLaundryDeluxe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Living Area &amp; Dining.
        /// </summary>
        public static string ViewDescriptionFeatures_LivingAreaDining {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_LivingAreaDining", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PropertyType.
        /// </summary>
        public static string ViewDescriptionFeatures_PropertyType {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_PropertyType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sleep(s).
        /// </summary>
        public static string ViewDescriptionFeatures_Sleeps {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_Sleeps", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sq ft.
        /// </summary>
        public static string ViewDescriptionFeatures_SquareFootage {
            get {
                return ResourceManager.GetString("ViewDescriptionFeatures_SquareFootage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To view 360 degrees panorama of the room click this symbol.
        /// </summary>
        public static string ViewFloorPlan_PanoramaInfo {
            get {
                return ResourceManager.GetString("ViewFloorPlan_PanoramaInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ago.
        /// </summary>
        public static string ViewGuestReviews_Ago {
            get {
                return ResourceManager.GetString("ViewGuestReviews_Ago", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Author.
        /// </summary>
        public static string ViewGuestReviews_Author {
            get {
                return ResourceManager.GetString("ViewGuestReviews_Author", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cleanliness.
        /// </summary>
        public static string ViewGuestReviews_Cleanliness {
            get {
                return ResourceManager.GetString("ViewGuestReviews_Cleanliness", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to days.
        /// </summary>
        public static string ViewGuestReviews_DaysAgo {
            get {
                return ResourceManager.GetString("ViewGuestReviews_DaysAgo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to hours.
        /// </summary>
        public static string ViewGuestReviews_HoursAgo {
            get {
                return ResourceManager.GetString("ViewGuestReviews_HoursAgo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location.
        /// </summary>
        public static string ViewGuestReviews_Location {
            get {
                return ResourceManager.GetString("ViewGuestReviews_Location", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to minutes.
        /// </summary>
        public static string ViewGuestReviews_MinutesAgo {
            get {
                return ResourceManager.GetString("ViewGuestReviews_MinutesAgo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Posted on.
        /// </summary>
        public static string ViewGuestReviews_PostedOn {
            get {
                return ResourceManager.GetString("ViewGuestReviews_PostedOn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quality.
        /// </summary>
        public static string ViewGuestReviews_Quality {
            get {
                return ResourceManager.GetString("ViewGuestReviews_Quality", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Highest Rating.
        /// </summary>
        public static string ViewGuestReviews_SortHighest {
            get {
                return ResourceManager.GetString("ViewGuestReviews_SortHighest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lowest Rating.
        /// </summary>
        public static string ViewGuestReviews_SortLowest {
            get {
                return ResourceManager.GetString("ViewGuestReviews_SortLowest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Newest.
        /// </summary>
        public static string ViewGuestReviews_SortNewest {
            get {
                return ResourceManager.GetString("ViewGuestReviews_SortNewest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Guest Reviews.
        /// </summary>
        public static string ViewGuestReviews_Title {
            get {
                return ResourceManager.GetString("ViewGuestReviews_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to From {0}.
        /// </summary>
        public static string ViewMainHeaderPriceFrom {
            get {
                return ResourceManager.GetString("ViewMainHeaderPriceFrom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} / night.
        /// </summary>
        public static string ViewMainHeaderPricePerNight {
            get {
                return ResourceManager.GetString("ViewMainHeaderPricePerNight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Distance to Main Attraction.
        /// </summary>
        public static string ViewStreetViewLocalArea_DistanceToMainAttraction {
            get {
                return ResourceManager.GetString("ViewStreetViewLocalArea_DistanceToMainAttraction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Local Attractions.
        /// </summary>
        public static string ViewStreetViewLocalArea_LocalAttractions {
            get {
                return ResourceManager.GetString("ViewStreetViewLocalArea_LocalAttractions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street View &amp; Local Info.
        /// </summary>
        public static string ViewStreetViewLocalArea_Title {
            get {
                return ResourceManager.GetString("ViewStreetViewLocalArea_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Arts &amp; Entertainment.
        /// </summary>
        public static string ViewStreetViewLocalArea_YelpArtsEntertaiment {
            get {
                return ResourceManager.GetString("ViewStreetViewLocalArea_YelpArtsEntertaiment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to * Local attractions provided are based on property&apos;s zip code..
        /// </summary>
        public static string ViewStreetViewLocalArea_YelpProviderInfo {
            get {
                return ResourceManager.GetString("ViewStreetViewLocalArea_YelpProviderInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Restaurants / Bars.
        /// </summary>
        public static string ViewStreetViewLocalArea_YelpRestaurantsBars {
            get {
                return ResourceManager.GetString("ViewStreetViewLocalArea_YelpRestaurantsBars", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shopping.
        /// </summary>
        public static string ViewStreetViewLocalArea_YelpShopping {
            get {
                return ResourceManager.GetString("ViewStreetViewLocalArea_YelpShopping", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have selected too short period, please choose more then {0} nights.
        /// </summary>
        public static string WrongPeriodPopUpDesc {
            get {
                return ResourceManager.GetString("WrongPeriodPopUpDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selected period is too short.
        /// </summary>
        public static string WrongPeriodPopUpTitle {
            get {
                return ResourceManager.GetString("WrongPeriodPopUpTitle", resourceCulture);
            }
        }
    }
}
