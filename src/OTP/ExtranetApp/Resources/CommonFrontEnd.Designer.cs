﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExtranetApp.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CommonFrontEnd {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CommonFrontEnd() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ExtranetApp.Resources.CommonFrontEnd", typeof(CommonFrontEnd).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If you cancel your reservation before 24 hours from booking date, you will receive a full refund. If you cancel before 30 days to arrival, your credit card will not be charged for the remaining fifty (50%) due. If there is less then 30 days to arrival OTP will attempt to re-sell the Place for equal or greater revenue. If OTP is able to re-sell the Place, you will receive a refund of your total costs paid, less transaction fees..
        /// </summary>
        public static string CancellationPolicies {
            get {
                return ResourceManager.GetString("CancellationPolicies", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If you cancel before {0}, your credit card will not be charged for the remaining fifty (50%) due..
        /// </summary>
        public static string CancellationPolicyHoursHalfRefund {
            get {
                return ResourceManager.GetString("CancellationPolicyHoursHalfRefund", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If you cancel your reservation by {0}, you will receive a full refund..
        /// </summary>
        public static string CancellationPolicyLessThen24HoursFullRefund {
            get {
                return ResourceManager.GetString("CancellationPolicyLessThen24HoursFullRefund", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If you choose to cancel your reservation, OTP will attempt to re-sell the Place for equal or greater revenue.  If OTP is able to re-sell the Place, you will receive a refund of your total costs paid, less transaction fees..
        /// </summary>
        public static string CancellationPolicyTryingToResell {
            get {
                return ResourceManager.GetString("CancellationPolicyTryingToResell", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If you choose to cancel your reservation after {0}, Oh The Places will attempt to re-sell the place for equal or greater revenue. If Oh the Places is able to re-sell the Place, you will receive a refund of your total costs paid, less transaction fees..
        /// </summary>
        public static string CancellationPolicyWhatIfAfterTime {
            get {
                return ResourceManager.GetString("CancellationPolicyWhatIfAfterTime", resourceCulture);
            }
        }
    }
}
