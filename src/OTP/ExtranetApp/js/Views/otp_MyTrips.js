﻿jQuery(document).ready(function () {
    jQuery('.thumbnail_container').each(function (index, element) {
        jQuery(this).cycle({
            fx: 'fade',
            timeout: 2000
        }).cycle("pause");
    });

    jQuery(".portfolio_image").each(function (index, element) {
        jQuery(this).hover(
        function () {
            jQuery(this).find(".thumbnail_container").cycle("resume");
        },
        function () {
            jQuery(this).find(".thumbnail_container").cycle("pause");
        }
        );
    });
});

function showPast(){
    jQuery(".page-title .selected").addClass("grey").removeClass("selected");
    jQuery("#PastLabel").addClass("selected").removeClass("grey");
    jQuery("#myTrips_main_wrap .selected").hide().removeClass("selected");
    jQuery("#PastTrips").show().addClass("selected");
}

function showCancelled() {
    jQuery(".page-title .selected").addClass("grey").removeClass("selected");
    jQuery("#CancelledLabel").addClass("selected").removeClass("grey");
    jQuery("#myTrips_main_wrap .selected").hide().removeClass("selected");
    jQuery("#CancelledTrips").show().addClass("selected");
}

function showCurrent() {
    jQuery(".page-title .selected").addClass("grey").removeClass("selected");
    jQuery("#CurrentLabel").addClass("selected").removeClass("grey");
    jQuery("#myTrips_main_wrap .selected").hide().removeClass("selected");
    jQuery("#CurrentTrips").show().addClass("selected");
}