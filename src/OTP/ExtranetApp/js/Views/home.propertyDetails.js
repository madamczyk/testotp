/* 
JS Logic for the /Home/PropertyDetails View 
*/
var oth = oth || {};
var availabilityInfoValues = null;

oth.propertyDetailsViewModel = (function (oth, $) {
    var ViewModel = function () {
        var self = this;

        var classNavigationLinks = '.navigationLink';

        // street view
        self.mapLatitude = null;
        self.mapLongitude = null;
        self.client = null;

        // availability calendar
        self.availabilityCalendarInitialized = false;
        self.calendarDefaultDate = '2012-12-12';
        self.startingDateForDatePicker = null;
        self.endingDateForDatePicker = null;

        // gallery slideshow
        self.galleryChangeImageTime = 2000;
        self.timeLeftToChange = 0;
        self.progressBarValue = 0;
        self.progressBarCallbackTimer = 250;

        self.propertyName = '';

        self.searchRequest = function () {
            getInvoiceData();
            getDataValuesForCalendar();
            jQuery('#availabilityDate').DatePickerAvailabilitySetDate([dateFrom, dateTo], true);
        };

        getDataForDate = function (date) {
            var priceDay = 0;
            var disabledElem = false;
            var specialClass = '';
            var mlosDay = 0;
            var cta = true;

            jQuery(availabilityInfoValues).each(function (index, element) {
                var jsonDate = new Date(element.Date.replace(/-/g, '/'));
                jsonDate.setHours(0, 0, 0, 0);
                if (jsonDate.valueOf() == date.valueOf()) {
                    priceDay = element.Price;
                    disabledElem = element.IsBlocked;
                    mlosDay = element.Mlos;
                    cta = element.Cta;
                }

            });
            return {
                disabled: disabledElem,
                className: disabledElem ? 'datepickerSpecial' : cta ? 'datepickerCTA' : '',
                price: priceDay,
                mlos: mlosDay ? mlosDay : "",
                cta: cta
            }
        };


        //localized resources
        self.resources = {
            DialogAtLeastOneNightTitle: "",
            DialogAtLeastOneNightDescription: "",
        }

        //init street view component
        function initStreetView(streetViewContainer, lat, lng) {
            var STREETVIEW_MAX_DISTANCE = 100000;            var latLng = new google.maps.LatLng(lat, lng);            var nearestLatLng = null;            var nearestPano = null;            self.client.getPanoramaByLocation(latLng, STREETVIEW_MAX_DISTANCE, function (result,            status) {
                if (status == google.maps.StreetViewStatus.OK) {
                    nearestPano = result.location.pano;                    nearestLatLng = result.location.latLng;                    var panoOptions = {
                        position: nearestLatLng,                        addressControlOptions: {
                            position: google.maps.ControlPosition.BOTTOM
                        },                        linksControl: false,                        panControl: false,                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.SMALL
                        },                        enableCloseButton: false
                    };                    var panorama = new google.maps.StreetViewPanorama(document.getElementById(streetViewContainer), panoOptions);                    panorama.setPano(nearestPano);
                }
            });
        }

        function hideAllSections() {
            jQuery(".content-sections").hide();
            jQuery(".section-nav-links").removeClass("active");
        }

        function changeVisibleSection(elemId, linkElem) {
            jQuery(".property-dialog").hide();
            hideAllSections();
            jQuery(elemId).show("slow");
            linkElem.addClass("active");
            return false;
        }

        function changeVisibleSectionMap(elemId, linkElem) {
            jQuery(".property-dialog").hide();
            hideAllSections();
            jQuery(elemId).show("slow", function () {
                if (jQuery(this).is(':visible')) {
                    initStreetView("streetViewContainer", self.mapLatitude, self.mapLongitude);
                }
            });
            linkElem.addClass("active");
            return false;
        }


        self.getInvoiceData = function () {
            jQuery("#widget-invoice").hide();

            if (dateFrom === dateTo) { // there must be at least one night selected
                return false;
            }

            var propertyId = jQuery("#Property_PropertyID").val();
            var unitId = jQuery("#Unit_UnitID").val();
            //var destinationId = jQuery("#Property_Destination_DestinationID").val();

            jQuery.ajax({
                url: "/PropertyDetail/GetInvoiceDetails?unitId=" + unitId + "&propertyId=" + propertyId + "&dateFrom=" + dateFrom + "&dateUntil=" + dateTo,
                success: function (data, textStatus, jqXHR) {
                    jQuery("#widget-invoice").show();
                    jQuery('#widget-invoice').html(data);

                    jQuery("div.collapse-section-trigger").not('[data-section="widget-tripDetails-section"]').click(function () {
                        var sectionId = jQuery(this).attr("data-section");
                        jQuery("#" + sectionId).toggle("slow");
                        jQuery(this).find(".section-title-overall-value").toggle();

                        var collapsibleSection = jQuery(this).find("span.ui-icon-triangle-1-e");
                        if (collapsibleSection.size() != 0) {
                            collapsibleSection.removeClass("ui-icon-triangle-1-e");
                            collapsibleSection.addClass("ui-icon-triangle-1-s");
                        } else {
                            collapsibleSection = jQuery(this).find("span.ui-icon-triangle-1-s");
                            collapsibleSection.removeClass("ui-icon-triangle-1-s");
                            collapsibleSection.addClass("ui-icon-triangle-1-e");
                        }
                    });

                    getMainSectionInfo();
                    showTripDetailsDates();
                }
            });
        }

        function initAvailabilityCalendar() {
            if (self.availabilityCalendarInitialized == false) {
                //init calendar 
                var dates = null;
                var dateStart = null;
                if (dateFrom != "" && dateTo != "") {
                    dates = [dateFrom, dateTo];
                    dateStart = (jQuery).datepicker.formatDate("yy-mm-dd", new Date(dateFrom.replace('/-/g', '/')).addMonths(1));
                }
                else {
                    var date = new Date();
                    date.addMonths(1);
                    //date.setMonth(date.getMonth() - 1);
                    dates = (jQuery).datepicker.formatDate("yy-mm-dd", date);
                    dateStart = (jQuery).datepicker.formatDate("yy-mm-dd", date);
                }

                jQuery('#availabilityDate').DatePickerAvailability({
                    flat: true,
                    date: dates,
                    current: dateStart,
                    calendars: 2,
                    mode: 'range',
                    starts: 0,
                    cultureName: propertyCultureName,
                    onRender: function (date) {
                        var priceDay = 0;
                        var disabledElem = false;
                        var specialClass = '';
                        var mlosDay = 0;
                        var cta = true;

                        jQuery(availabilityInfoValues).each(function (index, element) {
                            var jsonDate = new Date(element.Date.replace(/-/g, '/'));
                            jsonDate.setHours(0, 0, 0, 0);
                            if (jsonDate.valueOf() == date.valueOf()) {
                                priceDay = element.Price;
                                disabledElem = element.IsBlocked;
                                mlosDay = element.Mlos;
                                cta = element.Cta;
                            }

                        });
                        return {
                            disabled: disabledElem,
                            className: disabledElem ? 'datepickerSpecial' : cta ? 'datepickerCTA' : '',
                            price: priceDay,
                            mlos: mlosDay ? mlosDay : "",
                            cta: cta
                        }
                    },
                    onChange: function (date) {

                        jQuery("a#dateFromLink").text(oth.common.getLocalizedDateString(dateFrom));
                        if (dateTo != '')
                            jQuery("a#dateToLink").text(oth.common.getLocalizedDateString(dateTo));
                        jQuery('#datepickerFrom').DatePickerNavigationSetDate(dateFrom);
                        jQuery('#datepickerTo').DatePickerNavigationSetDate(dateTo);
                    }

                });
                self.availabilityCalendarInitialized = true;
            }
            return false;
        }

        self.getDataValuesForCalendar = function () {
            var unitId = jQuery("#Unit_UnitID").val();
            //var selectedDates = jQuery('#availabilityDate').DatePickerGetDate(true);    
            //TODO Zmienic z statycznej daty na jakis current
            //przydaloby sie pomysliec o keszowaniu
            jQuery.getJSON("/PropertyDetail/GetPropertyAvailabilityInfoForMonth", {
                id: jQuery("#Unit_UnitID").val(),
                fromDate: self.startingDateForDatePicker != null ? self.startingDateForDatePicker : "2012-11-04",
                untilDate: (self.endingDateForDatePicker != null) ? self.endingDateForDatePicker : "2013-12-04"
            }, function (data) {
                availabilityInfoValues = data;
                if (self.availabilityCalendarInitialized) {
                    jQuery('.datepickerBorderT').click();
                } else
                    initAvailabilityCalendar();
            });
        }


        function getMainSectionInfo() {
            jQuery.getJSON("/PropertyDetail/PropertyDetailsByDate", {
                id: jQuery("#Unit_UnitID").val(),
                dateFrom: dateFrom,
                dateUntil: dateTo,
                culture: jQuery("#CultureCode").val()
            }, function (data) {
                jQuery("#mainPropertyDetailsView-price > div > h1").html(data.Price);
                jQuery("#mainPropertyDetailsView-unitMLOS > div > p").html(data.UnitMOS);
            });
        }


        function showTripDetailsDates() {
            jQuery("#tripDates").show();
            jQuery("#dateFromTrip").html(dateFrom);
            jQuery("#dateUntilTrip").html(dateTo);
        }

        function initGallerySlideshow() {
            $('.thumbnail_container').cycle({
                fx: 'fade',
                timeout: self.galleryChangeImageTime,
                next: '#nextImage',
                prev: '#prevImage',
                after: self.resetProgressBar
            });

            if ($(".thumbnail_container img").size() > 0) { //only if there are gallery images

                $("div#galleryPropertyDetailsView .portfolio_image").hover(
                    function () {
                        $("div#galleryPropertyDetailsView .container_image_overlay_block").show();
                    },
                    function () {
                        $("div#galleryPropertyDetailsView .container_image_overlay_block").hide();
                    }
                    );


                $("#progressBar").progressbar(
                    {
                        value: self.progressBarValue
                    });
                setTimeout(updateProgressBar, self.progressBarCallbackTimer);
            }
        }

        self.updateProgressBar = function () {
            self.progressBarValue = (self.timeLeftToChange / self.galleryChangeImageTime) * 100;
            self.timeLeftToChange += self.progressBarCallbackTimer;
            $("#progressBar").progressbar("option", "value", self.progressBarValue);
            setTimeout(updateProgressBar, self.progressBarCallbackTimer);
        }

        self.resetProgressBar = function () {
            if ($("#progressBar").progressbar() == null) return;
            self.timeLeftToChange = 0;
            $("#progressBar").progressbar("option", "value", 0);

        }

        self.init = function (options) {
            $.extend(self, options);

            $(classNavigationLinks).click(function () {
                var linkElem = $('#' + $(this).attr('data-link'));
                linkElem.click();
                return false;
            });

            var queryString = parseHashUrl();
            if (queryString != null) {
                //set menu values
                setMenuValues(queryString);
            }
            loadResources();

            if (dateFrom != "" && dateTo != "") {
                //ajax call for invoice data
                getInvoiceData();
                showTripDetailsDates();

                self.startingDateForDatePicker = (jQuery).datepicker.formatDate("yy-mm-dd", new Date(dateFrom.replace(/-/g, '/')).addMonths(-2));
                self.endingDateForDatePicker = (jQuery).datepicker.formatDate("yy-mm-dd", new Date(dateTo.replace(/-/g, '/')).addMonths(2));
            }

            getDataValuesForCalendar();

            jQuery("#mainPropertyDetailsView").show();

            jQuery("#linkGoToDescription").click(function () {
                return changeVisibleSection("#descriptionPropertyDetailsView", jQuery(this));
            });
            jQuery("#linkGoToGalery").click(function () {
                return changeVisibleSection("#galleryPropertyDetailsView", jQuery(this));
            });
            jQuery("#linkGoToTour").click(function () {
                return changeVisibleSection("#floorPlanPropertyDetailsView", jQuery(this));
            });
            jQuery("#linkGoToAmenities").click(function () {
                return changeVisibleSection("#amenitiesPropertyDetailsView", jQuery(this));
            });
            jQuery("#linkGoToAvailability").click(function () {
                return changeVisibleSection("#availabilityPropertyDetailsView", jQuery(this));
            });
            jQuery("#linkGoToStreetView").click(function () {
                return changeVisibleSectionMap("#streetViewDetailsView", jQuery(this));
            });
            jQuery("#linkGoToReviews").click(function () {
                return changeVisibleSection("#guestReviewsDetailsView", jQuery(this));
            });
            jQuery("#linkGoToShare").click(function () {
                return changeVisibleSection("", jQuery(this));
            });

            jQuery(".appliance-group-link").click(function () {
                jQuery(".appliance-dialog").hide();

                var applianceGroupId = jQuery(this).attr("appliance-group-id");
                jQuery(".appliance-dialog[appliance-group-id=" + applianceGroupId + "]").show();

                return false;
            });

            jQuery("#review-sort-menu li").click(function () {
                jQuery(this).children("ul").toggle("slow");
                return false;
            });

            jQuery(".review-group-link").hover(function () {
                var reviewId = jQuery(this).attr("review-group-id");
                jQuery(".review-dialog[review-group-id=" + reviewId + "]").show();
            }, function () {
                var reviewId = jQuery(this).attr("review-group-id");
                jQuery(".review-dialog[review-group-id=" + reviewId + "]").hide();
            }
            );

            ///Perform sort on the Guest Review Container
            jQuery(".sort_type").click(function () {
                var typeOfSort = jQuery(this).attr("data-sort");
                var propertyId = jQuery("#Property_PropertyID").val();
                var currentElement = jQuery(this);
                jQuery(currentElement).parent("li").parent("ul").hide("slow");

                jQuery.ajax({
                    url: "/PropertyDetail/GetSortedPropertyReviews?propertyId=" + propertyId + "&sort=" + typeOfSort,
                    success: function (data, textStatus, jqXHR) {
                        jQuery("#sortByExpressionReviews").html(jQuery(currentElement).html());
                        jQuery('#review-list-container').html(data);

                    },
                    complete: function () {
                    }
                });
            });

            //Attach styled scroll bar for the popup windows    
            jQuery(".scrollable-x-area").slimScroll({
                height: '200px',
                alwaysVisible: true
            });

            self.mapLatitude = jQuery("#Property_Latitude").val().replace(",", ".");            self.mapLongitude = jQuery("#Property_Longitude").val().replace(",", ".");

            //hook click on the body for hidding the appliances dialog
            jQuery("body").click(function () {
                jQuery(".appliance-dialog").fadeOut();
                jQuery("#mlosDialog").fadeOut();
            });

            // Prevent events from getting pass .appliance-dialog
            jQuery(".appliance-dialog").click(function (e) {
                e.stopPropagation();
            });

            jQuery(".btnBookProperty").click(function () {
                var propertyId = jQuery("#Property_PropertyID").val();
                var unitId = jQuery("#Unit_UnitID").val();

                if (dateFrom != '' && dateTo != '') {
                    if (dateFrom === dateTo) {
                        $("<div title='" + self.resources.DialogAtLeastOneNightTitle + "'>" + self.resources.DialogAtLeastOneNightDescription + "</div>").dialog(
                            {
                                modal: true,
                                buttons: {
                                    OK: function () {
                                        $(this).dialog( "close" );
                                    }
                                }
                            }
                            );
                    }
                    else {
                        window.location = "/Booking/BookProperty?dateTripFrom=" + dateFrom + "&dateTripUntil=" + dateTo + "&PropertyId=" + propertyId + "&UnitId=" + unitId;
                    }
                } else {
                    jQuery("#linkGoToAvailability").click();
                }

                return false;
            });

            jQuery("div.collapse-section-trigger").click(function () {
                var sectionId = jQuery(this).attr("data-section");
                jQuery("#" + sectionId).toggle("slow");
                jQuery(this).find(".section-title-overall-value").toggle();

                var collapsibleSection = jQuery(this).find("span.ui-icon-triangle-1-e");
                if (collapsibleSection.size() != 0) {
                    collapsibleSection.removeClass("ui-icon-triangle-1-e");
                    collapsibleSection.addClass("ui-icon-triangle-1-s");
                } else {
                    collapsibleSection = jQuery(this).find("span.ui-icon-triangle-1-s");
                    collapsibleSection.removeClass("ui-icon-triangle-1-s");
                    collapsibleSection.addClass("ui-icon-triangle-1-e");
                }
            });

            getMainSectionInfo();
            //Firstly pre-load all the controls for the Google Street View
            if (google != null) {
                self.client = new google.maps.StreetViewService();
                //initStreetView("streetViewContainer", self.mapLatitude, self.mapLongitude);
            }
            //initAvailabilityCalendar();
            initGallerySlideshow();

            var $sidebar = jQuery(".sidebar"),
                $window = jQuery(window),
                offset = $sidebar.offset(),
                topPadding = 0;

            $window.scroll(function () {
                if ($window.scrollTop() > offset.top) {
                    $sidebar.css('marginTop', $window.scrollTop() - offset.top + topPadding);
                } else {
                    $sidebar.css('marginTop', 0);
                }
            });
        };
    }
    return new ViewModel();

}(oth || {}, jQuery));