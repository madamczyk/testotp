﻿jQuery(document).ready(function () {
    jQuery('.thumbnail_container').each(function (index, element) {
        jQuery(this).cycle({
            fx: 'fade',
            timeout: 2000
        }).cycle("pause");
    });

    jQuery(".portfolio_image").each(function (index, element) {
        jQuery(this).hover(
        function () {
            jQuery(this).find(".thumbnail_container").cycle("resume");
        },
        function () {
            jQuery(this).find(".thumbnail_container").cycle("pause");
        }
        );
    });

    var buttons = {};
    buttons[cancelDialogYesButton] = function () {
        jQuery(this).dialog("close");
        jQuery("#frmCancelRes").submit();
    };
    buttons[cancelDialogNoButton] = function () {
        jQuery(this).dialog("close");
    };

    jQuery("#btnConfirmCancel").click(function (e) {
        e.preventDefault();

        jQuery('<div id="dialog" title="' + cancelDialogTitle + '"><p>' + cancelDialogText + '</p></div>').dialog({
            modal: true,
            buttons: buttons
        });

        return false;
    });


});