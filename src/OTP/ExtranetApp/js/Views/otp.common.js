﻿var oth = oth || {};

oth.common = (function (oth, $, globalize) {
    var CommonObject = function () {
        var self = this;

        //contains localized dateTime patterns.
        self.localizedDatePattern = '';
        self.localizedDigitsDatePattern = '';
        self.localizedTimePattern = '';
        self.localizedDateTimePattern = '';
        self.localizedBedroom = '';
        self.localizedBedrooms = '';
        self.localizedBathroom = '';
        self.localizedBathrooms = '';
        self.localizedGuest = '';
        self.localizedGuests = '';
        self.localizedSleep = "";
        self.localizedSleeps = "";
        self.localizedBath = "";
        self.localizedBaths = "";

        //contains a culture set to the current thread
        self.defaultCulture = '';

        //init function
        self.init = function (options) {
            $.extend(self, options);

            //setting default culture for globalize plugin
            globalize.culture(self.defaultCulture);

            //preventing double submitting forms
            $('form').preventDoubleSubmit();
        }

        //returns formatted date string.
        self.getFormattedDateString = function (date, customFormat) {
            if (typeof (date) === "string")
                date = self.parseDateString(date);

            return globalize.format(date, customFormat);
        }

        //convert date to localized date string
        self.getLocalizedDateString = function (date) {
            if (typeof (date) === "string")
                date = self.parseDateString(date);

            return globalize.format(date, self.localizedDatePattern);
        }

        //convert date to localized digits date string
        self.getLocalizedDigitsDateString = function (date) {
            if (typeof (date) === "string")
                date = self.parseDateString(date);

            return globalize.format(date, self.localizedDigitsDatePattern);
        }

        //convert date to localized time string
        self.getLocalizedTimeString = function (date) {
            if (typeof (date) === "string")
                date = self.parseDateString(date);

            return globalize.format(date, self.localizedTimePattern);
        }
        
        //convert date to localized dateTime string
        self.getLocalizedDateTimeString = function (date) {
            if (typeof (date) === "string")
                date = self.parseDateString(date);

            return globalize.format(date, self.localizedDateTimePattern);
        }

        //parse date string and creates date object
        self.parseDateString = function (date) {
            var parsedDate = new Date(date);

            //fix for safari: not supported date format with "-"
            if (!_isValidDate(parsedDate) && typeof (date) === "string")
                parsedDate = new Date(date.replace(/-/g, '/'));

            if (!_isValidDate(parsedDate))
                parsedDate = globalize.parseDate(date);

            if (!_isValidDate(parsedDate))
                parsedDate = globalize.parseDate(date,
                    [ self.localizedDatePattern, self.localizedTimePattern, self.localizedDateTimePattern ]);

            return parsedDate;
        }

        //cardinality translation helper
        self.getCardinalityResource = function (cardOne, cardMore, value) {
            return value + " " + (value > 1 ? cardMore : cardOne);
        }

        //private functions
        //function determines wether date is valid or not
        function _isValidDate(date) {
            if (Object.prototype.toString.call(date) === "[object Date]" &&
                !isNaN(date.getTime()))
                return true;

            return false;
        }
        
    }

    return new CommonObject();

}(oth || {}, jQuery, Globalize));