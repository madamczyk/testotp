﻿
jQuery(document).ready(function () {

    jQuery("#privacyPolicy").click(function () {
        jQuery("#index").scrollTo("#c2", 800);
        return false;
    });

    jQuery("#cancelationPolicy").click(function () {
        jQuery("#index").scrollTo("#c1", 800);
        return false;
    });

    jQuery("#terms").click(function () {
        jQuery("#index").scrollTo("#c3", 800);
        return false;
    });
})
