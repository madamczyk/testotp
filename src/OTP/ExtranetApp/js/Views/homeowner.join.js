function validateUserEmail(textBox) {
        jQuery.ajax({
            url: "/Account/ValidateEmail?email=" + textBox + "&roleLevel=0",
            success: function (data, textStatus, jqXHR) {
                if(!data.Valid)
                    jQuery('<div id="dialog" title=""><p>' + data.Message + '</p></div>').dialog({
                        modal: false,
                        buttons: {
                            Ok: function () {
                                jQuery(this).dialog("close");
                            }
                        }
                    });
            } 
        });
}

jQuery(document).ready(function () {

    jQuery("form").find('input').each(function () {
        var defValue = jQuery(this).attr("defaultText");
        jQuery(this).watermark(defValue);
    });

    jQuery(".input-select").change(function () {
        if (jQuery(this).val() == '') {
            jQuery(this).css("color", "#999999");
        } else {
            jQuery(this).css("color", "#E1E1E1");
        }
    });

    jQuery("#Email").blur(function () {
        validateUserEmail(jQuery("#Email").val());
    });
});