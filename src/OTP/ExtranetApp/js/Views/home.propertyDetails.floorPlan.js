﻿var oth = oth || {};

oth.floorPlanViewModel = (function (oth, $) {
    var ViewModel = function () {
        var self = this;

        //properties
        self.propertyFloorPlans = null;

        self.menuSelector = ".floor-plan-menu";
        self.menuItemsSelector = self.menuSelector + " .floor-plan-item";
        self.selectedFloorSelector = self.menuSelector + " #selected-floor"
        self.floorPlanImageContainerSelector = "#floor-plan-image-container";
        self.floorPlanImageSelector = self.floorPlanImageContainerSelector + " > img";
        self.floorPlanMarkersSelector = self.floorPlanImageContainerSelector + " > .marker";
        self.propertyId = '';

        //Methods
        //view activation function
        self.init = function (options) {
            $.extend(self, options);

            if (self.propertyFloorPlans !== null && self.propertyFloorPlans.length > 0) {
                _initFloorPlanMenu();
                _changeFloorPlan(self.propertyFloorPlans[0]);
            }
        }

        //private functions
        function _initFloorPlanMenu() {
            $(self.menuSelector).menuNav({ modify_position: true });

            $(self.menuSelector + " li").click(function () {
                $(this).children("ul").toggle("slow");
                return false;
            });

            $(self.menuItemsSelector).click(function () {
                $(this).parent("li").parent("ul").hide("slow");

                var selectedFloorPlanName = $(this).html();
                $.each(self.propertyFloorPlans, function (index, floorPlan) {
                    if (floorPlan.name === selectedFloorPlanName) {
                        _changeFloorPlan(floorPlan);
                        return false;
                    }
                });
            });
        };

        //changes floor plan
        function _changeFloorPlan(floorPlan) {
            $(self.floorPlanImageContainerSelector).fadeOut(800, function () {
                $(self.selectedFloorSelector).html(floorPlan.name);

                $(self.floorPlanMarkersSelector).remove();
                $(self.floorPlanImageSelector).attr("src", floorPlan.displayImage);

                $.each(floorPlan.panoramas, function (index, panorama) {
                    $(self.floorPlanImageContainerSelector)
                        .append(_createMarker(panorama));
                });

                $(this).fadeIn(800);
            });
        }

        //creates marker element for display on image
        function _createMarker(panorama) {
            var imageMarker = $("<div />");
            imageMarker.addClass("marker");

            imageMarker.css("cursor", "pointer");
            imageMarker.css("left", panorama.offsetX);
            imageMarker.css("top", panorama.offsetY);

            imageMarker.click(function () {
                window.location = "/PropertyDetail/PanoramaView/"+self.propertyId+"?panoramaId=" + panorama.id;
            });

            return imageMarker;
        }    
    };

    return new ViewModel();

}(oth || {}, jQuery));