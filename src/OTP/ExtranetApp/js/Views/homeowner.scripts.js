﻿var goToDiv = '';
var offset = 140;

function OnScheduleSubmit(data) {
    if (!data.Valid) {
        jQuery("#vsScheduleMeeting").html(data.Message);
    }
    else {
        window.location = data.Message;
    }
}

function parseHashUrl() {
    goToDiv = window.location.hash;
}

jQuery(document).ready(function () {

    parseHashUrl();

    jQuery("#ownerContract").click(function () {
        jQuery("#index").scrollTo($("#c1").offset().top - offset, 800);
        return false;
    });

    jQuery("#ownerGuestContract").click(function () {
        jQuery("#index").scrollTo($("#c2").offset().top - offset, 800);
        return false;
    });

    jQuery("#insurance").click(function () {
        jQuery("#index").scrollTo($("#c3").offset().top - offset, 800);
        return false;
    });

    jQuery("#privacyPolicy").click(function () {
        jQuery("#index").scrollTo($("#c4").offset().top - offset, 800);
        return false;
    });


    jQuery("#DatesCtrl").multiDatesPicker({
        beforeShowDay: function (selectedDate) {
            var currentDateTime = new Date();
            var currentDate = new Date(currentDateTime.getFullYear(), currentDateTime.getMonth(), currentDateTime.getDate());
            if (selectedDate < currentDate)
                return [false, ""];

            return [true, ""];
        },
        onSelect: function (dateText, inst) {
            var hf = jQuery("#Dates");
            if (hf.val().indexOf(dateText) != -1) {
                hf.val(hf.val().replace("|" + dateText, ""));
            }
            else {
                hf.val(hf.val() + "|" + dateText);
            }
        }
    });

    var timeFromPicker = jQuery("#DateFrom");
    var timeToPicker = jQuery("#DateTo");

    jQuery("#DateFrom").timepicker({
        timeOnly: true,
        alwaysSetTime: true,
        timeFormat: "h tt",
        hourGrid: 4,
        ampm: true,
        showMinute: false,
        onClose: function (dateText, inst) {
            var testStartDate = timeFromPicker.datetimepicker('getDate');

            if (testStartDate == null) {
                testStartDate = new Date();
            }

            var hours = testStartDate.getHours() + 6;
            if (hours > 23) {
                var dateHour = new Date();
                dateHour.setHours(23)
                timeToPicker.datetimepicker('setDate', dateHour);
            } else {                
                var dateHour = testStartDate;
                dateHour.setHours(hours)
                timeToPicker.datetimepicker('setDate', dateHour);
            }
        },
        onSelect: function (selectedDateTime) {
        }
    });
    
    jQuery("#DateTo").timepicker({
        timeOnly: true,
        alwaysSetTime: true,
        timeFormat: "h tt",
        hourGrid: 4,
        ampm: true,
        showMinute: false,
        onClose: function (dateText, inst) {
            if (timeFromPicker.val() != '') {
                var testStartDate = timeFromPicker.datetimepicker('getDate');
                var testEndDate = timeToPicker.datetimepicker('getDate');

                if (testStartDate == null) {
                    testStartDate = new Date();
                }
                if (testEndDate == null) {
                    testEndDate = new Date();
                }
                
                if (testStartDate > testEndDate)
                    timeFromPicker.datetimepicker('setDate', testEndDate);
            }
            else {
                timeFromPicker.val(dateText);
            }
        }
    });

    var dateFromInitial = new Date();
    var dateToInitial = new Date();

    dateFromInitial.setHours(10);
    dateToInitial.setHours(16);

    timeFromPicker.datetimepicker('setDate', dateFromInitial);
    timeToPicker.datetimepicker('setDate', dateToInitial);

    //if (goToDiv != '') {

    //}

});