﻿

jQuery(document).ready(function () {

    jQuery("a#showLoginLink1").click(function () {
        jQuery("div#loginBlockPopup").toggle("slow");
    });

    jQuery("a#showLoginLink2").click(function () {
        jQuery("div#loginBlockPopup").toggle("slow");
    });

    jQuery("a#showLoginLink3").click(function () {
        jQuery("div#loginBlockPopup").toggle("slow");
    });

    jQuery("a#showLoginLink4").click(function () {
        jQuery("div#loginBlockPopup").toggle("slow");
    });

    jQuery("#guests").click(function () {
        jQuery("#index").scrollTo("#c1", 800);
        return false;
    });

    jQuery("#homeowners").click(function () {
        jQuery("#index").scrollTo("#c2", 800);
        return false;
    });

    jQuery("#whyOTP").click(function () {
        jQuery("#index").scrollTo("#c3", 800);
        return false;
    });

    jQuery("#help").click(function () {
        jQuery("#index").scrollTo("#c5", 800);
        return false;
    });

    jQuery("#security").click(function () {
        jQuery("#index").scrollTo("#c7", 800);
        return false;
    });

    jQuery("#myTrip").click(function () {
        jQuery("#index").scrollTo("#c4", 800);
        return false;
    });

    jQuery("#payments").click(function () {
        jQuery("#index").scrollTo("#c6", 800);
        return false;
    });

    jQuery("#cancel").click(function () {
        jQuery("#index").scrollTo("#c8", 800);
        return false;
    });

    jQuery("#maximize").click(function () {
        jQuery("#index").scrollTo("#c9", 800);
        return false;
    });

    jQuery("#joining").click(function () {
        jQuery("#index").scrollTo("#c10", 800);
        return false;
    });

    jQuery("#safety").click(function () {
        jQuery("#index").scrollTo("#c11", 800);
        return false;
    });

    jQuery("#fees").click(function () {
        jQuery("#index").scrollTo("#c12", 800);
        return false;
    });

    jQuery("#ab1").click(function () {
        jQuery("#index").scrollTo("#c10", 800);
        return false;
    });

    jQuery("#ab3").click(function () {
        jQuery("#index").scrollTo("#c10", 800);
        return false;
    });
})
