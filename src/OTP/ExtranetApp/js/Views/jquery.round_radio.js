﻿/// jQuery custom radios plugin for Oh the places
/// @Author BRaczek
/// usage $(selector).round_radioes()

(function ($) {
    var methods = {
        init: function (options) {
            return this.each(function (options) {
                var settings = $.extend({
                    'checkedClass': 'radio-state-checked',
                    'uncheckedClass': 'radio-state-notchecked'
                }, options);

                var radio = $(this);
                var spanElement = jQuery("<span />");

                if (radio.prop("checked"))
                    jQuery(spanElement).addClass(settings.checkedClass);
                else
                    jQuery(spanElement).addClass(settings.uncheckedClass);
                radio.css("visibility", "hidden");
                var parent = radio.parent();
                radio.remove();
                parent.prepend(spanElement);
                spanElement.append(radio);
                spanElement.bind('click.round_radioes', { settings: settings }, methods.toggle);
            });
        },
        destroy: function () {
            return this.each(function () {
                var radio = $(this);
                var parentSpan = radio.parent();
                parentSpan.unbind("round_radioes");
            });
        },
        toggle: function (event) {
            var spanElement = $(this);
            var element = spanElement.find("input");
            var settings = event.data.settings;

            spanElement.removeClass(settings.uncheckedClass);
            spanElement.addClass(settings.checkedClass);
            element.prop("checked", true);

            var radioGroupName = element.prop("name");

            var radioGroupList = $('input[name="' + radioGroupName + '"]')
            radioGroupList.each(function (index, ele) {
                var elemInList = $(ele);

                if (elemInList.is(element))
                    return;
                else {
                    elemInList.parent().removeClass(settings.checkedClass);
                    elemInList.parent().addClass(settings.uncheckedClass);
                    elemInList.prop("checked", false);
                }
            });
        }
    }

    $.fn.round_radio = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.round_radio');
        }
    }
})(jQuery)