﻿/// <reference path="account.forgotPassword.js" />
/* 
Common used JS logic across application
*/

/*-----------------------------------------------------------------------------------*/
/*	Prometheus Custom Script
/*-----------------------------------------------------------------------------------*/

var destination = "";
var dateFrom = "";
var dateTo = "";
var guests = "";
var bathrooms = "";
var bedrooms = "";
var propertyTypes = "";
var amenities = "";
var experiences = "";
var sort = "";
var refineSearchTextInitialValue = "";

var dateFromInitialValue = "";
var dateUntilInitailValue = "";

var refineSearchFilterText = '';
var propertyTypesTextValues = "";
var amenitiesTextValues = "";
var experiencesTextValues = "";

var defaultDateFrom = Date.today();
var defaultDateUntil = Date.today();
var currentDateFrom = "";
var currentDateUntil = "";

var defaultDestination = '';

var events = null;

//Invoked by click on the 'Score' link (ListView, MapView, Slideshow)
//Displays popup with details about non matching search criteria
function SetFinalScoreDetailsHandler() {
    jQuery(".scoreDetailsLink").click(function (event) {
        event.stopPropagation();
        var scoreValue = jQuery(this).attr("searchfinalscore-value");
        if (scoreValue == "100")
            return false;
        var unitId = jQuery(this).attr("searchfinalscore-unitid");
        var showUp = jQuery(this).attr("slideshow");
        var posX = event.pageX - 50;
        var posY = event.pageY + 10;
        var cssPos = 'top';
        if (showUp == "1") {
            cssPos = 'bottom';
            posX = 80;
            posY = 120;
        }
        DisplayScoreDetailsPopUp(unitId, posX, posY, cssPos);
        return false;
    });
}

//Show popup with information about search score score details on the map view
function ShowMapViewFinalScoreDetails(elem, unitId, scoreValue) {
    if (scoreValue == 100)
        return false;
    DisplayScoreDetailsPopUp(unitId, jQuery(elem).offset().left + 70, jQuery(elem).offset().top - 192, 'top');
    return false;
}

//Displays popup with information about search score score details on the SlideShow View and List View
function DisplayScoreDetailsPopUp(unitId, x, y, topBottom) {
    jQuery("#searchScoreDetailsContainer").html("");
    jQuery("#searchScoreDetailsContainer")
        .append(jQuery("<img style='background: none;' src='/images/otp/ajax_activity_indicator.gif' />"))
        .addClass("finalScoreDetails-dialog")
        .css('left', x)
        .css(topBottom, y)
        .show();
    jQuery.ajax({
        global: false,
        url: "/Home/GetScoreDetails?unitId=" + unitId + "&" + getSearchParameters(),
        success: function (data, textStatus, jqXHR) {
            jQuery("#searchScoreDetailsContainer").html(data);
        }
    });
}

//dummy for override in child scripts
function searchRequest() {
}

//dummy for override in child scripts, using Navigation datepickers
function getDataForDate(date) {
    return {};
}

function checkForEmptyResultAndPopup(htmlElem) {
    if (jQuery(htmlElem).find("img").size() == 0) { //no propertise were found
        jQuery("#empty-result-dialog").dialog("open");
    }
}

function resetSearchCriteria() {
    destination = defaultDestination;
    dateFrom = "";
    dateTo = "";
    guests = "";
    bathrooms = "";
    bedrooms = "";
    propertyTypes = "";
    amenities = "";
    experiences = "";
    sort = "";

    jQuery('#experienceList, #amenitiesList, #propertyList').find("input[type='checkbox']").each(function () {
        jQuery(this).prop("checked", false);
        highlightRefineSearchSelectedConditionCheckbox(jQuery(this));
        highlightRefineSearchSelectedConditionCheckboxParent(jQuery(this), false);
    });
    jQuery.uniform.update();

    //parseRefineSearchTextValues();    


    jQuery("#dateFromLink").html(dateFromInitialValue);
    jQuery("#dateToLink").html(dateUntilInitailValue);
    jQuery("#guestsCount, #bathroomsCount, #bedroomsCount").val('');
    highlightRefineSearchSelectedCondition(jQuery("#guestsCount"));
    highlightRefineSearchSelectedCondition(jQuery("#bathroomsCount"));
    highlightRefineSearchSelectedCondition(jQuery("#bedroomsCount"));

    refineSearchFilterText = '';
    propertyTypesTextValues = "";
    amenitiesTextValues = "";
    experiencesTextValues = "";

    parseRefineSearchTextValues();

}


//invoked when login completed
function onTestComplete(data) {
    if (data.Valid) {
        if (data.IsPasswordGenerated) {
            window.location = "/Account/ChangePassword";
        } else {
            window.location = "/Guest/Mytrips";
        }
    }
    else {
        jQuery("#blockLogin").html(data.Message);
    }
}

function onJoinComplete(data) {
    if (data.Valid) {
        jQuery("div#joinBlockPopup").hide("slow");
        jQuery("div#joinBlockPopup").find("form").clearForm();
        ShowDialog("Message", data.Message)
    }
    else {
        jQuery("#blockJoin").html(data.Message);
    }
}

function ShowDialog(title, description) {
    jQuery('<div id="dialog" title="' + title + '"><p>' + description + '</p></div>').dialog({
        modal: false,
        buttons: {
            Ok: function () {
                jQuery(this).dialog("close");
            }
        }
    });
}

//checks if validation passed (numbers 1-9)
function validateRefineSearchNumberCondition(htmlElem) {
    var inputVal = htmlElem.val();
    if (inputVal.length > 1)
        inputVal = inputVal.substr(0, 1);
    if (!jQuery.isNumeric(inputVal) || inputVal == '0') {
        htmlElem.val("");
        return;
    }
    htmlElem.val(inputVal);
}

function highlightRefineSearchSelectedCondition(htmlElem) {
    var inputVal = htmlElem.val();
    if (inputVal != '') {
        htmlElem.parent('a').css("color", "#FFFFFF");
    } else {
        htmlElem.parent('a').css("color", "#868686");
    }
}

function highlightRefineSearchSelectedConditionCheckbox(htmlElem) {
    if (htmlElem.prop('checked')) {
        getFilterChecboxALinkElem(htmlElem).css("color", "#FFFFFF");
    } else {
        getFilterChecboxALinkElem(htmlElem).css("color", "#868686");
    }
}


function highlightRefineSearchSelectedConditionCheckboxParent(htmlElem, highlight) {
    if (highlight) {
        getFilterChecboxUlParentElem(htmlElem).prev("a").css("color", "#FFFFFF");
    } else {
        getFilterChecboxUlParentElem(htmlElem).prev("a").css("color", "#868686");
    }
}

function getFilterChecboxALinkElem(htmlElem) {
    //when using fancy checbox image - dom tree is changed
    return htmlElem.parent().parent().parent('a');
}

function getFilterChecboxUlParentElem(htmlElem) {
    //when using fancy checbox image - dom tree is changed
    return htmlElem.parent().parent().parent().parent().parent("ul");
}

function disableFilterChecboxes() {
    jQuery('#experienceList, #amenitiesList, #propertyList').find("input[type='checkbox']").prop("disabled", true);
    jQuery.uniform.update();

}

function enableFilterChecboxes() {
    jQuery('#experienceList, #amenitiesList, #propertyList').find("input[type='checkbox']").prop("disabled", false);
    jQuery.uniform.update();

}

function getDestinationTextValueById(destinationId) {
    var searchPathUrl = "/NavBar/GetDestinationNameById?id="
    jQuery.ajax({
        url: searchPathUrl + destinationId,
        success: function (data, textStatus, jqXHR) {
            jQuery("#destinationLink").text(data.toString());
        }
    });

}

function setMenuValues(menuValues) {
    if (menuValues.destination != null) {
        destination = menuValues.destination;
        getDestinationTextValueById(destination);
    }
    if (menuValues.dateFrom != null) {
        jQuery("#dateFromLink").text(oth.common.getLocalizedDateString(menuValues.dateFrom.toString()));
        dateFrom = menuValues.dateFrom;
    }
    if (menuValues.dateTo != null) {
        jQuery("#dateToLink").text(oth.common.getLocalizedDateString((menuValues.dateTo.toString())));
        dateTo = menuValues.dateTo;
    }
    if (menuValues.guests != null) {
        jQuery("#guestsCount").val(menuValues.guests.toString());
        guests = menuValues.guests;
        validateRefineSearchNumberCondition(jQuery("#guestsCount"));
        highlightRefineSearchSelectedCondition(jQuery("#guestsCount"));
    }
    if (menuValues.bedrooms != null) {
        jQuery("#bedroomsCount").val(menuValues.bedrooms.toString());
        bedrooms = menuValues.bedrooms;
        validateRefineSearchNumberCondition(jQuery("#bedroomsCount"));
        highlightRefineSearchSelectedCondition(jQuery("#bedroomsCount"));
    }
    if (menuValues.bathrooms != null) {
        jQuery("#bathroomsCount").val(menuValues.bathrooms.toString());
        bathrooms = menuValues.bathrooms;
        validateRefineSearchNumberCondition(jQuery("#bathroomsCount"));
        highlightRefineSearchSelectedCondition(jQuery("#bathroomsCount"));
    }
    if (menuValues.propertyTypes != null) {
        menuValues.propertyTypes = menuValues.propertyTypes.replace(new RegExp('%3b', 'g'), ';');
        propertyTypes = menuValues.propertyTypes;
        setCheckboxSelected(menuValues.propertyTypes, 'property');
    }
    if (menuValues.experiences != null) {
        menuValues.experiences = menuValues.experiences.replace(new RegExp('%3b', 'g'), ';');
        experiences = menuValues.experiences;
        setCheckboxSelected(menuValues.experiences, 'experience');
    }
    if (menuValues.amenities != null) {
        menuValues.amenities = menuValues.amenities.replace(new RegExp('%3b', 'g'), ';');
        amenities = menuValues.amenities;
        setCheckboxSelected(menuValues.amenities, 'amenity');
    }

    jQuery.uniform.update();

    parseRefineSearchTextValues();
}

function setCheckboxSelected(ids, elemId) {
    ids = ids.split(';');
    for (i = 0; i < ids.length; i++) {
        if (ids[i] != '') {
            var computedElementId = "#" + elemId + '-' + ids[i];

            jQuery(computedElementId).prop('checked', true);
            //jQuery(computedElementId).parent("span").addClass('checked');
            highlightRefineSearchSelectedConditionCheckbox(jQuery(computedElementId));
            highlightRefineSearchSelectedConditionCheckboxParent(jQuery(computedElementId), true);

            if (elemId == 'amenity') {
                amenitiesTextValues += getFilterChecboxALinkElem(jQuery(computedElementId)).text().trim() + ", ";
            }
            if (elemId == 'property') {
                propertyTypesTextValues += getFilterChecboxALinkElem(jQuery(computedElementId)).text().trim() + ", ";
            }
            if (elemId == 'experience') {
                experiencesTextValues += getFilterChecboxALinkElem(jQuery(computedElementId)).text().trim() + ", ";
            }
        }
    }
}

function parseHashUrl(aURL) {

    aURL = aURL || window.location.href;
    if ((aURL.indexOf('#') == -1) || (aURL.length - 1) == (aURL.indexOf('#')))
        return null;

    var vars = {};
    var hashes = aURL.slice(aURL.indexOf('#') + 1).split('&');

    for (var i = 0; i < hashes.length; i++) {
        var hash = hashes[i].split('=');

        if (hash.length > 1) {
            vars[hash[0]] = hash[1];
        }
    }
    return vars;
}

function getSearchParameters(ignoreDestination) {
    searchParams = "";

    if (destination.length != 0 && (ignoreDestination === undefined || ignoreDestination === false)) {
        searchParams += "destination=" + destination + "&";
    }
    if (dateFrom.length != 0) {
        searchParams += "dateFrom=" + dateFrom + "&";
    }
    if (dateTo.length != 0) {
        searchParams += "dateTo=" + dateTo + "&";
    }
    if (guests.length != 0) {
        searchParams += "guests=" + guests + "&";
    }
    if (bedrooms.length != 0) {
        searchParams += "bedrooms=" + bedrooms + "&";
    }
    if (bathrooms.length != 0) {
        searchParams += "bathrooms=" + bathrooms + "&";
    }
    if (propertyTypes.length != 0) {
        searchParams += "propertyTypes=" + propertyTypes + "&";
    }
    if (amenities.length != 0) {
        searchParams += "amenities=" + amenities + "&";
    }
    if (experiences.length != 0) {
        searchParams += "experiences=" + experiences + "&";
    }
    if (sort.length != 0) {
        searchParams += "sort_by=" + sort + "&";
    }

    return searchParams;
}

function performRefineSearch(htmlElem) {
    if (htmlElem.attr("id") == "guestsCount") {
        highlightRefineSearchSelectedCondition(jQuery("a#refineSearchLink").siblings("ul").find("#guestsCount"));
        guests = jQuery("a#refineSearchLink").siblings("ul").find("#guestsCount")[0].value;
    }
    if (htmlElem.attr("id") == "bathroomsCount") {
        highlightRefineSearchSelectedCondition(jQuery("a#refineSearchLink").siblings("ul").find("#bathroomsCount"));
        bathrooms = jQuery("a#refineSearchLink").siblings("ul").find("#bathroomsCount")[0].value;

    }
    if (htmlElem.attr("id") == "bedroomsCount") {
        highlightRefineSearchSelectedCondition(jQuery("a#refineSearchLink").siblings("ul").find("#bedroomsCount"));
        bedrooms = jQuery("a#refineSearchLink").siblings("ul").find("#bedroomsCount")[0].value;
    }
    if (htmlElem.attr("id") == "amenitiesList") {
        amenities = "";
        amenitiesTextValues = "";
        jQuery('#amenitiesList').find('input[type=checkbox]:checked').each(function () {
            amenities += jQuery(this).attr("id").replace("amenity-", "") + ";";
            amenitiesTextValues += getFilterChecboxALinkElem(jQuery(this)).text().replace(/[\s\n\r]+/g, ' ') + ", ";
        });

        jQuery('#amenitiesList').find('input[type=checkbox]').each(function (index, element) {
            highlightRefineSearchSelectedConditionCheckbox(jQuery(this));
        });
    }
    if (htmlElem.attr("id") == "experienceList") {
        experiences = "";
        experiencesTextValues = "";
        jQuery('#experienceList').find('input[type=checkbox]:checked').each(function () {
            experiences += jQuery(this).attr("id").replace("experience-", "") + ";";
            experiencesTextValues += getFilterChecboxALinkElem(jQuery(this)).text().replace(/[\s\n\r]+/g, ' ') + ", ";
        });

        jQuery('#experienceList').find('input[type=checkbox]').each(function (index, element) {
            highlightRefineSearchSelectedConditionCheckbox(jQuery(this));
        });
    }
    if (htmlElem.attr("id") == "propertyList") {
        propertyTypes = "";
        propertyTypesTextValues = "";
        jQuery('#propertyList').find('input[type=checkbox]:checked').each(function () {
            propertyTypes += jQuery(this).attr("id").replace("property-", "") + ";";
            propertyTypesTextValues += getFilterChecboxALinkElem(jQuery(this)).text().replace(/[\s\n\r]+/g, ' ') + ", ";
        });

        jQuery('#propertyList').find('input[type=checkbox]').each(function (index, element) {
            highlightRefineSearchSelectedConditionCheckbox(jQuery(this));
        });
    }

    parseRefineSearchTextValues();

    searchRequest();
}

/*
Gets text values from refine filter to put in Link Text
*/
function parseRefineSearchTextValues() {

    refineSearchFilterText = "";

    if (guests != '')
        refineSearchFilterText += oth.common.getCardinalityResource(oth.common.localizedGuest, oth.common.localizedGuests, guests) + ", ";
    if (bathrooms != '')
        refineSearchFilterText += oth.common.getCardinalityResource(oth.common.localizedBathroom, oth.common.localizedBathrooms, bathrooms) + ", ";
    if (bedrooms != '')
        refineSearchFilterText += oth.common.getCardinalityResource(oth.common.localizedBedroom, oth.common.localizedBedrooms, bedrooms) + ", ";
    if (propertyTypesTextValues != '')
        refineSearchFilterText += propertyTypesTextValues.substring(0, propertyTypesTextValues.length - 2) + ", ";
    if (amenitiesTextValues != '')
        refineSearchFilterText += amenitiesTextValues.substring(0, amenitiesTextValues.length - 2) + ", ";
    if (experiencesTextValues != '')
        refineSearchFilterText += experiencesTextValues.substring(0, experiencesTextValues.length - 2) + ", ";

    if (refineSearchFilterText != '') {
        refineSearchFilterText = refineSearchFilterText.substring(0, refineSearchFilterText.length - 2);
    }

    jQuery(".scroller").RemoveScroller();

    if (refineSearchFilterText == '') {
        jQuery("a#refineSearchLink div div").text(refineSearchTextInitialValue);
    } else {
        jQuery("a#refineSearchLink div div").text(refineSearchFilterText);

        if (refineSearchFilterText.length > 13) {
            jQuery(".scroller").SetScroller({
                velocity: 60,
                direction: 'horizontal',
                startfrom: 'right',
                loop: 'infinite',
                movetype: 'linear',
                onmouseover: 'play',
                onmouseout: 'play',
                onstartup: 'play',
                cursor: 'pointer'
            });
        }
    }


}

/*
Sets cookie info for JPlayer to support playing mp3 after changing webpage
*/
function setJPlayerCookieVariables() {
    var playerCurrentStatus = jQuery("#audioBG").data("jPlayer").status.paused;
    jQuery("#audioBG").jPlayer("pause");
    var playerCurrentLocation = jQuery("#audioBG").data("jPlayer").status.currentTime;
    jQuery.cookie("playerCurrentLocation", playerCurrentLocation, { path: '/' });
    jQuery.cookie("playerCurrentStatus", playerCurrentStatus, { path: '/' });
}

/*
Gets cookie info for JPlayer about last JPlayer paused status
*/
function getJPlayerCookiePlayerStatus() {
    return jQuery.cookie("playerCurrentStatus");
}

/*
Gets cookie info for JPlayer about last known plaing location
*/
function getJPlayerCookiePlayerCurrentLocation() {
    return parseFloat(jQuery.cookie("playerCurrentLocation"));
}

function setReturnSearchPageCookie() {
    var url = window.location;
    jQuery.cookie("searchReturnUrl", url, { path: '/' });
}

function getReturnSearchPageCookie() {
    return jQuery.cookie("searchReturnUrl");
}

//Hook method for the sort event
function sortEventHook(sortTypeValue) {
}

//Retrieves destination id by text value
function PerformDestinationEnterSearch(termVal) {
    jQuery.getJSON('/NavBar/FindExactDestination',
        { term: termVal },
        function (response) {
            if (response != null) {
                jQuery(".ui-autocomplete").hide();
                jQuery(".ui-autocomplete .ui-menu-item").hide();

                jQuery("#destinationLink").text(response.value);
                jQuery("#localisationPhrase").autocomplete('close');
                jQuery("#destinationLink").siblings("div").hide("slow");

                destination = response.id;
                searchRequest();

                return false;
            }
        }
    );
}
//Validate if Email in Join Guest is unique; If not - displays message with info
function validateUserEmail(textBox) {
    jQuery.ajax({
        global: false,
        url: "/Account/ValidateEmail?email=" + textBox + "&roleLevel=1",
        success: function (data, textStatus, jqXHR) {
            if (!data.Valid)
                jQuery('<div id="dialog" title=""><p>' + data.Message + '</p></div>').dialog({
                    modal: false,
                    close: function (e, ui) { e.stopPropagation(); },
                    buttons: {
                        Ok: function () {
                            jQuery(this).dialog("close");
                        }
                    }
                });
        }
    });
}

jQuery.noConflict();

jQuery(document).ready(function () {
    //initial value of the refine search filter label
    refineSearchTextInitialValue = jQuery("a#refineSearchLink").text();
    dateFromInitialValue = jQuery("#dateFromLink").html();
    dateUntilInitailValue = jQuery("#dateToLink").html();

    currentDateFrom = Date.today().addMonths(1);
    currentDateUntil = Date.today().addMonths(1).addWeeks(1);


    //initial set destination
    defaultDestination = jQuery("#Location_ID").val();
    destination = defaultDestination;

    jQuery("#img-logo").jrumble({
        x: 0,
        y: 5,
        rotation: 0,
        speed: 60
    });

    jQuery.ajaxSetup({
        cache: false
    });

    jQuery(document).ajaxStart(function () {

        jQuery("#img-logo").trigger('startRumble', [0, 5]);

        //jQuery("#ajaxLoadingIndicator").show("slow");
    }).ajaxError(function () {
        jQuery("#img-logo").effect("shake");

    }).ajaxComplete(function () {
        jQuery("#img-logo").trigger('stopRumble');
        //jQuery("#ajaxLoadingIndicator").hide("slow");
    });


    var firstGroupContainerExp = jQuery("ul#experienceList").find("li.group-countainer").first();
    firstGroupContainerExp.siblings('li.groupped-item[data-group-id="' + firstGroupContainerExp.attr("data-group-id") + '"]').show();
    firstGroupContainerExp.find("span.ui-icon-triangle-1-e").toggleClass("ui-icon-triangle-1-e").toggleClass("ui-icon-triangle-1-s");
    var firstGroupContainerAmet = jQuery("ul#amenitiesList").find("li.group-countainer").first();
    firstGroupContainerAmet.find("span.ui-icon-triangle-1-e").toggleClass("ui-icon-triangle-1-e").toggleClass("ui-icon-triangle-1-s");
    firstGroupContainerAmet.siblings('li.groupped-item[data-group-id="' + firstGroupContainerAmet.attr("data-group-id") + '"]').show();

    jQuery("li.group-countainer").click(function () {
        var groupId = jQuery(this).attr("data-group-id");
        jQuery(this).find("span").first().addClass("ui-icon-triangle-1-s").removeClass("ui-icon-triangle-1-e");
        jQuery(this).siblings("li.group-countainer").find("span").removeClass("ui-icon-triangle-1-s").addClass("ui-icon-triangle-1-e");
        jQuery(this).siblings('li.groupped-item[data-group-id!="' + groupId + '"]').hide();
        jQuery(this).siblings('li.groupped-item[data-group-id="' + groupId + '"]').toggle("slow");
    });

    jQuery("#empty-result-dialog").dialog({
        autoOpen: false,
        modal: true,
        width: 400,
        height: 180,
        buttons: {
            Ok: function () {
                resetSearchCriteria();
                searchRequest();
                getDestinationTextValueById(destination);
                jQuery(this).dialog("close");
            }
        }
    });


    jQuery(window).bind('beforeunload', function () {
        setJPlayerCookieVariables();
    });

    jQuery(".popup-dialog").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            Ok: function () {
                jQuery(this).dialog("close");
            }
        }
    });


    jQuery("li#nv-return-search > a").click(function () {
        window.location = getReturnSearchPageCookie();
    });

    jQuery("#joinFormClose").click(function () {
        jQuery("#joinBlockPopup").hide(1);
    });

    jQuery('#flexsliderContainer, nav').click(function (sender, event) {
        jQuery("#banner > nav > ul > li > div").hide("slow");
        jQuery("#banner > nav > ul > li > ul").hide("slow");
    });

    jQuery("#localisationPhrase").autocomplete(
    {
        source: '/NavBar/FindDestination',
        select: function (event, ui) {
            jQuery("#destinationLink").text(ui.item.value);
            jQuery("#destinationLink").siblings("div").toggle("slow");
            destination = ui.item.id;
            searchRequest();
        },
        minLength: 2
    });

    jQuery("#localisationPhrase").keyup(function (e) {
        if (e.keyCode == 13)
            if (jQuery(this).val() != '') {
                PerformDestinationEnterSearch(jQuery(this).val());
            }
    });

    jQuery("a#destinationLink").siblings("div").find("a").click(function () {
        jQuery("#destinationLink").text(jQuery(this).text());
        jQuery("#destinationLink").siblings("div").toggle("slow");
        destination = jQuery(this).attr("id").replace("destination-", "");
        searchRequest();
    });

    jQuery("a.changeViewLink").each(function (index, element) {
        jQuery(this).click(function () {
            window.location = jQuery(this).attr("href") + location.hash;
            return false;
        });
    });

    jQuery("a.goToHomeLink").each(function (index, element) {
        jQuery(this).click(function () {
            window.location = jQuery(this).attr("href");
            return false;
        });
    });

    jQuery("a#refineSearchLink").siblings("ul").find("#guestsCount, #bathroomsCount, #bedroomsCount").keyup(function (e) {
        if (e.keyCode == 13) {
            jQuery(this).next(".refineSearchOkButton").click();
        } else {
            validateRefineSearchNumberCondition(jQuery(this));
            jQuery(this).next(".refineSearchOkButton").css("display", "block");
        }
    });

    jQuery(".refineSearchOkButton").click(function () {
        performRefineSearch(jQuery(this).prev("input"));
        jQuery(this).css("display", "none");
        var nextInput = jQuery(this).parent('a').parent("li").next("li").find("input");
        if (nextInput != null) {
            nextInput.focus();
        }
    });

    ///checbox
    jQuery('#experienceList, #amenitiesList, #propertyList').find("input[type=checkbox]").click(function () {

        var ulListElem = getFilterChecboxUlParentElem(jQuery(this));

        performRefineSearch(ulListElem);

        if (ulListElem.find("input[type=checkbox]:checked").size() > 0) {
            highlightRefineSearchSelectedConditionCheckboxParent(jQuery(this), true);
        } else {
            highlightRefineSearchSelectedConditionCheckboxParent(jQuery(this), false);
        }

        //jQuery(this).parent().parent().parent("ul").hide("slow");
    });

    jQuery(".sort_type").each(function (index, element) {
        jQuery(this).click(function () {
            sort = jQuery(this).attr("data-sort");
            sortEventHook(sort);
            //searchRequest();

            jQuery(this).parent("li").parent("ul").toggle("slow");
        });
    });

    jQuery("#banner > nav > ul > li > div").click(function () { return false; });

    jQuery("a#showLoginLink").click(function () {
        jQuery("div#loginBlockPopup").toggle("slow");
        jQuery("ul#aboutUsList").hide("slow");
        jQuery("div#joinBlockPopup").hide("slow");
        jQuery("#banner nav li").children("ul").hide("slow");
        jQuery("#banner nav li").children("div.scrollable-y-area").children("ul").hide("slow");
        jQuery("#banner nav li").children("div").not(".scrollable-y-area").hide("slow");
        return false;
    });

    jQuery("a.joinLink").click(function () {
        jQuery("div#joinBlockPopup").toggle("slow");
        jQuery("div#loginBlockPopup").hide("slow");
        jQuery("#banner nav li").children("ul").hide("slow");
        jQuery("#banner nav li").children("div.scrollable-y-area").children("ul").hide("slow");
        jQuery("#banner nav li").children("div").not(".scrollable-y-area").hide("slow");
        return false;
    })

    jQuery("a#showAboutUsLink").click(function () {
        jQuery("ul#aboutUsList").toggle("slow");
        jQuery("div#loginBlockPopup").hide("slow");
        jQuery("div#joinBlockPopup").hide("slow");

        jQuery("#banner nav li").children("ul").hide("slow");
        jQuery("#banner nav li").children("div.scrollable-y-area").children("ul").hide("slow");
        jQuery("#banner nav li").children("div").not(".scrollable-y-area").hide("slow");
        return false;
    });

    jQuery("#banner nav li").click(function () {
        jQuery(this).children("ul").toggle("slow", function () {
            jQuery("a#refineSearchLink").siblings("ul").find("#guestsCount").focus();
        });
        jQuery(this).children("ul").children("li").children("ul").hide(); //close child elements amenities and so on when clicked again on refine search

        jQuery(this).siblings("li").children("ul").hide("slow");
        //jQuery(this).siblings("li").children("div.scrollable-y-area").children("ul").hide("slow");
        jQuery(this).siblings("li").children("ul").children("li").find("ul").hide("slow");
        //jQuery(this).siblings("li").children("div.scrollable-y-area").children("ul").children("li").find("ul").hide("slow"); // child menus {amenities, properties and so on}
        jQuery(this).children("div").not(".scrollable-y-area").toggle("slow", function () {
            jQuery("#localisationPhrase").focus();
        }); //calendar, destination div
        jQuery(this).siblings("li").children("div").not(".scrollable-y-area").hide("slow");
        jQuery(this).parent("ul").siblings("ul").children("li").children("ul").hide("slow"); //siblings ul elements to the sort element
        jQuery(this).parent("ul").siblings("ul").children("li").children("div").not(".scrollable-y-area").hide("slow"); // siblings div elements to the sort element

        jQuery("div#joinBlockPopup").hide("slow");
        jQuery("div#loginBlockPopup").hide("slow");
        jQuery("ul#aboutUsList").hide("slow");
        jQuery("div#searchScoreDetailsContainer").hide("slow");

        //if (jQuery(this).parent("ul").index() != jQuery("ul.sort_element").index()) {
        //    jQuery("ul.sort_element li").children("ul").hide("slow");
        //}        
        if (dateFrom != "" && dateTo != "") {
            defaultDateFrom = dateFrom;
            defaultDateUntil = dateTo;
            currentDateFrom = dateFrom;
            currentDateUntil = dateTo;
        }


        //http://www.eyecon.ro/datepicker/
        if (jQuery(this).find(".datepicker").length == 0) {
            jQuery(this).find('#datepickerFrom').DatePickerNavigation({
                tag: [dateFrom, dateTo],
                flat: true,
                date: defaultDateFrom,
                current: currentDateFrom,
                onRender: function (date){
                    return getDataForDate(date);
                },
                onChange: function (dateSelected) {

                    // if (dateTo != '' &&  Date.compare(new Date(dateSelected), new Date(dateTo)) >= 0) {
                    //     jQuery("#date-from-wrong-dialog").dialog("open");
                    //     return;
                    // } 

                    dateFrom = dateSelected;
                    //jQuery("a#dateFromLink").siblings("span").first().text(dateSelected);
                    jQuery("a#dateFromLink").text(oth.common.getLocalizedDateString(dateSelected));
                    jQuery("a#dateFromLink").siblings("div").toggle("slow");
                    jQuery("a#dateToLink").parent("li").trigger("click");
                    var options = jQuery(this).data('datepicker');
                    if (dateTo != '' && dateTo != dateFrom && options.search == true) {
                        searchRequest();
                    }
                    jQuery(this).find('#datepickerTo').DatePickerNavigationSetDate(dateTo);
                    jQuery("#datepickerTo .datepickerBorderT").click();
                },
                calendars: 3,
                starts: 0
            });

            jQuery(this).find('#datepickerTo').DatePickerNavigation({
                tag: [dateFrom, dateTo],
                flat: true,
                date: defaultDateUntil,
                current: defaultDateUntil,
                onRender: function (date) {
                    return getDataForDate(date);
                },
                onChange: function (dateSelected) {
                    var propertyDetailsViewModel = oth.propertyDetailsViewModel || null;
                    if (dateFrom != '' && Date.compare(new Date(dateFrom.replace(/-/g, '/')), new Date(dateSelected.replace(/-/g, '/'))) >= 0) {
                        if (propertyDetailsViewModel == null) {
                            jQuery("#date-until-wrong-dialog").find('.date').css("color", "white").html(dateFrom + ".");
                            jQuery("#date-until-wrong-dialog").dialog("open");
                        }
                        return;
                    }

                    dateTo = dateSelected;
                    //jQuery("a#dateToLink").siblings("span").first().text(dateSelected);
                    jQuery("a#dateToLink").text(oth.common.getLocalizedDateString(dateSelected));
                    jQuery("a#dateToLink").siblings("div").toggle("slow");
                    jQuery("#datepickerFrom .datepickerBorderT").click();
                    if (dateTo != '' && dateTo != dateFrom) {
                        searchRequest();
                    }
                },
                calendars: 1,
                starts: 0
            });
        }

        if (jQuery(this).children("a").first().attr("id") == "refineSearchLink")
            jQuery("a#refineSearchLink").siblings("ul").find("#guestsCount").focus();

        return false;
    });

    jQuery("#banner nav input").click(function (event) {
        event.stopPropagation();
    });

    jQuery("body, .section-nav-links").click(function () {
        jQuery("nav").children("ul").children("li").children("ul").hide("slow");
        jQuery("nav").children("ul").children("li").children("div").hide("slow");
        jQuery("div#joinBlockPopup").hide("slow");
        jQuery("div#loginBlockPopup").hide("slow");
        jQuery("ul#aboutUsList").hide("slow");
        jQuery("div#searchScoreDetailsContainer").hide("slow");
    });

    jQuery("#loginBlockPopup, #joinForm").click(function (e) {
        e.stopPropagation();
    });

    /*
    window.addEventListener('DOMContentLoaded',function() {
        elem = jQuery("#index");
        if (elem.is (".home")) {
            jQuery(document).ready(function () {
                jQuery("#flexslider").queryLoader2({
                    completeAnimation: "fade"
                });
            });
        };
    });
    */


    //menu		
    jQuery("#banner nav").menuNav({ modify_position: true });

    // improves menu for mobile devices
    jQuery('#banner nav ul:eq(0)').mobileMenu({
        switchWidth: 768,                   							//width (in px to switch at)
        topOptionText: jQuery('#banner nav').data('selectname'),     	//first option text
        indentString: '&nbsp;&nbsp;&nbsp;'  							//string for indenting nested items
    });


    jQuery('.settings .openclose').click(function () {
        var target = jQuery(this).parent().parent('.settings');
        if (target.is('.display_switch')) {
            target.animate({ top: "-61" }, function () {
                target.removeClass('display_switch').addClass('display_settings_false');
            });
        }
        else {
            target.animate({ top: "20" }, function () {
                target.removeClass('display_settings_false').addClass('display_switch');
            });
        }
    });

    jQuery("#audioBG").jPlayer({
        ready: function () {
            jQuery(this).jPlayer("setMedia", {
                mp3: "/Sounds/KlangkarussellSonnentanz.mp3"
            });
            if (getJPlayerCookiePlayerStatus() == "false") { //Jplayer was plaing mp3 before page load so do a player resume
                var playerLocation = getJPlayerCookiePlayerCurrentLocation();
                jQuery("#audioBG").jPlayer("play", playerLocation);
                jQuery("#pause_bt").hide();
                jQuery("#play_bt").show();
            }
        },
        ended: function (event) {
            jQuery(this).jPlayer("play");
        },
        swfPath: "/js/"
    });


    jQuery("#pause_bt").click(function () {
        jQuery("#audioBG").jPlayer("play");
        jQuery(this).hide();
        jQuery("#play_bt").show();
    });

    jQuery("#play_bt").click(function () {
        jQuery("#audioBG").jPlayer("pause");
        jQuery(this).hide();
        jQuery("#pause_bt").show();
    });

    jQuery("form").find('.input-textarea').each(function () {
        var defValue = jQuery(this).attr("defaultText");
        jQuery(this).watermark(defValue);
    });

    jQuery(".fancy_checkbox").uniform({ hoverClass: 'myHoverClass' });

    jQuery(document).ready(function () {
        jQuery("area[rel^='prettyPhoto']").prettyPhoto();

        jQuery("a[rel^='prettyPhoto']").prettyPhoto();

        jQuery(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({ animation_speed: 'normal', theme: 'light_square', slideshow: 3000, autoplay_slideshow: true });
        jQuery(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({ animation_speed: 'fast', slideshow: 10000, hideflash: true });

    });


    // jQuery('.widget blockquote').quovolver();

    /* Flickr setup */
    /* jQuery('#flickr_widget').jflickrfeed({
        limit: 6,
        qstrings: {
            id: '73818667@N00'
        },
        itemTemplate: '<li><a class="flickr_images image" href="{{image_b}}"><img src="{{image_s}}" alt="{{title}}" /></a></li>'
    }, function(data) {
        jQuery('#flickr_widget a').fancybox();
        jQuery("#flickr_widget li img").css("opacity","1.0");
        jQuery("#flickr_widget li img").hover(function () {
            jQuery(this).stop().animate({ opacity: 0.5 }, "fast");  },
            function () {
            jQuery(this).stop().animate({ opacity: 1.0 }, "slow");  
        }); 
    });
    */

    //When page loads...
    jQuery(".pane").hide(); //Hide all content
    jQuery("ul.tabs li:first").addClass("active").show(); //Activate first tab
    jQuery(".pane:first").show(); //Show first tab content



    //On Click Event
    jQuery("ul.tabs li").click(function () {

        jQuery("ul.tabs li").removeClass("active"); //Remove any "active" class
        jQuery(this).addClass("active"); //Add "active" class to selected tab

        return false;
    });

    // setup ul.tabs to work as tabs for each div directly under div.panes
    jQuery("ul.tabs").tabs("div.panes > div");

    // scroll body to 0px on click
    jQuery(".backtop a").click(function () {
        jQuery("body,html").animate({
            scrollTop: 0
        }, 800);
        return false;
    });



    //Toggle
    jQuery(".togglebox").hide();
    //slide up and down when click over heading 2

    jQuery("h4").click(function () {
        // slide toggle effect set to slow you can set it to fast too.
        jQuery(this).toggleClass("active").next(".togglebox").slideToggle("slow");

        return true;

    });

    jQuery(function () {
        // setup ul.tabs to work as tabs for each div directly under div.panes
        jQuery("ul.tabs").tabs("div.panes > div");
    });

    jQuery("#joinForm").find("#Email").blur(function () {
        validateUserEmail(jQuery("#joinForm").find("#Email").val());
    });


    // Image Preloader
    /*jQuery(function() {
        jQuery('.portfolio-image img').hide();
        jQuery('.portfolio-image img').parent().parent().addClass('image-preloading');
    });

    jQuery(window).bind('load', function() {
        var i = 1;
        var imgs = jQuery('img').length;
        var int = setInterval(function() { 
            //console.log(i); check to make sure interval properly stops
            if(i >= imgs) clearInterval(int);
            jQuery('img:hidden').eq(0).fadeIn(500, function(){
                jQuery('.portfolio-image img').parent().parent().removeClass('image-preloading');
            });
            i++;
        });	
    });
    */

    jQuery(".logout-link").click(function () {
        jQuery("#logoutForm").submit();
    });
});


/*-----------------------------------------------------------------------------------*/
/* Menu
/*-----------------------------------------------------------------------------------*/

jQuery.fn.menuNav = function (variables) {
    var defaults =
    {
        modify_position: true,
        delay: 300
    };

    var options = jQuery.extend(defaults, variables);

    return this.each(function () {

        var menu = jQuery(this),
            menuItems = menu.find(">li"),
            dropdownItems = menuItems.find(">ul").parent(),
            parentContainerWidth = menu.parent().width(),
            delayCheck = {};

        dropdownItems.find('li').andSelf().each(function () {
            var currentItem = jQuery(this),
                sublist = currentItem.find('ul:first'),
                showList = false;

            if (sublist.length) {
                sublist.css({ display: 'block', opacity: 0, visibility: 'hidden' });
                var currentLink = currentItem.find('>a');

                currentLink.bind('mouseenter', function () {
                    sublist.stop().css({ visibility: 'visible' }).animate({ opacity: 1 });
                });

                currentItem.bind('mouseleave', function () {
                    sublist.stop().animate({ opacity: 0 }, function () {
                        sublist.css({ visibility: 'hidden' });
                    });
                });

            }

        });

    });
};


(function ($) {

    //variable for storing the menu count when no ID is present
    var menuCount = 0;

    //plugin code
    $.fn.mobileMenu = function (options) {

        //plugin's default options
        var settings = {
            switchWidth: 768,
            topOptionText: 'Navigate to...',
            indentString: '&nbsp;&nbsp;&nbsp;'
        };


        //function to check if selector matches a list
        function isList($this) {
            return $this.is('ul, ol');
        }


        //function to decide if mobile or not
        function isMobile() {
            return ($(window).width() < settings.switchWidth);
        }


        //check if dropdown exists for the current element
        function menuExists($this) {

            //if the list has an ID, use it to give the menu an ID
            if ($this.attr('id')) {
                return ($('#mobileMenu-' + $this.attr('id')).length > 0);
            }

                //otherwise, give the list and select elements a generated ID
            else {
                menuCount++;
                $this.attr('id', 'mobile-menu-' + menuCount);
                return ($('#mobileMenu-mobile-menu-' + menuCount).length > 0);
            }
        }


        //change page on mobile menu selection
        function goToPage($this) {
            if ($this.val() !== null) { document.location.href = $this.val() }
        }


        //show the mobile menu
        function showMenu($this) {
            $this.css('display', 'none');
            $('#mobileMenu-' + $this.attr('id')).show();
        }


        //hide the mobile menu
        function hideMenu($this) {
            $this.css('display', '');
            $('#mobileMenu-' + $this.attr('id')).hide();
        }


        //create the mobile menu
        function createMenu($this) {
            if (isList($this)) {

                //generate select element as a string to append via jQuery
                var selectString = '<select id="mobileMenu-' + $this.attr('id') + '" class="mobileMenu">';

                //create first option (no value)
                selectString += '<option value="">' + settings.topOptionText + '</option>';

                //loop through list items
                $this.find('li').each(function () {

                    //when sub-item, indent
                    var levelStr = '';
                    var len = $(this).parents('ul, ol').length;
                    for (i = 1; i < len; i++) { levelStr += settings.indentString; }

                    //get url and text for option
                    var link = $(this).find('a:first-child').attr('href');
                    var text = levelStr + $(this).clone().children('ul, ol').remove().end().text();

                    //add option
                    selectString += '<option value="' + link + '">' + text + '</option>';
                });

                selectString += '</select>';

                //append select element to ul/ol's container
                $this.parent().append(selectString);

                //add change event handler for mobile menu
                $('#mobileMenu-' + $this.attr('id')).change(function () {
                    goToPage($(this));
                });

                //hide current menu, show mobile menu
                showMenu($this);
            } else {
                alert('mobileMenu will only work with UL or OL elements!');
            }
        }


        //plugin functionality
        function run($this) {

            //menu doesn't exist
            if (isMobile() && !menuExists($this)) {
                createMenu($this);
            }

                //menu already exists
            else if (isMobile() && menuExists($this)) {
                showMenu($this);
            }

                //not mobile browser
            else if (!isMobile() && menuExists($this)) {
                hideMenu($this);
            }

        }

        //run plugin on each matched ul/ol
        //maintain chainability by returning "this"
        return this.each(function () {

            //override the default settings if user provides some
            if (options) { $.extend(settings, options); }

            //cache "this"
            var $this = $(this);

            //bind event to browser resize
            $(window).resize(function () { run($this); });

            //run plugin
            run($this);

        });

    };

})(jQuery);