﻿var oth = oth || {};

oth.forgotPasswordViewModel = (function (oth, $) {
    var ViewModel = function () {
        var self = this;
        
        self.watermarkInputSelector = ".input-textarea";
        self.backWizardButtonSelector = "#btnBackToWizard";
        self.infoBoxSelector = "#info-box";
        self.forgotPasswordFormSelector = "#forgotpassword_form > form";

        self.redirectPageUrl = "/Booking/Welcome";

        self.resources = {
            info: ""
        }

        self.init = function (options) {
            $.extend(self, options);

            $("form").find(self.watermarkInputSelector).each(function () {
                var defValue = $(this).attr("defaultText");
                $(this).watermark(defValue);
            });

            $(self.backWizardButtonSelector).click(function () {
                window.location = self.redirectPageUrl;
                return false;
            });
        }
        
        self.onPasswordResetRequestComplete = function (data) {
            $(self.infoBoxSelector).html("");

            if (data.Valid) {
                self.showDialog(data.Message, data.RedirectionURL);
            }
            else {
                $(self.infoBoxSelector).html(data.Message);
                $(self.infoBoxSelector).show();
            }
        }

        self.showDialog = function (msg, url) {
            $('<div id="dialog" title="' + self.resources.info + '"><p>' + msg + '</p></div>').dialog({
                modal: true,
                resizable: false,
                buttons: {
                    Ok: function () {
                        jQuery(this).dialog("close");
                        window.location = url;
                    }
                }
            });
        }
    };

    return new ViewModel();

}(oth || {}, jQuery));