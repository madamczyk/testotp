﻿/// <reference path="../date.js" />



var dateTripStart = '';
var dateTripEnd = '';


function onLoginComplete(data) {
    if (data.Valid) {
        window.location = "/Booking/Contact";
    }
    else {
        jQuery("#blockLogin").html(data.Message);
    }
}

jQuery(document).ready(function () {
    if (jQuery().round_checboxes)
        jQuery(".round_checbox").round_checboxes();
    if (jQuery().round_radio)
        jQuery(".round_radio").round_radio();

    // set default invoice sections icons to open 
    jQuery("span.ui-icon-triangle-1-e").toggleClass("ui-icon-triangle-1-e").toggleClass("ui-icon-triangle-1-s");

    //remove calendars links    
    jQuery(this).find('#datepickerFrom').remove();
    jQuery("#nv-calendar-from").find("span").remove();
    jQuery(this).find('#datepickerTo').remove();
    jQuery("#nv-calendar-to").find("span").remove();
    jQuery("#dateFromLink").text(dateTripStart);
    jQuery("#dateToLink").text(dateTripEnd);

    jQuery("#credit-holeder-combo > ul > li").click(function () {
        jQuery(this).children("ul").toggle("slow");
        return false;
    });


    var date25yearsAgo = new Date().addYears(-25);

    jQuery("#BirthDayDisplay").DatePicker({
        current: date25yearsAgo,
        date: date25yearsAgo,
        onChange: function (dateSelected) {
            jQuery('#BirthDay').val(dateSelected.replace(/-/g, '/'));
            jQuery('#BirthDayDisplay').val(oth.common.getLocalizedDigitsDateString(dateSelected));
            jQuery('#BirthDayDisplay').prev("span").text("");
            jQuery('#BirthDayDisplay').DatePickerHide();

            jQuery('#BirthDayDisplay').focus();
        },
        calendars: 1,
        starts: 0,
        mode: 'single'
    });

    jQuery('#BirthDayDisplay').bind('change keydown', function (e) {
        var date = oth.common.parseDateString(jQuery(this).val());
        if (date != null)
            jQuery('#BirthDay').val(oth.common.getFormattedDateString(date, "yyyy-MM-dd"));
        else
            jQuery('#BirthDay').val(jQuery(this).val());

        if (e.keyCode == 13) {
            jQuery(this).DatePickerHide();
        }

        //jQuery('#BirthDayDisplay').keydown();
        //jQuery('#BirthDayDisplay').focus();
    });

    jQuery("#btnBackToHome").click(function () {
        window.location = "/Home";
        return false;
    });

    jQuery("div#credit-holeder-combo > ul > li > ul > li a").click(function () {
        var creditHolderId = jQuery(this).attr("data-credit-holder");
        jQuery("#CreditCardholderId").val(creditHolderId);
        jQuery("div#credit-holeder-combo > ul > li >div.input-textarea").html(jQuery(this).html());
        jQuery("div#credit-holeder-combo > ul > li").children("ul").hide("slow");
        return false;
    });

    jQuery("#trip-calendar").DatePicker({
        date: [dateTripStart, dateTripEnd],
        current: dateTripStart,
        calendars: 1,
        starts: 0,
        mode: 'range',
        flat: true,
        readOnly: true
    });

    toggleSections(jQuery(".credit_type_radio:checked").parent());

    jQuery(".credit_type_radio").parent().click(function () {
        toggleSections(jQuery(this));
    })

    jQuery("#TermsReadVerificaction").parent().click(function () {
        if (jQuery("#TermsReadVerificaction").prop("checked")) {
            jQuery("#user-initials-container").show();
            jQuery("#UserInitials").focus();
        } else {
            jQuery("#user-initials-container").hide();
        }
    });

    jQuery("div.collapse-section-trigger").click(function () {
        var sectionId = jQuery(this).attr("data-section");
        jQuery("#" + sectionId).toggle("slow");
        jQuery(this).find(".section-title-overall-value").toggle();

        var collapsibleSection = jQuery(this).find("span.ui-icon-triangle-1-e");
        if (collapsibleSection.size() != 0) {
            collapsibleSection.removeClass("ui-icon-triangle-1-e");
            collapsibleSection.addClass("ui-icon-triangle-1-s");
        } else {
            collapsibleSection = jQuery(this).find("span.ui-icon-triangle-1-s");
            collapsibleSection.removeClass("ui-icon-triangle-1-s");
            collapsibleSection.addClass("ui-icon-triangle-1-e");
        }
    });

    //jQuery("#btnConfirmPay").click(function () {
    //    validateAge();
    //    return false;
    //});
});

jQuery.validator.setDefaults({
    ignore: ".ignore"
})

function toggleSections(element) {
    if (element.find("input").val() == "New") {
        jQuery("#existing-creditCard-container").hide();
        jQuery("#new-creditCard-container").show();
        jQuery("#existing-creditCard-container").find("input, select").addClass("ignore");
        jQuery("#new-creditCard-container").find("input, select").removeClass("ignore");
    } else {
        jQuery("#new-creditCard-container").hide();
        jQuery("#existing-creditCard-container").show();
        jQuery("#existing-creditCard-container").find("input, select").removeClass("ignore");
        jQuery("#new-creditCard-container").find("input, select").addClass("ignore");
    }
}

function validateAge(value, element) {
    var guestBirthday = jQuery("#BirthdayCheck").val();
    var guestBirthDate = new Date(guestBirthday.replace(/-/g, '/'));
    guestBirthDate.addYears(25);
    if ((new Date()).compareTo(guestBirthDate) < 0) {
        window.location = "/Booking/AgeVerificationFail";

        return false;
    }
    return true;

}

function onJoinComplete(data) {
    if (data.Valid) {
        jQuery("div#joinBlockPopup").hide("slow");
        jQuery("div#joinBlockPopup").find("form").clearForm();
        window.location = "/Booking/Contact";
    }
    else {
        jQuery("#blockJoin").html(data.Message);
    }
}

jQuery(function () {

    var $sidebar = jQuery(".sidebar"),
        $window = jQuery(window),
        offset = $sidebar.offset(),
        topPadding = 5;

    $window.scroll(function () {
        if ($window.scrollTop() > offset.top) {
            $sidebar.stop().animate({
                marginTop: $window.scrollTop() - offset.top + topPadding
            }, 0);
        } else {
            $sidebar.stop().animate({
                marginTop: 0
            }, 0);
        }
    });

});