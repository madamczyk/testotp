﻿jQuery(document).ready(function () {
    jQuery(".editable").hover(mouseOverShowEdit, mouseExitsHideEdit);
    jQuery(".editable").mousedown(function (ev) {
        jQuery(this).hide();
        jQuery(".edit").show();
        ev.stopPropagation();
        //jQuery("#creditCardContainer").addClass("first");
    });

    jQuery('.edit').mousedown(function (ev){
        ev.stopPropagation();
    });

    jQuery('body').mousedown(function (ev){
        jQuery('.editable').show();
        jQuery(".edit").hide();
    });
});

function mouseOverShowEdit(event) {
    jQuery(".toolTipEdit").css("display","inline");
}

function mouseExitsHideEdit(ev) {
    jQuery(".toolTipEdit").hide();
}

function submitCulture() {
    jQuery(".language").submit();
}

function submitCreditCardDelete(ev) {
    jQuery(".credit").val(ev);
    jQuery(".deleteCreditCar").submit();
}



