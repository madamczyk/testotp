
jQuery(document).ready(function () {

    jQuery("form").find('input:not(.ignore-input)').each(function () {
        var defValue = jQuery(this).attr("defaultText");
        jQuery(this).watermark(defValue);
    });

    jQuery(".input-select").change(function () {
        if (jQuery(this).val() == '') {
            jQuery(this).css("color", "#999999");
        } else {
            jQuery(this).css("color", "#E1E1E1");
        }
    });
});