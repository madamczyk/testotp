﻿/// jQuery custom checboxes plugin for Oh the places
/// @Author BRaczek
/// usage $(selector).round_checboxes()

(function ($) {
    var methods = {
        init: function (options) {
            return this.each(function (options) {
                var settings = $.extend({
                    'checkedClass': 'check-state-checked',
                    'uncheckedClass': 'check-state-notchecked'
                }, options);

                var checbox = $(this);
                var spanElement = jQuery("<span />");

                if (checbox.prop("checked"))
                    jQuery(spanElement).addClass(settings.checkedClass);
                else
                    jQuery(spanElement).addClass(settings.uncheckedClass);
                checbox.css("visibility", "hidden");
                var parent = checbox.parent();
                checbox.remove();
                parent.prepend(spanElement);
                spanElement.append(checbox);
                spanElement.bind('click.round_checboxes', { settings: settings } , methods.toggle);
            });
        },
        destroy: function () {
            return this.each(function () {
                var checbox = $(this);
                var parentSpan = checbox.parent();
                parentSpan.unbind("round_checboxes");
            });
        },
        toggle: function (event) {
            var spanElement = $(this);
            var element = spanElement.find("input");
            var settings = event.data.settings;

            if (element.prop("checked")) {
                spanElement.removeClass(settings.checkedClass);
                spanElement.addClass(settings.uncheckedClass);
                element.prop("checked", false);
            } else {
                spanElement.removeClass(settings.uncheckedClass);
                spanElement.addClass(settings.checkedClass);
                element.prop("checked", true);
            }
        }
    }

    $.fn.round_checboxes = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.round_checboxes');
        }
    }
})(jQuery)