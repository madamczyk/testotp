﻿self.ArrivalCalendarInitialized = false;
self.DepartureCalendarInitialized = false;
self.PropertyCultureName = "";

var availabilityInfoValues = null;

jQuery(document).ready(function () {

	jQuery('.thumbnail_container').each(function (index, element) {
	    jQuery(this).cycle({
	        fx: 'fade',
	        timeout: 2000
	    }).cycle("pause");
	});

	jQuery(".portfolio_image").each(function (index, element) {
	    jQuery(this).hover(
        function () {
            jQuery(this).find(".thumbnail_container").cycle("resume");
        },
        function () {
            jQuery(this).find(".thumbnail_container").cycle("pause");
        }
        );
	});

	jQuery("#btnConfirmChange").click(function (e) {      
	    e.preventDefault();

	    var currentTravelTime = confirmDialogDescription;
	    currentTravelTime += oth.common.getLocalizedDateString(jQuery("#DateArrival").val()) + ' ' + oth.common.getLocalizedDateString(jQuery("#DateDeparture").val()) + '.';

	    jQuery('<div id="dialog" title="' + confirmDialogTitle + '"><p>' + currentTravelTime + '</p></div>').dialog({
	        modal: true,
	        buttons: {
	            Ok: function () {
	                jQuery(this).dialog("close");
	                jQuery("#frmChangeDate").submit();
	            },
	            Cancel: function () {
	                jQuery(this).dialog("close");
	            }
	        }
	    });

	    return false;
	});

	
	//initArrivalCalendar();
	//initDepartureCalendar();

	function initArrivalCalendar() {
	    if (self.ArrivalCalendarInitialized == false) {
	        //init calendar 
	        dates = [dateTripStart, dateTripEnd];
	        dateStart = dateTripStart;

	        var Start = new Date(dateTripStart.replace(/-/g, '/'));
	        var Stop = new Date(dateTripEnd.replace(/-/g, '/'));

	        jQuery('#tripCalendarArrival').DatePickerChangeDate({
	            flat: true,
	            date: dates,
	            current: dateStart,
	            calendars: 1,
	            mode: 'range',
	            legend: true,
	            starts: 0,
	            currentCalendar: "ChangeArrival",
	            cultureName: propertyCultureName,
	            onRender: function (date) {
	                var priceDay = 0;
	                var disabledElem = false;
	                var specialClass = '';
	                var mlosDay = 0;
	                var cta = 0;

	                jQuery(availabilityInfoValues).each(function (index, element) {
	                    var jsonDate = new Date(element.Date.replace(/-/g, '/'));
	                    jsonDate.setHours(0, 0, 0, 0);


	                    if (jsonDate.valueOf() == date.valueOf()) {
	                        priceDay = element.Price;
	                        if (jsonDate.valueOf() < Start.valueOf() || Stop.valueOf() < jsonDate.valueOf()) {
	                            disabledElem = element.IsBlocked;
	                        } else
	                            disabledElem = false;
	                        mlosDay = element.Mlos;
	                        cta = element.Cta;
	                    }

	                });
	                return {
	                    disabled: disabledElem,
	                    className: disabledElem ? 'datepickerSpecial' : '',
	                    price: priceDay,
	                    mlos: mlosDay ? mlosDay : "",
	                    cta: cta
	                }
	            },
	            startDate: Start.valueOf(),
	            endDate: Stop.valueOf()

	        });
	        self.availabilityCalendarInitialized = true;
	    }
	    return false;
	}


	function initDepartureCalendar() {
	    if (self.DepartureCalendarInitialized == false) {
	        //init calendar 
	        dates = [dateTripStart, dateTripEnd];
	        dateStart = dateTripStart;

	        var Start = new Date(dateTripStart.replace(/-/g, '/'));
	        var Stop = new Date(dateTripEnd.replace(/-/g, '/'));


	        jQuery('#tripCalendarDeparture').DatePickerChangeDate({
	            flat: true,
	            date: dates,
	            current: dateTripEnd,
	            calendars: 1,
	            mode: 'range',
                legend: false,
	            starts: 0,
	            currentCalendar: "ChangeDeparture",
	            cultureName: propertyCultureName,
	            onRender: function (date) {
	                var priceDay = 0;
	                var disabledElem = false;
	                var specialClass = '';
	                var mlosDay = 0;
	                var cta = 0;

	                jQuery(availabilityInfoValues).each(function (index, element) {
	                    var jsonDate = new Date(element.Date.replace(/-/g, '/'));
	                    if (jsonDate.valueOf() == date.valueOf()) {
	                        priceDay = element.Price;
	                        if (jsonDate.valueOf() < Start.valueOf() || Stop.valueOf() < jsonDate.valueOf()) {
	                            disabledElem = element.IsBlocked;
	                        } else
	                            disabledElem = false;
	                        mlosDay = element.Mlos;
	                    }

	                });
	                return {
	                    disabled: disabledElem,
	                    className: disabledElem ? 'datepickerSpecial' : '',
	                    price: priceDay,
	                    mlos: mlosDay ? mlosDay : "",
	                    cta: cta
	                }
	            },
	            startDate: Start.valueOf(),
	            endDate: Stop.valueOf()

	        });
	        self.DepartureCalendarInitialized = true;
	        getInvoiceData();
	    }
	    return false;
	}
	self.getInvoiceData = function () {
	    jQuery("#widget-invoice").hide();

	    if (dateFrom === dateTo) {
	        return false;
	    }

	    var propertyId = jQuery("#Property_PropertyID").val();
	    var unitId = jQuery("#Unit_UnitID").val();
	    var totalPrice = jQuery("#reservation_TotalPrice").val();
	    //var destinationId = jQuery("#Property_Destination_DestinationID").val();

	    jQuery.ajax({
	        url: "/Guest/GetPriceAndInvoiceDetails?unitId=" + unitId + "&propertyId=" + propertyId + "&dateFrom=" + dateFrom + "&dateUntil=" + dateTo + "&totalPrice=" + totalPrice,
	        success: function (data, textStatus, jqXHR) {
	            jQuery("#widget-invoice").show();
	            jQuery('#widget-invoice').html(data);
	        }
	    });
        
	}



	self.getDataValuesForCalendar = function () {
	    var unitId = jQuery("#Unit_UnitID").val();
	    //var selectedDates = jQuery('#availabilityDate').DatePickerGetDate(true);    
	    //TODO Zmienic z statycznej daty na jakis current
	    //przydaloby sie pomysliec o keszowaniu
	    jQuery.getJSON("/PropertyDetail/GetPropertyAvailabilityInfoForMonth", {
	        id: jQuery("#Unit_UnitID").val(),
	        fromDate: self.startingDateForDatePicker != null ? self.startingDateForDatePicker : "2012-11-04",
	        untilDate: (self.endingDateForDatePicker != null) ? self.endingDateForDatePicker : "2013-12-04"
	    }, function (data) {
	        availabilityInfoValues = data;
	        if (self.DepartureCalendarInitialized)
	            jQuery('.datepickerBorderT').click();
	        else {
	            initArrivalCalendar();
	            initDepartureCalendar();
	        }
	    });
	}

	getDataValuesForCalendar();


});
	