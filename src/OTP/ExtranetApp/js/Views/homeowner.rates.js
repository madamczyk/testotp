﻿var oth = oth || {};

oth.ratesViewModel = (function (oth, $) {
    var ViewModel = function () {
        var self = this;

        //structures imitiating enum
        self.unitBlockingTypes = {
            available: 0,
            reservation: 1,
            ownerStay: 2,
            parseString: function (value) {
                switch (value) {
                    case "available":
                        return 0;
                    case "reservation":
                        return 1;
                    case "ownerStay":
                        return 2;
                }
            },
            toString: function (intValue) {
                switch (value) {
                    case 0:
                        return "available";
                    case 1:
                        return "reservation";
                    case 2:
                        return "ownerStay";
                }
            }
        };

        self.weekDays = {
            sunday: 0,
            monday: 1,
            tuesday: 2,
            wednesday: 3,
            thursday: 4,
            friday: 5,
            saturday: 6,
            parseString: function (value) {
                switch (value) {
                    case "sunday":
                        return 0;
                    case "monday":
                        return 1;
                    case "tuesday":
                        return 2;
                    case "wednesday":
                        return 3;
                    case "thursday":
                        return 4;
                    case "friday":
                        return 5;
                    case "saturday":
                        return 6;
                }
            },
            toString: function (intValue) {
                switch (intValue) {
                    case 0:
                        return "sunday";
                    case 1:
                        return "monday";
                    case 2:
                        return "tuesday";
                    case 3:
                        return "wednesday";
                    case 4:
                        return "thursday";
                    case 5:
                        return "friday";
                    case 6:
                        return "saturday";
                }
            }
        }

        self.unitId = null;

        //selectors
        self.ratesCalendarSelector = "#reservation-calendar";
        self.paintingTypeLinksSelector = ".button";
        self.ratesModalSelector = ".rates-modal-form";
        self.mlosModalSelector = ".mlos-modal-form";
        self.ctaModalSelector = ".cta-modal-form";
        self.ownerStayModalSelector = ".owner-stay-form";

        //csses
        self.calendarDayCssClassPrefix = "calendar-";
        self.selectedCalendarDayCssClassPrefix = "selected-";

        //controller actions
        self.getUnitBlockingsAction = "";
        self.getRatesAction = "";
        self.getMlosAction = "";
        self.addUnitBlockadeAction = "";
        self.getCtaAction = "";
        self.changeRatesAction = "";
        self.changeMlosesAction = "";
        self.changeCtasAction = "";

        //------------------------------------
        //  calendar related objects
        //------------------------------------
        self.numberOfCalendars = 3;
        self.selectedUnitBlockingType = null;
        self.selectedUnitBlockingCssClass = null;
        self.availabilityCalendarInitialized = false;
        self.unitInvBlockings = [];
        self.rates = [];
        self.mloses = [];
        self.ctas = [];
        self.propertyCultureName = null;

        //range of dates from which the data were loaded
        self.lastLoadedDataRange = {
            dateFrom: "",
            daterUntil: "",
        }

        //variables which are used for keeping state between rendering days on the calendar
        self.currentUnitInvBlockingIndex = null;
        self.currentRateIndex = null;
        self.currentMlosIndex = null;
        self.currentCtaIndex = null;

        //modal forms
        self.ratesModalForm = null;
        self.mlosesModalForm = null;
        self.ctaModalForm = null;
        self.ownerStayModalFormEditObject = null;


        //localized resources
        self.resources = {
            grossDue: "",
            transactionFee: "",
            ownerNet: "",
            confirmation: "",
            nightsText: "",
            mlosText: "",
            pernightText: "",
            availableText: "",
            unavailableText: "",
            inPastText: "",
            available: "",
            ownerStay: "",
            reservation: "",
            ctaText: "",
            noArrivalsThisDay: "",
            confirmationText: ""
        };

        self.init = function (options) {
            $.extend(self, options);

            //initializing modal forms
            _initRateModalForm();
            _initMlosModalForm();
            _initCtaModalForm();
            _initOwnerStayModalForm();

            //load initial data
            _checkAndLoadData();

            $(self.paintingTypeLinksSelector).click(function (event) {
                var unitBlockingType = $(this).attr("selection-type");
                if (unitBlockingType == undefined)
                    return false;

                if ($(this).hasClass("selected")) {
                    self.selectedUnitBlockingType = null;
                    self.selectedUnitBlockingCssClass = null;

                    $(self.paintingTypeLinksSelector).removeClass("selected");
                    $(document).click();
                }
                else {
                    self.selectedUnitBlockingType = self.unitBlockingTypes.parseString(unitBlockingType);
                    self.selectedUnitBlockingCssClass = self.selectedCalendarDayCssClassPrefix + unitBlockingType;

                    $(self.paintingTypeLinksSelector).removeClass("selected");
                    $(this).addClass("selected");
                }
            });
        }

        self.getCssClassForUnitBlockingType = function (unitBlockingType, cssClassPrefix) {
            switch (unitBlockingType) {
                case 1:
                    return cssClassPrefix + "reservation";
                case 2:
                    return cssClassPrefix + "ownerStay";
                default:
                    return cssClassPrefix + "available";
            }
        }

        self.getCalendarData = function (actionToCall, doneCallback, dateFrom, dateUntil) {
            var d = $.Deferred();

            $.getJSON(actionToCall, {
                id: self.unitId,
                fromDate: $.datepicker.formatDate("yy-mm-dd", dateFrom),
                untilDate: $.datepicker.formatDate("yy-mm-dd", dateUntil),
            }, function (data) {

                $.each(data, function (index, element) {
                    element.DateFrom = oth.common.parseDateString(element.DateFrom);
                    element.DateUntil = oth.common.parseDateString(element.DateUntil);
                });

                doneCallback(data);
                d.resolve(data);
            }).error(function () {
                d.reject();
            });

            return d.promise();
        }

        //private functions
        function _initOwnerStayModalForm() {
           
            var buttons = {}
            buttons[self.resources.applyButton] = function () {
                self.ownerStayModalFormEditObject.Comment = $(this).find("textarea[name=txtOwnerComment]").val();
                _addUnitBlockage(self.ownerStayModalFormEditObject);
                $(this).dialog("close");
            };            

            $(self.ownerStayModalSelector).dialog({
                autoOpen: false,
                height: 320,
                width: 450,
                modal: true,
                open: function () {
                    $(this).find("textarea[name=txtOwnerComment]").val("");
                    $(this).closest(".ui-dialog").find(".ui-dialog-titlebar:first").hide();
                },
                buttons:  buttons,                   
                zIndex: 10000,
                stack: false
            });
        }

        function _addUnitBlockage(model) {
            $.ajax({
                type: "POST",
                url: self.addUnitBlockadeAction,
                data: model,
            }).done(function (response) {
                self.getCalendarData(self.getUnitBlockingsAction, function (data) { self.unitInvBlockings = data },
                                     self.lastLoadedDataRange.dateFrom, self.lastLoadedDataRange.daterUntil)
                    .done(function () {
                        $(document).click();
                    });
            });
        }

        function _initRateModalForm() {

            var ratesModalOptions = {
                unitId: self.unitId,
                getObjectsAction: self.getRatesAction,
                changeObjectsAction: self.changeRatesAction,
                resources: self.resources,
                height: 250,
                width: 700,
                loadDataFromTemplate: false,
                onCalendarDataRefresh: function () {
                    _checkAndLoadData(self.lastLoadedDataRange.dateFrom, self.lastLoadedDataRange.daterUntil, true);
                },
                onFillTemplateRow: function (dataObject, template) {
                    if (dataObject == null) {
                        dataObject = {
                            UnitRateID: "",
                            DailyRate: "",
                            DateFrom: "",
                            DateUntil: ""
                        }
                    }

                    if (template != null) {
                        template = template.replace("{objectId}", self.unitId);
                    }

                    return template;
                },
                onGetRowElement: function (rowElement) {
                    var unitRateId = $(rowElement).attr("object-id");
                    var dateFrom = oth.common.parseDateString($(rowElement).find(".date-from").val());
                    var dateUntil = oth.common.parseDateString($(rowElement).find(".date-until").val());
                    var rate = parseFloat($(rowElement).find(".rate").val());

                    var monday = $(rowElement).find(".monday").is(':checked');
                    var tuesday = $(rowElement).find(".tuesday").is(':checked');
                    var wednesday = $(rowElement).find(".wednesday").is(':checked');
                    var thursday = $(rowElement).find(".thursday").is(':checked');
                    var friday = $(rowElement).find(".friday").is(':checked');
                    var saturday = $(rowElement).find(".saturday").is(':checked');
                    var sunday = $(rowElement).find(".sunday").is(':checked');

                    var item = {
                        UnitRateID: unitRateId,
                        DateFrom: $.datepicker.formatDate("yy-mm-dd", dateFrom),
                        DateUntil: $.datepicker.formatDate("yy-mm-dd", dateUntil),
                        DailyRate: rate,
                        Monday: monday,
                        Tuesday: tuesday,
                        Wednesday: wednesday,
                        Thursday: thursday,
                        Friday: friday,
                        Saturday: saturday,
                        Sunday: sunday
                    };

                    return item;
                }
            };

            self.ratesModalForm = new ModalForm(self.ratesModalSelector, ".edit-rates");
            self.ratesModalForm.init(ratesModalOptions);
        }

        function _initMlosModalForm() {
            var mlosesModalOptions = {
                unitId: self.unitId,
                getObjectsAction: self.getMlosAction,
                changeObjectsAction: self.changeMlosesAction,
                resources: self.resources,
                onCalendarDataRefresh: function () {
                    _checkAndLoadData(self.lastLoadedDataRange.dateFrom, self.lastLoadedDataRange.daterUntil, true);
                },
                onFillTemplateRow: function (dataObject, template) {
                    if (dataObject == null) {
                        dataObject = {
                            UnitTypeMLOSID: "",
                            MLOS: "",
                            DateFrom: "",
                            DateUntil: ""
                        }
                    }

                    template = template.replace("{objectId}", dataObject.UnitTypeMLOSID);
                    template = template.replace("{mlos}", dataObject.MLOS);
                    template = template.replace("{dateFrom}", dataObject.DateFrom !== "" ? oth.common.getLocalizedDateString(dataObject.DateFrom) : "");
                    template = template.replace("{dateUntil}", dataObject.DateFrom !== "" ? oth.common.getLocalizedDateString(dataObject.DateUntil) : "");

                    return template;
                },
                onGetRowElement: function (rowElement) {
                    var unitMlosId = $(rowElement).attr("object-id");
                    var dateFrom = oth.common.parseDateString($(rowElement).find(".date-from").val());
                    var dateUntil = oth.common.parseDateString($(rowElement).find(".date-until").val());
                    var mlos = parseFloat($(rowElement).find(".mlos").val());

                    var item = {
                        UnitTypeMLOSID: unitMlosId,
                        DateFrom: $.datepicker.formatDate("yy-mm-dd", dateFrom),
                        DateUntil: $.datepicker.formatDate("yy-mm-dd", dateUntil),
                        MLOS: mlos
                    };

                    return item;
                }
            };

            self.mlosesModalForm = new ModalForm(self.mlosModalSelector, ".edit-mloses");
            self.mlosesModalForm.init(mlosesModalOptions);
        }

        function _initCtaModalForm() {
            var ctaModalOptions = {
                unitId: self.unitId,
                getObjectsAction: self.getCtaAction,
                changeObjectsAction: self.changeCtasAction,
                resources: self.resources,
                onCalendarDataRefresh: function () {
                    _checkAndLoadData(self.lastLoadedDataRange.dateFrom, self.lastLoadedDataRange.daterUntil, true);
                },
                onFillTemplateRow: function (dataObject, template) {
                    if (dataObject == null) {
                        dataObject = {
                            UnitTypeCTAID: "",
                            DateFrom: "",
                            DateUntil: "",
                            Monday: false,
                            Tuesday: false,
                            Wednesday: false,
                            Thursday: false,
                            Friday: false,
                            Saturday: false,
                            Sunday: false,
                        }
                    }

                    template = template.replace("{objectId}", dataObject.UnitTypeCTAID);
                    template = template.replace("{dateFrom}", dataObject.DateFrom !== "" ? oth.common.getLocalizedDateString(dataObject.DateFrom) : "");
                    template = template.replace("{dateUntil}", dataObject.DateFrom !== "" ? oth.common.getLocalizedDateString(dataObject.DateUntil) : "");

                    template = template.replace("{monChecked}", dataObject.Monday == true ? "checked" : "");
                    template = template.replace("{tueChecked}", dataObject.Tuesday == true ? "checked" : "");
                    template = template.replace("{wedChecked}", dataObject.Wednesday == true ? "checked" : "");
                    template = template.replace("{thuChecked}", dataObject.Thursday == true ? "checked" : "");
                    template = template.replace("{friChecked}", dataObject.Friday == true ? "checked" : "");
                    template = template.replace("{satChecked}", dataObject.Saturday == true ? "checked" : "");
                    template = template.replace("{sunChecked}", dataObject.Sunday == true ? "checked" : "");

                    return template;
                },
                onGetRowElement: function (rowElement) {
                    var unitCtaId = $(rowElement).attr("object-id");
                    var dateFrom = oth.common.parseDateString($(rowElement).find(".date-from").val());
                    var dateUntil = oth.common.parseDateString($(rowElement).find(".date-until").val());
                    var monday = $(rowElement).find(".monday").is(':checked');
                    var tuesday = $(rowElement).find(".tuesday").is(':checked');
                    var wednesday = $(rowElement).find(".wednesday").is(':checked');
                    var thursday = $(rowElement).find(".thursday").is(':checked');
                    var friday = $(rowElement).find(".friday").is(':checked');
                    var saturday = $(rowElement).find(".saturday").is(':checked');
                    var sunday = $(rowElement).find(".sunday").is(':checked');

                    var item = {
                        UnitTypeCTAID: unitCtaId,
                        DateFrom: $.datepicker.formatDate("yy-mm-dd", dateFrom),
                        DateUntil: $.datepicker.formatDate("yy-mm-dd", dateUntil),
                        Monday: monday,
                        Tuesday: tuesday,
                        Wednesday: wednesday,
                        Thursday: thursday,
                        Friday: friday,
                        Saturday: saturday,
                        Sunday: sunday
                    };

                    return item;
                }
            };

            self.ctaModalForm = new ModalForm(self.ctaModalSelector, ".edit-cta");
            self.ctaModalForm.init(ctaModalOptions);
        }


        function _initRatesCalendar() {
            if (self.availabilityCalendarInitialized == false) {
                $(self.ratesCalendarSelector).DatePickerAvailability({
                    flat: true,
                    date: [],
                    current: $.datepicker.formatDate("yy-mm-dd", new Date()),
                    calendars: self.numberOfCalendars,
                    mode: 'range',
                    starts: 0,
                    onBeforeFill: _onBeforeFill,
                    onRender: _onRenderCalendarDay,
                    onRangeSelected: _onRangeSelected,
                    getSelectionCssClass: function () {
                        return self.selectedUnitBlockingCssClass;
                    },
                    checkAndLoadData: _checkAndLoadData,
                    canSelect: _canSelect
                });
                self.availabilityCalendarInitialized = true;
            }
            return false;
        }

        function _onBeforeFill() {
            //before repainting calendar, we need to reset global indexes
            self.currentUnitInvBlockingIndex = null;
            self.currentRateIndex = null;
            self.currentMlosIndex = null;
            self.currentCtaIndex = null;
        }

        function _onRenderCalendarDay(date) {
            var calendarObject = {
                date: date,
                unitBlockingType: self.unitBlockingTypes.available,
                disabled: false,
                className: self.getCssClassForUnitBlockingType(self.unitBlockingTypes.available, self.calendarDayCssClassPrefix),
                price: 0,
                hasCta: false,
                tooltipContent: ""
            }

            //checking and applying unit blockings
            _renderUnitBlockings(calendarObject);
            //checking and applying rates
            _renderRates(calendarObject);
            //checking and applying mloses
            _renderMloses(calendarObject);
            //checking and applying ctas
            _renderCtas(calendarObject);
            //rendering tooltip
            _renderTooltip(calendarObject);


            if (calendarObject.mlosDay > 0 && calendarObject.hasCta)
                calendarObject.className += " calendar-mlosCta";
            else if (calendarObject.mlosDay > 0 && !calendarObject.hasCta)
                calendarObject.className += " calendar-mlos";
            else if (calendarObject.hasCta)
                calendarObject.className += " calendar-cta";

            return calendarObject;
        }

        function _renderUnitBlockings(calendarObject) {
            if (self.currentUnitInvBlockingIndex == null ||
                self.unitInvBlockings[self.currentUnitInvBlockingIndex].DateFrom > calendarObject.date ||
                self.unitInvBlockings[self.currentUnitInvBlockingIndex].DateUntil < calendarObject.date)
                self.currentUnitInvBlockingIndex = _findFirstElementByDate(self.unitInvBlockings, calendarObject.date);

            if (self.currentUnitInvBlockingIndex != null) {
                var currentUnitInvBlocking = self.unitInvBlockings[self.currentUnitInvBlockingIndex];
                calendarObject.unitBlockingType = currentUnitInvBlocking.Type;
                calendarObject.disabled = true;
                calendarObject.className = self.getCssClassForUnitBlockingType(calendarObject.unitBlockingType, self.calendarDayCssClassPrefix);
                calendarObject.guestName = currentUnitInvBlocking.GuestName;
                calendarObject.comment = currentUnitInvBlocking.OwnerComment;
                calendarObject.confirmation = currentUnitInvBlocking.Confirmation;


            }
        }

        function _renderRates(calendarObject) {
            if (self.currentRateIndex == null ||
                self.rates[self.currentRateIndex].DateFrom > calendarObject.date ||
                self.rates[self.currentRateIndex].DateUntil < calendarObject.date)
                self.currentRateIndex = _findFirstElementByDate(self.rates, calendarObject.date);

            if (self.currentRateIndex != null) {
                var currentRate = self.rates[self.currentRateIndex];
                calendarObject.price = currentRate.DailyRate;
            }
        }

        function _renderMloses(calendarObject) {
            if (self.currentMlosIndex == null ||
                self.mloses[self.currentMlosIndex].DateFrom > calendarObject.date ||
                self.mloses[self.currentMlosIndex].DateUntil < calendarObject.date)
                self.currentMlosIndex = _findFirstElementByDate(self.mloses, calendarObject.date);

            if (self.currentMlosIndex != null) {
                var currentMlos = self.mloses[self.currentMlosIndex];
                calendarObject.mlosDay = currentMlos.MLOS;
            }
        }

        function _renderCtas(calendarObject) {
            if (self.currentCtaIndex == null ||
                self.ctas[self.currentCtaIndex].DateFrom > calendarObject.date ||
                self.ctas[self.currentCtaIndex].DateUntil < calendarObject.date)
                self.currentCtaIndex = _findFirstElementByDate(self.ctas, calendarObject.date);

            if (self.currentCtaIndex != null) {
                var currentCta = self.ctas[self.currentCtaIndex];
                if (hasDayOfWeekCta(calendarObject.date, currentCta))
                    calendarObject.hasCta = true;
            }
        }

        function hasDayOfWeekCta(date, cta) {
            var dayOfWeek = self.weekDays.toString(date.getDay())
            dayOfWeek = dayOfWeek.slice(0, 1).toUpperCase() + dayOfWeek.slice(1);

            if (!cta[dayOfWeek])
                return true;

            return false;
        }

        function _renderTooltip(calendarObject) {
            var currentDate = new Date();
            currentDate.setHours(0, 0, 0, 0);

            //status
            var status = self.resources.available;
            //guestName
            var guestFullName = '';
            //owner stay comment
            var ownerComment = '';
            //reservation confirmation
            var confirmation = '';

            if (calendarObject.date < currentDate.valueOf())
                status = self.resources.inPastText;
            else {
                switch (calendarObject.unitBlockingType) {
                    case self.unitBlockingTypes.reservation:
                        status = self.resources.reservation;
                        guestFullName = calendarObject.guestName;
                        confirmation = calendarObject.confirmation;
                        break;
                    case self.unitBlockingTypes.ownerStay:
                        status = self.resources.ownerStay;
                        ownerComment = calendarObject.comment;
                        break;
                }
            }

            Globalize.culture(self.propertyCultureName); 
            var formattedCurrency = Globalize.format(calendarObject.price, "c");

            calendarObject.tooltipContent = '<div class="popup-reservationDetails toolTipDatePicker standart-popup">' +
                '<div class="popup-header">' +
                    '<div>' + "Rate & Availability" + '</div>' +
                    '<div style="clear:both"></div>' +
                '</div>' +
                (guestFullName != '' ? '<div class="popup-information">' + guestFullName + '</div>' : '') +
                (confirmation != '' ? '<div class="popup-information">' + self.resources.confirmationText + " " + confirmation + '</div>' : '') +
                '<div class="popup-information">' + formattedCurrency + " " + self.resources.pernightText + '</div>' +
                '<div class="popup-information">' + status + '</div>' +
                (calendarObject.hasCta > 0 ? '<div class="popup-information">' + self.resources.noArrivalsThisDay + '</div>' : '') +
                (calendarObject.mlosDay > 0 ? '<div class="popup-information">' + calendarObject.mlosDay + ' ' + 'night Minimum Stay' + '</div>' : '') +
                (ownerComment != '' ? '<div class="popup-information">' + ownerComment + '</div>' : '') +
            '</div>';
        }

        function _findFirstElementByDate(arrayOfElements, date) {
            var date = oth.common.parseDateString(date).setHours(0, 0, 0, 0);
            var elementIndex = null;

            $.each(arrayOfElements, function (index, element) {
                if (date >= element.DateFrom.setHours(0, 0, 0, 0) &&
                    date <= element.DateUntil.setHours(0, 0, 0, 0)) {
                    elementIndex = index;
                    return false;
                }
            });

            return elementIndex;
        }

        function _canSelect(dateFrom, dateUntil) {
            dateFrom = oth.common.parseDateString(dateFrom);
            dateUntil = oth.common.parseDateString(dateUntil);
            if (dateFrom == null || dateUntil == null)
                return false;

            var canSelect = true;

            dateFrom.setHours(0, 0, 0, 0);
            dateUntil.setHours(0, 0, 0, 0);

            if (self.selectedUnitBlockingType != self.unitBlockingTypes.available) {
                $.each(self.unitInvBlockings, function (index, element) {
                    if (dateFrom <= element.DateUntil.setHours(0, 0, 0, 0) &&
                        dateUntil >= element.DateFrom.setHours(0, 0, 0, 0)) {
                        canSelect = false;
                        return false;
                    }
                });

                if (!canSelect)
                    return canSelect;
            }

            $.each(self.ctas, function (index, element) {
                if (dateFrom <= element.DateUntil.setHours(0, 0, 0, 0) &&
                    dateUntil >= element.DateFrom.setHours(0, 0, 0, 0)) {
                    var overlappedDateRange = {
                        dateFrom: new Date(Math.max(element.DateFrom, dateFrom)),
                        dateUntil: new Date(Math.min(element.DateUntil, dateUntil)),
                    }

                    var currentDate = overlappedDateRange.dateFrom;
                    while (true) {
                        if (hasDayOfWeekCta(currentDate, element)) {
                            canSelect = false;
                            return false;
                        }

                        currentDate.addDays(1);
                        if (currentDate > overlappedDateRange.dateUntil)
                            break;
                    }
                }
            });

            return canSelect;
        }

        function _onRangeSelected(dateFrom, dateUntil) {
            var model = {
                UnitId: self.unitId,
                DateFrom: dateFrom,
                DateTo: dateUntil,
                UnitBlockingType: self.selectedUnitBlockingType,
                Comment: ""
            };

            if (self.selectedUnitBlockingType == self.unitBlockingTypes.available) {
                _addUnitBlockage(model);
            } else {
                self.ownerStayModalFormEditObject = model;
                $(self.ownerStayModalSelector).dialog("open");
            }
        }

        function _checkAndLoadData(dateFrom, dateUntil, forceLoad) {
            if (dateFrom != undefined && dateUntil != undefined) {
                dateFrom.addMonths(-1);
                dateUntil.addMonths(1);

                //checking wether we need to download data
                if (dateFrom >= self.lastLoadedDataRange.dateFrom && dateUntil <= self.lastLoadedDataRange.daterUntil && !forceLoad)
                    return false;

                if (dateFrom.getFullYear() === dateUntil.getFullYear())
                    dateFrom.addYears(-1);
                dateFrom.setMonth(9);

                dateUntil.setMonth(11);
                dateUntil.setDate(31);
                dateUntil.addMonths(2);
            }
            else {
                var currentDate = new Date();
                dateFrom = new Date(currentDate.getFullYear() - 1, 9, 1);
                dateUntil = new Date(currentDate.getFullYear(), 12, 31);
            }

            $.when(self.getCalendarData(self.getUnitBlockingsAction, function (data) { self.unitInvBlockings = data }, dateFrom, dateUntil),
                   self.getCalendarData(self.getRatesAction, function (data) { self.rates = data }, dateFrom, dateUntil),
                   self.getCalendarData(self.getMlosAction, function (data) { self.mloses = data }, dateFrom, dateUntil),
                   self.getCalendarData(self.getCtaAction, function (data) { self.ctas = data }, dateFrom, dateUntil))
                .done(function () {
                    self.lastLoadedDataRange.dateFrom = dateFrom;
                    self.lastLoadedDataRange.daterUntil = dateUntil;

                    self.currentUnitInvBlockingIndex = null;
                    self.currentRateIndex = null;
                    self.currentMlosIndex = null;

                    if (self.availabilityCalendarInitialized) {
                        $('.datepickerBorderT').click();
                    } else
                        _initRatesCalendar();
                });

            return true;
        }
    };

    var ModalForm = function (dialogSelector, openLinkSelector) {
        var self = this;

        //selectors
        self.dialogSelector = dialogSelector;
        self.openLinkSelector = openLinkSelector;

        self.yearControl = self.dialogSelector + " .year-control";
        self.addPeriodButtonSelector = self.dialogSelector + " .add-period";
        self.rowTemplateSelector = self.dialogSelector + " script";
        self.dataTableTbodySelector = self.dialogSelector + " .objects > table > tbody";
        self.lastAddedRowSelector = self.dataTableTbodySelector + " > tr:last";
        self.lastAddedRowDateFieldSelector = self.lastAddedRowSelector + " > td.date";
        self.lastAddedRowDeleteFieldSelector = self.lastAddedRowSelector + " > td.delete > span";
        self.errorContainerSelector = " .ui-dialog-buttonpane";
        self.errorBoxSelector = self.errorContainerSelector + " .error-container";
        self.dialogButtonContainerSelector = ".ui-dialog-buttonset";
        self.height = 400;
        self.width = 627;
        self.loadDataFromTemplate = true;

        //actions
        self.getObjectsAction = "";
        self.changeObjectsAction = "";

        self.unitId = null;
        self.objects = [];
        self.ratesToDelete = [];

        self.isOpen = false;

        //event handlers
        self.onFillTemplateRow = function (dataObject, template) { return ""; }
        self.onCalendarDataRefresh = function () { };
        self.onGetRowElement = function (rowElement) { };

        //localized resources
        self.resources = {
            applyButton: "",
            cancelButton: "",
            fieldsAreInvalid: "",
            ratesCannotOverlap: "",
        };

        self.init = function (options) {
            $.extend(self, options);

            var buttons = {};
            buttons[self.resources.applyButton] = _applyChanges;
            buttons[self.resources.cancelButton] = _closeWindow;

            $(self.dialogSelector).dialog({
                autoOpen: false,
                height: self.height,
                width: self.width,
                modal: true,
                open: function () {
                    $(this).closest(".ui-dialog").find(".ui-dialog-titlebar:first").hide();
                },
                buttons: buttons,
                zIndex: 10000,
                stack: false,
                close: function () {
                    self.isOpen = false;
                }
            });

            $(self.openLinkSelector).click(function (event) {
                event.preventDefault();
                if (!self.isOpen) {
                    self.isOpen = true;
                    if (self.loadDataFromTemplate) {
                        _changeYear(new Date().getFullYear());
                    }
                    else {
                        $(self.dialogSelector).dialog("open");
                        _clearNonTemplateDialog();
                        _initDatePickers();
                    }
                }
            });

            $(self.yearControl).change(function (event) {
                event.preventDefault();
                _changeYear($(this).val());
            });

            $(self.addPeriodButtonSelector)
                .button()
                .click(function () {
                    _addRow(null);
                    $(self.addPeriodButtonSelector).focus();
                });
        }

        self.getData = function (year) {
            var d = $.Deferred();

            var dateFrom = new Date(year, 0, 1, 0, 0, 0, 0);
            var dateUntil = new Date(year, 11, 31, 0, 0, 0, 0);

            $.getJSON(self.getObjectsAction, {
                id: self.unitId,
                fromDate: $.datepicker.formatDate("yy-mm-dd", dateFrom),
                untilDate: $.datepicker.formatDate("yy-mm-dd", dateUntil),
            }, function (data) {

                $.each(data, function (index, element) {
                    element.DateFrom = oth.common.parseDateString(element.DateFrom);
                    element.DateUntil = oth.common.parseDateString(element.DateUntil);
                });

                self.objects = data;
                d.resolve(data);
            }).error(function () {
                d.reject();
            });

            return d.promise();
        }

        //private functions
        function _changeYear(year) {
            _showLoading();

            //clearing old data
            $(self.dataTableTbodySelector).html('');
            //retriving data
            $.when(self.getData(year))
                .done(function (data) {
                    self.objects = data;
                    self.ratesToDelete = [];

                    $.each(data, function (index, element) {
                        _addRow(element);
                    });

                    $(self.errorBoxSelector).remove();
                    $(self.dialogSelector).dialog("open");
                    $(self.addPeriodButtonSelector).focus();

                    _hideLoading();
                });
        }

        function _applyChanges() {
            var areFieldsValid = _validateFields();
            var areDateRangesNotOverlapping = _validateDateRanges();

            var errors = [];
            if (!areFieldsValid)
                errors.push(self.resources.fieldsAreInvalid);

            if (!areDateRangesNotOverlapping)
                errors.push(self.resources.ratesCannotOverlap);

            $(self.errorBoxSelector).remove();
            if (errors.length > 0)
                $(self.errorContainerSelector).append(_createErrorBox(errors));
            else
                _updateData();
        }

        function _updateData() {
            _showLoading();

            var objectsToUpdate = [];

            $.each($(self.dataTableTbodySelector + " tr"), function (index, element) {
                var object = self.onGetRowElement(element);
                if (object != null)
                    objectsToUpdate.push(object);
            });

            var model = {
                UnitId: self.unitId,
                UnitObjectsToUpdate: objectsToUpdate,
                UnitRatesToDelete: self.ratesToDelete
            }

            $.ajax({
                contentType: 'application/json',
                type: "POST",
                url: self.changeObjectsAction,
                data: JSON.stringify(model),
                dataType: "json",
            }).done(function (response) {
                _hideLoading();

                if (!response.Valid) {
                    $(self.errorContainerSelector).append(_createErrorBox([response.Message]));
                }
                else {
                    self.onCalendarDataRefresh();
                    _closeWindow();
                }
            });
        }


        function _validateFields() {
            var areFieldsValid = true;
            $.each($(self.dialogSelector + " input[type=text]"), function (index, element) {
                var elementValue = $(element).val();
                if ((elementValue === "" || elementValue == undefined) || //checking empty values
                    ($(element).hasClass("date") && oth.common.parseDateString(elementValue) == null) || // checking date values
                    ($(element).hasClass("number") && isNaN(parseFloat(elementValue)))) { // checking number values
                    $(element).addClass("required-field");
                    areFieldsValid = false;
                }
                else {
                    $(element).removeClass("required-field");
                }
            });

            return areFieldsValid;
        }

        function _validateDateRanges() {
            var dateRanges = [];

            $.each($(self.dataTableTbodySelector + " tr"), function (index, element) {
                var dateFrom = oth.common.parseDateString($(element).find(".date-from").val());
                var dateUntil = oth.common.parseDateString($(element).find(".date-until").val());

                if (dateFrom != null && dateUntil != null)
                    dateRanges.push({ dateFrom: dateFrom, dateUntil: dateUntil });
            });

            for (var i = 0; i < dateRanges.length; i++) {
                for (var j = 0; j < dateRanges.length; j++) {
                    if (i == j)
                        continue;

                    if (_haveDateRangesOverlap(dateRanges[i].dateFrom, dateRanges[i].dateUntil,
                                                 dateRanges[j].dateFrom, dateRanges[j].dateUntil))
                        return false;
                }
            }

            return true;
        }

        function _haveDateRangesOverlap(rangeFrom, rangeUntil, rangeFrom2, rangeUntil2) {
            return ((rangeFrom <= rangeUntil2) && (rangeUntil >= rangeFrom2))
        }

        function _createErrorBox(errors) {
            var errorContainer = $("<div class='error-container' />");

            $.each(errors, function (index, error) {
                errorContainer.append("<p>" + error + "</p>");
            });

            return errorContainer;
        }

        function _closeWindow() {
            self.isOpen = false;
            $(self.dialogSelector).dialog("close");
        }

        function _addRow(dataObject) {
            var template = $(self.rowTemplateSelector).html();
            $(self.dataTableTbodySelector).append(self.onFillTemplateRow(dataObject, template));

            //initializing datepickers in the new row
            _initDatePickers();

            $(self.lastAddedRowDeleteFieldSelector).click(function () {
                var tableRowElement = $(this).parent().parent();
                var elementId = tableRowElement.attr("object-id");
                if (elementId !== undefined && elementId !== "")
                    self.ratesToDelete.push(elementId);

                $(this).parent().parent().remove();
            });
        }

        function _initDatePickers() {
            $.each($(self.lastAddedRowDateFieldSelector + " > input[type=text]"), function (index, element) {
                var date = oth.common.parseDateString($(element).val());
                if (date == null)
                    date = new Date();

                $(element).DatePicker({
                    current: date,
                    date: date,
                    calendars: 1,
                    starts: 0,
                    position: 'r',
                    onChange: function (dateSelected) {
                        var options = $(this).data('datepicker');

                        $(options.el).val(oth.common.getLocalizedDateString(dateSelected));
                        $(options.el).DatePickerHide();
                        $(options.el).change();
                    },
                    mode: 'single'
                });
            });
        };

        function _clearNonTemplateDialog() {
            var rowElement = $(self.dialogSelector).find("table > tbody > tr");
            rowElement.find("input").each(function (index, val) {
                var input = $(this);
                switch (input.attr("type"))
                {
                    case "text":
                        input.val('');
                        break;
                    case "checkbox":
                        input.attr('checked', true);
                        break;
                }
            });

            $("div.error-container").remove();
            _validateDateRanges();
        }

        function _showLoading() {
            $(self.dialogButtonContainerSelector + " button").hide();
            $(self.dialogButtonContainerSelector).append("<img src='/images/otp/ajax-loader-black-bg.gif' />");
        }

        function _hideLoading() {
            $(self.dialogButtonContainerSelector + " button").show();
            $(self.dialogButtonContainerSelector).find("img").remove();
        }
    }

    return new ViewModel();

}(oth || {}, jQuery));