﻿var oth = oth || {};

oth.changePasswordViewModel = (function (oth, $) {
    var ViewModel = function () {
        var self = this;

        self.infoBoxSelector = "#info-box";
        self.changePasswordFormSelector = "#changepassword_form > form";

        self.resources = {
            info: ""
        }

        self.init = function (options) {
            $.extend(this, options);
        }

        //Methods
        self.onPasswordChangeRequestComplete = function (data) {
            $(self.infoBoxSelector).html("");

            if (data.Valid) {
                self.showDialog(data.Message);
                $(self.changePasswordFormSelector).clearForm();
            }
            else {
                $(self.infoBoxSelector).html(data.Message);
                $(self.infoBoxSelector).show();
            }
        }       

        self.showInfoDialog = function (msg) {
            $('<div id="dialog" title="' + self.resources.info + '"><p>' + msg + '</p></div>').dialog({
                modal: false,
                resizable: false,
                buttons: {
                    Ok: function () {
                        jQuery(this).dialog("close");
                    }
                }
            });
        }

        self.showDialog = function (msg) {
            $('<div id="dialog" title="' + self.resources.info + '"><p>' + msg + '</p></div>').dialog({
                modal: false,
                resizable: false,
                buttons: {
                    Ok: function () {
                        jQuery(this).dialog("close");
                        window.location = "/";
                    }
                }
            });
        }
    };


    return new ViewModel();

}(oth || {}, jQuery));