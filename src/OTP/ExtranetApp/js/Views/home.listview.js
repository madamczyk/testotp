/* 
JS Logic for the /Home/ListView View 
*/

var oth = oth || {};

oth.listViewViewModel = (function (oth, $) {
    var ViewModel = function () {
        var self = this;

        self.page_no_cookie = "list_view_page_no";
        self.scroll_ajax_active = false;
        self.currentResults = 0;
        self.maxResults = 0;

        function modifyPageNumber(number) {
            var cookie_value = parseInt(jQuery.cookie(self.page_no_cookie));
            jQuery.cookie(self.page_no_cookie, cookie_value + number);
        }

        function resetPageNumber() {
            jQuery.cookie("list_view_page_no", 1);
        }

        function updateSearchInfoBar() {
            jQuery("#lv-date-container").hide();
            jQuery("#lv-result-destination-container").hide();

            if (dateFrom != '' && dateTo != '') {
                jQuery("#lv-date-from").html(dateFrom);
                jQuery("#lv-date-to").html(dateTo);
                jQuery("#lv-date-container").show();
            }

            jQuery("#lv-destination").html(jQuery("#destinationLink").html());
            jQuery("#lv-result-destination-container").show();

            var countElem = jQuery("#lv-result-count");
            jQuery("#lv-result-count-container").html(countElem.html());
            countElem.remove();
        }

        function setHandlersAfterAjaxCall() {            
            // set thumbnail cycle - slideshow
            jQuery('.thumbnail_container').each(function (index, element) {
                jQuery(this).cycle({
                    fx: 'fade',
                    timeout: 2000
                }).cycle("pause");
            });

            //change thumbnails on hover
            jQuery(".portfolio_image").each(function (index, element) {
                jQuery(this).hover(
                    function () {
                        jQuery(this).find(".thumbnail_container").cycle("resume");
                    },
                    function () {
                        jQuery(this).find(".thumbnail_container").cycle("pause");
                    }
                );
            });

            jQuery(".propertyDetailsLink").each(function (index, element) {
                if (jQuery(this).attr("data-location") != '') {
                    jQuery(this).click(function () {
                        setReturnSearchPageCookie();
                        window.location = jQuery(this).attr("data-location") + window.location.hash;
                    });
                }
            });

            var documentTitle = "Oh the Places: " + $("a#destinationLink").text();
            if (dateFrom != '' && dateTo != '') {
                documentTitle += ", " + $("a#dateFromLink").text() + " - " + $("a#dateToLink").text();
            }
            document.title = documentTitle;
            jQuery("div[searchfinalscore-value='100']").css('cursor', 'default');
        }

        self.sort = function (sortTypeValue) {
            self.searchRequest("sort");
        }

        self.setResultCount = function (maxResults, currentResults) {
            self.maxResults = maxResults;
            self.currentResults = currentResults;
        }

        self.searchRequest = function(search_type) {
            if (search_type != "scroll") {
                resetPageNumber();
            }

            if (search_type != "scroll" && search_type != "sort") {
                search_type = "";
            }

            var searchPathUrl = "/Home/SearchListView?"
            var searchParams = getSearchParameters();


            if (search_type == "scroll") { //show ajax update only on content lazy loading
                jQuery("#lazyLoading").css("display", "block");
            }

            jQuery.ajax({
                url: searchPathUrl + searchParams + "requestType=" + search_type + "&",
                dataType: 'html',
                beforeSend: function () {
                    disableFilterChecboxes();
                },
                success: function (data, textStatus, jqXHR) {
                    location.hash = searchParams;

                    if (jQuery.cookie(self.page_no_cookie) != null && parseInt(jQuery.cookie(self.page_no_cookie)) != 1) {
                        jQuery('#listViewContainer').append(data);
                    } else {
                        jQuery('#listViewContainer').html(data);
                    }

                    if (data == null || data == "") {
                        modifyPageNumber(-1);
                    }

                    setHandlersAfterAjaxCall();

                    checkForEmptyResultAndPopup(jQuery("#listViewContainer"));

                    updateSearchInfoBar();

                    SetFinalScoreDetailsHandler();
                },
                complete: function () {
                    $("#lazyLoading").css("display", "none");
                    self.scroll_ajax_active = false;
                    enableFilterChecboxes();

                    _gaq.push(['_trackPageview', location]);
                }
            });
        }

        self.init = function (options) {
            $.extend(self, options);

            resetPageNumber();

            var queryString = parseHashUrl();
            if (queryString != null) {
                //set menu values
                setMenuValues(queryString);

                //search and display results
                $.ajax({
                    url: "/Home/SearchListView?" + getSearchParameters(),
                    success: function (data, textStatus, jqXHR) {
                        $('#listViewContainer').html(data);

                        setHandlersAfterAjaxCall();

                        updateSearchInfoBar();

                        checkForEmptyResultAndPopup($("#listViewContainer"));

                        SetFinalScoreDetailsHandler();
                    }
                });
            }
            else {
                self.searchRequest();
            }

            $(document).bind("scroll touchmove", function () {
                if (self.scroll_ajax_active == true)
                    return;

                if ($(window).scrollTop() + $(window).outerHeight() > $(document).outerHeight() - 100) { // -5 fix for IE it stops loading content after 4-th page
                    if (self.currentResults >= self.maxResults)
                        return;

                    self.scroll_ajax_active = true;
                    modifyPageNumber(1);
                    self.searchRequest("scroll");
                }

            });            
        }
    }

    return new ViewModel();

}(oth || {}, jQuery));