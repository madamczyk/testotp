/* 
JS Logic for the /Home/Index View 
*/

var oth = oth || {};

oth.slideshowViewModel = (function (oth, $) {
    var ViewModel = function () {
        var self = this;

        self.galleryChangeImageTime = 14000;
        self.timeLeftToChange = 0;
        self.progressBarValue = 0;
        self.progressBarCallbackTimer = 500;

        self.init = function (options) {
            $.extend(self, options);

            $("#progressBar").progressbar(
            {
                value: self.progressBarValue
            });

            var queryString = parseHashUrl();
            if (queryString != null) {
                //set menu values
                setMenuValues(queryString);

                //search and display results
                jQuery.ajax({
                    url: "/Home/Search?" + getSearchParameters(),
                    success: function (data, textStatus, jqXHR) {
                        jQuery('#flexsliderContainer').html(data);
                        jQuery('.flexslider').flexslider({
                            slideshowSpeed: self.galleryChangeImageTime,
                            after: self.resetProgressBar
                        });                        

                        setHandlersAfterAjaxCall();

                        checkForEmptyResultAndPopup(jQuery(".slides"));

                        SetFinalScoreDetailsHandler();
                    }
                });
            }
                //sample search result
            else {
                searchRequest();
            }

            jQuery('.flexslider').flexslider({
                slideshowSpeed: self.galleryChangeImageTime,
                after: self.resetProgressBar
            });

            elem = jQuery("#index");
            if (elem.is(".home")) {
                jQuery(document).ready(function () {
                    jQuery("#flexslider").queryLoader2({
                        completeAnimation: "fade"
                    });
                });
            };
        }

        self.searchRequest = function () {
            var searchPathUrl = "/Home/Search?"
            var searchParams = getSearchParameters();

            disableFilterChecboxes();

            jQuery.ajax({
                url: searchPathUrl + searchParams,
                success: function (data, textStatus, jqXHR) {
                    jQuery('#flexsliderContainer').html(data);
                    jQuery('.flexslider').flexslider();
                    setHandlersAfterAjaxCall();

                    location.hash = searchParams;

                    checkForEmptyResultAndPopup(jQuery(".slides"));

                    SetFinalScoreDetailsHandler();                    
                },
                complete: function () {
                    enableFilterChecboxes();
                    _gaq.push(['_trackPageview', location]);
                }
            });
        }

        self.updateProgressBar = function () {
            self.progressBarValue = (self.timeLeftToChange / self.galleryChangeImageTime) * 100;
            self.timeLeftToChange += self.progressBarCallbackTimer;
            $("#progressBar").progressbar("option", "value", self.progressBarValue);
            setTimeout(updateProgressBar, self.progressBarCallbackTimer);
        }

        self.resetProgressBar = function () {
            if ($("#progressBar").progressbar() == null) return;
            self.timeLeftToChange = 0;
            $("#progressBar").progressbar("option", "value", 0);

        }


        function setHandlersAfterAjaxCall() {
            jQuery(".propertyDetailsLink").each(function (index, element) {
                if (jQuery(this).attr("data-location") != '') {
                    jQuery(this).click(function () {
                        setReturnSearchPageCookie();
                        window.location = jQuery(this).attr("data-location") + window.location.hash;

                    });
                }
            });

            var documentTitle = "Oh the Places: " + $("a#destinationLink").text();
            if (dateFrom != '' && dateTo != '') {
                documentTitle += ", " + $("a#dateFromLink").text() + " - " + $("a#dateToLink").text();
            }
            document.title = documentTitle;

            setTimeout(updateProgressBar, self.progressBarCallbackTimer);
            jQuery("span[searchfinalscore-value='100']").css('cursor','auto');

        }
    }

    return new ViewModel();

}(oth || {}, jQuery));