﻿var oth = oth || {};

oth.mapViewViewModel = (function (oth, $) {
    var ViewModel = function () {
        var self = this;

        //selectors
        self.mapContainerSelector = ".map-container";
        self.leftBorderSelector = "#left";
        self.rightBorderSelector = "#right";
        self.imageCacheSelector = "#imageCache";
        self.destinationFilterContainerSelector = "#nv-destination";
        self.distanceFilterContainerSelector = "#nv-distance";
        self.distanceFilterContentSelector = self.distanceFilterContainerSelector + " > span";
        self.distanceFilterTitleSelector = self.distanceFilterContainerSelector + " > a";
        self.mapRemoveRectControlSelector = ".remove-rect";

        //height of top and bottom nav bar. Needed for calculating map container height
        self.topNavigationBarHeight = 80;
        self.bottomNavigationBarHeight = 30;

        //action urls
        self.searchActionUrl = "/Home/SearchMapView";
        self.propertyDetailsUrl = "/PropertyDetail/PropertyDetails";

        //The center point for google maps. Default is set somewhere in the center of USA
        self.defaultCenterPoint = new google.maps.LatLng(39.7392, -104.9842);

        //Google maps related objects
        self.map = null;
        self.drawingManager = null;
        self.mapMarkers = [];

        self.rectangle = null;
        self.rectangleOptions = {
            strokeWeight: 1,
            strokeOpacity: 1,
            fillOpacity: 0.2,
            editable: true,
            clickable: false,
            strokeColor: '#3399FF',
            fillColor: '#3399FF'
        }

        //flag which determines wether country uses metric system. Needed for distance calculation
        self.isMetric = false;

        //localized resources
        self.resources = {
            night: "",
            priceFrom: "",
            removeRectangle: "",
            from: ""
        }

        self.infoBubble = new InfoBubble({
            maxWidth: 300,
            borderWidth: 1,
            borderRadius: 1,
            borderColor: "#000000",
            backgroundColor: "#000000",
            closeImageSrc: { src: '/images/otp/map_markers/close_icon.gif', width: 28, height: 24 }
                
        });

        self.init = function (options) {
            $.extend(self, options);

            $(self.leftBorderSelector).remove();
            $(self.rightBorderSelector).remove();

            $(window).resize(function () {
                _resizeMapContainer();
            });

            _resizeMapContainer();
            _initMap();
            _initDrawingManager();


            var queryString = parseHashUrl();
            if (queryString != null &&
                queryString.beginLatitude !== undefined && queryString.endLatitude !== undefined &&
                queryString.beginLongitude !== undefined && queryString.endLongitude !== undefined) {
                _drawRectangle(queryString);
            }

            if(queryString != null)
                setMenuValues(queryString);

            self.doSearch();

        }

        //performs search for properties
        self.doSearch = function () {
            var searchParams = getSearchParameters(self.rectangle !== null);
            searchParams += _getRectangleFilterParameters();
            jQuery.ajax({
                url: self.searchActionUrl + "?" + searchParams,
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    location.hash = searchParams;

                    //clearing image cache
                    $(self.imageCacheSelector).html("");

                    _removeAllMarkers();
                    _showPropertiesOnMap(data);

                    if (self.mapMarkers.length > 0) {
                        self.map.setCenter(self.mapMarkers[0].getPosition());
                        self.map.setZoom(7);
                    }

                    var documentTitle = "Oh the Places: " + $("a#destinationLink").text();
                    if (dateFrom != '' && dateTo != '') {
                        documentTitle += ", " + $("a#dateFromLink").text() + " - " + $("a#dateToLink").text();
                    }
                    document.title = documentTitle;
                },
                complete: function () {
                    _gaq.push(['_trackPageview', location]);
                }
            });
        }

        //create url parameters based on rectangle that's been drawn on google maps.
        function _getRectangleFilterParameters() {
            var params = "";

            if (self.rectangle !== null) {
                var rectBounds = self.rectangle.getBounds();

                params += "beginLatitude=" + rectBounds.Z.b;
                params += "&endLatitude=" + rectBounds.Z.d;

                params += "&beginLongitude=" + rectBounds.ca.b;
                params += "&endLongitude=" + rectBounds.ca.d;
            }
            return params;
        }

        //shows properties locations by creating markers on the google map
        function _showPropertiesOnMap(properties) {
            $.each(properties, function (index, property) {
                //preloading image for better user expriance
                var image = $("<img />");
                image.attr("src", property.Thumbnails[0]);
                $(self.imageCacheSelector).prepend(image);

                var propertyPos = new google.maps.LatLng(property.Latitude, property.Longitude);
                var houseIcon = "/images/otp/map_markers/villa.png";
                var propertyMarker = new google.maps.Marker({
                    position: propertyPos,
                    map: self.map,
                    zIndex: 3,
                    icon: houseIcon
                });

                google.maps.event.addListener(propertyMarker, 'click', function (event) {
                    var bubleContent = _createPropertyDetailsPopup(property).html();

                    self.infoBubble.setContent(    
                        bubleContent);

                    self.infoBubble.open(self.map, propertyMarker);                   
                });

                self.mapMarkers.push(propertyMarker);
            });
            
        }

        //creates popup with property details
        function _createPropertyDetailsPopup(property) {
            var propertyDescriptionDiv = $("<div />");
            var propertyImageLink = $("<a />");
            propertyImageLink.attr("href", "/" + property.Destination + "/" + property.Id + "_" + property.TitleEncoded + "#" + getSearchParameters());
            propertyImageLink.addClass("propertyDetailsLink");
            propertyImageLink.attr("onclick", "setReturnSearchPageCookie()");            

            var backgroundImage = $("<img />");
            backgroundImage.attr("src", property.Thumbnails[0]);
            propertyImageLink.append(backgroundImage);


            var infoDiv = $("<div />");
            infoDiv.addClass("property-details");

            var propertyLink = $("<a />");
            propertyLink.attr("href", "/" + property.Destination + "/" + property.Id + "_" + property.TitleEncoded + "#" + getSearchParameters());
            propertyLink.addClass("propertyDetailsLink");
            propertyLink.attr("onclick", "setReturnSearchPageCookie()");
            propertyLink.html(property.Title);
            
            var scoreDetailsLink = $("<a />");
            scoreDetailsLink.addClass("propertyDetailsLink scoreDetailsLink");
            scoreDetailsLink.attr("searchfinalscore-unitid", property.Id);
            scoreDetailsLink.attr("searchfinalscore-value", property.FinalScore);
            scoreDetailsLink.attr("style", "float: right;");
            scoreDetailsLink.attr("onclick", "ShowMapViewFinalScoreDetails(this, " + property.Id + ", " + property.FinalScore + ");");
            scoreDetailsLink.html(property.ScoreLabel + " " + property.FinalScore + "%");
            if (property.FinalScore == 100) {
                scoreDetailsLink.css('cursor', 'default');
            }
            var informations = [];
            informations.push(property.Description);

            var bedroomBathroomInformation = "";
            if(property.Bedrooms !== undefined && property.Bedrooms > 0)
                bedroomBathroomInformation += oth.common.getCardinalityResource(oth.common.localizedBedroom, oth.common.localizedBedrooms, property.Bedrooms);

            if (property.Bathrooms !== undefined && property.Bathrooms > 0)
                bedroomBathroomInformation += " / " + oth.common.getCardinalityResource(oth.common.localizedBath, oth.common.localizedBaths, property.Bathrooms);

            if (bedroomBathroomInformation != "")
                informations.push(bedroomBathroomInformation);

            if (property.Sleeps !== undefined && property.Sleeps > 0)
                informations.push(oth.common.getCardinalityResource(oth.common.localizedSleep, oth.common.localizedSleeps, property.Sleeps));

            if(property.IsPricePerNight)
                informations.push(property.PricePerNight + " " + self.resources.night);
            else
                informations.push(self.resources.priceFrom + " " + property.Price);

            var detailsParagraph = $("<p />");
            detailsParagraph.html(_createInformationsHtml(informations));

            infoDiv.append(propertyLink);
            infoDiv.append(scoreDetailsLink);
            infoDiv.append(detailsParagraph);

            propertyDescriptionDiv.append(propertyImageLink);
            propertyDescriptionDiv.append(infoDiv);

            return propertyDescriptionDiv;
        }

        //creates html code for each property information(string) in the array. Informations are separated with br tag.
        function _createInformationsHtml(informations) {
            var informationHtml = "";
            $.each(informations, function (index, information) {
                informationHtml += information + "<br />";
            });

            return informationHtml;
        }
        
        //initializes google map
        function _initMap() {
            var settings = {
                zoom: 5,
                center: self.defaultCenterPoint,
                navigationControl: true,
                navigationControlOptions: { style: google.maps.NavigationControlStyle.SMALL },
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            self.map = new google.maps.Map($(self.mapContainerSelector).get(0), settings);
            self.map.controls[google.maps.ControlPosition.TOP_CENTER].push(_createRemoveRectButton(_deleteRectangle)[0]);
        }

        //creates a custom remove rectangle button to be displayed on google maps
        function _createRemoveRectButton(controlClickHandler) {
            var controlDiv = $("<div />");
            controlDiv.css("display", "none");
            controlDiv.addClass("remove-rect");
            controlDiv.click(controlClickHandler);

            var controlBorder = $("<div />");
            controlBorder.attr("title", self.resources.removeRectangle);

            var controlContent = $("<div><strong>X</strong></div>");

            controlBorder.append(controlContent);
            controlDiv.append(controlBorder);

            return controlDiv;
        }

        function _initDrawingManager() {
            self.drawingManager = new google.maps.drawing.DrawingManager({
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [
                      google.maps.drawing.OverlayType.RECTANGLE
                    ]             
                },
                rectangleOptions: self.rectangleOptions,
                map: self.map
            });

            new google.maps.event.addListener(self.drawingManager, "rectanglecomplete", function (rectangle) {
                self.rectangle = rectangle;
                _setRectangleEvents();

                $(self.mapRemoveRectControlSelector).show();

                self.drawingManager.setOptions({
                    drawingControl: false,
                    drawingMode: null
                });

                _showDistanceFilter();

                self.doSearch();
            });
        }

        //sets events which let you edit size of the rectangle 
        function _setRectangleEvents() {
            new google.maps.event.addListener(self.rectangle, 'bounds_changed', function (bounds) {
                self.doSearch();
            });
        }

        //draws a rectangle on google map based on query string filters
        function _drawRectangle(queryString) {
            self.rectangle = new google.maps.Rectangle({
                bounds: new google.maps.LatLngBounds(new google.maps.LatLng(queryString.beginLatitude, queryString.beginLongitude),
                                                     new google.maps.LatLng(queryString.endLatitude, queryString.endLongitude)),
                strokeWeight: self.rectangleOptions.strokeWeight,
                strokeOpacity: self.rectangleOptions.strokeOpacity,
                fillOpacity: self.rectangleOptions.fillOpacity,
                editable: self.rectangleOptions.editable,
                clickable: self.rectangleOptions.clickable,
                strokeColor: self.rectangleOptions.strokeColor,
                fillColor: self.rectangleOptions.fillColor,
                map: self.map
            });

            self.drawingManager.setOptions({
                drawingControl: false,
                drawingMode: null
            });

            _setRectangleEvents();
            _showDistanceFilter();

            //fix because control is being hidden by google maps.
            setTimeout(function () {
                $(self.mapRemoveRectControlSelector).show();
            }, 1000);
        }
        
        //removes rectangle filter from the map
        function _deleteRectangle() {
            self.rectangle.setMap(null);
            self.rectangle = null;

            $(self.mapRemoveRectControlSelector).hide();

            self.drawingManager.setOptions({
                drawingControl: true,
                drawingMode: null
            });

            _showDestinationFilter();

            self.doSearch();
        }

        //shows destination filter
        function _showDestinationFilter() {
            $(self.destinationFilterContainerSelector).show();
            $(self.distanceFilterContainerSelector).hide();
        }

        //shows distance filter
        function _showDistanceFilter() {
            if (self.rectangle != null) {
                $(self.destinationFilterContainerSelector).hide();
                $(self.distanceFilterContainerSelector).show();

                var rectBounds = self.rectangle.getBounds();
                var rectangleCenterPoint = rectBounds.getCenter();
                var distanceInKms = _calculateDistance(rectBounds.Z.b,
                                                       rectBounds.ca.b, rectangleCenterPoint.lat(), rectangleCenterPoint.lng());
                
                if (self.isMetric)
                    $(self.distanceFilterTitleSelector).html(distanceInKms.toFixed(1) + " km");
                else
                    $(self.distanceFilterTitleSelector).html(_convertKilometersToMiles(distanceInKms).toFixed(1) + " m");

                $(self.distanceFilterContentSelector).html(self.resources.from + " " + _formatCoords(rectangleCenterPoint.lat(), rectangleCenterPoint.lng()));
            }
        }

        //calculates distance(in km's) between two points on the map. Uses the haversine formula
        function _calculateDistance(lat1, lng1, lat2, lng2) {
            var r = 6371; //earth's radius
            var dLat = _toRadians(lat2 - lat1);
            var dLng = _toRadians(lng2 - lng1);
            var lat1 = _toRadians(lat1);
            var lat2 = _toRadians(lat2);

            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                    Math.sin(dLng / 2) * Math.sin(dLng / 2) * Math.cos(lat1) * Math.cos(lat2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

            return r * c;
        }

        //converts kilometers to miles
        function _convertKilometersToMiles(distanceInKms) {
            return distanceInKms * 0.621371;
        }

        //converts angles to radians.
        function _toRadians(angle) {
            return angle * Math.PI / 180;
        }

        function _formatCoords(lat, lng) {
            var formattedCoords = Math.abs(lat).toFixed(2) + "° ";
            formattedCoords += lat > 0 ? "N" : "S";
            formattedCoords += ", " + Math.abs(lng).toFixed(2) + "° ";
            formattedCoords += lng > 0 ? "E" : "W";

            return formattedCoords;
        }

        //resizes map container. Called every time the size of browser window change.
        function _resizeMapContainer() {
            $(self.mapContainerSelector).css("height",
                $(window).height() - self.topNavigationBarHeight - self.bottomNavigationBarHeight);
        }

        //removes all markers from map
        function _removeAllMarkers() {
            $.each(self.mapMarkers, function (index, marker) {
                marker.setMap(null);
            });

            self.mapMarkers = [];
        }       
        
    }

    return new ViewModel();

}(oth || {}, jQuery));