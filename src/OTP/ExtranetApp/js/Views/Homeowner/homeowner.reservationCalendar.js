﻿
var oth = oth || {};

oth.homeowner_reservationCalendar = (function (oth, $) {
    var ViewModel = function () {
        var self = this;


        self.cellStart = null;
        self.cellEnd = null;

        //localized resources
        self.resources = {
            GrossDue: "",
            TransactionFee: "",
            OwnerNet: "",
            Confirmation: ""
        }

        self.init = function (options) {
            $.extend(self, options);

            jQuery(document).tooltip({
                items: "table.ui-datepicker-calendar tbody td.markedDay a",
                position: { my: "left top+15", at: "left bottom", collision: "flipfit" },
                close: function (event, ui) {
                    jQuery("td a[data-date]").filter(function () {
                        var currentCellDate = new Date(jQuery(this).attr("data-date").replace(/-/g, '/'));
                        if (currentCellDate.clearTime().between(self.cellStart, self.cellEnd)) {
                            return true;
                        }
                        return false;
                    }).removeClass("rangeSelection");
                    jQuery('div[role="tooltip"]').fadeOut(500, function () {
                        jQuery(this).remove();
                    });
                },

                content: function () {
                    ChangeDataAtrib();

                    var dateCell = jQuery(this);

                    var titleVal = '';
                    titleVal = dateCell.attr("data-date");

                    var onDate = new Date(titleVal);
                    var objectItem = null;

                    calendarData.forEach(function (value, index) {
                        if (onDate.clearTime().between(value.dateArrival, value.dateLeave)) {
                            objectItem = value;
                            return;
                        }
                    });

                    var content = '<div id="popup-reservationDetails">' +
                        '<div id="popup-header">' +
                            '<div id="popup-user">' + objectItem.guest + '</div>' +
                            '<div id="popup-confirmation">'+self.resources.Confirmation+' ' + objectItem.confirmation + '</div>' +
                            '<div style="clear:both"></div>' +
                        '</div>' +
                        '<div id="popup-tripDates">' + oth.common.getLocalizedDateString(objectItem.dateArrival) + ' - ' + oth.common.getLocalizedDateString(objectItem.dateLeave) + '</div>' +
                        '<div class="popup-amount"><div class="price-desc">'+self.resources.GrossDue+'</div><div class="price-value">' + objectItem.grossAmount + '</div></div>' +
                        '<div class="popup-amount"><div class="price-desc">'+self.resources.TransactionFee+'</div><div class="price-value">' + objectItem.transactionFee + '</div></div>' +
                        '<div class="popup-amount" id="popup-net"><div class="price-desc">'+self.resources.OwnerNet+'</div><div class="price-value">' + objectItem.ownerNet + '</div></div>' +
                    '</div>';

                    self.cellStart = objectItem.dateArrival;
                    self.cellEnd = objectItem.dateLeave;

                    jQuery("td a[data-date]").filter(function () {
                        var currentCellDate = new Date(jQuery(this).attr("data-date"));
                        if (currentCellDate.clearTime().between(self.cellStart, self.cellEnd)) {
                            return true;
                        }
                        return false;
                    }).addClass("rangeSelection");

                    return content;
                }
            });

            jQuery("div.changeView ul#changeView li").click(function () {
                jQuery(this).children("ul").toggle("slow");
                return false;
            });

            var calendarStartDate = new Date();
            calendarStartDate.setDate(1);
            calendarStartDate.setMonth(0);

            jQuery("#reservation-calendar").datepicker(
                {
                    numberOfMonths: [3, 4],
                    showButtonPanel: false,
                    defaultDate: calendarStartDate,
                    firstDay: 0,
                    beforeShowDay: daysToMark
                });
        };

        function daysToMark(date) {
            var style = "";
            var title = "";

            calendarData.forEach(function (value, index) {
                if (date.clearTime().between(value.dateArrival, value.dateLeave)) {
                    if (value.propertyId == 0)
                        style = 'markedDayOwner';
                    else {
                        style = 'markedDay';
                        title = date.toString('yyyy/MM/dd');
                    }

                }
            })

            return [true, style, title];
        }

        function ChangeDataAtrib() {
            jQuery("table.ui-datepicker-calendar tbody td.markedDay").each(function (index, element) {
                var titleVal = jQuery(element).attr("title");
                jQuery(element).removeAttr("title");
                jQuery(element).find("a").attr("data-date", titleVal);
            });
        }

    }

    return new ViewModel();

}(oth || {}, jQuery));