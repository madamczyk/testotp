﻿var oth = oth || {};
var failed = false;

oth.contentUpdateViewModel = (function (oth, $) {
	var ViewModel = function () {
		var self = this;
		self.init = function (options) {

			$.extend(self, options);

			jQuery("form").find('.input-textarea').each(function () {
				var defValue = jQuery(this).attr("defaultText");
				jQuery(this).watermark(defValue);
			});


			function _closeUpload() {
				$(options.selectorPopUp).dialog("close");
			}
			function _applyChanges() {
				$(options.selectorPopUp).dialog("close");
			}

			jQuery(".fancy_checkbox").uniform({ hoverClass: 'myHoverClass' });
			var buttons = {};
			//buttons[options.attachButton] = _applyChanges;
			buttons[options.closeButton] = _closeUpload;

			jQuery("#btnBrowse").click(function () {
				//AJAX reference
				//jQuery.ajax({
				//	url: "/PropertyDetail/GetInvoiceDetails?unitId=" + unitId + "&propertyId=" + propertyId + "&dateFrom=" + dateFrom + "&dateUntil=" + dateTo,
				//	success: function (data, textStatus, jqXHR) {
				//		jQuery("#widget-invoice").show();
				//		jQuery('#widget-invoice').html(data);

				//		jQuery("div.collapse-section-trigger").not('[data-section="widget-tripDetails-section"]').click(function () {
				//			var sectionId = jQuery(this).attr("data-section");
				//			jQuery("#" + sectionId).toggle("slow");
				//			jQuery(this).find(".section-title-overall-value").toggle();

				//			var collapsibleSection = jQuery(this).find("span.ui-icon-triangle-1-e");
				//			if (collapsibleSection.size() != 0) {
				//				collapsibleSection.removeClass("ui-icon-triangle-1-e");
				//				collapsibleSection.addClass("ui-icon-triangle-1-s");
				//			} else {
				//				collapsibleSection = jQuery(this).find("span.ui-icon-triangle-1-s");
				//				collapsibleSection.removeClass("ui-icon-triangle-1-s");
				//				collapsibleSection.addClass("ui-icon-triangle-1-e");
				//			}
				//		});

				//		getMainSectionInfo();
				//		showTripDetailsDates();
				//	}
				//});

				return false;
			});
			$("#storageLinks").html("");
			$("#storageLinks").val("");
			$(options.selectorPopUp).dialog({
				autoOpen: false,
				height: 400,
				width: 627,
				modal: true,
				open: function () { $(this).closest(".ui-dialog").find(".ui-dialog-titlebar:first").hide(); },
				buttons: buttons,
				zIndex: 10000,
				stack: false
			});

			$(function () {
			    'use strict';
			    // Initialize the jQuery File Upload widget:
			    $('#fileupload').fileupload();
			    
			    $('#fileupload').fileupload('option', {
			        maxFileSize: 500000000,
			        autoUpload: false,
			        start: function (e) {
			            $(".cancel").hide();
			            $.blueimpUI.fileupload.prototype
                            .options.start.call(this, e);
			            jQuery("#img-logo").trigger('startRumble', [0, 5]);
			            $(this).addClass("fileupload-processing");
			            $("#openPopUp").unbind("click");
			            $(".error .filesList").html();
			            $(".error").hide();
			        },
			        change: function (e, data) {
			            $(".noImages").css("display", "none");
			            $.each(data.files, function (index, file) {
			                $("#links").append(file.name, " ");
			                //$("#storageLinks").val($("#storageLinks").val()+file.name + ";");
			            });
			        },

			        done: function(e,data){
			            $.blueimpUI.fileupload.prototype
                            .options.done.call(this, e, data);
			            $.each(data.result, function (index, res) {
			                $("#storageLinks").val($("#storageLinks").val() + res.name + ";");
			            });
			        },
                    
			        stop: function (e) {

			            //$(".noImages").css("display", "none");
			            //$.each(data.files, function (index, file) {
			            //    $("#links").append(file.name, " ");
			            //    $("#storageLinks").append(file.name,";");
			            //});
			            $.blueimpUI.fileupload.prototype
                             .options.stop.call(this, e);
			            $(this).removeClass("fileupload-processing");
			            jQuery("#img-logo").trigger('stopRumble');
			            $("#openPopUp").click(function () {
			                if (!$('#fileupload').hasClass("fileupload-processing")) {
			                    $(options.selectorPopUp).dialog("open");
			                }
			            });
			            if (failed) {
			                var links =$("#storageLinks").val();
			                var propId = $("#propertyId").val();
			                jQuery.ajax({
			                        type: "POST",
			                        url: "/HomeOwner/DeleteAfterError",
                                    success: function (data, textStatus, jqXHR) {
                                        $(options.selectorPopUp).dialog("open");
                                        $("#storageLinks").val("");
                                        $("#links").html("");
                                        $(".files").html("");
                                        $(".noImages").show();
                                        failed = false;
                                    },
                                    data: {propertyId: propId, fileNamesRaw: links }
			                	});
			            } else {
			                $("#formContentUpdate").submit();
			            }
			        },
			        fail: function (e, data) {
			            $.blueimpUI.fileupload.prototype
                                 .options.fail.call(this, e, data);
			            $.each(data.files, function (index, file) {
			                $("#links").html($("#links").html().replace(file.name + " ", ""));
			            });
			            if (!e.currentTarget) {
			                $(".error").show();
			                $.each(data.files, function (index, file) {
			                    var edited = $(".error .filesList").append(", " + file.name).html().replace(/^,|,$/g, '');
			                    $(".error .filesList").html(edited);
			                });
			                failed = true;
			            }
			            if  ($("#links").html()=="")
                            $(".noImages").show();
			        }
			    });

			    $('#fileupload').fileupload({
			        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
			    });

			});

			$("#openPopUp").click(function () {
			    if (!$('#fileupload').hasClass("fileupload-processing")) {
			        $(options.selectorPopUp).dialog("open");
			    }
			});

			$("#btnSend").click(function () {
			    if ($(".noImages").css('display')=='none')
			        $('#attach').click();
			    else
			        $("#formContentUpdate").submit();

			    return false;
			})
			jQuery("#img-logo").jrumble({
			    x: 0,
			    y: 5,
			    rotation: 0,
			    speed: 60
			});
		}

	}
	return new ViewModel();
}(oth || {}, jQuery));


