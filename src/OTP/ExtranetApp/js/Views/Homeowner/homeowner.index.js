﻿var otp = otp || {};

otp.homeowner_index = (function (otp, $) {
    var ViewModel = function () {
        var self = this;      

        self.init = function (options) {
            $.extend(self, options);

            self.loginRoleDropDownSelector = "#LoginRole";
            self.emailTextFieldSelector = "#Email";
            self.userRoleDataSourceUrl = "/Account/GetUserRoles";
            self.changePasswordActionUrl = "/Account/ChangePassword";
            self.loginButtonSelector = "#btnLogin";            

            $(self.emailTextFieldSelector).blur(function () {
                var userEmail = $("#Email").val();
                if (userEmail == '')
                    return;               

                var roleSelect = $(self.loginRoleDropDownSelector);
                roleSelect.find("option").remove();

                jQuery.ajax({
                    url: self.userRoleDataSourceUrl + "?email=" + userEmail,
                    success: function (data, textStatus, jqXHR) {
                        $.each(data, function (index, item) {
                            var option = $("<option>"+item+"</option>")
                            roleSelect.append(option);
                        });
                    },
                    complete: function () {
                    }
                });
            });

            $(self.loginRoleDropDownSelector).keydown(function (e) {
                if (e.keyCode === 13) {
                    var formElement = $(self.loginRoleDropDownSelector).closest('form');
                    formElement.submit();
                }
            });

        };        

        self.onLoginComplete = function onLoginComplete(data) {
            if (data.Valid) {
                if (data.IsPasswordGenerated) {
                    window.location = self.changePasswordActionUrl;
                }
                else {
                    window.location = data.RedirectUrl;
                }
            }
            else {
                jQuery("#vsLoginOwner").html(data.Message);
            }
        }

    }

    return new ViewModel();

}(otp || {}, jQuery));