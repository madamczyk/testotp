﻿
var otp = otp || {};

otp.homeowner_keymanagement = (function (otp, $) {
    var ViewModel = function () {
        var self = this;
               
        self.filterPropertyName = '';
        self.filterGuestLastName = '';
        self.filterConfirmation = '';

        //Selectors
        var tableSelector = '#keyManagementData';
        var filterBtnSelector = '#submitFilter';

        self.init = function (options) {
            $.extend(self, options);           

            $('input[type="text"]').each(function () {
                var defValue = jQuery(this).attr("defaultText");
                $(this).watermark(defValue);
            });

            $(".input-textarea").keydown(function (e) {                
                if (e.keyCode == 13) { //on enter key
                    _filterResults();
                }
            });

            $(filterBtnSelector).click(function () {
                _filterResults();
            });

            function _filterResults() {
                self.filterConfirmation = $("#txtConfirmation").val();
                self.filterGuestLastName = $("#txtGuestName").val();
                self.filterPropertyName = $("#txtPropertyName").val();

                $.ajax({
                    url: "/Homeowner/GetFilteredKeyManagementData",
                    data: { Confirmation: self.filterConfirmation, GuestLastName: self.filterGuestLastName, PropertyName: self.filterPropertyName },
                    success: function (data, textStatus, jqXHR) {
                        $(tableSelector).find("tbody").find("tr").remove();
                        $(tableSelector).find("tbody").append(data);
                    }
                });
            }
            
        };
    }

    return new ViewModel();

}(otp || {}, jQuery));