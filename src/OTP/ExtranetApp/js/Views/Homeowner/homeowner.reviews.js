﻿
var otp = otp || {};

otp.homeowner_reviews = (function (otp, $) {
    var ViewModel = function () {
        var self = this;


        self.PropertyId = null;

        self.init = function (options) {
            $.extend(self, options);

            jQuery("#review-sort-menu li").click(function () {
                jQuery(this).children("ul").toggle("slow");
                return false;
            });

            ///Perform sort on the Guest Review Container
            jQuery(".sort_type").click(function () {
                var typeOfSort = jQuery(this).attr("data-sort");
                var currentElement = jQuery(this);
                jQuery(currentElement).parent("li").parent("ul").hide("slow");

                jQuery.ajax({
                    url: "/PropertyDetail/GetSortedPropertyReviews?propertyId=" + self.PropertyId + "&sort=" + typeOfSort,
                    success: function (data, textStatus, jqXHR) {
                        jQuery("#sortByExpressionReviews").html(jQuery(currentElement).html());
                        jQuery('#review-list-container').html(data);

                    },
                    complete: function () {
                    }
                });
            });

            jQuery(".review-group-link").hover(function () {
                var reviewId = jQuery(this).attr("review-group-id");
                jQuery(".review-dialog[review-group-id=" + reviewId + "]").show();
            }, function () {
                var reviewId = jQuery(this).attr("review-group-id");
                jQuery(".review-dialog[review-group-id=" + reviewId + "]").hide();
            });

        };
    }

    return new ViewModel();

}(otp || {}, jQuery));