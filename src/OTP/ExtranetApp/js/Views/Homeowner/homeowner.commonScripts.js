﻿var otp = otp || {};

otp.homeowner_commonScripts = (function (otp, $) {
    var ViewModel = function () {
        var self = this;

        /*
        Sets cookie info for JPlayer to support playing mp3 after changing webpage
        */
        function setJPlayerCookieVariables() {
            var playerCurrentStatus = jQuery("#audioBG").data("jPlayer").status.paused;
            jQuery("#audioBG").jPlayer("pause");
            var playerCurrentLocation = jQuery("#audioBG").data("jPlayer").status.currentTime;
            jQuery.cookie("playerCurrentLocation", playerCurrentLocation, { path: '/' });
            jQuery.cookie("playerCurrentStatus", playerCurrentStatus, { path: '/' });
        }

        /*
        Gets cookie info for JPlayer about last JPlayer paused status
        */
        function getJPlayerCookiePlayerStatus() {
            return jQuery.cookie("playerCurrentStatus");
        }

        /*
        Gets cookie info for JPlayer about last known plaing location
        */
        function getJPlayerCookiePlayerCurrentLocation() {
            return parseFloat(jQuery.cookie("playerCurrentLocation"));
        }

        function initJPlayer() {
            jQuery("#audioBG").jPlayer({
                ready: function () {
                    jQuery(this).jPlayer("setMedia", {
                        mp3: "/Sounds/KlangkarussellSonnentanz.mp3"
                    });
                    if (getJPlayerCookiePlayerStatus() == "false") { //Jplayer was plaing mp3 before page load so do a player resume
                        var playerLocation = getJPlayerCookiePlayerCurrentLocation();
                        jQuery("#audioBG").jPlayer("play", playerLocation);
                        jQuery("#pause_bt").hide();
                        jQuery("#play_bt").show();
                    }
                },
                ended: function (event) {
                    jQuery(this).jPlayer("play");
                },
                swfPath: "/js/"
            });
        }

        self.init = function (options) {
            $.extend(self, options);

            initJPlayer();

            jQuery("#pause_bt").click(function () {
                jQuery("#audioBG").jPlayer("play");
                jQuery(this).hide();
                jQuery("#play_bt").show();
            });

            jQuery("#play_bt").click(function () {
                jQuery("#audioBG").jPlayer("pause");
                jQuery(this).hide();
                jQuery("#pause_bt").show();
            });

            jQuery(window).unload(function () {
                setJPlayerCookieVariables();
            });

            jQuery(".logout-link").click(function () {
                jQuery("#logoutForm").submit();
            });
        }

    }

    return new ViewModel();

}(otp || {}, jQuery));