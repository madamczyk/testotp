﻿var dateTripStart = '';
var dateTripEnd = '';


jQuery(document).ready(function () {

    function initialize(lat, lng, zoomValue) {
        var latlng = new google.maps.LatLng(lat, lng);
        var settings = {
            zoom: zoomValue,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU },
            navigationControl: true,
            navigationControlOptions: { style: google.maps.NavigationControlStyle.SMALL },
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("googlemap"), settings);

        var companyImage = new google.maps.MarkerImage('../../Images/google-map-pointer.png',
            new google.maps.Size(290, 240),
            new google.maps.Point(0, 0),
            new google.maps.Point(6, 28)
        );


        var companyPos = new google.maps.LatLng(lat, lng);

        var companyMarker = new google.maps.Marker({
            position: companyPos,
            map: map,
            icon: companyImage,
            zIndex: 3
        });

        //google.maps.event.addListener(companyMarker, 'click', function () {
        //    infowindow.open(map, companyMarker);
        //});
    }

    function initMap() {

        var lat = latData.replace(",", ".");
        var lng = lngData.replace(",", ".");
        if (lat != "" && lng != "") {
            //document.getElementById('tdGoogleMap').style.display = '';
            initialize(lat, lng, 12);
        }
    }


    jQuery("#trip-calendar").DatePicker({
        date: [dateTripStart, dateTripEnd],
        current: dateTripStart,
        calendars: 1,
        starts: 0,
        mode: 'range',
        flat: true,
        readOnly: true
    });

    initMap();
});

