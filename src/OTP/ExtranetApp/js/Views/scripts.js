/*
!!! DEPRECATED !!!
SUGESTION: CREATE SEPARATE JS FILE PER VIEW
*/

/*-----------------------------------------------------------------------------------*/
/*	Prometheus Custom Script
/*-----------------------------------------------------------------------------------*/

var destination = "";
var dateFrom = "";
var dateTo = "";
var guests = "";
var bathrooms = "";
var bedrooms = "";
var propertyTypes = "";
var amenities = "";
var experiences = "";



function onTestComplete(data) {
    if (data.Valid) {
        window.location.reload(true);
    }
    else {
        jQuery("#blockLogin").html(data.Message);
    }
}

function onJoinComplete(data) {
    if (data.Valid) {
        jQuery("div#joinBlockPopup").hide("slow");
        jQuery("div#joinBlockPopup").find("form").clearForm();
        ShowDialog("Message", data.Message)
    }
    else {
        jQuery("#blockJoin").html(data.Message);
    }
}

function ShowDialog(title, description)
{
    jQuery('<div id="dialog" title="' + title + '"><p>' + description + '</p></div>').dialog({
        modal: false
    });
}

function setMenuValues(menuValues)
{
    if (menuValues.destination != null) {
        destination = menuValues.destination;
        var searchPathUrl = "/Home/GetDestinationNameById?id="
        jQuery.ajax({
            url: searchPathUrl + menuValues.destination,
            success: function (data, textStatus, jqXHR) {
                jQuery("#destinationLink").text(data.toString() + ",");
            }
        });
    }
    if (menuValues.dateFrom != null) {
        jQuery("#dateFromLink").text(menuValues.dateFrom.toString());
        dateFrom = menuValues.dateFrom;
    }
    if (menuValues.dateTo != null) {
        jQuery("#dateToLink").text(menuValues.dateTo.toString());
        dateTo = menuValues.dateTo;
    }
    if (menuValues.guests != null) {
        jQuery("#guestsCount").val(menuValues.guests.toString());
        guests = menuValues.guests;
    }
    if (menuValues.bedrooms != null) {
        jQuery("#bedroomsCount").val(menuValues.bedrooms.toString());
        bedrooms = menuValues.bedrooms;
    }
    if (menuValues.bathrooms != null) {
        jQuery("#bathroomsCount").val(menuValues.bathrooms.toString());
        bathrooms = menuValues.bathrooms;
    }
    if (menuValues.propertyTypes != null) {
        menuValues.propertyTypes = menuValues.propertyTypes.replace(new RegExp('%3b', 'g'), ';');
        propertyTypes = menuValues.propertyTypes;
        setCheckboxSelected(menuValues.propertyTypes, 'property');
    }
    if (menuValues.experiences != null) {
        menuValues.experiences = menuValues.experiences.replace(new RegExp('%3b', 'g'), ';');
        experiences = menuValues.experiences;
        setCheckboxSelected(menuValues.experiences, 'experience');
    }
    if (menuValues.amenities != null) {
        menuValues.amenities = menuValues.amenities.replace(new RegExp('%3b', 'g'), ';');
        amenities = menuValues.amenities;
        setCheckboxSelected(menuValues.amenities, 'amenity');
    }   
}

function setCheckboxSelected(ids, elemId) {
    ids = ids.split(';');
    for (i = 0; i < ids.length; i++) {
        if (ids[i] != '')
            jQuery("#" + elemId + '-' + ids[i]).attr('checked', true);
    }
}

function searchRequest()
{
    //var searchPathUrl = "/Home/Search?"
    var searchPathUrl = "/Home/Search?"
    var searchParams = "";
    if (destination.length != 0) {
        searchParams += "destination=" + destination + "&";
    }
    if (dateFrom.length != 0) {
        searchParams += "dateFrom=" + dateFrom + "&";
    }
    if (dateTo.length != 0) {
        searchParams += "dateTo=" + dateTo + "&";
    }
    if (guests.length != 0) {
        searchParams += "guests=" + guests + "&";
    }
    if (bedrooms.length != 0) {
        searchParams += "bedrooms=" + bedrooms + "&";
    }
    if (bathrooms.length != 0) {
        searchParams += "bathrooms=" + bathrooms + "&";
    }
    if (propertyTypes.length != 0) {
        searchParams += "propertyTypes=" + propertyTypes + "&";
    }
    if (amenities.length != 0) {
        searchParams += "amenities=" + amenities + "&";
    }
    if (experiences.length != 0) {
        searchParams += "experiences=" + experiences + "&";
    }
    if ((dateFrom == "" && dateTo == "") || (dateFrom != "" && dateTo != "")) {
        jQuery.ajax({
            url: searchPathUrl + searchParams,
            success: function (data, textStatus, jqXHR) {
                jQuery('#flexsliderContainer').html(data);
                jQuery('.flexslider').flexslider();
                location.hash = searchParams;
            }
        });
    }
}

function parseHashUrl(aURL){

    aURL = aURL || window.location.href;
    if((aURL.indexOf('#') == -1) || (aURL.length -1) == (aURL.indexOf('#')))
        return null;
        
    var vars = {};
    var hashes = aURL.slice(aURL.indexOf('#') + 1).split('&');

    for(var i = 0; i < hashes.length; i++) {
        var hash = hashes[i].split('=');
      
        if(hash.length > 1) {
            vars[hash[0]] = hash[1];
        }
    }
    return vars;
}


jQuery.noConflict();

jQuery(document).ready(function () {
    
    var queryString = parseHashUrl();
    if (queryString != null) {
        //set menu values
        setMenuValues(queryString);

        //search and display results
        jQuery.ajax({
            url: "/Home/Search?" + parseHashUrl(),
            success: function (data, textStatus, jqXHR) {
                jQuery('#flexsliderContainer').html(data);
                //var domData = generateSlides(data);
                //jQuery('#flexsliderContainer').empty();
                //domData.appendTo('#flexsliderContainer');
                jQuery('.flexslider').flexslider();
            }
        });
    }
    //sample search result
    else {
        jQuery.ajax({
            url: "/Home/GetSampleResult",
            success: function (data, textStatus, jqXHR) {
                jQuery('#flexsliderContainer').html(data);
                //var domData = generateSlides(data);
                //jQuery('#flexsliderContainer').empty();
                //domData.appendTo('#flexsliderContainer');
                jQuery('.flexslider').flexslider();
            }
        });
    }

    jQuery("#joinFormClose").click(function () {
        jQuery("#joinBlockPopup").hide(1);
    });

    jQuery('#flexsliderContainer, nav').click(function (sender, event) {
        jQuery("#banner > nav > ul > li > div").hide("slow");
        jQuery("#banner > nav > ul > li > ul").hide("slow");
    });
    jQuery("#localisationPhrase").autocomplete(
    {
        source: 'Home/FindDestination',
        select: function (event, ui) {
            jQuery("#destinationLink").text(ui.item.value);
            jQuery("#destinationLink").siblings("div").toggle("slow");
            destination = ui.item.id;
            searchRequest();
        },
        minLength: 2
    });

    jQuery("a#destinationLink").siblings("div").find("a").click(function () {
        jQuery("#destinationLink").text(jQuery(this).text());
        jQuery("#destinationLink").siblings("div").toggle("slow");
        destination = jQuery(this).attr("id").replace("destination-", "");
        searchRequest();
    });

    /*TODO FIX THIS */
    jQuery("a#slideshowView").click(function() {
        window.location = jQuery(this).attr("href");
    });

    jQuery("a#listView").click(function () {
        window.location = jQuery(this).attr("href");
    });
    /**************/


    jQuery("a#refineSearchLink").click(function (event, ui)
    {
        if (jQuery("a#refineSearchLink").siblings("ul").css("display") != "none") {
            propertyTypes = "";
            amenities = "";
            experiences = "";
            guests = jQuery("a#refineSearchLink").siblings("ul").find("#guestsCount")[0].value;
            bathrooms = jQuery("a#refineSearchLink").siblings("ul").find("#bathroomsCount")[0].value;
            bedrooms = jQuery("a#refineSearchLink").siblings("ul").find("#bedroomsCount")[0].value;

            jQuery('#amenitiesList').find('input[type=checkbox]:checked').each(function () {
                amenities += jQuery(this).attr("id").replace("amenity-", "") + ";";                
            });
            jQuery('#experienceList').find('input[type=checkbox]:checked').each(function () {
                experiences += jQuery(this).attr("id").replace("experience-", "") + ";";
            });
            jQuery('#propertyList').find('input[type=checkbox]:checked').each(function () {
                propertyTypes += jQuery(this).attr("id").replace("property-", "") + ";";
            });
            searchRequest();
        }
    });

    jQuery("#banner > nav > ul > li > div").click(function () { return false; });

    jQuery("a#showLoginLink").click(function () {
        jQuery("div#loginBlockPopup").toggle("slow");
        jQuery("ul#aboutUsList").hide("slow");
        jQuery("div#joinBlockPopup").hide("slow");
    });

    jQuery("a#joinLink").click(function () {
        jQuery("div#joinBlockPopup").toggle("slow");
        jQuery("div#loginBlockPopup").hide("slow");
    })

    jQuery("a#showAboutUsLink").click(function () {
        jQuery("ul#aboutUsList").toggle("slow");
        jQuery("div#loginBlockPopup").hide("slow");
        //jQuery("ul#guestsMenu").hide("slow");
        jQuery("div#joinBlockPopup").hide("slow");
    });

    //jQuery("a#showGuestsMenu").click(function () {
    //    jQuery("ul#guestsMenu").toggle("slow");
    //    jQuery("ul#aboutUsList").hide("slow");
    //    jQuery("div#joinBlockPopup").hide("slow");
    //});

    jQuery("#banner nav li").click(function () {
            jQuery(this).children("ul").toggle("slow");
            jQuery(this).children("div").toggle("slow");

            //http://www.eyecon.ro/datepicker/
            if (jQuery(this).find(".datepicker").length==0) {
                jQuery(this).find('#datepickerFrom').DatePickerNavigation({
                    flat: true,
                    date: '2012-07-31',
                    current: '2012-07-31',
                    onChange: function (dateSelected) {
                        dateFrom = dateSelected;
                        jQuery("a#dateFromLink").text(dateSelected);
                        jQuery("a#dateFromLink").siblings("div").toggle("slow");
                        searchRequest();
                    },
                    calendars: 3,
                    starts: 0
                });
            
                jQuery(this).find('#datepickerTo').DatePickerNavigation({
                    flat: true,
                    date: '2012-07-31',
                    current: '2012-07-31',
                    onChange: function (dateSelected) {
                        dateTo = dateSelected;
                        jQuery("a#dateToLink").text(dateSelected);
                        jQuery("a#dateToLink").siblings("div").toggle("slow");
                        searchRequest();
                    },
                    calendars: 1,
                    starts: 0
                });
            }
            return false;
    });

    jQuery("#banner nav input").click(function (event) {
        event.stopPropagation();
    });


    elem = jQuery("#index");
	if (elem.is (".home")) {
		jQuery(document).ready(function () {
		    jQuery("#flexslider").queryLoader2({
		        completeAnimation: "fade"
		    });
		});
	};

	/*
	window.addEventListener('DOMContentLoaded',function() {
	    elem = jQuery("#index");
		if (elem.is (".home")) {
			jQuery(document).ready(function () {
			    jQuery("#flexslider").queryLoader2({
			        completeAnimation: "fade"
			    });
			});
		};
	});
	*/
	

	//menu		
	jQuery("#banner nav").menuNav({modify_position:true});
	
	// improves menu for mobile devices
	jQuery('#banner nav ul:eq(0)').mobileMenu({
	  switchWidth: 768,                   							//width (in px to switch at)
	  topOptionText: jQuery('#banner nav').data('selectname'),     	//first option text
	  indentString: '&nbsp;&nbsp;&nbsp;'  							//string for indenting nested items
	});
	
	jQuery('.flexslider').flexslider();

	jQuery('.settings .openclose').click(function()
    {
        var target = jQuery(this).parent().parent('.settings');
        if(target.is('.display_switch'))
        {
            target.animate({top: "-61"}, function()
            {
                target.removeClass('display_switch').addClass('display_settings_false');
            });
        }
        else
        {
            target.animate({top: "20"}, function()
            {
                target.removeClass('display_settings_false').addClass('display_switch');
            });
        }
    });

    jQuery("#audioBG").jPlayer({
		ready : function () {
		    jQuery(this).jPlayer("setMedia", {
		        mp3 : "Sounds/bubble.mp3",
		    });
		},
		ended : function (event) {
				jQuery(this).jPlayer("play");
		},
		swfPath : "js/"
	});
					

	jQuery("#pause_bt").click(function() {
		jQuery("#audioBG").jPlayer("play");
		jQuery(this).hide();
		jQuery("#play_bt").show();
	});
				
	jQuery("#play_bt").click(function() {
		jQuery("#audioBG").jPlayer("pause");
		jQuery(this).hide();
		jQuery("#pause_bt").show();
	});
	
	jQuery(document).ready(function(){
		jQuery("area[rel^='prettyPhoto']").prettyPhoto();

		jQuery("a[rel^='prettyPhoto']").prettyPhoto();
				
		jQuery(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: true});
		jQuery(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
		
	});			
	
	
	// jQuery('.widget blockquote').quovolver();
	
	/* Flickr setup */
	/* jQuery('#flickr_widget').jflickrfeed({
		limit: 6,
		qstrings: {
			id: '73818667@N00'
		},
		itemTemplate: '<li><a class="flickr_images image" href="{{image_b}}"><img src="{{image_s}}" alt="{{title}}" /></a></li>'
	}, function(data) {
		jQuery('#flickr_widget a').fancybox();
		jQuery("#flickr_widget li img").css("opacity","1.0");
		jQuery("#flickr_widget li img").hover(function () {
			jQuery(this).stop().animate({ opacity: 0.5 }, "fast");  },
			function () {
			jQuery(this).stop().animate({ opacity: 1.0 }, "slow");  
		}); 
	});
	*/

	//When page loads...
	jQuery(".pane").hide(); //Hide all content
	jQuery("ul.tabs li:first").addClass("active").show(); //Activate first tab
	jQuery(".pane:first").show(); //Show first tab content



	//On Click Event
	jQuery("ul.tabs li").click(function() {

		jQuery("ul.tabs li").removeClass("active"); //Remove any "active" class
		jQuery(this).addClass("active"); //Add "active" class to selected tab

	    return false;
	});
	
	// setup ul.tabs to work as tabs for each div directly under div.panes
	jQuery("ul.tabs").tabs("div.panes > div");
	
	// scroll body to 0px on click
	jQuery(".backtop a").click(function () {
		jQuery("body,html").animate({
			scrollTop: 0
		}, 800);
	    return false;
	});	
	
	

	//Toggle
	jQuery(".togglebox").hide();
	//slide up and down when click over heading 2
	
	jQuery("h4").click(function(){
	// slide toggle effect set to slow you can set it to fast too.
	jQuery(this).toggleClass("active").next(".togglebox").slideToggle("slow");
	
	return true;
	
	});

	jQuery(function() {
	    // setup ul.tabs to work as tabs for each div directly under div.panes
	    jQuery("ul.tabs").tabs("div.panes > div");
	});


	// Image Preloader
	/*jQuery(function() {
		jQuery('.portfolio-image img').hide();
		jQuery('.portfolio-image img').parent().parent().addClass('image-preloading');
	});

	jQuery(window).bind('load', function() {
		var i = 1;
		var imgs = jQuery('img').length;
		var int = setInterval(function() { 
			//console.log(i); check to make sure interval properly stops
			if(i >= imgs) clearInterval(int);
			jQuery('img:hidden').eq(0).fadeIn(500, function(){
				jQuery('.portfolio-image img').parent().parent().removeClass('image-preloading');
			});
			i++;
		});	
	});
	*/
	
});


/*-----------------------------------------------------------------------------------*/
/* Menu
/*-----------------------------------------------------------------------------------*/

jQuery.fn.menuNav = function(variables) 
{
	var defaults = 
	{
		modify_position:true,
		delay: 300
	};
		
	var options = jQuery.extend(defaults, variables);    
		
	return this.each(function()
	{
			
		var menu = jQuery(this),
			menuItems = menu.find(">li"),
			dropdownItems = menuItems.find(">ul").parent(),
			parentContainerWidth = menu.parent().width(),
			delayCheck = {};
			
		dropdownItems.find('li').andSelf().each(function()
		{	
			var currentItem = jQuery(this),
				sublist = currentItem.find('ul:first'),
				showList = false;
				
			if(sublist.length) 
			{ 
				sublist.css({display:'block', opacity:0, visibility:'hidden'}); 
				var currentLink = currentItem.find('>a');
					
				currentLink.bind('mouseenter', function()
				{
					sublist.stop().css({visibility:'visible'}).animate({opacity:1});
				});
					
				currentItem.bind('mouseleave', function()
				{
					sublist.stop().animate({opacity:0}, function()
					{
						sublist.css({visibility:'hidden'});
					});
				});

			}
		
		});
			
	});
};


(function($){

  //variable for storing the menu count when no ID is present
  var menuCount = 0;
  
  //plugin code
  $.fn.mobileMenu = function(options){
    
    //plugin's default options
    var settings = {
      switchWidth: 768,
      topOptionText: 'Navigate to...',
      indentString: '&nbsp;&nbsp;&nbsp;'
    };
    
    
    //function to check if selector matches a list
    function isList($this){
      return $this.is('ul, ol');
    }
  
  
    //function to decide if mobile or not
    function isMobile(){
      return ($(window).width() < settings.switchWidth);
    }
    
    
    //check if dropdown exists for the current element
    function menuExists($this){
      
      //if the list has an ID, use it to give the menu an ID
      if($this.attr('id')){
        return ($('#mobileMenu-'+$this.attr('id')).length > 0);
      } 
      
      //otherwise, give the list and select elements a generated ID
      else {
        menuCount++;
        $this.attr('id', 'mobile-menu-'+menuCount);
        return ($('#mobileMenu-mobile-menu-'+menuCount).length > 0);
      }
    }
    
    
    //change page on mobile menu selection
    function goToPage($this){
      if($this.val() !== null){document.location.href = $this.val()}
    }
    
    
    //show the mobile menu
    function showMenu($this){
      $this.css('display', 'none');
      $('#mobileMenu-'+$this.attr('id')).show();
    }
    
    
    //hide the mobile menu
    function hideMenu($this){
      $this.css('display', '');
      $('#mobileMenu-'+$this.attr('id')).hide();
    }
    
    
    //create the mobile menu
    function createMenu($this){
      if(isList($this)){
                
        //generate select element as a string to append via jQuery
        var selectString = '<select id="mobileMenu-'+$this.attr('id')+'" class="mobileMenu">';
        
        //create first option (no value)
        selectString += '<option value="">'+settings.topOptionText+'</option>';
        
        //loop through list items
        $this.find('li').each(function(){
          
          //when sub-item, indent
          var levelStr = '';
          var len = $(this).parents('ul, ol').length;
          for(i=1;i<len;i++){levelStr += settings.indentString;}
          
          //get url and text for option
          var link = $(this).find('a:first-child').attr('href');
          var text = levelStr + $(this).clone().children('ul, ol').remove().end().text();
          
          //add option
          selectString += '<option value="'+link+'">'+text+'</option>';
        });
        
        selectString += '</select>';
        
        //append select element to ul/ol's container
        $this.parent().append(selectString);
        
        //add change event handler for mobile menu
        $('#mobileMenu-'+$this.attr('id')).change(function(){
          goToPage($(this));
        });
        
        //hide current menu, show mobile menu
        showMenu($this);
      } else {
        alert('mobileMenu will only work with UL or OL elements!');
      }
    }
    
    
    //plugin functionality
    function run($this){
      
      //menu doesn't exist
      if(isMobile() && !menuExists($this)){
        createMenu($this);
      }
      
      //menu already exists
      else if(isMobile() && menuExists($this)){
        showMenu($this);
      }
      
      //not mobile browser
      else if(!isMobile() && menuExists($this)){
        hideMenu($this);
      }

    }
    
    //run plugin on each matched ul/ol
    //maintain chainability by returning "this"
    return this.each(function() {
      
      //override the default settings if user provides some
      if(options){$.extend(settings, options);}
      
      //cache "this"
      var $this = $(this);
    
      //bind event to browser resize
      $(window).resize(function(){run($this);});

      //run plugin
      run($this);

    });
    
  };
  
})(jQuery);