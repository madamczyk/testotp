﻿//Prevents double submit of forms with css class: "single-submit-form"
//usage: jQuery('form').preventDoubleSubmit();

jQuery.fn.preventDoubleSubmit = function () {
    jQuery(this).submit(function () {
        if (jQuery(this).hasClass("single-submit-form") && jQuery(this).valid()) {
            if (this.beenSubmitted)
                return false;
            else
                this.beenSubmitted = true;
        }
    });
};
