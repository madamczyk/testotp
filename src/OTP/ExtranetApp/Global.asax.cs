﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using Microsoft.WindowsAzure.ServiceRuntime;
using ModelMetadataExtensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Objects;
using Common.Exceptions;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using BusinessLogic.Enums;

namespace ExtranetApp
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Page not found error code
        /// </summary>
        private const int ERROR_404 = 404;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //localization mechanism
            ModelMetadataProviders.Current = new ConventionalModelMetadataProvider(
                requireConventionAttribute: true
            );

            //Init Global Cache for Application
            IStateService stateService = (IStateService)DependencyResolver.Current.GetService(typeof(IStateService));
            stateService.InitCache();
        }

        protected void Application_EndRequest()
        {
            RequestManager.SaveChanges();
        }

        void Application_Error(object sender, EventArgs e)
        {            
            IEventLogService eventLogService = (IEventLogService)DependencyResolver.Current.GetService(typeof(IEventLogService));
            ((BaseService)eventLogService).DisposeContext();
            Exception exception = Server.GetLastError();

            if (exception != null && exception is HttpException)
            {                
                string errorMessage = ExceptionHelper.FormatErrorMessage(exception);
                try
                {
                    HttpException httpException = (HttpException)exception;
                    if (httpException.GetHttpCode().Equals(ERROR_404)) // log only as warning on page not found error (404)
                    {
                        Trace.Write(errorMessage, EventCategory.Warning.ToString());
                    }
                    else
                    {                        
                        Trace.Write(errorMessage, EventCategory.Error.ToString());
                    }
                    
                }
                catch (Exception)
                {
                    eventLogService.LogError(exception.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.Extranet);
                }                
            }                 
        }
    }
}