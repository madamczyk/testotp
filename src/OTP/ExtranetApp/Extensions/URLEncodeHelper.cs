﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Extensions
{
    public static class URLEncodeHelper
    {
        public static string ToEncodedUrl(this UrlHelper helper, string urlToEncode)
        {
            urlToEncode = (urlToEncode ?? "").Trim().ToLower();
            urlToEncode = Regex.Replace(urlToEncode, @"[^a-z0-9]", "-"); // invalid chars
            urlToEncode = Regex.Replace(urlToEncode, @"-+", "-").Trim(); // convert multiple dashes into one
            return urlToEncode;
        }

    }
}