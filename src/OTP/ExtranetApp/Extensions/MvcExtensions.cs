﻿using DataAccess.Enums;
using ExtranetApp.Resources.Views;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Extensions
{
    public static class MvcExtensions
    {
        /// <summary>
        /// Removes all trailing characters if text has more characters than maxLenght parameter.
        /// Adds indicator ('...') to the end of text if it was longer than max length
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="text"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static MvcHtmlString ToShortText(this HtmlHelper helper, string text, int maxLength)
        {
            if (text.Length > maxLength)
            {
                text = text.Substring(0, maxLength).Trim();
                int lastSpaceChar = text.LastIndexOf(" ");
                text = text.Substring(0, lastSpaceChar);
                text = text + Shared.TooLongTextSuffix;
            }
            return new MvcHtmlString(text);
        }

        /// <summary>
        /// Returns base64 image from 
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="bitmap"></param>
        /// <param name="AltText"></param>
        /// <returns></returns>
        public static MvcHtmlString GetĄmenityIcon(this HtmlHelper helper, int iconId, string AltText = "")
        {
            string fileName = ((AmenityIcon)iconId).ToString();
            ResourceManager rm = new ResourceManager("ExtranetApp.Resources.AmenityGroupIcons", Assembly.GetExecutingAssembly());

            var resObj = rm.GetObject(fileName);
            if (resObj != null)
            {
                MemoryStream stream = new MemoryStream();
                (rm.GetObject(fileName) as Bitmap).Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Seek(0, SeekOrigin.Begin);
                string base64 = Convert.ToBase64String(stream.ToArray());
                return new MvcHtmlString(
                string.Format("<img src='data:image/gif;base64,{0}' alt='{1}' />",
                    base64, AltText));
            }
            return new MvcHtmlString(string.Empty);
        }

    }
}