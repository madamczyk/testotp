﻿using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtranetApp.Extensions
{
    /// <summary>
    /// Custom MVC validation attribute
    /// Used in: HomeOwner Model
    /// </summary>
    public class ExistingUserAttribute : ValidationAttribute
    {
        public string Email { get; set; }

        [Inject]
        public IUserService UserService
        {
            get; set;
        }      

        /// <summary>
        /// Validate if given email already has Owner role
        /// </summary>
        /// <param name="value">Email adress</param>
        /// <returns>True- email has not Owner role; Otherwise false</returns>
        public override bool IsValid(object value)
        {
            User user = null;
            UserRole userRole = null;
            user = this.UserService.GetUserByEmail(value.ToString());
            if (user != null)
            {
                userRole = this.UserService.GetUserRoleByLevel(user, RoleLevel.Owner);
                if (userRole != null)
                {
                    return false;
                }
            }
            return true;
        }
    }
}