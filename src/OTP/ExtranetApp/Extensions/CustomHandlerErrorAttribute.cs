﻿using BusinessLogic.Managers;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using Common.Exceptions;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Extensions
{
    public class CustomHandlerErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            IEventLogService eventLogService = (IEventLogService)DependencyResolver.Current.GetService(typeof(IEventLogService));
            ((BaseService)eventLogService).DisposeContext();
            Exception exception = filterContext.Exception;
            string errorMessage = ExceptionHelper.FormatErrorMessage(exception);
            System.Diagnostics.Trace.Write(errorMessage, EventCategory.Error.ToString());
            
            base.OnException(filterContext);

            RequestManager.SaveChanges();
        }
    }
}