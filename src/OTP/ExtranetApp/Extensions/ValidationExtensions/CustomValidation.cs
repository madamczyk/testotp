﻿using BusinessLogic.ServiceContracts;
using Common.Validation;
using DataAccess;
using DataAccess.Enums;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtranetApp.Extensions
{
    /// <summary>
    /// Custom MVC validation attribute
    /// Used in: HomeOwner Model
    /// </summary>
    public class ExistingUserAttribute : ValidationAttribute
    {
        public string Email { get; set; }

        [Inject]
        public IUserService UserService
        {
            get; set;
        }      

        /// <summary>
        /// Validate if given email already has Owner role
        /// </summary>
        /// <param name="value">Email adress</param>
        /// <returns>True- email has not Owner role; Otherwise false</returns>
        public override bool IsValid(object value)
        {
            User user = null;
            UserRole userRole = null;
            user = this.UserService.GetUserByEmail(value.ToString());
            if (user != null)
            {
                userRole = this.UserService.GetUserRoleByLevel(user, RoleLevel.Owner);
                if (userRole != null)
                {
                    return false;
                }
            }
            return true;
        }
    }

    /// <summary>
    /// Custom MVC validation attribute
    /// Used in: HomeOwner Model
    /// </summary>
    public class RoutingNumberChecksumAttribute : ValidationAttribute
    {
        public string RoutingNumber { get; set; }

        /// <summary>
        /// Validate given routing number, that is: 9 characters long, only numbers, has a valid checksum
        /// </summary>
        /// <param name="value">Routing number</param>
        /// <returns>Result of validation</returns>
        public override bool IsValid(object value)
        {
            string numberStr = (string)value;

            return BankData.ValidateRoutingNumber(numberStr);
        }
    }
}