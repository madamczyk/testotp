﻿using BusinessLogic.ServiceContracts;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Extensions
{
    public class AgeValidation : ValidationAttribute, IClientValidatable
    {
        [Inject]
        public IUserService UserService
        {
            get;
            set;
        }

        [Inject]
        public IStateService StateService
        {
            get;
            set;
        }  

        public override bool IsValid(object value)
        {
            var isValid = false;

            var user = this.UserService.GetUserByUserId(StateService.CurrentUser.UserID);

            var timeDiff = user.BirthDate.Value.AddYears(25);

            if (DateTime.Now > timeDiff)
                isValid = true;


            return isValid;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.DisplayName),
                ValidationType = "agevalidation"
            };
        }
    }
}