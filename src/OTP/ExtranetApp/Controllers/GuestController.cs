﻿
using BusinessLogic.Objects;
using BusinessLogic.Objects.Session;
using BusinessLogic.Objects.StaticContentRepresentation;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.PaymentGateway;
using BusinessLogic.ServiceContracts;
using Common.Exceptions;
using Common.Exceptions.ReservationDateChange;
using DataAccess;
using DataAccess.Enums;
using ExtranetApp.Models.Guest;
using ExtranetApp.Resources.Booking;
using ExtranetApp.Resources.Guest;
using ExtranetApp.Resources.Views;
using ExtranetApp.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Controllers
{
    [Authorize(Roles = "Guest")]
    public class GuestController : BaseController
    {
        public const string CANCEL_TRIP_ERROR_MESSAGE = "CANCEL_TRIP_ERROR_MESSAGE";
        public const string DELETE_CARD_ERROR_MESSAGE = "DELETE_CARD_ERROR_MESSAGE";


        public GuestController(IUserService userService, IContextService httpContextService,
            IBookingService bookingService, IStateService stateService, IDictionaryCountryService dictionaryService, IDictionaryCultureService dictionaryCultureService, IReservationsService reservationService, IPropertiesService propertyService, IConfigurationService configurationService, IUnitsService unitsService, IPaymentGatewayService paymentService, IMessageService messageService)
            : base(new Dictionary<Type, object>() {
            { typeof(IStateService), stateService },            
            { typeof(IContextService), httpContextService },
            { typeof(IBookingService), bookingService },
            { typeof(IUserService), userService },
            { typeof(IDictionaryCountryService), dictionaryService},
            { typeof(IDictionaryCultureService), dictionaryCultureService},
            { typeof(IReservationsService), reservationService},
            { typeof(IPropertiesService), propertyService},
            { typeof(IConfigurationService), configurationService},
            { typeof(IUnitsService), unitsService },
            { typeof(IPaymentGatewayService), paymentService},
            { typeof(IMessageService), messageService}
            })
        {
        }


        public new ActionResult Profile()
        {
            if (StateService.IsAuthenticated())
            {
                SessionUser sessionUsr = this.StateService.CurrentUser;
                var user = this.UserService.GetUserByUserId(sessionUsr.UserID);
                GuestProfileModel guestModel = GetProfileModel(user);
                ViewBag.Countries = new SelectList(this.DictionaryCountryService.GetCountries(), "CountryId", "CountryNameCurrentLanguage");
                ViewBag.Cultures = new SelectList(this.DictionaryCultureService.GetCultures(), "CultureId", "CultureNameCurrentLanguage");
                ViewBag.CardDeleteErrorMessage = TempData[DELETE_CARD_ERROR_MESSAGE];
                return View(guestModel);
            }
            else
                return RedirectToAction("Index", "Home");
        }


        [HttpPost]
        public new ActionResult Profile(GuestProfileModel model)
        {
            User usr = UpdateUserData(model);
            return RedirectToAction("Profile");
        }

        private User UpdateUserData(GuestProfileModel up)
        {
            var user = this.UserService.GetUserByUserId(up.UserId);
            user.Address1 = up.AddressLine1;
            user.CellPhone = up.PhoneNumber;
            user.City = up.City;
            user.Firstname = up.FirstName;
            user.Lastname = up.LastName;
            user.State = up.State;
            user.ZIPCode = up.ZIPCode;
            user.DictionaryCountry = this.DictionaryCountryService.GetCountryById(up.Country);
            return user;
        }

        [HttpPost]
        public ActionResult UpdateLanguage(GuestProfileModel up)
        {
            var user = this.UserService.GetUserByUserId(up.UserId);
            user.DictionaryCulture = this.DictionaryCultureService.GetCultureById(up.UserCulture);
            return RedirectToAction("Profile");
        }

        [HttpPost]
        public ActionResult UpdateEmailPreferences(GuestProfileModel up)
        {
            var user = this.UserService.GetUserByUserId(up.UserId);
            user.SendInfoFavoritePlaces = up.SendInfoFavoritePlaces;
            user.SendMePromotions = up.SendMePromotions;
            user.SendNews = up.SendNews;
            return RedirectToAction("Profile");
        }

        [IsGuestRelated(GuestRelationType.Reservation, "resId")]
        public ActionResult ChangeTripDate(int resId)
        {

            SessionUser sessionUsr = this.StateService.CurrentUser;
            User user = UserService.GetUserByUserId(sessionUsr.UserID);
            Reservation res = user.Reservations.Where(r => r.ReservationID == resId).FirstOrDefault();
            if (res != null)
            {
                ChangeTripDateModel model = new ChangeTripDateModel()
                {
                    reservation = res,
                    reservationId = resId,
                    user = user,
                    DateArrival = res.DateArrival,
                    DateDeparture = res.DateDeparture
                };
                return View(model);
            }
            else
                return RedirectToAction("MyTrips");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeTripDate(ChangeTripDateModel model)
        {
            if (StateService.IsAuthenticated())
            {
                string errorMessage = string.Empty;

                try
                {
                    this.ReservationService.ChangeReservationDate(model.reservationId, model.DateArrival, model.DateDeparture);
                    return RedirectToAction("MyTrips");
                }
                catch (PaymentGatewayException)
                {
                    ViewBag.ErrorMessage = Resources.Guest.ChangeTripDate.PaymentGatewayError;
                }
                catch (ResvDateChangeDateArrivalException)
                {
                    ViewBag.ErrorMessage = Resources.Guest.ChangeTripDate.ArrivalDateError;
                }
                catch (ResvDateChangeUnavailableException)
                {
                    ViewBag.ErrorMessage = Resources.Guest.ChangeTripDate.UnitNotAvailableError;
                }
                catch (ResvDateChangeExistingChildException)
                {
                    ViewBag.ErrorMessage = Resources.Guest.ChangeTripDate.ExistingChildException;
                }
                catch (ResvDateChangeNoChangeException)
                {
                    ViewBag.ErrorMessage = Resources.Guest.ChangeTripDate.NoChangeMadeError;
                }

                SessionUser sessionUsr = this.StateService.CurrentUser;
                var user = this.UserService.GetUserByUserId(sessionUsr.UserID);
                Reservation res = user.Reservations.Where(r => r.ReservationID == model.reservationId).FirstOrDefault();
                model.reservation = res;
                model.user = user;
                model.DateArrival = res.DateArrival;
                model.DateDeparture = res.DateDeparture;

                return View("ChangeTripDate", model);

            }
            else
                return RedirectToAction("Index", "Home");
        }

        public PartialViewResult GetPriceAndInvoiceDetails(int unitId, int propertyId, DateTime dateFrom, DateTime dateUntil, decimal totalPrice)
        {
            InvoiceDetailsModel model = new InvoiceDetailsModel()
            {
                invoiceDetail = this.BookingService.CalculateInvoice(propertyId, unitId, dateFrom, dateUntil),
                originalReservationTotalPrice = totalPrice
            };



            return PartialView("Partial/ChangeDate_InvoiceWithDetails", model);
        }

        [HttpPost]
        public ActionResult DeleteCreditCard(GuestProfileModel up)
        {
            SessionUser sessionUsr = this.StateService.CurrentUser;
            User user = UserService.GetUserByUserId(sessionUsr.UserID);
            Reservation res = user.Reservations.Where(r => (r.GuestPaymentMethod != null && r.GuestPaymentMethod.GuestPaymentMethodID == up.CreditCardToDelete)).FirstOrDefault();
            if (res != null)
            {
                if (res.BookingStatus == (int)BookingStatus.CheckedOut)
                {
                    try
                    {
                        TransactionResponse result = this.PaymentGatewayService.RetainPaymentMethod(res.GuestPaymentMethod.ReferenceToken, res.ReservationID);
                        if (result.Succeded)
                        {
                            this.UserService.RemoveGuestPaymentMethod(up.CreditCardToDelete, sessionUsr.UserID);
                        }
                        else
                        {
                            TempData[DELETE_CARD_ERROR_MESSAGE] = ExtranetApp.Resources.Guest.Profile.deleteCreditCardError;
                            return RedirectToAction("Profile");
                        }

                    }
                    catch (PaymentGatewayException e)
                    {
                        TempData[DELETE_CARD_ERROR_MESSAGE] = e.Message;
                        return RedirectToAction("Profile");
                    }
                }
                else
                    TempData[DELETE_CARD_ERROR_MESSAGE] = ExtranetApp.Resources.Guest.Profile.deleteCreditCardInUse;
            }
            return RedirectToAction("Profile");
        }

        public ActionResult Mytrips()
        {
            if (StateService.IsAuthenticated())
            {
                SessionUser sessionUsr = this.StateService.CurrentUser;
                var user = this.UserService.GetUserByUserId(sessionUsr.UserID);


                MyTripsModel model = new MyTripsModel()
                {
                    User = user,
                    reservations = this.ReservationService.GetReservationsByUserId(sessionUsr.UserID)
                };
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");


        }

        [IsGuestRelated(GuestRelationType.Reservation, "ReservationId")]
        public ActionResult TripDetails(int ReservationId)
        {
            if (StateService.IsAuthenticated())
            {
                SessionUser sessionUsr = this.StateService.CurrentUser;
                var user = this.UserService.GetUserByUserId(sessionUsr.UserID);
                Reservation reservation = ReservationService.GetReservationById(ReservationId);
                ViewBag.MostRestrictiveArrivalDate = this.ReservationService.FindMostRestrictiveArrivalDateForComparision(reservation, reservation.DateArrival);
                Reservation mostExpensiveReservation = this.ReservationService.FindMostExpensiveReservation(reservation);
                int keyProcess = reservation.Property.KeyProcessType;
                User keyManager = null;
                string lockBoxCode = null;
                string lockBoxLocation = null;
                string alarmCode = null;
                string alarmLocation = null;

                TimeSpan lengthOfStay = reservation.DateDeparture - reservation.DateArrival;
                var price = this.UnitsService.GetUnitPriceAccomodation(reservation.Unit.UnitID, reservation.DateArrival, reservation.DateDeparture);
                decimal PricePerNight = (price ?? 0) / lengthOfStay.Days;

                if (keyProcess == (int)KeyProcessType.KeyHandler)
                {
                    keyManager = PropertiesService.GetPropertyManagerByType(reservation.Property.PropertyID, RoleLevel.KeyManager).User;
                }
                else if (keyProcess == (int)KeyProcessType.Lockbox)
                {
                    lockBoxCode = reservation.Unit.KeysLockboxCode;
                    lockBoxLocation = reservation.Unit.KeysLockboxLocation;
                    alarmCode = reservation.Unit.AlarmCode;
                    alarmLocation = reservation.Unit.AlarmLocation;
                }

                if (user.Reservations.Contains(reservation))
                {
                    TripDetailModel model = new TripDetailModel
                    {
                        Reservation = reservation,
                        ReservationId = ReservationId,
                        mostExpensiveReservation =mostExpensiveReservation,
                        User = reservation.User,
                        Directions = this.PropertiesService.GetPropertyDirectionsToAirport(reservation.Property.PropertyID),
                        Parkings = this.PropertiesService.GetPropertyStaticContentByType(reservation.Property.PropertyID, PropertyStaticContentType.ParkingInformation),
                        ArrivalInformations = this.PropertiesService.GetPropertyStaticContentByType(reservation.Property.PropertyID, PropertyStaticContentType.ArrivalInformation),
                        WifiName = this.UnitsService.GetUnitStaticContentByType(reservation.Unit.UnitID, UnitStaticContentType.WifiName),
                        WifiPass = this.UnitsService.GetUnitStaticContentByType(reservation.Unit.UnitID, UnitStaticContentType.WifiPassword),
                        LockBoxCode = lockBoxCode,
                        LockBoxLocation = lockBoxLocation,
                        KeyMenager = keyManager,
                        KeyProcess = keyProcess,
                        PricePerNight = PricePerNight,
                        LengthOfStay = lengthOfStay.Days,
                        AlertCode = alarmCode,
                        AlertLocation = alarmLocation
                    };

                    ViewBag.MostRestrictiveArrivalDate = this.ReservationService.FindMostRestrictiveArrivalDateForComparision(reservation, reservation.DateArrival);
                    return View(model);
                }
                else
                    return RedirectToAction("Mytrips");
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [IsGuestRelated(GuestRelationType.Reservation, "ReservationId")]
        public ActionResult CancelTrip(int ReservationId)
        {
            Reservation reservation = ReservationService.GetReservationById(ReservationId);
            int bookingStatusRecieved = (int)BookingStatus.ReceivedBooking;
            int bookingStatusFinalized = (int)BookingStatus.FinalizedBooking;


            /// Trip cannot be cancelled if status is different than below
            if (!reservation.BookingStatus.Equals(bookingStatusFinalized) && !reservation.BookingStatus.Equals(bookingStatusRecieved))
            {
                throw new TripNonCancellableException("Trip id: " + reservation.ReservationID + " cannot be cancelled if is in status: " + reservation.BookingStatus);
            }

            string blobContainter = this.ConfigurationService.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl);
            IEnumerable<PropertyStaticContent> thumbStaticContent = this.PropertiesService.GetPropertyStaticContentByType(reservation.Property.PropertyID, PropertyStaticContentType.Thumbnail);
            IEnumerable<String> thumbnails;
            if (thumbStaticContent != null)
            {
                thumbnails = thumbStaticContent.Select(thumb => string.Format("{0}{1}", blobContainter, thumb.ContentValue));
            }
            else
                thumbnails = new List<String>();

            CancelTripModel model = new CancelTripModel()
            {
                reservation = reservation,
                User = reservation.User,
                ReservationId = reservation.ReservationID,
                Thumbnails = thumbnails,
                errorMessage = (String)TempData[CANCEL_TRIP_ERROR_MESSAGE]
            };
            TempData[CANCEL_TRIP_ERROR_MESSAGE] = null;

            ViewBag.MostRestrictiveArrivalDate = this.ReservationService.FindMostRestrictiveArrivalDateForComparision(reservation, reservation.DateArrival);
            return View(model);
        }

        [HttpPost]
        public ActionResult CancelTripConfirmed(CancelTripModel model)
        {
            Reservation reservation = ReservationService.GetReservationById(model.ReservationId);
            int bookingStatusRecieved = (int)BookingStatus.ReceivedBooking;
            int bookingStatusFinalized = (int)BookingStatus.FinalizedBooking;

            /// Trip cannot be cancelled if status is different than below
            if (!reservation.BookingStatus.Equals(bookingStatusFinalized) && !reservation.BookingStatus.Equals(bookingStatusRecieved))
            {
                throw new TripNonCancellableException("Trip id: " + reservation.ReservationID + " cannot be cancelled if is in status: " + reservation.BookingStatus);
            }

            DateTime mostRestrictiveArrivalDate = this.ReservationService.FindMostRestrictiveArrivalDateForComparision(reservation, reservation.DateArrival);

            double diffrenceBetweenBookingAndNow = (DateTime.Now - reservation.BookingDate).TotalHours;
            double diffrenceBetweenArrivalAndNow = (mostRestrictiveArrivalDate - DateTime.Now).TotalDays;

            SessionUser sessionUsr = this.StateService.CurrentUser;
            var user = this.UserService.GetUserByUserId(sessionUsr.UserID);

            try
            {
                bool paymentOperationSucceded = true;

                /// Invoke payment gateway only when still booking status is Recieved (no money has been Purchased)
                if (reservation.BookingStatus.Equals(bookingStatusRecieved))
                {
                    TransactionResponse result = this.PaymentGatewayService.CancelPayment(reservation.TransactionToken, reservation.ReservationID);
                    paymentOperationSucceded = result.Succeded;
                }

                if (paymentOperationSucceded)
                {
                    this.UnitsService.DeleteUnitBlockageByReservationId(model.ReservationId);
                    reservation.BookingStatus = (int)BookingStatus.Cancelled;
                    ReservationCancelationConfirmationEmail guestCancelationEmail = new ReservationCancelationConfirmationEmail(user.email);

                    guestCancelationEmail.NameOfPlace = reservation.Property.PropertyNameCurrentLanguage;
                    guestCancelationEmail.FromDate = reservation.DateArrival.ToString("dd MMM yyyy");
                    guestCancelationEmail.ToDate = reservation.DateDeparture.ToString("dd MMM yyyy");
                    guestCancelationEmail.ConfirmationNumber = reservation.ConfirmationID;
                    guestCancelationEmail.UserFirstName = user.Firstname;

                    this.MessageService.AddEmail(guestCancelationEmail, reservation.Property.DictionaryCulture.CultureCode);
                }
                else
                {
                    TempData[CANCEL_TRIP_ERROR_MESSAGE] = MyTrips.cancellationErrorDefaultMessage;
                    return RedirectToAction("CancelTrip", model);
                }

            }
            catch (PaymentGatewayException e)
            {
                TempData[CANCEL_TRIP_ERROR_MESSAGE] = e.Message;
                return RedirectToAction("CancelTrip");
            }


            CancellationConfirmationModel modelCancellation = new CancellationConfirmationModel();

            if ((diffrenceBetweenBookingAndNow < 24) && (diffrenceBetweenArrivalAndNow > 15))
            {
                modelCancellation.message = MyTrips.cancellationConfirmationBefore24h;
            }
            else
                if (diffrenceBetweenArrivalAndNow > 30)
                {
                    modelCancellation.message = MyTrips.cancellationConfirmationAfter24hBefore30d;
                }
                else
                {
                    modelCancellation.message = MyTrips.cancellationConfirmationAfter24hAfter30d;
                }


            modelCancellation.cancellationCode = reservation.ConfirmationID;
            modelCancellation.userEmail = user.email;
            return View(modelCancellation);
        }

        public ActionResult OnSiteServices()
        {
            return View();
        }

        private GuestProfileModel GetProfileModel(User userData)
        {
            GuestProfileModel pm = new GuestProfileModel()
            {
                AddressLine1 = userData.Address1,
                City = userData.City,
                Country = userData.DictionaryCountry == null ? 0 : userData.DictionaryCountry.CountryId,
                Email = userData.email,
                FirstName = userData.Firstname,
                LastName = userData.Lastname,
                PhoneNumber = userData.CellPhone,
                State = userData.State,
                ZIPCode = userData.ZIPCode,
                CountryName = userData.DictionaryCountry == null ? String.Empty : userData.DictionaryCountry.CountryNameCurrentLanguage,
                UserId = userData.UserID,
                CreditCards = this.BookingService.GetUserCreditCardTypes(userData.UserID),
                FullName = userData.FullName,
                UserCultureName = userData.DictionaryCulture == null ? String.Empty : userData.DictionaryCulture.CultureNameCurrentLanguage,
                UserCulture = userData.DictionaryCulture.CultureId,
                SendMePromotions = userData.SendMePromotions.GetValueOrDefault(),
                SendInfoFavoritePlaces = userData.SendInfoFavoritePlaces.GetValueOrDefault(),
                SendNews = userData.SendNews.GetValueOrDefault(),
                CreditCardsInUse = userData.Reservations.Where(r => r.BookingStatus != (int)BookingStatus.CheckedOut && r.GuestPaymentMethod != null).Select(r => r.GuestPaymentMethod.GuestPaymentMethodID)
            };

            return pm;
        }

        public FileStreamResult GetInvoiceDocument(int id)
        {
            var reservation = this.ReservationService.GetReservationById(id);
            if (reservation != null && reservation.Invoice != null)
            {
                Stream file = new MemoryStream(reservation.Invoice);
                Response.AddHeader("Content-Disposition", "attachment; filename=" + BookingProcess.Invoice_FileName);
                return new FileStreamResult(file, "application/pdf");
            }
            return null;
        }

        [IsGuestRelated(GuestRelationType.Reservation, "rId")]
        public ActionResult ConfirmArrivalTime(int dId, string t, int rId)
        {
            ViewBag.Message = KeyProcessManagement.ConfirmationBody_Failed;
            Guid reservationToken;
            if (Guid.TryParse(t, out reservationToken))
            {
                var result = this.ReservationService.SetNewArrivalTimeForReservation(rId, t, dId);
                if (result.HasValue && result.Value)
                {
                    ViewBag.Message = string.Format(KeyProcessManagement.ConfirmationBody_Success, ArrivalTimeRangeExtensions.GetTextRepresentation((ArrivalTimeRange)(dId)));
                }
                else if (result.HasValue && !result.Value)
                {
                    ViewBag.Message = string.Format(KeyProcessManagement.ConfirmationBody_Replace, ArrivalTimeRangeExtensions.GetTextRepresentation((ArrivalTimeRange)(dId)));
                }
            }
            return View();
        }

        [IsGuestRelated(GuestRelationType.Reservation, "rId")]
        public ActionResult KeyReturnConfirm(string t, int rId)
        {
            TripDetailModel model = new TripDetailModel();
            var reservation = this.ReservationService.GetReservationById(rId);
            if (reservation != null && reservation.LinkAuthorizationToken.ToLower() == t.ToLower())
            {
                if (!reservation.GuestConfirmedKeyDropOff.HasValue)
                {
                    model.ReservationId = rId;
                    model.Reservation = reservation;
                }
                else
                    ViewBag.Message = KeyProcessManagement.KeyConfirmationBody_Replace;
            }
            else
            {
                ViewBag.Message = KeyProcessManagement.KeyConfirmationBody_Failed;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult KeyReturnConfirm(TripDetailModel model)
        {
            if (model.Reservation != null && model.Reservation.LinkAuthorizationToken != null)
            {
                var reservation = this.ReservationService.GetReservationByLinkToken(model.Reservation.LinkAuthorizationToken);
                if (reservation != null)
                {
                    this.ReservationService.DropOffKeyByGuest(reservation.ReservationID);
                    model.Reservation = null;
                    ViewBag.Message = KeyProcessManagement.KeyConfirmationBody_Success;
                }
            }
            return View(model);
        }

        [IsGuestRelated(GuestRelationType.Reservation, "rId")]
        public ActionResult ConfirmCheckIn(string t, int rId)
        {
            var reservation = this.ReservationService.GetReservationById(rId);
            if (reservation != null && reservation.LinkAuthorizationToken.ToLower() == t.ToLower())
            {
                if (reservation.BookingStatus != (int)BookingStatus.FinalizedBooking)
                {
                    ViewBag.Message = KeyProcessManagement.CheckIn_InvalidCurrentStatus;
                    ViewBag.PageTitle = KeyProcessManagement.CheckIn_Title;
                }
                else
                {
                    this.ReservationService.SetReservationStatus(BookingStatus.CheckedIn, rId);
                    ViewBag.Message = string.Format(KeyProcessManagement.CheckIn_Success, reservation.ConfirmationID.ToString());
                    ViewBag.PageTitle = KeyProcessManagement.CheckIn_Success_Title;
                }
            }

            return View();
        }

        [IsGuestRelated(GuestRelationType.Reservation, "rId")]
        public ActionResult ConfirmCheckOut(string t, int rId)
        {
            var reservation = this.ReservationService.GetReservationById(rId);
            if (reservation != null && reservation.LinkAuthorizationToken.ToLower() == t.ToLower())
            {
                if (reservation.BookingStatus != (int)BookingStatus.CheckedIn)
                {
                    ViewBag.Message = KeyProcessManagement.CheckOut_InvalidCurrentStatus;
                    ViewBag.PageTitle = KeyProcessManagement.CheckOut_Title;
                }
                else
                {
                    this.ReservationService.SetReservationStatus(BookingStatus.CheckedOut, rId);
                    ViewBag.Message = string.Format(KeyProcessManagement.CheckOut_Success, reservation.ConfirmationID.ToString());
                    ViewBag.PageTitle = KeyProcessManagement.CheckOut_Success_Title;
                }
            }
            return View();
        }
    }
}
