﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtranetApp.Models;
using System.Web.Security;
using ExtranetApp.Models.Account;
using BusinessLogic.Managers;
using DataAccess.Enums;
using System.Diagnostics;
using System.Threading;
using DataAccess;
using BusinessLogic.ServiceContracts;
using ExtranetApp.Resources.Controllers;
using Common;
using DataAccess.CustomObjects;
using System.Text;
using BusinessLogic.Objects.Session;
using BusinessLogic.Objects.Templates.Email;
using ExtranetApp.Helpers;

namespace ExtranetApp.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(IAuthorizationService authorizationService,
            IUserService userService,
            IRolesService rolesService,
            IDictionaryCultureService dictionaryCultureService,
            IMessageService messageService,
            IStateService stateService)
            : base(new Dictionary<Type, object>() {{ 
                typeof(IAuthorizationService), authorizationService }, {
                typeof(IUserService), userService }, {
                typeof(IRolesService), rolesService }, {
                typeof(IDictionaryCultureService), dictionaryCultureService},{
                typeof(IMessageService), messageService }, {
                typeof(IStateService), stateService }
            })
        {
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        public ActionResult ErrorLogin(string returnUrl)
        {
            ViewBag.TopNavigationSection = new MvcHtmlString(string.Format("<h1>{0}</h1>", ExtranetApp.Resources.Account.ErrorLogin.ErrorLogin_Title));
            return View();
        }

        [HttpPost]
        public ActionResult LoginAsGuest(LoginModel model, string returnUrl)
        {
            return this.Login(model, returnUrl, RoleLevel.Guest);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LoginAsOwner(LoginModel model, string returnUrl)
        {
            return this.Login(model, returnUrl, model.LoginRole);
        }

        private ActionResult LoginOwner(LoginModel model, string returnUrl, RoleLevel roleLevel)
        {
            ModelState.AddModelError(string.Empty, Account.Error_WrongData);
            return View("Index", "Homeowner", model);
        }

        private ActionResult Login(LoginModel model, string returnUrl, RoleLevel roleLevel)
        {
            if (ModelState.IsValid && Membership.ValidateUser(model.Email, model.Password))
            {
                DataAccess.User user = this.UserService.GetUserByEmail(model.Email);
                DataAccess.UserRole userRole = this.UserService.GetUserRoleByLevel(user, roleLevel);

                if (user == null || userRole == null)
                    return Json(new { Valid = false, Message = Account.Error_IncorrectLoginPass });

                if (roleLevel == RoleLevel.Guest && userRole.Status != (int)UserStatus.Active && userRole.Status != (int)UserStatus.Inactive)
                    return Json(new { Valid = false, Message = Account.Error_IncorrectLoginPass });
                else if (roleLevel != RoleLevel.Guest && userRole.Status != (int)UserStatus.Active)
                    return Json(new { Valid = false, Message = Account.Error_IncorrectLoginPass });

                string redirectUrl = string.Empty;

                if (roleLevel == RoleLevel.Owner || roleLevel == RoleLevel.PropertyManager)
                {
                    redirectUrl = Url.RouteUrl(new { controller = "HomeOwner", action = "Places" });
                }
                else if (roleLevel == RoleLevel.KeyManager)
                {
                    redirectUrl = Url.RouteUrl(new { controller = "HomeOwner", action = "KeyManagement" });
                }
                else if (roleLevel == RoleLevel.Guest)
                {
                    redirectUrl = Url.RouteUrl(new { controller = "Guest", action = "MyTrips" });
                }

                FormsAuthentication.SetAuthCookie(model.Email, true);
                this.StateService.CurrentUser = new SessionUser(this.UserService.GetUserByEmail(model.Email));
                Debug.Assert(this.StateService.CurrentUser != null);
                this.StateService.CurrentUser.Apply(this.RolesService.GetRoleByRoleLevel(roleLevel));
                Debug.Assert(this.StateService.CurrentUser.LoggedInRole != null);

                return Json(new
                {
                    Valid = true,
                    Message = "",
                    IsPasswordGenerated = this.StateService.CurrentUser.IsGeneratedPassword,
                    RedirectUrl = redirectUrl
                });
            }
            return Json(new { Valid = false, Message = Account.Error_IncorrectLoginPass });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            this.StateService.CurrentUser = null;
            return RedirectToLocal();
        }

        [HttpGet]
        public ActionResult Join()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Join(JoinModel model, BookingInfoModel bookingModel = null)
        {
            string tempPassword = String.Empty;

            if (ModelState.IsValid)
            {
                bool newUser = false;
                UserRole userRole = null;
                DataAccess.User user = null;
                user = this.UserService.GetUserByEmail(model.Email);
                if (user != null)
                {
                    userRole = this.UserService.GetUserRoleByLevel(user, RoleLevel.Guest);
                    if (userRole != null)
                    {
                        return Json(new { Valid = false, Message = Account.Error_UsersExists });
                    }
                    else //User exists but not in Guest role - Adding role for existing user
                    {
                        userRole = this.RolesService.AddUserToRole(user, RoleLevel.Guest);
                        userRole.Status = (int)UserStatus.Inactive;
                        userRole.ActivationToken = Guid.NewGuid().ToString();
                        user.UserRoles.Add(userRole);
                    }
                }
                else //New user
                {
                    user = new DataAccess.User();
                    userRole = this.RolesService.AddUserToRole(user, RoleLevel.Guest);
                    userRole.Status = (int)UserStatus.Inactive;
                    userRole.ActivationToken = Guid.NewGuid().ToString();

                    user.email = model.Email;
                    user.Firstname = model.FirstName;
                    user.Lastname = model.LastName;
                    //ADD SENDING PASSWORD BY EMAIL
                    tempPassword = AuthorizationService.GeneratePassword();
                    user.Password = MD5Helper.Encode(tempPassword);
                    user.IsGeneratedPassword = true;
                    user.DictionaryCulture = this.DictionaryCultureService.GetCultureByCode(LocalizationHelper.GetUserBrowserCulture());
                    user.UserRoles.Add(userRole);
                    this.UserService.AddUser(user);
                    newUser = true;
                }

                //automatic user login after joining if this is booking wizard context
                if (bookingModel != null && bookingModel.PropertyId != 0)
                {
                    FormsAuthentication.SetAuthCookie(model.Email, true);
                    this.StateService.CurrentUser = new SessionUser(user);
                    Debug.Assert(this.StateService.CurrentUser != null);
                    this.StateService.CurrentUser.Apply(this.RolesService.GetRoleByRoleLevel(RoleLevel.Guest));
                    Debug.Assert(this.StateService.CurrentUser.LoggedInRole != null);
                }

                string activationLinkPattern = "{0}://{1}{2}/{3}";
                string parametes = "";
                //if (bookingModel != null && bookingModel.PropertyId != 0)
                //{
                //    activationLinkPattern = "{0}://{1}{2}/{3}?{4}";
                //    parametes = string.Format("PropertyId={0}&UnitId={1}&dateTripFrom={2}&dateTripUntil={3}", bookingModel.PropertyId, bookingModel.UnitId, bookingModel.dateTripFrom.ToShortDateString(), bookingModel.dateTripUntil.ToShortDateString());
                //}

                var activationLink = string.Format(activationLinkPattern,
                                 Request.Url.Scheme,
                                 Request.Headers["Host"],
                                 "/Account/Activation",
                                 userRole.ActivationToken,
                                 parametes);

                if (!string.IsNullOrEmpty(tempPassword))
                {
                    UserActivationEmailWithPassword guestActivationEmail = new UserActivationEmailWithPassword(user.email);
                    guestActivationEmail.TempPassword = tempPassword;
                    guestActivationEmail.UserFirstName = user.Firstname;
                    guestActivationEmail.ActivationLinkUrl = activationLink;
                    guestActivationEmail.ActivationLinkTitle = Account.Link_ConfirmEmail;
                    guestActivationEmail.Sender = MessageSender.Support;
                    this.MessageService.AddEmail(guestActivationEmail, user.DictionaryCulture.CultureCode);
                }
                else
                {
                    UserActivationEmail guestActivationEmail = new UserActivationEmail(user.email);
                    guestActivationEmail.UserFirstName = user.Firstname;
                    guestActivationEmail.ActivationLinkUrl = activationLink;
                    guestActivationEmail.ActivationLinkTitle = Account.Link_ConfirmEmail;
                    guestActivationEmail.Sender = MessageSender.Support;
                    this.MessageService.AddEmail(guestActivationEmail, user.DictionaryCulture.CultureCode);
                }

                return Json(new { Valid = true, Message = newUser ? Account.Info_AccountAdded : Account.Info_UserRoleAdded });
            }
            return Json(new { Valid = false, Message = Account.Error_InvalidData });
        }

        public ActionResult JoinAsGuestView()
        {
            return PartialView("Partial/JoinGuest", new JoinModel());
        }

        public ActionResult ForgotPassword(ViewWorkingMode workMode = ViewWorkingMode.Normal)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            ViewBag.WorkMode = workMode;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult SendNewPassword(ForgotPasswordModel model, ViewWorkingMode workingMode)
        {
            if (ModelState.IsValid)
            {
                User user = UserService.GetUserByEmail(model.Email);
                bool anyRoleDiffThanRequested = false;
                if (user != null)
                {
                    IEnumerable<UserRole> userRoles = UserService.GetUserRolesByUserId(user.UserID);
                    anyRoleDiffThanRequested = userRoles.Any(ur => ur.Status != (int)UserStatus.Requested);
                }
                if (user != null && anyRoleDiffThanRequested)
                {
                    string tempPassword = AuthorizationService.GeneratePassword();
                    string url;

                    user.Password = MD5Helper.Encode(tempPassword);
                    user.IsGeneratedPassword = true;

                    if (this.StateService.CurrentUser != null)
                        this.StateService.CurrentUser = new SessionUser(user);

                    if (workingMode == ViewWorkingMode.BookingWizard)
                    {
                        var bookingModel = this.StateService.GetFromSession<BookingInfoModel>(BookingController.BOOK_DATA_INFO);
                        var parametes = string.Format("PropertyId={0}&UnitId={1}&dateTripFrom={2}&dateTripUntil={3}", bookingModel.PropertyId, bookingModel.UnitId, bookingModel.dateTripFrom.ToShortDateString(), bookingModel.dateTripUntil.ToShortDateString());
                        var linkPattern = "{0}://{1}{2}?{3}";

                        PasswordResetBookingEmail passwordResetEmail =
                            new PasswordResetBookingEmail(user.email)
                            {
                                UserFirstName = user.Firstname,
                                TempPassword = tempPassword,
                                LinkUrl = string.Format(linkPattern,
                                                        Request.Url.Scheme,
                                                        Request.Headers["Host"],
                                                        "/Booking/BookProperty",
                                                        parametes)
                            };

                        MessageService.AddEmail(passwordResetEmail, user.DictionaryCulture.CultureCode);

                        url = "/Booking/Welcome";
                    }
                    else
                    {
                        PasswordResetEmail passwordResetEmail =
                            new PasswordResetEmail(user.email)
                                {
                                    UserFirstName = user.Firstname,
                                    TempPassword = tempPassword
                                };

                        MessageService.AddEmail(passwordResetEmail, user.DictionaryCulture.CultureCode);
                        url = "/";
                    }

                    return Json(new { Valid = true, Message = Account.Info_NewPasswordSent, RedirectionURL = url });
                }
                if (user != null && !anyRoleDiffThanRequested)
                    return Json(new { Valid = false, Message = Account.Info_RequestStatus });
            }

            return Json(new { Valid = false, Message = Account.Error_UserDoesNotExists });
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            ViewBag.Email = this.StateService.CurrentUser.Email;
            ViewBag.ShouldChangePassword = this.StateService.CurrentUser.IsGeneratedPassword;
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                User user = UserService.GetUserByEmail(model.Email);
                if (user == null)
                    return Json(new { Valid = false, Message = Account.Error_IncorrectEmail });

                if (MD5Helper.Encode(model.CurrentPassword) != user.Password)
                    return Json(new { Valid = false, Message = Account.Error_IncorrectPass });

                if (MD5Helper.Encode(model.NewPassword) == user.Password)
                    return Json(new { Valid = false, Message = Account.Error_SamePass });

                user.Password = MD5Helper.Encode(model.NewPassword);
                user.IsGeneratedPassword = false;

                var sessionUser = SessionManager.CurrentUser;
                sessionUser.IsGeneratedPassword = false;
                SessionManager.CurrentUser = sessionUser;

                return Json(new { Valid = true, Message = Account.Info_PasswordChanged });
            }

            return Json(new { Valid = false });
        }

        private ActionResult RedirectToLocal()
        {
            return Redirect(SessionManager.ReturnUrl ?? "/");
        }

        [HttpGet]
        public ActionResult Activation(string id)
        {
            UserRole userRole = this.UserService.GetUserRoleByActivationToken(id);
            if (userRole != null && userRole.Status == (int)UserStatus.Inactive && (userRole.Role.RoleLevel != (int)RoleLevel.Guest || userRole.Role.RoleLevel != (int)RoleLevel.Owner || userRole.Role.RoleLevel != (int)RoleLevel.KeyManager || userRole.Role.RoleLevel != (int)RoleLevel.PropertyManager))
            {
                ViewBag.Message = Account.Info_AccountActivated;
                userRole.Status = (int)UserStatus.Active;
                userRole.ActivationToken = null;
            }
            else
            {
                ViewBag.Message = Account.Error_TokenInvalid;
            }

            return View("Activation");
        }

        public ActionResult ActivationHomeowner()
        {
            ViewBag.Message = TempData["Message"];
            return View("ActivationOwner");
        }

        [HttpGet]
        public ActionResult ValidateEmail(string email = "", string roleLevel = "")
        {
            RoleLevel targetRoleLevel = (RoleLevel)(int.Parse(roleLevel));
            RoleLevel currentRole = (targetRoleLevel == RoleLevel.Guest) ? RoleLevel.Owner : RoleLevel.Guest;
            DataAccess.User user = null;
            user = this.UserService.GetUserByEmail(email);
            if (user != null)
            {
                var userRoles = this.UserService.GetRoleLevelsForUser(user.UserID).ToList();
                if (userRoles.Count() > 0)
                {
                    string userCurrentRoles = "";
                    foreach (RoleLevel rl in userRoles)
                    {
                        if (rl != currentRole)
                            userCurrentRoles += rl.ToString() + ", ";
                    }
                    if (userCurrentRoles != "")
                    {
                        userCurrentRoles = userCurrentRoles.Trim();
                        userCurrentRoles = userCurrentRoles.TrimEnd(',');
                        return Json(new { Valid = false, Message = string.Format(Account.Error_UserExistInOtherRole, userCurrentRoles, currentRole.ToString()) }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { Valid = true, Message = "" }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult AccessDenied()
        {
            FormsAuthentication.SignOut();
            this.StateService.CurrentUser = null;

            ViewBag.TopNavigationSection = new MvcHtmlString(string.Format("<h1>{0}</h1>", ExtranetApp.Resources.Controllers.Account.Error_AccessDenied));

            return View();
        }

        [AllowAnonymous]
        public JsonResult GetUserRoles(string email)
        {
            List<string> roleList = new List<string>();

            User usr = this.UserService.GetUserByEmail(email);
            if (usr != null)
            {
                var userRoles = this.UserService.GetRoleLevelsForUser(usr.UserID, true);
                foreach (var role in userRoles)
                {
                    roleList.Add(role.ToString());
                }
            }
            return Json(roleList, JsonRequestBehavior.AllowGet);
        }
    }
}
