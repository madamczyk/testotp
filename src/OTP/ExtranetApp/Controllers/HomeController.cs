﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.CustomObjects;
using ExtranetApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Converters;
using System.Threading;
using BusinessLogic.Enums;
using System.Web.Caching;
using ExtranetApp.Models.PropertyDetails;
using DataAccess.Enums;
using BusinessLogic.ServiceContracts;
using ExtranetApp.Models.Home;
using System.Globalization;
using ExtranetApp.Extensions;
using ExtranetApp.Resources.Views;


namespace ExtranetApp.Controllers
{
    public class HomeController : BaseController
    {
        #region Ctor

        public HomeController(IDestinationsService destinationsService, IPropertiesService propertiesService, IStateService stateService, ISettingsService settingsService)
            : base(new Dictionary<Type, object>() {
            {   typeof(IDestinationsService), destinationsService   }, 
            {   typeof(IPropertiesService), propertiesService   },
            {   typeof(IStateService), stateService  },
            {   typeof(ISettingsService), settingsService  },})
        {
        }

        #endregion

        const int IMAGES_PER_PAGE_LIST_VIEW = 6;
        const string COOKIE_PAGE_NAME = "list_view_page_no";
        const string CACHE_LIST_VIEW_RESULT = "list_view_result";

        #region Views

        /// <summary>
        /// Shows Slidehow View {Home/Slideshow}
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.InfoMessage = TempData["InfoMessage"];
            return View();
        }

        /// <summary>
        /// Shows List View {Home/ListView}
        /// </summary>
        /// <returns></returns>
        public ActionResult ListView()
        {
            return View();
        }

        /// <summary>
        /// Shows List View {Home/MapView}
        /// </summary>
        /// <returns></returns>
        public ActionResult MapView()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Faq()
        {
            List<OTP_Settings> settings = this.SettingsService.GetAllSettingsByPrefix("FAQ").ToList();

            ViewBag.SupportPhone = settings.Where(m => m.SettingCode == "FAQ.SupportPhone").FirstOrDefault().SettingValue;
            ViewBag.SupportEmail = settings.Where(m => m.SettingCode == "FAQ.SupportEmail").FirstOrDefault().SettingValue;
            ViewBag.HomeownersEmail = settings.Where(m => m.SettingCode == "FAQ.HomeownersEmail").FirstOrDefault().SettingValue;

            return View();
        }

        public ActionResult Policies()
        {
            return View();
        }

        public ActionResult Contact()
        {
            List<OTP_Settings> settings = this.SettingsService.GetAllSettingsByPrefix("ContactUs").ToList();
            
            ViewBag.GuestServicesEmail = settings.Where(m => m.SettingCode == "ContactUs.GuestServicesEmail").FirstOrDefault().SettingValue;
            ViewBag.GuestServicesPhone = settings.Where(m => m.SettingCode == "ContactUs.GuestServicesPhone").FirstOrDefault().SettingValue;
            ViewBag.HomeownersServicesEmail = settings.Where(m => m.SettingCode == "ContactUs.HomeownersServicesEmail").FirstOrDefault().SettingValue;
            ViewBag.HomeownersServicesPhone = settings.Where(m => m.SettingCode == "ContactUs.HomeownersServicesPhone").FirstOrDefault().SettingValue;
            ViewBag.OTPHeadquartersAddressLine1 = settings.Where(m => m.SettingCode == "ContactUs.OTPHeadquartersAddressLine1").FirstOrDefault().SettingValue;
            ViewBag.OTPHeadquartersAddressLine2 = settings.Where(m => m.SettingCode == "ContactUs.OTPHeadquartersAddressLine2").FirstOrDefault().SettingValue;
            ViewBag.OTPHeadquartersEmail = settings.Where(m => m.SettingCode == "ContactUs.OTPHeadquartersEmail").FirstOrDefault().SettingValue;
            ViewBag.OTPHeadquartersPhone = settings.Where(m => m.SettingCode == "ContactUs.OTPHeadquartersPhone").FirstOrDefault().SettingValue;

            return View();
        }


        /// <summary>
        /// Displays terms of service
        /// </summary>
        [HttpGet]
        public ActionResult TermsOfService()
        {
            ViewBag.Mode = "TOS";
            return View("Regulations");
        }

        /// <summary>
        /// Displays privacy policy
        /// </summary>
        [HttpGet]
        public ActionResult PrivacyPolicy()
        {
            ViewBag.Mode = "PP";
            return View("Regulations");
        }

        public ActionResult CancellationPolicy()
        {
            ViewBag.Mode = "CP";
            return View("Regulations");
        }

        #endregion

        #region PartialViews
        /// <summary>
        /// Returns first results when entered to the slideshow wihtout search criteria
        /// </summary>
        /// <returns>Default properties search result</returns>
        public PartialViewResult GetSampleResult()
        {
            var result = GenerateSampleData();
            return PartialView("Partial/Slider", result);
        }

        /// <summary>
        /// Returns first results when entered to the listView wihtout search criteria
        /// </summary>
        /// <returns>Default properties search result</returns>
        public PartialViewResult GetSampleResultListVIew()
        {
            var result = GenerateSampleData();
            ListViewModel lvModel = new ListViewModel();
            lvModel.PropertiesResult = result;
            lvModel.CurrentResultCount = result.Count();
            lvModel.TotalResults = result.Count();
            return PartialView("Partial/ListViewSearchResult", lvModel);
        }

        /// <summary>
        /// Action invoke by Ajax request after search filter changes on the Slideshow
        /// </summary>
        /// <param name="destination">Destination location</param>
        /// <param name="dateFrom">Date from</param>
        /// <param name="dateTo">Date until</param>
        /// <param name="guests">Number of guests</param>
        /// <param name="bedrooms">Number of bedrooms</param>
        /// <param name="bathrooms">Number of bathrooms</param>
        /// <param name="propertyTypes">Comma separated list of property types</param>
        /// <param name="amenities">Comma separated list of amenities</param>
        /// <param name="experiences">Comma separated list of experiences</param>
        /// <returns>View with property search result</returns>
        public PartialViewResult Search(string destination = "", string dateFrom = "", string dateTo = "", string guests = "", string bedrooms = "", string bathrooms = "", string propertyTypes = "", string amenities = "", string experiences = "")
        {
            var result = PerformSearch(destination, dateFrom, dateTo, guests, bedrooms, bathrooms, propertyTypes, amenities, experiences);
            return PartialView("Partial/Slider", result);
        }

        /// <summary>
        /// Action invoke by Ajax request after search filter changes on the ListView
        /// </summary>
        /// <param name="destination">Destination location</param>
        /// <param name="dateFrom">Date from</param>
        /// <param name="dateTo">Date until</param>
        /// <param name="guests">Number of guests</param>
        /// <param name="bedrooms">Number of bedrooms</param>
        /// <param name="bathrooms">Number of bathrooms</param>
        /// <param name="propertyTypes">Comma separated list of property types</param>
        /// <param name="amenities">Comma separated list of amenities</param>
        /// <param name="experiences">Comma separated list of experiences</param>
        /// <param name="requestType">Type of the current request i.e. lazy content loading</param>
        /// <param name="sort_by">Sort by parameter and direction</param>
        /// <returns>View with property search result</returns>
        public PartialViewResult SearchListView(string destination = "", string dateFrom = "", string dateTo = "", string guests = "", string bedrooms = "", string bathrooms = "", string propertyTypes = "", string amenities = "", string experiences = "", string requestType = "", string sort_by = "")
        {
            var page_no = Int32.Parse(Request.Cookies[COOKIE_PAGE_NAME].Value);
            //ask what sort should be default
            SortByType sortType = String.IsNullOrEmpty(sort_by) ? SortByType.PriceAsc : ((SortByType)Enum.Parse(typeof(SortByType), sort_by, true));
            SearchRequestType searchRequestType = String.IsNullOrEmpty(requestType) ? SearchRequestType.Unknown : ((SearchRequestType)Enum.Parse(typeof(SearchRequestType), requestType, true));

            //invalidate cache when selected different filter data, but remain it when: content lazy load or sort request
            if (searchRequestType == SearchRequestType.Unknown)
            {
                this.StateService.RemoveFromCache(CACHE_LIST_VIEW_RESULT);
            }

            IEnumerable<PropertiesSearchResult> result = this.StateService.GetFromCache<IEnumerable<PropertiesSearchResult>>(CACHE_LIST_VIEW_RESULT);

            if (result == null)
            {
                result = PerformSearch(destination, dateFrom, dateTo, guests, bedrooms, bathrooms, propertyTypes, amenities, experiences);
                this.StateService.AddToCache(CACHE_LIST_VIEW_RESULT, result, 600);
            }

            if (searchRequestType == SearchRequestType.Sort)
            {
                SortResultsBy(sortType, ref result);
            }

            ListViewModel lvModel = new ListViewModel();
            lvModel.TotalResults = result.Count();

            result = result.Skip((page_no - 1) * IMAGES_PER_PAGE_LIST_VIEW).Take(IMAGES_PER_PAGE_LIST_VIEW);
            lvModel.PropertiesResult = result;
            lvModel.CurrentResultCount = ((page_no - 1) * IMAGES_PER_PAGE_LIST_VIEW) + result.Count();

            Response.AddHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
            Response.AddHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
            return PartialView("Partial/ListViewSearchResult", lvModel);
        }

        /// <summary>
        /// Action invoke by Ajax request after search filter changes on the MapView
        /// </summary>
        /// <param name="destination">Destination location</param>
        /// <param name="dateFrom">Date from</param>
        /// <param name="dateTo">Date until</param>
        /// <param name="guests">Number of guests</param>
        /// <param name="bedrooms">Number of bedrooms</param>
        /// <param name="bathrooms">Number of bathrooms</param>
        /// <param name="propertyTypes">Comma separated list of property types</param>
        /// <param name="amenities">Comma separated list of amenities</param>
        /// <param name="experiences">Comma separated list of experiences</param>
        /// <returns>View with property search result</returns>
        [HttpGet]
        public ActionResult SearchMapView(string destination = "", string dateFrom = "", string dateTo = "", string guests = "",
                                          string bedrooms = "", string bathrooms = "", string propertyTypes = "",
                                          string amenities = "", string experiences = "", string beginLatitude = "",
                                          string endLatitude = "", string beginLongitude = "", string endLongitude = "")
        {
            var propertySearchResult = PerformSearch(destination, dateFrom, dateTo, guests, bedrooms,
                                                     bathrooms, propertyTypes, amenities, experiences,
                                                     beginLatitude, endLatitude, beginLongitude, endLongitude);

            var jsonResult = from propertyResult in propertySearchResult
                             select new
                             {
                                propertyResult.Id,
                                propertyResult.Title,
                                propertyResult.Image,
                                propertyResult.Thumbnails,
                                propertyResult.Description,
                                propertyResult.IsPricePerNight,
                                propertyResult.Bathrooms,
                                propertyResult.Bedrooms,
                                propertyResult.Sleeps,
                                propertyResult.Latitude,
                                propertyResult.Longitude,
                                PricePerNight = propertyResult.PricePerNight.ToString("c", new System.Globalization.CultureInfo(propertyResult.CultureCode)),
                                Price = propertyResult.Price.ToString("c", new System.Globalization.CultureInfo(propertyResult.CultureCode)),
                                propertyResult.Destination,
                                TitleEncoded = Url.ToEncodedUrl(propertyResult.Title),
                                propertyResult.FinalScore,
                                ScoreLabel = Shared.FinalScore
                             };

            return Json(jsonResult.ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Action invoke by Ajax request after click on Score Details. 
        /// Retrieve details about search final score (what parameters are not meet)
        /// </summary>
        /// <param name="unitId">ID of the unit</param>
        /// <param name="guests">No of guests defined in search filters</param>
        /// <param name="bedrooms">No of bedrooms defined in the search filters</param>
        /// <param name="bathrooms">No of bathrooms defined in the search filters</param>
        /// <param name="propertyTypes">The list of proeprty types id (separated by semicolon) seleced in the search filters</param>
        /// <param name="amenities">The list of amenties separated by semicolon</param>
        /// <param name="experiences">The list of tags (experiences) separated by semicolon</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetScoreDetails(string unitId = "", string guests = "", string bedrooms = "", string bathrooms = "", string propertyTypes = "", string amenities = "", string experiences = "")
        {
            var scoreDetailsDb = this.PerformGetFinalScoreDetails(unitId, guests, bedrooms, bathrooms, propertyTypes, amenities, experiences);
            FinalScoreDetailsModel scoreDetailsModel = new FinalScoreDetailsModel();
            scoreDetailsModel.Details = scoreDetailsDb;
            scoreDetailsModel.SearchFilterBathrooms = bathrooms.ToInt();
            scoreDetailsModel.SearchFilterBedrooms = bedrooms.ToInt();
            scoreDetailsModel.SearchFilterGuests = guests.ToInt();

            return PartialView("Partial/SearchFinalScoreDetails", scoreDetailsModel);
        }

        #endregion

        #region PrivateMembers

        /// <summary>
        /// Performs search for the properties
        /// </summary>
        /// <param name="destination">Destination location</param>
        /// <param name="dateFrom">Date from</param>
        /// <param name="dateTo">Date until</param>
        /// <param name="guests">Number of guests</param>
        /// <param name="bedrooms">Number of bedrooms</param>
        /// <param name="bathrooms">Number of bathrooms</param>
        /// <param name="propertyTypes">Comma separated list of property types</param>
        /// <param name="amenities">Comma separated list of amenities</param>
        /// <param name="experiences">Comma separated list of experiences</param>
        /// <param name="beginLatitude"></param>
        /// <param name="endLatitude"></param>
        /// <param name="beginLongitude"></param>
        /// <param name="endLongitude"></param>
        /// <returns></returns>
        private IEnumerable<PropertiesSearchResult> PerformSearch(string destination = "", string dateFrom = "", string dateTo = "", string guests = "", string bedrooms = "", string bathrooms = "", string propertyTypes = "", string amenities = "", string experiences = "", string beginLatitude = "", string endLatitude = "", string beginLongitude = "", string endLongitude = "")
        {
            var result =
                this.PropertiesService.SearchProperties(
                destination.ToInt(),
                dateFrom.ToDateTime(),
                dateTo.ToDateTime(),
                guests.ToInt(),
                bathrooms.ToInt(),
                bedrooms.ToInt(),
                propertyTypes.ToSerializedList("PropertyTypes"),
                experiences.ToSerializedList("Tags"),
                amenities.ToSerializedList("Amenities"),
                CultureCode.en_US,
                String.IsNullOrWhiteSpace(beginLatitude)
                    ? (decimal?)null
                    : Decimal.Parse(beginLatitude, CultureInfo.InvariantCulture),
                String.IsNullOrWhiteSpace(endLatitude)
                    ? (decimal?)null
                    : Decimal.Parse(endLatitude, CultureInfo.InvariantCulture),
                String.IsNullOrWhiteSpace(beginLongitude)
                    ? (decimal?)null
                    : Decimal.Parse(beginLongitude, CultureInfo.InvariantCulture),
                String.IsNullOrWhiteSpace(endLongitude)
                    ? (decimal?)null
                    : Decimal.Parse(endLongitude, CultureInfo.InvariantCulture));
            return result;
        }

        /// <summary>
        /// Performs get for the score details
        /// </summary>
        /// <param name="unitId">Unit ID</param>
        /// <param name="guests">Number of guests</param>
        /// <param name="bedrooms">Number of bedrooms</param>
        /// <param name="bathrooms">Number of bathrooms</param>
        /// <param name="propertyTypes">Comma separated list of property types</param>
        /// <param name="amenities">Comma separated list of amenities</param>
        /// <param name="experiences">Comma separated list of experiences</param>
        /// <returns></returns>
        private SearchFinalScoreDetails PerformGetFinalScoreDetails(string unitId, string guests = "", string bedrooms = "", string bathrooms = "", string propertyTypes = "", string amenities = "", string experiences = "")
        {
            var result = this.PropertiesService.GetSearchScoreDetails(
                unitId.ToInt().Value, 
                guests.ToInt(),
                bathrooms.ToInt(),
                bedrooms.ToInt(),
                propertyTypes.ToSerializedList("PropertyTypes"),
                experiences.ToSerializedList("Tags"),
                amenities.ToSerializedList("Amenities"));
            return result;
        }

        /// <summary>
        /// Performs sort operation on search results
        /// </summary>
        /// <param name="sortType">Sort order by column and direction</param>
        /// <param name="searchResults">Results sorted by type</param>
        private void SortResultsBy(SortByType sortType, ref IEnumerable<PropertiesSearchResult> searchResults)
        {
            switch (sortType)
            {
                case SortByType.PriceAsc:
                    searchResults = searchResults.OrderBy(p => p.Price);
                    break;
                case SortByType.PriceDesc:
                    searchResults = searchResults.OrderByDescending(p => p.Price);
                    break;
                case SortByType.RatingDesc:
                    searchResults = searchResults.OrderBy(p => p.Rating);
                    break;
                case SortByType.SleepAsc:
                    searchResults = searchResults.OrderBy(p => p.Sleeps);
                    break;
                case SortByType.SleepDesc:
                    searchResults = searchResults.OrderByDescending(p => p.Sleeps);
                    break;
            }
        }

        /// <summary>
        /// Generates first results when entered on the slideshow or list view
        /// </summary>
        /// <returns>Properties list</returns>
        private IEnumerable<PropertiesSearchResult> GenerateSampleData()
        {
            var destId = this.DestinationsService.GetTopXDestinationsByPropertiesCount(1).Select(d => d.DestinationID).FirstOrDefault();
            var result = PerformSearch(destId.ToString());

            return result;

        }


        #endregion

    }
}
