﻿using BusinessLogic.Managers;
using DataAccess;
using ExtranetApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic.ServiceContracts;

namespace ExtranetApp.Controllers
{
    public class NavBarController : BaseController
    {
        #region Ctor

        public NavBarController(IDestinationsService destinationsService,
            IAmenityService amenitiesService,
            IPropertyTypesService propertyTypesService,
            ITagsService tagsService,
            ITagGroupsService tagGroupsService,
            IAmenityGroupsService amenityGroupService)
            : base(new Dictionary<Type, object>() {{ 
                typeof(IDestinationsService), destinationsService }, {
                typeof(IAmenityService), amenitiesService }, {
                typeof(IPropertyTypesService), propertyTypesService }, {
                typeof(IAmenityGroupsService), amenityGroupService }, {
                typeof(ITagGroupsService), tagGroupsService }, {
                typeof(ITagsService), tagsService }
            })
        {
        }

        #endregion

        #region PartialViews

        /// <summary>
        /// Shows Navbar menu with sort option
        /// </summary>
        /// <returns>Nav menu view with sort</returns>
        [ChildActionOnly]
        public ActionResult NavBarWithSort()
        {
            return PartialView("NavBarWithSort", InitSearchFilterData());
        }

        /// <summary>
        /// Shows Navbar menu without sort option
        /// </summary>
        /// <returns>Nav menu view without sort</returns>
        [ChildActionOnly]
        public ActionResult NavBar()
        {
            return PartialView("NavBar", InitSearchFilterData());
        }

        #endregion

        #region JSONResponses

        /// <summary>
        /// Retrieves destinations that match "term" values {used for the autocomplete}
        /// </summary>
        /// <param name="term">Search destination term</param>
        /// <returns>List of the matched terms</returns>
        public JsonResult FindDestination(string term)
        {
            var result = this.DestinationsService.GetMatchesDestinations(term).Select(d => new { id = d.DestinationID, value = d.DestinationNameCurrentLanguage }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Retrieves destinations that match "term"
        /// </summary>
        /// <param name="term">Search destination term</param>
        /// <returns>Matched destination</returns>
        public JsonResult FindExactDestination(string term)
        {
            var result = this.DestinationsService.GetMatchesDestinations(term).Where(d => d.DestinationNameCurrentLanguage.ToUpper().Equals(term.ToUpper())).Select(d => new { id = d.DestinationID, value = d.DestinationNameCurrentLanguage }).FirstOrDefault();           
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Retrieves destination name by its ID
        /// </summary>
        /// <param name="id">Destination id</param>
        /// <returns>Destination location name</returns>
        public string GetDestinationNameById(string id)
        {
            string destName = string.Empty;
            Destination destObj = this.DestinationsService.GetDestinationById(int.Parse(id));
            if (destObj != null)
            {
                destName = destObj.DestinationName.ToString();
            }
            return destName;
        }
        #endregion

        #region PrivateMembers

        /// <summary>
        /// Initializes nav menu bar filters to default values
        /// </summary>
        /// <returns>Initilized filter model</returns>
        private SearchFilterModel InitSearchFilterData()
        {
            SearchFilterModel viewModel = new SearchFilterModel();

            //Specify initial filter values
            var destinations = this.DestinationsService.GetTopXDestinationsByPropertiesCount(9).Select(d => new ObjectItem() { ID = d.DestinationID, Value = d.DestinationName.ToString(), Selected = false }).ToList();
            viewModel.Bathrooms = null;
            viewModel.Bedrooms = null;
            viewModel.Guests = null;
            viewModel.Location = destinations.Take(1).FirstOrDefault();
            viewModel.DateFrom = null;
            viewModel.DateTo = null;

            //bind Filters
            #region amenity groups + amenities
            var amenityGroups = this.AmenityGroupsService.GetAmenityGroups().OrderBy(ag => ag.NameCurrentLanguage);
            viewModel.Amenities = amenityGroups.Select(d => new GroupedObjectItem()
                                    {
                                        GroupId = d.AmenityGroupId,
                                        GroupName = d.NameCurrentLanguage,
                                        Items = d.Amenities.Select(a => new ObjectItem()
                                        {
                                            ID = a.AmenityID,
                                            Selected = false,
                                            Value = a.TitleCurrentLanguage
                                        }).ToList()
                                    });
            #endregion

            #region tags groups + tags
            var tagsGroups = this.TagGroupsService.GetTagGroups().OrderBy(tag => tag.Position);
            viewModel.Experiences = tagsGroups.Select(d => new GroupedObjectItem()
            {
                GroupId = d.TagGroupId,
                GroupName = d.NameCurrentLanguage,
                Items = d.Tags.Select(a => new ObjectItem()
                {
                    ID = a.TagID,
                    Selected = false,
                    Value = a.NameCurrentLanguage
                }).ToList()
            });
            #endregion

            viewModel.Destinations = destinations;
            viewModel.PropertyTypes = this.PropertyTypesService.GetPropertyTypes().Select(d => new ObjectItem() { ID = d.PropertyTypeId, Value = d.Name.ToString(), Selected = false });
            return viewModel;
        }

        #endregion

    }
}
