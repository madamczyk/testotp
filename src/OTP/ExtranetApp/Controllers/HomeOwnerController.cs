﻿using BusinessLogic.Enums;
using ExtranetApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataAccess;
using DataAccess.Enums;
using System.Threading;
using BusinessLogic.ServiceContracts;
using ExtranetApp.Resources.Controllers;
using Common;
using Common.Converters;
using BusinessLogic.Objects.Templates.Email;
using ExtranetApp.Models.HomeOwner;
using BusinessLogic.Objects.Session;
using Common.Extensions;
using Common.Exceptions;
using BusinessLogic.Objects;
using ExtranetApp.Helpers;
using ExtranetApp.Security;
using DataAccess.CustomObjects;
using System.IO;

namespace ExtranetApp.Controllers
{
    public class HomeOwnerController : BaseController
    {
        #region Ctor

        public HomeOwnerController(IStateService stateService,
            IMessageService messageService,
            IDictionaryCountryService dictionaryCountryService,
            IDictionaryCultureService dictionaryCultureService,
            IRolesService roleService,
            IUserService userService,
            IScheduledVisitsService scheduledVisitsService,
            IReservationsService reservationService,
            IPropertiesService propertiesService,
            IConfigurationService configurationService,
            IUnitsService unitService,
            ISettingsService settingsService,
            IAuthorizationService authorizationService,
            IBlobService blobService)
            : base(new Dictionary<Type, object>() {{ 
                typeof(IStateService), stateService }, {
                typeof(IMessageService), messageService }, {
                typeof(IDictionaryCountryService), dictionaryCountryService }, {                                                                    
                typeof(IDictionaryCultureService), dictionaryCultureService},{
                typeof(IRolesService), roleService }, {
                typeof(IUserService), userService }, 
                { typeof(IScheduledVisitsService), scheduledVisitsService},
                { typeof(IReservationsService), reservationService},
                { typeof(IPropertiesService), propertiesService },
                { typeof(IConfigurationService), configurationService},
                { typeof(IUnitsService), unitService},
                { typeof(ISettingsService), settingsService},
                { typeof(IAuthorizationService), authorizationService},
                { typeof(IBlobService), blobService},
            }
            )
        {
        }

        #endregion

        /// <summary>
        /// Just for backward compability (redirects to new action)
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return RedirectToAction("AboutJoin");
        }

        /// <summary>
        /// Streams OwnerAgreement pdf to the browser
        /// </summary>
        /// <returns></returns>
        public ActionResult OwnerAgreement()
        {
            return File(BlobService.DownloadFileByUrl("FullFormOwnerContract.pdf", "pdfs"), "application/pdf");
        }

        /// <summary>
        /// Streams ShortTermRentalAgreement pdf to the browser
        /// </summary>
        /// <returns></returns>
        public ActionResult ShortTermRentalAgreement()
        {
            return File(BlobService.DownloadFileByUrl("FullFormOwnerGuestContract.pdf", "pdfs"), "application/pdf");
        }

        /// <summary>
        /// Opens About Join view
        /// </summary>
        /// <param name="id">Affility Signup Code</param>
        /// <returns></returns>
        public ActionResult AboutJoin(string id = "")
        {
            this.StateService.RemoveFromSession(SessionKeys.HomeownerSignUpCode.ToString());
            this.StateService.AddToSession(SessionKeys.HomeownerSignUpCode.ToString(), id);

            string examplePropertyUrl = string.Empty;
            string examplePropertyIdSetting = SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ExamplePropertyId); // obtain setting with exmaple property id

            var blobUrl = ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray());

            // if setting doesn't exists, don't show the link
            if (!String.IsNullOrWhiteSpace(examplePropertyIdSetting))
            {
                int examplePropertyId;

                if (int.TryParse(examplePropertyIdSetting, out examplePropertyId))
                {
                    var exampleProperty = PropertiesService.GetProperties().Where(p => p.PropertyID == examplePropertyId).SingleOrDefault();

                    if (exampleProperty != null)
                        examplePropertyUrl = "/PropertyDetail/PropertyDetails/" + exampleProperty.PropertyID;
                }
            }

            ViewBag.ExamplePropertyUrl = examplePropertyUrl;
            ViewBag.TopNavigationSection = new MvcHtmlString(string.Format("<h1>{0}</h1>", ExtranetApp.Resources.Views.HomeOwner.Index_Title));

            return View("Index");
        }

        /// <summary>
        /// Displays MyAccount View
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Owner, Property Manager")]
        public ActionResult MyAccount()
        {
            SessionUser sessionUser = this.StateService.CurrentUser;
            List<PropertyWithImages> listPropImage = new List<PropertyWithImages>();
            List<Property> propertyList = GetPropertiesListForLoggedUser();

            string blobContainter = this.ConfigurationService.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl);

            propertyList.ForEach(delegate(Property p)
            {
                var retinaImage = this.PropertiesService.GetPropertyStaticContentByType(p.PropertyID, PropertyStaticContentType.FullScreen_Retina).SingleOrDefault();
                var normalImage = this.PropertiesService.GetPropertyStaticContentByType(p.PropertyID, PropertyStaticContentType.FullScreen).SingleOrDefault();

                PropertyWithImages propWithImg = new PropertyWithImages()
                {
                    Property = p,
                    FullScreenImage = normalImage == null ? null : string.Format("{0}{1}", blobContainter, normalImage.ContentValue),
                    FullScreenImageRetina = retinaImage == null ? null : string.Format("{0}{1}", blobContainter, retinaImage.ContentValue),
                };
                listPropImage.Add(propWithImg);
            });

            MyAccountModel model = new MyAccountModel()
            {
                Owner = GetProfileModel(this.UserService.GetUserByUserId(sessionUser.UserID)),
                PropertyWithImages = listPropImage
            };

            ViewBag.Countries = new SelectList(this.DictionaryCountryService.GetCountries(), "CountryCode2Letters", "CountryNameCurrentLanguage");

            return View(model);
        }

        /// <summary>
        /// Invoked on Homeowner update data request (my account)
        /// </summary>
        /// <param name="model">Update model</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Owner, Property Manager")]
        public ActionResult MyAccount(MyAccountModel model)
        {
            if (ModelState.IsValid)
            {
                UpdateUserData(model.Owner);
                return RedirectToAction("MyAccount");
            }
            else
            {
                SessionUser sessionUser = this.StateService.CurrentUser;
                List<PropertyWithImages> listPropImage = new List<PropertyWithImages>();

                List<Property> propertyList = GetPropertiesListForLoggedUser();

                string blobContainter = this.ConfigurationService.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl);

                propertyList.ForEach(delegate(Property p)
                {
                    var retinaImage = this.PropertiesService.GetPropertyStaticContentByType(p.PropertyID, PropertyStaticContentType.FullScreen_Retina).SingleOrDefault();
                    var normalImage = this.PropertiesService.GetPropertyStaticContentByType(p.PropertyID, PropertyStaticContentType.FullScreen).SingleOrDefault();

                    PropertyWithImages propWithImg = new PropertyWithImages()
                    {
                        Property = p,
                        FullScreenImage = normalImage == null ? null : string.Format("{0}{1}", blobContainter, normalImage.ContentValue),
                        FullScreenImageRetina = retinaImage == null ? null : string.Format("{0}{1}", blobContainter, retinaImage.ContentValue),
                    };
                    listPropImage.Add(propWithImg);
                });

                MyAccountModel newModel = new MyAccountModel()
                {
                    Owner = GetProfileModel(this.UserService.GetUserByUserId(sessionUser.UserID)),
                    PropertyWithImages = listPropImage
                };

                ViewBag.Countries = new SelectList(this.DictionaryCountryService.GetCountries(), "CountryCode2Letters", "CountryNameCurrentLanguage");

                return View(newModel);
            }
        }

        /// <summary>
        /// Display Places View
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Owner, Property Manager")]
        public ActionResult Places()
        {
            var sessionUser = this.StateService.CurrentUser;

            var propertyList = GetPropertiesListForLoggedUser();
            string blobContainter = this.ConfigurationService.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl);

            List<PlacesModel> model = new List<PlacesModel>();
            foreach (var property in propertyList)
            {
                PlacesModel m = new PlacesModel()
                {
                    Property = property,
                    Thumbnails = this.PropertiesService.GetPropertyStaticContentByType(property.PropertyID, PropertyStaticContentType.Thumbnail).Select(s => string.Format("{0}{1}", blobContainter, s.ContentValue))
                };
                model.Add(m);
            };

            return View(model);
        }

        /// <summary>
        /// Display list of units for specyfic Property
        /// </summary>
        /// <param name="id">Property Id</param>
        /// <returns></returns>
        [Authorize(Roles = "Owner, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Property, "id")]
        public ActionResult Units(int id)
        {
            var user = this.StateService.CurrentUser;
            var list = this.UnitsService.GetUnitsByPropertyId(id);
            string blobContainter = this.ConfigurationService.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl);


            Property property = this.PropertiesService.GetPropertyById(id);
            List<PlacesModel> model = new List<PlacesModel>();
            foreach (var unit in list)
            {
                PlacesModel m = new PlacesModel()
                {
                    Property = property,
                    Unit = unit,
                    Thumbnails = this.PropertiesService.GetPropertyStaticContentByType(id, PropertyStaticContentType.Thumbnail).Select(s => string.Format("{0}{1}", blobContainter, s.ContentValue))
                };
                model.Add(m);
            };

            return View(model);
        }

        /// <summary>
        /// Display reservation list view
        /// </summary>
        /// <param name="id">Property Id</param>
        /// <returns></returns>
        [Authorize(Roles = "Owner, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Property, "id")]
        public ActionResult ReservationsList(int id)
        {
            var user = this.StateService.CurrentUser;
            var reservationList = this.ReservationService.GetReservationsForProperty(id, new List<BookingStatus>()
                {
                    //excluded reservation statuses
                    BookingStatus.DatesChanged,
                    BookingStatus.NoFunds,
                    BookingStatus.TentativeBooking,
                    BookingStatus.Withdrew
                });
            var property = this.PropertiesService.GetPropertyById(id);
            var model = new ReservationsModel()
            {
                Reservations = reservationList,
                PropertyId = id,
                Property = property
            };
            return View(model);
        }

        /// <summary>
        /// Display reservation calendar view
        /// </summary>
        /// <param name="id">Unit id</param>
        /// <returns></returns>
        [Authorize(Roles = "Owner, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Unit, "id")]
        public ActionResult ReservationsCalendar(int id)
        {
            var user = this.StateService.CurrentUser;
            var reservationList = this.ReservationService.GetReservationsForUnit(id);
            var unit = this.UnitsService.GetUnitById(id);
            var property = unit.Property;
            var model = new ReservationsModel()
            {
                Reservations = reservationList,
                Unit = unit,
                Property = property,
                PropertyId = property.PropertyID
            };
            return View(model);
        }


        /// <summary>
        /// Display content update form. 
        /// </summary>
        /// <param name="id">Property id</param>
        /// <returns></returns>
        [Authorize(Roles = "Owner, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Property, "propertyId")]
        public ActionResult ContentUpdate(int propertyId)
        {
            var userSession = this.StateService.CurrentUser;
            User user = UserService.GetUserByUserId(userSession.UserID);
            Property property = this.PropertiesService.GetPropertyById(propertyId);
            string blobContainter = this.ConfigurationService.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl);

            var retinaImage = this.PropertiesService.GetPropertyStaticContentByType(property.PropertyID, PropertyStaticContentType.FullScreen_Retina).SingleOrDefault();
            var normalImage = this.PropertiesService.GetPropertyStaticContentByType(property.PropertyID, PropertyStaticContentType.FullScreen).SingleOrDefault();

            PropertyWithImages propWithImg = new PropertyWithImages()
            {
                Property = property,
                FullScreenImage = normalImage == null ? null : string.Format("{0}{1}", blobContainter, normalImage.ContentValue),
                FullScreenImageRetina = retinaImage == null ? null : string.Format("{0}{1}", blobContainter, retinaImage.ContentValue),
            };

            ContentUpdateModel model = new ContentUpdateModel()
            {
                propWithImages = propWithImg,
                storageLinks = ""
            };

            return View(model);
        }

        /// <summary>
        /// Display content update form. 
        /// </summary>
        /// <param name="id">Property id</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Owner, Property Manager")]
        public ActionResult ContentUpdate(ContentUpdateModel model)
        {
            var userSession = this.StateService.CurrentUser;
            User user = UserService.GetUserByUserId(userSession.UserID);
            Property property = this.PropertiesService.GetPropertyById(model.propertyId);

            List<String> inStorage = (model.storageLinks != null ? model.storageLinks.TrimEnd(';').Split(';').ToList() : new List<String>());

            if (!(inStorage.Count == 0 && model.message == null))
            {
                ContentUpdateRequest newRequest = new ContentUpdateRequest()
                {
                    Description = model.message ?? "",
                    Property = property,
                    RequestDateTime = DateTime.Now,
                    ContentUpdateRequestDetails = new List<ContentUpdateRequestDetail>()
                };

                PropertiesService.AddContentUpdateRequest(newRequest);

                foreach (string newDetailText in inStorage)
                {
                    this.PropertiesService.AddContentUpdateRequestDetail(newRequest, model.updateContentCode, newDetailText);
                }
            }

            return RedirectToAction("Places");
        }
        


        /// <summary>
        /// Displays reviews list for all the Property
        /// </summary>
        /// <param name="id">Property id</param>
        /// <returns></returns>
        [Authorize(Roles = "Owner, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Property, "id")]
        public ActionResult Reviews(int id)
        {
            GuestReviewsModel model = new GuestReviewsModel();

            Property property = this.PropertiesService.GetPropertyById(id);
            var rating = this.PropertiesService.GetPropertyReviewRate(id);
            if (rating.HasValue)
                model.PropertyRating = rating.Value;

            model.Property = property;
            model.GuestReviews = this.PropertiesService.GetPropertyGuestReviews(id);

            return View(model);
        }

        /// <summary>
        /// Displays rates & availability for unit
        /// </summary>
        /// <param name="id">Unit id</param>
        /// <returns></returns>
        [Authorize(Roles = "Owner, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Unit, "id")]
        public ActionResult Rates(int id)
        {
            var user = this.StateService.CurrentUser;
            var unit = this.UnitsService.GetUnitById(id);
            var property = unit.Property;

            if (property.User.UserID != user.UserID)
                return RedirectToAction("Places");

            var model = new RatesModel()
                {
                    Unit = unit,
                    Property = property,
                    PropertyId = property.PropertyID,
                    PropertyCurrency = property.DictionaryCulture.ISOCurrencySymbol
                };
            return View(model);
        }

        [Authorize(Roles = "Owner, Property Manager")]
        [HttpPost]
        public ActionResult ChangeRates(ChangeRateModel model)
        {
            try
            {
                if (model.UnitObjectsToDelete != null && model.UnitObjectsToDelete.Count() > 0)
                    UnitsService.DeleteUnitRates(model.UnitObjectsToDelete);

                if (model.UnitObjectsToUpdate.Count() == 1)
                {
                    UnitRate unitRate = model.UnitObjectsToUpdate.First();
                    unitRate.Unit = UnitsService.GetUnitById(model.UnitId);

                    if (unitRate.DateFrom > unitRate.DateUntil)
                    {
                        return Json(new { Valid = false, Message = Resources.Homeowner.Rates.RatesDateFromIsLaterTHanDateUntil });
                    }
                    else
                    {
                        UnitsService.UpdateUnitRates(model.UnitId, unitRate, StateService.CurrentUser.UserID);
                    }
                }
                else
                {
                    return Json(new { Valid = false, Message = Resources.Homeowner.Rates.TooManyRatesToUpdate });
                }

                return Json(new { Valid = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DateRangeOverlapException)
            {
                return Json(new { Valid = false, Message = Resources.Homeowner.Rates.RatesCannotOverlapEachOther });
            }
        }

        [Authorize(Roles = "Owner, Property Manager")]
        [HttpPost]
        public ActionResult ChangeMloses(ChangeMlosesModel model)
        {
            try
            {
                if (model.UnitObjectsToDelete != null && model.UnitObjectsToDelete.Count() > 0)
                    UnitsService.DeleteMloses(model.UnitObjectsToDelete);

                UnitsService.UpdateMloses(model.UnitId, model.UnitObjectsToUpdate);

                return Json(new { Valid = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DateRangeOverlapException)
            {
                return Json(new { Valid = false, Message = Resources.Homeowner.Rates.RatesCannotOverlapEachOther });
            }
        }

        [Authorize(Roles = "Owner, Property Manager")]
        [HttpPost]
        public ActionResult ChangeCtas(ChangeCtasModel model)
        {
            try
            {
                if (model.UnitObjectsToDelete != null && model.UnitObjectsToDelete.Count() > 0)
                    UnitsService.DeleteCtas(model.UnitObjectsToDelete);

                UnitsService.UpdateCtas(model.UnitId, model.UnitObjectsToUpdate);

                return Json(new { Valid = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DateRangeOverlapException)
            {
                return Json(new { Valid = false, Message = Resources.Homeowner.Rates.RatesCannotOverlapEachOther });
            }
        }

        /// <summary>
        /// Uploads files from contentupdate request to storage, and adds data to tables.
        /// Modified tables contentUpdateDescription.
        /// </summary>
        [Authorize(Roles = "Owner, Property Manager")]
        [HttpPost]
        public ActionResult UploadFiles(int propertyId)
        {
            var r = new List<UploadFilesResult>();

            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadFilesResult>();
                var headers = Request.Headers;

                this.PropertiesService.UploadFileToStorage(Request, statuses, propertyId);

                JsonResult result = Json(statuses);
                result.ContentType = "text/plain";

                return result;
            }

            return Json(r);
        }

        [Authorize]
        [HttpPost]
        public void DeleteAfterError(int propertyId, string fileNamesRaw)
        {

            IEnumerable<string> filenames = fileNamesRaw != null ? fileNamesRaw.TrimEnd(';').Split(';').AsEnumerable() : new List<string>();
            this.BlobService.InitContainer(SettingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersContentUpdateRequests));
            foreach (string name in filenames)
            {
                this.BlobService.DeleteFile(propertyId, name);
            }

        }

        //private void UploadFile(HttpRequestBase request, List<ViewDataUploadFilesResult> statuses, int propertyId)
        //{
        //    for (int i = 0; i < request.Files.Count; i++)
        //    {
        //        var file = request.Files[i];



        //        var name =file.FileName;


        //        FileInfo info = new FileInfo(name);
        //        int j = 0;
        //        this.BlobService.InitContainer("contentupdates");

        //        while (this.BlobService.FileExists(propertyId, name))
        //        {
        //            j++;
        //            name = file.FileName.Replace(info.Extension, "_" + j + info.Extension);
        //        }


        //        string blobContainter = this.ConfigurationService.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl);

        //        this.BlobService.AddFile(file.InputStream, propertyId, name,file.ContentType);

        //        statuses.Add(new ViewDataUploadFilesResult()
        //        {
        //            name = name,
        //            size = file.ContentLength,
        //            type = file.ContentType,
        //            //url = "/HomeOwner/Download/" + name,
        //            //delete_url = "/HomeOwner/Delete/" + name,
        //            thumbnail_url = String.Format("{0}/{1}/id{2}/{3}", blobContainter, "contentupdates", propertyId, name),
        //            //delete_type = "GET"
        //        });
        //    }
        //}

        /// <summary>
        /// Retrieves unit blockings for specific date range
        /// </summary>
        /// <param name="id">Unit id</param>
        /// <param name="fromDate">Start date</param>
        /// <param name="untilDate">End date</param>
        [Authorize(Roles = "Owner, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Unit, "id")]
        public JsonResult GetUnitBlockings(int id, DateTime fromDate, DateTime untilDate)
        {
            var unitBlockings = UnitsService.GetUnitBlockings(id, fromDate, untilDate);

            var transformedUnitBlockings =
                    from unitInvBlocking in unitBlockings
                    select new
                    {
                        unitInvBlocking.UnitInvBlockingID,
                        DateFrom = unitInvBlocking.DateFrom.ToShortDateString(),
                        DateUntil = unitInvBlocking.DateUntil.ToShortDateString(),
                        unitInvBlocking.Type,
                        GuestName =  unitInvBlocking.Reservation != null ?  unitInvBlocking.Reservation.User.Firstname + " " + unitInvBlocking.Reservation.User.Lastname : "",
                        Confirmation = unitInvBlocking.Reservation != null ? unitInvBlocking.Reservation.ConfirmationID : "",
                        OwnerComment = !string.IsNullOrEmpty(unitInvBlocking.Comment) ? unitInvBlocking.Comment : ""
                    };


            return Json(transformedUnitBlockings.ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Retrieves rates for specific date range
        /// </summary>
        /// <param name="id">Unit id</param>
        /// <param name="fromDate">Start date</param>
        /// <param name="untilDate">End date</param>
        [Authorize(Roles = "Owner, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Unit, "id")]
        public JsonResult GetRates(int id, DateTime fromDate, DateTime untilDate)
        {
            var rates = UnitsService.GetUnitRates(id, fromDate, untilDate);
            var transformedRates =
                    from rate in rates
                    select new
                    {
                        rate.UnitRateID,
                        DateFrom = rate.DateFrom.ToShortDateString(),
                        DateUntil = rate.DateUntil.ToShortDateString(),
                        rate.DailyRate,
                    };
            return Json(transformedRates.ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Retrieves mlos for specific date range
        /// </summary>
        /// <param name="id">Unit id</param>
        /// <param name="fromDate">Start date</param>
        /// <param name="untilDate">End date</param>
        [Authorize(Roles = "Owner, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Unit, "id")]
        public JsonResult GetMlos(int id, DateTime fromDate, DateTime untilDate)
        {
            var mloses = UnitsService.GetUnitMlos(id, fromDate, untilDate);
            var transformedRates =
                    from mlos in mloses
                    select new
                    {
                        mlos.UnitTypeMLOSID,
                        mlos.MLOS,
                        DateFrom = mlos.DateFrom.ToShortDateString(),
                        DateUntil = mlos.DateUntil.ToShortDateString(),
                    };


            return Json(transformedRates.ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Retrieves cta for specific date range
        /// </summary>
        /// <param name="id">Unit id</param>
        /// <param name="fromDate">Start date</param>
        /// <param name="untilDate">End date</param>
        [Authorize(Roles = "Owner, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Unit, "id")]
        public JsonResult GetCta(int id, DateTime fromDate, DateTime untilDate)
        {
            var ctas = UnitsService.GetCTAs(id, fromDate, untilDate);
            var transformedRates =
                    from cta in ctas
                    select new
                    {
                        cta.UnitTypeCTAID,
                        cta.Monday,
                        cta.Tuesday,
                        cta.Wednesday,
                        cta.Thursday,
                        cta.Friday,
                        cta.Saturday,
                        cta.Sunday,
                        DateFrom = cta.DateFrom.ToShortDateString(),
                        DateUntil = cta.DateUntil.ToShortDateString(),
                    };


            return Json(transformedRates.ToList(), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Owner, Property Manager")]
        public ActionResult Contact()
        {
            this.ViewBag.isPostBack = false;
            return View();
        }

        /// <summary>
        /// Posted contact form with written message.
        /// </summary>
        /// <param name="message">Homeowner message.</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Owner, Property Manager")]
        public ActionResult Contact(string message)
        {
            if (string.IsNullOrEmpty(message))
                return View();

            //TODO: email should have from display name - email of currently logged homeowner
            string emailAddr = this.SettingsService.GetSettingValue(SettingKeyName.EmailAccountHomeownersTicketSystem);

            HomeownerContactEmail email = new HomeownerContactEmail(emailAddr);
            email.Message = message;
            email.DisplayFrom = this.StateService.CurrentUser.Email;
            this.MessageService.AddEmail(email, CultureCode.en_US);

            this.ViewBag.isPostBack = true;

            return View();
        }

        [Authorize(Roles = "Ownerm, Property Manager")]
        [IsOwnedBy(OwnedObjectType.Property, "id")]
        public ActionResult Transactions(int? id)
        {
            var user = this.StateService.CurrentUser;
            var reservationList = this.ReservationService.GetReservationTransactionsForHomeOwner(user.UserID, id);
            var model = new TransactionModel()
            {
                ReservationsList = reservationList
            };

            return View(model);
        }

        /// <summary>
        /// Displays Key Management form.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Owner, Property Manager, Key Manager")]
        public ActionResult KeyManagement()
        {
            KeyManagementModel model = new KeyManagementModel();
            model.KeyManagementList = this.GetKeyManagementReservationsByLoogedUser();

            return View(model);
        }

        /// <summary>
        /// Sets keys delivered to guest in reservation
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Owner, Property Manager, Key Manager")]
        [IsOwnedBy(OwnedObjectType.Reservation, "reservationId")]
        public ContentResult KeyManagementSetKeysDeliveredToGuest(int reservationId)
        {
            DateTime now = DateTime.Now;
            Reservation reservation = this.ReservationService.GetReservationById(reservationId);
            reservation.KeysDeliveredToGuestDateTime = now;
            return Content(now.ToLocalizedDateTimeString());
        }

        /// <summary>
        /// Sets keys recived from guest in the reservation
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Owner, Property Manager, Key Manager")]
        [ValidateAntiForgeryToken]
        [IsOwnedBy(OwnedObjectType.Reservation, "reservationId")]
        public ContentResult KeyManagementSetKeysRecievedFromGuest(int reservationId)
        {
            DateTime now = DateTime.Now;
            Reservation reservation = this.ReservationService.GetReservationById(reservationId);
            reservation.KeysReceivedFromGuestDateTime = now;
            return Content(now.ToLocalizedDateTimeString());
        }

        /// <summary>
        /// Retrieves data for key management form on filter selection
        /// </summary>
        /// <param name="Confirmation">Reservation confrimation id</param>
        /// <param name="GuestLastName">Guest last name</param>
        /// <param name="PropertyName">Property name - current language</param>
        /// <returns></returns>
        [Authorize(Roles = "Owner, Property Manager, Key Manager")]
        public PartialViewResult GetFilteredKeyManagementData(string Confirmation, string GuestLastName, string PropertyName)
        {
            var result = this.GetKeyManagementReservationsByLoogedUser();
            if (!string.IsNullOrEmpty(Confirmation))
            {
                result = result.Where(r => r.Reservation.ConfirmationID.ToUpper().Equals(Confirmation.ToUpper()));
            }
            if (!string.IsNullOrEmpty(GuestLastName))
            {
                result = result.Where(r => r.Guest.Lastname.ToUpper().Contains(GuestLastName.ToUpper()));
            }
            if (!string.IsNullOrEmpty(PropertyName))
            {
                result = result.Where(r => r.Property.PropertyNameCurrentLanguage.ToUpper().Contains(PropertyName.ToUpper()));
            }

            return PartialView("Partial/KeyManagentListView", result.ToList());
        }

        [HttpGet]
        public ActionResult ListYourHome()
        {
            this.StateService.RemoveFromSession(SessionKeys.HomeownerBasicData.ToString());
            this.StateService.RemoveFromSession(SessionKeys.HomeownerSchedulingMeetingData.ToString());

            UserProfileModel model = new UserProfileModel()
            {
                SignUpCode = this.StateService.GetFromSession<string>(SessionKeys.HomeownerSignUpCode.ToString())
            };

            ViewBag.Countries = new SelectList(this.DictionaryCountryService.GetCountries(), "CountryCode2Letters", "CountryNameCurrentLanguage");
            ViewBag.TopNavigationSection = new MvcHtmlString(string.Format("<h1>{0}</h1>", ExtranetApp.Resources.Views.HomeOwner.ListYourHome_Title));

            return View(model);
        }

        [HttpPost]
        public ActionResult Confirm(UserProfileModel model)
        {
            return PartialView("JoinConfirmation", model);
        }


        [HttpPost]
        public ActionResult ListYourHome(UserProfileModel model)
        {
            ViewBag.TopNavigationSection = new MvcHtmlString(string.Format("<h1>{0}</h1>", ExtranetApp.Resources.Views.HomeOwner.ListYourHome_Title));

            if (ModelState.IsValid)
            {
                this.StateService.AddToSession(SessionKeys.HomeownerBasicData.ToString(), model);
                return View("ScheduleMeeting");
            }
            else
            {
                ViewBag.Countries = new SelectList(this.DictionaryCountryService.GetCountries(), "CountryCode2Letters", "CountryNameCurrentLanguage");
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult JoinConfirmation(SchedulingMeeting model)
        {
            return RedirectToAction("JoinConfirmation", "Homeowner");
        }

        public ActionResult ScheduleMeeting()
        {
            ViewBag.TopNavigationSection = new MvcHtmlString(string.Format("<h1>{0}</h1>", ExtranetApp.Resources.Views.HomeOwner.SchedulingMeeting_Title));
            return View();
        }

        public ActionResult JoinConfirmation()
        {
            UserProfileModel model = this.StateService.GetFromSession<UserProfileModel>(SessionKeys.HomeownerBasicData.ToString());
            SchedulingMeeting meetingData = this.StateService.GetFromSession<SchedulingMeeting>(SessionKeys.HomeownerSchedulingMeetingData.ToString());
            string tempPassword = String.Empty;

            if (model != null)
            {
                ScheduledVisit visit = new ScheduledVisit();
                UserRole userRole = null;
                User existingUser = null;

                User user = new DataAccess.User();
                user.email = model.Email;
                user.Firstname = model.FirstName;
                user.Lastname = model.LastName;
                user.Address1 = model.AddressLine1;
                user.Address2 = model.AddressLine2;
                user.City = model.City;
                user.State = model.State;
                user.ZIPCode = model.ZIPCode;
                user.CellPhone = model.PhoneNumber;
                user.DictionaryCountry = this.DictionaryCountryService.GetCountryByCountryCode(model.CountryCode2Letters);
                user.DictionaryCulture = this.DictionaryCultureService.GetCultureByCode(LocalizationHelper.GetUserBrowserCulture());
                tempPassword = this.AuthorizationService.GeneratePassword();
                user.Password = MD5Helper.Encode(tempPassword);
                user.AcceptedTCsInitials = model.TCInitials;
                user.SignUpCode = model.SignUpCode;

                existingUser = this.UserService.GetUserByEmail(model.Email);
                if (existingUser != null)
                {
                    userRole = this.UserService.GetUserRoleByLevel(user, RoleLevel.Guest);
                    if (userRole != null)
                    {
                        return Json(new { Valid = false, Message = Account.Error_UsersExists });
                    }
                    else //User exists but not in Owner role - Adding role for existing user
                    {
                        user = this.UserService.MergeUserData(existingUser, user);
                    }
                }
                else
                {
                    this.UserService.AddHomeowner(user);
                }

                userRole = this.RolesService.AddUserToRole(user, RoleLevel.Owner);
                userRole.Status = (int)UserStatus.Requested;
                userRole.ActivationToken = Guid.NewGuid().ToString();
                user.UserRoles.Add(userRole);

                visit.User = user;
                visit.DateCreated = DateTime.Now;
                visit.HourFrom = DateTime.Parse(meetingData.DateFrom).Hour;
                visit.HourUntil = DateTime.Parse(meetingData.DateTo).Hour;
                visit.DatesList = new List<DateTime>(meetingData.Dates.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(d => DateTime.Parse(d)));

                this.ScheduledVisitsService.AddVisit(visit);

                #region Mail to Ticket System

                List<string> ticketSystemJoiningAddress = this.SettingsService.GetSettingValue(SettingKeyName.TicketSystemJoinEmailAddress).Replace(";", ",").Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
                HomeownerJoiningTicketSystemEmail mailToTicket = new HomeownerJoiningTicketSystemEmail(ticketSystemJoiningAddress);
                mailToTicket.AddressLine1 = model.AddressLine1;
                mailToTicket.AddressLine2 = model.AddressLine2;
                mailToTicket.City = model.City;
                mailToTicket.Country = user.DictionaryCountry.CountryNameCurrentLanguage;
                mailToTicket.FirstName = model.FirstName;
                mailToTicket.FullName = string.Format("{0} {1}", model.FirstName, model.LastName);
                mailToTicket.Initials = model.TCInitials;
                mailToTicket.LastName = model.LastName;
                mailToTicket.PhoneNumber = model.PhoneNumber;
                mailToTicket.PreferredDates = String.Join(", ", visit.DatesList.Select(d => d.ToLocalizedDateString()));
                mailToTicket.PreferredTimeFrom = meetingData.DateFrom;
                mailToTicket.PreferredTimeTo = meetingData.DateTo;
                mailToTicket.State = model.State;
                mailToTicket.ZipCode = model.ZIPCode;
                mailToTicket.Email = model.Email;
                this.MessageService.AddEmail(mailToTicket, CultureCode.en_US);

                #endregion

                OwnerJoiningEmail ownerJoiningEmail = new OwnerJoiningEmail(model.Email);
                ownerJoiningEmail.UserFirstName = model.FirstName;
                ownerJoiningEmail.PhoneNumber = model.PhoneNumber;
                ownerJoiningEmail.PreferredDates = String.Join(", ", visit.DatesList.Select(d => d.ToLocalizedDateString()));
                ownerJoiningEmail.PreferredTimeFrom = meetingData.DateFrom;
                ownerJoiningEmail.PreferredTimeTo = meetingData.DateTo;
                ownerJoiningEmail.Sender = MessageSender.Support;
                this.MessageService.AddEmail(ownerJoiningEmail, user.DictionaryCulture.CultureCode);

                this.StateService.RemoveFromSession(SessionKeys.HomeownerBasicData.ToString());

                ViewBag.Email = model.Email;
                return View();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ScheduleMeeting(SchedulingMeeting model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Valid = false, Message = HomeOwner.Req_ChooseDates }, JsonRequestBehavior.AllowGet);
            }
            this.StateService.AddToSession(SessionKeys.HomeownerSchedulingMeetingData.ToString(), model);
            return Json(new { Valid = true, Message = Url.RouteUrl(new { controller = "HomeOwner", action = "JoinConfirmation" }) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddUnitBlockade(AddUnitBlockadeModel model)
        {
            if (ModelState.IsValid)
            {
                UnitsService.AddAndAdjustUnitBlockade(
                    new UnitInvBlocking()
                        {
                            DateFrom = model.DateFrom,
                            DateUntil = model.DateTo,
                            Unit = UnitsService.GetUnitById(model.UnitId),
                            Type = (int)model.UnitBlockingType,
                            Comment = model.Comment
                        });

                return Json(new { Valid = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Valid = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Activate(string id)
        {
            ActivateOwnerBankAccountRelatedModel model = new ActivateOwnerBankAccountRelatedModel()
            {
                ActivationToken = id
            };

            ViewBag.TopNavigationSection = new MvcHtmlString(string.Format("<h1>{0}</h1>", ExtranetApp.Resources.Views.Account.Activation_Title));
            return View(model);
        }

        [HttpPost]
        public ActionResult Activate(ActivateOwnerBankAccountRelatedModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.TopNavigationSection = new MvcHtmlString(string.Format("<h1>{0}</h1>", ExtranetApp.Resources.Views.Account.Activation_Title));
                return View(model);
            }

            UserRole userRole = this.UserService.GetUserRoleByActivationToken(model.ActivationToken);
            if (userRole == null || userRole.Status != (int)UserStatus.Inactive || userRole.Role.RoleLevel != (int)RoleLevel.Owner)
            {
                TempData["Message"] = Account.Error_TokenInvalid;
            }
            else if (userRole.Status == (int)UserStatus.Inactive)
            {
                TempData["Message"] = Account.Info_AccountActivated;
                userRole.Status = (int)UserStatus.Active;
                userRole.ActivationToken = null;
                User user = userRole.User;
                user.BankName = Base64.Encode(model.BankName);
                user.RoutingNumber = Base64.Encode(model.RoutingNumber);
                user.AccountNumber = Base64.Encode(model.AccountNumber);
            }
            return RedirectToAction("ActivationHomeowner", "Account");
        }

        #region PrivateMembers

        /// <summary>
        /// Converts Data Entity object to Model object
        /// </summary>
        /// <param name="up">Data Entity object</param>
        /// <returns></returns>
        private OwnerUpdateProfileModel GetProfileModel(User userData)
        {
            OwnerUpdateProfileModel pm = new OwnerUpdateProfileModel()
            {
                AddressLine1 = userData.Address1,
                City = userData.City,
                Country = userData.DictionaryCountry == null ? 0 : userData.DictionaryCountry.CountryId,
                CountryCode2Letters = userData.DictionaryCountry == null ? string.Empty : userData.DictionaryCountry.CountryCode2Letters,
                Email = userData.email,
                FirstName = userData.Firstname,
                LastName = userData.Lastname,
                PhoneNumber = userData.CellPhone,
                State = userData.State,
                ZIPCode = userData.ZIPCode,
                CountryName = userData.DictionaryCountry == null ? String.Empty : userData.DictionaryCountry.CountryNameCurrentLanguage,
                UserId = userData.UserID,
                BankName = Base64.Decode(userData.BankName),
                AccountNumber = Base64.Decode(userData.AccountNumber),
                RoutingNumber = Base64.Decode(userData.RoutingNumber)
            };

            return pm;
        }

        /// <summary>
        /// Converts Model object to Data Entity object
        /// </summary>
        /// <param name="up">Model object</param>
        /// <returns></returns>
        private User UpdateUserData(OwnerUpdateProfileModel up)
        {
            var user = this.UserService.GetUserByUserId(up.UserId);
            user.Address1 = up.AddressLine1;
            user.CellPhone = up.PhoneNumber;
            user.City = up.City;
            user.Firstname = up.FirstName;
            user.Lastname = up.LastName;
            user.State = up.State;
            user.ZIPCode = up.ZIPCode;
            user.DictionaryCountry = this.DictionaryCountryService.GetCountryByCountryCode(up.CountryCode2Letters);
            user.BankName = Base64.Encode(up.BankName);
            user.RoutingNumber = Base64.Encode(up.RoutingNumber);
            user.AccountNumber = Base64.Encode(up.AccountNumber);

            return user;
        }

        private List<Property> GetPropertiesListForLoggedUser()
        {
            SessionUser sessionUser = this.StateService.CurrentUser;
            SessionRole sessionRole = sessionUser.LoggedInRole;
            List<Property> propertyList = new List<Property>();

            if (sessionRole.RoleLevel == (int)RoleLevel.Owner)
            {
                propertyList = this.PropertiesService.GetPropertiesByOwnerId(sessionUser.UserID, true).ToList();
            }
            else
            {
                propertyList = this.PropertiesService.GetPropertiesByManager(sessionUser.UserID, (RoleLevel)sessionRole.RoleLevel, true).ToList();
            }

            return propertyList;
        }

        private IEnumerable<DataAccess.CustomObjects.KeyManagementData> GetKeyManagementReservationsByLoogedUser()
        {
            SessionUser sessionUser = this.StateService.CurrentUser;
            SessionRole sessionRole = sessionUser.LoggedInRole;
            List<KeyManagementData> reservationList = new List<KeyManagementData>();

            if (sessionRole.RoleLevel == (int)RoleLevel.Owner)
            {
                reservationList = this.ReservationService.GetKeyManagementReservationsByHomeowner(sessionUser.UserID);
            }
            else
            {
                reservationList = this.ReservationService.GetKeyManagementReservationsByManagerRole(sessionUser.UserID, (RoleLevel)sessionRole.RoleLevel);
            }

            return reservationList;
        }

        #endregion
    }
}
