﻿using BusinessLogic.Managers;
using BusinessLogic.ServiceContracts;
using ExtranetApp.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using ExtranetApp.Security;

namespace ExtranetApp.Controllers
{
    [HttpsRequired(RequireSecure = false)]
    public class BaseController : Controller
    {
        #region Private members

        private readonly Dictionary<Type, object> services;

        #endregion Private members

        #region Ctor

        public BaseController(Dictionary<Type, object> services)
        {
            this.services = services;
        }

        #endregion

        #region Services

        public IAuthorizationService AuthorizationService
        {
            get
            {
                return (IAuthorizationService)services[typeof(IAuthorizationService)];
            }
        }

        public IUserService UserService
        {
            get
            {
                return (IUserService)services[typeof(IUserService)];
            }
        }

        public IRolesService RolesService
        {
            get
            {
                return (IRolesService)services[typeof(IRolesService)];
            }
        }

        public IAmenityGroupsService AmenityGroupsService
        {
            get
            {
                return (IAmenityGroupsService)services[typeof(IAmenityGroupsService)];
            }
        }

        public IAmenityService AmenityService
        {
            get
            {
                return (IAmenityService)services[typeof(IAmenityService)];
            }
        }

        public IAppliancesService AppliancesService
        {
            get
            {
                return (IAppliancesService)services[typeof(IAppliancesService)];
            }
        }

        public IBlobService BlobService
        {
            get
            {
                return (IBlobService)services[typeof(IBlobService)];
            }
        }

        public IConfigurationService ConfigurationService
        {
            get
            {
                return (IConfigurationService)services[typeof(IConfigurationService)];
            }
        }

        public IDestinationsService DestinationsService
        {
            get
            {
                return (IDestinationsService)services[typeof(IDestinationsService)];
            }
        }

        public IDictionaryCountryService DictionaryCountryService
        {
            get
            {
                return (IDictionaryCountryService)services[typeof(IDictionaryCountryService)];
            }
        }
        
        public IEmailService EmailService
        {
            get
            {
                return (IEmailService)services[typeof(IEmailService)];
            }
        }

        public IEventLogService EventLogService
        {
            get
            {
                return (IEventLogService)services[typeof(IEventLogService)];
            }
        }

        public IMessageService MessageService
        {
            get
            {
                return (IMessageService)services[typeof(IMessageService)];
            }
        }

        public IPropertiesService PropertiesService
        {
            get
            {
                return (IPropertiesService)services[typeof(IPropertiesService)];
            }
        }

        public IPropertyAddOnsService PropertyAddOnsService
        {
            get
            {
                return (IPropertyAddOnsService)services[typeof(IPropertyAddOnsService)];
            }
        }

        public IPropertyTypesService PropertyTypesService
        {
            get
            {
                return (IPropertyTypesService)services[typeof(IPropertyTypesService)];
            }
        }

        public ISalesPersonsService SalesPersonsService
        {
            get
            {
                return (ISalesPersonsService)services[typeof(ISalesPersonsService)];
            }
        }

        public ISettingsService SettingsService
        {
            get
            {
                return (ISettingsService)services[typeof(ISettingsService)];
            }
        }

        public IStateService StateService
        {
            get
            {
                return (IStateService)services[typeof(IStateService)];
            }
        }

        public IStorageQueueService StorageQueueService
        {
            get
            {
                return (IStorageQueueService)services[typeof(IStorageQueueService)];
            }
        }

        public IStorageTableService StorageTableService
        {
            get
            {
                return (IStorageTableService)services[typeof(IStorageTableService)];
            }
        }

        public ITagsService TagsService
        {
            get
            {
                return (ITagsService)services[typeof(ITagsService)];
            }
        }

        public ITagGroupsService TagGroupsService
        {
            get
            {
                return (ITagGroupsService)services[typeof(ITagGroupsService)];
            }
        }

        public ITaxesService TaxesService
        {
            get
            {
                return (ITaxesService)services[typeof(ITaxesService)];
            }
        }

        public IUnitsService UnitsService
        {
            get
            {
                return (IUnitsService)services[typeof(IUnitsService)];
            }
        }

        public IUnitTypesService UnitTypesService
        {
            get
            {
                return (IUnitTypesService)services[typeof(IUnitTypesService)];
            }
        }

        public IScheduledVisitsService ScheduledVisitsService
        {
            get
            {
                return (IScheduledVisitsService)services[typeof(IScheduledVisitsService)];
            }
        }

        public IBookingService BookingService
        {
            get
            {
                return (IBookingService)services[typeof(IBookingService)];
            }
        }

        public IReservationsService ReservationService
        {
            get
            {
                return (IReservationsService)services[typeof(IReservationsService)];
            }
        }

        public IDocumentService DocumentService
        {
            get
            {
                return (IDocumentService)services[typeof(IDocumentService)];
            }
        }


        public IPaymentGatewayService PaymentGatewayService
        {
            get
            {
                return (IPaymentGatewayService)services[typeof(IPaymentGatewayService)];
            }
        }

        public IScheduledTasksService ScheduledTaskService
        {
            get
            {
                return (IScheduledTasksService)services[typeof(IScheduledTasksService)];
            }
        }

        public ITaskSchedulerEventLogService TaskSchedulerEventLogService
        {
            get
            {
                return (ITaskSchedulerEventLogService)services[typeof(ITaskSchedulerEventLogService)];
            }
        }

        public IDictionaryCultureService DictionaryCultureService
        {
            get
            {
                return (IDictionaryCultureService)services[typeof(IDictionaryCultureService)];
            }
        }

        public IZohoCrmService ZohoService
        {
            get
            {
                return (IZohoCrmService)services[typeof(IZohoCrmService)];
            }
        }

        public ICrmOperationLogsService CrmOperationsLogService
        {
            get
            {
                return (ICrmOperationLogsService)services[typeof(ICrmOperationLogsService)];
            }
        }

        #endregion

        #region Controller overrides

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string currentCulture = string.Empty;
            if (SessionManager.IsAuthenticated())
                currentCulture = SessionManager.CurrentUser.CultureCode;
            else
                currentCulture = LocalizationHelper.GetUserBrowserCulture();

            CultureInfo ci = new CultureInfo(currentCulture);
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            base.OnActionExecuting(filterContext);
        }

        #endregion
    }
}