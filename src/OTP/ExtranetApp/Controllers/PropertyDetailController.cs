﻿using BusinessLogic.Enums;
using BusinessLogic.Managers;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using ExtranetApp.Models;
using ExtranetApp.Models.PropertyDetails;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Controllers
{
    public class PropertyDetailController : BaseController
    {
        #region Ctor

        public PropertyDetailController(IPropertiesService propertiesService, 
            IUnitsService unitsService, 
            IAmenityGroupsService amenityGroupsService,
            IConfigurationService configurationService,
            IPropertyAddOnsService propertyAddOnsService,
            ITaxesService taxesService,
            IBookingService bookingService,
            IAppliancesService appliancesService)
            : base(new Dictionary<Type, object>() 
                {
                    { typeof(IPropertiesService), propertiesService }, 
                    { typeof(IUnitsService), unitsService }, 
                    { typeof(IAmenityGroupsService), amenityGroupsService }, 
                    { typeof(IConfigurationService), configurationService }, 
                    { typeof(IPropertyAddOnsService), propertyAddOnsService }, 
                    { typeof(ITaxesService), taxesService }, 
                    { typeof(IBookingService), bookingService },
                    { typeof(IAppliancesService), appliancesService },
                })
        {
        }

        #endregion

        #region Views

        /// <summary>
        /// Shows property details page
        /// </summary>
        /// <param name="id">Unit id of the selected property</param>
        /// <param name="token">Sign Off token</param>
        /// <returns>Property details view</returns>
        public ActionResult PropertyDetails(int id = 1, string token = "")
        {
            int numberOfUnits = 0;

            PropertyDetailsModel model = new PropertyDetailsModel();                       

            model.StorageUrl = this.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray());
            model.Unit = this.UnitsService.GetUnitById(id);
            Int32 propertyId = this.PropertiesService.GetPropertyIdByUnitId(id);
            Property property = this.PropertiesService.GetPropertyById(propertyId, out numberOfUnits);

            /// We can only show property that is active
            if (!property.PropertyLive)
            {
                if (!token.Equals(property.SignOffCode))
                {
                    throw new Exception("Property is no active! You cannot visit it yet!");
                }
            } 
            
            model.NumberOfPropertyUnits = numberOfUnits;
            var staticPropertyInfo = this.PropertiesService.GetPropertyStaticContentInfo(propertyId).AsEnumerable();
            var staticUnitInfo = this.UnitsService.GetUnitStaticContentInfo(id).AsEnumerable();

            model.PropertyRating = property.StarRating;

            model.Property = property;
            var images = GetPropertyStaticContentByType(PropertyStaticContentType.FullScreen, staticPropertyInfo, false);
            var galleryImages = GetPropertyStaticContentByType(PropertyStaticContentType.GalleryImage, staticPropertyInfo, false);
            string blobContainter = this.ConfigurationService.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl);
            model.PropertyImages = images.Select(s => string.Format("{0}{1}", blobContainter, s));
            model.GalleryImages = galleryImages.Select(s => string.Format("{0}{1}", blobContainter, s));
            model.SquareFootage = GetPropertyStaticContentByType(PropertyStaticContentType.SquareFootage, staticPropertyInfo, true).FirstOrDefault();
            model.KitchenDesc = GetUnitStaticContentByType(UnitStaticContentType.KitchenAndLaundry, staticUnitInfo, true);
            model.BathroomsDesc = GetUnitStaticContentByType(UnitStaticContentType.Bathrooms, staticUnitInfo, true);
            model.BedroomsDesc = GetUnitStaticContentByType(UnitStaticContentType.Bedrooms, staticUnitInfo, true);
            model.LivingArea = GetUnitStaticContentByType(UnitStaticContentType.LivingAreasAndDining, staticUnitInfo, true);
            model.Features = GetPropertyStaticContentByType(PropertyStaticContentType.Feature, staticPropertyInfo, true);
            model.DistanceFormAttractions = GetPropertyStaticContentByType(PropertyStaticContentType.DistanceToMainAttraction, staticPropertyInfo, true);

            model.ApplianceGroups = this.AppliancesService.GetApplianceGroups().ToList();
            model.Appliances = this.UnitsService.GetUnitAppliances(id).ToList();
            model.GuestReviews = this.PropertiesService.GetPropertyGuestReviews(propertyId);
            model.FloorPlans = this.PropertiesService.GetPropertyFloorPlans(propertyId);
            model.DestinationDetails = model.Property.Destination;

            var propertyUnitName = new StringBuilder( model.Property.PropertyNameCurrentLanguage );

            if (model.NumberOfPropertyUnits > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }

            model.PropertyUnitName = propertyUnitName.ToString();


            //var groups = this.PropertiesService.GetAmenityGroupsByPropertyId(propertyId);
            var amenities = this.PropertiesService.GetPropertyAmenitiesByPropertyId(propertyId).ToList();
            model.Amenities = GetAmenitiesForAmenityGroups( amenities);
            return View(model);
        }

        /// <summary>
        /// View redendered for homeowner link to preview property in frontend
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult SignOffConfirm(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new Exception("No token to validate");
            }

            Property property = this.PropertiesService.GetPropertyBySingOffToken(id);
            if (property == null)
            {
                throw new Exception("Cannot retrieve property - token is invalid!");
            }

            SignOffModel model = new SignOffModel()
            {
                Property = property,
                SignOffToken = id
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignOffConfirm(SignOffModel model)
        {
            Property property = this.PropertiesService.GetPropertyBySingOffToken(model.SignOffToken);
            if (property == null)
            {
                throw new Exception("Cannot retrieve property - token is invalid!");
            }
            property.SignOffCode = string.Empty;
            property.PropertyLive = true;

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Shows 360 view
        /// </summary>
        /// <param name="id">Property id</param>
        /// <param name="panoramaId">Panorama id</param>
        /// <returns></returns>
        public ActionResult PanoramaView(int id, int panoramaId)
        {
            string blobContainter = this.ConfigurationService.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl).TrimEnd("/".ToCharArray());            
            var floorPlans = this.PropertiesService.GetPropertyFloorPlans(id);            

            PanoramaModel model = new PanoramaModel()
            {
                Panorama = floorPlans.SelectMany(f => f.Panoramas).Where(s => s.Id.Equals(panoramaId)).SingleOrDefault(),
                FloorPlan = floorPlans.Where(f => f.Panoramas.Any(s => s.Id.Equals(panoramaId))).SingleOrDefault(),
                Property = this.PropertiesService.GetPropertyById(id),
                StorageUrl = blobContainter
            };
            
            return View(model);
        }        

        #endregion

        #region PartialViews

        /// <summary>
        /// Retrieves Guest Reviews sorted by parameter
        /// </summary>
        /// <param name="propertyId">Id of the property</param>
        /// <param name="sortType">Type of the sort - direction and column</param>
        /// <returns>Property Guest Reviews Partial View</returns>
        public PartialViewResult GetSortedPropertyReviews(int propertyId, string sort = "")
        {
            SortByType sortType = String.IsNullOrEmpty(sort) ? SortByType.TimeDesc : ((SortByType)Enum.Parse(typeof(SortByType), sort, true));

            var guestReviews = this.PropertiesService.GetPropertyGuestReviews(propertyId);
            SortGuestReviewsBy(sortType, ref guestReviews);

            return PartialView("Partial/GuestReviews_ReviewList", guestReviews);
        }

        /// <summary>
        /// Retrieves Invoice Details for currently selected Unit
        /// </summary>
        /// <param name="unitId">Id of the Unit</param>
        /// <param name="dateFrom">Date of customer arrival</param>
        /// <param name="dateUntil">Date of the customer leave</param>
        /// <returns>Partial View with invoice details data</returns>
        public PartialViewResult GetInvoiceDetails(int unitId, int propertyId, DateTime dateFrom, DateTime dateUntil)
        {
            var result = this.BookingService.CalculateInvoice(propertyId, unitId, dateFrom, dateUntil);


            return PartialView("Partial/PropertyDetails_InvoiceSection", result);
        }

        /// <summary>
        /// Retrieves Partial view InvoiceSection for given invoice data
        /// </summary>
        /// <param name="InvoiceDetails">Calculated invoice data</param>
        /// <returns>Partial View with invoice details data</returns>
        public PartialViewResult GetInvoiceDetailsByInvoice(DataAccess.CustomObjects.PropertyDetailsInvoice invoice)
        {
            return PartialView("Partial/PropertyDetails_InvoiceSection", invoice);
        }

        #endregion

        #region JSONResponses

        /// <summary>
        /// Retrieves UnitPrice and MLOS of the Unit by its ID
        /// </summary>
        /// <param name="id">Unit ID</param>
        /// <param name="dateFrom">Selected date from</param>
        /// <param name="dateUntil">Selected date until</param>
        /// <param name="culture">Culture of the property</param>
        /// <returns>Additional Property/Unit properties available for data available only after page load</returns>
        public JsonResult PropertyDetailsByDate(int id, DateTime? dateFrom, DateTime? dateUntil, String culture)
        {            
            var unitMos = this.UnitsService.GetUnitMlos(id, dateFrom, dateUntil);
            var price = this.UnitsService.GetUnitPrice(id, dateFrom, dateUntil);

            if (dateFrom.HasValue && dateUntil.HasValue)
            {
                TimeSpan diff = dateUntil.Value - dateFrom.Value;
                if (diff.Days!=0)
                    price = price / (diff.Days);
            }

            var priceString = (dateFrom.HasValue && dateUntil.HasValue) ? Resources.Home.PropertyDetails.ViewMainHeaderPricePerNight : Resources.Home.PropertyDetails.ViewMainHeaderPriceFrom;
            var anynomObj = new
            {
                Price = price.HasValue ? String.Format(priceString, price.Value.ToString("c", new CultureInfo(culture))) : String.Empty,
                UnitMOS = unitMos.Count() == 0 ? String.Empty : String.Format(Resources.Home.PropertyDetails.MinimumStayLength, unitMos.Max(m=>m.MLOS))
            };

            return Json(anynomObj, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Retrieves data for the unit availability calendar
        /// </summary>
        /// <param name="id">Unit id</param>
        /// <param name="fromDate">Start date</param>
        /// <param name="untilDate">End date</param>
        /// <returns>List of the availability data</returns>
        public JsonResult GetPropertyAvailabilityInfoForMonth(int id, DateTime fromDate, DateTime untilDate)
        {
            DateTime startOfTheMonth = fromDate.AddMonths(-1).AddDays(1 - fromDate.Day); //first day of the month (the month before)
            DateTime endOfTheMonth = new DateTime(untilDate.Year, untilDate.Month, DateTime.DaysInMonth(untilDate.Year, untilDate.Month)); //last day of the month (current)

            var list = UnitsService.GetUnitAvailability(id, startOfTheMonth, endOfTheMonth);

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region PrivateMembers

        /// <summary>
        /// Retrieves concrete static content connected to the property by it's code
        /// </summary>
        /// <param name="contentType">Static content type code</param>
        /// <param name="staticPropertyInfo">List containing all the static content values</param>
        /// <param name="isI18n">If i18n translation should be use, in practise it means to get value form th ContentValue or ContentValue_i18n fields</param>
        /// <returns>Specyfic content type defined by content code or empty list if no values were found</returns>
        private IEnumerable<String> GetPropertyStaticContentByType(PropertyStaticContentType contentType, IEnumerable<PropertyStaticContentResult> staticPropertyInfo, bool isI18n)
        {
            var r = staticPropertyInfo.Where(s => s.ContentCode.Equals((int)contentType)).SingleOrDefault();
            return r == null ? new List<string>() : r.ContentItems.Select(s => isI18n ? s.ContentValueCurrentLanguage : s.ContentValue);
        }

        /// <summary>
        /// Retrieves concrete static content connected to the property by it's code
        /// </summary>
        /// <param name="contentType">Static content type code</param>
        /// <param name="staticPropertyInfo">List containing all the static content values</param>
        /// <param name="isI18n">If i18n translation should be use, in practise it means to get value form th ContentValue or ContentValue_i18n fields</param>
        /// <returns>Specyfic content type defined by content code or empty list if no values were found</returns>
        private IEnumerable<String> GetUnitStaticContentByType(UnitStaticContentType contentType, IEnumerable<UnitStaticContentResult> staticUnitInfo, bool isI18n)
        {
            var r = staticUnitInfo.Where(s => s.ContentCode.Equals((int)contentType)).SingleOrDefault();
            return r == null ? new List<string>() : r.ContentItems.Select(s => isI18n ? s.ContentValueCurrentLanguage : s.ContentValue);
        }

        /// <summary>
        /// Performs sor opertaion on guest reviews by sort type
        /// </summary>
        /// <param name="sortType">Sort order by column and direction</param>
        /// <param name="guestReviews">List of guest reviews to sort</param>
        private void SortGuestReviewsBy(SortByType sortType, ref IEnumerable<GuestReview> guestReviews)
        {
            switch (sortType)
            {
                case SortByType.TimeDesc:
                    guestReviews = guestReviews.OrderByDescending(p => p.ReviewDate);
                    break;

                case SortByType.RatingDesc:
                    guestReviews = guestReviews.OrderByDescending(p => p.OverallRating);
                    break;

                case SortByType.RatingAsc:
                    guestReviews = guestReviews.OrderBy(p => p.OverallRating);
                    break;
            }
        }

        /// <summary>
        /// Retrieves amenity groups and property amenities for that groups
        /// </summary>
        /// <param name="groups">List of amenity groups for the property</param>
        /// <param name="amenities">List of property amenity groups for the property</param>
        /// <returns></returns>
        private IEnumerable<PropertyAmenities> GetAmenitiesForAmenityGroups(IEnumerable<PropertyAmenity> amenities)
        {
            PropertyAmenities others = null;
            List<PropertyAmenities> _amenities = new List<PropertyAmenities>();            
            AmenityGroup agOthers = this.AmenityGroupsService.GetOtherAmenityGroupById();
            List<AmenityGroup> groups = amenities.Select(a => a.Amenity.AmenityGroup).Distinct().ToList();
            foreach (AmenityGroup ag in groups)
            {
                PropertyAmenities propertyAmenity = new PropertyAmenities()
                {
                    AmenityGroupName = ag.NameCurrentLanguage,
                    Amenities = amenities
                                .Where(p => p.Amenity.AmenityGroup != null && p.Amenity.AmenityGroup.AmenityGroupId == ag.AmenityGroupId)
                                .Select(a => a.Amenity.TitleCurrentLanguage + (((a.Quantity.HasValue && a.Quantity.Value > 1) == true) ? " (" + a.Quantity.Value.ToString() + ")" : ""))
                                .ToList(),
                    Icon = ag.AmenityIcon,
                    Position = ag.Position
                };

                if(propertyAmenity.AmenityGroupName == agOthers.NameCurrentLanguage)
                {
                    others = propertyAmenity;
                    continue;
                }
                _amenities.Add(propertyAmenity);
            }
            foreach (var am in _amenities)
            {
                am.Amenities.Sort();
            }
            _amenities = _amenities.OrderBy(a => a.Position).ToList();
            if (others != null)
            {
                _amenities.Add(others);
            }
            return _amenities;
        }

        #endregion
    }
}
