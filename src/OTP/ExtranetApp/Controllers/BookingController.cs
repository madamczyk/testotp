﻿using BusinessLogic.Enums;
using BusinessLogic.EVS;
using BusinessLogic.Objects;
using BusinessLogic.Objects.Session;
using BusinessLogic.Objects.Templates.Document;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.ServiceContracts;
using Common.Exceptions;
using Common.Extensions;
using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using ExtranetApp.Models.Account;
using ExtranetApp.Models.Booking;
using ExtranetApp.Resources.Booking;
using ExtranetApp.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace ExtranetApp.Controllers
{
    [Authorize(Roles = "Guest")]
    public class BookingController : BaseController
    {
        public const string BOOK_CONFIRMATION_ERROR_MESSAGE = "CONFIRMATION_ERROR_MESSAGE";
        public const string BOOK_CONFIRMATION_ID = "BookingConfirmationId";
        public const string BOOK_DATA_INFO = "BookingDataInfo";
        public const string BOOK_PAYMENT_ERROR_MESSAGE = "PAYMENT_ERROR_MESSAGE";
        public const string BOOK_QUESTION_DATA = "BookingIdentityVerificationInfo";
        public const string BOOK_RESERVATION_ID = "BookingReservationId";
        public const string BOOK_RESPONSE_EVS = "BookingResponseEVS";
        public const string BOOK_SKIP_EVS = "BookingSkipEVS";
        public const string BOOK_TEMP_PAYMENT_RESULT = "BookingTempPaymentResult";
        #region Ctor

        public BookingController(IPropertiesService propertiesService, IStateService stateService,
            IUnitsService unitService, IContextService httpContextService,
            IBookingService bookingService, IUserService userService,
            IDictionaryCountryService dictionaryService,
            IMessageService messageService, ISettingsService settingsService,
            IDocumentService documentService,
            IPaymentGatewayService paymentGatewayService,
            IReservationsService reservationService,
            IAuthorizationService authorizationService)
            : base(new Dictionary<Type, object>() {
            { typeof(IPropertiesService), propertiesService },
            { typeof(IStateService), stateService },
            { typeof(IUnitsService), unitService },
            { typeof(IContextService), httpContextService },
            { typeof(IBookingService), bookingService },
            { typeof(IUserService), userService },
            { typeof(IDictionaryCountryService), dictionaryService },
            { typeof(IMessageService), messageService },
            { typeof(IDocumentService), documentService },
            { typeof(ISettingsService), settingsService },
            { typeof(IPaymentGatewayService), paymentGatewayService },
            { typeof(IReservationsService), reservationService },
            { typeof(IAuthorizationService), authorizationService }
            })
        {
        }

        #endregion Ctor

        #region Views

        /// <summary>
        /// Invoked when user is under 25 years old and checkbox age confirmation is chcecked
        /// </summary>
        /// <returns></returns>
        [HttpsRequired(RequireSecure = true)]
        public ActionResult AgeVerificationFail()
        {
            TempData["InfoMessage"] = BookingProcess.Payment_AgeTermsValidationMessage;
            String emailAddr = SettingsService.GetSettingValue(SettingKeyName.EVSFailNotifyEmail);
            SessionUser sessionUsr = this.StateService.CurrentUser;
            var user = this.UserService.GetUserByUserId(sessionUsr.UserID);

            PaymentAgeVerificationEmail email = new PaymentAgeVerificationEmail(emailAddr)
            {
                UserName = user.FullName,
                BirthDate = user.BirthDate.Value.ToLocalizedDateString()
            };
            MessageService.AddEmail(email, CultureCode.en_US);

            CleanBookingSessionVaribles();

            //logout user
            this.AuthorizationService.SingOutUser();
            this.StateService.CurrentUser = null;

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Redirects to correct view depending on the current user logged status
        /// </summary>
        /// <param name="bookingModel">Booking information data</param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult BookProperty(BookingInfoModel bookingModel)
        {
            this.StateService.AddToSession(BOOK_DATA_INFO, bookingModel);

            SessionUser usr = this.StateService.CurrentUser;

            if (!IsInvoiceRatesValid(bookingModel))
            {
                return RedirectToAction("InvoiceValueFailure");
            }

            if (usr != null && usr.LoggedInRole.RoleLevel == (int)RoleLevel.Guest)
            {
                return RedirectToAction("Contact");
            }
            else
            {
                return RedirectToAction("Welcome");
            }
        }

        /// <summary>
        /// Confirmation View
        /// </summary>
        /// <returns></returns>
        [HttpsRequired(RequireSecure = true)]
        public ActionResult Confirmation()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            PaymentResult paymentGateWayResult = TempData[BOOK_TEMP_PAYMENT_RESULT] as PaymentResult;
            BookingProcessModel bookingProcessModel = GetBookProcesCommonData(bookingInfo);
            string confirmationId;

            if (paymentGateWayResult == null)
            {
                return RedirectToAction("PropertyNotAvailable");
            }

            SessionUser sessionUsr = this.StateService.CurrentUser;
            var user = this.UserService.GetUserByUserId(sessionUsr.UserID);
            var model = GetBookProcesCommonData(bookingInfo);

            model.GuestProfile = GetProfileModel(user);
            model.InvoiceData = this.BookingService.CalculateInvoice(bookingInfo.PropertyId, bookingInfo.UnitId, bookingInfo.dateTripFrom, bookingInfo.dateTripUntil);
            model.PaymentGateWayResult = paymentGateWayResult;
            bookingProcessModel.InvoiceData = model.InvoiceData;

            confirmationId = !string.IsNullOrWhiteSpace(paymentGateWayResult.BookingConfirmationID) ? paymentGateWayResult.BookingConfirmationID : "-";
            this.StateService.AddToSession(BOOK_CONFIRMATION_ID, confirmationId);

            Reservation reservation = this.ReservationService.GetReservationById(this.StateService.GetFromSession<int>(BOOK_RESERVATION_ID));
            SendNotifications(bookingProcessModel, reservation);

            ViewBag.BOOK_PAYMENT_ERROR_MESSAGE = TempData[BOOK_PAYMENT_ERROR_MESSAGE];
            ViewBag.ReservationId = reservation.ReservationID;

            //CleanBookingSessionVaribles();

            return View(model);
        }

        /// <summary>
        /// Contact view
        /// </summary>
        /// <returns></returns>
        [HttpsRequired(RequireSecure = true)]
        public ActionResult Contact()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            SessionUser sessionUsr = this.StateService.CurrentUser;
            var user = this.UserService.GetUserByUserId(sessionUsr.UserID);

            if (!this.BookingService.IsUnitAvailable(bookingInfo))
            {
                return RedirectToAction("PropertyNotAvailable");
            }

            int reservationId = this.BookingService.DoTemporaryReservartion(bookingInfo, user); //performs temporary booking for defined period of time for example 15 minutes
            this.StateService.AddToSession(BOOK_RESERVATION_ID, reservationId);

            var model = GetBookProcesCommonData(bookingInfo);            
            model.GuestProfile = GetProfileModel(user);

            ViewBag.Countries = new SelectList(this.DictionaryCountryService.GetCountries(), "CountryCode2Letters", "CountryNameCurrentLanguage");
            return View(model);
        }

        /// <summary>
        /// Invoked on user click "Next" on the Contact Form - save user data to DB
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [HttpsRequired(RequireSecure = true)]
        public ActionResult Contact(UserProfileMinModel model, BookingProcessModel processModel)
        {
            if (ModelState.IsValid)
            {
                User usr = UpdateUserData(model);
                int reservationId = this.StateService.GetFromSession<int>(BOOK_RESERVATION_ID);
                this.BookingService.UpdateReservationGuestNumber(processModel.NumberOfGuests.Value, reservationId);

                //user was previously verified no further verificaion is needed
                if (IsUserIDVerified(usr))
                    return RedirectToAction("Payment");
                else // there was no positive validation yet
                    return RedirectToAction("IdentityVerification");
            }

            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            SessionUser sessionUsr = this.StateService.CurrentUser;
            var user = this.UserService.GetUserByUserId(sessionUsr.UserID);
            var viewModel = GetBookProcesCommonData(bookingInfo);
            viewModel.GuestProfile = GetProfileModel(user);
            ViewBag.Countries = new SelectList(this.DictionaryCountryService.GetCountries(), "CountryCode2Letters", "CountryNameCurrentLanguage");

            return View(viewModel);
        }

        /// <summary>
        /// Identity Verification View
        /// </summary>
        /// <returns></returns>
        [HttpsRequired(RequireSecure = true)]
        public ActionResult IdentityVerification()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            SessionUser sessionUsr = this.StateService.CurrentUser;

            var model = GetBookProcesCommonData(bookingInfo);
            IEVSResponse evsResponse = null;

            User usr = this.UserService.GetUserByUserId(sessionUsr.UserID);

            if (usr.DictionaryCountry.CountryCode2Letters == "us") //if user is from US - try to get some question verification data
            {
                var questionModelList = GetIdentityQuestions(sessionUsr.UserID, out evsResponse);
                if (questionModelList == null) //this means service error or no questions were found for user
                {
                    TempData[BOOK_SKIP_EVS] = true; // send info to view about lack of the verification data
                    return RedirectToAction("Payment");
                }

                this.StateService.AddToSession(BOOK_QUESTION_DATA, questionModelList);
                this.StateService.AddToSession(BOOK_RESPONSE_EVS, evsResponse);

                int takeQuestionCount = 1;
                if (questionModelList.Count() > 1)
                    takeQuestionCount = 2;

                model.IdentityVerification = questionModelList.Take(takeQuestionCount).ToList();

                return View(model);
            }
            else // if not then ask service for result code
            {
                var response = this.BookingService.GetEVSInternational(sessionUsr.UserID) as EVSResponseInternational;

                // TODO do something with result data

                TempData[BOOK_SKIP_EVS] = true; // send info to view about lack of the verification data so no need to display EVS
                return RedirectToAction("Payment");
            }
        }

        /// <summary>
        /// On question answered
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [HttpsRequired(RequireSecure = true)]
        public ActionResult IdentityVerification(BookingProcessModel model)
        {
            List<IdentityVerificationQuestionModel> webQuestions = this.StateService.GetFromSession<List<IdentityVerificationQuestionModel>>(BOOK_QUESTION_DATA);
            SessionUser sessionUsr = this.StateService.CurrentUser;
            var user = this.UserService.GetUserByUserId(sessionUsr.UserID);

            foreach (var question in model.IdentityVerification)
            {
                var webQuestion = webQuestions.Where(s => s.QuestionType.Equals(question.QuestionType)).First();
                var correctAnswerId = webQuestion.Answers.Where(s => s.IsCorrect == true).Select(s => s.AnswerId).First();

                // CHECK IF VALID IF NOT SHOW 2 ADDITIONAL QUESTIONS
                if (correctAnswerId != question.SelectedAnswer) //selected wrong answer show additional question
                {
                    if (user.VerificationPositive == false) //on the first verification User.VerificationPositive should be null
                    {
                        this.BookingService.SetVerificationInfo(user, false, this.StateService.GetFromSession<IEVSResponse>(BOOK_RESPONSE_EVS));
                        this.BookingService.BlockUserAfterWrongIDVerification(user);
                        return RedirectToAction("IdentityVerificationFailure");
                    }
                    this.BookingService.SetVerificationInfo(user, false, this.StateService.GetFromSession<IEVSResponse>(BOOK_RESPONSE_EVS));

                    ModelState.Clear();
                    ModelState.AddModelError("WrongAnswer", BookingProcess.IdentityVerification_WrongAnswer);

                    BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
                    var newModel = GetBookProcesCommonData(bookingInfo);

                    if (webQuestions.Count() > 3) // at least 4 questions were available
                    {
                        newModel.IdentityVerification = webQuestions.Skip(2).Take(2).ToList(); //take next 2 questions
                    }
                    else if (webQuestions.Count() > 2) // at least 3 questions were available
                    {
                        newModel.IdentityVerification = webQuestions.Skip(1).Take(2).ToList(); //take q=2 and q=3 questions
                    }
                    else // max 2 qustions are available
                    {
                        newModel.IdentityVerification = webQuestions.ToList();
                    }

                    return View(newModel);
                }
            }

            this.BookingService.SetVerificationInfo(user, true, this.StateService.GetFromSession<IEVSResponse>(BOOK_RESPONSE_EVS));

            return RedirectToAction("Payment");
        }

        /// <summary>
        /// View - showed on the second wrog identity verification
        /// </summary>
        /// <returns></returns>
        [HttpsRequired(RequireSecure = true)]
        public ActionResult IdentityVerificationFailure()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            var model = GetBookProcesCommonData(bookingInfo);

            CleanBookingSessionVaribles();

            return View(model);
        }

        /// <summary>
        /// Shows error page with info about invoice of value 0
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult InvoiceValueFailure()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            var model = GetBookProcesCommonData(bookingInfo);

            ViewBag.Error_Ttile = BookingProcess.InvoiceRateNotSet_Title;
            ViewBag.Error_Subtitle = BookingProcess.InvoiceRateNotSet_Description;

            CleanBookingSessionVaribles();

            return View("WizardFailure", model);
        }

        /// <summary>
        /// Payement View
        /// </summary>
        /// <returns></returns>
        [HttpsRequired(RequireSecure = true)]
        public ActionResult Payment()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            var model = GetBookProcesCommonData(bookingInfo);
            SessionUser sessionUsr = this.StateService.CurrentUser;
            var user = this.UserService.GetUserByUserId(sessionUsr.UserID);
            var userCreditCardList = this.BookingService.GetUserCreditCardTypes(user.UserID);
            int reservationId = this.StateService.GetFromSession<int>(BOOK_RESERVATION_ID);

            if (!IsUserIDVerified(user) && TempData[BOOK_SKIP_EVS] == null) //check if someone is not trying to type some url in browser bar
            {
                return RedirectToAction("PaymentFailure");
            }

            PaymentGatewaySettings paymentGatewaySettings = new PaymentGatewaySettings()
            {
                ApiLogin = this.SettingsService.GetSettingValue(SettingKeyName.PaymentApiLogin),
                ApiSecret = this.SettingsService.GetSettingValue(SettingKeyName.PaymentApiSecret),
                RedirectURL = this.SettingsService.GetSettingValue(SettingKeyName.PaymentRedirectURL)
            };

            model.BillingAddress = GetBillingAddressModel(user);
            model.PaymentData = new PaymentModel();
            model.PaymentData.CreditCardSelection = (userCreditCardList.Count() > 0) ? CreditCardSelection.Existing : CreditCardSelection.New;
            model.PaymentGatewaySettings = paymentGatewaySettings;

            ViewBag.UserCreditCards = new SelectList(userCreditCardList, "GuestPaymentMethodID", "CreditCardLast4Digits");
            ViewBag.CreditCardList = this.BookingService.GetCreditCardTypes();
            ViewBag.Countries = new SelectList(this.DictionaryCountryService.GetCountries(), "CountryCode2Letters", "CountryNameCurrentLanguage");
            ViewBag.Months = new SelectList(Enumerable.Range(1, 12));
            ViewBag.Years = new SelectList(Enumerable.Range(DateTime.Now.Year, 10));
            Reservation resv = this.ReservationService.GetReservationById(reservationId);
            ViewBag.Reservation = resv;
            ViewBag.MostRestrictiveArrivalDate = resv.DateArrival;
            ViewBag.PAYMENT_ERROR_MESSAGE = TempData[BOOK_PAYMENT_ERROR_MESSAGE];

            return View(model);
        }

        /// <summary>
        /// On payment acceptance / when using existing credit card
        /// </summary>
        /// <param name="billingAddrModel">Billing addres data</param>
        /// <param name="paymentModel">Guest payment data</param>
        /// <param name="termsVerification">Acceptance of the terms and age</param>
        /// <returns></returns>
        [HttpPost]
        [HttpsRequired(RequireSecure = true)]
        public ActionResult Payment(UserBillingAddress billingAddrModel, PaymentAgeTermsVerification termsVerification, PaymentModel paymentModel)
        {
            try
            {
                PaymentResult paymentGateWayResult = PreparePaymentOperations(billingAddrModel, termsVerification, paymentModel);
                if (paymentGateWayResult.PaymentGatewayResult) //show payment form once again
                {
                    TempData[BOOK_TEMP_PAYMENT_RESULT] = paymentGateWayResult;
                    return RedirectToAction("Confirmation");
                }

                TempData[BOOK_SKIP_EVS] = true;
                TempData[BOOK_PAYMENT_ERROR_MESSAGE] = paymentGateWayResult.GatewayResponse.Message;
                return RedirectToAction("Payment");
            }
            catch (SecurityDepositException)
            {
                return RedirectToAction("PaymentFailure");
            }
            catch (PaymentGatewayException)
            {
                return RedirectToAction("PaymentFailure");
            }            
        }

        /// <summary>
        /// Invoked on payment exception
        /// </summary>
        /// <returns></returns>
        [HttpsRequired(RequireSecure = true)]
        public ActionResult PaymentFailure()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            var model = GetBookProcesCommonData(bookingInfo);

            ViewBag.Error_Ttile = BookingProcess.PaymentFailure_Title;
            ViewBag.Error_Subtitle = BookingProcess.PaymentFailure_Subtitle;

            CleanBookingSessionVaribles();

            return View("WizardFailure", model);
        }

        /// <summary>
        /// Invoked by Spreedly Core service on redirect
        /// </summary>
        /// <param name="paymentToken">Spreedly Core Payment method Refernce Token</param>
        /// <returns></returns>
        [HttpsRequired(RequireSecure = true)]
        public ActionResult PaymentGatewayRedirect(string token)
        {
            int reservationId = this.StateService.GetFromSession<int>(BOOK_RESERVATION_ID);
            var result = this.PaymentGatewayService.GetPaymentAdditionalData(token, reservationId);
            SessionUser sessionUsr = this.StateService.CurrentUser;

            UserBillingAddress billingAddrModel = new UserBillingAddress();
            billingAddrModel.AddressLine1 = result.AdditionalData.Address;
            billingAddrModel.City = result.AdditionalData.City;
            billingAddrModel.Country = result.AdditionalData.CountryId;
            billingAddrModel.LastName = result.AdditionalData.LastName;
            billingAddrModel.State = result.AdditionalData.State;
            billingAddrModel.ZIPCode = result.AdditionalData.ZipCode;
            billingAddrModel.UserId = sessionUsr.UserID;
            billingAddrModel.FirstName = result.AdditionalData.FirstName;

            PaymentAgeTermsVerification termsVerification = new PaymentAgeTermsVerification();
            termsVerification.UserInitials = result.AdditionalData.Initials;
            termsVerification.TermsReadVerificaction = true;
            termsVerification.AgeVerification = true;

            PaymentModel paymentModel = new PaymentModel();
            paymentModel.CreditCardholderId = Int32.Parse(result.AdditionalData.CreditHolderId);
            paymentModel.CreditCardNumber = result.CreditCardNumber;
            paymentModel.ReferenceToken = result.Token;
            paymentModel.CreditCardSelection = CreditCardSelection.Existing;
            paymentModel.UserId = sessionUsr.UserID;

            GuestPaymentMethod payment = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = paymentModel.CreditCardNumber,
                ReferenceToken = paymentModel.ReferenceToken
            };

            bool paymentMethodRetained = false;

            try
            {
                paymentModel.GuestPaymentMethodId = this.BookingService.SetGuestPaymentMethod(payment, paymentModel.UserId, paymentModel.CreditCardholderId.Value, reservationId, out paymentMethodRetained);

                PaymentResult paymentGatewayResult = PreparePaymentOperations(billingAddrModel, termsVerification, paymentModel);
                if (paymentGatewayResult.PaymentGatewayResult) //everything is ok go to confirmation
                {
                    TempData[BOOK_TEMP_PAYMENT_RESULT] = paymentGatewayResult;
                    if (!paymentMethodRetained)
                    {
                        TempData[BOOK_CONFIRMATION_ERROR_MESSAGE] = BookingProcess.Payment_CreditSavingError;
                        this.BookingService.RemoveGuestPaymentMethod(paymentModel.GuestPaymentMethodId.Value); // spreedly core didn't save this credit card
                    }
                    return RedirectToAction("Confirmation");
                }

                TempData[BOOK_SKIP_EVS] = true;
                TempData[BOOK_PAYMENT_ERROR_MESSAGE] = paymentGatewayResult.GatewayResponse.Message;
                return RedirectToAction("Payment"); //show payment once more with error message
            }
            catch (PaymentGatewayException)
            {
                return RedirectToAction("PaymentFailure");
            }
        }

        /// <summary>
        /// When state of the selected property is changed during (unavailable) booking wizard
        /// </summary>
        /// <returns></returns>
        [HttpsRequired(RequireSecure = true)]
        public ActionResult PropertyNotAvailable()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            var model = GetBookProcesCommonData(bookingInfo);

            ViewBag.Error_Ttile = BookingProcess.PropertyNotAvailable_Title;
            ViewBag.Error_Subtitle = BookingProcess.PropertyNotAvailable_Subtitle;

            CleanBookingSessionVaribles();

            return View("WizardFailure", model);
        }

        /// <summary>
        /// Return PDF file with invoice details
        /// </summary>
        /// <returns>File Stream as PDF</returns>
        public FileStreamResult SaveInvoiceDetailDoc()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            BookingProcessModel bookingProcessModel = GetBookProcesCommonData(bookingInfo);
            bookingProcessModel.InvoiceData = this.BookingService.CalculateInvoice(bookingInfo.PropertyId, bookingInfo.UnitId, bookingInfo.dateTripFrom, bookingInfo.dateTripUntil);
            SessionUser sessionUsr = this.StateService.CurrentUser;
            string confirmationId = this.StateService.GetFromSession<string>(BOOK_CONFIRMATION_ID);
            Reservation reservation = this.ReservationService.GetReservationById(this.StateService.GetFromSession<int>(BOOK_RESERVATION_ID));
            MemoryStream ms = new MemoryStream(reservation.Invoice);

            Response.AddHeader("Content-Disposition", "attachment; filename=" + String.Format(BookingProcess.Confirmation_InvoiceFileName, confirmationId));
            return new FileStreamResult(ms, "application/pdf");
        }

        /// <summary>
        /// Return PDF file with trip details
        /// </summary>
        /// <returns>File Stream as PDF</returns>
        public FileStreamResult SaveTripDetailDoc()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);
            BookingProcessModel bookingProcessModel = GetBookProcesCommonData(bookingInfo);
            bookingProcessModel.InvoiceData = this.BookingService.CalculateInvoice(bookingInfo.PropertyId, bookingInfo.UnitId, bookingInfo.dateTripFrom, bookingInfo.dateTripUntil);
            SessionUser sessionUsr = this.StateService.CurrentUser;
            string confirmationId = this.StateService.GetFromSession<string>(BOOK_CONFIRMATION_ID);
            Reservation reservation = this.ReservationService.GetReservationById(this.StateService.GetFromSession<int>(BOOK_RESERVATION_ID));

            TripDetails tripDetails = this.ReservationService.GenerateTripDetailsDocument(reservation, bookingProcessModel.InvoiceData);

            Stream file = this.DocumentService.CreateDocument(tripDetails, reservation.DictionaryCulture.CultureCode);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + String.Format(BookingProcess.Confirmation_SaveFileName, confirmationId));
            return new FileStreamResult(file, "application/pdf");
        }

        /// <summary>
        /// Welcome view when user is not logged in
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpsRequired(RequireSecure = true)]
        public ActionResult Welcome()
        {
            BookingInfoModel bookingInfo = this.StateService.GetFromSession<BookingInfoModel>(BOOK_DATA_INFO);

            if (!IsInvoiceRatesValid(bookingInfo))
            {
                return RedirectToAction("InvoiceValueFailure");
            }

            var model = GetBookProcesCommonData(bookingInfo);

            return View(model);
        }
        #endregion Views

        #region PrivateMembers

        /// <summary>
        /// Cleans all the data set up by Booking Wizard
        /// </summary>
        private void CleanBookingSessionVaribles()
        {
            this.StateService.RemoveFromSession(BOOK_RESERVATION_ID);
            this.StateService.RemoveFromSession(BOOK_QUESTION_DATA);
            this.StateService.RemoveFromSession(BOOK_DATA_INFO);
            this.StateService.RemoveFromSession(BOOK_RESPONSE_EVS);
        }

        /// <summary>
        /// Changes DataAccess.Model to UserBillingAddress
        /// </summary>
        /// <returns>User billing address model</returns>
        private UserBillingAddress GetBillingAddressModel(User userData)
        {
            UserBillingAddress pm = new UserBillingAddress()
            {
                AddressLine1 = userData.Address1,
                City = userData.City,
                Country = userData.DictionaryCountry == null ? string.Empty : userData.DictionaryCountry.CountryCode2Letters,
                FirstName = userData.Firstname,
                LastName = userData.Lastname,
                State = userData.State,
                ZIPCode = userData.ZIPCode,
                CountryName = userData.DictionaryCountry == null ? String.Empty : userData.DictionaryCountry.CountryNameCurrentLanguage,
                UserId = userData.UserID,
                BirthDay = userData.BirthDate
            };

            return pm;
        }

        /// <summary>
        /// Retrieves common data that is needed to every step of the Booking Wizard
        /// </summary>
        /// <param name="bookingInfo">Booking trip base information</param>
        /// <returns></returns>
        private BookingProcessModel GetBookProcesCommonData(BookingInfoModel bookingInfo)
        {
            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
           model.PropertyImages = this.PropertiesService.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen);
            model.Property = this.PropertiesService.GetPropertyById(model.BookingInfoData.PropertyId, true);
            model.Unit = this.UnitsService.GetUnitById(model.BookingInfoData.UnitId);
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();

            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);

            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }

            model.PropertyUnitName = propertyUnitName.ToString();

            return model;
        }

        /// <summary>
        /// Retrieve questions & answers from Service and convert to View model
        /// </summary>
        /// <param name="userId">Id of the user to verify</param>
        /// <returns></returns>
        private IList<IdentityVerificationQuestionModel> GetIdentityQuestions(int userId, out IEVSResponse evsResponse)
        {
            evsResponse = null;

            var questionModelList = new List<IdentityVerificationQuestionModel>();

            var response = (EVSResponse)this.BookingService.GetEVSQuestions(userId);

            if (response == null || response.UserIdentityData.Questions == null || response.UserIdentityData.Questions.Count() == 0)
                return null;

            var questions = response.UserIdentityData.Questions;
            questions.ToList().ForEach(delegate(Question q)
            {
                IdentityVerificationQuestionModel questionModel = new IdentityVerificationQuestionModel()
                {
                    QuestionText = q.QuestionText,
                    QuestionType = q.QuestionType
                };

                int aId = 1;

                q.Answers.ToList().ForEach(delegate(Answer a)
                {
                    IdentityVerificationAnswerModel answerModel = new IdentityVerificationAnswerModel()
                    {
                        AnswerId = aId,
                        AnswerText = a.AnswerText,
                        IsCorrect = a.IsCorrect
                    };
                    aId++;

                    questionModel.Answers.Add(answerModel);
                });

                questionModelList.Add(questionModel);
            });

            evsResponse = response;
            return questionModelList;
        }

        /// <summary>
        /// Changes DataAccess.Model to ProfileModel
        /// </summary>
        /// <returns>User profile model</returns>
        private UserProfileMinModel GetProfileModel(User userData)
        {
            UserProfileMinModel pm = new UserProfileMinModel()
            {
                AddressLine1 = userData.Address1,
                City = userData.City,
                Country = userData.DictionaryCountry == null ? string.Empty : userData.DictionaryCountry.CountryCode2Letters,
                Email = userData.email,
                FirstName = userData.Firstname,
                LastName = userData.Lastname,
                PhoneNumber = userData.CellPhone,
                State = userData.State,
                ZIPCode = userData.ZIPCode,
                CountryName = userData.DictionaryCountry == null ? String.Empty : userData.DictionaryCountry.CountryNameCurrentLanguage,
                UserId = userData.UserID,
                Gender = userData.Gender,
                BirthDay = userData.BirthDate
            };

            return pm;
        }
        /// <summary>
        /// Validation of the UnitRates and Invoice total amount
        /// </summary>
        /// <param name="bookingInfo">Booking info details</param>
        /// <returns>True if valid, false in other case</returns>
        private bool IsInvoiceRatesValid(BookingInfoModel bookingInfo)
        {
            bool result = true;

            TimeSpan totalDaysOfVacation = bookingInfo.dateTripUntil.Date - bookingInfo.dateTripFrom.Date;

            if (totalDaysOfVacation.Days < 1)
            {
                return false;
            }

            var invoice = this.BookingService.CalculateInvoice(bookingInfo.PropertyId, bookingInfo.UnitId, bookingInfo.dateTripFrom, bookingInfo.dateTripUntil);
            if (invoice.InvoiceTotal.Equals(0))
            {
                return false;
            }

            result = this.BookingService.CheckUnitRatesForPeriod(bookingInfo.UnitId, bookingInfo.dateTripFrom, bookingInfo.dateTripUntil);

            return result;
        }

        /// <summary>
        /// Helper to check if user is already verified
        /// </summary>
        /// <param name="usr"></param>
        /// <returns></returns>
        private bool IsUserIDVerified(User usr)
        {
            if (usr.VerificationPositive == true)
                return true;
            return false;
        }

        /// <summary>
        /// Prepares all the neccessery data for invoking payment
        /// </summary>
        /// <param name="billingAddrModel">Billing address data</param>
        /// <param name="termsVerification">Terms and age verification data</param>
        /// <param name="paymentModel">Payment data</param>
        /// <returns>Result of the payment process</returns>
        private PaymentResult PreparePaymentOperations(UserBillingAddress billingAddrModel, PaymentAgeTermsVerification termsVerification, PaymentModel paymentModel)
        {
            int reservationId = this.StateService.GetFromSession<int>(BOOK_RESERVATION_ID);
            SessionUser sessionUsr = this.StateService.CurrentUser;
            var user = this.UserService.GetUserByUserId(sessionUsr.UserID);

            // save user initials
            user.AcceptedTCs = DateTime.Now;
            user.AcceptedTCsInitials = termsVerification.UserInitials;

            ReservationBillingAddress address = new ReservationBillingAddress()
            {
                City = billingAddrModel.City,
                Firstname = billingAddrModel.FirstName,
                Lastname = billingAddrModel.LastName,
                State = billingAddrModel.State,
                Street = billingAddrModel.AddressLine1,
                Zip = billingAddrModel.ZIPCode,
                DictionaryCountry = this.DictionaryCountryService.GetCountryByCountryCode(billingAddrModel.Country)
            };

            int existingCreditCardId = paymentModel.GuestPaymentMethodId.Value;
            PaymentResult paymentGateWayResult = this.BookingService.PerformPayment(reservationId, billingAddrModel.UserId, address, existingCreditCardId);

            return paymentGateWayResult;
        }

        /// <summary>
        /// Sends all the notifications on booking wizard confirmation final step
        /// </summary>
        /// <param name="bookingProcessModel">All information about booking</param>
        /// <param name="reservation">Created reservation</param>
        private void SendNotifications(BookingProcessModel bookingProcessModel, Reservation reservation)
        {
            int reservationId = reservation.ReservationID;

            #region Send notification with agreement
            UserAgreement agreement = this.ReservationService.GenerateAgreementDocument(reservation, bookingProcessModel.InvoiceData);
            Invoice invoice = this.ReservationService.GenerateInvoiceDocument(reservation, bookingProcessModel.InvoiceData);
            TripDetails tripDetails = this.ReservationService.GenerateTripDetailsDocument(reservation, bookingProcessModel.InvoiceData);

            string subject = String.Format(BookingProcess.ConfirmationEmail_Subject, reservation.Property.PropertyNameCurrentLanguage, string.Format("{0} {1}", reservation.User.Firstname, reservation.User.Lastname));
            this.ReservationService.SendTripDetailEmails(subject, reservation, bookingProcessModel.InvoiceData, agreement, invoice, tripDetails);

            #endregion Send notification with agreement

            #region Save Agreement and Invoice to Reservation
            this.BookingService.AddReservationAgreement(reservationId, this.DocumentService.CreateDoc(agreement, reservation.Property.DictionaryCulture.CultureCode));
            this.BookingService.AddReservationInvoice(reservationId, this.DocumentService.CreateDoc(invoice, reservation.Property.DictionaryCulture.CultureCode));

            #endregion Save Agreement and Invoice to Reservation

            #region Send notification with key management information
            TimeSpan reservationBookArrivalDiff = reservation.DateArrival - reservation.BookingDate;
            if (reservationBookArrivalDiff.Days < 2) //this situation will not be handled by IntranetProcessor
            {
                if (reservation.Property.KeyProcessType == (int)KeyProcessType.Lockbox)
                {
                    this.ReservationService.SendCheckInLockboxEmail(reservation);
                }
                else if (reservation.Property.KeyProcessType == (int)KeyProcessType.KeyHandler)
                {
                    this.ReservationService.SendCheckInPickUpKeyHandlerEmail(reservation);
                }
            }
            #endregion
        }

        /// <summary>
        /// Changes ProfileModel to DataAccess.Model
        /// </summary>
        /// <param name="up"></param>
        /// <returns></returns>
        private User UpdateUserData(UserProfileMinModel up)
        {
            var user = this.UserService.GetUserByUserId(up.UserId);
            user.Address1 = up.AddressLine1;
            user.CellPhone = up.PhoneNumber;
            user.City = up.City;
            user.Firstname = up.FirstName;
            user.Lastname = up.LastName;
            user.State = up.State;
            user.ZIPCode = up.ZIPCode;
            user.DictionaryCountry = this.DictionaryCountryService.GetCountryByCountryCode(up.Country);
            user.Gender = up.Gender;
            user.BirthDate = up.BirthDay;
            return user;
        }
        #endregion PrivateMembers
    }
}