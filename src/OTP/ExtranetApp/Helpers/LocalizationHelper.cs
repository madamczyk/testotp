﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace ExtranetApp.Helpers
{
    public static class LocalizationHelper
    {
        /// <summary>
        /// Returns user's browser culture. If cannot retrive it, default culture will be returned(en-US)
        /// </summary>
        public static string GetUserBrowserCulture()
        {
            if (HttpContext.Current != null && HttpContext.Current.Request.UserLanguages != null && !string.IsNullOrEmpty(HttpContext.Current.Request.UserLanguages[0]))
            {
                return CultureInfo.CreateSpecificCulture(HttpContext.Current.Request.UserLanguages[0]).Name;
            }
            else
                return "en-US";
        }

        public static RegionInfo GetCurrentCultureRegionInfo()
        {
            try
            {
                return new RegionInfo(GetUserBrowserCulture());
            }
            catch (ArgumentException)
            {
                //if region is not specified, return null
                return null;
            }
        }
    }
}