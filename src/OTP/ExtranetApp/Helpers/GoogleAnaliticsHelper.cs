﻿using BusinessLogic.Enums;
using BusinessLogic.ServiceContracts;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Helpers
{
    /// <summary>
    /// Helper that retrieves correct Google Analitics tracing code depending on environment.
    /// </summary>
    public static class GoogleAnaliticsHelper
    {        
        /// <summary>
        /// Retrives Google Analitics tracing code from cache or setting ( GA key cache manager )
        /// </summary>
        /// <returns></returns>
        public static string GetGoogleAnaliticsTrackingCode()
        {
            IStateService stateService = (IStateService)DependencyResolver.Current.GetService(typeof(IStateService));            
            ISettingsService settingsService = (ISettingsService)DependencyResolver.Current.GetService(typeof(ISettingsService));

            string googleTrackingCode = stateService.GetFromCache<string>(CacheKeys.GoogleTrackingId.ToString());
            if (!string.IsNullOrEmpty(googleTrackingCode))
            {
                return googleTrackingCode;
            }

            /// If there is no key in cache reppopulate cache from OTPSettings database table
            googleTrackingCode = settingsService.GetSettingValue(SettingKeyName.WebAnaliticsGoogleTrackingId);
            stateService.AddToCache(CacheKeys.GoogleTrackingId.ToString(), googleTrackingCode, 86400);
            return googleTrackingCode;
        }
    }
}