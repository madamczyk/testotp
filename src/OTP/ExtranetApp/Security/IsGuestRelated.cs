﻿using BusinessLogic.ServiceContracts;
using Common.Exceptions;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ExtranetApp.Security
{
    /// <summary>
    /// Custom security validation - checking if object is in relation in Guest
    /// </summary>
    public sealed class IsGuestRelated : ActionFilterAttribute
    {
        readonly GuestRelationType relationType;
        readonly string parameterName;

        [Inject]
        public IReservationsService reservationService { get; set; }
        [Inject]
        public IStateService stateService { get; set; }

        // This is a positional argument
        public IsGuestRelated(GuestRelationType relationType, string parameterName = "id")
        {
            this.relationType = relationType;
            this.parameterName = parameterName;
        }

        public GuestRelationType OwnedObjectType
        {
            get { return relationType; }
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isRelatedTo = true;

            int object_id = (int)filterContext.ActionParameters[parameterName];

            var user = this.stateService.CurrentUser;

            switch (relationType)
            {
                case GuestRelationType.Reservation:
                    isRelatedTo = this.reservationService.GetReservationsByUserId(user.UserID).Any(r => r.ReservationID.Equals(object_id));
                    break;
                default:
                    break;
            }

            if (!isRelatedTo)
            {
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("action", "AccessDenied");
                redirectTargetDictionary.Add("controller", "Account");

                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            }
        }
    }

    public enum GuestRelationType
    {
        Reservation
    }
}