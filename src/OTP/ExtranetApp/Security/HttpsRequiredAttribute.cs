﻿using BusinessLogic.Enums;
using BusinessLogic.ServiceContracts;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp.Security
{
    /// <summary>
    /// Redirect to HTTP version of page if the SSL Security is not required and current context is it HTTPS
    /// </summary>
    public class HttpsRequiredAttribute : System.Web.Mvc.RequireHttpsAttribute
    {
        #region Fields

        [Inject]
        public ISettingsService settingsService { get; set; }
        private readonly string httpPrefix = "http://";
        public bool RequireSecure = false;
        #endregion

        #region Overriddes

        public override void OnAuthorization(System.Web.Mvc.AuthorizationContext filterContext)
        {
            if (RequireSecure)
            {
                // default RequireHttps functionality
                base.OnAuthorization(filterContext);
            }
            else
            {
                // non secure requested
                if (filterContext.HttpContext.Request.IsSecureConnection)
                {
                    HandleNonHttpRequest(filterContext);
                }
            }
        }

        #endregion

        #region Private Helpers

        protected virtual void HandleNonHttpRequest(AuthorizationContext filterContext)
        {
            // redirect to HTTP version of page
            if ( String.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase) && !filterContext.IsChildAction)
            {
                string extranetUrl = settingsService.GetSettingValue(SettingKeyName.ExtranetURL);
                string url = httpPrefix + extranetUrl + filterContext.HttpContext.Request.RawUrl;
                filterContext.Result = new RedirectResult(url);
            }
        }

        #endregion
    }
}