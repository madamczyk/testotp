﻿using BusinessLogic.Objects.Session;
using BusinessLogic.ServiceContracts;
using Common.Exceptions;
using DataAccess.Enums;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ExtranetApp.Security
{
    /// <summary>
    /// Custom security validation - checking for ownership of the object ( only for Homeowner & Property Manager ownership )
    /// </summary>
    public sealed class IsOwnedBy : ActionFilterAttribute
    {
        /// <summary>
        /// Type of object to set
        /// </summary>
        readonly OwnedObjectType ownedObjectType;
        /// <summary>
        /// Name of the request parameter "id"
        /// </summary>
        readonly string parameterName;

        [Inject]
        public IPropertiesService propertiesService { get; set; }
        [Inject]
        public IUnitsService unitsService { get; set; }
        [Inject]
        public IReservationsService reservationService { get; set; }
        [Inject]
        public IStateService stateService { get; set; }

        public IsOwnedBy(OwnedObjectType ownedObjectType, string parameterName = "id")
        {
            this.ownedObjectType = ownedObjectType;
            this.parameterName = parameterName;
        }

        public OwnedObjectType OwnedObjectType
        {
            get { return ownedObjectType; }
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isOwnedBy = true;

            int object_id = (int)filterContext.ActionParameters[parameterName];

            var user = this.stateService.CurrentUser;
            SessionRole loggedInRole = user.LoggedInRole;

            switch (ownedObjectType)
            {
                case Security.OwnedObjectType.Property:
                    if (loggedInRole.RoleLevel == (int)RoleLevel.Owner) { 
                        isOwnedBy = this.propertiesService.GetPropertiesByOwnerId(user.UserID, true).Any(p => p.PropertyID.Equals(object_id));
                    }
                    else
                    {
                        isOwnedBy = this.propertiesService.GetPropertiesByManager(user.UserID, RoleLevel.PropertyManager, true).Any(p => p.PropertyID.Equals(object_id));
                    }
                    break;
                case Security.OwnedObjectType.Unit:
                    if (loggedInRole.RoleLevel == (int)RoleLevel.Owner)
                    {
                        isOwnedBy = this.unitsService.GetUnitsByOwnerId(user.UserID, true).Any(u => u.UnitID.Equals(object_id));
                    }
                    else
                    {
                        isOwnedBy = this.unitsService.GetUnitsByManagerId(user.UserID, (RoleLevel)loggedInRole.RoleLevel, true).Any(p => p.UnitID.Equals(object_id));
                    }
                    break;
                case Security.OwnedObjectType.Reservation:
                    if (loggedInRole.RoleLevel == (int)RoleLevel.Owner)
                    {
                        isOwnedBy = this.reservationService.GetReservationsByHomeowner(user.UserID).Any(r => r.ReservationID.Equals(object_id));
                    }
                    else
                    {
                        isOwnedBy = this.reservationService.GetReservationsByManagerRole(user.UserID, (RoleLevel)loggedInRole.RoleLevel).Any(r => r.ReservationID.Equals(object_id));
                    }
                    break;

                default:
                    break;
            }

            if (!isOwnedBy)
            {
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("action", "AccessDenied");
                redirectTargetDictionary.Add("controller", "Account");

                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            }

        }
    }

    public enum OwnedObjectType
    {
        Property, Unit, Reservation
    }
}