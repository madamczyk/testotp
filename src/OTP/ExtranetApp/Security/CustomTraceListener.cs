﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic.Managers;
using DataAccess.Enums;
using BusinessLogic.ServiceContracts;
using Ninject;
using System.Web.Mvc;

namespace ExtranetApp.Security
{
    public class CustomTraceListener : System.Diagnostics.TraceListener
    {        
        private EventLogSource EventSource
        {
            get 
            {
                if (this.Name == "ExtranetAppTraceListener")
                    return EventLogSource.Extranet;
                else
                    return EventLogSource.Intranet;
            }
        }

        public override void Write(string message)
        {
            IEventLogService logService = (IEventLogService)DependencyResolver.Current.GetService(typeof(IEventLogService));

            logService.LogError(message, DateTime.Now, DataAccess.Enums.EventCategory.Warning.ToString(), EventSource);
        }

        public override void Write(object o)
        {
            Write(o.ToString());
        }

        public override void WriteLine(string message)
        {
            Write(message);
        }

        public override void WriteLine(object o)
        {
            Write(o.ToString());
        }

        public override void Write(string message, string category)
        {
            IEventLogService logService = (IEventLogService)DependencyResolver.Current.GetService(typeof(IEventLogService));

            logService.LogError(message, DateTime.Now, category, EventSource);
        }

        public override void Write(object o, string category)
        {
            Write(o.ToString(), category);
        }

        public override void WriteLine(string message, string category)
        {
            Write(message, category);
        }

        public override void WriteLine(object o, string category)
        {
            Write(o.ToString(), category);
        }
    }
}