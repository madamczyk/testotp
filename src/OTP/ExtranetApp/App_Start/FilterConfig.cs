﻿using ExtranetApp.Extensions;
using System.Web;
using System.Web.Mvc;

namespace ExtranetApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CustomHandlerErrorAttribute());
        }
    }
}