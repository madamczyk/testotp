[assembly: WebActivator.PreApplicationStartMethod(typeof(ExtranetApp.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(ExtranetApp.App_Start.NinjectWebCommon), "Stop")]

namespace ExtranetApp.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using DataAccess;
    using BusinessLogic.ServiceContracts;
    using BusinessLogic.Services;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            
            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IAmenityGroupsService>().To<AmenityGroupsService>().InRequestScope();
            kernel.Bind<IAmenityService>().To<AmenityService>().InRequestScope();
            kernel.Bind<IAppliancesService>().To<AppliancesService>().InRequestScope();
            kernel.Bind<IAuthorizationService>().To<AuthorizationService>().InRequestScope();
            kernel.Bind<IBlobService>().To<BlobService>().InRequestScope();
            kernel.Bind<IConfigurationService>().To<ConfigurationService>().InRequestScope();
            kernel.Bind<IDestinationsService>().To<DestinationsService>().InRequestScope();
            kernel.Bind<IDictionaryCountryService>().To<DictionaryCountryService>().InRequestScope();
            kernel.Bind<IEmailService>().To<SendGridEmailService>().InRequestScope();
            kernel.Bind<IEventLogService>().To<EventLogService>().InRequestScope();
            kernel.Bind<IMessageService>().To<MessageService>().InRequestScope();
            kernel.Bind<IPropertiesService>().To<PropertiesServices>().InRequestScope();
            kernel.Bind<IPropertyAddOnsService>().To<PropertyAddOnsService>().InRequestScope();
            kernel.Bind<IPropertyTypesService>().To<PropertyTypeService>().InRequestScope();
            kernel.Bind<IRolesService>().To<RolesService>().InRequestScope();
            kernel.Bind<ISettingsService>().To<SettingsService>().InRequestScope();
            kernel.Bind<ISiteMapService>().To<SiteMapService>().InRequestScope();
            kernel.Bind<IStateService>().To<StateService>().InRequestScope();
            kernel.Bind<IStorageQueueService>().To<StorageQueueService>().InRequestScope();
            kernel.Bind<IStorageTableService>().To<StorageTableService>().InRequestScope();
            kernel.Bind<ITagsService>().To<TagsService>().InRequestScope();
            kernel.Bind<ITagGroupsService>().To<TagGroupsService>().InRequestScope();
            kernel.Bind<ITaxesService>().To<TaxesService>().InRequestScope();
            kernel.Bind<IUnitsService>().To<UnitsService>().InRequestScope();
            kernel.Bind<IUnitTypesService>().To<UnitTypesService>().InRequestScope();
            kernel.Bind<IUserService>().To<UsersService>().InRequestScope();
            kernel.Bind<IScheduledVisitsService>().To<ScheduledVisitService>().InRequestScope();
            kernel.Bind<IContextService>().To<HttpContextService>().InRequestScope();
            kernel.Bind<IBookingService>().To<BookingService>().InRequestScope();
            kernel.Bind<IReservationsService>().To<ReservationsService>().InRequestScope();
            kernel.Bind<IDocumentService>().To<PdfDocumentService>().InRequestScope().Named("PDFDocService");
            kernel.Bind<IPaymentGatewayService>().To<PaymentGatewayService>().InRequestScope();
            kernel.Bind<IDictionaryCultureService>().To<DictionaryCultureService>().InRequestScope();
            kernel.Bind<ISalesPersonsService>().To<SalesPersonsService>().InRequestScope();
            kernel.Bind<IZohoCrmService>().To<ZohoCrmService>().InRequestScope();
            kernel.Bind<ICrmOperationLogsService>().To<CrmOperationLogsService>().InRequestScope();           
        }        
    }
}
