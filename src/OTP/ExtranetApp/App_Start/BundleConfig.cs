﻿using System.Web;
using System.Web.Optimization;

namespace ExtranetApp
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/js/jquery-ui-1.9.0.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryplug").Include(
                        "~/js/jquery.flexslider.js",
                        "~/js/jquery.prettyPhoto.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/jqueryplug2").Include(
                        "~/js/datepicker.js",
                        "~/js/datepicker_navigation.js",
                        "~/js/date.js",
                        "~/js/jquery.jplayer.js",
                        "~/js/jquery.tools.js",
                        "~/js/queryLoader.js",
                        "~/js/jquery.cookie.js",
                        "~/js/jquery.form.js",
                        "~/js/jquery.watermark.js",
                        "~/js/jquery.uniform.js",
                        "~/js/jquery-scroller-v1.js",
                        "~/js/jquery.jrumble.js",
                        "~/js/globalize.js",
                        "~/js/globalize.cultures.js",
                        "~/js/twitter.js",
                        "~/js/Views/commonScripts.js",
                        "~/js/jquery.preventDoubleSubmit.js",
                        "~/js/Views/otp.common.js"));

            bundles.Add(new ScriptBundle("~/bundles/mapView").Include(
                                    "~/js/googleMapsInfoBox.js",
                                    "~/js/Views/home.mapView.js"
                                    ));

            bundles.Add(new ScriptBundle("~/bundles/propertyDetails").Include(
                "~/js/jquery.cycle.all.js",
                "~/js/slimScroll.js",
                "~/js/Views/home.propertyDetails.js",
                "~/js/Views/home.propertyDetails.floorPlan.js",
                "~/js/Views/datepicker_availability.js",
                "~/js/jquery.scrollto.js"
                ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Content/otpstyles")
                .Include("~/css/uniform.default.css",
                "~/css/flexslider.css",
                "~/css/grid.css",
                "~/css/layout-center.css",
                "~/css/main.css",
                "~/css/prettyPhoto.css",
                "~/css/queryLoader.css",
                "~/css/datepicker.css",
                "~/css/otp.css",
                "~/css/otp_navBarSort.css")
                );
        }
    }
}