﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.ServiceRuntime;
using IntranetProcessorRole.TaskScheduler;
using BusinessLogic.Enums;
using System.Net;
using DataAccess.Enums;
using System.Threading;

namespace IntranetProcessorRole
{
    public class WorkerRole : RoleEntryPoint
    {
        #region Private Properties

        private ITaskScheduler _taskScheduler;

        #endregion

        #region RoleEntryPoint methods

        public override void Run()
        {
            _taskScheduler.Run();
        }

        public override bool OnStart()
        {
            try
            {
                ServicePointManager.DefaultConnectionLimit = 48;
                ServicePointManager.UseNagleAlgorithm = false;
                ServicePointManager.Expect100Continue = false;

                var schedulerWorkerCount = Convert.ToInt32(RoleEnvironment.GetConfigurationSettingValue(CloudConfigurationKeys.SchedulerWorkerCount.ToString()));

                //creates workers for task scheduler
                var workers = new List<ITaskSchedulerWorker>();
                for (int i = 0; i < schedulerWorkerCount; i++)
                    workers.Add(CreateTaskSchedulerWorker());

                _taskScheduler = new TaskScheduler.TaskScheduler(workers);
                return _taskScheduler.InitializeWorkers();
            }
            catch (Exception e)
            {
                var services = new BusinessLogic.Services.Services(null);
                services.EventLogService.LogError(e.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.IntranetProcessor);
                throw;
            }
        }

        public override void OnStop()
        {
            _taskScheduler.Stop();
        }

        #endregion

        #region Private Helpers

        /// <summary>
        /// Creates single task scheduler worker
        /// </summary>
        /// <returns></returns>
        private TaskSchedulerWorker CreateTaskSchedulerWorker()
        {
            var services = new BusinessLogic.Services.Services(null);
            var taskFactory = new TaskFactory(services.StorageQueueService, services.TaskSchedulerEventLogService, 
                                              services.MessageService, services.EmailService, services.SettingsService,
                                              services.PaymentGatewayService, services.UsersService, services.ReservationsService, 
                                              services.UnitsService, services.BookingService, services.EventLogService, services.ConfigurationService, services.PropertiesService, services.BlobService);

            return new TaskSchedulerWorker(services.ConfigurationService,
                                           services.EventLogService,
                                           services.ScheduledTasksService,
                                           services.StorageQueueService,
                                           services.TaskSchedulerEventLogService,
                                           taskFactory);
        }

        #endregion
    }
}
