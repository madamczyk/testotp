﻿using BusinessLogic.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace IntranetProcessorRole.WorkerServices
{
    public class ExceptionMessenger
    {
        #region Fields

        private Timer _timer;
        //private int _jobInterval = 86400000;
        private int _jobInterval = 864;
        
        #endregion
    
        public ExceptionMessenger()
        {
            Initialize();
        }

        private void Initialize()
        {
            _timer = new Timer();
            _timer.Interval = _jobInterval;
            _timer.Elapsed += _timer_Elapsed;
        }

        void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //RequestManager.Services.EventLogService.GetEventLogs().Take(50);
        }

        public void Start()
        {
            if (_timer == null)
            {
                this.Initialize();
            }
            _timer.Start();
        }

        public void Stop()
        {
            if (_timer != null)
            {
                _timer.Stop();
            }
        }


        
        
        
        
        
        
    }
}
