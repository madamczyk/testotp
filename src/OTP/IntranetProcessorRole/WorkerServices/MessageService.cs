﻿using DataAccess;
using DataAccess.Enums;
using IntranetProcessorRole.CustomObjects;
using Microsoft.WindowsAzure.StorageClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Transactions;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using BusinessLogic.ServiceContracts;
using System.Net.Mime;

namespace IntranetProcessorRole.WorkerServices
{
    public class MessageServerService
    {
        #region Fields

        private readonly IStorageQueueService _storageQueueService;
        private readonly IMessageService _messageService;

        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(5);
        private readonly TimeSpan _disableAccountTimeWhenQuotaExceeded = TimeSpan.FromMinutes(30);
        private readonly TimeSpan _disableServiceTimeWhenConnectionLost = TimeSpan.FromMinutes(5);
        private readonly List<EmailAccount> _emailAccounts;

        private EmailAccount _currentAccount;
        private bool _isListenerStarted;
        private List<LinkedResource> _resource = new List<LinkedResource>();

        #endregion

        #region Ctor

        public MessageServerService(IStorageQueueService storageQueueService, IMessageService emailDescriptorDao, IEnumerable<string> accountConnectionStrings)
        {
            this._storageQueueService = storageQueueService;
            this._messageService = emailDescriptorDao;
            this._emailAccounts = new List<EmailAccount>();

            var connectionBuilder = new DbConnectionStringBuilder();
            foreach (var accountConnectionString in accountConnectionStrings)
            {
                connectionBuilder.ConnectionString = accountConnectionString;
                _emailAccounts.Add(new EmailAccount
                {
                    DisplayFrom = (string)connectionBuilder["DisplayFrom"],
                    UserName = (string)connectionBuilder["UserName"],
                    Password = (string)connectionBuilder["Password"],
                    Port = Convert.ToInt32(connectionBuilder["Port"]),
                    SSL = Convert.ToBoolean(connectionBuilder["SSL"]),
                    Server = (string)connectionBuilder["Server"],
                    NotAvailableUntil = DateTime.Now
                });
            }


        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets settings for outgoing emails
        /// </summary>
        /// <returns></returns>
        protected EmailAccount GetAvailableAccount()
        {
            return (from account in _emailAccounts where account.NotAvailableUntil <= DateTime.Now select account).FirstOrDefault();
        }

        /// <summary>
        /// Starts the email listener.
        /// </summary>
        /// <param name="queueCheckInterval">The queue check interval.</param>
        public void StartListener(TimeSpan queueCheckInterval)
        {
            _isListenerStarted = true;
            while (_isListenerStarted)
            {
                try
                {
                    bool messageWasPresent = ProcessMessage();
                    if (messageWasPresent == false)
                    {
                        Thread.Sleep(queueCheckInterval);
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("[EmailServerService] Error during execution of method ProcessMessage().  Exception:", e);
                }
            }
        }

        /// <summary>
        /// Stops the email listener
        /// </summary>
        public void StopListener()
        {
            _isListenerStarted = false;
        }

        /// <summary>
        /// Processes the message.
        /// </summary>
        /// <returns>true, if message has been processed</returns>
        public bool ProcessMessage()
        {
            _currentAccount = GetAvailableAccount();
            if (_currentAccount == null)
            {
                Thread.Sleep(_disableAccountTimeWhenQuotaExceeded);
            }
            CloudQueueMessage cloudQueueMessage = _storageQueueService.GetMessage(_maxProcessingTime);
            if (cloudQueueMessage == null)
            {
                return false;
            }
            Message mailMessage = _messageService.GetById(Convert.ToInt32(cloudQueueMessage.AsString));
            if (mailMessage == null)
            {
                _storageQueueService.DeleteMessage(cloudQueueMessage);
                return true;
            }

            if (mailMessage.Status == (int)MessageStatus.Rejected || mailMessage.Status == (int)MessageStatus.Completed)
            {
                _storageQueueService.DeleteMessage(cloudQueueMessage);
                return true;
            }

            using (var transaction = new TransactionScope(TransactionScopeOption.Required, _maxProcessingTime))
            {
                MessageStatus newStatus = MessageStatus.Pending;

                while (newStatus == MessageStatus.Pending)
                {
                    try
                    {
                        _storageQueueService.UpdateVisibility(cloudQueueMessage, _maxProcessingTime);

                        SendEmail(mailMessage);
                        newStatus = MessageStatus.Completed;
                        mailMessage.Status = (int)newStatus;
                        mailMessage.SendAttempts = 1;
                        mailMessage.CompleteDate = DateTime.Now;
                        _messageService.Update(mailMessage);
                        transaction.Complete();
                        _storageQueueService.DeleteMessage(cloudQueueMessage);
                    }
                    catch (Exception e)
                    {
                        if (Regex.IsMatch(e.Message, "\\bquota\\b"))
                        {
                            _currentAccount.NotAvailableUntil = DateTime.Now + _disableAccountTimeWhenQuotaExceeded;
                            _currentAccount = GetAvailableAccount();
                            if (_currentAccount == null)
                            {
                                newStatus = MessageStatus.Error;
                                mailMessage.Status = (int)newStatus;
                                _messageService.Update(mailMessage);
                                transaction.Complete();
                                return true;
                            }
                        }
                        else if (e.InnerException is System.Net.WebException)
                        {
                            newStatus = MessageStatus.Error;
                            mailMessage.Status = (int)newStatus;
                            _messageService.Update(mailMessage);
                            transaction.Complete();
                            Thread.Sleep(_disableServiceTimeWhenConnectionLost);
                        }
                        else
                        {
                            mailMessage.SendAttempts += 1;
                            newStatus = mailMessage.SendAttempts >= 3 ? MessageStatus.Rejected : MessageStatus.Error;
                            mailMessage.Status = (int)newStatus;
                            if (newStatus == MessageStatus.Rejected)
                            {
                                _storageQueueService.DeleteMessage(cloudQueueMessage);
                            }
                            _messageService.Update(mailMessage);
                            transaction.Complete();
                            return true;
                        }
                    }
                }
            }
            return true;
        }


        /// <summary>
        /// Sends the email using Smtp Client
        /// </summary>
        /// <param name="emailDescriptor">The email descriptor.</param>
        protected void SendEmail(Message mailMessage)
        {
            MailMessage message = ConvertToMailMessage(mailMessage);

            SmtpClient mailClient = new SmtpClient
            {
                Host = _currentAccount.Server,
                Port = _currentAccount.Port,
                EnableSsl = _currentAccount.SSL,
                
                Credentials = new NetworkCredential
                {
                    UserName = _currentAccount.UserName,
                    Password = _currentAccount.Password
                }
            };
            mailClient.Send(message);
        }

        /// <summary>
        /// Converts mail message in DB format to MailMessage object used by SmtpClient
        /// </summary>
        /// <param name="mailMessage">Mail from DB</param>
        /// <returns></returns>
        protected MailMessage ConvertToMailMessage(Message mailMessage)
        {
            var eMailMessage = new MailMessage
            {
                IsBodyHtml = true,
                From = new MailAddress(_currentAccount.UserName, _currentAccount.DisplayFrom),
                SubjectEncoding = Encoding.UTF8,
                BodyEncoding = Encoding.UTF8,
                Subject = mailMessage.Subject,
                Body = mailMessage.Body
            };

            List<string> recipients = mailMessage.Recipients.Split(',').ToList();
            foreach (var recipient in recipients)
            {
                eMailMessage.To.Add(recipient);
            }

            List<string> replyToList = mailMessage.ReplyTo.Split(',').ToList();
            foreach (var replyTo in replyToList)
            {
                if (String.IsNullOrWhiteSpace(replyTo) == false)
                {
                    eMailMessage.ReplyToList.Add(new MailAddress(replyTo));
                }
            }
            if (mailMessage.CC != null)
            {
                List<string> CCList = mailMessage.CC.Split(',').ToList();
                foreach (var cc in CCList)
                {
                    if (String.IsNullOrWhiteSpace(cc) == false)
                    {
                        eMailMessage.CC.Add(new MailAddress(cc));
                    }
                }
            }
            if (mailMessage.BCC != null)
            {
                List<string> BCCList = mailMessage.BCC.Split(',').ToList();
                foreach (var bcc in BCCList)
                {
                    if (String.IsNullOrWhiteSpace(bcc) == false)
                    {
                        eMailMessage.CC.Add(new MailAddress(bcc));
                    }
                }
            }

            //attachements
            foreach (DataAccess.Attachment att in mailMessage.Attachments)
            {
                eMailMessage.Attachments.Add(AttachmentFactory.CreateMailAttachment(att));
            }

            //embed images
            mailMessage.Body = ConvertImagesTags(mailMessage.Body);
            eMailMessage.Body = mailMessage.Body;
            var alternateView = AlternateView.CreateAlternateViewFromString(mailMessage.Body, Encoding.UTF8, "text/html");
            AddContentImages(alternateView, mailMessage.Body);            
            eMailMessage.AlternateViews.Add(alternateView);

            return eMailMessage;
        }

        private string ConvertImagesTags(string body)
        {
            //images from external source (Internet)
            string fileUrl;
            string imgPattern = "src=\"ecid:[^\"]*\"";
            MatchCollection matches = Regex.Matches(body, imgPattern);

            foreach (Match m in matches)
            {
                Image image;
                fileUrl = m.Value.Replace("src=\"ecid:", "").Replace("\"", "");
                {
                    string newFileId = "file_" + Guid.NewGuid().ToString();
                    image = Image.FromStream(GetWebResponse(fileUrl));
                    MemoryStream stream1 = new System.IO.MemoryStream();
                    image.Save(stream1, ImageFormat.Jpeg);                    
                    stream1.Position = 0;
                    LinkedResource lr1 = new LinkedResource(stream1, "image/jpg");
                    lr1.ContentId = newFileId;
                    lr1.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                    _resource.Add(lr1);
                    body = body.Replace("ecid:" + fileUrl, "cid:" + newFileId);
                }
            }
            return body;
        }

        private string AddContentImages(AlternateView altView, string body)
        {
            string fileId = string.Empty;
            Image img = null;
            MemoryStream stream;

            string imgPattern = "src=\"cid:[^\"]*\"";
            MatchCollection matches = Regex.Matches(body, imgPattern);

            foreach (Match m in matches)
            {
                fileId = m.Value.Replace("src=\"cid:", "").Replace("\"", "");
                img = (Image)Common.Resources.Logos.ResourceManager.GetObject(fileId);
                if (img != null)
                {
                    stream = new System.IO.MemoryStream();
                    img.Save(stream, ImageFormat.Png);
                    stream.Position = 0;
                    LinkedResource lr = new LinkedResource(stream);
                    lr.ContentId = fileId;
                    lr.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                    _resource.Add(lr);
                }
            }

            foreach (LinkedResource lr in _resource)
            {
                altView.LinkedResources.Add(lr);
            }
            return body;
        }

        private Stream GetWebResponse(string imgUrl)
        {
            // Create a 'WebRequest' object with the specified url. 
            WebRequest webRequest = WebRequest.Create(imgUrl);

            // Send the 'WebRequest' and wait for response.
            WebResponse webResponse = webRequest.GetResponse();

            // Obtain a 'Stream' object associated with the response object.
            Stream receiveStream = webResponse.GetResponseStream();

            return receiveStream;
        }

        #endregion
    }
}
