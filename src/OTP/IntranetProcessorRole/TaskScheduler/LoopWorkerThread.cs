﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace IntranetProcessorRole.TaskScheduler
{
    /// <summary>
    /// Basic thread worker in which logic is executed in the endless loop.
    /// </summary>
    public class LoopWorkerThread : BaseThreadWorker
    {
        #region Ctor

        public LoopWorkerThread(Action executionMethod)
            : base(executionMethod)
        {

        }

        #endregion

        #region ICustomThreadWorker members

        public override void DoWork()
        {
            //throw exception if method is in proccess of disposing or it is aleardy disposed
            ThrowIfDisposedOrDisposing();

            //execution logic
            while (!_stopping.WaitOne(0))
            {
                _executionMethod();
            }

            //when object is stopped, we are signaling it
            _stopped.Set();
        }

        #endregion
    }
}
