﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Threading;

namespace IntranetProcessorRole.TaskScheduler
{
    public class TaskScheduler : ITaskScheduler
    {

        #region Private fields

        private IList<ICustomThreadWorker> _threads = new List<ICustomThreadWorker>();
        private IList<ITaskSchedulerWorker> _workers;
        protected EventWaitHandle EventWaitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);

        #endregion

        #region Ctor

        public TaskScheduler(IList<ITaskSchedulerWorker> workers)
        {
            this._workers = workers;
        }

        #endregion

        #region ITaskScheduler members

        /// <summary>
        /// Runs all the scheduler workers. Makes sure that every worker is alive. 
        /// If someone stopped working, it will try to restart it
        /// </summary>
        public void Run()
        {
            foreach (ITaskSchedulerWorker worker in _workers)
                _threads.Add(new LoopWorkerThread(worker.Run));

            foreach (ICustomThreadWorker thread in _threads)
                thread.Start();

            while (!EventWaitHandle.WaitOne(0))
            {
                for (Int32 i = 0; i < _threads.Count; i++)
                {
                    if (!_threads[i].IsAlive() && !_threads[i].IsDisposed())
                        _threads[i].Start();
                }

                EventWaitHandle.WaitOne(1000);
            }

        }

        /// <summary>
        /// Initializes workers by calling OnStart method on each of them
        /// </summary>
        /// <returns></returns>
        public bool InitializeWorkers()
        {
            foreach (ITaskSchedulerWorker worker in _workers)
                worker.OnStart();

            return true;
        }

        /// <summary>
        /// Method resposible for stopping task scheduler
        /// Abort all threads where workers are running and executes OnStop method for each.
        /// </summary>
        public void Stop()
        {
            EventWaitHandle.Set();

            foreach (ICustomThreadWorker thread in _threads)
                    thread.Stop();

            foreach (ITaskSchedulerWorker worker in _workers)
                worker.OnStop();
        }

        #endregion
    }
}
