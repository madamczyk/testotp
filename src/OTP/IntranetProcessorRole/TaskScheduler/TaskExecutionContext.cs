﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntranetProcessorRole.TaskScheduler
{
    /// <summary>
    /// Object which contains data about current execution of the task.
    /// </summary>
    public class TaskExecutionContext
    {
        #region Ctor

        public TaskExecutionContext(int taskInstanceId, string objectId)
        {
            TaskInstanceId = taskInstanceId;
            ObjectId = objectId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Id of the task instance object in database. Needed for contextual info/error logging.
        /// </summary>
        public int TaskInstanceId 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Object id which has been on queue message.
        /// </summary>
        public string ObjectId
        {
            get;
            private set;
        }

        #endregion
    }
}
