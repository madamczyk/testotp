﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntranetProcessorRole.TaskScheduler
{
    public interface ITaskScheduler
    {
        void Run();
        bool InitializeWorkers();
        void Stop();
    }
}
