﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using Common.Serialization;

namespace IntranetProcessorRole.TaskScheduler
{
    /// <summary>
    /// Abstract Class which implements base logic for task running under task scheduler.
    /// All other tasks should derive from that class
    /// </summary>
    public abstract class BaseTask : ITask
    {

        #region Private properties

        protected IStorageQueueService _storageQueueService;
        protected ITaskSchedulerEventLogService _taskSchedulerEventLogService;

        #endregion

        #region Ctor

        public BaseTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService)
        {
            _storageQueueService = storageQueueService;
            _taskSchedulerEventLogService = taskSchedulerEventLogService;
        }

        #endregion

        #region ITask members

        public abstract ScheduledTaskType TaskType
        {
            get;
        }

        /// <summary>
        /// Adds messages to queue. By default single message is inserted into queue.
        /// Allows to customize how task handle their work(e.g u can split work into smaller task)
        /// </summary>
        /// <param name="scheduledTaskId">The id of task which needs to be executed</param>
        public virtual void AddToQueue(int scheduledTaskId)
        {
            AddMessageToQueue(scheduledTaskId);
        }

        /// <summary>
        /// Method contains logic which needs to be executed
        /// </summary>
        /// <param name="taskExecutionContext">Object which contains information about current execution like task instance id and object id from queue message</param>
        /// <returns>Returns result which determines whether task executed successfully</returns>
        public abstract TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext);

        #endregion

        #region Protected helpers

        /// <summary>
        /// Helper for adding message to queue which wrapping basic logic
        /// </summary>
        /// <param name="scheduledTaskId">The scheduled task id</param>
        /// <param name="objectId">(Optional)Id of the object which can be used to divide task into smaller ones</param>
        protected void AddMessageToQueue(int scheduledTaskId, string objectId = null)
        {
            _storageQueueService.AddMessage(SerializationHelper.SerializeObject(new ScheduledTaskMessage(scheduledTaskId, TaskType, objectId)));
        }

        #endregion
    }
}
