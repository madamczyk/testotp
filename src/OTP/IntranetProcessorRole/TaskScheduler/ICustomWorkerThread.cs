﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntranetProcessorRole.TaskScheduler
{
    /// <summary>
    /// Common interface for thread wrappers
    /// </summary>
    public interface ICustomThreadWorker
    {
        /// <summary>
        /// Starting custom thread worker
        /// </summary>
        void Start();
        /// <summary>
        /// Stopping custom thread worker
        /// </summary>
        void Stop();
        /// <summary>
        /// Executes custom thread worker logic
        /// </summary>
        void DoWork();
        /// <summary>
        /// Determines wether custom thread is still working
        /// </summary>
        /// <returns></returns>
        bool IsAlive();
        /// <summary>
        /// Determines wether custom thread worker is disposed or in process of disposing
        /// </summary>
        /// <returns></returns>
        bool IsDisposed();
    }
}
