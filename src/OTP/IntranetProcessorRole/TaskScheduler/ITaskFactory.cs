﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Enums;

namespace IntranetProcessorRole.TaskScheduler
{
    public interface ITaskFactory
    {
        ITask Create(ScheduledTaskType type);
    }
}
