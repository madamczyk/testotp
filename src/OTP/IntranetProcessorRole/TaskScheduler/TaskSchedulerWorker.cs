﻿using BusinessLogic.Enums;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using DataAccess;
using DataAccess.Enums;
using System.Transactions;
using Common.Serialization;
using Common.Exceptions;

namespace IntranetProcessorRole.TaskScheduler
{
    /// <summary>
    /// Main task scheduler worker which is responsible for scheduling and executing tasks
    /// </summary>
    public class TaskSchedulerWorker : ITaskSchedulerWorker, IDisposable
    {
        #region Consts

        #if(!DEBUG)

        public const string BlobLeaseContainerName = "taskSchedulerLock";

        #else

        public const string BlobLeaseContainerName = "taskSchedulerLockDebug";

        #endif

        #endregion

        #region Private Members

        private IConfigurationService _configurationService;
        private IEventLogService _eventLogService;
        private ITaskSchedulerEventLogService _taskSchedulerEventLogService;
        private IScheduledTasksService _scheduledTasksService;
        private IStorageQueueService _storageQueueService;
        private ITaskFactory _taskFactory;

        //private Thread _renewalThread;
        private ICustomThreadWorker _renevalWorkerThread;
        private bool _disposed = false;

        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);

        #endregion

        #region Ctor

        public TaskSchedulerWorker(IConfigurationService configurationService, 
                                   IEventLogService eventLogService,
                                   IScheduledTasksService scheduledTasksService,
                                   IStorageQueueService storageQueueService,
                                   ITaskSchedulerEventLogService taskSchedulerEventLogService,
                                   ITaskFactory taskFactory)
        {
            _configurationService = configurationService;
            _scheduledTasksService = scheduledTasksService;
            _storageQueueService = storageQueueService;
            _eventLogService = eventLogService;
            _taskSchedulerEventLogService = taskSchedulerEventLogService;
            _taskFactory = taskFactory;
        }

        #endregion

        #region IDisposable members

        ~TaskSchedulerWorker()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_renevalWorkerThread != null)
                    {
                        _renevalWorkerThread.Stop();
                        _renevalWorkerThread = null;
                    }
                }
                _disposed = true;
            }
        }

        #endregion

        #region ITaskSchedulerWorker members

        public bool OnStart()
        {
            return true;
        }

        public void Run()
        {
            try
            {
                var didWork = false;
                didWork = didWork || CheckForAndEnqueueNewTasks();
                didWork = didWork || CheckForAndProcessQueueMessages();

                //if worker has nothing to do, putting him to sleep for 1 second to avoid tight loop.
                if (!didWork)
                    Thread.Sleep(TimeSpan.FromSeconds(1));
            }
            catch (Exception exception)
            {
                //logging unexpected error
                _eventLogService.LogError(ExceptionHelper.FormatErrorMessage(exception),
                                            DateTime.Now,
                                            EventCategory.Error.ToString(),
                                            EventLogSource.IntranetProcessor);

                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
        }

        public void OnStop()
        {
            //if renewal thread still running, stopping it
            if (_renevalWorkerThread != null)
            {
                _renevalWorkerThread.Stop();
                _renevalWorkerThread = null;
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Checks for tasks to execute and enqueue them to storage queue
        /// </summary>
        /// <returns>Return flag which determines wether method processed any task</returns>
        private bool CheckForAndEnqueueNewTasks()
        {
            var storageConnectionString = _configurationService.GetKeyValue(CloudConfigurationKeys.StorageConnectionString);
            var blobClient = CloudStorageAccount.Parse(storageConnectionString).CreateCloudBlobClient();
            blobClient.RetryPolicy = RetryPolicies.Retry(3, TimeSpan.FromMilliseconds(5000));
            var blob = blobClient.GetBlobReference(BlobLeaseContainerName);

            using (var autoRenewLease = new AutoRenewLease(blob))
            {
                if (autoRenewLease.HasLease)
                {
                    var tasksToExecute = _scheduledTasksService.GetScheduledTasksToExecute();
                    if (tasksToExecute != null && tasksToExecute.Count() > 0)
                    {
                        foreach (var task in tasksToExecute)
                            try
                            {
                                EnqueueTask(task);
                            }
                            catch (Exception)
                            {
                                ResetEFContext();
                                throw;
                            }

                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Enqueue scheduled task for immediate execution on azure storage queue
        /// </summary>
        /// <param name="scheduledTask">The sceduled task</param>
        private void EnqueueTask(ScheduledTask scheduledTask) 
        {
            //calculates next planned execution time
            scheduledTask.LastExecutionTime = DateTime.Now;
            switch ((ScheduleType)scheduledTask.ScheduleType)
            {
                case ScheduleType.Every:
                    if (!scheduledTask.ScheduleIntervalType.HasValue || !scheduledTask.TimeInterval.HasValue)
                        throw new InvalidOperationException("Calculating time for ScheduleType.Every. ScheduleIntervalType or TimeInverval is undefined");

                    scheduledTask.NextPlannedExecutionTime =
                        DateTimeHelper.CalculateEveryExecutionTime(scheduledTask.LastExecutionTime ?? DateTime.Now,
                                                                   (ScheduleIntervalType)scheduledTask.ScheduleIntervalType.Value,
                                                                   scheduledTask.TimeInterval.Value);
                    break;
                case ScheduleType.EverydayAt:
                    if (!scheduledTask.ScheduleTimestamp.HasValue)
                        throw new InvalidOperationException("Calculating time for ScheduleType.EverydayAt. ScheduleTimestamp is undefined");

                    scheduledTask.NextPlannedExecutionTime =
                        DateTimeHelper.CalculateEverydayAtExecutionTime(scheduledTask.LastExecutionTime ?? DateTime.Now,
                                                                        new TimeSpan(scheduledTask.ScheduleTimestamp.Value.Hour,
                                                                                     scheduledTask.ScheduleTimestamp.Value.Minute,
                                                                                     scheduledTask.ScheduleTimestamp.Value.Second));
                    break;
                case ScheduleType.Once:
                    scheduledTask.TaskCompleted = true;
                    scheduledTask.NextPlannedExecutionTime = null;
                    break;
            }

            //execute logic which adds messages to queue
            var scheduledTaskProcessor = _taskFactory.Create((ScheduledTaskType)scheduledTask.ScheduledTaskType);
            scheduledTaskProcessor.AddToQueue(scheduledTask.ScheduledTaskId);

            //updates scheduled task
            _scheduledTasksService.UpdateScheduledTask(scheduledTask);
        }

        /// <summary>
        /// Method responsible for task execution
        /// </summary>
        /// <returns>Return flag which determines wether method executed any task</returns>
        private bool CheckForAndProcessQueueMessages()
        {
            var cloudQueueMessage = _storageQueueService.GetMessage(_maxProcessingTime);
            if (cloudQueueMessage == null)
                return false;

            //renew message invisibility in case of long processing
            StartRenevalMessageVisibilityThread(cloudQueueMessage);

            var scheduledTaskMessage = SerializationHelper.DeserializeObject<ScheduledTaskMessage>(cloudQueueMessage.AsString);
            var taskInstanceId = _taskSchedulerEventLogService.CreateTaskInstance(scheduledTaskMessage.TaskId);

            try
            {
                var scheduledTaskProcessor = _taskFactory.Create((ScheduledTaskType)scheduledTaskMessage.TaskType);
                var result = scheduledTaskProcessor.Execute(new TaskExecutionContext(taskInstanceId, scheduledTaskMessage.ObjectId));

                _taskSchedulerEventLogService.UpdateTaskInstanceStatus(taskInstanceId, result);
            }
            catch (Exception exception)
            {
                //Rollback db transaction
                ResetEFContext();

                //handling all messages that may be thrown inside specific task. Logging them into database
                _taskSchedulerEventLogService.LogTaskInstanceError(taskInstanceId, exception);                
                //marks scheduled task execution as failed
                _taskSchedulerEventLogService.UpdateTaskInstanceStatus(taskInstanceId, TaskExecutionResult.Failure);
            }
            finally
            {
                //stopping renewal message visibility thread
                if (_renevalWorkerThread != null)
                    _renevalWorkerThread.Stop();

                //remove task from storage queue
                _storageQueueService.DeleteMessage(cloudQueueMessage);
            }

            return true;
        }

        /// <summary>
        /// This is necessary for recreating context to avoid transaction commiting on exception (SaveChanges is transaction commit).
        /// Rollback in EF application in performed by context disposing and recreating it for logging purposes.
        /// </summary>
        private void ResetEFContext()
        {
            var services = new Services(null);

            var taskFactory = new TaskFactory(services.StorageQueueService, services.TaskSchedulerEventLogService, services.MessageService, services.EmailService,
                services.SettingsService, services.PaymentGatewayService, services.UsersService, services.ReservationsService, services.UnitsService,
                services.BookingService, services.EventLogService, services.ConfigurationService, services.PropertiesService, services.BlobService);

            _configurationService = services.ConfigurationService;
            _scheduledTasksService = services.ScheduledTasksService;
            _storageQueueService = services.StorageQueueService;
            _eventLogService = services.EventLogService;
            _taskSchedulerEventLogService = services.TaskSchedulerEventLogService;
            _taskFactory = taskFactory;
        }

        private void StartRenevalMessageVisibilityThread(CloudQueueMessage cloudQueueMessage)
        {
            _renevalWorkerThread = new RenevalWorkerThread(_maxProcessingTime.Subtract(new TimeSpan(0, 0, 20)), 
                                                           () => _storageQueueService.UpdateVisibility(cloudQueueMessage, _maxProcessingTime));

            _renevalWorkerThread.Start();
        }

        #endregion
    }
}
