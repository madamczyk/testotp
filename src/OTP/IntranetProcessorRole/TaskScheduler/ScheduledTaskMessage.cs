﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Enums;
using Microsoft.WindowsAzure.StorageClient;
using Common.Serialization;

namespace IntranetProcessorRole.TaskScheduler
{
    /// <summary>
    /// Message that is serialized and stored on queue storage
    /// </summary>
    [Serializable]
    public class ScheduledTaskMessage
    {
        #region Ctor

        public ScheduledTaskMessage()
        {

        }

        public ScheduledTaskMessage(int scheduledTaskId, ScheduledTaskType taskType, string objectId)
        {
            TaskId = scheduledTaskId;
            TaskType = taskType;
            ObjectId = objectId;
        }

        #endregion

        #region Properties

        public int TaskId
        {
            get;
            set;
        }

        public ScheduledTaskType TaskType
        {
            get;
            set;
        }

        public String ObjectId
        {
            get;
            set;
        }

        #endregion
    }
}
