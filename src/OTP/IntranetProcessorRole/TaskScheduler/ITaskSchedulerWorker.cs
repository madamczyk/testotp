﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntranetProcessorRole.TaskScheduler
{
    /// <summary>
    /// Common interface for task scheduler workers
    /// </summary>
    public interface ITaskSchedulerWorker
    {
        bool OnStart();
        void Run();
        void OnStop();
    }
}
