﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace IntranetProcessorRole.TaskScheduler
{
    public abstract class BaseThreadWorker : ICustomThreadWorker, IDisposable
    {
        #region Protected memebers

        protected Thread _workerThread;
        protected Action _executionMethod;

        protected ManualResetEvent _stopping;
        protected ManualResetEvent _stopped;
        protected bool _disposed;
        protected bool _disposing;

        #endregion

        #region Ctor

        public BaseThreadWorker(Action executionMethod)
        {
            _executionMethod = executionMethod;

            _disposing = false;
            _disposed = false;

            _stopped = new ManualResetEvent(false);
            _stopping = new ManualResetEvent(false);
        }

        #endregion

        #region IWorkerThread interface implementation

        public abstract void DoWork();

        public void Start()
        {
            //throw exception if method is in proccess of disposing or it is aleardy disposed
            ThrowIfDisposedOrDisposing();

            _workerThread = new Thread(DoWork);
            _workerThread.Start();
        }

        public void Stop()
        {
            //throw exception if method is in proccess of disposing or it is aleardy disposed
            ThrowIfDisposedOrDisposing();

            _stopping.Set();
            _stopped.WaitOne();
        }

        public bool IsAlive()
        {
            return _workerThread.IsAlive;
        }

        public bool IsDisposed()
        {
            return _disposing || _disposed;
        }

        #endregion

        #region IDisposable members

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

        #region Private members

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _disposing = true;

                //execute stop method
                Stop();

                _workerThread.Join(1000);

                DisposeWaitHandle(_stopping);
                DisposeWaitHandle(_stopped);

                //mark object as disposed
                _disposing = true;
                _disposed = true;
            }
        }

        protected void ThrowIfDisposedOrDisposing()
        {
            if (_disposing)
            {
                throw new InvalidOperationException("Cannot execute action if worker is disposing");
            }

            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name, "Cannot execute action if worker is disposed");
            }
        }

        private void DisposeWaitHandle(WaitHandle waitHandle)
        {
            if (waitHandle != null)
            {
                waitHandle.Close();
                waitHandle = null;
            }
        }

        #endregion
    }
}
