﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Enums;

namespace IntranetProcessorRole.TaskScheduler
{
    public static class DateTimeHelper
    {
        public static DateTime CalculateEveryExecutionTime(DateTime lastExecutionTime,
                                                           ScheduleIntervalType scheduleIntervalType,
                                                           int timeInvertal)
        {
            var nextPlannedExecutionTime = lastExecutionTime;
            switch (scheduleIntervalType)
            {
                case ScheduleIntervalType.Seconds:
                    nextPlannedExecutionTime = lastExecutionTime.AddSeconds(timeInvertal);
                    break;
                case ScheduleIntervalType.Minutes:
                    nextPlannedExecutionTime = lastExecutionTime.AddMinutes(timeInvertal);
                    break;
                case ScheduleIntervalType.Hours:
                    nextPlannedExecutionTime = lastExecutionTime.AddHours(timeInvertal);
                    break;
                case ScheduleIntervalType.Days:
                    nextPlannedExecutionTime = lastExecutionTime.AddDays(timeInvertal);
                    break;
            }

            return nextPlannedExecutionTime;
        }

        public static DateTime CalculateEverydayAtExecutionTime(DateTime lastExecutionTime, TimeSpan timestamp)
        {
            var nextDay = lastExecutionTime.AddDays(1);
            return new DateTime(nextDay.Year, nextDay.Month, nextDay.Day, timestamp.Hours, timestamp.Minutes, timestamp.Seconds);
        }
    }
}
