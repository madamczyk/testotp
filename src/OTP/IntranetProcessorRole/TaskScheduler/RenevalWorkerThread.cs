﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace IntranetProcessorRole.TaskScheduler
{
    /// <summary>
    /// Custom thread wrapper for basic resource reneval(e.g blob lease, cloud queue message visibility)
    /// </summary>
    public class RenevalWorkerThread : BaseThreadWorker
    {
        #region Private memebers

        private TimeSpan _renevalTime;

        #endregion

        #region Ctor

        public RenevalWorkerThread(TimeSpan renevalTime, Action executionMethod) 
            : base(executionMethod)
        {
            _renevalTime = renevalTime;
        }

        #endregion

        #region IWorkerThread interface implementation

        public override void DoWork()
        {
            //throw exception if method is in proccess of disposing or it is aleardy disposed
            ThrowIfDisposedOrDisposing();

            //execution logic
            while (!_stopping.WaitOne(0))
            {
                bool isStopping = _stopping.WaitOne(_renevalTime);
                if (!isStopping)
                    _executionMethod();
            }

            //when object is stopped, we are signaling it
            _stopped.Set();
        }

        #endregion
    }
}
