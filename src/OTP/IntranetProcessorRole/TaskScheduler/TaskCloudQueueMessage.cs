﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Enums;
using Microsoft.WindowsAzure.StorageClient;
using Common.Serialization;

namespace IntranetProcessorRole.TaskScheduler
{
    public class TaskCloudQueueMessage
    {
        public TaskCloudQueueMessage(ScheduledTaskType taskType, string objectId)
        {
            TaskType = taskType;
            ObjectId = objectId;
        }

        #region Properties

        public ScheduledTaskType TaskType
        {
            get;
            private set;
        }

        public String ObjectId
        {
            get;
            private set;
        }

        #endregion
    }
}
