﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Enums;
using BusinessLogic.ServiceContracts;
using IntranetProcessorRole.Tasks;

namespace IntranetProcessorRole.TaskScheduler
{
    public class TaskFactory : ITaskFactory
    {

        #region Private properties

        private IStorageQueueService _storageQueueService;
        private IMessageService _messageService;
        private IEmailService _emailService;
        private ISettingsService _settingsService;
        private ITaskSchedulerEventLogService _taskSchedulerEventLogService;
        private IPaymentGatewayService _paymentGatewayService;
        private IUserService _userService;
        private IReservationsService _reservationService;
        private IUnitsService _unitsService;
        private IBookingService _bookingService;
        private IEventLogService _eventLogService;
        private IConfigurationService _configurationService;
        private IPropertiesService _propertiesService;
        private IBlobService _blobService;

        #endregion

        #region Ctor

        public TaskFactory(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService,
            IMessageService messageService, IEmailService emailService, ISettingsService settingsService, IPaymentGatewayService paymentGatewayService,
            IUserService userService, IReservationsService reservationService, IUnitsService unitsService, IBookingService bookingService,
            IEventLogService eventLogService, IConfigurationService configurationService, IPropertiesService propertiesService, IBlobService blobService)
        {
            _storageQueueService = storageQueueService;
            _messageService = messageService;
            _emailService = emailService;
            _taskSchedulerEventLogService = taskSchedulerEventLogService;
            _settingsService = settingsService;
            _paymentGatewayService = paymentGatewayService;
            _userService = userService;
            _reservationService = reservationService;
            _unitsService = unitsService;
            _bookingService = bookingService;
            _eventLogService = eventLogService;
            _configurationService = configurationService;
            _propertiesService = propertiesService;
            _blobService = blobService;
        }

        #endregion

        #region ITaskFactory members

        /// <summary>
        /// Creates instances of task processors.
        /// </summary>
        /// <param name="type">Type of the scheduled task</param>
        /// <returns></returns>
        public ITask Create(ScheduledTaskType type)
        {
            switch (type)
            {
                case ScheduledTaskType.Email:
                    return new EmailTask(_storageQueueService, _taskSchedulerEventLogService, _settingsService, _messageService, _emailService);
                case ScheduledTaskType.CreditCardRedact:
                    return new CreditCardRedactTask(_storageQueueService, _taskSchedulerEventLogService,
                                                    _paymentGatewayService, _userService);
                case ScheduledTaskType.Reservation:
                    return new ReservationTask(_storageQueueService, _taskSchedulerEventLogService,
                                               _paymentGatewayService, _reservationService, _unitsService, _bookingService, _propertiesService, _settingsService);
                case ScheduledTaskType.ReservationPayment:
                    return new ReservationPaymentTask(_storageQueueService, _taskSchedulerEventLogService,
                                                      _paymentGatewayService, _reservationService, _bookingService);
                case ScheduledTaskType.EventLogSummary:
                    return new EventLogSummaryTask(_storageQueueService, _taskSchedulerEventLogService,
                                                   _eventLogService, _messageService, _settingsService);
                case ScheduledTaskType.SessionCleaner:
                    return new SessionCleanerTask(_storageQueueService, _taskSchedulerEventLogService, _configurationService);
                case ScheduledTaskType.CheckInEmail:
                    return new CheckInEmailTask(_storageQueueService, _taskSchedulerEventLogService, _reservationService, _propertiesService, _messageService, _settingsService, _unitsService);
                case ScheduledTaskType.CheckOutEmail:
                    return new CheckOutEmailTask(_storageQueueService, _taskSchedulerEventLogService, _reservationService, _propertiesService, _messageService, _settingsService, _unitsService);
                case ScheduledTaskType.SetOfArrivalsForKeyManager:
                    return new SetOfArrivalsEmailTask(_storageQueueService, _taskSchedulerEventLogService, _reservationService, _propertiesService, _messageService, _settingsService, _unitsService);
                case ScheduledTaskType.KeyDropOffConfirmEmail:
                    return new KeyDropOffConfirmEmailTask(_storageQueueService, _taskSchedulerEventLogService, _reservationService, _propertiesService, _messageService, _settingsService, _unitsService);
                case ScheduledTaskType.KeyDropOffAlertEmail:
                    return new KeyDropOffAlertEmailTask(_storageQueueService, _taskSchedulerEventLogService, _reservationService, _propertiesService, _messageService, _settingsService, _unitsService);
                case ScheduledTaskType.KeyDropOffSecondAlertEmail:
                    return new KeyDropOffSecondAlertEmailTask(_storageQueueService, _taskSchedulerEventLogService, _reservationService, _propertiesService, _messageService, _settingsService, _unitsService);
                case ScheduledTaskType.StorageCleaner:
                    return new StorageCleanerTask(_storageQueueService, _taskSchedulerEventLogService, _configurationService, _blobService, _settingsService, _propertiesService);
                case ScheduledTaskType.SecurityDepositAquireRetry:
                    return new SecurityDepositAquireRetryTask(_storageQueueService, _taskSchedulerEventLogService, _bookingService, _reservationService);
                default:
                    throw new InvalidOperationException("Invalid schedule task type");
            }
        }

        #endregion
    }
}