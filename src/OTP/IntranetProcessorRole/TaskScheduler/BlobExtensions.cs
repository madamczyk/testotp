﻿using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure.StorageClient.Protocol;
using System;
using System.IO;
using System.Threading;
using System.Net;
using System.Transactions;
using System.Linq;
using System.Collections.Generic;

namespace IntranetProcessorRole.TaskScheduler
{
    public static class BlobExtensions
    {
        public static string TryAcquireLease(this CloudBlob blob)
        {
            try 
            { 
                return blob.AcquireLease(); 
            }
            catch (WebException e)
            {
                if ((e.Response == null) || ((HttpWebResponse)e.Response).StatusCode != HttpStatusCode.Conflict)
                {
                    throw;
                }
                e.Response.Close();
                return null;
            }
        }

        public static string AcquireLease(this CloudBlob blob)
        {
            var creds = blob.ServiceClient.Credentials;
            var transformedUri = new Uri(creds.TransformUri(blob.Uri.AbsoluteUri));
            var req = BlobRequest.Lease(transformedUri,
                90,
                LeaseAction.Acquire,
                null);
            blob.ServiceClient.Credentials.SignRequest(req);
            using (var response = req.GetResponse())
            {
                return response.Headers["x-ms-lease-id"];
            }
        }

        private static void DoLeaseOperation(CloudBlob blob, string leaseId, LeaseAction action)
        {
            var creds = blob.ServiceClient.Credentials;
            var transformedUri = new Uri(creds.TransformUri(blob.Uri.AbsoluteUri));
            var req = BlobRequest.Lease(transformedUri, 90, action, leaseId);
            creds.SignRequest(req);
            req.GetResponse().Close();
        }

        public static void ReleaseLease(this CloudBlob blob, string leaseId)
        {
            DoLeaseOperation(blob, leaseId, LeaseAction.Release);
        }

        public static bool TryRenewLease(this CloudBlob blob, string leaseId)
        {
            try { blob.RenewLease(leaseId); return true; }
            catch { return false; }
        }

        public static void RenewLease(this CloudBlob blob, string leaseId)
        {
            DoLeaseOperation(blob, leaseId, LeaseAction.Renew);
        }

        public static void BreakLease(this CloudBlob blob)
        {
            DoLeaseOperation(blob, null, LeaseAction.Break);
        }
    }
}