﻿using Microsoft.WindowsAzure.StorageClient;
using System;
using System.Threading;
using System.Net;
using System.Globalization;

namespace IntranetProcessorRole.TaskScheduler
{
    /// <summary>
    /// Class which is used as global lock for task scheduler.
    /// Uses mechanizm of blob leases to perform lock.
    /// </summary>
    public class AutoRenewLease : IDisposable
    {
        #region Private members

        private CloudBlob _blob;
        private string _leaseId;
        private ICustomThreadWorker _renevalWorkerThread;
        private bool _disposed = false;

        #endregion

        #region Ctor

        public AutoRenewLease(CloudBlob blob)
        {
            _blob = blob;
            _blob.Container.CreateIfNotExist();

            try
            {
                _blob.UploadByteArray(new byte[0], new BlobRequestOptions { AccessCondition = AccessCondition.IfNoneMatch("*") });
            }
            catch (StorageClientException e)
            {
                if (e.ErrorCode != StorageErrorCode.BlobAlreadyExists
                    && e.StatusCode != HttpStatusCode.PreconditionFailed)
                {
                    throw;
                }
            }

            //trying to acquire lease on blob(read/write lock)
            _leaseId = _blob.TryAcquireLease();

            //if lease has been acquired, starting reneval thread.
            //Thread will be responsible for keeping lock if execution time will be longer than 60 seconds(standart blob lease time)
            if (HasLease)
            {
                _renevalWorkerThread = new RenevalWorkerThread(TimeSpan.FromSeconds(40),
                                                               () => 
                                                                   {
                                                                       _blob.RenewLease(_leaseId);
                                                                   });

                _renevalWorkerThread.Start();
            }
        }

        #endregion

        #region IDisposable members

        ~AutoRenewLease()
        {
            Dispose(true);
        }

        public bool HasLease 
        { 
            get 
            { 
                return _leaseId != null; 
            } 
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_renevalWorkerThread != null)
                    {
                        _renevalWorkerThread.Stop();
                        _blob.ReleaseLease(_leaseId);
                        _renevalWorkerThread = null;
                    }
                }
                _disposed = true;
            }
        }

        #endregion
    }
}