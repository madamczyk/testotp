﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Enums;

namespace IntranetProcessorRole.TaskScheduler
{
    public interface ITask
    {
        ScheduledTaskType TaskType { get; }

        void AddToQueue(int scheduledTaskId);
        TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext);
    }
}
