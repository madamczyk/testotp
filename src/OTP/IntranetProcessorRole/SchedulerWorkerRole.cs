﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Microsoft.WindowsAzure.ServiceRuntime;
//using IntranetProcessorRole.TaskScheduler;

/* ******************************************************
 * 
 * DO NOT UNCOMMENT - TASK SCHEDULER IN DEVELOPMENT
 * 
 ****************************************************** */

//namespace IntranetProcessorRole
//{
//    public class SchedulerWorkerRole : RoleEntryPoint
//    {
//        private static int SchedulerWorkerCount = 1;
//        private ITaskScheduler _taskScheduler;

//        public override void Run()
//        {
//            _taskScheduler.Run();
//        }

//        public override bool OnStart()
//        {
//            var services = new BusinessLogic.Services.Services(null);

//            var workers = new List<ITaskSchedulerWorker>();
//            for (int i = 0; i < SchedulerWorkerCount; i++)
//                workers.Add(new TaskSchedulerWorker(services.ConfigurationService));

//            _taskScheduler = new TaskScheduler.TaskScheduler(workers);

//            return _taskScheduler.InitializeWorkers();
//        }

//        public override void OnStop()
//        {
//            _taskScheduler.Stop();
//        }
//    }
//}
