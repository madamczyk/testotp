﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IntranetProcessorRole.TaskScheduler;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using System.Transactions;
using DataAccess;
using BusinessLogic.Objects.Templates.Email;
using Common.Extensions;
using BusinessLogic.Enums;

namespace IntranetProcessorRole.Tasks
{
    public class SetOfArrivalsEmailTask : BaseTask
    {
        #region Private properties

        IReservationsService _reservationService;
        IPropertiesService _propertyService;
        IMessageService _messageService;
        ISettingsService _settingsService;
        IUnitsService _unitService;
        
        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);

        #endregion

        #region Ctor

        public SetOfArrivalsEmailTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService, 
            IReservationsService reservationService, IPropertiesService propertyService, IMessageService messageService, ISettingsService settingsService, IUnitsService unitService)
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _reservationService = reservationService;
            _propertyService = propertyService;
            _messageService = messageService;
            _settingsService = settingsService;
            _unitService = unitService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.SetOfArrivalsForKeyManager;
            }
        }

        /// <summary>
        /// Send Check In email 2 days before arrival date
        /// </summary>
        /// <param name="taskExecutionContext"></param>
        /// <returns></returns>
        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            var reservations = _reservationService.GetUpcomingXDaysUpfrontReservations(2);
            foreach (var reservation in reservations)
            {
                //property local date ime (Now)
                DateTime propertyDateTimeNow = _propertyService.GetPropertyLocalTime(DateTime.UtcNow, reservation.Property.PropertyID);
                propertyDateTimeNow = propertyDateTimeNow.CleanMinAndSec();

                if (reservation.DateArrival.Date.AddDays(-1) == propertyDateTimeNow.Date)
                {
                    //One day before arrival date at 4 pm
                    DateTime arrivalDate = reservation.DateArrival.Date.AddDays(-1).AddHours(16);
                    if (propertyDateTimeNow == arrivalDate)
                    {
                        if (reservation.Property.KeyProcessType == (int)KeyProcessType.KeyHandler)
                        {
                            SendSetOfArrivalsEmail(reservation);
                        }
                    }
                }
            }            
            return TaskExecutionResult.Success;
        }

        #endregion

        #region Private helpers

        private void SendSetOfArrivalsEmail(Reservation reservation)
        {
            var keyManager = _propertyService.GetPropertyManagerByType(reservation.Property.PropertyID, RoleLevel.KeyManager);
            if(keyManager == null || keyManager.User == null || string.IsNullOrWhiteSpace(keyManager.User.email))
                throw new NullReferenceException(string.Format("Key Manager for Property with id {0} cannot be null", reservation.Property.PropertyID.ToString()));

            SetOfArrivalsForKeyHandlerEmail email = new SetOfArrivalsForKeyHandlerEmail(keyManager.User.email);
            email.CheckIn = new DateTime(reservation.Property.CheckIn.Ticks).ToString("t");
            if (reservation.EstimatedArrivalTime.HasValue)
            {
                email.EstimatedArrivalTime = ArrivalTimeRangeExtensions.GetTextRepresentation((ArrivalTimeRange)reservation.EstimatedArrivalTime.Value);
            }
            else
            {
                email.EstimatedArrivalTime = "Na";
            }
            email.GuestFullName = string.Format("{0} {1}", reservation.User.Firstname, reservation.User.Lastname);
            email.KeyManagerFirstName = keyManager.User.Firstname;
            email.PropertyName = reservation.Property.PropertyNameCurrentLanguage;
            email.TomorrowDate = DateTime.Today.AddDays(1).ToLocalizedDateString();
            email.UnitName = reservation.Unit.UnitTitleCurrentLanguage;
            _messageService.AddEmail(email, keyManager.User.DictionaryCulture.CultureCode);
        }

        
        #endregion
    }
}
