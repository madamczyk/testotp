﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IntranetProcessorRole.TaskScheduler;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using System.Transactions;
using BusinessLogic.PaymentGateway;
using System.Data.SqlClient;

namespace IntranetProcessorRole.Tasks
{
    public class SessionCleanerTask : BaseTask
    {
        #region Private properties

        private IConfigurationService _configurationService;

        //settings
        private const string _spDeleteExpiredSessions = "DeleteExpiredSessions";

        #endregion

        #region Ctor

        public SessionCleanerTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService, IConfigurationService configurationService)
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _configurationService = configurationService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.SessionCleaner;
            }
        }

        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_configurationService.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.SessionDBConnectionString)))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand(_spDeleteExpiredSessions, sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException sqlEx)
                {
                    throw sqlEx;
                }
            }
            return TaskExecutionResult.Success;
        }

        #endregion
    }
}