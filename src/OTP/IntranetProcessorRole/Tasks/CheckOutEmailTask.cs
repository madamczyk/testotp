﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IntranetProcessorRole.TaskScheduler;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using System.Transactions;
using DataAccess;
using BusinessLogic.Objects.Templates.Email;
using Common.Extensions;
using BusinessLogic.Enums;

namespace IntranetProcessorRole.Tasks
{
    public class CheckOutEmailTask : BaseTask
    {
        #region Private properties

        IReservationsService _reservationService;
        IPropertiesService _propertyService;
        IMessageService _messageService;
        ISettingsService _settingsService;
        IUnitsService _unitService;
        
        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);

        #endregion

        #region Ctor

        public CheckOutEmailTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService, 
            IReservationsService reservationService, IPropertiesService propertyService, IMessageService messageService, ISettingsService settingsService, IUnitsService unitService)
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _reservationService = reservationService;
            _propertyService = propertyService;
            _messageService = messageService;
            _settingsService = settingsService;
            _unitService = unitService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.CheckOutEmail;
            }
        }

        /// <summary>
        /// Send Check Out email 1 day before departure date
        /// </summary>
        /// <param name="taskExecutionContext"></param>
        /// <returns></returns>
        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            var reservations = _reservationService.GetCompletedReservationsWithinXDays(2);
            foreach (var reservation in reservations)
            {
                //property local date time (Now)
                DateTime propertyDateTimeNow = _propertyService.GetPropertyLocalTime(DateTime.UtcNow, reservation.Property.PropertyID);
                propertyDateTimeNow = propertyDateTimeNow.CleanMinAndSec();

                if (reservation.DateDeparture.Date.AddDays(-1) == propertyDateTimeNow.Date)
                {
                    //Day before departure date at 5 AM of local property time
                    DateTime departureDate = reservation.DateDeparture.Date.AddDays(-1).AddHours(5);
                    if (propertyDateTimeNow == departureDate)
                    {
                        if(reservation.Property.KeyProcessType == (int)KeyProcessType.Lockbox)
                        {
                            SendCheckOutLockboxEmail(reservation);
                        }
                        else if (reservation.Property.KeyProcessType == (int)KeyProcessType.KeyHandler)
                        {
                            SendCheckOutPickUpKeyHandlerEmail(reservation);
                        }
                    }
                }
            }
            
            return TaskExecutionResult.Success;
        }

        #endregion

        #region Private helpers

        private void SendCheckOutLockboxEmail(Reservation reservation)
        {
            string extranetUrl = _settingsService.GetSettingValue(SettingKeyName.ExtranetURL);
            CheckOutLockboxEmail email = new CheckOutLockboxEmail(reservation.User.email);
            email.DepartureDate = reservation.DateDeparture.ToLocalizedDateString();
            email.GuestFirstName = reservation.User.Firstname;
            email.LockboxCode = reservation.Unit.KeysLockboxCode;
            email.LockboxLocation = reservation.Unit.KeysLockboxLocation;
            email.CheckOutLinkUrl = string.Format("http://{0}/Guest/ConfirmCheckOut?t={1}&rId={2}", extranetUrl, reservation.LinkAuthorizationToken, reservation.ReservationID.ToString());
            _messageService.AddEmail(email, reservation.User.DictionaryCulture.CultureInfo.Name);
        }

        private void SendCheckOutPickUpKeyHandlerEmail(Reservation reservation)
        {
            string extranetUrl = _settingsService.GetSettingValue(SettingKeyName.ExtranetURL);
            CheckOutPickupKeyHandlerEmail email = new CheckOutPickupKeyHandlerEmail(reservation.User.email);
            email.DepartureDate = reservation.DateDeparture.ToLocalizedDateString();
            email.GuestFirstName = reservation.User.Firstname;
            var keyManager = _propertyService.GetPropertyManagerByType(reservation.Property.PropertyID, RoleLevel.KeyManager);
            if (keyManager != null && keyManager.User != null)
            {
                email.KeyManagerFullName = string.Format("{0} {1}", keyManager.User.Firstname, keyManager.User.Lastname);
                email.KeyManagerAddress = string.Format("{0}, {1}, {2} {3}", keyManager.User.Address1, keyManager.User.Address2, keyManager.User.City, keyManager.User.State);
            }
            email.CheckOutLinkUrl = string.Format("http://{0}/Guest/ConfirmCheckOut?t={1}&rId={2}", extranetUrl, reservation.LinkAuthorizationToken, reservation.ReservationID.ToString());
            _messageService.AddEmail(email, reservation.User.DictionaryCulture.CultureInfo.Name);
        }
        
        #endregion
    }
}
