﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IntranetProcessorRole.TaskScheduler;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using System.Transactions;
using DataAccess;
using BusinessLogic.Objects.Templates.Email;
using Common.Extensions;
using BusinessLogic.Enums;

namespace IntranetProcessorRole.Tasks
{
    public class KeyDropOffConfirmEmailTask : BaseTask
    {
        #region Private properties

        IReservationsService _reservationService;
        IPropertiesService _propertyService;
        IMessageService _messageService;
        ISettingsService _settingsService;
        IUnitsService _unitService;
        
        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);

        #endregion

        #region Ctor

        public KeyDropOffConfirmEmailTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService, 
            IReservationsService reservationService, IPropertiesService propertyService, IMessageService messageService, ISettingsService settingsService, IUnitsService unitService)
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _reservationService = reservationService;
            _propertyService = propertyService;
            _messageService = messageService;
            _settingsService = settingsService;
            _unitService = unitService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.KeyDropOffConfirmEmail;
            }
        }

        /// <summary>
        /// Send email with link to key drop-off confirmation at the departure morning - at 5 AM of property local time
        /// </summary>
        /// <param name="taskExecutionContext"></param>
        /// <returns></returns>
        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            var reservations = _reservationService.GetCompletedReservationsWithinXDays(1);
            foreach (var reservation in reservations)
            {
                //property local date ime (Now)
                DateTime propertyDateTimeNow = _propertyService.GetPropertyLocalTime(DateTime.UtcNow, reservation.Property.PropertyID);
                propertyDateTimeNow = propertyDateTimeNow.CleanMinAndSec();

                if (reservation.DateDeparture.Date == propertyDateTimeNow.Date)
                {
                    //Day before departure date at 5 AM of local property time
                    DateTime departureDate = reservation.DateDeparture.Date.AddHours(5);
                    if (propertyDateTimeNow == departureDate)
                    {
                        if(reservation.Property.KeyProcessType == (int)KeyProcessType.Lockbox)
                        {
                            SendKeyDropOffLockboxConfirmEmail(reservation);
                        }
                        else if (reservation.Property.KeyProcessType == (int)KeyProcessType.KeyHandler)
                        {
                            SendKeyDropOffPickupConfirmEmail(reservation);
                        }
                    }
                }
            }
            
            return TaskExecutionResult.Success;
        }

        #endregion

        #region Private helpers        

        private void SendKeyDropOffLockboxConfirmEmail(Reservation reservation)
        {
            string extranetUrl = _settingsService.GetSettingValue(SettingKeyName.ExtranetURL);
            KeyDropOffLockboxEmail email = new KeyDropOffLockboxEmail(reservation.User.email);
            email.ActivationLinkUrl = string.Format("http://{0}/Guest/KeyReturnConfirm?t={1}&rId={2}", extranetUrl, reservation.LinkAuthorizationToken, reservation.ReservationID.ToString());
            email.GuestFirstName = reservation.User.Firstname;
            _messageService.AddEmail(email, reservation.User.DictionaryCulture.CultureCode);
        }

        private void SendKeyDropOffPickupConfirmEmail(Reservation reservation)
        {
            string extranetUrl = _settingsService.GetSettingValue(SettingKeyName.ExtranetURL);
            KeyDropOffPickupHandlerEmail email = new KeyDropOffPickupHandlerEmail(reservation.User.email);
            email.ActivationLinkUrl = string.Format("http://{0}/Guest/KeyReturnConfirm?t={1}&rId={2}", extranetUrl, reservation.LinkAuthorizationToken, reservation.ReservationID.ToString());
            email.GuestFirstName = reservation.User.Firstname;
            _messageService.AddEmail(email, reservation.User.DictionaryCulture.CultureCode);
        }
        
        #endregion
    }
}
