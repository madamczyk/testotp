﻿using BusinessLogic.PaymentGateway;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using IntranetProcessorRole.TaskScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace IntranetProcessorRole.Tasks
{
    public class SecurityDepositAquireRetryTask : BaseTask
    {
        #region Private properties

        private IBookingService _bookingService;
        private IReservationsService _reservationService;

        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);
        private readonly int _inactiveDaysPeriod = 14;

        #endregion

        #region Ctor

        public SecurityDepositAquireRetryTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService,
            IBookingService bookingService, IReservationsService reservationsService)
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _bookingService = bookingService;
            _reservationService = reservationsService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.SecurityDepositAquireRetry;
            }
        }

        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            var list = this._reservationService.GetSecurityDepositRetryReservations();
            foreach (Reservation reservation in list)
            {
                DateTime mostRestrictiveArrivalDate = this._reservationService.FindMostRestrictiveArrivalDateForComparision(reservation, reservation.DateArrival).Date;
                var today = DateTime.Now.Date;
                var bookArrivalDiffDays = mostRestrictiveArrivalDate - reservation.BookingDate.Date;
                var todayArrivalDiffDays = mostRestrictiveArrivalDate - today;

                if (bookArrivalDiffDays.Days < 30)
                {
                    if (bookArrivalDiffDays.Days < 10)
                    {
                        if (bookArrivalDiffDays.Days == 1)
                        {
                            AquireSecurityDepositWithEmail(reservation);
                        }
                        else if (bookArrivalDiffDays.Days > 1)
                        {
                            this._bookingService.AquireSecurityDeposit(reservation);
                        }
                    }
                    else
                    {
                        if (reservation.BookingDate.AddDays(10).Date == today)
                        {
                            AquireSecurityDepositWithEmail(reservation);
                        }
                        else if (reservation.BookingDate.AddDays(10).Date > today)
                        {
                            this._bookingService.AquireSecurityDeposit(reservation);
                        }
                    }
                }
                else if (todayArrivalDiffDays.Days <= 30)
                {
                    if (todayArrivalDiffDays.Days == 20)
                    {
                        AquireSecurityDepositWithEmail(reservation);
                    }
                    else if (todayArrivalDiffDays.Days > 20)
                    {
                        this._bookingService.AquireSecurityDeposit(reservation);
                    }
                }                
            }

            return TaskExecutionResult.Success;

        }
        #endregion

        private void AquireSecurityDepositWithEmail(Reservation reservation)
        {
            TransactionResponse authorizeResult = null;
            try
            {
                authorizeResult = this._bookingService.AquireSecurityDeposit(reservation);
            }
            finally
            {
                if (authorizeResult == null || !authorizeResult.Succeded)
                {
                    this._bookingService.SendSecurityDepositAlertEmail(reservation);
                }
            }
        }

    }
}
