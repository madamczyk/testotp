﻿using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using IntranetProcessorRole.TaskScheduler;
using System;
using System.Linq;
using Common.Extensions;
using System.Collections.Generic;
using DataAccess;
using BusinessLogic.Enums;

namespace IntranetProcessorRole.Tasks
{
    public class EventLogSummaryTask : BaseTask
    {
        #region Private properties

        private IMessageService _messageService;
        private IEventLogService _eventLogService;
        private ISettingsService _settingsService;

        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);

        #endregion

        #region Ctor

        public EventLogSummaryTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService,
            IEventLogService eventLogService, IMessageService messageService, ISettingsService settingsService)
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _eventLogService = eventLogService;
            _messageService = messageService;
            _settingsService = settingsService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.EventLogSummary;
            }
        }

        /// <summary>
        /// Main task method. Sends daily summary emails to recipients provided in the database.
        /// </summary>
        /// <param name="taskExecutionContext">Task execution context</param>
        /// <returns>Failure, if needed setings are empty, otherwise Success</returns>
        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            string recipients = _settingsService.GetSettingValue(SettingKeyName.EventLogSummaryRecipients);
            string deploymentTypeSetting = _settingsService.GetSettingValue(SettingKeyName.EventLogSummaryDeploymentType);
            DateTime summaryDay;

            if (String.IsNullOrWhiteSpace(recipients))
            {
                _taskSchedulerEventLogService.LogTaskInstanceError(taskExecutionContext.TaskInstanceId, new Exception("Setting 'ScheduledTasks.EventLogSummary.Recipients' is null or empty"));

                return TaskExecutionResult.Failure;
            }

            if (String.IsNullOrWhiteSpace(deploymentTypeSetting))
            {
                _taskSchedulerEventLogService.LogTaskInstanceError(taskExecutionContext.TaskInstanceId, new Exception("Setting 'DeploymentType' is null or empty"));

                return TaskExecutionResult.Failure;
            }

            //Step 1: 
            // Get all logged events, which occured after last execution time and filter them for extranet and intranet
            summaryDay = DateTime.Now.AddDays(-1); // task executed today gets logs for yesterday

            var dayLog = _eventLogService.GetEventLogsForDay(summaryDay);     

            var intranetLog = dayLog.Where(el => el.Source == EventLogSource.Intranet.ToString() || el.Source == EventLogSource.IntranetProcessor.ToString());
            var extranetLog = dayLog.Where(el => el.Source == EventLogSource.Extranet.ToString() || el.Source == EventLogSource.EVS.ToString() || el.Source == EventLogSource.PaymentGateway.ToString());

            //Step 2: 
            // Send e-mail notifications to users
            SendEmailWithSummary(recipients, "Intranet", deploymentTypeSetting, summaryDay, intranetLog);
            SendEmailWithSummary(recipients, "Extranet", deploymentTypeSetting, summaryDay, extranetLog);

            return TaskExecutionResult.Success;
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Prepares the template and fills required data. Email is the added to database to be sent. 
        /// </summary>
        /// <param name="recipients">Comma-seperated list of e-mail addresses</param>
        /// <param name="source">Name of the application</param>
        /// <param name="type">Type of the deployment</param>
        /// <param name="summaryDay">Day of the summary</param>
        /// <param name="eventLog">Collection of EventLog records. Categories Error and Warning are counted</param>
        private void SendEmailWithSummary(string recipients, string source, string type, DateTime summaryDay, IEnumerable<EventLog> eventLog)
        {
            EventLogSummaryEmail eventLogSummaryEmail = new EventLogSummaryEmail(recipients)
            {
                Source = source,
                SummaryDay = summaryDay.ToLocalizedDateString(),
                NumberOfErrors = eventLog.Where(el => el.Category == EventCategory.Error.ToString()).Count().ToString(),
                NumberOfWarnings = eventLog.Where(el => el.Category == EventCategory.Warning.ToString()).Count().ToString(),
                DeploymentType = type
            };

            _messageService.AddEmail(eventLogSummaryEmail, CultureCode.en_US);
        }

        #endregion
    }
}
