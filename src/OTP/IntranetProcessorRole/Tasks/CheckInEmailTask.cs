﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IntranetProcessorRole.TaskScheduler;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using System.Transactions;
using DataAccess;
using BusinessLogic.Objects.Templates.Email;
using Common.Extensions;
using BusinessLogic.Enums;

namespace IntranetProcessorRole.Tasks
{
    public class CheckInEmailTask : BaseTask
    {
        #region Private properties

        IReservationsService _reservationService;
        IPropertiesService _propertyService;
        IMessageService _messageService;
        ISettingsService _settingsService;
        IUnitsService _unitService;
        
        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);

        #endregion

        #region Ctor

        public CheckInEmailTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService, 
            IReservationsService reservationService, IPropertiesService propertyService, IMessageService messageService, ISettingsService settingsService, IUnitsService unitService)
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _reservationService = reservationService;
            _propertyService = propertyService;
            _messageService = messageService;
            _settingsService = settingsService;
            _unitService = unitService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.CheckInEmail;
            }
        }

        /// <summary>
        /// Send Check In email 2 days before arrival date
        /// </summary>
        /// <param name="taskExecutionContext"></param>
        /// <returns></returns>
        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {            
            var reservations = _reservationService.GetUpcomingXDaysUpfrontReservations(2);
            foreach (var reservation in reservations)
            {
                //property local date time (Now)
                DateTime propertyDateTimeNow = _propertyService.GetPropertyLocalTime(DateTime.UtcNow, reservation.Property.PropertyID);
                propertyDateTimeNow = propertyDateTimeNow.CleanMinAndSec();

                if (reservation.DateArrival.Date.AddDays(-2) == propertyDateTimeNow.Date)
                {
                    //Two days before arrival date at 2 am
                    DateTime arrivalDate = reservation.DateArrival.Date.AddDays(-2).AddHours(2);
                    if (propertyDateTimeNow == arrivalDate)
                    {
                        if(reservation.Property.KeyProcessType == (int)KeyProcessType.Lockbox)
                        {
                            _reservationService.SendCheckInLockboxEmail(reservation);
                        }
                        else if (reservation.Property.KeyProcessType == (int)KeyProcessType.KeyHandler)
                        {
                            _reservationService.SendCheckInPickUpKeyHandlerEmail(reservation);
                        }
                    }
                }
            }
            
            return TaskExecutionResult.Success;
        }

        #endregion

        #region Private helpers
        
        #endregion
    }
}
