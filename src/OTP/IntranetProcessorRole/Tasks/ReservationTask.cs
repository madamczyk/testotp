﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IntranetProcessorRole.TaskScheduler;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using System.Transactions;
using DataAccess;
using BusinessLogic.PaymentGateway;
using Common.Exceptions;
using BusinessLogic.Enums;

namespace IntranetProcessorRole.Tasks
{
    public class ReservationTask : BaseTask
    {
        #region Private properties

        private IPaymentGatewayService _paymentGatewayService;
        private IReservationsService _reservationService;
        private IUnitsService _unitService;
        private IPropertiesService _propertyService;
        private ISettingsService _settingsService;

        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);

        #endregion

        #region Ctor

        public ReservationTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService,
                               IPaymentGatewayService paymentGatewayService, IReservationsService reservationService, 
                               IUnitsService unitsService, IBookingService bookingService, 
                               IPropertiesService propertyService, ISettingsService settingsService)
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _paymentGatewayService = paymentGatewayService;
            _reservationService = reservationService;
            _unitService = unitsService;
            _propertyService = propertyService;
            _settingsService = settingsService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.Reservation;
            }
        }

        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            //Step 1: 
            //For finalized reservations, capture payments if 24 hours passed
            CapturePayments(taskExecutionContext);

            //Step 2: 
            //For tentative reservation, delete reservation with unit blockings
            //For canceled reservation, delete unit blockings
            ClearTentativeOrCanceledBookings(taskExecutionContext);

            //Set reservation booking status as Check In if current reservation status is Finalized
            //and current day (according property time zone) is equal to arrival date 
            UpdateFinalizedReservations();

            //Set reservation booking status as Check Out if current reservation status is Check In
            //and current day (according property time zone) is grater than departure date 
            UpdatedCheckedInReservations();

            //Set reservation booking status as 'Completed' 14 days (configurable) after departure date
            CompleteReservations();

            return TaskExecutionResult.Success;
        }

        #endregion

        #region Private helpers


        //set CheckIn status
        private void UpdateFinalizedReservations()
        {
            var finalizedReservations = _reservationService.GetUpcomingXDaysUpfrontReservations(1);
            foreach (var reservation in finalizedReservations)
            {
                //property local date time (Now)
                DateTime propertyDateTimeNow = _propertyService.GetPropertyLocalTime(DateTime.UtcNow, reservation.Property.PropertyID);
                if (reservation.DateArrival.Date == propertyDateTimeNow.Date)
                {
                    _reservationService.SetReservationStatus(BookingStatus.CheckedIn, reservation.ReservationID);
                }
            }
        }

        //set check out status
        private void UpdatedCheckedInReservations()
        {
            var checkedInReservations = _reservationService.GetCompletedReservationsWithinXDays(0);
            foreach (var reservation in checkedInReservations)
            {
                //property local date time (Now)
                DateTime propertyDateTimeNow = _propertyService.GetPropertyLocalTime(DateTime.UtcNow, reservation.Property.PropertyID);
                if (reservation.DateDeparture.Date.AddDays(1) == propertyDateTimeNow.Date)
                {
                    _reservationService.SetReservationStatus(BookingStatus.CheckedOut, reservation.ReservationID);
                }
            }
        }

        //set complete status
        private void CompleteReservations()
        {
            var checkedOutReservations = _reservationService.GetRervationsByBookingStatus(BookingStatus.CheckedOut);
            int completedAfterDays = int.Parse(_settingsService.GetSettingValue(SettingKeyName.ReservationCompleteAfterDaysOfDeparture));
            foreach (var reservation in checkedOutReservations)
            {
                //property local date time (Now)
                DateTime propertyDateTimeNow = _propertyService.GetPropertyLocalTime(DateTime.UtcNow, reservation.Property.PropertyID);
                if (reservation.DateDeparture.Date.AddDays(completedAfterDays) == propertyDateTimeNow.Date)
                {
                    _reservationService.SetReservationStatus(BookingStatus.Completed, reservation.ReservationID);

                    ReleaseSecurityDeposit(reservation);
                }
            }
        }

        private void CapturePayments(TaskExecutionContext taskExecutionContext)
        {
            var reservations = _reservationService.GetBookedReservationsAfter24Hours();
            if (reservations.Count() == 0)
            {
                return;
            }

            foreach (var reservation in reservations)
            {
                if (String.IsNullOrWhiteSpace(reservation.TransactionToken))
                {
                    _taskSchedulerEventLogService.LogTaskInstanceError(taskExecutionContext.TaskInstanceId,
                        String.Format("Transaction token is null for reservation with an id {0}", reservation.ReservationID));

                    continue;
                }

                var response = _paymentGatewayService.CapturePayment(reservation.TransactionToken, reservation.ReservationID);
                if (response != null && response.Succeded)
                {
                    _reservationService.SetReservationStatus(BookingStatus.FinalizedBooking, reservation.ReservationID);
                }
            }
        }

        private void ClearTentativeOrCanceledBookings(TaskExecutionContext taskExecutionContext)
        {
            var reservations = _reservationService.GetNotFinalizedOrCanceledReservations();
            if (reservations.Count() == 0)
            {
                return;
            }

            foreach (var reservation in reservations)
            {
                //remove unit blockings
                if (reservation.UnitInvBlockings.Count() > 0)
                {
                    _unitService.DeleteUnitBlockageByReservationId(reservation.ReservationID);
                }
                if (reservation.BookingStatus == (int)BookingStatus.TentativeBooking)
                {
                    _reservationService.SetReservationStatus(BookingStatus.Withdrew, reservation.ReservationID);
                }
            }
        }

        /// <summary>
        /// Realeases authorized amount on behalf of the SecurityDeposit and resets SecurityDeposit amount and token columns
        /// </summary>
        /// <param name="reservation">Reservtion to release SecurityDeposit</param>
        private void ReleaseSecurityDeposit(Reservation reservation)
        {
            /// Check if current reservation SecurityDeposit has been Authorized
            if (reservation.SecurityDeposit.HasValue && !string.IsNullOrEmpty(reservation.SecurityDepositToken))
            {
                TransactionResponse response = _paymentGatewayService.CancelPayment(reservation.SecurityDepositToken, reservation.ReservationID);
                if (response == null || response.Succeded.Equals(false))
                {
                    throw new PaymentGatewayException("Could not perform Void on Authorized SecurityDeposit - reservationId: " + reservation.ReservationID, null);
                }
                reservation.SecurityDeposit = null;
                reservation.SecurityDepositToken = string.Empty;
            }
        }

        #endregion
    }
}
