﻿using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using IntranetProcessorRole.TaskScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace IntranetProcessorRole.Tasks
{
    public class CreditCardRedactTask : BaseTask
    {
        #region Private properties

        private IPaymentGatewayService _paymentGatewayService;
        private IUserService _userService;

        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);
        private readonly int _inactiveDaysPeriod = 14;

        #endregion

        #region Ctor

        public CreditCardRedactTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService, 
            IPaymentGatewayService paymentGatewayService, IUserService userService) 
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _paymentGatewayService = paymentGatewayService;
            _userService = userService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get 
            {
                return ScheduledTaskType.CreditCardRedact;
            }
        }

        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            var guestPaymentMethodsToDelete = _userService.GetInactiveGuestPaymentMethods(_inactiveDaysPeriod);
            foreach (var guestPaymentMethod in guestPaymentMethodsToDelete)
            {
                //using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required,
                //    new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted, Timeout = _maxProcessingTime }))
                //{
                    var paymentGatewayResponse = _paymentGatewayService.RedactPaymentMethod(guestPaymentMethod.ReferenceToken);
                    if (paymentGatewayResponse != null && paymentGatewayResponse.Succeded)
                    {
                        _userService.RemoveGuestPaymentMethod(guestPaymentMethod.GuestPaymentMethodID, guestPaymentMethod.User.UserID);
                    }

                    //ts.Complete();
                //}
            }

            return TaskExecutionResult.Success;
        }

        #endregion
    }
}
