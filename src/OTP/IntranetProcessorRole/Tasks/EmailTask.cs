﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Enums;
using IntranetProcessorRole.TaskScheduler;
using BusinessLogic.ServiceContracts;
using System.Transactions;
using System.Data.Common;
using BusinessLogic.Enums;
using BusinessLogic.Objects;
using Common.Extensions;
using Common.Exceptions;
using DataAccess;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using BusinessLogic.Managers;
using Common;

namespace IntranetProcessorRole.Tasks
{
    public class EmailTask : BaseTask
    {
        #region Private properties

        private IMessageService _messageService;
        private IEmailService _emailService;
        private ISettingsService _settingsService;

        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);
        private readonly TimeSpan _disableServiceTimeWhenConnectionLost = TimeSpan.FromMinutes(5);
        private IList<EmailAccount> _emailAccounts;

        #endregion

        #region Ctor

        public EmailTask(IStorageQueueService storageQueueService,ITaskSchedulerEventLogService taskSchedulerEventLogService, ISettingsService settingsService, IMessageService messageService, IEmailService emailService) 
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _messageService = messageService;
            _emailService = emailService;
            _settingsService = settingsService;
            _emailAccounts = new List<EmailAccount>();

            var accountConnectionStrings = settingsService.GetAllSettingsByPrefix(SettingKeyName.EmailServerAccount);
            var connectionBuilder = new DbConnectionStringBuilder();
            foreach (var accountConnectionString in accountConnectionStrings)
            {
                connectionBuilder.ConnectionString = accountConnectionString.SettingValue;
                string senderAccount = accountConnectionString.SettingCode.Replace(SettingKeyName.EmailServerAccount.ToString() + ".", string.Empty);
                    
                _emailAccounts.Add(new EmailAccount
                {
                    UserName = connectionBuilder.ContainsKey("UserName") ? (string)connectionBuilder["UserName"] : string.Empty,
                    Password = connectionBuilder.ContainsKey("Password") ? (string)connectionBuilder["Password"] : string.Empty,
                    Port = connectionBuilder.ContainsKey("Port") ? Convert.ToInt32(connectionBuilder["Port"]) : (int?)null,
                    SSL = connectionBuilder.ContainsKey("SSL") ? Convert.ToBoolean(connectionBuilder["SSL"]) : (bool?)null,
                    Server = connectionBuilder.ContainsKey("Server") ? (string)connectionBuilder["Server"] : string.Empty
                });
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets settings for outgoing emails
        /// </summary>
        /// <returns></returns>
        protected EmailAccount GetAvailableAccount()
        {
            return _emailAccounts.FirstOrDefault();          
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.Email;
            }
        }

        public override void AddToQueue(int scheduledTaskId)
        {
            var messagesToSend = _messageService.GetMessagesToSend();
            foreach (var message in messagesToSend)
            {
                AddMessageToQueue(scheduledTaskId, message.MessageId.ToString());
                message.Status = (int)MessageStatus.Pending;
                _messageService.Update(message);
            }
        }

        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            //using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required,
            //    new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted, Timeout = _maxProcessingTime }))
            //{
                Message mailMessage = _messageService.GetById(Convert.ToInt32(taskExecutionContext.ObjectId));

                if (mailMessage == null)
                {
                    _taskSchedulerEventLogService.LogTaskInstanceInfo(taskExecutionContext.TaskInstanceId,
                        String.Format("Email message with an id: {0} has not been found in the database", taskExecutionContext.ObjectId));

                    //ts.Complete();
                    return TaskExecutionResult.Success;
                }

                if (mailMessage.Status == (int)MessageStatus.Rejected || mailMessage.Status == (int)MessageStatus.Completed)
                {
                    _taskSchedulerEventLogService.LogTaskInstanceInfo(taskExecutionContext.TaskInstanceId,
                        String.Format("Email message with an id: {0} has aleardy been sent/rejected", taskExecutionContext.ObjectId));

                    //ts.Complete();
                    return TaskExecutionResult.Success;
                }

                try
                {
                    var randomEmailAccount = GetAvailableAccount();
                    if (randomEmailAccount == null)
                        throw new ArgumentNullException("No available email accounts");
                    _emailService.SendEmail(mailMessage, randomEmailAccount, _messageService.GetAttachmentsByMessageId(mailMessage.MessageId));

                    mailMessage.Status = (int)MessageStatus.Completed;
                    mailMessage.SendAttempts = 1;
                    mailMessage.CompleteDate = DateTime.Now;
                    _messageService.Update(mailMessage);

                    //ts.Complete();
                }
                catch (EmailCriticalException ece)
                {
                    //logging error
                    _taskSchedulerEventLogService.LogTaskInstanceError(taskExecutionContext.TaskInstanceId, ece);

                    mailMessage.Status = (int)MessageStatus.Error;
                    _messageService.Update(mailMessage);
                    //ts.Complete();

                    return TaskExecutionResult.Failure;
                }
                catch (Exception e)
                {
                    //logging error
                    _taskSchedulerEventLogService.LogTaskInstanceError(taskExecutionContext.TaskInstanceId, e);

                    mailMessage.SendAttempts += 1;
                    mailMessage.Status = (int)(mailMessage.SendAttempts >= 3 ? MessageStatus.Rejected : MessageStatus.New);
                    _messageService.Update(mailMessage);
                    //ts.Complete();

                    return TaskExecutionResult.Failure;
                }
            //}

            return TaskExecutionResult.Success;
        }

        #endregion
    }
}
