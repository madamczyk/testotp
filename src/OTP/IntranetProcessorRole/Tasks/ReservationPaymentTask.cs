﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IntranetProcessorRole.TaskScheduler;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using System.Transactions;
using BusinessLogic.PaymentGateway;

namespace IntranetProcessorRole.Tasks
{
    public class ReservationPaymentTask : BaseTask
    {
        #region Private properties

        private IPaymentGatewayService _paymentGatewayService;
        private IReservationsService _reservationService;
        private IBookingService _bookingService;

        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);

        #endregion

        #region Ctor

        public ReservationPaymentTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService,
            IPaymentGatewayService paymentGatewayService, IReservationsService reservationService, IBookingService bookingService)
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _paymentGatewayService = paymentGatewayService;
            _reservationService = reservationService;
            _bookingService = bookingService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.ReservationPayment;
            }
        }

        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            var reservations = _reservationService.GetUpcomingXDaysUpfrontReservationsForPayment();
            if (reservations.Count() == 0)
                return TaskExecutionResult.Success;

            foreach (var reservation in reservations)
            {
                //using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required,
                //    new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted, Timeout = _maxProcessingTime }))
                //{
                    if (reservation.GuestPaymentMethod == null)
                    {
                        _taskSchedulerEventLogService.LogTaskInstanceError(taskExecutionContext.TaskInstanceId,
                            String.Format("Guest payment method is undefined for reservation with an id {0}", reservation.ReservationID));

                        //ts.Complete();
                        continue;
                    }

                    var amountToCharge = Math.Abs(reservation.TripBalance);

                    var response = _paymentGatewayService.Purchase(
                        new TransactionRequest()
                            {
                                PaymentToken = reservation.GuestPaymentMethod.ReferenceToken,
                                Amount = ( amountToCharge * 100).ToString(), //convert to cents (without decimal places)
                                CurrencyCode = reservation.Property.DictionaryCulture.ISOCurrencySymbol
                            }, reservation.ReservationID);

                    if (response != null && response.Succeded)
                    {
                        _bookingService.AddPaymentTransaction(reservation, amountToCharge);
                        reservation.UpfrontPaymentProcessed = true;
                    }

                //    ts.Complete();
                //}
            }

            return TaskExecutionResult.Success;
        }

        #endregion
    }
}
