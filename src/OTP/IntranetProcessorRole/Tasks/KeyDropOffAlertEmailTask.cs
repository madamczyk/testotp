﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IntranetProcessorRole.TaskScheduler;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using System.Transactions;
using DataAccess;
using BusinessLogic.Objects.Templates.Email;
using Common.Extensions;
using BusinessLogic.Enums;

namespace IntranetProcessorRole.Tasks
{
    public class KeyDropOffAlertEmailTask : BaseTask
    {
        #region Private properties

        IReservationsService _reservationService;
        IPropertiesService _propertyService;
        IMessageService _messageService;
        ISettingsService _settingsService;
        IUnitsService _unitService;
        
        //settings
        private readonly TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(1);

        #endregion

        #region Ctor

        public KeyDropOffAlertEmailTask(IStorageQueueService storageQueueService, ITaskSchedulerEventLogService taskSchedulerEventLogService, 
            IReservationsService reservationService, IPropertiesService propertyService, IMessageService messageService, ISettingsService settingsService, IUnitsService unitService)
            : base(storageQueueService, taskSchedulerEventLogService)
        {
            _reservationService = reservationService;
            _propertyService = propertyService;
            _messageService = messageService;
            _settingsService = settingsService;
            _unitService = unitService;
        }

        #endregion

        #region BaseTask overrides

        public override ScheduledTaskType TaskType
        {
            get
            {
                return ScheduledTaskType.KeyDropOffAlertEmail;
            }
        }

        /// <summary>
        /// Send email with link to key drop-off confirmation at the departure morning - at 5 AM of property local time
        /// </summary>
        /// <param name="taskExecutionContext"></param>
        /// <returns></returns>
        public override TaskExecutionResult Execute(TaskExecutionContext taskExecutionContext)
        {
            var reservations = _reservationService.GetCompletedReservationsWithinXDays(1);
            foreach (var reservation in reservations)
            {
                if (!reservation.KeysReceivedFromGuestDateTime.HasValue)
                    continue;
                //property local date ime (Now)
                DateTime propertyDateTimeNow = _propertyService.GetPropertyLocalTime(DateTime.UtcNow, reservation.Property.PropertyID);
                propertyDateTimeNow = propertyDateTimeNow.CleanMinAndSec();

                if (reservation.DateDeparture.Date == propertyDateTimeNow.Date)
                {
                    //Departure date at 2 PM of local property time
                    DateTime departureDate = reservation.DateDeparture.Date.AddHours(14);
                    if (propertyDateTimeNow == departureDate)
                    {
                        if (reservation.Property.KeyProcessType == (int)KeyProcessType.KeyHandler)
                        {
                            SendKeyDropOffAlertEmail(reservation);
                        }
                    }
                }
            }
            
            return TaskExecutionResult.Success;
        }

        #endregion

        #region Private helpers        

        private void SendKeyDropOffAlertEmail(Reservation reservation)
        {
            var uploadManager = _propertyService.GetPropertyManagerByType(reservation.Property.PropertyID, RoleLevel.Manager);
            if (uploadManager == null || uploadManager.User == null || string.IsNullOrWhiteSpace(uploadManager.User.email))
                throw new NullReferenceException(string.Format("Upload Manager for Property with id {0} cannot be null", reservation.Property.PropertyID.ToString()));

            KeyDropOffAlertEmail email = new KeyDropOffAlertEmail(uploadManager.User.email);
            email.DepartureDate = reservation.DateDeparture.ToLocalizedDateString();
            email.GuestFullName = string.Format("{0} {1}", reservation.User.Firstname, reservation.User.Lastname);
            email.PropertyAddress = string.Format("{0}, {1}, {2} {3}", reservation.Unit.Property.Address1, reservation.Unit.Property.Address2, reservation.Unit.Property.City, reservation.Unit.Property.State);
            email.PropertyName = reservation.Property.PropertyNameCurrentLanguage;
            email.ReservationConfirmationId = reservation.ConfirmationID;
            email.UploadManagerFirstName = uploadManager.User.Firstname;
            _messageService.AddEmail(email, uploadManager.User.DictionaryCulture.CultureCode);
        }

        #endregion
    }
}
