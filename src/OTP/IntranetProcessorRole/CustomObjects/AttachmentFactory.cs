﻿using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace IntranetProcessorRole.CustomObjects
{
    public static class AttachmentFactory
    {
        public static System.Net.Mail.Attachment CreateMailAttachment(DataAccess.Attachment dbAttachment)
        {
            DocumentType docType = (DocumentType)dbAttachment.Type;
            Stream attachmentContent = new MemoryStream();
            System.Net.Mail.Attachment attachment = null;
            if (dbAttachment.Content != null)
            {
                attachmentContent.Write(dbAttachment.Content, 0, dbAttachment.Content.Length);
                attachmentContent.Position = 0;
            }

            switch (docType)
            {
                case DocumentType.PDF:
                    attachment = new System.Net.Mail.Attachment(attachmentContent, dbAttachment.Title);
                    attachment.ContentType = new System.Net.Mime.ContentType("application/pdf");
                    attachment.Name = dbAttachment.Title + ".pdf";
                    break;
            }
            return attachment;
        }
    }
}
