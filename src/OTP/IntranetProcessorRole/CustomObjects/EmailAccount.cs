﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntranetProcessorRole.CustomObjects
{
    /// <summary>
    /// Class containing information about outgoing email account
    /// </summary>
    public class EmailAccount
    {
        public string Server { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool SSL { get; set; }
        public string DisplayFrom { get; set; }
        public DateTime NotAvailableUntil { get; set; }        
    }
}
