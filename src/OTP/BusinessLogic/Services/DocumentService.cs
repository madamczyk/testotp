﻿using BusinessLogic.Objects.Templates;
using BusinessLogic.Objects.Templates.Document;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Common;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Provides basic functionality of documents generation. Used to generate pdf by the PdfDocumentService.
    /// </summary>
    public class DocumentService : BaseTemplateService
    {
        #region Properties

        public DocumentType DocType;



        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTemplateService"/> class.
        /// </summary>
        public DocumentService(IContextService httpContextService)
            : base(httpContextService)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTemplateService"/> class.
        /// </summary>
        public DocumentService(IContextService httpContextService, Dictionary<Type, object> services)
            : base(httpContextService, services)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTemplateService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public DocumentService(Dictionary<Type, object> services, OTPEntities ctx)
            : base(services, ctx)
		{
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTemplateService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public DocumentService(OTPEntities ctx)
            : base(ctx)
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Replace images tags in template based on relative path on the value using absolute path
        /// Pattern replaced: src="ecid:NAME"
        /// </summary>
        /// <param name="templateText">The text of the template</param>
        /// <param name="absoluteUrl">The absoulte URL of the Front End app</param>
        /// <returns></returns>
        public string CompleteImages(string templateText, string absoluteUrl)
        {
            string imgPattern = "src=\"ecid:[^\"]*\"";
            string fileRelPath;
            MatchCollection matches = Regex.Matches(templateText, imgPattern);
            foreach (Match m in matches)
            {
                fileRelPath = m.Value.Replace("src=\"ecid:", "").Replace("\"", "");
                templateText = templateText.Replace(m.Value,
                        string.Format("src='{0}/{1}'",
                        absoluteUrl.TrimEnd('/'),
                        fileRelPath.TrimStart('/')));
            }
            return templateText;
        }

        /// <summary>
        /// Replaces image tags in template based on resource name
        /// Pattern replaced: src="cid:RESOURCE_NAME"
        /// </summary>
        /// <param name="templateText">Text text of the template</param>
        /// <returns></returns>
        public string CompleteImages(string templateText)
        {
            string fileId;
            string imgPattern = "src=\"cid:[^\"]*\"";
            MatchCollection matches = Regex.Matches(templateText, imgPattern);

            foreach (Match m in matches)
            {
                fileId = m.Value.Replace("src=\"cid:", "").Replace("\"", "");
                
                templateText = templateText.Replace(m.Value,
                        string.Format("src=\"data:image/png;base64,{0}\"",
                        ResourceHelper.ToBase64(fileId)));
            }
            return templateText;
        }

        #endregion
    }
}
