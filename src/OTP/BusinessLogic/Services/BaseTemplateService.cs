﻿using BusinessLogic.Objects.Templates;
using BusinessLogic.Objects.Templates.Document;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Contains common opertions for managing pdf and e-mail templates
    /// </summary>
    public abstract class BaseTemplateService : BaseService
    {
        #region Fields

        private const string ParameterTagStart = "{{";
        private const string ParameterTagEnd = "}}";

        /// <summary>
        /// Template mappings: " BaseTemplateClass - TemplateType "
        /// </summary>
        private static Dictionary<Type, TemplateType> templatesMappings = new Dictionary<Type, TemplateType>() 
        { 		
            //notifications
            { typeof(UserActivationEmail), TemplateType.UserSignupConfirmation },
            { typeof(UserActivationEmailWithPassword), TemplateType.UserSignupConfirmationPassword },

            { typeof(OwnerJoiningEmail), TemplateType.OwnerJoinLetter },          
            { typeof(OwnerActivationEmail), TemplateType.OwnerActivationEmail },       
            { typeof(PasswordResetEmail), TemplateType.PasswordResetEmail },
			{ typeof(EVSServiceErrorEmail), TemplateType.EVSServiceError },
            { typeof(EVSVerificationFaildEmail), TemplateType.EVSVerificationFailed },
            { typeof(ReservationConfirmationForGuest), TemplateType.ReservationConfirmationForGuest },
            { typeof(ReservationConfirmationForOwner), TemplateType.ReservationConfirmationForOwner },
            { typeof(PaymentAgeVerificationEmail), TemplateType.AgeVerificationFailed },
            { typeof(PasswordResetBookingEmail), TemplateType.PasswordResetBookingEmail },
            { typeof(ReservationCancelationConfirmationEmail), TemplateType.ReservationCancelationConfirmation },
            { typeof(HomeownerContactEmail), TemplateType.HomeownerContactEmail },
            { typeof(EventLogSummaryEmail), TemplateType.EventLogSummaryEmail },
            { typeof(HomeownerJoiningTicketSystemEmail), TemplateType.TicketSystemJoinEmail},
            { typeof(UserAccountAdded), TemplateType.UserAccountAdded},
            { typeof(CheckInLockboxEmail), TemplateType.CheckInLocboxEmail},
            { typeof(CheckInPickupKeyHandlerEmail), TemplateType.CheckInPickUpFromKeyHandlerEmail},
            { typeof(CheckOutLockboxEmail), TemplateType.CheckOutLockboxEmail},
            { typeof(CheckOutPickupKeyHandlerEmail), TemplateType.CheckOutPickUpFromKeyHandlerEmail},
            { typeof(SetOfArrivalsForKeyHandlerEmail), TemplateType.SetOfArrivalsForKeyManagerEmail},
            { typeof(KeyDropOffLockboxEmail), TemplateType.KeyDropOffLockboxEmail},
            { typeof(KeyDropOffPickupHandlerEmail), TemplateType.KeyDropOffPickUpFromKeyHandlerEmail},
            { typeof(KeyDropOffAlertEmail), TemplateType.KeyDropOffAlertEmail},
            { typeof(KeyDropOffSecondAlertEmail), TemplateType.KeyDropOffSecondAlertEmail},
            { typeof(ReservationNoFundsAlertEmail), TemplateType.ReservationNoFunds},
            { typeof(CrmAddLeadOperationErrorEmail), TemplateType.CrmAddLeadOperationErrorEmail},
            { typeof(CrmConvertLeadOperationErrorEmail), TemplateType.CrmConvertLeadOperationErrorEmail},
            { typeof(PropertySignOffEmail), TemplateType.PropertySignOff},

            //documents
            { typeof(UserAgreement), TemplateType.ReservationAgreement },
            { typeof(TripDetails), TemplateType.TripDetails },
            { typeof(Invoice), TemplateType.Invoice},
            { typeof(UserAgreementDateChange), TemplateType.ReservationAgreementOnDateChange}
        };

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTemplateService"/> class.
        /// </summary>
        public BaseTemplateService(IContextService httpContextService)
            : base(httpContextService)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTemplateService"/> class.
        /// </summary>
        public BaseTemplateService(IContextService httpContextService, Dictionary<Type, object> services)
            : base(httpContextService, services)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTemplateService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public BaseTemplateService(Dictionary<Type, object> services, OTPEntities ctx)
            : base(services, ctx)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTemplateService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public BaseTemplateService(OTPEntities ctx)
            : base(ctx)
        {

        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets template depending on given class
        /// </summary>
        /// <param name="baseTemplate"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public Template GetTemplate(BaseTemplate baseTemplate, string cultureCode)
        {
            //determine template name
            TemplateType templateType = templatesMappings.Single(kvp => kvp.Key.IsInstanceOfType(baseTemplate)).Value;
            Template template = this.GetTemplateByType(templateType, cultureCode);
            return template;
        }

        /// <summary>
        /// Gets notification template by Type
        /// </summary>
        /// <param name="notificationType">Notification Type</param>
        /// <param name="language">Language of NotificationTemplate</param>
        /// <returns>NotificationTemplate</returns>
        public Template GetTemplateByType(TemplateType notificationType, string cultureCode)
        {
            return _Context.Templates.Where(nt => nt.Type == (int)notificationType && nt.DictionaryCulture.CultureCode == cultureCode).SingleOrDefault();
        }

        /// <summary>
        /// Replace values in tags ('{{' as start tag, '}}' as end tag) by concrete values
        /// </summary>
        /// <param name="template">Template content</param>
        /// <param name="values">Dictionary of values</param>
        /// <returns>Completed template content</returns>
        public string CompleteTemplate(string template, IDictionary<string, string> values)
        {
            //replace {{tag}} in template by value
            foreach (KeyValuePair<string, string> kvp in values)
            {
                if (kvp.Value == null)
                    continue;
                template = Regex.Replace(template, ParameterTagStart +
                                        @"\s*" +
                                        kvp.Key +
                                        @"\s*" +
                                        ParameterTagEnd,
                                        kvp.Value);
            }
            return template;
        }

        #endregion
    }
}
