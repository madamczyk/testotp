﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using BusinessLogic.ServiceContracts;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using BusinessLogic.Enums;
using BusinessLogic.Objects.Templates.Document;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool.xml;
using EO.Pdf;

namespace BusinessLogic.Services
{
    public class HeaderF : PdfPageEventHelper
    {        
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            if (document.PageNumber != 1)
            {
                Rectangle pageSize = document.PageSize;
                string date = DateTime.Now.ToString("MMMMM dd, yyyy");
                Paragraph header = new Paragraph(date + " " + document.PageNumber.ToString(), FontFactory.GetFont(FontFactory.TIMES, 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE));
                header.Alignment = Element.ALIGN_LEFT;

                PdfPTable headerTbl = new PdfPTable(1);
                headerTbl.TotalWidth = 600;
                headerTbl.HorizontalAlignment = Element.ALIGN_LEFT;

                StyleSheet styles = new StyleSheet();
                styles.LoadTagStyle("div", "color", "Red");
                List<IElement> elements = HTMLWorker.ParseToList(new StringReader("<div>my<br />text</div>"), styles);

                PdfPCell cell = new PdfPCell();
                //cell.t
                //cell.Border = 0;
                //cell.PaddingLeft = 10;
                //cell.BackgroundColor = BaseColor.GRAY;

                foreach (IElement el in elements)
                {
                    cell.AddElement(el);
                }

                headerTbl.AddCell(cell);
                headerTbl.WriteSelectedRows(0, -1, pageSize.GetLeft(5), pageSize.GetTop(5), writer.DirectContent);
            }
            base.OnEndPage(writer, document);
        }
    } 

    /// <summary>
    /// Provides functionality of pdf documents generation.
    /// </summary>
    public class PdfDocumentService : DocumentService, IDocumentService
    {
        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="PdfDocumentService"/> class.
        /// </summary>
        public PdfDocumentService(IContextService httpContextService, ISettingsService settingsService)
            : base(httpContextService, new Dictionary<Type, object>() {
                { typeof(ISettingsService), settingsService }
            })
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="PdfDocumentService"/> class.
        /// </summary>
        public PdfDocumentService(OTPEntities ctx)
            : base(ctx)
        { }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create new document based on filled by data object
        /// </summary>
        /// <param name="documentTemplate">Document object</param>
        /// <param name="language">Culture of template (en-US)</param>
        /// <returns>Document as stream object; </returns>
        public Stream CreateDocument(BaseDocument documentTemplate, string cultureCode)
        {
            //TODO cultureCode
            cultureCode = CultureCode.en_US;

            try
            {
                string extranetUrl = this._settingsService.GetSettingValue(SettingKeyName.ExtranetURL);
                var template = base.GetTemplate(documentTemplate, cultureCode);
                if (template != null)
                {
                    string htmlCode = template.Content;
                    htmlCode = CompleteTemplate(htmlCode, documentTemplate.Parameters);
                    htmlCode = CompleteImages(htmlCode, extranetUrl);
                    htmlCode = CompleteImages(htmlCode);
                    return ConvertToPDF(htmlCode);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured during creating new pdf document", ex);
            }            
            return null;
        }

        /// <summary>
        /// Create new document based on filled by data object
        /// </summary>
        /// <param name="document">Document</param>
        /// <param name="language">Language</param>
        /// <returns>File as byte array</returns>
        public byte[] CreateDoc(BaseDocument documentTemplate, string cultureCode)
        {
            try
            {
                string extranetUrl = this._settingsService.GetSettingValue(SettingKeyName.ExtranetURL);
                var template = base.GetTemplate(documentTemplate, cultureCode);
                if (template != null)
                {
                    string htmlCode = template.Content;
                    htmlCode = CompleteTemplate(htmlCode, documentTemplate.Parameters);
                    htmlCode = CompleteImages(htmlCode, extranetUrl);
                    htmlCode = CompleteImages(htmlCode);
                    Stream s = ConvertToPDF(htmlCode);
                    MemoryStream ms = new MemoryStream();
                    s.CopyTo(ms);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured during creating new pdf document", ex);
            }
            return null;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates PDF from html code
        /// </summary>
        /// <param name="htmlCode">Template as html code</param>
        /// <returns>Stream with PDF document</returns>
        private Stream ConvertToPDF(string htmlCode)
        {
            //Stream for returning PDF content
            MemoryStream ms = new MemoryStream();
            HtmlToPdf.Options.SaveImageAsJpeg = true;

            var result = HtmlToPdf.ConvertHtml(htmlCode, ms);

            //New PDF document
            //Document doc = new Document(PageSize.A4);

            ////String from html code
            //StringReader sReader = new StringReader(htmlCode);

            ////var htmlparser = new HTMLWorker(doc);

            //PdfWriter writer = PdfWriter.GetInstance(doc, ms);

            //// writer.PageEvent = new HeaderF();

            //doc.Open();

            //XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, sReader);
            
            //doc.Close();

            byte[] file = ms.ToArray();
            MemoryStream pdfFile = new MemoryStream();
            pdfFile.Write(file, 0, file.Length);
            pdfFile.Position = 0;
            return pdfFile;
        }

        #endregion
    }
}
