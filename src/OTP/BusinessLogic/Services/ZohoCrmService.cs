﻿using BusinessLogic.Enums;
using BusinessLogic.Objects.ZohoCrm;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;

namespace BusinessLogic.Services
{
    public class ZohoCrmService : BaseService, IZohoCrmService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ZohoCrmService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public ZohoCrmService(IContextService httpContextService, ISettingsService settingsService, ICrmOperationLogsService crmLogService)
            : base(httpContextService, new Dictionary<Type, object>() {  
                {typeof(ISettingsService), settingsService },
                {typeof(ICrmOperationLogsService), crmLogService}
            })
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZohoCrmService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public ZohoCrmService(OTPEntities context, ISettingsService settingsService, ICrmOperationLogsService crmLogService)
            : base(new Dictionary<Type, object>() {       
                {typeof(ISettingsService), settingsService },
                {typeof(ICrmOperationLogsService), crmLogService}
            }, context)
        { }

        public ZohoCrmAuthentication GetAuthenticationToken(string zohoUsername, string zohoPassword)
        {
            string authenticationURL = @"https://accounts.zoho.com/apiauthtoken/nb/create";
            string postParams = string.Format("SCOPE=ZohoCRM/crmapi&EMAIL_ID={0}&PASSWORD={1}", zohoUsername, zohoPassword);

            var communication = new ZohoCrmCommunication();
            
            string[] responseArray = communication.SendRequest(authenticationURL, postParams).Split(new[] { '\n' });
            ZohoCrmAuthentication authentication = new ZohoCrmAuthentication();

            foreach (var responseString in responseArray)
            {
                if (responseString.Contains("#") && responseString.Length > 1)
                {
                    authentication.TimeCreated = responseString.Substring(1);
                }
                else if(responseString.Contains("AUTHTOKEN"))
                {
                    authentication.Token = responseString.Substring(("AUTHTOKEN=").Length);
                }
                else if(responseString.Contains("RESULT"))
                {
                    if(string.Compare(responseString.Substring(("RESULT=").Length), "TRUE") == 0)
                    {
                        authentication.Result = true;
                    }
                }
            }

            return authentication;
        }

        public ZohoCrmResponse AddZohoCrmLead(User user)
        {
            string authorizationToken = base._settingsService.GetSettingValue(SettingKeyName.ZohoCrmAuthorizationToken);
            string url = string.Format(@"https://crm.zoho.com/crm/private/xml/Leads/insertRecords?newFormat=1&authtoken={0}&scope=crmapi", authorizationToken);

            #region Create Lead XML

            ZohoCrmFieldsGroup row = new ZohoCrmFieldsGroup();
            row.no = "1";
            row.Fields = new List<ZohoCrmFieldLabel>();
            
            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "Email",
                Value = user.email
            });

            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "Company",
                Value = string.Format("{0} {1}", user.Firstname, user.Lastname)
            });

            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "First Name",
                Value = user.Firstname
            });

            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "Last Name",
                Value = user.Lastname
            });

            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "Phone",
                Value = user.CellPhone
            });

            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "Street",
                Value = string.Format("{0}{1}", user.Address1, user.Address2)
            });
            
            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "Zip Code",
                Value = user.ZIPCode
            });
            
            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "City",
                Value = user.City
            });
            
            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "State",
                Value = user.State
            });

            string country = user.DictionaryCountry == null ? string.Empty : user.DictionaryCountry.CountryName.i18nContent
                                                                                        .Where(dc => dc.Code == "en-US")
                                                                                        .Select(dc => dc.Content)
                                                                                        .SingleOrDefault();

            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "Country",
                Value = country
            });

            row.Fields.Add(new ZohoCrmFieldLabel
            {
                ValAttribute = "Homeowner",
                Value = "true"
            });

            ZohoCrmLead lead = new ZohoCrmLead();
            lead.Rows = new List<ZohoCrmFieldsGroup>();

            lead.Rows.Add(row);

            #endregion

            XmlSerializer serializer = new XmlSerializer(typeof(ZohoCrmLead));
            
            string serializedString = string.Empty;
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            using(MemoryStream stream = new MemoryStream())
            {
                serializer.Serialize(stream, lead, ns);
                stream.Position = 0;
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, (int)stream.Length);
                serializedString += UTF8Encoding.UTF8.GetString(buffer);
            }

            var communication = new ZohoCrmCommunication();

            string responseString = communication.SendRequest(url, "xmlData=" + serializedString);

            XmlSerializer xmlResponseSerializer = new XmlSerializer(typeof(ZohoCrmResponse));

            ZohoCrmResponse response = null;
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
            {
                try
                {
                    response = (ZohoCrmResponse)xmlResponseSerializer.Deserialize(stream);
                }
                catch (Exception ex)
                {
                    response = new ZohoCrmResponse { Error = new ZohoCrmResult { Code = "Deserialization Exception", Message = ex.Message } };
                }
            }

            string logMessage = string.Empty;
            if (response.Error != null)
            {
                logMessage = string.Format("Add Lead Operation Failed: {0} - {1}", response.Error.Code, response.Error.Message);
            }

            // Replace encoding for DB
            responseString = System.Text.RegularExpressions.Regex.Replace(responseString, "UTF\\-8", "UTF-16");

            LogCrmOperation(serializedString, responseString, logMessage, CrmOparationLogsType.AddLead, user);

            return response;
        }

        public ZohoCrmConversionResponse ConvertZohoCrmLeadToAccount(string leadId, User user)
        {
            string authorizationToken = base._settingsService.GetSettingValue(SettingKeyName.ZohoCrmAuthorizationToken);
            string url = string.Format(@"https://crm.zoho.com/crm/private/xml/Leads/convertLead?newFormat=1&authtoken={0}&scope=crmapi&leadId={1}", authorizationToken, leadId);

            ZohoCrmPotential potential = new ZohoCrmPotential();
            ZohoCrmPotentialRow row = new ZohoCrmPotentialRow();

            row.no = "1";
            row.Fields = new List<ZohoCrmFieldLabel>();
            row.Fields.Add(new ZohoCrmFieldLabel { ValAttribute = "createPotential", Value = "false" });
            row.Fields.Add(new ZohoCrmFieldLabel { ValAttribute = "notifyLeadOwner", Value = "false" });
            row.Fields.Add(new ZohoCrmFieldLabel { ValAttribute = "notifyNewEntityOwner", Value = "false" });

            potential.Rows = new List<ZohoCrmPotentialRow>();
            potential.Rows.Add(row);

            XmlSerializer serializer = new XmlSerializer(typeof(ZohoCrmPotential));

            string serializedString = string.Empty;
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            using (MemoryStream stream = new MemoryStream())
            {
                serializer.Serialize(stream, potential, ns);
                stream.Position = 0;
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, (int)stream.Length);
                serializedString += UTF8Encoding.UTF8.GetString(buffer);
            }

            var communication = new ZohoCrmCommunication();

            string responseString = communication.SendRequest(url, "xmlData=" + serializedString);

            ZohoCrmConversionResponse response = null;
            ZohoCrmResponse error = null;

            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
            {
                XmlSerializer xmlResponseSerializer = new XmlSerializer(typeof(ZohoCrmConversionResponse));

                try
                {
                    response = (ZohoCrmConversionResponse)xmlResponseSerializer.Deserialize(stream);
                }
                catch(InvalidOperationException) 
                {
                    XmlSerializer xmlErrorResponseSerializer = new XmlSerializer(typeof(ZohoCrmResponse));
                    stream.Position = 0;
                    error = (ZohoCrmResponse)xmlErrorResponseSerializer.Deserialize(stream);
                }
                catch(Exception ex)
                {
                    error = new ZohoCrmResponse { Error = new ZohoCrmResult { Code = "Deserialization Exception", Message = ex.Message } };
                }
            }

            string logMessage = string.Empty;
            if (error != null)
            {
                logMessage = string.Format("Lead Conversion operation failed: {0} - {1}", error.Error.Code, error.Error.Message);
            }

            // Replace encoding for DB 
            responseString = System.Text.RegularExpressions.Regex.Replace(responseString, "UTF\\-8", "UTF-16");

            LogCrmOperation(serializedString, responseString, logMessage, CrmOparationLogsType.ConvertLeadIntoAccount, user);

            return response;
        }

        public ZohoCrmResponse DeleteZohoCrmLeadRecord(string recordId)
        {
            string authorizationToken = base._settingsService.GetSettingValue(SettingKeyName.ZohoCrmAuthorizationToken);
            string url = string.Format(@"https://crm.zoho.com/crm/private/xml/Leads/deleteRecords?authtoken={0}&scope=crmapi&id={1}", authorizationToken, recordId);

            var communication = new ZohoCrmCommunication();

            string responseString = communication.SendRequest(url, string.Empty);

            ZohoCrmResponse response = null;
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
            {
                XmlSerializer xmlResponseSerializer = new XmlSerializer(typeof(ZohoCrmResponse));

                try
                {
                    response = (ZohoCrmResponse)xmlResponseSerializer.Deserialize(stream);
                }
                catch (Exception ex)
                {
                    response = new ZohoCrmResponse { Error = new ZohoCrmResult { Code = string.Empty, Message = ex.Message } };
                }
            }

            // Replace encoding for DB 
            responseString = System.Text.RegularExpressions.Regex.Replace(responseString, "UTF\\-8", "UTF-16");

            LogCrmOperation(string.Empty, responseString, string.Format("Delete Lead Record with Id {0}", recordId), CrmOparationLogsType.DeleteLead, null);

            return response;
        }

        public ZohoCrmResponse DeleteZohoCrmAccountRecord(string recordId)
        {
            string authorizationToken = base._settingsService.GetSettingValue(SettingKeyName.ZohoCrmAuthorizationToken);
            string url = string.Format(@"https://crm.zoho.com/crm/private/xml/Accounts/deleteRecords?authtoken={0}&scope=crmapi&id={1}", authorizationToken, recordId);
            
            var communication = new ZohoCrmCommunication();

            string responseString = communication.SendRequest(url, string.Empty);

            ZohoCrmResponse response = null;
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
            {
                XmlSerializer xmlResponseSerializer = new XmlSerializer(typeof(ZohoCrmResponse));
                try
                {
                    response = (ZohoCrmResponse)xmlResponseSerializer.Deserialize(stream);
                }
                catch (Exception ex)
                {
                    response = new ZohoCrmResponse { Error = new ZohoCrmResult { Code = string.Empty, Message = ex.Message } };
                }
            }

            // Replace encoding for DB 
            responseString = System.Text.RegularExpressions.Regex.Replace(responseString, "UTF\\-8", "UTF-16");

            LogCrmOperation(string.Empty, responseString, string.Format("Delete Account Record with Id {0}", recordId), CrmOparationLogsType.DeleteAccount, null);

            return response;
        }

        public ZohoCrmResponse DeleteZohoCrmContactRecord(string recordId)
        {
            string authorizationToken = base._settingsService.GetSettingValue(SettingKeyName.ZohoCrmAuthorizationToken);
            string url = string.Format(@"https://crm.zoho.com/crm/private/xml/Contacts/deleteRecords?authtoken={0}&scope=crmapi&id={1}", authorizationToken, recordId);

            var communication = new ZohoCrmCommunication();

            string responseString = communication.SendRequest(url, string.Empty);

            ZohoCrmResponse response = null;
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
            {
                XmlSerializer xmlResponseSerializer = new XmlSerializer(typeof(ZohoCrmResponse));

                try
                {
                    response = (ZohoCrmResponse)xmlResponseSerializer.Deserialize(stream);
                }
                catch (Exception ex)
                {
                    response = new ZohoCrmResponse { Error = new ZohoCrmResult { Code = string.Empty, Message = ex.Message } };
                }
            }

            // Replace encoding for DB 
            responseString = System.Text.RegularExpressions.Regex.Replace(responseString, "UTF\\-8", "UTF-16");

            LogCrmOperation(string.Empty, responseString, string.Format("Delete Contact Record with Id {0}", recordId), CrmOparationLogsType.DeleteContact, null);

            return response;
        }

        #region private members

        private void LogCrmOperation(string request, string response, string message, CrmOparationLogsType operationType, User user)
        {
            CrmOperationLog log = new CrmOperationLog
            {
                CrmRequest = request,
                CrmResponse = response,
                Message = message,
                OperationType = (int)operationType,
                RowDate = DateTime.Now,
                User = user
            };

            _crmOperationLogService.AddOperationLog(log);
        }

        #endregion
    }
}
