﻿using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Services class for managing payment gateway operation log entries. Contains basic retrieving operations.
    /// </summary>
    public class PaymentGatewayOperationLogService : BaseService, IPaymentGatewayOperationLogService
    {
        #region Ctor

        public PaymentGatewayOperationLogService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventLogService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public PaymentGatewayOperationLogService(OTPEntities ctx)
            : base(ctx)
		{ }

        #endregion

        /// <summary>
        /// Add new operation log to the db
        /// </summary>
        /// <param name="operationLog"></param>
        public void AddOperationLog(PaymentGatewayOperationsLog operationLog)
        {
            try
            {
                _Context.PaymentGatewayOperationsLogs.Add(operationLog);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Payment Gateway Operation Log", ex);
            }
        }

        /// <summary>
        /// Gets all logs
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PaymentGatewayOperationsLog> GetPaymentGatewayOperationsLogs()
        {
            var list = _Context.PaymentGatewayOperationsLogs.AsEnumerable().OrderByDescending(e => e.RowDate);
            return list;
        }

        /// <summary>
        /// Gets log with specific id
        /// </summary>
        /// <returns></returns>
        public PaymentGatewayOperationsLog GetPaymentGatewayOperationsLogById(int id)
        {
            return _Context.PaymentGatewayOperationsLogs.Where(e => e.Id == id).SingleOrDefault();
        }

        /// <summary>
        /// Deletes specific event log entry
        /// </summary>
        /// <param name="id"></param>
        public void DeletePaymentGatewayOperationsLogById(int id)
        {
            try
            {
                PaymentGatewayOperationsLog eventLog = this.GetPaymentGatewayOperationsLogById(id);
                _Context.PaymentGatewayOperationsLogs.Remove(eventLog);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Payment Gateway Operation Log with id {0}", id), ex);
            }
        }
    }
}
