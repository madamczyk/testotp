﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Services class for managing amenities groups. Contains basic CRUD operations.
    /// </summary>
    public class AmenityGroupsService : BaseService, IAmenityGroupsService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AmenityGroupService"/> class.
        /// </summary>
        public AmenityGroupsService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AmenityGroupsService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public AmenityGroupsService(OTPEntities ctx)
            : base(ctx)
        { }

        /// <summary>
        /// Get Amenity Group by its internal Id
        /// </summary>
        /// <param name="id">internal id of the Amenity Group</param>
        /// <returns>Amenity Group retrieved</returns>
        public AmenityGroup GetAmenityGroupById(int id)
        {
            try
            {
                return this._Context.AmenityGroups.Where(p => p.AmenityGroupId == id).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get Amenity Group with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Add Amenity Group
        /// </summary>
        /// <param name="p">Amenity Group to add</param>
        public void AddAmenityGroup(AmenityGroup p)
        {
            p.Position = GetPositionForNewAmenityGroup();

            try
            {
                _Context.AmenityGroups.Add(p);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Amenity Group", ex);
            }
        }

        /// <summary>
        /// Delete Amenity Group
        /// </summary>
        /// <param name="id">internal id of the Amenity Group</param>
        public void DeleteAmenityGroup(int id)
        {
            try
            {
                AmenityGroup amenityGroup = GetAmenityGroupById(id);
                IEnumerable<AmenityGroup> amenityGroups = GetAmenityGroups().Where(ag => ag.Position > amenityGroup.Position);

                _Context.AmenityGroups.Remove(amenityGroup);

                foreach (var ag in amenityGroups)
                    ag.Position -= 1;

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Amenity Group with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets all amenity groups
        /// </summary>
        /// <returns>List of all amenity groups</returns>
        public IEnumerable<AmenityGroup> GetAmenityGroups()
        {
            var list =
                _Context.AutoRetryQuery<IEnumerable<AmenityGroup>>(() =>
                _Context.AmenityGroups.Include("Amenities")).AsEnumerable().OrderBy(p => p.Position);
            return list;
        }

        public bool HasAnyDependencies(int id)
        {
            return _Context.Amenities.Any(a => a.AmenityGroup.AmenityGroupId == id);
        }

        public AmenityGroup GetOtherAmenityGroupById()
        {
            return _Context.AmenityGroups.Where(ag => ag.AmenityIcon == 0).FirstOrDefault();
        }

        /// <summary>
        /// Find the last position in the list of amenity groups, increment it and return. If no amenity groups present, return 1.
        /// </summary>
        /// <returns>position for new amenity group</returns>
        private int GetPositionForNewAmenityGroup()
        {
            try
            {
                var list = _Context.AmenityGroups.ToList();

                if (list.Count == 0)
                    return 1;
                else
                    return list.Max(ag => ag.Position) + 1;
            }
            catch (Exception ex)
            {
                throw new Exception("Can't get new position for amenity group", ex);
            }
        }

        /// <summary>
        /// Change the position of amenity group with chosen id by given number of positions
        /// </summary>
        /// <param name="amenityGroupId">internal Id of amenity group</param>
        /// <param name="difference">difference between current and new position. This value is automatically changed to fit the range of (1, number of groups + 1)</param>
        public void ChangeAmenityGroupPosition(int amenityGroupId, int difference)
        {
            IEnumerable<AmenityGroup> list;
            AmenityGroup amenityGroup;

            list = GetAmenityGroups();

            if (list.Count() <= 1) // return, if no items in the list or just one item
                return;

            try
            {
                amenityGroup = list.Where(ag => ag.AmenityGroupId == amenityGroupId).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't find Amenity Group with id {0}", amenityGroupId), ex);
            }

            AmenityGroup modifiedAmenityGroup;
            int oldPosition, newPosition;

            oldPosition = amenityGroup.Position;
            newPosition = Math.Max(1, Math.Min(list.Count(), oldPosition + difference));

            if (oldPosition == newPosition) // return, if new position is same as the old one
                return;

            try
            {
                modifiedAmenityGroup = list.Where(ag => ag.Position == newPosition).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't find Amenity Group with position {0}", amenityGroupId), ex);
            }

            amenityGroup.Position = newPosition;
            modifiedAmenityGroup.Position = oldPosition;

            try
            {
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't save changes after position change", amenityGroupId), ex);
            }
        }
    }
}
