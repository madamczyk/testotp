﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;
using DataAccess;

namespace BusinessLogic.Services
{
    public class ScheduledTasksService : BaseService, IScheduledTasksService
    {
        #region Ctor

        public ScheduledTasksService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventLogService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public ScheduledTasksService(OTPEntities ctx)
            : base(ctx)
		{ }

        #endregion

        #region IScheduledTasksService members

        /// <summary>
        /// Updates the schedule task
        /// </summary>
        /// <param name="message"></param>
        public void UpdateScheduledTask(ScheduledTask scheduledTask)
        {
            _Context.SaveChanges();
        }

        /// <summary>
        /// Removes scheduled task from the database.
        /// </summary>
        /// <param name="scheduledTaskId">The id of scheduled task</param>
        public void DeleteScheduledTask(int scheduledTaskId)
        {
            try
            {
                ScheduledTask scheduledTask =
                    _Context.AutoRetryQuery<ScheduledTask>(() => _Context.ScheduledTasks.SingleOrDefault(st => st.ScheduledTaskId == scheduledTaskId));

                _Context.ScheduledTasks.Remove(scheduledTask);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Can't delete scheduled task with id {0}", scheduledTaskId), ex);
            }
        }

        /// <summary>
        /// Add new scheduled task
        /// </summary>
        /// <param name="message"></param>
        public void AddScheduledTask(ScheduledTask scheduledTask)
        {
            try
            {
                _Context.ScheduledTasks.Add(scheduledTask);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Scheduled Task", ex);
            }
        }

        /// <summary>
        /// Returns tasks which needs to be executed by task scheduler.
        /// </summary>
        /// <returns>The list of scheduled tasks</returns>
        public IEnumerable<ScheduledTask> GetScheduledTasksToExecute()
        {
            return _Context.AutoRetryQuery<IEnumerable<ScheduledTask>>(
                () => _Context.ScheduledTasks
                          .Where(st => (st.NextPlannedExecutionTime == null || st.NextPlannedExecutionTime <= DateTime.Now) && !st.TaskCompleted)
                          .ToList());
        }

        /// <summary>
        /// Returns all scheduled tasks.
        /// </summary>
        /// <returns>The list of scheduled tasks</returns>
        public IEnumerable<ScheduledTask> GetScheduledTasks()
        {
            return _Context.AutoRetryQuery<IEnumerable<ScheduledTask>>(
                () => _Context.ScheduledTasks.OrderByDescending(st => st.NextPlannedExecutionTime).ToList());
        }

        /// <summary>
        /// Get scheduled task with specific id
        /// </summary>
        /// <param name="id">Id of the scheduled task</param>
        /// <returns></returns>
        public ScheduledTask GetScheduledTaskById(int id)
        {
            return _Context.AutoRetryQuery<ScheduledTask>(
                () => _Context.ScheduledTasks.Where(e => e.ScheduledTaskId == id).SingleOrDefault());
        }

        #endregion
    }
}
