﻿using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.Entity;
using BusinessLogic.Managers;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Objects;
using Common.Serialization;
using System.IO;
using System.Drawing.Imaging;
using Common.ImageProcessing;
using BusinessLogic.Objects.StaticContentRepresentation;
using Common.Converters;

namespace BusinessLogic.Services
{
    public class PropertiesServices : BaseService, IPropertiesService
    {
        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertiesServices"/> class.
        /// </summary>
        public PropertiesServices(IConfigurationService configurationService, IBlobService blobService, ISettingsService settingsService, IContextService httpContextService)
            : base(httpContextService, new Dictionary<Type, object>() {
                                                                        { typeof(IConfigurationService), configurationService },
                                                                        { typeof(IBlobService),  blobService },
                                                                        { typeof(ISettingsService), settingsService}})
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertiesServices"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public PropertiesServices(OTPEntities ctx, IConfigurationService configurationService, IBlobService blobService, ISettingsService settingsService)
            : base(new Dictionary<Type, object>() {
                                                    { typeof(IConfigurationService), configurationService },
                                                    { typeof(IBlobService),  blobService },
                                                    { typeof(ISettingsService), settingsService}}, ctx)
        { }

        #endregion

        #region Property CRUD

        /// <summary>
        /// Get Property by its internal Id
        /// </summary>
        /// <param name="id">internal id of the property</param>
        /// <returns>Retrieved property</returns>
        public Property GetPropertyById(int id, bool onlyAvailable = false)
        {
            try
            {
                var q = this._Context.Properties.Where(p => p.PropertyID == id).FirstOrDefault();

                if (onlyAvailable && !q.PropertyLive)
                {
                    throw new Exception("Cannot take property that is not active!");
                }

                return q;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get property with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Get Property by its internal Id
        /// </summary>
        /// <param name="id">internal id of the property</param>
        /// <param name="numberOfUnits">Output parameter - number of units connected to property</param>
        /// <param name="onlyAvailable">True if tak only active property</param>
        /// <returns>Retrieved property</returns>
        public Property GetPropertyById(int id, out int numberOfUnits, bool onlyAvailable = false)
        {
            try
            {
                var q = this._Context.Properties.Where(p => p.PropertyID == id).FirstOrDefault();

                if (onlyAvailable && !q.PropertyLive)
                {
                    throw new Exception("Cannot take property that is not active!");
                }

                numberOfUnits = q.Units.Count();

                return q;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get property with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Get Properties by their owner id
        /// </summary>
        /// <param name="id">internal id of the owner</param>
        /// <param name="onlyAvailable">true when return only active properties</param>
        /// <returns></returns>
        public IEnumerable<Property> GetPropertiesByOwnerId(int id, bool onlyAvailable = false)
        {
            try
            {
                var query = this._Context.Properties.Where(p => p.User.UserID == id).Include(s => s.Units).Include(s => s.Destination);
                if (onlyAvailable)
                    query = query.Where(p => p.PropertyLive.Equals(true));

                return query.AsEnumerable().OrderBy(p => p.PropertyNameCurrentLanguage);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get properties with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Get properties owned by assigned Manager
        /// </summary>
        /// <param name="managerId">Id of the Manager</param>
        /// <param name="onlyAvailable">true when return only active properties</param>
        /// <param name="roleLevel">Manager assigned role</param>
        /// <returns></returns>
        public IEnumerable<Property> GetPropertiesByManager(int managerId, RoleLevel roleLevel, bool onlyAvailable = false)
        {
            int managerRole = (int)roleLevel;

            var query = this._Context.Properties.Where(p => p.PropertyAssignedManagers.Any(m => m.User.UserID.Equals(managerId) && m.Role.RoleLevel.Equals(managerRole)));
            if (onlyAvailable)
                query = query.Where(p => p.PropertyLive.Equals(true));

            return query.AsEnumerable().OrderBy(p => p.PropertyNameCurrentLanguage);
        }

        /// <summary>
        /// Retrieves PropertyID from the UnitId value
        /// </summary>
        /// <param name="unitID"></param>
        /// <returns>PropertyID of the Unit</returns>
        public int GetPropertyIdByUnitId(int unitID)
        {
            return _Context.Units.Where(u => u.UnitID.Equals(unitID)).Select(u => u.Property.PropertyID).SingleOrDefault();
        }

        /// <summary>
        /// Add Property
        /// </summary>
        public void AddProperty(Property p)
        {
            try
            {
                _Context.Properties.Add(p);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Property", ex);
            }
        }

        /// <summary>
        /// Searches Properties
        /// </summary>
        public IEnumerable<PropertiesSearchResult> SearchProperties(int? destinationId = null, DateTime? dateFrom = null, DateTime? dateTo = null, int? numberOfGuests = null, int? numberOfBathrooms = null, int? numberOfBedrooms = null, string propertyTypes = null, string tags = null, string amenities = null, string languageCode = null, decimal? beginLatitude = null, decimal? endLatitude = null, decimal? beginLongitude = null, decimal? endLongitude = null)
        {
            List<PropertiesSearchResult> searchResult = new List<PropertiesSearchResult>(); ;
            string blobContainter = _configurationService.GetKeyValue(Enums.CloudConfigurationKeys.StorageBlobUrl);

            int timeOfStayDays = 1;

            if (dateFrom.HasValue && dateTo.HasValue)
            {
                timeOfStayDays = (dateTo.Value - dateFrom.Value).Days;
            }

            try
            {
                searchResult = _Context.sp_SearchProperties(destinationId, dateFrom, dateTo, numberOfGuests, numberOfBathrooms, numberOfBedrooms, propertyTypes, amenities, tags, beginLatitude, endLatitude, beginLongitude, endLongitude, languageCode)
                .Select(item => new PropertiesSearchResult()
                {
                    Bathrooms = item.NumberOfBathrooms,
                    Bedrooms = item.NumberOfBedrooms,
                    Description = item.Description,
                    Id = item.UnitID,
                    Image = string.Format("{0}{1}", blobContainter, item.Image),
                    IsPricePerNight = item.IsPricePerNight.Value,
                    Price = item.Price.HasValue ? item.Price.Value : 0,
                    PricePerNight = item.Price.HasValue ? (item.Price.Value / timeOfStayDays) : 0,
                    Rating = item.Rating,
                    Sleeps = item.NumberOfGuests,
                    Thumbnails = item.Thumbnails == null ? new List<string>() : item.Thumbnails.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(s => string.Format("{0}{1}", blobContainter, s)).ToList(),
                    Title = item.Title,
                    Latitude = item.Latitude,
                    Longitude = item.Longitude,
                    CultureCode = item.CultureCode,
                    Destination = item.Destination,
                    FinalScore = System.Convert.ToInt32(item.FinalScore.Value * 10)
                }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't search for properties", ex);
            }
            return searchResult;
        }

        /// <summary>
        /// Gets set of details related to final score
        /// </summary>
        /// <param name="numberOfGuests">Number of guests defined in the search parameters</param>
        /// <param name="numberOfBathrooms">Number of bathrooms defined in the search parameters</param>
        /// <param name="numberOfBedrooms">Number of bedrooms defined in search parameters</param>
        /// <param name="propertyTypes">Property types selcted in the search parameters</param>
        /// <param name="tags">Property tags (experiences) selected in the search parameters</param>
        /// <param name="amenities">Selected amenities</param>
        /// <returns></returns>
        public SearchFinalScoreDetails GetSearchScoreDetails(int unitId, int? numberOfGuests = null, int? numberOfBathrooms = null, int? numberOfBedrooms = null, string propertyTypes = null, string tags = null, string amenities = null)
        {
            var amenitiesList = _Context.Amenities.ToList();
            var tagsList = _Context.Tags.ToList();
            var propertyTypesList = _Context.PropertyTypes.ToList();
            var property = _Context.Units.Where(pr => pr.UnitID == unitId).SingleOrDefault();
            var result = _Context.sp_GetFinalScoreDetails(unitId, property.Property.PropertyID, numberOfGuests, numberOfBathrooms, numberOfBedrooms, propertyTypes, amenities, tags)
                .Select(item => new SearchFinalScoreDetails()
                {
                    FinalScore = item.FinalScore.HasValue ? System.Convert.ToInt32(item.FinalScore.Value * 10) : 0,
                    PropertyType = property.Property.PropertyType.NameCurrentLanguage,
                    UnitBathrooms = item.UnitBathrooms,
                    UnitBedrooms = item.UnitBedrooms,
                    UnitGuests = item.UnitGuests,
                    NotMatchedAmenities = (!string.IsNullOrWhiteSpace(item.NotMatchingAmenities)) ? item.NotMatchingAmenities.ToIntList(";").Select(o => amenitiesList.Where(am => am.AmenityID == o).Select(ob => ob.TitleCurrentLanguage).SingleOrDefault()).ToList() : null,
                    NotMatchedPropertyTypes = (!string.IsNullOrWhiteSpace(item.NotMatchingPropertyTypes)) ? item.NotMatchingPropertyTypes.ToIntList(";").Select(o => propertyTypesList.Where(am => am.PropertyTypeId == o).Select(ob => ob.NameCurrentLanguage).SingleOrDefault()).ToList() : null,
                    NotMatchedExperiences = (!string.IsNullOrWhiteSpace(item.NotMatchingTags)) ? item.NotMatchingTags.ToIntList(";").Select(o => tagsList.Where(am => am.TagID == o).Select(ob => ob.NameCurrentLanguage).SingleOrDefault()).ToList() : null
                }).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// Gets propety list
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Property> GetProperties()
        {
            return _Context.Properties.AsEnumerable().OrderBy(p => p.PropertyID);
        }

        public void AddPropertyManager(int userId, int propertyId, RoleLevel managerRole)
        {
            var property = _Context.Properties.Where(pr => pr.PropertyID == propertyId).Single();
            PropertyAssignedManager pam = new PropertyAssignedManager();
            pam.User = _Context.Users.Where(us => us.UserID == userId).Single();
            pam.Role = _Context.Roles.Where(r => r.RoleLevel == (int)managerRole).Single();
            pam.Property = property;
            property.PropertyAssignedManagers.Add(pam);
        }

        public PropertyAssignedManager GetPropertyManagerByType(int propertyId, RoleLevel managerType)
        {
            int manager = (int)managerType;
            return _Context.PropertyAssignedManagers.Where(pr => pr.Property.PropertyID == propertyId && pr.Role.RoleLevel == manager).SingleOrDefault();
        }

        public void DeleteManagerFromProperty(int propertyAssignedManagerId)
        {
            var propAssignedManager = _Context.PropertyAssignedManagers.Where(id => id.PropertyAssignedManagerId == propertyAssignedManagerId).SingleOrDefault();
            _Context.PropertyAssignedManagers.Remove(propAssignedManager);
        }

        public Property GetPropertyByCode(string code)
        {
            return _Context.Properties.Where(pc => pc.PropertyCode.ToLower() == code.ToLower().Trim()).FirstOrDefault();
        }

        #endregion

        #region Check dependencies

        /// <summary>
        /// Determine if tag is used in any property
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public bool HasAnyUseTag(int tagId)
        {
            return _Context.PropertyTags.Any(pt => pt.Tag.TagID == tagId);
        }

        /// <summary>
        /// Determine if property type is used in any property
        /// </summary>
        /// <param name="propertyTypeId"></param>
        /// <returns></returns>
        public bool HasAnyUsePropertyType(int propertyTypeId)
        {
            return _Context.Properties.Any(pr => pr.PropertyType.PropertyTypeId == propertyTypeId);
        }

        /// <summary>
        /// Determine if amenity is used in any property
        /// </summary>
        /// <param name="amenityId"></param>
        /// <returns></returns>
        public bool HasAnyUseAmenity(int amenityId)
        {
            return _Context.PropertyAmenities.Any(pa => pa.Amenity.AmenityID == amenityId);
        }

        /// <summary>
        /// Determine if this property has any reservation
        /// </summary>
        /// <param name="propertyId">Property Id</param>
        /// <returns></returns>
        public bool HasAnyReservation(int propertyId)
        {
            return _Context.Reservations.Any(r => r.Property.PropertyID == propertyId);
        }

        #endregion

        #region Guest Reviews

        /// <summary>
        /// Gets calculated Guest Review rate for the property
        /// </summary>
        /// <param name="propertyId">Id of the property</param>
        /// <returns>Guest review rate</returns>
        public int? GetPropertyReviewRate(int propertyId)
        {
            return _Context.Properties.Where(s => s.PropertyID.Equals(propertyId)).Select(s => OTPEntities.GetPropertyRating(propertyId)).First();
        }

        /// <summary>
        /// Retrieves guest reviews connected to concrete Property
        /// </summary>
        /// <param name="propertyId">Property Id for reviews</param>
        /// <returns>List of reviews ordered by the newest review</returns>
        public IEnumerable<GuestReview> GetPropertyGuestReviews(int propertyId)
        {
            var q = _Context.GuestReviews.Where(r => r.Property.PropertyID.Equals(propertyId)).Include(s => s.User).OrderByDescending(r => r.ReviewDate).Select(r => r);

            return q.AsEnumerable();
        }

        #endregion

        #region Static Content

        public IEnumerable<T> GetPropertyStaticContentByType<T>(int propertyId, PropertyStaticContentType propertyStaticContentType)
        {
            IEnumerable<PropertyStaticContent> staticContent = GetPropertyStaticContentByType(propertyId, propertyStaticContentType);
            return staticContent
                    .Select(pfp => SerializationHelper.DeserializeObject<T>(pfp.ContentValue_i18n))
                    .AsEnumerable();
        }

        public IEnumerable<T> GetPropertyStaticContentByTypeFrom<T>()
        {
            return null;
        }



        public void AddPropertyStaticContent<T>(object staticContentObject, PropertyStaticContentType type, int propertyId)
        {
            PropertyStaticContent psc = new PropertyStaticContent()
            {
                ContentCode = Convert.ToInt32(type),
                ContentValue_i18n = SerializationHelper.SerializeObject(staticContentObject),
                Property = GetPropertyById(propertyId)
            };

            this.AddPropertyStaticContent(psc);
            _Context.SaveChanges();
        }

        public IEnumerable<PropertyStaticContentResult> GetPropertyStaticContentInfo(int propertyId)
        {
            var q = (
                from item in _Context.PropertyStaticContents
                where
                   item.Property.PropertyID.Equals(propertyId)
                group item by item.ContentCode into g
                select new PropertyStaticContentResult
                {
                    ContentCode = g.Key,
                    ContentItems = g
                }
                     );
            return q;
        }

        /// <summary>
        /// Gets property static content by property ID and content code
        /// </summary>
        /// <param name="propertyId">Property ID</param>
        /// <param name="propertyStaticContentType">Content Code</param>
        /// <returns>List of property static content</returns>
        public IEnumerable<PropertyStaticContent> GetPropertyStaticContentByType(int propertyId, PropertyStaticContentType propertyStaticContentType)
        {
            return _Context.PropertyStaticContents.Where(p => p.Property.PropertyID == propertyId && p.ContentCode == (int)propertyStaticContentType).AsEnumerable();
        }

        /// <summary>
        /// Remove static content by id
        /// </summary>
        /// <param name="itemId"></param>
        public void RemoveStaticContent(int itemId)
        {
            try
            {
                PropertyStaticContent psc = GetPropertyStaticContentById(itemId);
                _Context.PropertyStaticContents.Remove(psc);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove property static content with id" + itemId.ToString(), ex);
            }
        }

        /// <summary>
        /// Gets property static content by id
        /// </summary>
        /// <param name="id">Static Content ID</param>
        /// <returns></returns>
        public PropertyStaticContent GetPropertyStaticContentById(int id)
        {
            return _Context.PropertyStaticContents.Where(p => p.PropertyStaticContentId == id).FirstOrDefault();
        }

        /// <summary>
        /// Adds static content to property
        /// </summary>
        /// <param name="obj">Property Static Content</param>
        public void AddPropertyStaticContent(PropertyStaticContent obj)
        {
            _Context.PropertyStaticContents.Add(obj);
        }

        /// <summary>
        /// Gets Property Static Contents which match to given content types
        /// </summary>
        /// <param name="pid">Property ID</param>
        /// <param name="cts">Content Type List</param>
        /// <returns></returns>
        public IEnumerable<PropertyStaticContent> GetPropertyStaticContentByType(int pid, List<PropertyStaticContentType> cts)
        {
            List<PropertyStaticContent> result = new List<PropertyStaticContent>();
            foreach (PropertyStaticContentType psc in cts)
            {
                int code = (int)psc;
                result.AddRange(_Context.PropertyStaticContents.Where(p => p.Property.PropertyID == pid && p.ContentCode == code));
            }
            return result.AsEnumerable();
        }

        /// <summary>
        /// Get Property Square Footage from static content by Property Id
        /// </summary>
        /// <param name="propertyId">Id of the property</param>
        /// <returns>Retrieved Square Footage value</returns>
        public string GetPropertySquareFootage(int propertyId)
        {
            var s = (from item in _Context.PropertyStaticContents
                     where item.Property.PropertyID.Equals(propertyId)
                     && item.ContentCode.Equals((int)PropertyStaticContentType.SquareFootage)
                     select item).SingleOrDefault();

            return s == null ? string.Empty : s.ContentValue;
        }

        /// <summary>
        /// Get images for property fullscreen on thumbnail - depending on param
        /// </summary>
        /// <param name="id">Property id</param>
        /// <param name="imageType">Image type</param>
        /// <returns></returns>
        public IEnumerable<String> GetPropertyImagesByPropertyId(int id, PropertyStaticContentType imageType)
        {
            string blobContainter = _configurationService.GetKeyValue(Enums.CloudConfigurationKeys.StorageBlobUrl);

            IEnumerable<string> q = (from content in _Context.PropertyStaticContents
                                     where
                                         content.ContentCode == ((int)imageType)
                                         && content.Property.PropertyID.Equals(id)
                                     select content.ContentValue).AsEnumerable();

            return q.Select(s => string.Format("{0}{1}", blobContainter, s));
        }

        /// <summary>
        /// Gets property static contents with ContentValue, that references file in Storage
        /// </summary>
        /// <param name="includeTypes">Records of type in the list will be included in result. Set to null, if result should contain all records from database.</param>
        /// <returns></returns>
        public IEnumerable<PropertyStaticContent> GetPropertyStaticContents(List<PropertyStaticContentType> includeTypes = null)
        {
            IEnumerable<PropertyStaticContent> list;

            if (includeTypes != null)
            {
                var types = includeTypes.Select(t => (int)t);

                list = _Context.PropertyStaticContents.Where(psc => types.Contains(psc.ContentCode)).OrderBy(psc => psc.ContentCode).ToList();
            }
            else
                list = _Context.PropertyStaticContents.OrderBy(psc => psc.ContentCode).ToList();

            return list;
        }
        #endregion

        #region Property Associations CRUD

        /// <summary>
        /// Adds property amenity
        /// </summary>
        /// <param name="propAmenity"></param>
        public void AddPropertyAmenity(PropertyAmenity propAmenity)
        {
            _Context.PropertyAmenities.Add(propAmenity);
        }

        /// <summary>
        /// Adds property tag 
        /// </summary>
        /// <param name="propTag"></param>
        public void AddPropertyTag(PropertyTag propTag)
        {
            _Context.PropertyTags.Add(propTag);
        }

        /// <summary>
        /// Gets property amenity by internal id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PropertyAmenity GetPropertyAmenityById(int id)
        {
            return _Context.PropertyAmenities.Where(p => p.PropertyAmenityID == id).FirstOrDefault();
        }

        /// <summary>
        /// Gets property tag by internal id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PropertyTag GetPropertyTagById(int id)
        {
            return _Context.PropertyTags.Where(p => p.PropertyTagID == id).FirstOrDefault();
        }

        /// <summary>
        /// Delete property amenity
        /// </summary>
        /// <param name="id"></param>
        public void RemovePropertyAmenity(int id)
        {
            try
            {
                PropertyAmenity pa = GetPropertyAmenityById(id);
                _Context.PropertyAmenities.Remove(pa);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove property amenity with id" + id.ToString(), ex);
            }
        }

        /// <summary>
        /// Delete property amenity
        /// </summary>
        /// <param name="id"></param>
        public void RemovePropertyAmenitiesByPropertyId(int propertyId)
        {
            try
            {
                List<PropertyAmenity> list = _Context.PropertyAmenities.Where(pa => pa.Property.PropertyID == propertyId).ToList();

                foreach (var pa in list)
                    _Context.PropertyAmenities.Remove(pa);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove property amenities with property id" + propertyId.ToString(), ex);
            }
        }

        /// <summary>
        /// Delete property tag by internal id
        /// </summary>
        /// <param name="id"></param>
        public void RemovePropertyTag(int id)
        {
            try
            {
                PropertyTag pt = GetPropertyTagById(id);
                _Context.PropertyTags.Remove(pt);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove object with id" + id.ToString(), ex);
            }
        }

        /// <summary>
        /// Delete property by internal id
        /// </summary>
        /// <param name="id">Id of the property</param>
        public void RemoveProperty(int id)
        {
            try
            {
                Property property = GetPropertyById(id);

                #region Remove all depended data without delete cascade option

                for(int i = property.Units.Count - 1; i >= 0; --i)
                {
                    _Context.Units.Remove(property.Units.ElementAt(i));
                }

                for (int i = property.UnitTypes.Count - 1; i >= 0; --i)
                {
                    _Context.UnitTypes.Remove(property.UnitTypes.ElementAt(i));
                }

                for (int i = property.PropertySupportedLanguages.Count - 1; i >= 0; --i)
                {
                    _Context.PropertySupportedLanguages.Remove(property.PropertySupportedLanguages.ElementAt(i));
                }

                #endregion

                _Context.Properties.Remove(property);

                _Context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove object with id" + id.ToString(), ex);
            }
        }

        /// <summary>
        /// Gets property amenity by property id and amenity id
        /// </summary>
        /// <param name="pid">Property ID</param>
        /// <param name="amId">Amenity ID</param>
        /// <returns></returns>
        public PropertyAmenity GetPropertyAmenity(int pid, int amId)
        {
            var res = _Context.PropertyAmenities.Where(p => p.Property.PropertyID == pid && p.Amenity.AmenityID == amId).FirstOrDefault();
            return res;
        }

        /// <summary>
        /// Gets property tag by property id and tag id
        /// </summary>
        /// <param name="pid">Property ID</param>
        /// <param name="tagId">Tag ID</param>
        /// <returns></returns>
        public PropertyTag GetPropertyTag(int pid, int tagId)
        {
            return _Context.PropertyTags.Where(p => p.Property.PropertyID == pid && p.Tag.TagID == tagId).FirstOrDefault();
        }

        /// <summary>
        /// Gets property amenities
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public IEnumerable<PropertyAmenity> GetPropertyAmenitiesByPropertyId(int pid)
        {
            return _Context.PropertyAmenities.Include(ag => ag.Amenity.AmenityGroup).Where(pr => pr.Property.PropertyID == pid).AsEnumerable();
        }

        /// <summary>
        /// Gets property tags
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public IEnumerable<PropertyTag> GetPropertyTagsByPropertyId(int pid)
        {
            return _Context.PropertyTags.Include(pt => pt.Tag.TagGroup).Where(pr => pr.Property.PropertyID == pid).AsEnumerable();
        }

        /// <summary>
        /// Gets distinct amenity groups for property
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public IEnumerable<int> GetAmenityGroupsByPropertyId(int pid)
        {
            return _Context.PropertyAmenities
                .Where(pa => pa.Property.PropertyID == pid && pa.Amenity.AmenityGroup != null)
                .Select(pr => pr.Amenity.AmenityGroup.AmenityGroupId)
                .Distinct()
                .AsEnumerable();
        }

        /// <summary>
        /// Calculate commission for period. Calculate % of total sum which OTP will get from transaction
        /// </summary>
        /// <param name="dateFrom">Period date start</param>
        /// <param name="dateTo">Period date end</param>
        /// <param name="total">Total sum of invoice</param>
        /// <param name="propertyId">Property ID</param>
        /// <returns>Commission of total amount</returns>
        public decimal CalculatePropertyCommission(DateTime dateFrom, DateTime dateTo, decimal total, int propertyId, int lengthOfStay)
        {
            decimal avgCommission = 0;
            decimal stdCommission = GetPropertyById(propertyId).StandardCommission;
            List<PropertyCommissionOverride> propertyCommissions = _Context.PropertyCommissionOverrides.Where(c => c.Property.PropertyID == propertyId && dateFrom >= c.DateFrom && dateTo <= c.DateUntil).ToList();
            PropertyCommissionOverride commission;
            dateFrom = dateFrom.AddDays(1);

            while (dateFrom <= dateTo)
            {
                commission = propertyCommissions.Where(c => dateFrom >= c.DateFrom && c.DateUntil <= dateFrom).SingleOrDefault();
                if (commission != null)
                {
                    avgCommission += commission.CommissionOverride;
                }
                else
                {
                    avgCommission += stdCommission;
                }
                dateFrom = dateFrom.AddDays(1);
            }
            return (total * (avgCommission / (decimal)lengthOfStay)) / 100M;
        }

        #endregion

        #region Floor Plans

        /// <summary>
        /// Returns all floor plans for specified property
        /// </summary>
        /// <param name="propertyID">The property id</param>
        /// <returns></returns>
        public IEnumerable<FloorPlan> GetPropertyFloorPlans(int propertyID)
        {
            IEnumerable<PropertyStaticContent> propertyFloorPlans = GetPropertyStaticContentByType(propertyID,
               new List<PropertyStaticContentType> { PropertyStaticContentType.FloorPlan });

            return propertyFloorPlans
                    .Select(pfp => SerializationHelper.DeserializeObject<FloorPlan>(pfp.ContentValue))
                    .AsEnumerable();
        }

        /// <summary>
        /// Adds floor plan to specified property
        /// </summary>
        /// <param name="propertyID">ID of the property</param>
        /// <param name="floorPlan">Floor plan object</param>
        /// <returns></returns>
        public PropertyStaticContent AddPropertyFloorPlan(int propertyID, FloorPlan floorPlan)
        {
            //If floor plan should be default. Set all other to false
            //if (floorPlan.IsDefault)
            //{
                IList<PropertyStaticContent> propertyFloorPlans = GetPropertyStaticContentByType(propertyID,
                    new List<PropertyStaticContentType> { PropertyStaticContentType.FloorPlan }).ToList();

                foreach (var propertyFloorPlan in propertyFloorPlans)
                {
                    FloorPlan dbFloorPlan = SerializationHelper.DeserializeObject<FloorPlan>(propertyFloorPlan.ContentValue);
                    //dbFloorPlan.IsDefault = false;
                    propertyFloorPlan.ContentValue = SerializationHelper.SerializeObject(dbFloorPlan);
                }
           // }

            PropertyStaticContent psc = new PropertyStaticContent()
            {
                ContentCode = Convert.ToInt32(PropertyStaticContentType.FloorPlan),
                ContentValue = SerializationHelper.SerializeObject(floorPlan),
                Property = GetPropertyById(propertyID)
            };

            AddPropertyStaticContent(psc);
            _Context.SaveChanges();

            return psc;
        }

        /// <summary>
        /// Edit specified floor plan
        /// </summary>
        /// <param name="floorPlanID">Floor plan id</param>
        /// <param name="floorPlan">Floor plan object</param>
        public void EditPropertyFloorPlan(int floorPlanID, FloorPlan floorPlan)
        {
            PropertyStaticContent psc = GetPropertyStaticContentById(floorPlanID);
            if (psc != null)
            {              
                var dbFloorPlanValues = SerializationHelper.DeserializeObject<FloorPlan>(psc.ContentValue);
                floorPlan.Name_i18n = floorPlan.Name_i18n;
                psc.ContentValue = SerializationHelper.SerializeObject(floorPlan);
            }

            _Context.SaveChanges();
        }

        /// <summary>
        /// Adds specific panorama to floor plan.
        /// </summary>
        /// <param name="propertyFloorPlanId">Floor plan static content id</param>
        /// <param name="panorama">The panorama object</param>
        public void AddPanoramaToPropertyFloorPlan(int floorPlanID, Panorama panorama)
        {
            PropertyStaticContent sc = GetPropertyStaticContentById(floorPlanID);
            if (sc != null)
            {
                FloorPlan fp = SerializationHelper.DeserializeObject<FloorPlan>(sc.ContentValue);

                IList<Panorama> panoramas = fp.Panoramas == null
                    ? new List<Panorama>()
                    : new List<Panorama>(fp.Panoramas);

                Panorama pano = new Panorama()
                {
                    Id = panoramas.Count > 0 ? panoramas.Select(p => p.Id).Max() + 1 : 1,
                    PanoramaPath = panorama.PanoramaPath,
                    OffsetX = panorama.OffsetX,
                    OffsetY = panorama.OffsetY,                    
                };
                pano.SetPanoramaNameValue(panorama.PanoramaName.ToString());

                panoramas.Add(pano);

                fp.Panoramas = panoramas.ToArray();

                sc.ContentValue = SerializationHelper.SerializeObject(fp);
            }
        }

        /// <summary>
        /// Modifies specified floor plan's panorama
        /// </summary>
        /// <param name="floorPlanID">The floor plan id</param>
        /// <param name="panorama">The panorama object which contains modifications</param>
        public void EditPropertyFloorPlansPanorama(int floorPlanID, Panorama modifiedPanorama)
        {
            PropertyStaticContent sc = GetPropertyStaticContentById(floorPlanID);
            if (sc != null)
            {
                FloorPlan fp = SerializationHelper.DeserializeObject<FloorPlan>(sc.ContentValue);
                var panorama = fp.Panoramas.FirstOrDefault(p => p.Id == modifiedPanorama.Id);
                if (panorama != null)
                {
                    panorama.OffsetX = modifiedPanorama.OffsetX;
                    panorama.OffsetY = modifiedPanorama.OffsetY;
                    panorama.PanoramaPath = modifiedPanorama.PanoramaPath;
                }
                panorama.SetPanoramaNameValue(modifiedPanorama.PanoramaName.ToString());

                sc.ContentValue = SerializationHelper.SerializeObject(fp);
            }
        }

        /// <summary>
        /// Deletes specified floor plan's panorama
        /// </summary>
        /// <param name="floorPlanID">The floor plan id</param>
        /// <param name="panoramaID">The floor plan's panorama id</param>
        public void DeletePropertyFloorPlansPanorama(int floorPlanID, int panoramaID)
        {
            PropertyStaticContent sc = GetPropertyStaticContentById(floorPlanID);
            if (sc != null)
            {
                FloorPlan fp = SerializationHelper.DeserializeObject<FloorPlan>(sc.ContentValue);
                fp.Panoramas = fp.Panoramas.Where(p => p.Id != panoramaID).ToArray();
                sc.ContentValue = SerializationHelper.SerializeObject(fp);
            }

            _Context.SaveChanges();
        }

        /// <summary>
        /// Deletes floor plan(from database and from azure storage)
        /// </summary>
        /// <param name="floorPlanId">The floor plan id</param>
        public void DeletePropertyFloorPlan(int floorPlanId)
        {
            _blobService.InitContainer(_settingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersProperties));

            PropertyStaticContent sc = GetPropertyStaticContentById(floorPlanId);
            if (sc != null)
            {
                FloorPlan fp = SerializationHelper.DeserializeObject<FloorPlan>(sc.ContentValue);
                _blobService.DeleteFile(sc.Property.PropertyID,
                                        fp.OriginalImagePath.Replace(
                                            String.Format("/properties/id{0}/", sc.Property.PropertyID), ""));
                _blobService.DeleteFile(sc.Property.PropertyID,
                                        fp.DisplayImagePath.Replace(
                                            String.Format("/properties/id{0}/", sc.Property.PropertyID), ""));

                RemoveStaticContent(floorPlanId);
            }
        }

        #endregion

        #region DirectionToAirport

        /// <summary>
        /// Returns all directions to airport for specified property
        /// </summary>
        /// <param name="propertyID">The property id</param>
        /// <returns></returns>
        public IEnumerable<DirectionToAirport> GetPropertyDirectionsToAirport(int propertyID)
        {
            IEnumerable<PropertyStaticContent> propertyDirectionsToAirport = GetPropertyStaticContentByType(propertyID, new List<PropertyStaticContentType> { PropertyStaticContentType.DirectionToAirport });

            return propertyDirectionsToAirport
                    .Select(pfp => SerializationHelper.DeserializeObject<DirectionToAirport>(pfp.ContentValue))
                    .AsEnumerable();
        }

        #endregion

        #region Raw Data

        /// <summary>
        /// Get property raw data by its internal ID
        /// </summary>
        /// <param name="id">ID of the property raw data record</param>
        /// <returns></returns>
        public PropertyRawData GetPropertyRawDataById(int id)
        {
            try
            {
                return _Context.PropertyRawDatas.Where(p => p.PropertyRawDataRecordID == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get property raw data with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets property raw data for selected property
        /// </summary>
        /// <param name="propertyId">ID of the property</param>
        /// <returns></returns>
        public IEnumerable<PropertyRawData> GetPropertyRawDatas(int propertyId)
        {
            return _Context.PropertyRawDatas.Where(p => p.Property.PropertyID == propertyId).OrderBy(p => p.RawRecordType).ToList();
        }

        /// <summary>
        /// Add property raw data
        /// </summary>
        /// <param name="p">property raw data to add</param>
        public void AddPropertyRawData(PropertyRawData p)
        {
            try
            {
                _Context.PropertyRawDatas.Add(p);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Property Raw Data", ex);
            }
        }

        /// <summary>
        /// Delete property raw data
        /// </summary>
        /// <param name="id">ID of the property raw data record</param>
        public void DeletePropertyRawData(int id)
        {
            try
            {
                PropertyRawData prd = GetPropertyRawDataById(id);
                string fileName = prd.RawRecordContent;

                _Context.PropertyRawDatas.Remove(prd);
                _Context.SaveChanges();

                _blobService.InitContainer(_settingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersPropertyDawData));
                _blobService.DeleteFile(fileName, true);

            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove property raw data with id" + id, ex);
            }
        }

        /// <summary>
        /// Gets all property raw data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PropertyRawData> GetPropertyRawDatas()
        {
            return _Context.PropertyRawDatas.OrderBy(p => p.RawRecordType).ToList();
        }
        #endregion

        #region Content Update Requests & Details

        /// <summary>
        /// Gets content update request by its internal ID
        /// </summary>
        /// <param name="id">ID of the content update request</param>
        /// <returns></returns>
        public ContentUpdateRequest GetContentUpdateRequestById(int id)
        {
            try
            {
                return _Context.ContentUpdateRequests.Where(p => p.ContentUpdateRequestId == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get content update request with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets content update request detail by its internal ID
        /// </summary>
        /// <param name="id">ID of the content update request detail</param>
        /// <returns></returns>
        public ContentUpdateRequestDetail GetContentUpdateRequestDetailById(int id)
        {
            try
            {
                return _Context.ContentUpdateRequestDetails.Where(p => p.ContentUpdateRequestDetailId == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get content update request detail with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets all content update requests
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ContentUpdateRequest> GetContentUpdateRequests()
        {
            return _Context.ContentUpdateRequests.OrderByDescending(p => p.RequestDateTime).ToList();
        }

        /// <summary>
        /// Gets content update request details for specified request
        /// </summary>
        /// <param name="requestId">Id of the request</param>
        /// <returns></returns>
        public IEnumerable<ContentUpdateRequestDetail> GetContentUpdateRequestDetailsByRequestId(int requestId)
        {
            return _Context.ContentUpdateRequestDetails.Where(d => d.ContentUpdateRequest.ContentUpdateRequestId == requestId).OrderBy(d=> d.ContentCode).ToList();
        }

        /// <summary>
        /// Add content update request
        /// </summary>
        /// <param name="p">content update request to add</param>
        public void AddContentUpdateRequest(ContentUpdateRequest p)
        {
            try
            {
                _Context.ContentUpdateRequests.Add(p);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Content Update Request", ex);
            }
        }


        public void UploadFileToStorage(HttpRequestBase request, List<UploadFilesResult> statuses, int propertyId)
        {
            for (int i = 0; i < request.Files.Count; i++)
            {
                var file = request.Files[i];
                var encodedName = Common.UrlHelper.Encode(file.FileName);
                var name = encodedName;

                FileInfo info = new FileInfo(name);
                int j = 0;
                _blobService.InitContainer(_settingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersContentUpdateRequests));

                while (_blobService.FileExists(propertyId, name))
                {
                    j++;
                    name = encodedName.Replace(info.Extension, "_" + j + info.Extension);
                }
                
                string blobContainter = _configurationService.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl);

                _blobService.AddFile(file.InputStream, propertyId, name, file.ContentType);

                statuses.Add(new UploadFilesResult()
                {
                    name = name,
                    size = file.ContentLength,
                    type = file.ContentType,
                    //url = "/HomeOwner/Download/" + name,
                    //delete_url = "/HomeOwner/Delete/" + name,
                    thumbnail_url = String.Format("{0}/{1}/id{2}/{3}", blobContainter, _settingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersContentUpdateRequests), propertyId, name),
                    //delete_type = "GET"
                });
            }
        }

        /// <summary>
        /// Add content update request detail
        /// </summary>
        /// <param name="p">content update request detail to add</param>
        public void AddContentUpdateRequestDetail(ContentUpdateRequestDetail p)
        {
            try
            {
                _Context.ContentUpdateRequestDetails.Add(p);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Content Update Request Detail", ex);
            }
        }


        /// <summary>
        /// Adds new detail of update request based on type,detailtext,and parent request
        /// </summary>
        /// <returns>ID of the content update request detail</param>
        public int AddContentUpdateRequestDetail(ContentUpdateRequest newRequest, 
                ContentUpdateRequestDetailContentType contentUpdateRequestDetailContentType, string newDetailText)
        {
            ContentUpdateRequestDetail newDetail = new ContentUpdateRequestDetail()
            {
                ContentCode = (int)contentUpdateRequestDetailContentType,
                ContentUpdateRequest = newRequest,
                ContentValue = String.Format("{0}/id{1}/{2}", _settingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersContentUpdateRequests), newRequest.Property.PropertyID, newDetailText)
            };
            AddContentUpdateRequestDetail(newDetail);
            return newDetail.ContentUpdateRequestDetailId;
        }

        /// <summary>
        /// Delete content update request
        /// </summary>
        /// <param name="id">ID of the content update request</param>
        public void DeleteContentUpdateRequest(int id)
        {
            try
            {
                ContentUpdateRequest cur = GetContentUpdateRequestById(id);
                _Context.ContentUpdateRequests.Remove(cur);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove content update request with id" + id, ex);
            }
        }

        /// <summary>
        /// Delete content update request detail
        /// </summary>
        /// <param name="id">ID of the content update request detail</param>
        public void DeleteContentUpdateRequestDetail(int id)
        {
            try
            {
                ContentUpdateRequestDetail curd = GetContentUpdateRequestDetailById(id);
                _Context.ContentUpdateRequestDetails.Remove(curd);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove content update request detail with id" + id, ex);
            }
        }

        /// <summary>
        /// Gets all content update request details
        /// </summary>
        /// <param name="requestId">Id of the request</param>
        /// <returns></returns>
        public IEnumerable<ContentUpdateRequestDetail> GetContentUpdateRequestDetails()
        {
            return _Context.ContentUpdateRequestDetails.OrderBy(d => d.ContentCode).ToList();
        }

        #endregion

        #region Property Time Zone

        /// <summary>
        /// Convert date time to date time based on property time zone
        /// </summary>
        /// <param name="utcDateTime"></param>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public DateTime GetPropertyLocalTime(DateTime utcDateTime, int propertyId)
        {
            DateTime propertyLocalTime = utcDateTime;

            var p = _Context.Properties.Where(pr => pr.PropertyID == propertyId).SingleOrDefault();
            if (p != null)
            {
                try
                {
                    TimeZoneInfo propertyTimeZone = TimeZoneInfo.FindSystemTimeZoneById(p.TimeZone);
                    propertyLocalTime = TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, propertyTimeZone);
                }
                catch (TimeZoneNotFoundException ex)
                {
                    throw ex;
                }
                
            }
            return propertyLocalTime;
        }

        #endregion


        public Property GetPropertyBySingOffToken(string id)
        {
            return _Context.Properties.Where(p => p.SignOffCode.Equals(id)).SingleOrDefault();
        }
    }
}
