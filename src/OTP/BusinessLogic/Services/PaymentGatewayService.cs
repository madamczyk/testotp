﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.ServiceContracts;
using System.IO;
using DataAccess.Enums;
using BusinessLogic.PaymentGateway;
using System.Net;
using BusinessLogic.Enums;
using System.Net.Http.Formatting;
using System.Net.Http;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Common.Exceptions;
using System.Net.Http.Headers;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Communication with the SpreedlyCore payment gateway. Basic payment operations.
    /// </summary>
    public class PaymentGatewayService : BaseService, IPaymentGatewayService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentGatewayService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public PaymentGatewayService(IContextService httpContextService, IEventLogService logService,
            ISettingsService settingsService, IMessageService messageService)
            : base(httpContextService, new Dictionary<Type, object>() {                             
                {typeof(IEventLogService), logService },
                {typeof(ISettingsService), settingsService },
                {typeof(IMessageService), messageService },
            })
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentGatewayService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public PaymentGatewayService(OTPEntities context, IEventLogService logService,
            ISettingsService settingsService, IMessageService messageService)
            : base(new Dictionary<Type, object>() {                             
                {typeof(IEventLogService), logService },
                {typeof(ISettingsService), settingsService },
                {typeof(IMessageService), messageService },
            }, context)
        { }


        /// <summary>
        /// Gets payment method data with additional data 
        /// </summary>
        /// <param name="paymentMethodId">SpreedCore payment method id (reference token)</param>
        /// <param name="reservationId">Id of the reservation</param>
        /// <returns></returns>
        public PaymentMethodResponse GetPaymentAdditionalData(string paymentMethodId, int reservationId)
        {
            var addr = @"https://spreedlycore.com/v1/payment_methods/";

            using (HttpClientHandler handler = new HttpClientHandler())
            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(addr);
                SetAuthorizationHeader(client.DefaultRequestHeaders);

                HttpResponseMessage response = client.GetAsync(paymentMethodId + ".xml").Result;
                var stream = response.Content.ReadAsStreamAsync().Result;

                PaymentMethodResponse serializerResponse = null;                
                try
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.AddPaymentMethod, reservationId, stream);
                    XmlSerializer serializer = new XmlSerializer(typeof(PaymentMethodResponse));
                    serializerResponse = (PaymentMethodResponse)serializer.Deserialize(stream);
                    return serializerResponse;
                }
                catch (InvalidOperationException ex) //this means xml response is different than expected we can only log this and throw it again
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.AddPaymentMethod, reservationId, stream, ex.Message);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
                catch (Exception ex)
                {
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
            }
        }

        /// <summary>
        /// Performs block funds on guest credit card wihout taking it.
        /// </summary>
        /// <param name="request">Request containing amount to take</param>
        /// <param name="reservationId">Id of the reservation</param>
        /// <returns>Response from the service</returns>
        public TransactionResponse AuthorizePayment(TransactionRequest request, int reservationId)
        {
            var addr = @"https://spreedlycore.com/v1/gateways/";

            using (HttpClientHandler handler = new HttpClientHandler())
            using (HttpClient client = new HttpClient(handler))
            {
                var gatewayToken = this._settingsService.GetSettingValue(SettingKeyName.PaymentGatewayToken);
                client.BaseAddress = new Uri(addr);
                SetAuthorizationHeader(client.DefaultRequestHeaders);

                MediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
                {
                    UseXmlSerializer = true
                };
                HttpContent content = new ObjectContent<TransactionRequest>(request, xmlFormatter);

                HttpResponseMessage response = client.PostAsync(gatewayToken + "/authorize.xml", content).Result;

                var stream = response.Content.ReadAsStreamAsync().Result;
                TransactionResponse serializerResponse = null;
                try
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Authorize, reservationId, stream);
                    XmlSerializer serializer = new XmlSerializer(typeof(TransactionResponse));
                    serializerResponse = (TransactionResponse)serializer.Deserialize(stream);
                    return serializerResponse;
                }
                catch (InvalidOperationException ex) //this means xml response is different than expected we can only log this and throw it again
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Authorize, reservationId, stream, ex.Message);
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
                catch (Exception ex)
                {
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
            }            
        }

        /// <summary>
        /// Performs authorize and capture on guest credit card in one API call.
        /// </summary>
        /// <param name="request">Request containing amount to take</param>
        /// <param name="reservationId">Id of the reservation</param>
        /// <returns>Response from the service</returns>
        public TransactionResponse Purchase(TransactionRequest request, int reservationId)
        {
            var addr = @"https://spreedlycore.com/v1/gateways/";

            using (HttpClientHandler handler = new HttpClientHandler())
            using (HttpClient client = new HttpClient(handler))
            {
                var gatewayToken = this._settingsService.GetSettingValue(SettingKeyName.PaymentGatewayToken);
                client.BaseAddress = new Uri(addr);
                SetAuthorizationHeader(client.DefaultRequestHeaders);

                MediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
                {
                    UseXmlSerializer = true
                };
                HttpContent content = new ObjectContent<TransactionRequest>(request, xmlFormatter);

                HttpResponseMessage response = client.PostAsync(gatewayToken + "/purchase.xml", content).Result;
                var stream = response.Content.ReadAsStreamAsync().Result;

                TransactionResponse serializerResponse = null;
                try
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Purchase, reservationId, stream);
                    XmlSerializer serializer = new XmlSerializer(typeof(TransactionResponse));
                    serializerResponse = (TransactionResponse)serializer.Deserialize(stream);
                    return serializerResponse;
                }
                catch (InvalidOperationException ex) //this means xml response is different than expected we can only log this and throw it again
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Purchase, reservationId, stream, ex.Message);
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
                catch (Exception ex)
                {
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
            }
        }

        /// <summary>
        /// Invokes retain method on payment method
        /// </summary>
        /// <param name="paymentMethodId">Id of the payment method</param>
        /// <param name="reservationId">Id of the reservation</param>
        /// <returns></returns>
        public TransactionResponse RetainPaymentMethod(string paymentMethodId, int reservationId)
        {
            var addr = @"https://spreedlycore.com/v1/payment_methods/";

            using (HttpClientHandler handler = new HttpClientHandler())
            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(addr);
                SetAuthorizationHeader(client.DefaultRequestHeaders);

                HttpResponseMessage response = client.PutAsync(paymentMethodId + "/retain.xml","", new XmlMediaTypeFormatter()).Result;
                var stream = response.Content.ReadAsStreamAsync().Result;

                TransactionResponse serializerResponse = null;
                try
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Retain, reservationId, stream);
                    XmlSerializer serializer = new XmlSerializer(typeof(TransactionResponse));
                    serializerResponse = (TransactionResponse)serializer.Deserialize(stream);
                    return serializerResponse;
                }
                catch (InvalidOperationException ex) //this means xml response is different than expected we can only log this and throw it again
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Retain, reservationId, stream, ex.Message);
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
                catch (Exception ex)
                {
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
            }
        }

        /// <summary>
        /// Invokes redact method on payment method
        /// </summary>
        /// <param name="paymentMethodId">Id of the payment method</param>
        /// <returns></returns>
        public TransactionResponse RedactPaymentMethod(string paymentMethodId)
        {
            var addr = @"https://spreedlycore.com/v1/payment_methods/";

            using (HttpClientHandler handler = new HttpClientHandler())
            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(addr);
                SetAuthorizationHeader(client.DefaultRequestHeaders);

                HttpResponseMessage response = client.PutAsync(paymentMethodId + "/redact.xml", "", new XmlMediaTypeFormatter()).Result;
                var stream = response.Content.ReadAsStreamAsync().Result;

                TransactionResponse serializerResponse = null;
                try
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Redact, 0, stream);
                    XmlSerializer serializer = new XmlSerializer(typeof(TransactionResponse));
                    serializerResponse = (TransactionResponse)serializer.Deserialize(stream);
                    return serializerResponse;
                }
                catch (InvalidOperationException ex) //this means xml response is different than expected we can only log this and throw it again
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Redact, 0, stream, ex.Message);
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
                catch (Exception ex)
                {
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
            }
        }

        /// <summary>
        /// Performs capture of authorization amount of concrete transaction
        /// </summary>
        /// <param name="authorizationToken">Token generated by authorization process</param>
        /// <param name="reservationId">Id of the reservation</param>
        /// <returns>Response from the service</returns>
        public TransactionResponse CapturePayment(string authorizationToken, int reservationId)
        {
            var addr = @"https://spreedlycore.com/v1/transactions/";

            using (HttpClientHandler handler = new HttpClientHandler())
            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(addr);
                SetAuthorizationHeader(client.DefaultRequestHeaders);

                HttpResponseMessage response = client.PostAsync(authorizationToken + "/capture.xml", "", new XmlMediaTypeFormatter()).Result;
                var stream = response.Content.ReadAsStreamAsync().Result;

                TransactionResponse serializerResponse = null;
                try
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Capture, reservationId, stream);
                    XmlSerializer serializer = new XmlSerializer(typeof(TransactionResponse));
                    serializerResponse = (TransactionResponse)serializer.Deserialize(stream);
                    return serializerResponse;
                }
                catch (InvalidOperationException ex) //this means xml response is different than expected we can only log this and throw it again
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Capture, reservationId, stream, ex.Message);
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
                catch (Exception ex)
                {
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
            }
        }

        /// <summary>
        /// Performs cancel of authorization within 24 hours
        /// </summary>
        /// <param name="authorizationToken">Token generated by authorization process</param>
        /// <param name="reservationId">Id of the reservation</param>
        /// <returns>Response from the service</returns>
        public TransactionResponse CancelPayment(string authorizationToken, int reservationId)
        {
            var addr = @"https://spreedlycore.com/v1/transactions/";

            using (HttpClientHandler handler = new HttpClientHandler())
            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(addr);
                SetAuthorizationHeader(client.DefaultRequestHeaders);

                HttpResponseMessage response = client.PostAsync(authorizationToken + "/void.xml", "", new XmlMediaTypeFormatter()).Result;
                var stream = response.Content.ReadAsStreamAsync().Result;

                TransactionResponse serializerResponse = null;
                try
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Void, reservationId, stream);
                    XmlSerializer serializer = new XmlSerializer(typeof(TransactionResponse));
                    serializerResponse = (TransactionResponse)serializer.Deserialize(stream);
                    return serializerResponse;
                }
                catch (InvalidOperationException ex) //this means xml response is different than expected we can only log this and throw it again
                {
                    LogPaymentGatewayResponse(PaymentGatewayOperationType.Void, reservationId, stream, ex.Message);
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }
                catch (Exception ex)
                {
                    this._eventLogService.LogError(ex.Message, DateTime.Now, EventCategory.Error.ToString(), EventLogSource.PaymentGateway);
                    throw new PaymentGatewayException(ex.Message, ex);
                }            
            }
        }

        #region PrivateMembers

        /// <summary>
        /// Logs response from the payment gateway
        /// </summary>
        /// <param name="operationType">Type of the operation</param>
        /// <param name="reservationId">Id of the reservation</param>
        /// <param name="response">Response stream</param>
        private void LogPaymentGatewayResponse(PaymentGatewayOperationType operationType, int reservationId, Stream response, String message = "")
        {
            response.Position = 0;
            StreamReader reader = new StreamReader(response);

            PaymentGatewayOperationsLog log = new PaymentGatewayOperationsLog()
            {
                OperationType = (int)operationType,
                Reservation = _Context.Reservations.Find(reservationId),
                GatewayResponse = reader.ReadToEnd(),
                Message = message,
                RowDate = DateTime.Now
            };

            _Context.PaymentGatewayOperationsLogs.Add(log);
            response.Position = 0;
        }

        /// <summary>
        /// Set Authorization header - Workaround for setting handler Credentials
        /// </summary>
        /// <param name="headers"></param>
        private void SetAuthorizationHeader(HttpRequestHeaders headers)
        {
            string UserName = this._settingsService.GetSettingValue(SettingKeyName.PaymentApiLogin);
            string Password = this._settingsService.GetSettingValue(SettingKeyName.PaymentApiSecret);
            string authenticationToken = Common.Converters.Base64.Encode(string.Format("{0}:{1}", UserName, Password));
            headers.Add("Authorization", "Basic " + authenticationToken);
        }

        #endregion
    }
}
