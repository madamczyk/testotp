﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Services class for managing dictionary with cultures. Contains basic CRUD operations.
    /// </summary>
    public class DictionaryCultureService : BaseService, IDictionaryCultureService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryCultureService"/> class.
        /// </summary>
        public DictionaryCultureService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryCultureService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public DictionaryCultureService(OTPEntities ctx)
            : base(ctx)
        { }

        /// <summary>
        /// Gets all cultures
        /// </summary>
        /// <returns>List of all cultures</returns>
        public IEnumerable<DictionaryCulture> GetCultures()
        {
            return _Context.DictionaryCultures.OrderBy(p => p.CultureCode).ToList();
        }

        /// <summary>
        /// Gets culture by id
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public DictionaryCulture GetCultureById(int cultureId)
        {
            return _Context.DictionaryCultures.Where(p => p.CultureId == cultureId).FirstOrDefault();
        }

        /// <summary>
        /// Gets culture by code
        /// </summary>
        /// <param name="code">Culture code</param>
        /// <returns></returns>
        public DictionaryCulture GetCultureByCode(string code)
        {
            return _Context.DictionaryCultures.Where(l => l.CultureCode == code).SingleOrDefault();
        }
    }
}
