﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.Enums;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class SettingsService : BaseService, ISettingsService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsService"/> class.
        /// </summary>
        public SettingsService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public SettingsService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Gets all settings
        /// </summary>
        /// <returns>List of all settings</returns>
        public IEnumerable<OTP_Settings> GetSettings()
        {
            return _Context.OTP_Settings.AsEnumerable().OrderBy(stn => stn.SettingCode);
        }

        /// <summary>
        /// Gets setting by code
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetSettingValue(string key)
        {
            string keyName = key.ToString();
            OTP_Settings setting = _Context.OTP_Settings.Where(s => s.SettingCode == keyName).FirstOrDefault();
            return setting != null ? setting.SettingValue : null;
        }

        /// <summary>
        /// Gets all settings value with Code starts with prefix
        /// </summary>
        /// <param name="prefix">Prefix</param>
        /// <returns></returns>
        public IEnumerable<OTP_Settings> GetAllSettingsByPrefix(string prefix)
        {
            return
                _Context.AutoRetryQuery<IEnumerable<OTP_Settings>>(() =>
                _Context.OTP_Settings.Where(s => s.SettingCode.StartsWith(prefix))).AsEnumerable();
        }
    }
}
