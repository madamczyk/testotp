﻿using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Services class for managing event log entries. Contains basic retrieving operations.
    /// </summary>
    public class EventLogService : BaseService, IEventLogService
    {
        #region Fields

        static int NVARCHAR_MAX = 4000;

        #endregion Fields

        #region Ctor

        public EventLogService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventLogService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public EventLogService(OTPEntities ctx)
            : base(ctx)
        { }

        #endregion

        /// <summary>
        /// Logs error message to database
        /// </summary>
        /// <param name="message"></param>
        public void LogError(string message, DateTime date, string category, EventLogSource eventSource)
        {
            EventLog eventLog = new EventLog();
            string errorMessage = message.Length <= NVARCHAR_MAX ? message : message.Substring(0, NVARCHAR_MAX);

            eventLog.Message = errorMessage;
            eventLog.Timestamp = date;
            eventLog.Category = category;
            eventLog.Source = eventSource.ToString();
            eventLog.CorrelationId = Guid.NewGuid();

            _Context.EventLogs.Add(eventLog);
        }

        /// <summary>
        /// Adds new event log to the db
        /// </summary>
        /// <param name="eventLog"></param>
        public void AddEventLog(EventLog eventLog)
        {
            try
            {
                _Context.EventLogs.Add(eventLog);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Event Log", ex);
            }
        }

        /// <summary>
        /// Gets last occured error from DB
        /// </summary>
        /// <returns></returns>
        public EventLog GetLastError()
        {
            return _Context.EventLogs.OrderByDescending(e => e.Timestamp).First();
        }

        /// <summary>
        /// Gets all logs
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EventLog> GetEventLogs()
        {
            var list = _Context.EventLogs.AsEnumerable().OrderByDescending(e => e.Timestamp);
            return list;
        }

        /// <summary>
        /// Gets log with specific id
        /// </summary>
        /// <param name="id">Id of the vent log entry</param>
        /// <returns></returns>
        public EventLog GetEventLogById(int id)
        {
            return _Context.EventLogs.Where(e => e.Id == id).SingleOrDefault();
        }

        /// <summary>
        /// Gets all logs, which occured at specified day
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EventLog> GetEventLogsForDay(DateTime day)
        {
            return
                _Context.AutoRetryQuery<IEnumerable<EventLog>>(() =>
                _Context.EventLogs.Where(el => el.Timestamp.Day == day.Day && el.Timestamp.Month == day.Month && el.Timestamp.Year == day.Year).OrderByDescending(e => e.Timestamp)).AsEnumerable();
        }

        /// <summary>
        /// Deletes specific event log entry
        /// </summary>
        /// <param name="id"></param>
        public void DeleteEventLogById(int id)
        {
            try
            {
                EventLog eventLog = this.GetEventLogById(id);
                _Context.EventLogs.Remove(eventLog);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Event Log with id {0}", id), ex);
            }
        }
    }
}
