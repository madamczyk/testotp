﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class TaxesService : BaseService, ITaxesService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaxesService"/> class.
        /// </summary>
        public TaxesService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="TaxesService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public TaxesService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Get Tax by its internal Id
        /// </summary>
        /// <param name="id">internal id of the Tax</param>
        /// <returns>Tax retrieved</returns>
        public Tax GetTaxById(int id)
        {
            try
            {
                return this._Context.Taxes.Where(p => p.TaxId == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get Tax with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Add Tax
        /// </summary>
        public void AddTax(Tax p)
        {
            try
            {
                _Context.Taxes.Add(p);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Tax", ex);
            }
        }

        /// <summary>
        /// Gets all taxes
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Tax> GetTaxes()
        {
            var list = _Context.Taxes.AsEnumerable().OrderBy(t => t.NameCurrentLanguage);
            return list;
        }

        /// <summary>
        /// Gets all taxes for destination
        /// </summary>
        /// <param name="destinationId">internal id of Destination</param>
        /// <returns></returns>
        public IEnumerable<Tax> GetTaxesForDestination(int destinationId)
        {
            var list = _Context.Taxes.Where(t => t.Destination.DestinationID == destinationId).AsEnumerable().OrderBy(t => t.NameCurrentLanguage);
            return list;
        }

        /// <summary>
        /// Remove tax from DB
        /// </summary>
        /// <param name="id">Tax id</param>
        public void DeleteTax(int id)
        {
            try
            {
                Tax tax = this.GetTaxById(id);
                _Context.Taxes.Remove(tax);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Tax with id {0}", id), ex);
            }
        }
    }
}
