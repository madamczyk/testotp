﻿using BusinessLogic.Managers;
using BusinessLogic.Objects;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;

namespace BusinessLogic.Services
{
    class SMTPClientEmailService : IEmailService
    {
        #region Ctor

        public SMTPClientEmailService()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method sends email using SmtpClient
        /// </summary>
        /// <param name="dbMessage">Message from the database</param>
        /// <param name="account">Account used to send emails</param>
        /// <param name="attachments">Attachments for the email</param>
        public void SendEmail(Message dbMessage, EmailAccount account, IEnumerable<DataAccess.Attachment> attachments = null)
        {
            MailMessage message = ConvertToMailMessage(dbMessage, account, attachments);

            SmtpClient mailClient = new SmtpClient
            {
                Host = account.Server,
                Port = account.Port.Value,
                EnableSsl = account.SSL.Value,

                Credentials = new NetworkCredential
                {
                    UserName = account.UserName,
                    Password = account.Password
                }
            };

            mailClient.Send(message);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Converts mail message in DB format to MailMessage object used by SmtpClient
        /// </summary>
        /// <param name="mailMessage">Mail from DB</param>
        /// <param name="account">Account used to send emails</param>
        /// <param name="attachments">Attachments fot the email</param>
        /// <returns></returns>
        protected MailMessage ConvertToMailMessage(Message mailMessage, EmailAccount account, IEnumerable<DataAccess.Attachment> attachments)
        {
            var eMailMessage = new MailMessage
            {
                IsBodyHtml = true,
                From = new MailAddress(account.UserName, account.DisplayFrom),
                SubjectEncoding = Encoding.UTF8,
                BodyEncoding = Encoding.UTF8,
                Subject = mailMessage.Subject,
                Body = mailMessage.Body
            };

            List<string> recipients = mailMessage.Recipients.Split(',').ToList();
            foreach (var recipient in recipients)
            {
                eMailMessage.To.Add(recipient);
            }

            List<string> replyToList = mailMessage.ReplyTo.Split(',').ToList();
            foreach (var replyTo in replyToList)
            {
                if (String.IsNullOrWhiteSpace(replyTo) == false)
                {
                    eMailMessage.ReplyToList.Add(new MailAddress(replyTo));
                }
            }
            if (mailMessage.CC != null)
            {
                List<string> CCList = mailMessage.CC.Split(',').ToList();
                foreach (var cc in CCList)
                {
                    if (String.IsNullOrWhiteSpace(cc) == false)
                    {
                        eMailMessage.CC.Add(new MailAddress(cc));
                    }
                }
            }
            if (mailMessage.BCC != null)
            {
                List<string> BCCList = mailMessage.BCC.Split(',').ToList();
                foreach (var bcc in BCCList)
                {
                    if (String.IsNullOrWhiteSpace(bcc) == false)
                    {
                        eMailMessage.CC.Add(new MailAddress(bcc));
                    }
                }
            }

            //attachments
            foreach (DataAccess.Attachment att in attachments)
                eMailMessage.Attachments.Add(CreateMailAttachment(att));

            //embed images
            IList<LinkedResource> linkedResources;
            mailMessage.Body = ConvertImagesTags(mailMessage.Body, out linkedResources);
            eMailMessage.Body = mailMessage.Body;
            var alternateView = AlternateView.CreateAlternateViewFromString(mailMessage.Body, Encoding.UTF8, "text/html");
            AddContentImages(alternateView, mailMessage.Body, linkedResources);
            eMailMessage.AlternateViews.Add(alternateView);

            return eMailMessage;
        }

        private string ConvertImagesTags(string body, out IList<LinkedResource> resource)
        {
            //images from external source (Internet)
            string fileUrl;
            string imgPattern = "src=\"ecid:[^\"]*\"";
            MatchCollection matches = Regex.Matches(body, imgPattern);
            resource = new List<LinkedResource>();

            foreach (Match m in matches)
            {
                Image image;
                fileUrl = m.Value.Replace("src=\"ecid:", "").Replace("\"", "");
                {
                    string newFileId = "file_" + Guid.NewGuid().ToString();
                    image = Image.FromStream(GetWebResponse(fileUrl));
                    MemoryStream stream1 = new System.IO.MemoryStream();
                    image.Save(stream1, ImageFormat.Jpeg);
                    stream1.Position = 0;
                    LinkedResource lr1 = new LinkedResource(stream1, "image/jpg");
                    lr1.ContentId = newFileId;
                    lr1.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                    resource.Add(lr1);
                    body = body.Replace("ecid:" + fileUrl, "cid:" + newFileId);
                }
            }
            return body;
        }

        private string AddContentImages(AlternateView altView, string body, IList<LinkedResource> resource)
        {
            string fileId = string.Empty;
            Image img = null;
            MemoryStream stream;

            string imgPattern = "src=\"cid:[^\"]*\"";
            MatchCollection matches = Regex.Matches(body, imgPattern);

            foreach (Match m in matches)
            {
                fileId = m.Value.Replace("src=\"cid:", "").Replace("\"", "");
                img = (Image)Common.Resources.Logos.ResourceManager.GetObject(fileId);
                if (img != null)
                {
                    stream = new System.IO.MemoryStream();
                    img.Save(stream, ImageFormat.Png);
                    stream.Position = 0;
                    LinkedResource lr = new LinkedResource(stream);
                    lr.ContentId = fileId;
                    lr.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                    resource.Add(lr);
                }
            }

            foreach (LinkedResource lr in resource)
            {
                altView.LinkedResources.Add(lr);
            }
            return body;
        }

        public static System.Net.Mail.Attachment CreateMailAttachment(DataAccess.Attachment dbAttachment)
        {
            System.Net.Mail.Attachment attachment = null;
            DocumentType docType = (DocumentType)dbAttachment.Type;

            switch (docType)
            {
                case DocumentType.PDF:
                    attachment = new System.Net.Mail.Attachment(new MemoryStream(dbAttachment.Content), dbAttachment.Title);
                    attachment.ContentType = new System.Net.Mime.ContentType("application/pdf");
                    attachment.Name = dbAttachment.Title + ".pdf";
                    break;

                default:
                    throw new ArgumentException("DocumentType of given Attachment is not supported");
            }

            return attachment;
        }

        private Stream GetWebResponse(string imgUrl)
        {
            // Create a 'WebRequest' object with the specified url. 
            WebRequest webRequest = WebRequest.Create(imgUrl);

            // Send the 'WebRequest' and wait for response.
            WebResponse webResponse = webRequest.GetResponse();

            // Obtain a 'Stream' object associated with the response object.
            Stream receiveStream = webResponse.GetResponseStream();

            return receiveStream;
        }

        #endregion
    }
}
