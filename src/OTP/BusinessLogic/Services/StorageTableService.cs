﻿using BusinessLogic.Enums;
using BusinessLogic.Objects;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class StorageTableService : BaseService, IStorageTableService
    {
        #region Fields

        private readonly string _connectionString;
        private readonly string _tableName;

        private static CloudStorageAccount _account;
        private static CloudTableClient _tableClient;

        #endregion

        public StorageTableService(IContextService httpContextService)
            : base(httpContextService)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageTableService"/> class.
        /// </summary>
        public StorageTableService(IConfigurationService configurationService, IContextService httpContextService)
            : base( httpContextService, new Dictionary<Type, object>() {{ 
                typeof(IConfigurationService), configurationService }})
        {
            _connectionString = this._configurationService.GetKeyValue(CloudConfigurationKeys.StorageConnectionString);
            _tableName = this._configurationService.GetKeyValue(CloudConfigurationKeys.SubscribersEmailTable);

            InitializeTables();
        }

        /// <summary>
        /// Initializes the tables.
        /// </summary>
        private void InitializeTables()
        {
            if (CloudStorageAccount.TryParse(_connectionString, out _account))
            {
                _tableClient = _account.CreateCloudTableClient();
                _tableClient.CreateTableIfNotExist(_tableName);
            }
        }

        /// <summary>
        /// Adds the subscriber.
        /// </summary>
        /// <param name="email">The subsriber's email.</param>
        public void AddSubscriber(string email)
        {
            Subscriber s = new Subscriber();
            s.PartitionKey = "SubscribersPartition";
            s.RowKey = Guid.NewGuid().ToString();
            s.Email = email;
            s.Timestamp = DateTime.Now;

            TableServiceContext ctx = _tableClient.GetDataServiceContext();
            ctx.AddObject(_tableName, s);

            ctx.SaveChanges();
        }
    }
}
