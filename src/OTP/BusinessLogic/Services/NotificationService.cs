﻿using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BusinessLogic.Services
{
    public class NotificationService : BaseService
    {
        #region Ctor

        public NotificationService(Services services)
			: base(services)
		{ }

		#endregion

        /// <summary>
        /// Gets notification template by Type
        /// </summary>
        /// <param name="notificationType">Notification Type</param>
        /// <param name="language">Language of NotificationTemplate</param>
        /// <returns>NotificationTemplate</returns>
        public NotificationTemplate GetByType(NotificationTemplateType notificationType, LanguageCode language)
        {
            var languageCode = language.ToString();
            return _Context.NotificationTemplates.Where(nt => nt.Type == (int)notificationType && nt.DictionaryLanguage.LanguageCode == languageCode).SingleOrDefault();
        }    
    }
}
