﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data;
using BusinessLogic.Enums;
using System.Web;
using BusinessLogic.ServiceContracts;
using System.Web.Security;
using Microsoft.ApplicationServer.Caching;
using System.Web.Caching;
using BusinessLogic.Objects.Session;

namespace BusinessLogic.Services
{
    public class StateService : BaseService, IStateService
    {
        /// <summary>
        /// Defines if Cache should be store in Web Role or in the Http Context
        /// </summary>
        private bool _coLocatedCache
        {
            get
            {
                if (this._configurationService.IsCloudEnvironmentAvailable() &&
                    !this._configurationService.IsCloudEnvironmentEmulated())
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Global cache object
        /// </summary>
        private static DataCache _dataCache;

        /// <summary>
        /// Data cache object
        /// </summary>
        private DataCache DataCache
        {
            get
            {
                if (_dataCache == null)
                {
                    InitCache();
                }
                return _dataCache;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DictionariesService"/> class.
        /// </summary>
        public StateService(IContextService httpContextService)
            : base(httpContextService)
        {

        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="DictionariesService"/> class.
        /// </summary>
        public StateService(IContextService httpContextService, IConfigurationService configurationService)
            : base(httpContextService, new Dictionary<Type, object>() {{ 
                typeof(IConfigurationService), configurationService }})
        {
            
        }

        public SessionUser CurrentUser
        {
            get
            {
                return (SessionUser)System.Web.HttpContext.Current.Session["CurrentUser"];
            }
            set
            {
                HttpContext.Current.Session["CurrentUser"] = value;
            }
        }

        /// <summary>
        /// Adds object to session 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public  void AddToSession(string key, object value)
        {
            HttpContext.Current.Session.Add(key, value);
        }

        /// <summary>
        /// Gets object with key from session
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetFromSession<T>(string key)
        {
            return (T)HttpContext.Current.Session[key];
        }

        /// <summary>
        /// Remove object from session
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public void RemoveFromSession(string key)
        {
            HttpContext.Current.Session.Remove(key);
        }
        
        /// <summary>
        /// Checks if object persists in session
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool IsNullInSession(string key)
        {
            return (HttpContext.Current.Session[key] == null);
        }

        public bool IsAuthenticated()
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                if (CurrentUser == null || CurrentUser.LoggedInRole == null)
                {
                    FormsAuthentication.SignOut();
                    return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Initialize cache object
        /// </summary>
        public void InitCache()
        {
            if (_coLocatedCache)
            {
                try
                {
                    #region Client cache - diagnostic level
                    int diagnosticLevel;
                    System.Diagnostics.TraceLevel traceLevel = System.Diagnostics.TraceLevel.Error;
                    string cacheDiagnosticLevel = this._configurationService.GetKeyValue(CloudConfigurationKeys.CacheClientDiagnosticLevel);
                    if (int.TryParse(cacheDiagnosticLevel, out diagnosticLevel))
                    {
                        traceLevel = (System.Diagnostics.TraceLevel)diagnosticLevel;
                    }
                    #endregion

                    DataCacheFactoryConfiguration dataCacheConfiguration = new DataCacheFactoryConfiguration();
                    DataCacheFactory cacheFactory = new DataCacheFactory(dataCacheConfiguration);                    
                    DataCacheClientLogManager.ChangeLogLevel(traceLevel);
                    DataCacheClientLogManager.SetSink(DataCacheTraceSink.DiagnosticSink, traceLevel);
                    _dataCache = cacheFactory.GetDefaultCache();
                }
                catch (Exception ex)
                {
                    throw new Exception("Can't init cache", ex);
                }
            }
        }


        /// <summary>
        /// Adds object to global cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="secondsCount"></param>
        public void AddToCache(string key, object value, int secondsCount = 5 * 60)
        {
            try
            {
                TimeSpan duration = TimeSpan.FromSeconds(secondsCount);
                if (_coLocatedCache)
                {
                    DataCache.Put(key, value, duration);
                }
                else
                {
                    HttpContext.Current.Cache.Add(key, value, null, DateTime.Now.AddSeconds(secondsCount), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot add object to cache", ex); 
            }
        }

        /// <summary>
        /// Checks if object persist in global cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool IsNullInCache(string key)
        {
            if (_coLocatedCache)
            {
                return (DataCache[key] == null);
            }
            else
            {
                return (HttpContext.Current.Cache[key] == null);
            }
        }

        /// <summary>
        /// Gets object from global cache
        /// </summary>
        /// <typeparam name="T">Type of the object</typeparam>
        /// <param name="key">Key name of object</param>
        /// <returns>Value from cache</returns>
        public T GetFromCache<T>(string key)
        {
            if (_coLocatedCache)
            {
                return (T)DataCache[key];
            }
            return ((T)HttpContext.Current.Cache[key]);
        }

        /// <summary>
        /// Removes object from cache
        /// </summary>
        /// <param name="key"></param>
        public void RemoveFromCache(string key)
        {
            if (_coLocatedCache)
            {
                DataCache.Remove(key);
            }
            HttpContext.Current.Cache.Remove(key);
        }
    }
}
