﻿using BusinessLogic.Enums;
using BusinessLogic.ServiceContracts;
using DataAccess;
using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Generic;
using System.Web;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Base class for all services that contains dependant services which are injected by the IoC
    /// </summary>
    public class BaseService
    {
        #region Fields


        private OTPEntities _ctx;
        /// <summary>
        /// Dictionary for services
        /// </summary>
        private readonly Dictionary<Type, object> services;

        #endregion

        #region Services

        public IAuthorizationService _authorizationService
        {
            get
            {
                return (IAuthorizationService)services[typeof(IAuthorizationService)];
            }
        }

        public IUserService _userService
        {
            get
            {
                return (IUserService)services[typeof(IUserService)];
            }
        }

        public IRolesService _rolesService
        {
            get
            {
                return (IRolesService)services[typeof(IRolesService)];
            }
        }

        public IAmenityGroupsService _amenityGroupsService
        {
            get
            {
                return (IAmenityGroupsService)services[typeof(IAmenityGroupsService)];
            }
        }

        public IAmenityService _amenityService
        {
            get
            {
                return (IAmenityService)services[typeof(IAmenityService)];
            }
        }

        public IAppliancesService _appliancesService
        {
            get
            {
                return (IAppliancesService)services[typeof(IAppliancesService)];
            }
        }

        public IBlobService _blobService
        {
            get
            {
                return (IBlobService)services[typeof(IBlobService)];
            }
        }

        public IConfigurationService _configurationService
        {
            get
            {
                return (IConfigurationService)services[typeof(IConfigurationService)];
            }
        }

        public IDestinationsService _destinationsService
        {
            get
            {
                return (IDestinationsService)services[typeof(IDestinationsService)];
            }
        }

        public IDictionaryCountryService _dictionaryCountryService
        {
            get
            {
                return (IDictionaryCountryService)services[typeof(IDictionaryCountryService)];
            }
        }
        
        public IEmailService _emailService
        {
            get
            {
                return (IEmailService)services[typeof(IEmailService)];
            }
        }

        public IEventLogService _eventLogService
        {
            get
            {
                return (IEventLogService)services[typeof(IEventLogService)];
            }
        }

        public IMessageService _messageService
        {
            get
            {
                return (IMessageService)services[typeof(IMessageService)];
            }
        }

        public IPropertiesService _propertiesService
        {
            get
            {
                return (IPropertiesService)services[typeof(IPropertiesService)];
            }
        }

        public IPropertyAddOnsService _propertyAddOnsService
        {
            get
            {
                return (IPropertyAddOnsService)services[typeof(IPropertyAddOnsService)];
            }
        }

        public IPropertyTypesService _propertyTypesService
        {
            get
            {
                return (IPropertyTypesService)services[typeof(IPropertyTypesService)];
            }
        }

        public ISalesPersonsService _salesPersonsService
        {
            get
            {
                return (ISalesPersonsService)services[typeof(ISalesPersonsService)];
            }
        }

        public ISettingsService _settingsService
        {
            get
            {
                return (ISettingsService)services[typeof(ISettingsService)];
            }
        }

        public IStateService _stateService
        {
            get
            {
                return (IStateService)services[typeof(IStateService)];
            }
        }

        public IStorageQueueService _storageQueueService
        {
            get
            {
                return (IStorageQueueService)services[typeof(IStorageQueueService)];
            }
        }

        public IStorageTableService _storageTableService
        {
            get
            {
                return (IStorageTableService)services[typeof(IStorageTableService)];
            }
        }

        public ITagsService _tagsService
        {
            get
            {
                return (ITagsService)services[typeof(ITagsService)];
            }
        }

        public ITaxesService _taxesService
        {
            get
            {
                return (ITaxesService)services[typeof(ITaxesService)];
            }
        }

        public IUnitsService _unitsService
        {
            get
            {
                return (IUnitsService)services[typeof(IUnitsService)];
            }
        }

        public IUnitTypesService _unitTypesService
        {
            get
            {
                return (IUnitTypesService)services[typeof(IUnitTypesService)];
            }
        }

        public IScheduledVisitsService _scheduledVisitsService
        {
            get
            {
                return (IScheduledVisitsService)services[typeof(IScheduledVisitsService)];
            }
        }

        public IBookingService _bookingService
        {
            get
            {
                return (IBookingService)services[typeof(IBookingService)];
            }
        }

        public IReservationsService _reservationsService
        {
            get
            {
                return (IReservationsService)services[typeof(IReservationsService)];
            }
        }

        public IDocumentService _documentService
        {
            get
            {
                return (IDocumentService)services[typeof(IDocumentService)];
            }
        }

        public IPaymentGatewayService _paymentGatewayService
        {
            get
            {
                return (IPaymentGatewayService)services[typeof(IPaymentGatewayService)];
            }
        }

        public IScheduledTasksService _scheduledTaskService
        {
            get
            {
                return (IScheduledTasksService)services[typeof(IScheduledTasksService)];
            }
        }

        public ITaskSchedulerEventLogService _taskSchedulerEventLogService
        {
            get
            {
                return (ITaskSchedulerEventLogService)services[typeof(ITaskSchedulerEventLogService)];
            }
        }

        public IPaymentGatewayOperationLogService _paymentGatewayOperationLogService
        {
            get
            {
                return (IPaymentGatewayOperationLogService)services[typeof(IPaymentGatewayOperationLogService)];
            }
        }

        public IZohoCrmService _zohoCrmService
        {
            get
            {
                return (IZohoCrmService)services[typeof(IZohoCrmService)];
            }
        }

        public ICrmOperationLogsService _crmOperationLogService
        {
            get
            {
                return (ICrmOperationLogsService)services[typeof(ICrmOperationLogsService)];
            }
        }

        public IContextService _httpContextService
        {
            get;
            set;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the data context.
        /// </summary>
        /// <value>The data context.</value>
        protected OTPEntities _Context
        {
            get
            {
                if (_httpContextService != null)
                {
                    return (OTPEntities)_httpContextService.GetFromContext("EFContext");
                }
                return _ctx;
            }
        }

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseService"/> class.
        /// </summary>
        public BaseService(IContextService httpContextService)
        {
            this._httpContextService = httpContextService;
            _ctx = CreateContext();
            this.services = new Dictionary<Type, object>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseService"/> class.
        /// </summary>
        public BaseService(IContextService httpContextService, Dictionary<Type, object> services)
        {
            this._httpContextService = httpContextService;
            _ctx = CreateContext();
            this.services = services;            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseService"/> class.
        /// </summary>
        public BaseService(Dictionary<Type, object> services, OTPEntities ctx)
        {
            _ctx = ctx;
            this.services = services;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseService"/> class.
        /// </summary>
        /// <param name="services">The services.</param>
        protected BaseService(OTPEntities ctx)
        {
            _ctx = ctx;
        }

        #endregion        

        #region Helpers

        public void DisposeContext()
        {
            if (_Context != null)
            {
                _ctx.Dispose();
                _httpContextService.RemoveFromContext("EFContext");
                CreateContext();
            }
        }

        private OTPEntities CreateContext()
        {
            if (_httpContextService.Current != null)
            {
                if (_httpContextService.GetFromContext("EFContext") as OTPEntities == null)
                {
                    _httpContextService.AddToContext("EFContext", new OTPEntities(RoleEnvironment.GetConfigurationSettingValue(CloudConfigurationKeys.DBConnectionString.ToString())));
                }

                _ctx = (OTPEntities)_httpContextService.GetFromContext("EFContext");
                return (OTPEntities)_httpContextService.GetFromContext("EFContext");
            }
            return null;
        }

        #endregion
    }
}
