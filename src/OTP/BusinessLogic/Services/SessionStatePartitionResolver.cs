﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace BusinessLogic.Services
{
    public class SessionStatePartitionResolver : IPartitionResolver
    {
        /// <summary>
        /// Initializes the custom partition resolver.
        /// </summary>
        public void Initialize()
        {
        }

        /// <summary>
        /// Resolves the partition based on a key parameter.
        /// </summary>
        /// <param name="key">An identifier used to determine which partition to use for the current session state.</param>
        /// <returns>
        /// A string with connection information.
        /// </returns>
        public string ResolvePartition(object key)
        {
            return new ConfigurationService((OTPEntities)null).GetKeyValue(Enums.CloudConfigurationKeys.SessionDBConnectionString).ToString();
        }
    }
}
