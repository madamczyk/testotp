﻿using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic.ServiceContracts;
using System.Data.Entity;
using BusinessLogic.Objects;
using BusinessLogic.Helpers;
using Common.Exceptions;
using System.Data.Objects;

namespace BusinessLogic.Services
{

    public class UnitsService : BaseService, IUnitsService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnitsService"/> class.
        /// </summary>
        public UnitsService(IContextService httpContextService, IStateService stateService, IUnitTypesService unitTypesService)
            : base(httpContextService, 
                   new Dictionary<Type, object> 
                   {
                        { typeof(IStateService), stateService }, 
                        { typeof(IUnitTypesService), unitTypesService } 
                   })
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitsService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public UnitsService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Gets Unit entity by its Id
        /// </summary>
        /// <param name="unitId">Unit id</param>
        /// <returns>Retrieved Unit entity</returns>
        public Unit GetUnitById(int unitId)
        {
            return _Context.Units.Where(u => u.UnitID.Equals(unitId)).SingleOrDefault();
        }

        /// <summary>
        /// Gets unit type MLOS values depending on recived parameters
        /// </summary>
        /// <param name="unitId">Unit id</param>
        /// <param name="startDate">Date from</param>
        /// <param name="dateEnd">Date until</param>
        /// <returns></returns>
        public IEnumerable<UnitTypeMLO> GetUnitMlos(int unitId, DateTime? startDate, DateTime? dateEnd)
        {
            if (startDate == null || dateEnd == null)
                return new List<UnitTypeMLO>();

            var q =
                    from unit in _Context.Units
                    from unitTypeMlos in unit.UnitType.UnitTypeMLOS
                    where
                        unit.UnitID.Equals(unitId)                        
                        && 
                        (
                            ((unitTypeMlos.DateFrom >= startDate.Value && unitTypeMlos.DateFrom <= dateEnd.Value))
                            ||
                            ((unitTypeMlos.DateUntil >= startDate.Value && unitTypeMlos.DateUntil <= dateEnd.Value))
                            ||
                            ((startDate.Value >= unitTypeMlos.DateFrom) && (dateEnd.Value <= unitTypeMlos.DateUntil))                            
                        )
                    select unitTypeMlos;

            return q.AsEnumerable();

        }

        /// <summary>
        /// Update mloses. Validating if rate periods overlap each other.
        /// </summary>
        /// <param name="unitRatesToUpdate"></param>
        /// <param name="unitRateIdsToDelete"></param>
        public void UpdateMloses(int unitId, IEnumerable<UnitTypeMLO> mlosesToUpdate)
        {
            var unitType = _unitTypesService.GetUnitTypeMlosByUnitId(unitId);
            var mloses = _unitTypesService.GetUnitTypeMlos(unitType.UnitTypeID);
            var unitMlosesToUpdateIds = mlosesToUpdate.Select(ur => ur.UnitTypeMLOSID).ToList();
            var newMloses = mlosesToUpdate.Where(ur => ur.UnitTypeMLOSID == 0).ToList();

            var dateRangesToValidate = new List<DateRange>();
            foreach (var originalMlos in mloses)
            {
                var changedMlos = mlosesToUpdate.Where(ur => ur.UnitTypeMLOSID == originalMlos.UnitTypeMLOSID).FirstOrDefault();
                dateRangesToValidate.Add(
                    new DateRange()
                    {
                        DateFrom = changedMlos != null ? changedMlos.DateFrom : originalMlos.DateFrom,
                        DateUntil = changedMlos != null ? changedMlos.DateUntil : originalMlos.DateUntil
                    });
            }

            foreach (var newMlos in newMloses)
            {
                dateRangesToValidate.Add(
                    new DateRange()
                    {
                        DateFrom = newMlos.DateFrom,
                        DateUntil = newMlos.DateUntil
                    });
            }

            if (DateRangeHelper.HaveDateRangesOverlap(dateRangesToValidate.ToArray()))
                throw new DateRangeOverlapException();

            foreach (var mlos in newMloses)
            {
                _Context.UnitTypeMLOS.Add(
                    new UnitTypeMLO()
                    {
                        DateFrom = mlos.DateFrom,
                        DateUntil = mlos.DateUntil,
                        MLOS = mlos.MLOS,
                        
                    });
            }

            foreach (var originalMlos in mloses)
            {
                var changedMlos = mlosesToUpdate.Where(ur => ur.UnitTypeMLOSID == originalMlos.UnitTypeMLOSID).FirstOrDefault();
                if (changedMlos != null)
                {
                    originalMlos.DateFrom = changedMlos.DateFrom;
                    originalMlos.DateUntil = changedMlos.DateUntil;
                    originalMlos.MLOS = changedMlos.MLOS;
                }
            }

            _Context.SaveChanges();
        }

        /// <summary>
        /// Delete mloses by ids.
        /// </summary>
        /// <param name="mlosIds">Collection of mlos ids</param>
        public void DeleteMloses(IEnumerable<int> mlosIds)
        {
            var mloses = _Context.UnitTypeMLOS.Where(m => mlosIds.Contains(m.UnitTypeMLOSID)).ToList();
            foreach (var mlos in mloses)
            {
                _Context.UnitTypeMLOS.Remove(mlos);
            }

            _Context.SaveChanges();
        }

        /// <summary>
        /// Gets Units by property Id
        /// </summary>
        /// <param name="id">internal id of the property</param>
        /// <returns>Units retrieved</returns>
        public IEnumerable<Unit> GetUnitsByPropertyId(int pid)
        {
            try
            {
                return this._Context.Units.Where(p => p.Property.PropertyID == pid).AsEnumerable();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get Units with property id {0}", pid), ex);
            }
        }

        /// <summary>
        /// Adds unit
        /// </summary>
        /// <param name="unit"></param>
        public void AddUnit(Unit unit)
        {
            try
            {
                _Context.Units.Add(unit);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Unit", ex);
            }
        }

        public IEnumerable<UnitStaticContentResult> GetUnitStaticContentInfo(int unitId)
        {
            var q = (
                from item in _Context.UnitStaticContents
                where
                   item.Unit.UnitID.Equals(unitId)
                group item by item.ContentCode into g
                select new UnitStaticContentResult
                {
                    ContentCode = g.Key,
                    ContentItems = g
                }
                     );
            return q;
        }

        /// <summary>
        /// Get unit connected Applicances
        /// </summary>
        /// <param name="unitId">Unit Id</param>
        /// <returns>List of matched Applicances</returns>
        public IEnumerable<Appliance> GetUnitAppliances(int unitId)
        {
            return _Context.UnitAppliancies.Include(ap => ap.Appliance).Include(ap => ap.Appliance.ApplianceGroup).Where(ua => ua.Unit.UnitID == unitId).Select(ap => ap.Appliance).AsEnumerable();
        }

        /// <summary>
        /// Remove unit by id
        /// </summary>
        /// <param name="unitId">Unit ID</param>
        public void DeleteUnit(int unitId)
        {
            try
            {
                Unit u = GetUnitById(unitId);
                _Context.Units.Remove(u);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Unit with id {0}", unitId), ex);
            }
        }

        /// <summary>
        /// Gets unit static content
        /// </summary>
        /// <param name="unitId">Unit ID</param>
        /// <param name="propertyStaticContentType">Content type</param>
        /// <returns></returns>
        public IEnumerable<UnitStaticContent> GetUnitStaticContentByType(int unitId, UnitStaticContentType propertyStaticContentType)
        {
            return _Context.UnitStaticContents.Where(p => p.Unit.UnitID == unitId && p.ContentCode == (int)propertyStaticContentType).AsEnumerable();
        }

        /// <summary>
        /// Deletes unit static content by id
        /// </summary>
        /// <param name="itemId"></param>
        public void RemoveStaticContent(int itemId)
        {
            try
            {
                UnitStaticContent usc = GetUnitStaticContentById(itemId);
                _Context.UnitStaticContents.Remove(usc);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Unit Static Content with id {0}", itemId), ex);
            }
        }

        /// <summary>
        /// Gets unit static content
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public UnitStaticContent GetUnitStaticContentById(int itemId)
        {
            return _Context.UnitStaticContents.Where(u => u.UnitStaticContentId == itemId).FirstOrDefault();
        }

        /// <summary>
        /// Adds static content to unit
        /// </summary>
        /// <param name="obj">Static content object</param>
        public void AddUnitStaticContent(UnitStaticContent obj)
        {
            try
            {
                _Context.UnitStaticContents.Add(obj);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Unit Static Content", ex);
            }
        }

        /// <summary>
        /// Gets units rates
        /// </summary>
        /// <param name="p">The ID of the Unit</param>
        /// <returns></returns>
        public IEnumerable<UnitRate> GetUnitRates(int unitID)
        {
            return _Context.UnitRates.Where(r => r.Unit.UnitID == unitID).AsEnumerable().OrderByDescending(r => r.DateFrom);
        }

        /// <summary>
        /// Gets unit rates for a specific unit id within date range
        /// </summary>
        /// <param name="unitId">The id of the Unit</param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public IEnumerable<UnitRate> GetUnitRates(int unitId, DateTime dateFrom, DateTime dateTo)
        {
            return _Context.UnitRates
                .Where(r => r.Unit.UnitID == unitId && dateFrom <= r.DateUntil && dateTo >= r.DateFrom)
                .OrderBy(r => r.DateFrom).ToList();
        }

        /// <summary>
        /// Update unit rates. Validating if rate periods overlap each other.
        /// </summary>
        /// <param name="unitRatesToUpdate"></param>
        /// <param name="unitRateIdsToDelete"></param>
        public void UpdateUnitRates(int unitId, UnitRate unitRateToUpdate, int userId)
        {
            var unit = GetUnitById(unitId);

            // Get existing unit rates from DB 
            var originalUnitRates = _Context.UnitRates.Where(ur => ur.Unit.UnitID == unitId
                                                                    && ur.Unit.Property.User.UserID == userId
                                                                    && ((ur.DateFrom >= unitRateToUpdate.DateFrom && ur.DateFrom <= unitRateToUpdate.DateUntil)
                                                                        || (ur.DateUntil >= unitRateToUpdate.DateFrom && ur.DateUntil <= unitRateToUpdate.DateUntil)
                                                                        || (ur.DateFrom < unitRateToUpdate.DateFrom && ur.DateUntil > unitRateToUpdate.DateUntil))).ToList();

            // Add one unit rate after and before user defined period
            // It's for better DB scalability

            var minTimeSpan = _Context.UnitRates.Where(ur => ur.DateUntil < unitRateToUpdate.DateFrom && ur.Unit.Property.User.UserID == userId).Select(ur => EntityFunctions.DiffDays(unitRateToUpdate.DateFrom, ur.DateUntil)).Max();
            if (minTimeSpan.HasValue)
            {
                var unitRateBefore = _Context.UnitRates.FirstOrDefault(ur => EntityFunctions.DiffDays(unitRateToUpdate.DateFrom, ur.DateUntil) == minTimeSpan && ur.DateUntil < unitRateToUpdate.DateFrom && ur.Unit.Property.User.UserID == userId);

                if (unitRateBefore != null)
                {
                    originalUnitRates.Add(unitRateBefore);
                }
            }

            minTimeSpan = _Context.UnitRates.Where(ur => ur.DateFrom > unitRateToUpdate.DateUntil && ur.Unit.Property.User.UserID == userId).Select(ur => EntityFunctions.DiffDays(ur.DateFrom, unitRateToUpdate.DateUntil)).Max();
            if (minTimeSpan.HasValue)
            {
                var unitRateAfter = _Context.UnitRates.FirstOrDefault(ur => EntityFunctions.DiffDays(ur.DateFrom, unitRateToUpdate.DateUntil) == minTimeSpan && ur.DateFrom > unitRateToUpdate.DateUntil && ur.Unit.Property.User.UserID == userId);

                if (unitRateAfter != null)
                {
                    originalUnitRates.Add(unitRateAfter);
                }
            }

            // Sort unit rates on DateFrom
            originalUnitRates.Sort((unitRate1, unitRate2) =>
                {
                    return unitRate1.DateFrom.CompareTo(unitRate2.DateFrom);
                });

            // List containing unit rates periods defined by user and divided by selected week's days
            var calculatedUnitRatesToUpdate = new List<UnitRate>();

            // if whole week's days are selected 
            if (unitRateToUpdate.Monday & unitRateToUpdate.Tuesday & unitRateToUpdate.Wednesday & unitRateToUpdate.Thursday
                & unitRateToUpdate.Friday & unitRateToUpdate.Saturday & unitRateToUpdate.Sunday)
            {
                calculatedUnitRatesToUpdate.Add(unitRateToUpdate);
            }
            else  // else recalculte period avoiding week's days
            {
                DateTime dateStart = DateTime.MinValue;
                DateTime dateEnd = DateTime.MinValue;
                for (DateTime date = unitRateToUpdate.DateFrom; date <= unitRateToUpdate.DateUntil; date += TimeSpan.FromDays(1))
                {
                    bool isSelectedDay = (date.DayOfWeek == DayOfWeek.Sunday && unitRateToUpdate.Sunday
                        || date.DayOfWeek == DayOfWeek.Monday && unitRateToUpdate.Monday
                        || date.DayOfWeek == DayOfWeek.Tuesday && unitRateToUpdate.Tuesday
                        || date.DayOfWeek == DayOfWeek.Wednesday && unitRateToUpdate.Wednesday
                        || date.DayOfWeek == DayOfWeek.Thursday && unitRateToUpdate.Thursday
                        || date.DayOfWeek == DayOfWeek.Friday && unitRateToUpdate.Friday
                        || date.DayOfWeek == DayOfWeek.Saturday && unitRateToUpdate.Saturday);

                    if (dateStart == DateTime.MinValue && isSelectedDay)    // begin period
                    {
                        dateStart = date;
                        dateEnd = date;
                    }
                    else if (isSelectedDay)         // continue period
                    {
                        dateEnd = date;
                    }
                    else if(dateStart != DateTime.MinValue)  // end period
                    {
                        calculatedUnitRatesToUpdate.Add(new UnitRate
                                                        {
                                                            DailyRate = unitRateToUpdate.DailyRate,
                                                            DateFrom = dateStart,
                                                            DateUntil = dateEnd,
                                                            UnitRateID = unitRateToUpdate.UnitRateID
                                                        });

                        dateStart = DateTime.MinValue;
                        dateEnd = DateTime.MinValue;
                    }
                }

                // Add rest of period after loop
                if (dateStart != DateTime.MinValue)
                {
                    calculatedUnitRatesToUpdate.Add(new UnitRate
                                                    {
                                                        DailyRate = unitRateToUpdate.DailyRate,
                                                        DateFrom = dateStart,
                                                        DateUntil = dateEnd,
                                                        UnitRateID = unitRateToUpdate.UnitRateID
                                                    });
                }
            }

            // return if nothing defined by user
            if (!calculatedUnitRatesToUpdate.Any())
            {
                return;
            }

            DateTime beginDate;
            DateTime endDate;

            // calculate begin and end date of all periods (defined by user and stored in DB)
            if (originalUnitRates.Any())
            {
                beginDate = originalUnitRates.First().DateFrom < calculatedUnitRatesToUpdate.First().DateFrom ? originalUnitRates.First().DateFrom : calculatedUnitRatesToUpdate.First().DateFrom;
                endDate = originalUnitRates.Last().DateUntil > calculatedUnitRatesToUpdate.Last().DateUntil ? originalUnitRates.Last().DateUntil : calculatedUnitRatesToUpdate.Last().DateUntil;
            }
            else
            {
                beginDate = calculatedUnitRatesToUpdate.First().DateFrom;
                endDate = calculatedUnitRatesToUpdate.Last().DateUntil;
            }

            /*
                Merge all dates to one timeline as Dictionary containing dates and Unit Rate.
                Only one possilibity is taking for every day: 
                    - if for appropriate date user defined new rate for period, then this date and unit rate are beeing taken
                    - else if for appropriate date, already exists date in DB, then it is beeing taken
                    - else continue - date is beeing omitted.
                
                In this loop exisiting unit rates are devided:
                    - first part contains existing UnitRateId for future update 
                    - in second part UnitRateId is beeing set to 0
             */

            Dictionary<DateTime, UnitRate> mergedUnitRates = new Dictionary<DateTime, UnitRate>();
            int unitRateId = 0;
            bool isUpdate = false;

            for (DateTime date = beginDate; date <= endDate; date += TimeSpan.FromDays(1))
            {
                UnitRate calculatedRate = calculatedUnitRatesToUpdate.SingleOrDefault(ur => ur.DateFrom <= date && ur.DateUntil >= date);

                if (calculatedRate != null)
                {
                    mergedUnitRates.Add(date, calculatedRate);
                    isUpdate = false;
                }
                else
                {
                    UnitRate originalUnitRate = originalUnitRates.SingleOrDefault(ur => ur.DateFrom <= date && ur.DateUntil >= date);

                    if (originalUnitRate != null)
                    {
                        int tmpUnitRateId = originalUnitRate.UnitRateID;

                        if ((unitRateId == 0 && !isUpdate) || (unitRateId != tmpUnitRateId))
                        {
                            unitRateId = tmpUnitRateId;
                            isUpdate = true;
                        }
                        else if (!isUpdate)
                        {
                            tmpUnitRateId = 0;
                        }

                        mergedUnitRates.Add(date, new UnitRate 
                                                    { 
                                                        DailyRate = originalUnitRate.DailyRate,
                                                        DateFrom = originalUnitRate.DateFrom,
                                                        DateUntil = originalUnitRate.DateUntil,
                                                        UnitRateID = tmpUnitRateId
                                                    });
                    }
                    else
                    {
                        unitRateId = 0;
                        isUpdate = false;
                    }
                }
            }

            List<UnitRate> insertUnitRates = new List<UnitRate>();
            List<UnitRate> updateUnitRates = new List<UnitRate>();
            List<UnitRate> deleteUnitRates = new List<UnitRate>();

            UnitRate lastOne = null;
            TimeSpan oneDayTimeSpan = TimeSpan.FromDays(1);

            // Take from merge dictionary periods for update and insert
            // Periods must be continous !
            foreach (var mergedUnitRate in mergedUnitRates)
            {
                if (lastOne != null //&& lastOne.UnitRateID == mergedUnitRate.Value.UnitRateID
                    && lastOne.DailyRate == mergedUnitRate.Value.DailyRate
                    && mergedUnitRate.Key - lastOne.DateUntil == oneDayTimeSpan)
                {
                    lastOne.DateUntil = mergedUnitRate.Key;
                }
                else
                {
                    if (lastOne != null)
                    {
                        if (lastOne.UnitRateID != 0)
                        {
                            updateUnitRates.Add(lastOne);
                            lastOne = null;
                        }
                        else
                        {
                            insertUnitRates.Add(lastOne);
                            lastOne = null;
                        }
                    }

                    lastOne = new UnitRate
                    {
                        DailyRate = mergedUnitRate.Value.DailyRate,
                        DateFrom = mergedUnitRate.Key,
                        DateUntil = mergedUnitRate.Key,
                        UnitRateID = mergedUnitRate.Value.UnitRateID
                    };
                }
            }

            // After loop check 
            if (lastOne.UnitRateID != 0)
            {
                updateUnitRates.Add(lastOne);
            }
            else
            {
                insertUnitRates.Add(lastOne);
            }

            // For existing (DB) unit rates select these, which don't appear in update list
            // These are for delete
            foreach (var originalUnitRate in originalUnitRates)
            {
                if (!updateUnitRates.Any(ur => ur.UnitRateID == originalUnitRate.UnitRateID))
                {
                    deleteUnitRates.Add(originalUnitRate);
                }
            }

            // Finally DB context opertions 
            foreach (var deleteUnitRate in deleteUnitRates)
            {
                UnitRate tmpDeleteUnitRate = GetUnitRateById(deleteUnitRate.UnitRateID);
                _Context.UnitRates.Remove(tmpDeleteUnitRate);
            }

            foreach (var updateUnitRate in updateUnitRates)
            {
                UnitRate tmpUpdateUnitRate = GetUnitRateById(updateUnitRate.UnitRateID);
                
                tmpUpdateUnitRate.DateFrom = updateUnitRate.DateFrom;
                tmpUpdateUnitRate.DateUntil = updateUnitRate.DateUntil;
            }

            foreach (var insertUnitRate in insertUnitRates)
            {
                insertUnitRate.Unit = unit;
                _Context.UnitRates.Add(insertUnitRate);
            }

            _Context.SaveChanges();

        }

        /// <summary>
        /// Delete unit rates by ids.
        /// </summary>
        /// <param name="unitRateIds">Collection of unit rate ids</param>
        public void DeleteUnitRates(IEnumerable<int> unitRateIds)
        {
            var user = _stateService.CurrentUser;
            var unitRates = _Context.UnitRates.Where(ur => unitRateIds.Contains(ur.UnitRateID) && ur.Unit.Property.User.UserID == user.UserID).ToList();

            foreach (var unitRate in unitRates)
            {
                _Context.UnitRates.Remove(unitRate);
            }

            _Context.SaveChanges();
        }


        /// <summary>
        /// Gets calculated Unit Price for the unit based on the date of stay
        /// </summary>
        /// <param name="unitId">Id of the unit</param>
        /// <param name="dateFrom">Starting date of stay</param>
        /// <param name="dateTo">Ending date of stay</param>
        /// <returns></returns>
        public decimal? GetUnitPrice(int unitId, DateTime? dateFrom, DateTime? dateTo)
        {
            return _Context.Units.Where(s => s.UnitID.Equals(unitId)).Select(s => OTPEntities.GetUnitPrice(unitId, dateFrom, dateTo)).First();
        }

        /// <summary>
        /// Gets calculated Unit Price for the unit based on the date of stay only for Accomodation
        /// </summary>
        /// <param name="unitId">Id of the unit</param>
        /// <param name="dateFrom">Starting date of stay</param>
        /// <param name="dateTo">Ending date of stay</param>
        /// <returns></returns>
        public decimal? GetUnitPriceAccomodation(int unitId, DateTime? dateFrom, DateTime? dateTo)
        {
            return _Context.Units.Where(s => s.UnitID.Equals(unitId)).Select(s => OTPEntities.GetUnitPriceAccomodation(unitId, dateFrom, dateTo)).First();
        }

        public UnitRate GetUnitRateById(int unitRateId)
        {
            return _Context.UnitRates.Where(ur => ur.UnitRateID == unitRateId).FirstOrDefault();
        }

        /// <summary>
        /// Deletes rate by id
        /// </summary>
        /// <param name="unitRateId"></param>
        /// <returns></returns>
        public void DeleteUnitRate(int unitRateId)
        {
            try
            {
                UnitRate ur = GetUnitRateById(unitRateId);
                _Context.UnitRates.Remove(ur);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Unit Rate with id {0}", unitRateId), ex);
            }
        }

        /// <summary>
        /// Adds new unit rate
        /// </summary>
        /// <param name="newUnit"></param>
        public void AddUnitRate(UnitRate rate)
        {
            try
            {
                _Context.UnitRates.Add(rate);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Unit Rate", ex);
            }
        }

        /// <summary>
        /// Gets units appliances
        /// </summary>
        /// <param name="unitId">Unit ID</param>
        /// <returns></returns
        public IEnumerable<UnitAppliancy> GetUnitApplianciesByUnitId(int unitId)
        {
            return _Context.UnitAppliancies.Where(u => u.Unit.UnitID == unitId).AsEnumerable().OrderBy(p => p.UnitApplianceId);
        }

        /// <summary>
        /// Adds unti appliance 
        /// </summary>
        /// <param name="appliance"></param>
        public void AddUnitAppliance(UnitAppliancy appliance)
        {
            try
            {
                _Context.UnitAppliancies.Add(appliance);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Unit Appliance", ex);
            }
        }

        /// <summary>
        /// Gets unit appliance by unit id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UnitAppliancy GetUnitApplianceById(int id)
        {
            return _Context.UnitAppliancies.Where(ua => ua.UnitApplianceId == id).FirstOrDefault();
        }

        /// <summary>
        /// Deletes appliance by its id
        /// </summary>
        /// <param name="appId"></param>
        public void DeleteUnitAppliance(int appId)
        {
            try
            {
                UnitAppliancy ur = GetUnitApplianceById(appId);
                _Context.UnitAppliancies.Remove(ur);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Unit Appliance with id {0}", appId), ex);
            }
        }

        /// <summary>
        /// Gets unit blocking
        /// </summary>
        /// <param name="unitId">Unit ID</param>
        /// <returns></returns>
        public IEnumerable<UnitInvBlocking> GetUnitBlockings(int unitId)
        {
            return _Context.UnitInvBlockings.Where(b => b.Unit.UnitID == unitId).AsEnumerable();
        }

        /// <summary>
        /// Gets unit blockings within specific date range
        /// </summary>
        /// <param name="unitId">The unit id</param>
        /// <param name="dateFrom">The date from</param>
        /// <param name="dateUntil">The date until</param>
        /// <returns></returns>
        public IEnumerable<UnitInvBlocking> GetUnitBlockings(int unitId, DateTime dateFrom, DateTime dateUntil)
        {
            return _Context.UnitInvBlockings
                    .Where(b => b.Unit.UnitID == unitId && dateFrom <= b.DateUntil && dateUntil >= b.DateFrom)
                    .OrderBy(b => b.DateFrom).ToList();
        }

        /// <summary>
        /// Gets unit blockings within specific date range
        /// Additonal option available which lets u determine wether exclude reservation unit blockades
        /// </summary>
        /// <param name="unitId">The unit id</param>
        /// <param name="dateFrom">The date from</param>
        /// <param name="dateUntil">The date until</param>
        /// <param name="excludeReservation">Flag which determinse wether exclude reservation unit blockades</param>
        /// <returns></returns>
        public IEnumerable<UnitInvBlocking> GetUnitBlockings(int unitId, DateTime dateFrom, DateTime dateUntil, bool excludeReservation)
        {
            var query = _Context.UnitInvBlockings.Where(b => b.Unit.UnitID == unitId && dateFrom <= b.DateUntil && dateUntil >= b.DateFrom);
            if(excludeReservation)
                query.Where(b => b.Type != (int)UnitBlockingType.Reservation);

            return query.OrderBy(b => b.DateFrom).ToList();
        }

        /// <summary>
        /// Gets cta's within specific date range
        /// </summary>
        /// <param name="unitId">The unit id</param>
        /// <param name="dateFrom">The date from</param>
        /// <param name="dateUntil">The date until</param>
        /// <returns></returns>
        public IEnumerable<UnitTypeCTA> GetCTAs(int unitId, DateTime dateFrom, DateTime dateUntil)
        {
            var unitType = _unitTypesService.GetUnitTypeMlosByUnitId(unitId);
            return _Context.UnitTypeCTAs
                    .Where(c => c.UnitType.UnitTypeID == unitType.UnitTypeID && dateFrom <= c.DateUntil && dateUntil >= c.DateFrom)
                    .OrderBy(c => c.DateFrom).ToList();
        }

        /// <summary>
        /// Update ctas. Validating if ctas periods overlap each other.
        /// </summary>
        /// <param name="ctasToUpdate"></param>
        public void UpdateCtas(int unitId, IEnumerable<UnitTypeCTA> ctasToUpdate)
        {
            var unitType = _unitTypesService.GetUnitTypeMlosByUnitId(unitId);
            var originalCtas = _unitTypesService.GetUnitTypeCta(unitType.UnitTypeID);
            var ctasToUpdateIds = ctasToUpdate.Select(ur => ur.UnitTypeCTAID).ToList();
            var newCtas = ctasToUpdate.Where(ur => ur.UnitTypeCTAID == 0).ToList();

            var dateRangesToValidate = new List<DateRange>();
            foreach (var originalMlos in originalCtas)
            {
                var changedCtas = ctasToUpdate.Where(ur => ur.UnitTypeCTAID == originalMlos.UnitTypeCTAID).FirstOrDefault();
                dateRangesToValidate.Add(
                    new DateRange()
                    {
                        DateFrom = changedCtas != null ? changedCtas.DateFrom : originalMlos.DateFrom,
                        DateUntil = changedCtas != null ? changedCtas.DateUntil : originalMlos.DateUntil
                    });
            }

            foreach (var newCta in newCtas)
            {
                dateRangesToValidate.Add(
                    new DateRange()
                    {
                        DateFrom = newCta.DateFrom,
                        DateUntil = newCta.DateUntil
                    });
            }

            if (DateRangeHelper.HaveDateRangesOverlap(dateRangesToValidate.ToArray()))
                throw new DateRangeOverlapException();

            foreach (var newCta in newCtas)
            {
                _Context.UnitTypeCTAs.Add(
                    new UnitTypeCTA()
                    {
                        UnitType = unitType,
                        DateFrom = newCta.DateFrom,
                        DateUntil = newCta.DateUntil,
                        Monday = newCta.Monday,
                        Tuesday = newCta.Tuesday,
                        Wednesday = newCta.Wednesday,
                        Thursday = newCta.Thursday,
                        Friday = newCta.Friday,
                        Saturday = newCta.Saturday,
                        Sunday = newCta.Sunday
                    });
            }

            foreach (var originalCta in originalCtas)
            {
                var changedCta = ctasToUpdate.Where(ur => ur.UnitTypeCTAID == originalCta.UnitTypeCTAID).FirstOrDefault();
                if (changedCta != null)
                {
                    originalCta.DateFrom = changedCta.DateFrom;
                    originalCta.DateUntil = changedCta.DateUntil;
                    originalCta.Monday = changedCta.Monday;
                    originalCta.Tuesday = changedCta.Tuesday;
                    originalCta.Wednesday = changedCta.Wednesday;
                    originalCta.Thursday = changedCta.Thursday;
                    originalCta.Friday = changedCta.Friday;
                    originalCta.Saturday = changedCta.Saturday;
                    originalCta.Sunday = changedCta.Sunday;
                }
            }

            _Context.SaveChanges();
        }

        /// <summary>
        /// Delete ctas by ids.
        /// </summary>
        /// <param name="unitRateIds">Collection of cta ids</param>
        public void DeleteCtas(IEnumerable<int> ctasToIds)
        {
            var user = _stateService.CurrentUser;
            var ctasToDelete = _Context.UnitTypeCTAs.Where(ur => ctasToIds.Contains(ur.UnitTypeCTAID)).ToList();

            foreach (var unitRate in ctasToDelete)
            {
                _Context.UnitTypeCTAs.Remove(unitRate);
            }

            _Context.SaveChanges();
        }

        /// <summary>
        /// Deletes unit blockage by id
        /// </summary>
        /// <param name="unitRateId"></param>
        /// <returns></returns>
        public void DeleteUnitBlockage(int unitBlockageId)
        {
            try
            {
                UnitInvBlocking ur = GetUnitBlockageById(unitBlockageId);
                _Context.UnitInvBlockings.Remove(ur);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Unit Blockage with id {0}", unitBlockageId), ex);
            }
        }

        /// <summary>
        /// Deletes unit blockage by its corresponding reservation id
        /// </summary>
        /// <param name="reservationId"></param>
        public void DeleteUnitBlockageByReservationId(int reservationId)
        {
            UnitInvBlocking ur = GetUnitBlockageByReservationId(reservationId);
            _Context.UnitInvBlockings.Remove(ur);
        }

        /// <summary>
        /// Same as <see cref="DeleteUnitBlockageByReservationId"/> but immediately saves data do database
        /// </summary>
        /// <param name="reservationId">Id of the reservation</param>
        public void PersistDeleteUnitBlockageByReservationId(int reservationId)
        {
            try
            {
                this.DeleteUnitBlockageByReservationId(reservationId);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Unit Blockage with reservation ID {0}", reservationId), ex);
            }
        }

        public UnitInvBlocking GetUnitBlockageByReservationId(int reservationId)
        {
            return _Context.UnitInvBlockings.Where(b => b.Reservation.ReservationID == reservationId).FirstOrDefault();
        }

        public UnitInvBlocking GetUnitBlockageById(int unitBlId)
        {
            return _Context.UnitInvBlockings.Where(b => b.UnitInvBlockingID == unitBlId).FirstOrDefault();
        }

        /// <summary>
        /// Adds blockage for unit
        /// </summary>
        /// <param name="newUnitBlockage">Blocking object</param>
        public void AddUnitBlockade(UnitInvBlocking newUnitBlockage)
        {
            try
            {
                _Context.UnitInvBlockings.Add(newUnitBlockage);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot add unit inv blocking", ex);
            }
        }

        /// <summary>
        /// Adds a new unit blockade and adjust other unit blockades. 
        /// If adding blockade with "available" unit blockade type, it needs to adjust other blockades within unit blockade's date range
        /// </summary>
        /// <param name="newUnitBlockage">Blocking object</param>
        public void AddAndAdjustUnitBlockade(UnitInvBlocking newUnitBlockage)
        {
            try
            {
                if (newUnitBlockage.Type == (int)UnitBlockingType.Available)
                {
                    var unitBlockades = GetUnitBlockings(newUnitBlockage.Unit.UnitID, newUnitBlockage.DateFrom, newUnitBlockage.DateUntil, true);
                    foreach(var unitBlockade in unitBlockades)
                    {
                        //if unit blocking is inside area we want to clear, we need to remove it
                        if ((unitBlockade.DateFrom >= newUnitBlockage.DateFrom) && (unitBlockade.DateUntil <= newUnitBlockage.DateUntil))
                        {
                            _Context.UnitInvBlockings.Remove(unitBlockade);
                        }
                        //if area we want to clear is inside unit blocking, we need to split it into 2 parts.
                        else if ((unitBlockade.DateFrom <= newUnitBlockage.DateFrom) && (unitBlockade.DateUntil >= newUnitBlockage.DateUntil))
                        {
                            UnitInvBlocking additionalUnitInvBlocking =
                                new UnitInvBlocking()
                                {
                                    DateFrom = newUnitBlockage.DateUntil.AddDays(1),
                                    DateUntil = unitBlockade.DateUntil,
                                    Unit = unitBlockade.Unit,
                                    Type = unitBlockade.Type
                                };

                            _Context.UnitInvBlockings.Add(additionalUnitInvBlocking);
                            unitBlockade.DateUntil = newUnitBlockage.DateFrom.AddDays(-1);
                        }
                        else if (newUnitBlockage.DateUntil <= unitBlockade.DateUntil)
                        {
                            unitBlockade.DateFrom = newUnitBlockage.DateUntil.AddDays(1);
                            unitBlockade.DateUntil = unitBlockade.DateUntil;
                        }
                        else if (newUnitBlockage.DateFrom >= unitBlockade.DateFrom)
                        {
                            unitBlockade.DateFrom = unitBlockade.DateFrom;
                            unitBlockade.DateUntil = newUnitBlockage.DateFrom.AddDays(-1);
                        }
                    }

                    _Context.SaveChanges();
                }
                else if (newUnitBlockage.Type == (int)UnitBlockingType.OwnerStay)
                {
                    AddUnitBlockade(newUnitBlockage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot add unit inv blocking", ex);
            }
        }

        /// <summary>
        /// Retrieves data for the Availability controll on the PropertyDetails page
        /// </summary>
        /// <param name="unitId">Id of the unit</param>
        /// <param name="dateFrom">Starting date</param>
        /// <param name="dateUntil">Ending date</param>
        /// <returns>List of the unit availability data</returns>
        public IEnumerable<UnitAvailabilityResult> GetUnitAvailability(int unitId, DateTime dateFrom, DateTime dateUntil)
        {
            var list = _Context.sp_GetUnitAvailability(dateFrom, dateUntil, unitId).Select(s =>
                new UnitAvailabilityResult()
                {
                    Date = s.monthDay.Value.ToString("yyyy-MM-dd"),
                    Price = s.priceValue.HasValue ? s.priceValue.Value : 0,
                    IsBlocked = s.isBlocked.Value,
                    UnitBlockingType = (UnitBlockingType)s.unitBlockingType.Value,
                    Mlos = s.mlos.HasValue ? s.mlos.Value : 0,
                    Cta = s.cta.Value
                });
            return list.ToList();
        }

        public IEnumerable<Unit> GetUnitsByOwnerId(int ownerId, bool onlyAvailable = false)
        {
            var list = _Context.Units.Where(p => p.Property.User.UserID.Equals(ownerId));
            if (onlyAvailable)
            {
                list = list.Where(p=> p.Property.PropertyLive.Equals(true));
            }
            return list.ToList();
        }

        public Unit GetUnitForPropertyByCode(int propertyId, string code)
        {
            return _Context.Units.Where(pc => pc.UnitCode.ToLower() == code.ToLower().Trim() && pc.Property.PropertyID == propertyId).FirstOrDefault();
        }

        public IEnumerable<Unit> GetUnitsByManagerId(int managerId, RoleLevel roleLevel, bool onlyAvailable = false)
        {
            int managerRole = (int)roleLevel;
            var list = _Context.Units.Where(p => p.Property.PropertyAssignedManagers.Any(m => m.User.UserID.Equals(managerId) && m.Role.RoleLevel.Equals(managerRole)));
            if (onlyAvailable)
            {
                list = list.Where(p => p.Property.PropertyLive.Equals(true));
            }
            return list.ToList();
        }
    }
}
