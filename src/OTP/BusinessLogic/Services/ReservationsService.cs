﻿using BusinessLogic.Enums;
using BusinessLogic.Objects.StaticContentRepresentation;
using BusinessLogic.Objects.Templates.Document;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.PaymentGateway;
using BusinessLogic.ServiceContracts;
using Common.Converters;
using Common.Exceptions;
using Common.Exceptions.ReservationDateChange;
using Common.Extensions;
using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Transactions;

namespace BusinessLogic.Services
{
    public class ReservationsService : BaseService, IReservationsService
    {

        public ReservationsService(IConfigurationService configurationService, IBlobService blobService, IContextService httpContextService,
            IPropertiesService propertiesService, IBookingService bookingService, IPaymentGatewayService paymentGatewayService,
            IDocumentService documentService, IMessageService messageService, ISettingsService settingsService, IUnitsService unitService)
            : base(httpContextService, new Dictionary<Type, object>() {
                                                                        { typeof(IConfigurationService), configurationService },
                                                                        { typeof(IPropertiesService), propertiesService },
                                                                        { typeof(IBlobService),  blobService },
                                                                        { typeof(IBookingService),  bookingService },
                                                                        { typeof(IPaymentGatewayService),  paymentGatewayService },
                                                                        { typeof(IDocumentService),  documentService },
                                                                        { typeof(IMessageService),  messageService },
                                                                        { typeof(ISettingsService), settingsService },
                                                                        { typeof(IUnitsService), unitService }
            }
            )
        { }

        public ReservationsService(OTPEntities ctx, IConfigurationService configurationService, IBlobService blobService, IBookingService bookingService, ISettingsService settingService, IMessageService messageService, IUnitsService unitService)
            : base(new Dictionary<Type, object>() {
                                                    { typeof(IConfigurationService), configurationService },
                                                    { typeof(IBlobService),  blobService },
                                                    { typeof(IBookingService),  bookingService },
                                                    { typeof(ISettingsService),  settingService},
                                                    { typeof(IMessageService),  messageService },
                                                    { typeof(IUnitsService),  unitService }

            }
            , ctx)
        { }

        public void AddReservation(Reservation reservation)
        {
            try
            {
                _Context.Reservations.Add(reservation);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Reservation", ex);
            }
        }

        public Reservation GetReservationById(int id)
        {
            return _Context.Reservations.Where(m => m.ReservationID.Equals(id)).FirstOrDefault<Reservation>();
        }


        public Reservation GetReservationByLinkToken(string reservationToken)
        {
            return _Context.Reservations.Where(m => m.LinkAuthorizationToken.Equals(reservationToken)).SingleOrDefault();
        }

        public IEnumerable<ReservationForUser> GetReservationsByUserId(int userId)
        {
            List<ReservationForUser> reservations = new List<ReservationForUser>();
            string blobContainter = _configurationService.GetKeyValue(Enums.CloudConfigurationKeys.StorageBlobUrl);
            try
            {
                reservations = _Context.sp_SearchForMyTrips(userId)
                .Select(item => new ReservationForUser()
                {
                    BookingDate = item.BookingDate,
                    BookingStatus = item.BookingStatus,
                    ConfirmationID = item.ConfirmationID,
                    DateArrival = item.DateArrival,
                    DateDeparture = item.DateDeparture,
                    PropertyID = item.PropertyID,
                    ReservationID = item.ReservationID,
                    Thumbnails = item.Thumbnails == null ? new List<string>() : item.Thumbnails.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(s => string.Format("{0}{1}", blobContainter, s)).ToList(),
                    TotalPrice = item.TotalPrice,
                    TripBalance = item.TripBalance,
                    Property = this._propertiesService.GetPropertyById(item.PropertyID)
                }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't search MyTrips for selected user", ex);
            }
            return reservations;
        }

        public IEnumerable<ReservationForUser> GetCurrentReservationsByUserId(int userId)
        {
            List<ReservationForUser> reservations = new List<ReservationForUser>();
            string blobContainter = _configurationService.GetKeyValue(Enums.CloudConfigurationKeys.StorageBlobUrl);
            try
            {
                reservations = _Context.sp_SearchForMyTrips(userId)
                .Select(item => new ReservationForUser()
                {
                    BookingDate = item.BookingDate,
                    BookingStatus = item.BookingStatus,
                    ConfirmationID = item.ConfirmationID,
                    DateArrival = item.DateArrival,
                    DateDeparture = item.DateDeparture,
                    PropertyID = item.PropertyID,
                    ReservationID = item.ReservationID,
                    Thumbnails = item.Thumbnails == null ? new List<string>() : item.Thumbnails.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(s => string.Format("{0}{1}", blobContainter, s)).ToList(),
                    TotalPrice = item.TotalPrice,
                    TripBalance = item.TripBalance,
                    Property = this._propertiesService.GetPropertyById(item.PropertyID)
                }).Where(r => r.BookingStatus != (int)BookingStatus.Cancelled && r.BookingStatus != (int)BookingStatus.CheckedOut).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't search MyTrips for selected user", ex);
            }
            return reservations;
        }

        /// <summary>
        /// Returns all reservations in the database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Reservation> GetReservations()
        {
            var list = _Context.Reservations.OrderBy(r => r.BookingDate).ToList();
            return list;
        }

        /// <summary>
        /// Returns reservation details data for property
        /// </summary>
        /// <param name="propertyId">Id of the property</param>
        /// <returns></returns>
        public IEnumerable<ReservationForHomeOwner> GetReservationsForProperty(int propertyId, List<BookingStatus> excludedStatuses = null)
        {
            excludedStatuses = excludedStatuses == null ? new List<BookingStatus>() : excludedStatuses;
            List<int> excludeStatusesIntVal = new List<int>();

            /// Convert to INT value list - Azure has problem with comparing enums -- IMPORTANT!
            foreach (BookingStatus status in excludedStatuses)
            {
                excludeStatusesIntVal.Add((int)status);
            }
            
            var list = _Context.Reservations
                .Where(s =>
                    !excludeStatusesIntVal.Contains(s.BookingStatus)
                    && s.Property.PropertyID.Equals(propertyId)
                    && s.Property.PropertyLive.Equals(true)
                    )
                .Select(item => new ReservationForHomeOwner()
            {
                Booked = item.BookingDate,
                ConfirmationId = item.ConfirmationID,
                CheckIn = item.DateArrival,
                CheckOut = item.DateDeparture,
                GrossDue = item.TotalPrice,
                CultureCode = item.Property.DictionaryCulture.CultureCode,
                User = item.User,
                TransactionFee = item.TransactionFee,
                OwnerNet = (item.TotalPrice - item.TransactionFee),
                PropertyId = item.Property.PropertyID,
                UnitId = item.Unit.UnitID,
                BookingStatus = item.BookingStatus
            }
            ).ToList();

            return list;
        }

        /// <summary>
        /// Returns reservation details data for Homeowner
        /// </summary>
        /// <param name="userId">Id of the homeowner user</param>
        /// <param name="propertyId">Id of the property</param>
        /// <returns></returns>
        public IEnumerable<ReservationForHomeOwner> GetReservationTransactionsForHomeOwner(int userId, int? propertyId)
        {
            int bookingStatusCheckedOut = (int)BookingStatus.CheckedOut;

            var q = _Context.Reservations
                .Where(s => s.Property.User.UserID.Equals(userId)
                    && s.Property.PropertyLive.Equals(true)
                    && s.BookingStatus.Equals(bookingStatusCheckedOut)
                    );
            if (propertyId.HasValue)
            {
                q = q.Where(r => r.Property.PropertyID.Equals(propertyId.Value));
            }

            int transactionCodeTax = (int)TransactionCodes.Tax;

            var list = q.Select(item => new ReservationForHomeOwner()
            {
                Booked = item.BookingDate,
                ConfirmationId = item.ConfirmationID,
                CheckIn = item.DateArrival,
                CheckOut = item.DateDeparture,
                GrossDue = item.TotalPrice,
                TaxValue = item.ReservationTransactions.Where(s => s.TransactionCode.TransactionCode1.Value.Equals(transactionCodeTax)).Sum(t => t.Amount),
                CultureCode = item.Property.DictionaryCulture.CultureCode,
                User = item.User,
                TransactionFee = item.TransactionFee,
                OwnerNet = (item.TotalPrice - item.TransactionFee),
                PropertyId = item.Property.PropertyID,
                UnitId = item.Unit.UnitID,
                BookingStatus = item.BookingStatus
            }).ToList();

            return list;
        }

        /// <summary>
        /// Returns reservation details data for unit
        /// </summary>
        /// <param name="unitId">Id of the unit</param>
        /// <returns></returns>
        public IEnumerable<ReservationForHomeOwner> GetReservationsForUnit(int unitId)
        {
            int bookingStatusCancelled = (int)BookingStatus.Cancelled;
            int bookingStatusTentative = (int)BookingStatus.TentativeBooking;
            int bookingStatusWithdrew = (int)BookingStatus.Withdrew;
            int bookingStatusDateChanged = (int)BookingStatus.DatesChanged;

            var list = _Context.Reservations
                .Where(s =>
                    !s.BookingStatus.Equals(bookingStatusCancelled)
                    && !s.BookingStatus.Equals(bookingStatusTentative)
                    && !s.BookingStatus.Equals(bookingStatusWithdrew)
                    && !s.BookingStatus.Equals(bookingStatusDateChanged)
                    && s.Property.PropertyLive.Equals(true)
                    && s.Unit.UnitID.Equals(unitId)
                    )
                .Select(item => new ReservationForHomeOwner()
                {
                    Booked = item.BookingDate,
                    ConfirmationId = item.ConfirmationID,
                    CheckIn = item.DateArrival,
                    CheckOut = item.DateDeparture,
                    GrossDue = item.TotalPrice,
                    CultureCode = item.Property.DictionaryCulture.CultureCode,
                    User = item.User,
                    TransactionFee = item.TransactionFee,
                    OwnerNet = (item.TotalPrice - item.TransactionFee),
                    PropertyId = item.Property.PropertyID,
                    UnitId = item.Unit.UnitID,
                    BookingStatus = item.BookingStatus
                }
            ).ToList();

            int ownerStayBlocking = (int)UnitBlockingType.OwnerStay;

            var listOwner = _Context.UnitInvBlockings.Where(s => s.Type.Equals(ownerStayBlocking)).Select(item => new ReservationForHomeOwner()
            {
                UnitId = item.Unit.UnitID,
                CheckIn = item.DateFrom,
                CheckOut = item.DateUntil,
                CultureCode = item.Reservation.Property.DictionaryCulture.CultureCode,
                GrossDue = 0,
                TransactionFee = 0,
                OwnerNet = 0

            }).ToList();

            list.AddRange(listOwner);

            return list;
        }

        /// <summary>
        /// Returns not finalized reservations(with status "Tenative Booking" and older than 15 minutes) and canceled reservations
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Reservation> GetNotFinalizedOrCanceledReservations()
        {
            var tentativeValidityPeriod = DateTime.Now.AddMinutes(-15);
            return
                _Context.AutoRetryQuery<IEnumerable<Reservation>>(() =>
                _Context.Reservations
                    .Include(r => r.UnitInvBlockings)
                    .Where(r => r.BookingStatus == (int)BookingStatus.Cancelled ||
                                r.BookingStatus == (int)BookingStatus.NoFunds ||
                                (r.BookingStatus == (int)BookingStatus.TentativeBooking && r.BookingDate <= tentativeValidityPeriod))).ToList();
        }

        public IEnumerable<Reservation> GetBookedReservationsAfter24Hours()
        {
            var yeasterday = DateTime.Now.AddDays(-1);
            return _Context.AutoRetryQuery<IEnumerable<Reservation>>(() => _Context.Reservations
                .Include(r => r.GuestPaymentMethod)
                .Include(r => r.Property)
                .Include(r => r.Property.DictionaryCulture)
                .Where(r => r.BookingStatus == (int)BookingStatus.ReceivedBooking && r.BookingDate <= yeasterday)).ToList();
        }

        private IQueryable<Reservation> GetUpcomingXDaysUpfrontReservationsQuery(int days)
        {
            var q =
                _Context.AutoRetryQuery<IQueryable<Reservation>>(() =>
                _Context.Reservations
                .Where(r => r.BookingStatus == (int)BookingStatus.FinalizedBooking
                             && EntityFunctions.DiffDays(r.BookingDate, r.DateArrival) > days));
            return q;
        }

        public IEnumerable<Reservation> GetUpcomingXDaysUpfrontReservationsForPayment(int days = 30)
        {
            List<Reservation> list = new List<Reservation>();

            var commonQuery = GetUpcomingXDaysUpfrontReservationsQuery(days);
            var q = commonQuery.Where(r => (r.UpfrontPaymentProcessed == false || r.UpfrontPaymentProcessed == null)).ToList();

            foreach (var item in q)
            {
                DateTime mostRestrictiveArrivalDate = FindMostRestrictiveArrivalDateForComparision(item, item.DateArrival); // look by most restrictive arrival date
                if ((DateTime.Now - mostRestrictiveArrivalDate).Days < days)
                {
                    list.Add(item);
                }
            }

            return list;
        }

        public IEnumerable<Reservation> GetUpcomingXDaysUpfrontReservations(int days = 30)
        {
            List<Reservation> list = new List<Reservation>();

            var q = GetUpcomingXDaysUpfrontReservationsQuery(days).ToList();
                
            foreach (var item in q)
            {
                DateTime mostRestrictiveArrivalDate = FindMostRestrictiveArrivalDateForComparision(item, item.DateArrival); // look by most restrictive arrival date
                if ((DateTime.Now - mostRestrictiveArrivalDate).Days < days)
                {
                    list.Add(item);
                }
            }

            return list;
        }

        public IEnumerable<Reservation> GetCompletedReservationsWithinXDays(int days)
        {
            DateTime today = DateTime.Now.Date;
            var q =
                _Context.AutoRetryQuery<IQueryable<Reservation>>(() =>
                _Context.Reservations
                .Where(r => r.BookingStatus == (int)BookingStatus.CheckedIn && EntityFunctions.DiffDays(today, r.DateDeparture) <= days)).ToList();

            return q;
        }

        public IEnumerable<Reservation> GetSecurityDepositRetryReservations()
        {
            DateTime today = DateTime.Now.Date;
            var q =
                _Context.AutoRetryQuery<IQueryable<Reservation>>(() =>
                _Context.Reservations
                .Where(r => 
                    (
                        r.BookingStatus == (int)BookingStatus.FinalizedBooking
                    )                    
                    && (r.TakeSecurityDeposit.HasValue && r.TakeSecurityDeposit.Value.Equals(true))
                    && (r.SecurityDepositToken == null || r.SecurityDepositToken == "")
                )).ToList();
            
            return q;
        }

        public void DeleteTentativeReservation(int reservationId)
        {
            var reservation = _Context.Reservations.FirstOrDefault(r => r.ReservationID == reservationId);

            if (reservation == null)
                throw new ArgumentNullException(String.Format("Reservation with an id {0} does not exists", reservationId));

            if (reservation.BookingStatus != (int)BookingStatus.TentativeBooking)
                throw new InvalidOperationException(String.Format("Reservation with an id {0} cannot be deleted", reservation.ReservationID));

            //clearing payment gateway operations log for reservation
            foreach (var logEntry in reservation.PaymentGatewayOperationsLogs)
            {
                logEntry.Reservation = null;
            }
            reservation.PaymentGatewayOperationsLogs.Clear();

            //clearing billing addresses for reservation
            foreach (var billingAddress in reservation.ReservationBillingAddresses)
            {
                _Context.ReservationBillingAddresses.Remove(billingAddress);
            }
            reservation.ReservationBillingAddresses.Clear();

            //clearing reservation transactions for reservation
            foreach (var reservationTransaction in reservation.ReservationTransactions)
            {
                _Context.ReservationTransactions.Remove(reservationTransaction);
            }
            reservation.ReservationTransactions.Clear();

            //clearing reservation details for reservation
            foreach (var reservationDetails in reservation.ReservationDetails)
            {
                _Context.ReservationDetails.Remove(reservationDetails);
            }
            reservation.ReservationDetails.Clear();

            //clearing guest reviews
            foreach (var guestReview in reservation.GuestReviews)
            {
                _Context.GuestReviews.Remove(guestReview);
            }
            reservation.GuestReviews.Clear();

            _Context.Reservations.Remove(reservation);
            _Context.SaveChanges();
        }

        /// <summary>
        /// Change reservation dates logic with: payment gateway handling, invoice recalculation, guest email notification
        /// </summary>
        /// <param name="reservationId">Id of the guest reservation</param>
        /// <param name="dateFrom">New arrival date of the vacation</param>
        /// <param name="dateUntil">New departure date of the vacation</param>
        public void ChangeReservationDate(int reservationId, DateTime dateFrom, DateTime dateUntil)
        {
            Reservation currentReservation; //reservation send by parameter
            Reservation newReservation; // newly created reservation by this service
            DateTime bookingDateToDiff = DateTime.MinValue; // Booking date to comparision { orginal or newest }
            DateTime arrivalDateToDiff = DateTime.MinValue; // Arrival date to comparision { orginal or closest }
            UserAgreement agreementDoc = null;
            Invoice invoiceDoc = null;
            TripDetails tripDetails = null;
            UserAgreementDateChange agreementDateChangeDoc = null;

            try
            {
                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    currentReservation = _Context.Reservations.Find(reservationId);
                    ValidateReservationChangePossibility(currentReservation, dateFrom, dateUntil); // perform all the validation steps before any action

                    arrivalDateToDiff = FindMostRestrictiveArrivalDateForComparision(currentReservation, dateFrom); // get most restrictive arrival date to compare with payment policy

                    #region Calculate new invoice
                    PropertyDetailsInvoice newReservationInvoiceDetails = this._bookingService.CalculateInvoice(currentReservation.Property.PropertyID, currentReservation.Unit.UnitID, dateFrom, dateUntil);
                    decimal corectionInvoiceBalance = newReservationInvoiceDetails.InvoiceTotal - currentReservation.TotalPrice;
                    #endregion

                    #region Save new reservation

                    decimal newReservationTotalPrice = corectionInvoiceBalance > 0 ? newReservationInvoiceDetails.InvoiceTotal : currentReservation.TotalPrice; // if new reservation has been more expensive new amount else old amount
                    decimal newReservationFee = corectionInvoiceBalance > 0 ?
                        this._propertiesService.CalculatePropertyCommission(dateFrom, dateUntil, newReservationInvoiceDetails.InvoiceTotal, currentReservation.Property.PropertyID, newReservationInvoiceDetails.LengthOfStay) : currentReservation.TransactionFee;

                    newReservation = new Reservation()
                    {
                        BookingDate = currentReservation.BookingDate,
                        DateArrival = dateFrom,
                        DateDeparture = dateUntil,
                        BookingStatus = currentReservation.BookingStatus,
                        TripBalance = -newReservationTotalPrice, // calculate later
                        TotalPrice = newReservationTotalPrice,
                        Unit = currentReservation.Unit,
                        UnitType = currentReservation.UnitType,
                        Property = currentReservation.Property,
                        DictionaryCulture = currentReservation.DictionaryCulture,
                        User = currentReservation.User,
                        ParentReservationId = currentReservation.ReservationID,
                        Agreement = currentReservation.Agreement,
                        GuestPaymentMethod = currentReservation.GuestPaymentMethod,
                        GuestReviews = currentReservation.GuestReviews,
                        NumberOfGuests = currentReservation.NumberOfGuests,
                        TransactionFee = newReservationFee,
                        TransactionToken = currentReservation.TransactionToken,
                        UpfrontPaymentProcessed = currentReservation.UpfrontPaymentProcessed,
                        ConfirmationID = currentReservation.ConfirmationID,
                        Invoice = currentReservation.Invoice,
                        LinkAuthorizationToken = Guid.NewGuid().ToString(),
                        SecurityDeposit = currentReservation.SecurityDeposit,
                        SecurityDepositToken = currentReservation.SecurityDepositToken
                    };
                    _Context.Reservations.Add(newReservation);

                    //TODO copy billing address

                    #endregion

                    #region Release old and create new Unit Blockings
                    SwitchUnitBlockings(currentReservation, newReservation);
                    #endregion

                    #region Create new reservation transactions
                    HandleTransactionCreation(currentReservation, newReservation, newReservationInvoiceDetails, corectionInvoiceBalance);
                    #endregion

                    #region Recalculate trip balance
                    //we have to do recalculation of the trip balance      
                    decimal alreadyPaidSum = 0;
                    decimal calculatedTripBalance = CalculateReservationTripBalance(newReservation, out alreadyPaidSum);
                    newReservation.TripBalance = calculatedTripBalance;
                    #endregion

                    #region Payment Gateway Operations
                    HandlePaymentGatewayOperations(newReservation, currentReservation, arrivalDateToDiff, newReservationInvoiceDetails, corectionInvoiceBalance, alreadyPaidSum);
                    #endregion

                    #region Cancel current reservation
                    currentReservation.BookingStatus = (int)BookingStatus.DatesChanged;
                    #endregion

                    #region Generate documents and send email
                    HandleDocumentGeneration(newReservation, newReservationInvoiceDetails, corectionInvoiceBalance, ref agreementDoc, ref agreementDateChangeDoc, ref invoiceDoc, ref tripDetails);
                    string subject = String.Format(Common.Resources.Common.ConfirmationEmail_Subject, newReservation.Property.PropertyNameCurrentLanguage, string.Format("{0} {1}", newReservation.User.Firstname, newReservation.User.Lastname));
                    SendTripDetailEmails(subject, newReservation, newReservationInvoiceDetails, agreementDoc, invoiceDoc, tripDetails, agreementDateChangeDoc);

                    //Arrival guest information ( Key management, WiFi ... )
                    TimeSpan reservationBookArrivalDiff = newReservation.DateArrival - DateTime.Now;
                    if (reservationBookArrivalDiff.Days < 2) //this situation will not be handled by IntranetProcessor
                    {
                        if (newReservation.Property.KeyProcessType == (int)KeyProcessType.Lockbox)
                        {
                            SendCheckInLockboxEmail(newReservation);
                        }
                        else if (newReservation.Property.KeyProcessType == (int)KeyProcessType.KeyHandler)
                        {
                            SendCheckInPickUpKeyHandlerEmail(newReservation);
                        }
                    }
                    #endregion

                    ts.Complete();
                }
            }
            catch (PaymentGatewayException) //manual transaction handling
            {
                DisposeContext();
                throw;
            }
            catch (ResvDateChangeExceptionBase) //manual transaction handling
            {
                DisposeContext();
                throw;
            }
        }

        public void SendTripDetailEmails(string emailSubject, Reservation newReservation, PropertyDetailsInvoice newReservationInvoiceDetails,
            UserAgreement agreement, Invoice invoice, TripDetails tripDetails, UserAgreementDateChange agreementDateChangeDoc = null)
        {
            BaseDocument agreementDocument = null;

            CultureInfo cInfo = newReservation.DictionaryCulture.CultureInfo;

            ReservationConfirmationForOwner confirmOwner = new ReservationConfirmationForOwner(newReservation.Property.User.email);
            confirmOwner.OwnerFirstName = newReservation.Property.User.Firstname;
            confirmOwner.GuestFullName = string.Format("{0} {1}", newReservation.User.Firstname, newReservation.User.Lastname);
            confirmOwner.Booked = DateTime.Now.ToLocalizedDateString();
            confirmOwner.CheckIn = newReservation.DateArrival.ToLocalizedDateString();
            confirmOwner.CheckOut = newReservation.DateDeparture.ToLocalizedDateString();
            confirmOwner.ConfirmationId = newReservation.ConfirmationID;
            confirmOwner.GrossDue = newReservation.TotalPrice.ToString("c", cInfo);
            confirmOwner.TransactionFee = newReservation.TransactionFee.ToString("c", cInfo);
            confirmOwner.OwnerNet = (newReservation.TotalPrice - newReservation.TransactionFee).ToString("c", cInfo);
            confirmOwner.PropertyName = newReservation.Property.PropertyNameCurrentLanguage;
            confirmOwner.Subject = emailSubject;
            confirmOwner.OTPFrontEnd = string.Format("http://{0}", this._settingsService.GetSettingValue(SettingKeyName.ExtranetURL));

            //agreement as attachment
            if (agreementDateChangeDoc == null)
            {
                agreementDocument = agreement;
            }
            else
            {
                agreementDocument = agreementDateChangeDoc;
            }

            //agreement email
            this._messageService.AddEmail(confirmOwner, newReservation.Property.DictionaryCulture.CultureCode, new List<BaseDocument>() { agreementDocument });

            ReservationConfirmationForGuest confirmGuest = new ReservationConfirmationForGuest(newReservation.User.email);
            confirmGuest.GuestFirstName = string.Format("{0}", newReservation.User.Firstname);
            confirmGuest.BookedDate = DateTime.Now.ToLocalizedDateTimeString();
            confirmGuest.CheckIn = newReservation.DateArrival.ToLongDateString();
            confirmGuest.CheckOut = newReservation.DateDeparture.ToLongDateString();
            confirmGuest.CheckInTime = new DateTime(newReservation.Property.CheckIn.Ticks).ToString("t");
            confirmGuest.CheckOutTime = new DateTime(newReservation.Property.CheckOut.Ticks).ToString("t");
            confirmGuest.ConfirmationCode = newReservation.ConfirmationID;
            confirmGuest.DateBeforeArrival = newReservation.DateArrival.AddDays(-2).ToLocalizedDateString();
            confirmGuest.Directions = "";
            confirmGuest.GuestFullName = string.Format("{0} {1}", newReservation.User.Firstname, newReservation.User.Lastname);
            confirmGuest.GuestsNumber = newReservation.NumberOfGuests.ToString();
            confirmGuest.CardinalityPerson = newReservation.NumberOfGuests > 1 ? Common.Resources.Common.Cardinality_Persons.ToLower() : Common.Resources.Common.Cardinality_Person.ToLower();
            confirmGuest.Latitude = newReservation.Property.Latitude.ToString().Replace(",", ".");
            confirmGuest.Longitude = newReservation.Property.Longitude.ToString().Replace(",", ".");
            confirmGuest.MaxNoOfGuests = newReservation.Unit.MaxNumberOfGuests.ToString();
            confirmGuest.NightsNumber = newReservationInvoiceDetails.LengthOfStay.ToString();
            confirmGuest.PropertyAddress = string.Format("{0}, {1}, {2} {3}", newReservation.Property.Address1, newReservation.Property.Address2, newReservation.Property.City, newReservation.Property.State);
            confirmGuest.PropertyCountry = newReservation.Property.DictionaryCountry.CountryNameCurrentLanguage;
            confirmGuest.GuestEmail = newReservation.User.email;
            confirmGuest.PropertyName = newReservation.Property.PropertyNameCurrentLanguage;
            confirmGuest.Subject = emailSubject;
            DateTime mostRestrictiveArrivalDate = FindMostRestrictiveArrivalDateForComparision(newReservation, newReservation.DateArrival);
            confirmGuest.CancellationPolicy = CancellationPolicy(mostRestrictiveArrivalDate, newReservation.BookingDate, newReservation.Property.DictionaryCulture.CultureInfo);
            confirmGuest.OTPFrontEnd = string.Format("http://{0}", this._settingsService.GetSettingValue(SettingKeyName.ExtranetURL));
            confirmGuest.TermsOfServiceHref = string.Format("http://{0}{1}", this._settingsService.GetSettingValue(SettingKeyName.ExtranetURL), "/Home/TermsOfService");

            var psc = this._propertiesService.GetPropertyStaticContentByType(newReservation.Property.PropertyID, PropertyStaticContentType.ParkingInformation).SingleOrDefault();
            confirmGuest.OnStreetParkingAvailable = psc != null ? psc.ContentValueCurrentLanguage : "";
            var directions = this._propertiesService.GetPropertyDirectionsToAirport(newReservation.Property.PropertyID);

            foreach (DirectionToAirport direction in directions)
            {
                confirmGuest.Directions += string.Format("<a href='{0}'>{1}</a><br />", direction.DirectionLink, direction.Name.ToString());
            }

            if (invoice != null)
            {
                this._messageService.AddEmail(confirmGuest, newReservation.Property.DictionaryCulture.CultureCode, new List<BaseDocument>() { agreementDocument, tripDetails, invoice });
            }
            else
            {
                var cultureCode = CultureCode.en_US;
                Template tmpl = _Context.Templates.Where(nt => nt.Type == (int)TemplateType.Invoice && nt.DictionaryCulture.CultureCode == cultureCode).SingleOrDefault();
                string tmpTitle = tmpl.Title;
                string title = tmpTitle.Replace("{{ConfirmationId}}", newReservation.ConfirmationID);

                Attachment invoiceData = new Attachment()
                {
                    Content = newReservation.Invoice,
                    Type = (int)DocumentType.PDF,
                    Title = title
                };
                this._messageService.AddEmail(confirmGuest, newReservation.Property.DictionaryCulture.CultureCode, new List<BaseDocument>() { agreementDocument, tripDetails }, invoiceData);
            }
        }

        /// <summary>
        /// Search for the most restrictive arrival date ( the closesst to today )
        /// </summary>
        /// <param name="currentReservation">Currently active reservation</param>
        /// <param name="newArrivalDate">New arrival date</param>
        /// <returns></returns>
        public DateTime FindMostRestrictiveArrivalDateForComparision(Reservation currentReservation, DateTime newArrivalDate)
        {
            DateTime arrivalDateToDiff = newArrivalDate;
            Reservation iterationReservation = currentReservation;

            if (iterationReservation.DateArrival < arrivalDateToDiff)
            {
                arrivalDateToDiff = iterationReservation.DateArrival;
            }

            while (iterationReservation.ParentReservationId.HasValue)
            {
                iterationReservation = _Context.Reservations.Find(iterationReservation.ParentReservationId.Value);
                if (iterationReservation.DateArrival < arrivalDateToDiff)
                {
                    arrivalDateToDiff = iterationReservation.DateArrival;
                }
            }
            return arrivalDateToDiff;

        }

        public Reservation FindMostExpensiveReservation(Reservation currentReservation)
        {
            
            Reservation mostExpensiveReservation = currentReservation;
            Reservation iterationReservation = currentReservation;

            while (iterationReservation.ParentReservationId.HasValue)
            {
                iterationReservation = _Context.Reservations.Find(iterationReservation.ParentReservationId.Value);
                if (mostExpensiveReservation.TotalPrice <= iterationReservation.TotalPrice)
                    mostExpensiveReservation = iterationReservation;
            }

            return mostExpensiveReservation;

        }

        public Reservation FindOriginalReservation(Reservation currentReservation)
        {
            Reservation originalReservation;

            if (currentReservation.ParentReservationId.HasValue)
            {
                int parentReservationId = currentReservation.ParentReservationId.Value;
                int? tmpReservationId = currentReservation.ParentReservationId.Value;
                while (tmpReservationId.HasValue)
                {
                    tmpReservationId = _Context.Reservations.Find(tmpReservationId).ParentReservationId;
                    if (tmpReservationId.HasValue)
                    {
                        parentReservationId = tmpReservationId.Value;
                    }
                }

                originalReservation = _Context.Reservations.Find(parentReservationId);
            }
            else //this means no parent reservation so parent reservation is the same as current
            {
                originalReservation = currentReservation;
            }

            return originalReservation;
        }

        /// <summary>
        /// Validates if change reservation dates is possible
        /// </summary>
        /// <param name="currentReservation">Current reservation (parent of the new one)</param>
        /// <param name="dateArrival">Date of the arrival</param>
        /// <param name="dateDeparture">Date of the departure</param>
        public void ValidateReservationChangePossibility(Reservation currentReservation, DateTime dateArrival, DateTime dateDeparture)
        {
            #region Validate if last child
            int count = _Context.Reservations.Where(r => r.ParentReservationId.Value.Equals(currentReservation.ReservationID)).Count();
            if (count > 0)
            {
                throw new ResvDateChangeExistingChildException(); // there is child reservation of the current reservation
            }
            #endregion

            #region Validation if change posible
            TimeSpan dateDiff = dateArrival - DateTime.Now;

            if (currentReservation.DateArrival.Date < DateTime.Now.Date)
            {
                throw new ResvDateChangeDateArrivalException(); // vacation is already in progress or it was in the past
            }

            if (dateDiff.Days < 0)
            {
                throw new ResvDateChangeDateArrivalException(); // arrival date is earlier than now
            }

            if (currentReservation.DateArrival.Date == dateArrival.Date && currentReservation.DateDeparture.Date == dateDeparture.Date)
            {
                throw new ResvDateChangeNoChangeException(); // dates wre not modified
            }


            #endregion

            #region Validation unit availability
            BookingInfoModel bookingModel = new BookingInfoModel()
            {
                dateTripFrom = dateArrival,
                dateTripUntil = dateDeparture,
                PropertyId = currentReservation.Property.PropertyID,
                UnitId = currentReservation.Unit.UnitID,
            };
            bool isAvailable = this._bookingService.IsUnitAvailable(bookingModel, currentReservation.ReservationID); // check for availability but exclude current reservation form unit blocking check
            if (!isAvailable)
            {
                throw new ResvDateChangeUnavailableException(); //unit is not available
            }

            #endregion
        }

        /// <summary>
        /// Calculates trip balance for the reservation basing on reservation transactions
        /// </summary>
        /// <param name="reservation">Reservation to calculate trip balance</param>
        /// <param name="sumOfPayments">Amount sum that already have been paid</param>
        /// <returns></returns>
        private decimal CalculateReservationTripBalance(Reservation reservation, out decimal sumOfPayments)
        {
            decimal calculatedTripBalance = 0;
            decimal alreadyPaidSum = 0;
            foreach (var tran in reservation.ReservationTransactions)
            {
                calculatedTripBalance += tran.Payment == true ? tran.Amount : -tran.Amount;
                alreadyPaidSum += tran.Payment == true ? tran.Amount : 0;
            }
            sumOfPayments = alreadyPaidSum;

            return calculatedTripBalance;
        }

        /// <summary>
        /// Removes unit inv blockings from the old reservation period and creates for the new reservation period
        /// </summary>
        /// <param name="currentReservation">Old reservation</param>
        /// <param name="newReservation">New reservation</param>
        private void SwitchUnitBlockings(Reservation currentReservation, Reservation newReservation)
        {
            var blockings = currentReservation.UnitInvBlockings.ToList();
            foreach (var unitBlocking in blockings)
            {
                _Context.UnitInvBlockings.Remove(unitBlocking);
            }

            // create new
            UnitInvBlocking newUnitBlocking = new UnitInvBlocking()
            {
                DateFrom = newReservation.DateArrival,
                DateUntil = newReservation.DateDeparture.AddDays(-1), //one day less than actual selection date
                Type = (int)UnitBlockingType.Reservation,
                Unit = currentReservation.Unit,
                Reservation = newReservation
            };
            newReservation.UnitInvBlockings.Add(newUnitBlocking);
            _Context.UnitInvBlockings.Add(newUnitBlocking);
        }



        /// <summary>
        /// Creates all the transactions for the new reservation - using old reservation payment transactions
        /// </summary>
        /// <param name="currentReservation"></param>
        /// <param name="newReservation"></param>
        /// <param name="newReservationInvoiceDetails"></param>
        /// <param name="corectionInvoiceBalance"></param>
        private void HandleTransactionCreation(Reservation currentReservation, Reservation newReservation, PropertyDetailsInvoice newReservationInvoiceDetails, decimal corectionInvoiceBalance)
        {
            if (newReservation.BookingStatus == (int)BookingStatus.ReceivedBooking) // it means that there is only authorize payment we should not copy payments
            {
                this._bookingService.AddReservationTransactions(newReservation, newReservationInvoiceDetails);
                newReservation.TotalPrice = newReservationInvoiceDetails.InvoiceTotal; // if invoice is lower then we create lower invoice
            }
            else if (corectionInvoiceBalance > 0)
            { // if new invoice is greater than old one create new transactions                
                this._bookingService.AddReservationTransactions(newReservation, newReservationInvoiceDetails);

                //then copy all the payment transactions from old one to get proper balance
                var oldPaymentTransactions = currentReservation.ReservationTransactions.Where(t => t.Payment.Equals(true));
                foreach (var oldTransaction in oldPaymentTransactions)
                {
                    ReservationTransaction newtransaction = new ReservationTransaction()
                    {
                        Reservation = newReservation,
                        Amount = oldTransaction.Amount,
                        Caption_i18n = oldTransaction.Caption_i18n,
                        Charge = oldTransaction.Charge,
                        Payment = oldTransaction.Payment,
                        ReferenceDetails = oldTransaction.ReferenceDetails,
                        TransactionCode = oldTransaction.TransactionCode,
                        TransactionDate = oldTransaction.TransactionDate,
                    };
                    newtransaction.SetNameValue(oldTransaction.Caption.ToString());
                    _Context.ReservationTransactions.Add(newtransaction);
                }
            }
            else
            { // if new invoice is lower than old one, copy transactions and reservation details from old reservations, so new reservation will have the same cost

                foreach (var oldTransaction in currentReservation.ReservationTransactions)
                {
                    ReservationTransaction newtransaction = new ReservationTransaction()
                    {
                        Reservation = newReservation,
                        Amount = oldTransaction.Amount,
                        Caption_i18n = oldTransaction.Caption_i18n,
                        Charge = oldTransaction.Charge,
                        Payment = oldTransaction.Payment,
                        ReferenceDetails = oldTransaction.ReferenceDetails,
                        TransactionCode = oldTransaction.TransactionCode,
                        TransactionDate = oldTransaction.TransactionDate,
                    };
                    newtransaction.SetNameValue(oldTransaction.Caption.ToString());

                    if (oldTransaction.ReservationDetail != null)
                    {
                        ReservationDetail oldReservationDetails = oldTransaction.ReservationDetail;
                        ReservationDetail newReservationDetails = new ReservationDetail()
                        {
                            Base = oldReservationDetails.Base,
                            BasePrice = oldReservationDetails.BasePrice,
                            EndDate = newReservation.DateDeparture,
                            LinePrice = oldReservationDetails.LinePrice,
                            Reservation = newReservation,
                            Sequence = oldReservationDetails.Sequence,
                            StartDate = newReservation.DateArrival,
                            Type = oldReservationDetails.Type
                        };

                        newtransaction.ReservationDetail = newReservationDetails;
                        _Context.ReservationDetails.Add(newReservationDetails);

                    }

                    _Context.ReservationTransactions.Add(newtransaction);
                }
            }
        }

        private void HandleDocumentGeneration(Reservation newReservation, PropertyDetailsInvoice newReservationInvoiceDetails, decimal corectionInvoiceBalance,
            ref UserAgreement agreementDoc, ref UserAgreementDateChange agreementDateChangeDoc, ref Invoice invoiceDoc, ref TripDetails tripDetails)
        {
            string cultureCode = newReservation.Property.DictionaryCulture.CultureCode;

            if (newReservation.BookingStatus == (int)BookingStatus.ReceivedBooking)
            {
                agreementDoc = GenerateAgreementDocument(newReservation, newReservationInvoiceDetails);
                newReservation.Agreement = this._documentService.CreateDoc(agreementDoc, cultureCode);
                invoiceDoc = GenerateInvoiceDocument(newReservation, newReservationInvoiceDetails);
                this._documentService.CreateDoc(invoiceDoc, newReservation.Property.DictionaryCulture.CultureCode);
                newReservation.Invoice = this._documentService.CreateDoc(agreementDoc, cultureCode);
            }
            else if (corectionInvoiceBalance > 0)
            {
                agreementDoc = GenerateAgreementDocument(newReservation, newReservationInvoiceDetails);
                newReservation.Agreement = this._documentService.CreateDoc(agreementDoc, cultureCode);
                invoiceDoc = GenerateCorrectionInvoiceDocument(newReservation, corectionInvoiceBalance);
                newReservation.Invoice = this._documentService.CreateDoc(invoiceDoc, cultureCode);
            }
            else // invoice is lower
            {
                agreementDateChangeDoc = GenerateAgreementDateChangeDocument(newReservation, newReservationInvoiceDetails);
                newReservation.Agreement = this._documentService.CreateDoc(agreementDateChangeDoc, cultureCode);
                //invoice is copied from current reservation
            }

            tripDetails = GenerateTripDetailsDocument(newReservation, newReservationInvoiceDetails);
        }

        /// <summary>
        /// Handles all the payment gateway logic needed by reservation date change
        /// </summary>
        /// <param name="newReservation">Reservation for the new period</param>
        /// <param name="currentReservation">Reservation for the previous period</param>
        /// <param name="arrivalDateToDiff">Arrival date to comparision (most restrictive one)</param>
        /// <param name="newReservationInvoiceDetails">Recalculated invoice for changed reservation</param>
        /// <param name="corectionInvoiceBalance">Balance amount beetween new and old reservation</param>
        /// <param name="alreadyPaidSum">Amount which is already been paid by guest</param>
        private void HandlePaymentGatewayOperations(Reservation newReservation, Reservation currentReservation, DateTime arrivalDateToDiff, PropertyDetailsInvoice newReservationInvoiceDetails, decimal corectionInvoiceBalance, decimal alreadyPaidSum)
        {
            TimeSpan dateDiffFromArrivalDate = arrivalDateToDiff - DateTime.Now;

            if (newReservation.BookingStatus == (int)BookingStatus.ReceivedBooking) //it means that there is no more than 24 hours from the reservation made time
            {
                //Create new authorization
                decimal amountToAuthorize = this._bookingService.CalculateAmountToAuthorize(newReservationInvoiceDetails.InvoiceTotal, arrivalDateToDiff);

                var responseNew = this._paymentGatewayService.AuthorizePayment(
                   new TransactionRequest()
                   {
                       PaymentToken = newReservation.GuestPaymentMethod.ReferenceToken,
                       Amount = (amountToAuthorize * 100).ToString(), //convert to dolars (without decimal places)
                       CurrencyCode = newReservation.Property.DictionaryCulture.ISOCurrencySymbol
                   }, newReservation.ReservationID);

                if (responseNew == null || !responseNew.Succeded)
                {
                    throw new PaymentGatewayException("New amount authorization operation failed", null);
                }
                newReservation.TransactionToken = responseNew.Token;

                _bookingService.AddPaymentTransaction(newReservation, amountToAuthorize);

                //release old authorization
                var response = this._paymentGatewayService.CancelPayment(currentReservation.TransactionToken, currentReservation.ReservationID);
                //if this is not sucesfull it's ok because authorization will be realeased automatically
            }
            else if (dateDiffFromArrivalDate.Days > 30 && corectionInvoiceBalance > 0) //more than 30 days & invoice is greater - if invoice is equal intranet procesor will handle rest
            {
                // charge additional money to get 50% of the price
                decimal amountToCharge = (newReservation.TotalPrice / 2) - alreadyPaidSum; //get missing value to charge 50%

                var response = _paymentGatewayService.Purchase(
                   new TransactionRequest()
                   {
                       PaymentToken = newReservation.GuestPaymentMethod.ReferenceToken,
                       Amount = (amountToCharge * 100).ToString(), //convert to dolars (without decimal places)
                       CurrencyCode = newReservation.Property.DictionaryCulture.ISOCurrencySymbol
                   }, newReservation.ReservationID);

                if (response != null && response.Succeded)
                {
                    _bookingService.AddPaymentTransaction(newReservation, amountToCharge);
                }
                else
                {
                    throw new PaymentGatewayException("Purchase operation failed", null);
                }
            }
            else if (dateDiffFromArrivalDate.Days <= 30) //less or equal than 30 days but more than 24h hours from reservation
            {
                // charge 100%
                decimal amountToCharge = Math.Abs(newReservation.TripBalance);

                if (amountToCharge > 0)
                {
                    var response = _paymentGatewayService.Purchase(
                       new TransactionRequest()
                       {
                           PaymentToken = newReservation.GuestPaymentMethod.ReferenceToken,
                           Amount = (amountToCharge * 100).ToString(), //convert to dolars (without decimal places)
                           CurrencyCode = newReservation.Property.DictionaryCulture.ISOCurrencySymbol
                       }, newReservation.ReservationID);

                    if (response != null && response.Succeded)
                    {
                        _bookingService.AddPaymentTransaction(newReservation, amountToCharge);
                        newReservation.UpfrontPaymentProcessed = true;
                    }
                    else
                    {
                        throw new PaymentGatewayException("Purchase operation failed", null);
                    }
                }
            }
        }

        public UserAgreement GenerateAgreementDocument(Reservation newReservation, PropertyDetailsInvoice invoiceDetails)
        {
            UserAgreement agreement = new UserAgreement();
            agreement.DocumentType = DocumentType.PDF;
            agreement.ConfirmationCode = newReservation.ConfirmationID;

            agreement.CreatedDate = DateTime.Now.ToLocalizedDateString();
            agreement.Amenities = GetPropertyAmenities(newReservation.Unit.Property);
            agreement.CheckInDate = newReservation.DateArrival.ToLocalizedDateString();
            agreement.CheckOutDate = newReservation.DateDeparture.ToLocalizedDateString();
            agreement.GuestFullName = newReservation.User.FullName;
            agreement.GuestInitials = newReservation.User.AcceptedTCsInitials;
            agreement.GuestsNumbers = newReservation.Unit.MaxNumberOfGuests.ToString();
            agreement.OwnerFullName = newReservation.Unit.Property.User.FullName;
            agreement.OwnerInitials = !string.IsNullOrWhiteSpace(newReservation.Unit.Property.User.AcceptedTCsInitials) ? newReservation.Unit.Property.User.AcceptedTCsInitials : string.Empty;
            agreement.PaymentInfo = FormatInvoiceData(invoiceDetails);
            agreement.PropertyAddress = string.Format("{0}, {1}, {2} {3}", newReservation.Unit.Property.Address1, newReservation.Unit.Property.Address2, newReservation.Unit.Property.City, newReservation.Unit.Property.State); 
            agreement.TermsOfServiceHref = string.Format("http://{0}{1}", this._settingsService.GetSettingValue(SettingKeyName.ExtranetURL), "/Home/TermsOfService");
            return agreement;
        }

        private UserAgreementDateChange GenerateAgreementDateChangeDocument(Reservation newReservation, PropertyDetailsInvoice invoiceDetails)
        {
            UserAgreementDateChange agreement = new UserAgreementDateChange();
            agreement.DocumentType = DocumentType.PDF;
            agreement.ConfirmationCode = newReservation.ConfirmationID;

            agreement.CheckInDate = newReservation.DateArrival.ToLocalizedDateString();
            agreement.CheckOutDate = newReservation.DateDeparture.ToLocalizedDateString();
            agreement.GuestFullName = newReservation.User.FullName;
            agreement.GuestInitials = newReservation.User.AcceptedTCsInitials;
            agreement.OwnerFullName = newReservation.Unit.Property.User.FullName;
            agreement.OwnerInitials = !string.IsNullOrWhiteSpace(newReservation.Unit.Property.User.AcceptedTCsInitials) ? newReservation.Unit.Property.User.AcceptedTCsInitials : string.Empty;
            return agreement;
        }

        public Invoice GenerateInvoiceDocument(Reservation newReservation, PropertyDetailsInvoice invoiceDetails)
        {
            Invoice invoiceDoc = new Invoice();
            invoiceDoc.DocumentType = DocumentType.PDF;

            CultureInfo propertyCulture = newReservation.Property.DictionaryCulture.CultureInfo;

            invoiceDoc.ConfirmationId = newReservation.ConfirmationID;
            invoiceDoc.GuestAddress1 = newReservation.User.Address1;
            invoiceDoc.GuestAddress2 = string.Format("{0} {1} {2}, {3}", newReservation.User.Address2, newReservation.User.City, newReservation.User.State, newReservation.User.ZIPCode);
            invoiceDoc.GuestFullName = newReservation.User.FullName;
            invoiceDoc.PropertyAddress = string.Format("{0} {1}, {2}", newReservation.Property.Address1, newReservation.Property.City, newReservation.Property.State);
            invoiceDoc.PropertyName = newReservation.Property.PropertyNameCurrentLanguage;
            invoiceDoc.Total = invoiceDetails.InvoiceTotal.ToString("c", propertyCulture);
            invoiceDoc.Subtotal = (invoiceDetails.SubtotalAccomodationAddons).ToString("c", propertyCulture);

            string salesAndTaxes = string.Empty;
            foreach (var tax in invoiceDetails.TaxList)
            {
                salesAndTaxes += string.Format("<tr><td class='sumAlignLeft'>{0}</td><td class='sumAlignRight'>{1}</td></tr>",
                   tax.NameCurrentLanguage, tax.CalculatedValue.ToString("c", propertyCulture));
            }
            invoiceDoc.SalesAndTaxes = salesAndTaxes;
            invoiceDoc.CreatedDate = DateTime.Now.ToLocalizedDateString();
            invoiceDoc.InvoiceId = newReservation.ConfirmationID;
            DateTime bookingDate = newReservation.BookingDate;
            invoiceDoc.DuePercent = string.Format("{0} {1}", bookingDate.AddDays(30) < newReservation.DateArrival ? "50" : "100", Common.Resources.Common.Invoice_Document_PercentDueBy);
            invoiceDoc.DueByDate = bookingDate.AddHours(24).ToLocalizedDateString() + " " + new DateTime(bookingDate.Ticks).ToString("hh:mm tt", CultureInfo.InvariantCulture);
            invoiceDoc.SecondPartDue = DateTime.Now.AddDays(30) < newReservation.DateArrival ?
                string.Format("<tr><td class='sumAlignLeft boldRow'>50% BALANCE DUE BY " + newReservation.DateArrival.AddDays(-30).ToLocalizedDateString() + "</td><td class='sumAlignRight boldRow'>&nbsp;</td></tr>") : "<tr></tr>";

            string sevenDaysDiscount = string.Empty;
            if (invoiceDetails.TotalSevenDaysDiscount != 0)
            {
                sevenDaysDiscount = string.Format("<tr><td class='sumAlignLeft'>{0}</td><td class='sumAlignRight'>{1}</td></tr>",
                   Common.Resources.Common.Booking_7DayDiscount, invoiceDetails.TotalSevenDaysDiscount.ToString("c", propertyCulture));
            }
            invoiceDoc.SevenDayDiscount = sevenDaysDiscount;
           
            /// Generate Security Deposit section - only if it needs to be taken
            string securityDepositInfo = string.Empty;
            if (newReservation.TakeSecurityDeposit.HasValue && newReservation.TakeSecurityDeposit.Value.Equals(true))
            {
                var reservationDaysComplete = this._settingsService.GetSettingValue(SettingKeyName.ReservationCompleteAfterDaysOfDeparture);
                securityDepositInfo = "<li>" + string.Format(Common.Resources.Common.BookingInvoiceSecurityDepositBlock, newReservation.SecurityDeposit.Value) + "</li>";
            }
            invoiceDoc.SecurityDeposit = securityDepositInfo;

            DateTime mostRestrictiveArrivalDate = FindMostRestrictiveArrivalDateForComparision(newReservation, newReservation.DateArrival);
            invoiceDoc.CancellationPolicy = CancellationPolicy(mostRestrictiveArrivalDate, bookingDate, newReservation.Property.DictionaryCulture.CultureInfo);
            invoiceDoc.InvoicePositions = string.Format(invoiceDoc.PositionsTag, Common.Resources.Common.Section_Invoice_Accomodation, invoiceDetails.PricePerNight.ToString("c", propertyCulture), invoiceDetails.LengthOfStay, invoiceDetails.TotalAccomodation.ToString("c", propertyCulture));
                        
            return invoiceDoc;
        }

        private Invoice GenerateCorrectionInvoiceDocument(Reservation newReservation, decimal invoiceBalance)
        {
            Invoice invoiceDoc = new Invoice();
            invoiceDoc.DocumentType = DocumentType.PDF;

            CultureInfo propertyCulture = newReservation.Property.DictionaryCulture.CultureInfo;

            invoiceDoc.ConfirmationId = newReservation.ConfirmationID;
            invoiceDoc.GuestAddress1 = newReservation.User.Address1;
            invoiceDoc.GuestAddress2 = string.Format("{0} {1} {2}, {3}", newReservation.User.Address2, newReservation.User.City, newReservation.User.State, newReservation.User.ZIPCode);
            invoiceDoc.GuestFullName = newReservation.User.FullName;
            invoiceDoc.PropertyAddress = string.Format("{0} {1}, {2}", newReservation.Property.Address1, newReservation.Property.City, newReservation.Property.State);
            invoiceDoc.PropertyName = newReservation.Property.PropertyNameCurrentLanguage;
            invoiceDoc.Total = invoiceBalance.ToString("c", propertyCulture);
            invoiceDoc.Subtotal = invoiceBalance.ToString("c", propertyCulture);
            invoiceDoc.DueByDate = string.Empty;
            invoiceDoc.SecondPartDue = string.Empty;
            invoiceDoc.DuePercent = string.Empty;

            string salesAndTaxes = string.Empty;
            invoiceDoc.SalesAndTaxes = salesAndTaxes;
            invoiceDoc.CreatedDate = DateTime.Now.ToLocalizedDateString();
            invoiceDoc.InvoiceId = newReservation.ConfirmationID;
            DateTime bookingDate = newReservation.BookingDate;
            string sevenDaysDiscount = string.Empty;
            invoiceDoc.SevenDayDiscount = sevenDaysDiscount;

            DateTime mostRestrictiveArrivalDate = FindMostRestrictiveArrivalDateForComparision(newReservation, newReservation.DateArrival);
            invoiceDoc.CancellationPolicy = CancellationPolicy(mostRestrictiveArrivalDate, bookingDate, newReservation.Property.DictionaryCulture.CultureInfo);
            invoiceDoc.InvoicePositions = string.Format(invoiceDoc.PositionsTag, Common.Resources.Common.Section_Invoice_Correction, invoiceBalance.ToString("c", propertyCulture), 1, invoiceBalance.ToString("c", propertyCulture));

            return invoiceDoc;
        }

        /// <summary>
        /// Generates Trip details document based on given data
        /// </summary>
        /// <param name="newReservation">Reservation data</param>
        /// <returns>instance of TripDetails class</returns>
        public TripDetails GenerateTripDetailsDocument(Reservation newReservation, PropertyDetailsInvoice invoiceDetails)
        {
            TripDetails tripDetails = new TripDetails();
            tripDetails.DocumentType = DocumentType.PDF;
            tripDetails.GuestFullName = string.Format("{0}", newReservation.User.Firstname);
            tripDetails.BookedDate = DateTime.Now.ToString("g");
            tripDetails.CheckIn = newReservation.DateArrival.ToLongDateString();
            tripDetails.CheckOut = newReservation.DateDeparture.ToLongDateString();
            tripDetails.CheckInTime = new DateTime(newReservation.Property.CheckIn.Ticks).ToString("t");
            tripDetails.CheckOutTime = new DateTime(newReservation.Property.CheckOut.Ticks).ToString("t");
            tripDetails.ConfirmationCode = newReservation.ConfirmationID;
            tripDetails.DateBeforeArrival = newReservation.DateArrival.AddDays(-2).ToLocalizedDateString();
            tripDetails.Directions = "";
            tripDetails.GuestFullName = string.Format("{0} {1}", newReservation.User.Firstname, newReservation.User.Lastname);
            tripDetails.GuestsNumber = newReservation.NumberOfGuests.ToString();
            tripDetails.CardinalityPeron = newReservation.NumberOfGuests > 1 ? Common.Resources.Common.Cardinality_Persons.ToLower() : Common.Resources.Common.Cardinality_Person.ToLower();
            tripDetails.Latitude = newReservation.Property.Latitude.ToString().Replace(",", ".");
            tripDetails.Longitude = newReservation.Property.Longitude.ToString().Replace(",", ".");
            tripDetails.MaxNoOfGuests = newReservation.Unit.MaxNumberOfGuests.ToString();
            tripDetails.NightsNumber = invoiceDetails.LengthOfStay.ToString();
            tripDetails.PropertyAddress = string.Format("{0}, {1}, {2} {3}", newReservation.Property.Address1, newReservation.Property.Address2, newReservation.Property.City, newReservation.Property.State);
            tripDetails.PropertyCountry = newReservation.Property.DictionaryCountry.CountryNameCurrentLanguage;
            tripDetails.GuestEmail = newReservation.User.email;
            tripDetails.PropertyName = newReservation.Property.PropertyNameCurrentLanguage; 
            tripDetails.TermsOfServiceHref = string.Format("http://{0}{1}", this._settingsService.GetSettingValue(SettingKeyName.ExtranetURL), "/Home/TermsOfService");

            DateTime mostRestrictiveArrivalDate = FindMostRestrictiveArrivalDateForComparision(newReservation, newReservation.DateArrival);
            tripDetails.CancellationPolicy = this.CancellationPolicy(mostRestrictiveArrivalDate, newReservation.BookingDate, newReservation.Property.DictionaryCulture.CultureInfo);

            var psc = this._propertiesService.GetPropertyStaticContentByType(newReservation.Property.PropertyID, PropertyStaticContentType.ParkingInformation).SingleOrDefault();
            tripDetails.OnStreetParkingAvailable = psc != null ? psc.ContentValueCurrentLanguage : "";

            var directions = this._propertiesService.GetPropertyDirectionsToAirport(newReservation.Property.PropertyID);
            foreach (DirectionToAirport direction in directions)
            {
                tripDetails.Directions += string.Format("<a href='{0}'>{1}</a><br />", direction.DirectionLink, direction.Name.ToString());
            }

            return tripDetails;
        }

        /// <summary>
        /// Format data about invoice for Agreement document purpose
        /// </summary>
        /// <param name="propertyDetailsInvoice">Invoice Data</param>
        /// <returns>Invoice data summary</returns>
        private string FormatInvoiceData(PropertyDetailsInvoice invoice)
        {
            CultureInfo propertyCulture = invoice.Culture;
            StringBuilder sb = new StringBuilder();
            sb.Append(Common.Resources.Common.Section_Invoice_TotalAccomodation);
            sb.Append(": " + invoice.TotalAccomodation.ToString("c", propertyCulture));
            sb.Append("<br />");

            sb.Append(Common.Resources.Common.Section_Invoice_Nights);
            sb.Append(": " + invoice.LengthOfStay);
            sb.Append("<br />");

            sb.Append(Common.Resources.Common.Section_Invoice_PricePerNight);
            sb.Append(": " + invoice.PricePerNight.ToString("c", propertyCulture));
            sb.Append("<br />");

            foreach (Tax tax in invoice.TaxList)
            {
                sb.Append(tax.NameCurrentLanguage);
                sb.Append(": " + tax.CalculatedValue.ToString("c", propertyCulture));
                sb.Append("<br />");
            }

            sb.Append(Common.Resources.Common.Section_Invoice_Total);
            sb.Append(": " + invoice.InvoiceTotal.ToString("c", propertyCulture));
            sb.Append("<br />");
            return sb.ToString();
        }

        /// <summary>
        /// Gets all amenities for property and join titles
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        private string GetPropertyAmenities(Property property)
        {
            string result = "";
            foreach (PropertyAmenity pa in property.PropertyAmenities)
            {
                result += pa.Amenity.TitleCurrentLanguage + ", ";
            }
            result = result.Trim().TrimEnd(',');
            return result;
        }

        /// <summary>
        /// Returns cancellation text depending on Trip start and booked date
        /// </summary>
        /// <param name="start">The date of trip start</param>
        /// <param name="bookingDate">The date, when booking was made</param>
        /// <returns>Text about posibility of booking cancellation</returns>
        public string CancellationPolicy(DateTime start, DateTime bookingDate, CultureInfo propertyCultureInfo)
        {
            string cancellationText;

            double diffrenceBetweenArrivalAndNow = (start - DateTime.Now).TotalDays;
            double diffrenceBetweenBookingAndNow = (DateTime.Now - bookingDate).TotalHours;
            double diffrenceBetweenBookingAndArrival = (start - bookingDate).TotalHours;
            DateTime h24AfterBooking = bookingDate.AddHours(24);
            DateTime d30BeforeArrival = start.AddDays(-30);

            if (diffrenceBetweenArrivalAndNow >= 30)
            {
                if (diffrenceBetweenBookingAndNow <= 24)
                {
                    cancellationText = String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", propertyCultureInfo));
                }
                else
                {
                    cancellationText = String.Format(Common.Resources.CancellationPolicy.CancellationPolicyHoursHalfRefund, d30BeforeArrival.ToString("hh:mm tt, MMMM dd, yyyy", propertyCultureInfo));
                }
            }
            else
                if (diffrenceBetweenArrivalAndNow >= 15)
                {
                    if (diffrenceBetweenBookingAndNow <= 24)
                    {
                        cancellationText = String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", propertyCultureInfo));
                    }
                    else
                    {
                        cancellationText = Common.Resources.CancellationPolicy.CancellationPolicyTryingToResell;
                    }
                }
                else
                {
                    cancellationText = Common.Resources.CancellationPolicy.CancellationPolicyTryingToResell;
                }
            return cancellationText;
        }

        public List<KeyManagementData> GetKeyManagementReservationsByHomeowner(int ownerId)
        {
            int bookingStatusFinalized = (int)BookingStatus.FinalizedBooking;
            int bookingStatusCheckIn = (int)BookingStatus.CheckedIn;
            int bookingStatusCheckOut = (int)BookingStatus.CheckedOut;

            var rawData = GetReservationsByHomeowner(ownerId);
            var query = rawData.Where(reservation =>
                            (
                                reservation.BookingStatus == bookingStatusFinalized
                                || reservation.BookingStatus == bookingStatusCheckIn
                                || reservation.BookingStatus == bookingStatusCheckOut
                            )).Select(reservation => new KeyManagementData()
                            {
                                Guest = reservation.User,
                                Property = reservation.Property,
                                Reservation = reservation,
                                Unit = reservation.Unit
                            });
            return query.ToList();
        }

        public List<KeyManagementData> GetKeyManagementReservationsByManagerRole(int userId, RoleLevel managerRole)
        {
            int bookingStatusFinalized = (int)BookingStatus.FinalizedBooking;
            int bookingStatusCheckIn = (int)BookingStatus.CheckedIn;
            int bookingStatusCheckOut = (int)BookingStatus.CheckedOut;
            int assignedManagerRole = (int)managerRole;

            var rawData = GetReservationsByManagerRole(userId, managerRole);
            var query = rawData.Where(reservation =>
                            (
                                reservation.BookingStatus == bookingStatusFinalized
                                || reservation.BookingStatus == bookingStatusCheckIn
                                || reservation.BookingStatus == bookingStatusCheckOut
                            )).Select(reservation => new KeyManagementData()
                            {
                                Guest = reservation.User,
                                Property = reservation.Property,
                                Reservation = reservation,
                                Unit = reservation.Unit
                            });
            return query.ToList();
        }


        public IQueryable<Reservation> GetReservationsByHomeowner(int ownerId)
        {
            var query = from property in _Context.Properties
                        from reservation in property.Reservations
                        where
                            property.User.UserID.Equals(ownerId)
                            && property.PropertyLive.Equals(true)
                        orderby reservation.DateArrival ascending
                        select reservation;
            return query;
        }

        public IQueryable<Reservation> GetReservationsByManagerRole(int userId, RoleLevel managerRole)
        {
            int assignedManagerRole = (int)managerRole;

            var query = from property in _Context.Properties
                        from reservation in property.Reservations
                        from assignedManager in property.PropertyAssignedManagers
                        where
                            property.PropertyLive.Equals(true)
                            && assignedManager.User.UserID.Equals(userId) && assignedManager.Role.RoleLevel.Equals(assignedManagerRole)
                        orderby reservation.DateArrival ascending
                        select reservation;
            return query;
        }

        public bool? SetNewArrivalTimeForReservation(int reservationId, string reservationToken, int arrivalTime)
        {
            bool? replaced = null;
            var reservation = _Context.Reservations.Where(r => r.ReservationID == reservationId &&
                                    r.LinkAuthorizationToken.ToLower() == reservationToken.ToLower()).SingleOrDefault();
            if (reservation != null)
            {
                replaced = !reservation.EstimatedArrivalTime.HasValue;
                reservation.EstimatedArrivalTime = arrivalTime;
            }
            return replaced;
        }

        public void DropOffKeyByGuest(int reservationId)
        {
            var reservation = _Context.Reservations.Where(r => r.ReservationID == reservationId).SingleOrDefault();
            reservation.GuestConfirmedKeyDropOff = true;
        }

        public void SendCheckInLockboxEmail(Reservation reservation)
        {
            string extranetUrl = _settingsService.GetSettingValue(SettingKeyName.ExtranetURL);
            CheckInLockboxEmail email = new CheckInLockboxEmail(reservation.User.email);

            email.AlarmCode = !string.IsNullOrWhiteSpace(reservation.Unit.AlarmCode) ?
                string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Alarm Code", reservation.Unit.AlarmCode) : string.Empty;

            email.AlarmLocation = !string.IsNullOrWhiteSpace(reservation.Unit.AlarmLocation) ?
                string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Alarm Location", reservation.Unit.AlarmLocation) : string.Empty;

            email.ArrivalDate = reservation.DateArrival.ToLocalizedDateString();

            string timeRanges = string.Empty;
            foreach (var tr in ArrivalTimeRangeExtensions.GetAllValuesWithKeys())
            {
                timeRanges += string.Format("<a href=\"{0}\">{1}</a><br />",
                                    string.Format("http://{0}/Guest/ConfirmArrivalTime?dId={1}&t={2}&rId={3}", extranetUrl, tr.Key.ToString(), reservation.LinkAuthorizationToken, reservation.ReservationID.ToString()), tr.Value);
            }
            email.ArrivalTimeRanges = timeRanges;
            email.CheckIn = new DateTime(reservation.Property.CheckIn.Ticks).ToString("t");
            email.ExtranetUrl = string.Format("http://{0}", extranetUrl);
            email.GuestFirstName = reservation.User.Firstname;
            email.Latitude = reservation.Property.Latitude.ToString().Replace(",", ".");
            email.Longitude = reservation.Property.Longitude.ToString().Replace(",", ".");
            email.LockboxCode = reservation.Unit.KeysLockboxCode;
            email.LockboxLocation = reservation.Unit.KeysLockboxLocation;
            email.OtherAccessInformation = !string.IsNullOrWhiteSpace(reservation.Unit.OtherAccessInformation) ?
                string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Other Access Information", reservation.Unit.OtherAccessInformation) : "";
            email.PropertyAddress = string.Format("{0}, {1}, {2} {3}", reservation.Unit.Property.Address1, reservation.Unit.Property.Address2, reservation.Unit.Property.City, reservation.Unit.Property.State);
            email.PropertyName = reservation.Property.PropertyNameCurrentLanguage;

            var wifiName = this._unitsService.GetUnitStaticContentByType(reservation.Unit.UnitID, UnitStaticContentType.WifiName).SingleOrDefault();
            email.WifiName = wifiName != null ? wifiName.ContentValue : string.Empty;

            var wifiPassword = this._unitsService.GetUnitStaticContentByType(reservation.Unit.UnitID, UnitStaticContentType.WifiPassword).SingleOrDefault();
            email.WifiPassword = wifiPassword != null ? Base64.Decode(wifiPassword.ContentValue) : string.Empty;
            email.CheckInLinkUrl = string.Format("http://{0}/Guest/ConfirmCheckIn?t={1}&rId={2}", extranetUrl, reservation.LinkAuthorizationToken, reservation.ReservationID.ToString());
            _messageService.AddEmail(email);
        }

        public void SendCheckInPickUpKeyHandlerEmail(Reservation reservation)
        {
            string extranetUrl = _settingsService.GetSettingValue(SettingKeyName.ExtranetURL);
            CheckInPickupKeyHandlerEmail email = new CheckInPickupKeyHandlerEmail(reservation.User.email);

            email.AlarmCode = !string.IsNullOrWhiteSpace(reservation.Unit.AlarmCode) ?
                string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Alarm Code", reservation.Unit.AlarmCode) : string.Empty;

            email.AlarmLocation = !string.IsNullOrWhiteSpace(reservation.Unit.AlarmLocation) ?
                string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Alarm Location", reservation.Unit.AlarmLocation) : string.Empty;

            email.ArrivalDate = reservation.DateArrival.ToLocalizedDateString();

            string timeRanges = string.Empty;
            foreach (var tr in ArrivalTimeRangeExtensions.GetAllValuesWithKeys())
            {
                timeRanges += string.Format("<a href=\"{0}\">{1}</a><br />",
                                    string.Format("http://{0}/Guest/ConfirmArrivalTime?dId={1}&t={2}&rId={3}", extranetUrl, tr.Key.ToString(), reservation.LinkAuthorizationToken, reservation.ReservationID.ToString()), tr.Value);
            }
            email.ArrivalTimeRanges = timeRanges;
            email.CheckIn = new DateTime(reservation.Property.CheckIn.Ticks).ToString("t");
            email.ExtranetUrl = string.Format("http://{0}", extranetUrl);
            email.GuestFirstName = reservation.User.Firstname;
            email.Latitude = reservation.Property.Latitude.ToString().Replace(",", ".");
            email.Longitude = reservation.Property.Longitude.ToString().Replace(",", ".");
            var keyManager = this._propertiesService.GetPropertyManagerByType(reservation.Property.PropertyID, RoleLevel.KeyManager);
            if (keyManager != null && keyManager.User != null)
            {
                email.KeyManagerFullName = string.Format("{0} {1}", keyManager.User.Firstname, keyManager.User.Lastname);
                email.KeyManagerAddress = string.Format("{0}, {1}, {2} {3}", keyManager.User.Address1, keyManager.User.Address2, keyManager.User.City, keyManager.User.State);
            }
            email.OtherAccessInformation = !string.IsNullOrWhiteSpace(reservation.Unit.OtherAccessInformation) ?
                string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Other Access Information", reservation.Unit.OtherAccessInformation) : "";
            email.PropertyAddress = string.Format("{0}, {1}, {2} {3}", reservation.Unit.Property.Address1, reservation.Unit.Property.Address2, reservation.Unit.Property.City, reservation.Unit.Property.State);
            email.PropertyName = reservation.Property.PropertyNameCurrentLanguage;

            var wifiName = this._unitsService.GetUnitStaticContentByType(reservation.Unit.UnitID, UnitStaticContentType.WifiName).SingleOrDefault();
            email.WifiName = wifiName != null ? wifiName.ContentValue : string.Empty;

            var wifiPassword = this._unitsService.GetUnitStaticContentByType(reservation.Unit.UnitID, UnitStaticContentType.WifiPassword).SingleOrDefault();
            email.WifiPassword = wifiPassword != null ? Base64.Decode(wifiPassword.ContentValue) : string.Empty;
            email.CheckInLinkUrl = string.Format("http://{0}/Guest/ConfirmCheckIn?t={1}&rId={2}", extranetUrl, reservation.LinkAuthorizationToken, reservation.ReservationID.ToString());

            _messageService.AddEmail(email);
        }

        public void SetReservationStatus(BookingStatus status, int reservationId)
        {
            var reservation = _Context.Reservations.Where(r => r.ReservationID == reservationId).SingleOrDefault();
            reservation.BookingStatus = (int)status;
        }

        public IEnumerable<Reservation> GetRervationsByBookingStatus(BookingStatus status)
        {
            return _Context.Reservations.Where(r => r.BookingStatus == (int)status).AsEnumerable();
        }
    }
}
