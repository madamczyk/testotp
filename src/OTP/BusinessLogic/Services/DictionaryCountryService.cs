﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Services class for managing dictionary with countries. Contains basic CRUD operations.
    /// </summary>
    public class DictionaryCountryService : BaseService, IDictionaryCountryService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryCountryService"/> class.
        /// </summary>
        public DictionaryCountryService(IContextService httpContextService)
            : base(httpContextService)
        { }

         /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryCountryService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public DictionaryCountryService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Gets all countries
        /// </summary>
        /// <returns>List of all countries</returns>
        public IEnumerable<DictionaryCountry> GetCountries()
        {
            return _Context.DictionaryCountries.AsEnumerable().OrderBy(p => p.CountryNameCurrentLanguage);
        }

        /// <summary>
        /// Get Country by id
        /// </summary>
        /// <param name="countryId">Country Id</param>
        /// <returns></returns>
        public DictionaryCountry GetCountryById(int countryId)
        {
            return _Context.DictionaryCountries.AsEnumerable().Where(c => c.CountryId == countryId).SingleOrDefault();
        }

        /// <summary>
        /// Get Country by 2 letter code, according to ISO 3166
        /// </summary>
        /// <param name="countryCode">2 letter country code</param>
        /// <returns></returns>
        public DictionaryCountry GetCountryByCountryCode(string countryCode)
        {
            return _Context.DictionaryCountries.AsEnumerable().Where(c => c.CountryCode2Letters == countryCode).SingleOrDefault();
        }
    }
}
