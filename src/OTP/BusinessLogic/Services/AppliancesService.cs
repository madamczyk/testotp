﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Services class for managing appliances. Contains basic CRUD operations.
    /// </summary>
    public class AppliancesService : BaseService, IAppliancesService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppliancesService"/> class.
        /// </summary>zr
        public AppliancesService(IContextService httpContextService)
            : base(httpContextService)
        { }        

         /// <summary>
        /// Initializes a new instance of the <see cref="AppliancesService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public AppliancesService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Gets all appliances groups
        /// </summary>
        /// <returns>List of all appliances groups</returns>
        public IEnumerable<ApplianceGroup> GetApplianceGroups()
        {
            return _Context.ApplianceGroups.AsEnumerable().OrderBy(p => p.GroupNameCurrentLanguage).AsEnumerable();
        }

        /// <summary>
        /// Gets appliance by id
        /// </summary>
        /// <param name="applianceId"></param>
        /// <returns></returns>
        public Appliance GetApplianceById(int applianceId)
        {
            return _Context.Appliances.Where(a => a.ApplianceId == applianceId).FirstOrDefault();
        }

        /// <summary>
        /// Gets appliance by group id
        /// </summary>
        /// <param name="applianceId"></param>
        /// <returns></returns>
        public IEnumerable<Appliance> GetApplianceByGroupId(int applianceGroupId)
        {
            return _Context.Appliances.Where(a => a.ApplianceGroup.ApplianceGroupId == applianceGroupId).AsEnumerable();
        }

        /// <summary>
        /// Gets all appliances
        /// </summary>
        /// <returns>List of all appliances</returns>
        public IEnumerable<Appliance> GetAppliances()
        {
            return _Context.Appliances.AsEnumerable().OrderBy(p => p.NameCurrentLanguage);
        }
    }
}
