﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using DataAccess.Enums;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class PropertyAddOnsService : BaseService, IPropertyAddOnsService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyAddOnsService"/> class.
        /// </summary>
        public PropertyAddOnsService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyAddOnsService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public PropertyAddOnsService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Gets all property add ons
        /// </summary>
        /// <returns>List of all languages</returns>
        public IEnumerable<PropertyAddOn> GetAddOnsByPropertyId(int pid)
        {
            return _Context.PropertyAddOns.Where(pa => pa.Property.PropertyID == pid).OrderBy(p => p.PropertyAddOnID).AsEnumerable();
        }
        
        /// <summary>
        /// Gets property add on by its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PropertyAddOn GetById(int id)
        {
            return _Context.PropertyAddOns.Where(a => a.PropertyAddOnID == id).FirstOrDefault();
        }

        /// <summary>
        /// Adds new property addOn
        /// </summary>
        /// <param name="addOn"></param>
        public void Add(PropertyAddOn addOn)
        {
            _Context.PropertyAddOns.Add(addOn);
            _Context.SaveChanges();
        }

        /// <summary>
        /// Remove property Add On by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void DeleteAddOn(int id)
        {
            PropertyAddOn addOn = _Context.PropertyAddOns.Where(p => p.PropertyAddOnID == id).SingleOrDefault();
            try
            {
                _Context.PropertyAddOns.Remove(addOn);
                _Context.SaveChanges();
            }
            catch(Exception ex)
            {
                throw new Exception("Can't remove property add on with id" + id.ToString(), ex);
            }
        }
    }
}