﻿using BusinessLogic.Enums;
using DataAccess;
using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Provides operations on OTP Settings in the database.
    /// </summary>
    public class ConfigurationService : BaseService, IConfigurationService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryLanguageService"/> class.
        /// </summary>
        public ConfigurationService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
		/// Initializes a new instance of the <see cref="ConfigurationService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
		public ConfigurationService(OTPEntities ctx)
            : base(ctx)
		{ }

        public string GetKeyValue(CloudConfigurationKeys key)
        {
            if (IsCloudEnvironmentAvailable())
            {
                return RoleEnvironment.GetConfigurationSettingValue(key.ToString());
            }
            else
            {
                return ConfigurationManager.AppSettings[key.ToString()] ?? ConfigurationManager.AppSettings[key.ToString()].ToString();
            }
        }

        /// <summary>
        /// Checks if Azure environement is available
        /// </summary>
        /// <returns></returns>
        public bool IsCloudEnvironmentAvailable()
        {
            return RoleEnvironment.IsAvailable;
        }

        /// <summary>
        /// Checks if Azure environement is emulated
        /// </summary>
        /// <returns></returns>
        public bool IsCloudEnvironmentEmulated()
        {
            return RoleEnvironment.IsEmulated;
        }
    }
}
