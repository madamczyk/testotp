﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class UnitTypesService : BaseService, IUnitTypesService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTypesService"/> class.
        /// </summary>
        public UnitTypesService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTypesService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public UnitTypesService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Gets unit types by property id
        /// </summary>
        /// <param name="id">internal id of the property id</param>
        /// <returns>Tax retrieved</returns>
        public IEnumerable<UnitType> GetUnitTypesByPropertyId(int id)
        {
            try
            {
                return this._Context.UnitTypes.Where(p => p.Property.PropertyID == id).AsEnumerable();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get unit type with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets unit type by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UnitType GetUnitTypeById(int id)
        {
            return _Context.UnitTypes.Where(u => u.UnitTypeID == id).FirstOrDefault();
        }

        /// <summary>
        /// Add Tax
        /// </summary>
        public void AddUnitType(UnitType unitType)
        {
            try
            {
                _Context.UnitTypes.Add(unitType);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Unit Type", ex);
            }
        }
        
        /// <summary>
        /// Delete unit type
        /// </summary>
        /// <param name="id"></param>
        public void DeleteUnitType(int id)
        {
            try
            {
                UnitType ut = this.GetUnitTypeById(id);
                _Context.UnitTypes.Remove(ut);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove unit type with id" + id.ToString(), ex);
            }
        }

        /// <summary>
        /// Gets 7 days unit type discounts
        /// </summary>
        /// <param name="p">Unit Type ID</param>
        /// <returns>List of discounts</returns>
        public IEnumerable<UnitType7dayDiscounts> Get7DayUnitTypeDiscounts(int unitTypeId)
        {
            return _Context.UnitType7dayDiscounts.Where(d => d.UnitType.UnitTypeID == unitTypeId).AsEnumerable();
        }

        /// <summary>
        /// Gets unit type 7 day discount by id
        /// </summary>
        /// <param name="id">Unit Type 7 day Discount ID</param>
        /// <returns>UnitType7dayDiscounts object</returns>
        public UnitType7dayDiscounts Get7DayUnitTypeDiscountById(int id)
        {
            return _Context.UnitType7dayDiscounts.Where(d => d.UnitType7dayID == id).FirstOrDefault();
        }

        /// <summary>
        /// Add 7 day discount
        /// </summary>
        /// <param name="newUnit"></param>
        public void AddUnitType7DayDiscount(UnitType7dayDiscounts discount)
        {
            try
            {
                _Context.UnitType7dayDiscounts.Add(discount);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Unit Discount", ex);
            }
        }

        /// <summary>
        /// Deletes discount
        /// </summary>
        /// <param name="unitTypeID"></param>
        /// <returns></returns>
        public void DeleteUnitType7DaysDiscount(int id)
        {
            try
            {
                _Context.UnitType7dayDiscounts.Remove(Get7DayUnitTypeDiscountById(id));
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove unit type 7 discount with id" + id.ToString(), ex);
            }
        }

        /// <summary>
        /// Gets discount
        /// </summary>
        /// <param name="discountId">Discount ID</param>
        /// <returns></returns>
        public UnitType7dayDiscounts GetUnitType7DaysDiscount(int discountId)
        {
            return _Context.UnitType7dayDiscounts.Where(d => d.UnitType7dayID == discountId).FirstOrDefault();
        }

        /// <summary>
        /// Get unit type - minimum len. of stay
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UnitTypeMLO GetUnitTypeMlosById(int id)
        {
            return _Context.UnitTypeMLOS.Where(i => i.UnitTypeMLOSID == id).FirstOrDefault();
        }

        /// <summary>
        /// Deletes unit type mlos setting
        /// </summary>
        /// <param name="mlosID"></param>
        /// <returns></returns>
        public void DeleteUnitTypeMlos(int mlosID)
        {
            try
            {
                _Context.UnitTypeMLOS.Remove(GetUnitTypeMlosById(mlosID));
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove unit type with id" + mlosID.ToString(), ex);
            }
        }

        /// <summary>
        /// Add 7 day discount
        /// </summary>
        /// <param name="newUnit"></param>
        public void AddUnitTypeMlos(UnitTypeMLO mlos)
        {
            try
            {
                _Context.UnitTypeMLOS.Add(mlos);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add MLOS", ex);
            }
        }

        /// <summary>
        /// Gets unit type mlos list
        /// </summary>
        /// <param name="id">Unit Type 7 day Discount ID</param>
        /// <returns>UnitType7dayDiscounts object</returns>
        public IEnumerable<UnitTypeMLO> GetUnitTypeMlos(int unitTypeId)
        {
            return _Context.UnitTypeMLOS.Where(m => m.UnitType.UnitTypeID == unitTypeId).AsEnumerable();
        }

        /// <summary>
        /// Returns unit type by unit id
        /// </summary>
        /// <param name="unitId">The unit id</param>
        /// <returns></returns>
        public UnitType GetUnitTypeMlosByUnitId(int unitId)
        {
            var unit = _Context.Units.Where(u => u.UnitID == unitId).FirstOrDefault();
            var unitType = _Context.UnitTypes.Where(ut => ut.UnitTypeID == unit.UnitType.UnitTypeID).FirstOrDefault();

            return unitType;
        }

        /// <summary>
        /// Gets unit type cta
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public IEnumerable<UnitTypeCTA> GetUnitTypeCta(int p)
        {
            return _Context.UnitTypeCTAs.Where(i => i.UnitType.UnitTypeID == p);
        }

        /// <summary>
        /// Gets unit type cta by id of the unit type and date of arrival
        /// </summary>
        /// <param name="unitId">Id of the unit</param>
        /// <param name="dateArrival">Date of the guest arrival</param>
        /// <returns></returns>
        public IEnumerable<UnitTypeCTA> GetUnitTypeCtaByDate(int unitId, DateTime dateArrival)
        {
            DayOfWeek dayOfWeek = dateArrival.DayOfWeek;

            IQueryable<UnitTypeCTA> q;
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    q = _Context.Units.Where(u => u.UnitID.Equals(unitId))
                        .SelectMany(u => u.UnitType.UnitTypeCTAs)
                        .Where(utc => (dateArrival >= utc.DateFrom && dateArrival <= utc.DateUntil && utc.Monday));
                    break;
                case DayOfWeek.Tuesday:
                    q = _Context.Units.Where(u => u.UnitID.Equals(unitId))
                        .SelectMany(u => u.UnitType.UnitTypeCTAs)
                        .Where(utc => (dateArrival >= utc.DateFrom && dateArrival <= utc.DateUntil && utc.Tuesday));
                    break;
                case DayOfWeek.Wednesday: 
                    q = _Context.Units.Where(u => u.UnitID.Equals(unitId))
                        .SelectMany(u => u.UnitType.UnitTypeCTAs)
                        .Where(utc => (dateArrival >= utc.DateFrom && dateArrival <= utc.DateUntil && utc.Wednesday));
                    break;
                case DayOfWeek.Thursday: 
                    q = _Context.Units.Where(u => u.UnitID.Equals(unitId))
                        .SelectMany(u => u.UnitType.UnitTypeCTAs)
                        .Where(utc => (dateArrival >= utc.DateFrom && dateArrival <= utc.DateUntil && utc.Thursday));
                    break;
                case DayOfWeek.Friday: 
                    q = _Context.Units.Where(u => u.UnitID.Equals(unitId))
                        .SelectMany(u => u.UnitType.UnitTypeCTAs)
                        .Where(utc => (dateArrival >= utc.DateFrom && dateArrival <= utc.DateUntil && utc.Friday));
                    break;
                case DayOfWeek.Saturday: 
                    q = _Context.Units.Where(u => u.UnitID.Equals(unitId))
                        .SelectMany(u => u.UnitType.UnitTypeCTAs)
                        .Where(utc => (dateArrival >= utc.DateFrom && dateArrival <= utc.DateUntil && utc.Saturday));
                    break;
                case DayOfWeek.Sunday:
                    q = _Context.Units.Where(u => u.UnitID.Equals(unitId))
                        .SelectMany(u => u.UnitType.UnitTypeCTAs)
                        .Where(utc => (dateArrival >= utc.DateFrom && dateArrival <= utc.DateUntil && utc.Sunday));
                    break;
                default: 
                    q = _Context.Units.Where(u => u.UnitID.Equals(unitId))
                        .SelectMany(u => u.UnitType.UnitTypeCTAs)
                        .Where(utc => (dateArrival >= utc.DateFrom && dateArrival <= utc.DateUntil));
                    break;
            }
            return q.AsEnumerable();
        }




        /// <summary>
        /// Adds new unit type CTA
        /// </summary>
        /// <param name="newUnit"></param>
        public void AddUnitTypeCta(UnitTypeCTA cta)
        {
            _Context.UnitTypeCTAs.Add(cta);
        }




        /// <summary>
        /// Gets unit type cta by its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UnitTypeCTA GetUnitTypeCtaById(int id)
        {
            return _Context.UnitTypeCTAs.Where(c => c.UnitTypeCTAID == id).FirstOrDefault();
        }
        
        /// <summary>
        /// Delete unit type cta by id
        /// </summary>
        /// <param name="ctaId"></param>
        /// <returns></returns>
        public void DeleteUnitTypeCta(int ctaId)
        {
            try
            {
                _Context.UnitTypeCTAs.Remove(GetUnitTypeCtaById(ctaId));
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't remove unit type cta with id" + ctaId.ToString(), ex);
            }
        }

        public UnitType GetUnitTypeForPropertyByCode(int propertyId, string code)
        {
            return _Context.UnitTypes.Where(pc => pc.UnitTypeCode.ToLower() == code.ToLower().Trim() && pc.Property.PropertyID == propertyId).FirstOrDefault();
        }
    }
}

