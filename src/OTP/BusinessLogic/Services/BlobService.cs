﻿using BusinessLogic.Enums;
using BusinessLogic.Objects;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class BlobService : BaseService, IBlobService
    {
        private readonly string _connectionString;

        private static CloudStorageAccount _account;
        private static CloudBlobClient _blobClient;
        private static CloudBlobContainer _blobContainer;

        public BlobService(IContextService httpContextService)
            : base(httpContextService)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageTableService"/> class.
        /// </summary>
        public BlobService(IConfigurationService configurationService, IContextService httpContextService)
            : base(httpContextService, new Dictionary<Type, object>() {{ 
                typeof(IConfigurationService), configurationService }})
        {
            _connectionString = this._configurationService.GetKeyValue(CloudConfigurationKeys.StorageConnectionString);

            InitializeBlob();
        }

        /// <summary>
        /// Initializes the blob storage
        /// </summary>
        private void InitializeBlob()
        {
            if (CloudStorageAccount.TryParse(_connectionString, out _account))
            {
                _blobClient = _account.CreateCloudBlobClient();
                _blobClient.RetryPolicy = RetryPolicies.Retry(5, TimeSpan.FromSeconds(3));
            }
        }

        public void InitContainer(string name)
        {
            if (_blobContainer == null || _blobContainer.Name != name)
            {
                _blobContainer = _blobClient.GetContainerReference(name);
                _blobContainer.CreateIfNotExist();
                _blobContainer.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Container });
            }
        }

        /// <summary>
        /// Adds file to the container in subfolder named id<id of propertyy>
        /// </summary>
        public string AddFile(Stream s, int id, string fileName, string contentType = "")
        {
            CloudBlockBlob blockBlob = _blobContainer.GetBlockBlobReference(string.Format("id{0}/{1}", id.ToString(), fileName));

            if (!String.IsNullOrWhiteSpace(contentType))
                blockBlob.Properties.ContentType = contentType;

            blockBlob.UploadFromStream(s);
            return blockBlob.Uri.AbsolutePath;
        }

        /// <summary>
        /// Deletes block blob file
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileName"></param>
        public void DeleteFile(int id, string fileName)
        {
            CloudBlockBlob blockBlob = _blobContainer.GetBlockBlobReference(string.Format("id{0}/{1}", id.ToString(), fileName));
            if (FileExists(id, fileName))
            {
                blockBlob.Delete();
            }
        }

        /// <summary>
        /// Download file by Url path from Azure storage
        /// </summary>
        /// <param name="fileRelativePath"></param>
        /// <returns></returns>
        public Stream DownloadFileByUrl(string fileRelativePath, string container)
        {
            MemoryStream ms = new MemoryStream();
            InitContainer(container);
            fileRelativePath = fileRelativePath.Replace(container, "").TrimStart("//".ToCharArray());
            CloudBlockBlob blockBlob = _blobContainer.GetBlockBlobReference(fileRelativePath);
            try
            {
                blockBlob.FetchAttributes();

            }
            catch (StorageClientException e)
            {
                if (e.ErrorCode == StorageErrorCode.ResourceNotFound)
                {
                    return null;
                }
                throw;
            }
            blockBlob.DownloadToStream(ms);
            ms.Position = 0;         
           
            return ms;
        }

        /// <summary>
        /// Determines if file exists
        /// </summary>
        /// <param name="fileRelativePath"></param>
        /// <param name="container"></param>
        /// <returns></returns>
        public bool FileExists(int id, string fileName)
        {
            CloudBlockBlob blockBlob = _blobContainer.GetBlockBlobReference(string.Format("id{0}/{1}", id.ToString(), fileName));
            try
            {
                blockBlob.FetchAttributes();
                return true;
            }
            catch (StorageClientException e)
            {
                if (e.ErrorCode == StorageErrorCode.ResourceNotFound)
                {
                    return false;
                }
                throw;
            }
        }

        /// <summary>
        /// Gets all files (absolute paths) in container
        /// </summary>
        /// <returns>list of files (string)</returns>
        public List<string> GetFiles()
        {
            var listOptions = new BlobRequestOptions()
            {
                UseFlatBlobListing = true
            };

            var blobItems = _blobContainer.ListBlobs(listOptions);

            return blobItems.Select(b => Uri.UnescapeDataString(b.Uri.AbsolutePath).Trim()).ToList();
        }

        /// <summary>
        /// Deletes block blob file
        /// </summary>
        /// <param name="fileName">Name of the file</param>
        /// <returns>Value indicating, whether deletion succeeded </returns>
        public bool DeleteFile(string fileName, bool removeContainerName = false)
        {
            if (removeContainerName)
                fileName = fileName.Substring(_blobContainer.Name.Length + 2);

            CloudBlockBlob blockBlob = _blobContainer.GetBlockBlobReference(fileName);

            return blockBlob.DeleteIfExists();
        }
    }
}
