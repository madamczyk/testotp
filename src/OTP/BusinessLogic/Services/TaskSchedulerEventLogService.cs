﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using Common.Exceptions;

namespace BusinessLogic.Services
{
    public class TaskSchedulerEventLogService : BaseService, ITaskSchedulerEventLogService
    {
        #region Ctor

        public TaskSchedulerEventLogService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventLogService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public TaskSchedulerEventLogService(OTPEntities ctx)
            : base(ctx)
		{ }

        #endregion

        #region Task instance 

        /// <summary>
        /// Creates new task instance for logging messages in context of specific scheduled task execution.
        /// </summary>
        /// <param name="scheduledTaskId">The id of the scheduled task</param>
        /// <returns>The id of the task instance</returns>
        public int CreateTaskInstance(int scheduledTaskId)
        {
            var taskInstance = new TaskInstance()
                {
                    ExecutionTimestamp = DateTime.Now,
                    ExecutionResult = null,
                    ScheduledTask = 
                        _Context.ScheduledTasks.SingleOrDefault(st => st.ScheduledTaskId.Equals(scheduledTaskId))
                };

            _Context.TaskInstances.Add(taskInstance);
            _Context.SaveChanges();

            return taskInstance.Id;
        }

        public void UpdateTaskInstanceStatus(int taskInstanceId, TaskExecutionResult taskExecutionResult)
        {
            var taskInstance = _Context.TaskInstances.Single(ti => ti.Id == taskInstanceId);
            taskInstance.ExecutionResult = (int)taskExecutionResult;

            _Context.SaveChanges();
        }

        /// <summary>
        /// Add new task instance
        /// </summary>
        /// <param name="message"></param>
        public void AddTaskInstance(TaskInstance taskInstance)
        {
            try
            {
                _Context.TaskInstances.Add(taskInstance);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Task Instance", ex);
            }
        }

        /// <summary>
        /// Add new task instance detail
        /// </summary>
        /// <param name="message"></param>
        public void AddTaskInstanceDetail(TaskInstanceDetail taskInstanceDetail)
        {
            try
            {
                _Context.TaskInstanceDetails.Add(taskInstanceDetail);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Task Instance Detail", ex);
            }
        }

        #endregion

        #region Information/Error logging

        /// <summary>
        /// Logs error for specific scheduled task execution
        /// </summary>
        /// <param name="taskInstanceId">Task instance id</param>
        /// <param name="exception">Exception</param>
        public void LogTaskInstanceError(int taskInstanceId, System.Exception exception)
        {
            LogTaskInstanceMessage(taskInstanceId, TaskDetailsMessageType.Error, ExceptionHelper.FormatErrorMessage(exception));
        }

        /// <summary>
        /// Logs error for specific scheduled task execution
        /// </summary>
        /// <param name="taskInstanceId">Task instance id</param>
        /// <param name="errorMessage">Exception</param>
        public void LogTaskInstanceError(int taskInstanceId, string errorMessage)
        {
            LogTaskInstanceMessage(taskInstanceId, TaskDetailsMessageType.Error, errorMessage);
        }

        /// <summary>
        /// Logs custom information message for specific scheduled task execution
        /// </summary>
        /// <param name="taskInstanceId"></param>
        /// <param name="message"></param>
        public void LogTaskInstanceInfo(int taskInstanceId, string message)
        {
            LogTaskInstanceMessage(taskInstanceId, TaskDetailsMessageType.Info, message);
        }
        
        #endregion

        #region Obtaining logs

        /// <summary>
        /// Returns all Task Instance Details
        /// </summary>
        /// <returns>The list of task instance details</returns>
        public IEnumerable<TaskInstanceDetail> GetTaskInstanceDetails()
        {
            var list = _Context.TaskInstanceDetails.AsEnumerable().OrderBy(tid => tid.TaskInstance.ScheduledTask.ScheduledTaskId);
            return list;
        }

        /// <summary>
        /// Returns all Task Instance Details for specific scheduled task
        /// </summary>
        /// <param name="taskId">Id of the scheduled task</param>
        /// <returns>The list of task instance details</returns>
        public IEnumerable<TaskInstanceDetail> GetTaskInstanceDetailsByTaskId(int taskId)
        {
            var list = _Context.TaskInstanceDetails.AsEnumerable().Where(tid => tid.TaskInstance.ScheduledTask.ScheduledTaskId == taskId);
            return list;
        }

        /// <summary>
        /// Return specific task instance detail by id
        /// </summary>
        /// <param name="id">Id of task instance detail</param>
        /// <returns></returns>
        public TaskInstanceDetail GetTaskInstanceDetailById(int id)
        {
            return _Context.TaskInstanceDetails.Where(tid => tid.Id == id).SingleOrDefault();
        }

        /// <summary>
        /// Return specific task instance by id
        /// </summary>
        /// <param name="id">Id of task instance detail</param>
        /// <returns></returns>
        public TaskInstance GetTaskInstanceById(int id)
        {
            return _Context.TaskInstances.Where(ti => ti.Id == id).SingleOrDefault();
        }
        #endregion

        #region Deletion
        /// <summary>
        /// Delete Task Instance
        /// </summary>
        /// <param name="id">internal id of the Task Instance</param>
        public void DeleteTaskInstance(int id)
        {
            try
            {
                TaskInstance taskInstance = this.GetTaskInstanceById(id);
                _Context.TaskInstances.Remove(taskInstance);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Task Instance with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Delete Task Instance Detail
        /// </summary>
        /// <param name="id">internal id of the Task Instance Detail</param>
        public void DeleteTaskInstanceDetail(int id)
        {
            try
            {
                TaskInstanceDetail taskInstanceDetail = this.GetTaskInstanceDetailById(id);
                _Context.TaskInstanceDetails.Remove(taskInstanceDetail);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Task Instance Detail with id {0}", id), ex);
            }
        }
        #endregion

        #region Private helpers

        /// <summary>
        /// Helper method which logs custom message for specific scheduled task execution
        /// </summary>
        /// <param name="taskInstanceId"></param>
        /// <param name="category"></param>
        /// <param name="message"></param>
        private void LogTaskInstanceMessage(int taskInstanceId, TaskDetailsMessageType category, string message)
        {
            var taskInstanceDetail = new TaskInstanceDetail()
            {
                Category = (int)category,
                Message = message,
                TaskInstance = _Context.TaskInstances.Single(ti => ti.Id == taskInstanceId)
            };

            _Context.TaskInstanceDetails.Add(taskInstanceDetail);
            _Context.SaveChanges();
        }

        #endregion
    }
}
