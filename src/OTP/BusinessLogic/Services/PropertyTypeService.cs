﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class PropertyTypeService : BaseService, IPropertyTypesService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyType"/> class.
        /// </summary>
        public PropertyTypeService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyTypeService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public PropertyTypeService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Get Property Type by its internal Id
        /// </summary>
        /// <param name="id">internal id of the Property Type</param>
        /// <returns>Property Type retrieved</returns>
        public PropertyType GetPropertyTypeById(int id)
        {
            try
            {
                return this._Context.PropertyTypes.Where(p => p.PropertyTypeId == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get Property Type with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Add Property Type
        /// </summary>
        /// <param name="p">Property Type to add</param>
        public void AddPropertyType(PropertyType p)
        {
            try
            {
                _Context.PropertyTypes.Add(p);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Property Type", ex);
            }
        }

        /// <summary>
        /// Delete Property Type
        /// </summary>
        /// <param name="id">internal id of the Property Type</param>
        public void DeletePropertyType(int id)
        {
            try
            {
                PropertyType propertyType = this.GetPropertyTypeById(id);
                _Context.PropertyTypes.Remove(propertyType);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Property Type with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets all property types
        /// </summary>
        /// <returns>List of all property types</returns>
        public IEnumerable<PropertyType> GetPropertyTypes()
        {
            var list = 
                _Context.AutoRetryQuery<IEnumerable<PropertyType>>( () =>
                _Context.PropertyTypes.AsEnumerable().OrderBy(p => p.NameCurrentLanguage));
            return list;
        }
    }
}
