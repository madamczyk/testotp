﻿using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BusinessLogic.Services
{
    public class ScheduledVisitService : BaseService, IScheduledVisitsService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduledVisitService"/> class.
        /// </summary>
        public ScheduledVisitService(IContextService httpContextService)
            :base(httpContextService)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduledVisitService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public ScheduledVisitService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Gets all visits
        /// </summary>
        /// <returns>List of all scheduled visits present in DB, ordered by the request date</returns>
        public IEnumerable<ScheduledVisit> GetVisits()
        {
            return _Context.ScheduledVisits.AsEnumerable().OrderBy(v => v.DateCreated);
        }

        /// <summary>
        /// Gets all visits for specified user
        /// </summary>
        /// <param name="id">internal id of the user</param>
        /// <returns>List of visits for specified user, ordered by the request date</returns>
        public IEnumerable<ScheduledVisit> GetVisitsByUserId(int id)
        {
            return _Context.ScheduledVisits.AsEnumerable().Where(v => v.User.UserID == id).OrderBy(v => v.DateCreated);
        }

        /// <summary>
        /// Gets visit by internal id
        /// </summary>
        /// <param name="id">internal id of the visit</param>
        /// <returns>Scheduled visit with specified id</returns>
        public ScheduledVisit GetVisitById(int id)
        {
            return _Context.ScheduledVisits.AsEnumerable().Where(v => v.VisitId == id).FirstOrDefault();
        }
        
        /// <summary>
        /// Add visit
        /// </summary>
        /// <returns>scheduled visit</returns>
        public void AddVisit(ScheduledVisit v)
        {
            try
            {
                _Context.ScheduledVisits.Add(v);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add scheduled visit", ex);
            }
        }

        /// <summary>
        /// Delete visit
        /// </summary>
        /// <param name="id">internal id of the visit</param>
        public void DeleteVisit(int id)
        {
            try
            {
                ScheduledVisit v = this.GetVisitById(id);
                _Context.ScheduledVisits.Remove(v);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Scheduled Visit with id {0}", id), ex);
            }
        }
    }
}
