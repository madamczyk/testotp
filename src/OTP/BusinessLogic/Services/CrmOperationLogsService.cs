﻿using BusinessLogic.ServiceContracts;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Services
{
    public class CrmOperationLogsService : BaseService, ICrmOperationLogsService
    {
        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="EventLogService"/> class.
        /// </summary>
        /// <param name="ctx">Context service</param>
        public CrmOperationLogsService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventLogService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public CrmOperationLogsService(OTPEntities ctx)
            : base(ctx)
		{ }

        #endregion

        #region Methods

        public void AddOperationLog(CrmOperationLog operationLog)
        {
            try
            {
                _Context.CrmOperationLogs.Add(operationLog);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Crm Operation Log", ex);
            }
        }
          
        public IEnumerable<CrmOperationLog> GetCrmOperationsLog()
        {
            var list = _Context.CrmOperationLogs.AsEnumerable().OrderByDescending(e => e.RowDate);
            return list;
        }

        public CrmOperationLog GetCrmOperationLogsById(int id)
        {
            return _Context.CrmOperationLogs.Where(e => e.Id == id).SingleOrDefault();
        }

        public void DeleteCrmOperationsLogById(int id)
        {
            try
            {
                CrmOperationLog eventLog = this.GetCrmOperationLogsById(id);
                _Context.CrmOperationLogs.Remove(eventLog);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Crm Operation Log with id {0}", id), ex);
            }
        }

        #endregion
    }
}
