﻿using BusinessLogic.Managers;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.Objects.Templates.Document;
using System.IO;

namespace BusinessLogic.Services
{
    public class EmailService : BaseTemplateService, IEmailService
    {
        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailService"/> class.
        /// </summary>
        public EmailService(IMessageService messageService, IStorageQueueService storageQueueService, IDocumentService docService, IContextService httpContextService)
            : base(httpContextService, new Dictionary<Type, object>()
            {
                { typeof(IMessageService), messageService },
                { typeof(IStorageQueueService),  storageQueueService },
                { typeof(IDocumentService), docService }
            })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailService"/> class.
        /// </summary>
        public EmailService(IMessageService messageService, IStorageQueueService storageQueueService, OTPEntities ctx)
            : base(new Dictionary<Type, object>()
            {
                { typeof(IMessageService), messageService },
                { typeof(IStorageQueueService),  storageQueueService }
            }, ctx)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailService"/> class.
        /// </summary>
        public EmailService(IMessageService messageService, IStorageQueueService storageQueueService, IContextService ctx)
            : base(ctx, new Dictionary<Type, object>()
            {
                { typeof(IMessageService), messageService },
                { typeof(IStorageQueueService),  storageQueueService }
            })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public EmailService(OTPEntities ctx)
            : base(ctx)
		{
        }

        #endregion

        #region Public Methods

        public void SendEmail(BaseEmail email, LanguageCode language, List<BaseDocument> attachments = null)
        {
            StringBuilder recipients = new StringBuilder();
            StringBuilder CCList = new StringBuilder();
            StringBuilder ReplyToList = new StringBuilder();

            //get template
            Template emailTemplate = base.GetTemplate(email, language);
            StringBuilder sb = new StringBuilder();
            sb.Append(emailTemplate.Content);

            //set parameters
            string body = base.CompleteTemplate(sb.ToString(), email.Parameters);
            string subject = base.CompleteTemplate(String.IsNullOrEmpty(email.Subject) ? emailTemplate.Title : email.Subject, email.Parameters);

            //add recepient address
            foreach (string addr in email.RecipientAddresses)
            {
                recipients.Append(addr);
                recipients.Append(",");
            }
            recipients.Remove(recipients.Length - 1, 1);

            //add cc address
            if (email.CcAddresses != null)
            {
                foreach (string addr in email.CcAddresses)
                {
                    CCList.Append(addr);
                    CCList.Append(",");
                }
                if (CCList.Length > 0)
                {
                    CCList.Remove(CCList.Length - 1, 1);
                }
            }

            //add replyTo addresses
            if (email.ReplyToAddresses != null && email.ReplyToAddresses.Count > 0)
            {
                foreach (string addr in email.ReplyToAddresses)
                {
                    ReplyToList.Append(addr);
                    ReplyToList.Append(",");
                }
            }

            if (ReplyToList.Length > 0)
            {
                ReplyToList.Remove(ReplyToList.Length - 1, 1);
            }
            Message mail = new Message
            {
                Recipients = recipients.ToString(),
                Created = DateTime.Now,
                Subject = subject,
                Body = body,
                Status = (int)MessageStatus.New,
                ReplyTo = ReplyToList.ToString(),
                CC = CCList.ToString(),
                Type = emailTemplate.TemplateId,
                Sender = (int)email.Sender
            };
            //TimeSpan _maxProcessingTime = TimeSpan.FromMinutes(5);
            //using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required,
            //   new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted, Timeout = _maxProcessingTime }))
            //{
                if (attachments != null)
                {
                    foreach (BaseDocument att in attachments)
                    {
                        DataAccess.Attachment attachment = new DataAccess.Attachment();
                        attachment.Content = GetFileContent(att, language);
                        attachment.Message = mail;
                        attachment.Type = (int)att.DocumentType;

                        string title = CompleteTemplate(base.GetTemplate(att, language).Title, att.Parameters);
                        attachment.Title = title;
                        _Context.Attachments.Add(attachment);
                    }
                }
                _messageService.Add(mail);                
            
            _Context.SaveChanges();
        }

        public Template GetTemplateByTemplateType(TemplateType notificationType, LanguageCode language)
        {
            return base.GetTemplateByType(notificationType, language);
        }

        private byte[] GetFileContent(BaseDocument document, LanguageCode language)
        {
            Stream stream = this._documentService.CreateDocument(document, language);
            if (stream == null)
                return null;
            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            return ms.ToArray();
        }

        #endregion
    }
}
