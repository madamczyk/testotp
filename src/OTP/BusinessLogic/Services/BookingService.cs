﻿using BusinessLogic.Enums;
using BusinessLogic.EVS;
using BusinessLogic.Objects;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.PaymentGateway;
using BusinessLogic.ServiceContracts;
using Common.Exceptions;
using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Security;
using System.Xml.Serialization;
using Common.Extensions;

namespace BusinessLogic.Services
{
    /// <summary>
    /// BOOKING WIZARD related service
    /// 
    /// Contains all the Booking Wizard business logic, temporary reservations creation, EVS, payment gateway invokes
    /// </summary>
    public class BookingService : BaseService, IBookingService
    {
        private const string CURRENCY_CODE_USD = "USD";
        private IPaymentGatewayService paymentGatewayService;
        private IMessageService messageService;
        private ISettingsService settingsService;
        private OTPEntities oTPEntities;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookingService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public BookingService(IContextService httpContextService, IPropertiesService propertiesService,
            IPropertyAddOnsService propertyAddOnsService, ITaxesService taxesService,
            IUnitsService unitService, IUserService userService,
            IStateService stateService, IEventLogService logService,
            ISettingsService settingsService, IMessageService messageService,
            IPaymentGatewayService paymentGatewayService,
            IUnitTypesService unitTypeService,
            IAuthorizationService authorizationService)
            : base(httpContextService, new Dictionary<Type, object>() { 
                {typeof(IPropertiesService), propertiesService },
                {typeof(IPropertyAddOnsService), propertyAddOnsService },
                {typeof(IUnitsService), unitService },
                {typeof(IUserService), userService },
                {typeof(ITaxesService), taxesService },
                {typeof(IStateService), stateService },
                {typeof(IEventLogService), logService },
                {typeof(ISettingsService), settingsService },
                {typeof(IMessageService), messageService },
                {typeof(IPaymentGatewayService), paymentGatewayService }, 
                {typeof(IUnitTypesService), unitTypeService},
                {typeof(IAuthorizationService), authorizationService}
            })
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BookingService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public BookingService(OTPEntities ctx)
            : base(ctx)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BookingService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public BookingService(IPropertiesService propertiesService, IPropertyAddOnsService propertyAddOnsService,
            ITaxesService taxesService, IUnitsService unitService,
            IUserService userService, IStateService stateService, OTPEntities ctx)
            : base(new Dictionary<Type, object>() { 
                {typeof(IPropertiesService), propertiesService },
                {typeof(IPropertyAddOnsService), propertyAddOnsService },
                {typeof(IUnitsService), unitService },
                {typeof(IUserService), userService },
                {typeof(ITaxesService), taxesService },
                {typeof(IStateService), stateService }
            }, ctx)
        { }

        public BookingService(
            IPaymentGatewayService paymentGatewayService, IMessageService messageService, 
            ISettingsService settingsService, OTPEntities ctx)
            : base(new Dictionary<Type, object>() {
                {typeof(IPaymentGatewayService), paymentGatewayService},
                {typeof(IMessageService), messageService},
                {typeof(ISettingsService), settingsService}
            }, ctx)
        { }

        /// <summary>
        /// Generates invoce data for booking process and information purposes
        /// </summary>
        /// <param name="propertyId">Id of the property</param>
        /// <param name="unitId">Id of the unit</param>
        /// <param name="dateFrom">Reservation start day</param>
        /// <param name="dateUntil">Reservation end day</param>
        /// <returns></returns>
        public DataAccess.CustomObjects.PropertyDetailsInvoice CalculateInvoice(int propertyId, int unitId, DateTime dateFrom, DateTime dateUntil)
        {
            int numberOfGuests = 1;

            PropertyDetailsInvoice invoiceDetails = new PropertyDetailsInvoice();
            TimeSpan diff = dateUntil.Date - dateFrom.Date;
            invoiceDetails.LengthOfStay = diff.Days;
            List<Invoice7DayDiscount> daysDiscount = new List<Invoice7DayDiscount>();

            var property = this._propertiesService.GetPropertyById(propertyId);

            #region 7 Day Discount Calculation

            if (invoiceDetails.LengthOfStay >= 7)
            { // only for 7 or more days


                var discountInTimePeriod = _Context.Units.Where(u => u.UnitID.Equals(unitId))
                    .SelectMany(u => u.UnitType.UnitType7dayDiscounts)
                    .Where(
                        (d => (d.DateFrom >= dateFrom && d.DateFrom <= dateUntil)
                            || (d.DateUntil >= dateFrom && d.DateUntil <= dateUntil)
                            || (dateFrom >= d.DateFrom && dateUntil <= d.DateUntil))
                    //|| (d.DateFrom >= dateFrom && dateUntil <= d.DateUntil && dateUntil >= d.DateFrom ))
                    );

                discountInTimePeriod.ToList().ForEach(delegate(UnitType7dayDiscounts discount)
                {
                    DateTime nextDay = DateTime.Now;
                    int numberOfDays = 0;

                    foreach (var i in Enumerable.Range(0, Convert.ToInt32(dateUntil.Subtract(dateFrom).TotalDays)))
                    {
                        nextDay = dateFrom.AddDays(i);
                        if (nextDay >= discount.DateFrom && nextDay <= discount.DateUntil)
                            numberOfDays++;
                    }

                    if (numberOfDays > 0)
                    {
                        daysDiscount.Add(new Invoice7DayDiscount()
                        {
                            DiscountValue = discount.Discount,
                            NumberOfDiscountDays = numberOfDays,
                            NumberOfDiscountPeriodDays = (discount.DateUntil - discount.DateFrom).Days
                        }
                            );
                    }
                });
            }

            #endregion

            #region Total Accomodation
            var price = this._unitsService.GetUnitPriceAccomodation(unitId, dateFrom, dateUntil);
            invoiceDetails.TotalAccomodation = price ?? 0;
            invoiceDetails.PricePerNight = invoiceDetails.TotalAccomodation / diff.Days;

            foreach (var item in daysDiscount)
            {
                invoiceDetails.TotalSevenDaysDiscount += (invoiceDetails.TotalAccomodation * item.DiscountValue / 100) * item.NumberOfDiscountDays / diff.Days;
            }
            invoiceDetails.TotalSevenDaysDiscount = -invoiceDetails.TotalSevenDaysDiscount;
            #endregion

            #region Total Add Ons

            invoiceDetails.AddonsList = this._propertyAddOnsService.GetAddOnsByPropertyId(propertyId).Where(a => a.IsMandatory);
            //total amount of addOn depends on unit of mesaure of PropertyAddOn (see:  Enum PropertyAddOnUnit)
            //invoiceDetails.TotalAddons = invoiceDetails.AddonsList.Sum(a => a.Price);

            foreach (PropertyAddOn addOn in invoiceDetails.AddonsList)
            {
                switch ((PropertyAddOnUnit)addOn.Unit)
                {
                    case PropertyAddOnUnit.PerDay:
                        addOn.TotalPrice = diff.Days * addOn.Price;
                        break;
                    case PropertyAddOnUnit.PerPersonPerDay:
                        addOn.TotalPrice = diff.Days * addOn.Price * numberOfGuests;
                        break;
                    case PropertyAddOnUnit.PerPersonPerWeek:
                        addOn.TotalPrice = (decimal)(diff.Days / 7.00) * addOn.Price * numberOfGuests;
                        break;
                    case PropertyAddOnUnit.PerStay:
                        addOn.TotalPrice = addOn.Price;
                        break;
                    case PropertyAddOnUnit.PerWeek:
                        addOn.TotalPrice = (decimal)(diff.Days / 7.00) * addOn.Price;
                        break;
                }
                addOn.TotalPrice = (addOn.TotalPrice * addOn.Tax.Percentage) / 100;
                invoiceDetails.TotalAddons += addOn.TotalPrice;

            }
            #endregion

            invoiceDetails.SubtotalAccomodationAddons = invoiceDetails.TotalAccomodation + invoiceDetails.TotalAddons;

            #region Total Tax

            invoiceDetails.TaxList = this._taxesService.GetTaxesForDestination(property.Destination.DestinationID);
            decimal taxBase = invoiceDetails.TotalAccomodation + invoiceDetails.TotalSevenDaysDiscount;

            foreach (var item in invoiceDetails.TaxList)
            {
                item.CalculatedValue = (taxBase * item.Percentage) / 100;
            }
            invoiceDetails.TotalTax = invoiceDetails.TaxList.Sum(t => t.CalculatedValue);

            #endregion

            invoiceDetails.InvoiceTotal = invoiceDetails.SubtotalAccomodationAddons + invoiceDetails.TotalTax + invoiceDetails.TotalSevenDaysDiscount;

            invoiceDetails.Culture = property.DictionaryCulture.CultureInfo;

            return invoiceDetails;
        }

        /// <summary>
        /// Performs Tentative booking on the currently selected unit for defined period of time
        /// </summary>
        /// <param name="bookingInfo">Booking trip base information</param>
        /// <param name="usr">User for which booking should be performed</param>
        /// <returns>Id of the created reservation</returns>
        public int DoTemporaryReservartion(BookingInfoModel bookingInfo, User usr)
        {
            var unit = _unitsService.GetUnitById(bookingInfo.UnitId);
            var invoiceDetails = CalculateInvoice(bookingInfo.PropertyId, bookingInfo.UnitId, bookingInfo.dateTripFrom, bookingInfo.dateTripUntil);

            Reservation resv = new Reservation()
            {
                BookingDate = DateTime.Now,
                DateArrival = bookingInfo.dateTripFrom,
                DateDeparture = bookingInfo.dateTripUntil,
                BookingStatus = (int)BookingStatus.TentativeBooking,
                TripBalance = -invoiceDetails.InvoiceTotal,
                TotalPrice = invoiceDetails.InvoiceTotal,
                Unit = unit,
                UnitType = unit.UnitType,
                Property = unit.Property,
                DictionaryCulture = unit.Property.DictionaryCulture,
                User = usr,
                LinkAuthorizationToken = Guid.NewGuid().ToString()
            };

            UnitInvBlocking unitBlocking = new UnitInvBlocking()
            {
                DateFrom = bookingInfo.dateTripFrom,
                DateUntil = bookingInfo.dateTripUntil.AddDays(-1), //one day less than actual selection date
                Type = (int)UnitBlockingType.Reservation,
                Unit = unit,
                Reservation = resv
            };

            resv.UnitInvBlockings.Add(unitBlocking);

            _unitsService.AddUnitBlockade(unitBlocking);

            _Context.SaveChanges(); //needed for getting new reservation ID

            return resv.ReservationID;
        }

        /// <summary>
        /// Performs user roles blockage after wrong ID Verification process
        /// </summary>
        /// <param name="usr">Currently logged user</param>
        public void BlockUserAfterWrongIDVerification(User usr)
        {
            try
            {
                EVSVerificationFaildEmail email = new EVSVerificationFaildEmail(this._settingsService.GetSettingValue(SettingKeyName.EVSFailNotifyEmail))
                {
                    UserId = usr.UserID.ToString(),
                    UserName = usr.FullName,
                };
                this._messageService.AddEmail(email);
            }
            finally
            {

                foreach (var role in usr.UserRoles)
                {
                    role.Status = (int)UserStatus.Blocked;
                }

                //
                //FormsAuthentication.SignOut();
                this._authorizationService.SingOutUser();
                this._stateService.CurrentUser = null;
            }
        }

        /// <summary>
        /// Sets User verification hisotry data
        /// </summary>
        /// <param name="usr">Logged user</param>
        /// <param name="IsVerified">Verification result</param>
        /// <param name="evsResponse">Response from EVS system</param>
        public void SetVerificationInfo(User usr, bool IsVerified, IEVSResponse evsResponse)
        {
            usr.VerificationPositive = IsVerified;
            SetVerificationDetails(usr, IsVerified, evsResponse);
        }

        /// <summary>
        /// Saves PaymentData to DB
        /// </summary>
        /// <param name="reservationId">Id of the reservation</param>
        /// <param name="userId">Id of the current logged user</param>
        /// <param name="address">Billing address of the reservation</param>
        /// <param name="guestPaymentId">Id of the user credit card</param>
        /// <returns>Result of the payment opertaion</returns>
        public PaymentResult PerformPayment(int reservationId, int userId, ReservationBillingAddress address, int guestPaymentId)
        {
            var resv = this._Context.Reservations.Where(r => r.ReservationID.Equals(reservationId)).FirstOrDefault();
            var paymentMethod = _Context.GuestPaymentMethods.Find(guestPaymentId);
            resv.GuestPaymentMethod = paymentMethod;

            var invoiceDetails = CalculateInvoice(resv.Property.PropertyID, resv.Unit.UnitID, resv.DateArrival, resv.DateDeparture);
            var amount = CalculateAmountToAuthorize(invoiceDetails.InvoiceTotal, resv.DateArrival);
            var amountInCents = (amount * 100); // convert form dolars to cents ( $27.85 -> 2785 )

            PaymentResult paymentResult = new PaymentResult();
            TransactionRequest request = new TransactionRequest()
            {
                Amount = ((int)amountInCents).ToString(),
                CurrencyCode = resv.Property.DictionaryCulture.ISOCurrencySymbol,
                PaymentToken = paymentMethod.ReferenceToken
            };

            //try
            //{
            /// Take the amount for the reservation invoice
            TransactionResponse response = null;
            BookingStatus bookingStatus = BookingStatus.ReceivedBooking;

            TimeSpan reservationBookArrivalDiff = resv.DateArrival.Date - resv.BookingDate.Date;

            if (reservationBookArrivalDiff.Days < 15)
            {
                // less than 15 days to arrival
                response = this._paymentGatewayService.Purchase(request, reservationId);
                resv.UpfrontPaymentProcessed = true;
                bookingStatus = BookingStatus.FinalizedBooking;
            }
            else
            {
                response = this._paymentGatewayService.AuthorizePayment(request, reservationId);
            }

            if (response == null || response.Succeded.Equals(false))
            {
                throw new PaymentGatewayException("Trip payment amount has not been aquired!", null);
            }

            paymentResult.PaymentGatewayResult = response.Succeded;
            paymentResult.GatewayResponse = response;

            address.Reservation = resv;
            var user = this._userService.GetUserByUserId(userId);
            address.User = user;
            user.ReservationBillingAddresses.Add(address);

            paymentResult.BookingConfirmationID = GenerateConfirmationId();

            decimal transactionFee = _propertiesService.CalculatePropertyCommission(resv.DateArrival, resv.DateDeparture, invoiceDetails.InvoiceTotal, resv.Property.PropertyID, invoiceDetails.LengthOfStay);

            resv.ReservationBillingAddresses.Add(address);
            resv.ConfirmationID = paymentResult.BookingConfirmationID;
            resv.TransactionFee = transactionFee;
            resv.TransactionToken = paymentResult.GatewayResponse.Token;
            resv.BookingStatus = (int)bookingStatus;
            paymentMethod.Reservations.Add(resv);

            AddReservationTransactions(resv, invoiceDetails);
            AddPaymentTransaction(resv, amount);

            CalculateSecurityDeposit(resv);
            /// Only aquire security deposit when there is less than 30 days to travel (from most restrictive arrival date) and it should be taken
            if (reservationBookArrivalDiff.Days <= 30 && resv.TakeSecurityDeposit.HasValue)
            {
                try
                {
                    AquireSecurityDeposit(resv);
                }

                catch (SecurityDepositException)
                {
                    /// If reservation is made today and travel arrival date is also today - send email with no wait
                    if (reservationBookArrivalDiff.Days < 1)
                    {
                        SendSecurityDepositAlertEmail(resv);
                    }
                    /// If not nothing happens - reservation is made correctly without taking security deposit. It will be taken on next day.
                }
            }

            //}
            //catch (PaymentGatewayException)
            //{
            //    /// Only when security deposit was taken
            //    if (!string.IsNullOrEmpty(resv.SecurityDepositToken)) {
            //        resv.BookingStatus = (int)BookingStatus.NoFunds;

            //        /// Send email on security deposit taken and fail on taking reservation amount
            //        var supportEmail = this._settingsService.GetSettingValue(SettingKeyName.EmailAccountOTPSupport);
            //        ReservationNoFundsAlertEmail noFundsEmail = new ReservationNoFundsAlertEmail(supportEmail);
            //        noFundsEmail.GuestFullName = resv.User.FullName;
            //        noFundsEmail.PropertyName = resv.Property.PropertyNameCurrentLanguage;
            //        noFundsEmail.TravelPeriod = resv.DateArrival.ToLocalizedDateString() + " - " + resv.DateDeparture.ToLocalizedDateString();

            //        this._messageService.AddEmail(noFundsEmail, LanguageCode.en);                    
            //    }

            //    throw;
            //}
            return paymentResult;
        }

        public void SendSecurityDepositAlertEmail(Reservation reservation)
        {
            /// Send email on security deposit taken and fail on taking reservation amount
            var supportEmail = this._settingsService.GetSettingValue(SettingKeyName.EmailAccountOTPSupport);
            ReservationNoFundsAlertEmail noFundsEmail = new ReservationNoFundsAlertEmail(supportEmail);
            noFundsEmail.GuestFullName = reservation.User.FullName;
            noFundsEmail.PropertyName = reservation.Property.PropertyNameCurrentLanguage;
            noFundsEmail.TravelPeriod = reservation.DateArrival.ToLocalizedDateString() + " - " + reservation.DateDeparture.ToLocalizedDateString();
            noFundsEmail.BookingDate = reservation.BookingDate.ToLocalizedDateString();
            noFundsEmail.ConfirmID = reservation.ConfirmationID;
            noFundsEmail.DepositAmount = reservation.SecurityDeposit.Value.ToString();

            this._messageService.AddEmail(noFundsEmail);     
        }

        /// <summary>
        /// Calculates amount to be authorized for the SecurityDeposit.
        /// </summary>
        /// <param name="reservation">Reservation to calculate amount for.</param>
        public void CalculateSecurityDeposit(Reservation reservation)
        {
            /// TODO: When there are service providers check for service providers that disables the security deposit and put amount = null

            decimal amountToAuthorize = 0;

            if (reservation.Property.SecurityDeposit.HasValue)
            {
                amountToAuthorize = reservation.Property.SecurityDeposit.Value;
            }
            else
            {
                var securityDepositTableValue = _Context.SecurityDeposits.Where(s => s.DictionaryCulture.CultureId.Equals(reservation.DictionaryCulture.CultureId)).SingleOrDefault();
                if (securityDepositTableValue != null)
                {
                    amountToAuthorize = securityDepositTableValue.Amount;
                }
            }

            if (amountToAuthorize > 0)
            {
                reservation.TakeSecurityDeposit = true;
                reservation.SecurityDeposit = amountToAuthorize;
            }
        }

        public TransactionResponse AquireSecurityDeposit(Reservation reservation)
        {
            TransactionResponse response = new TransactionResponse();
            decimal amountInCents = reservation.SecurityDeposit.Value * 100;

            TransactionRequest request = new TransactionRequest()
            {
                Amount = ((int)amountInCents).ToString(),
                CurrencyCode = CURRENCY_CODE_USD,
                PaymentToken = reservation.GuestPaymentMethod.ReferenceToken
            };

            try
            {
                response = this._paymentGatewayService.AuthorizePayment(request, reservation.ReservationID);

                if (response == null || response.Succeded != true)
                {
                    throw new SecurityDepositException("Could not aquire security deposit!", null);
                }
                reservation.SecurityDepositToken = response.Token;
            }
            catch (PaymentGatewayException ex)
            {
                throw new SecurityDepositException("Could not aquire security deposit!", ex);
            }

            return response;
        }

        /// <summary>
        /// Remove guest credit card from DB
        /// </summary>
        /// <param name="guestPaymentId">Id of the payment method</param>
        public void RemoveGuestPaymentMethod(int guestPaymentId)
        {
            _Context.GuestPaymentMethods.Remove(_Context.GuestPaymentMethods.Find(guestPaymentId));
        }

        /// <summary>
        /// Retrieves all the supported credtit card types
        /// </summary>
        /// <returns>List of the credit card types</returns>
        public List<DataAccess.CreditCardType> GetCreditCardTypes()
        {
            return _Context.CreditCardTypes.ToList();
        }

        /// <summary>
        /// Retrieves all user used credit cards data
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <returns>List of the saved user credit cards</returns>
        public List<GuestPaymentMethod> GetUserCreditCardTypes(int userId)
        {
            return _Context.GuestPaymentMethods.Where(p => p.User.UserID.Equals(userId)).ToList();
        }

        /// <summary>
        /// Checks whenever Unit is available for Booking
        /// </summary>
        /// <param name="bookModel">Trip details</param>
        /// <param name="reservationId">Reservation that should be excluded from check</param>
        /// <returns>True if available, false in other case</returns>
        public bool IsUnitAvailable(BookingInfoModel bookModel, int? reservationId)
        {
            var unitBlockage = from blockage in _Context.UnitInvBlockings
                               where
                                    blockage.Unit.UnitID.Equals(bookModel.UnitId)
                                    && ((bookModel.dateTripFrom >= blockage.DateFrom && bookModel.dateTripFrom <= blockage.DateUntil)
                                    || (bookModel.dateTripUntil >= blockage.DateFrom && bookModel.dateTripUntil <= blockage.DateUntil)
                                    )
                               select blockage;
            if (reservationId.HasValue)
                unitBlockage = unitBlockage.Where(b => !b.Reservation.ReservationID.Equals(reservationId.Value));

            if (unitBlockage.Count() > 0) // check if is not already reserved
                return false;

            var mlosList = this._unitsService.GetUnitMlos(bookModel.UnitId, bookModel.dateTripFrom, bookModel.dateTripUntil);

            var mlos = mlosList.Count() == 0 ? 0 : mlosList.Max(s => s.MLOS);
            var nights = bookModel.dateTripUntil - bookModel.dateTripFrom;
            if (mlos > nights.Days) //check if mlos is ok
                return false;

            var ctaList = this._unitTypesService.GetUnitTypeCtaByDate(bookModel.UnitId, bookModel.dateTripFrom);
            if (ctaList.Count() > 0)
                return false;

            return true;
        }

        /// <summary>
        /// Contacts EVS Service provider to get user verification questions
        /// </summary>
        /// <param name="userId">ID of the user to verify</param>
        /// <returns>Verification service answer</returns>        
        public IEVSResponse GetEVSQuestions(Int32 userId)
        {
            var userData = this._userService.GetUserByUserId(userId);

            // prepare service
            ChannelFactory<IEVSContract> factory = new ChannelFactory<IEVSContract>("EVSService");
            var proxy = factory.CreateChannel();

            // prepare request data
            var request = PrepareEVSRequest(userData);

            // request response            
            EVSResponse response = null;

            try
            {
                response = proxy.Consumer(request);
                if (response.TransactionDetails.Errors.Count() > 0)
                    LogEVSEvents(response.TransactionDetails.Errors, userData);
                if (response.TransactionDetails.Warnings.Count() > 0)
                    LogEVSEvents(response.TransactionDetails.Warnings, userData);

                return response;
            }
            catch (EndpointNotFoundException ex) //this means that service is unavailable
            {
                string message = ex.Message;
                LogEvent(message, EventCategory.Error, userData);
            }
            catch (TimeoutException ex) //this means that service is unavailable
            {
                string message = ex.Message;
                LogEvent(message, EventCategory.Error, userData);
            }
            finally
            {
                ((IDisposable)proxy).Dispose(); // dispose service object
            }
            return null;
        }

        /// <summary>
        /// Contacts EVS Service provider to get user verification data
        /// </summary>
        /// <param name="userId">ID of the user to verify</param>
        /// <returns>Verification service answer</returns>
        public IEVSResponse GetEVSInternational(Int32 userId)
        {
            var userData = this._userService.GetUserByUserId(userId);

            // prepare service
            ChannelFactory<IEVSContractInternational> factory = new ChannelFactory<IEVSContractInternational>("EVSServiceInternational");
            var proxy = factory.CreateChannel();

            // prepare request data
            var request = PrepareEVSRequest(userData);

            // request response            
            EVSResponseInternational response = null;

            try
            {
                response = proxy.International(request);
                if (response.TransactionDetails.Errors.Count() > 0)
                    LogEVSEvents(response.TransactionDetails.Errors, userData);
                if (response.TransactionDetails.Warnings.Count() > 0)
                    LogEVSEvents(response.TransactionDetails.Warnings, userData);

                if (response.ResponseResult.Result == null || Int32.Parse(response.ResponseResult.Result.Code) == (int)EVSResponseCodesInternational.NO_MATCH) //this means no match in EVS service
                    SetVerificationDetails(userData, false, response);

                return response;
            }
            catch (EndpointNotFoundException ex) //this means that service is unavailable
            {
                string message = ex.Message;
                LogEvent(message, EventCategory.Error, userData);
            }
            finally
            {
                ((IDisposable)proxy).Dispose(); // dispose service object
            }
            return null;
        }

        /// <summary>
        /// Prepare EVS request data
        /// </summary>
        /// <param name="userData">User data to verify</param>
        /// <returns></returns>
        public EVSRequest PrepareEVSRequest(User userData)
        {
            // get login & pass for EVS Service
            string evsUser = this._settingsService.GetSettingValue(SettingKeyName.EVSUser);
            string evsPassword = this._settingsService.GetSettingValue(SettingKeyName.EVSPassword);

            EVSRequest request = new EVSRequest();
            Credentials userCredentials = new Credentials();
            userCredentials.Username = evsUser;
            userCredentials.Password = evsPassword;
            request.UserCredentials = userCredentials;
            request.CustomerReference = "";
            UserIdentity identity = new UserIdentity();
            identity.City = userData.City;
            identity.FirstName = userData.Firstname;
            identity.LastName = userData.Lastname;
            identity.PhoneNumber = userData.CellPhone;
            identity.State = userData.State;
            identity.Street = userData.Address1;
            identity.ZipCode = userData.ZIPCode;
            identity.CountryCode = userData.DictionaryCountry.CountryCode2Letters;
            identity.Gender = userData.Gender;
            request.Identity = identity;

            return request;
        }

        /// <summary>
        /// Add new Guest Payment Method to DB
        /// </summary>
        /// <param name="payment">Guest payment data</param>
        /// <param name="userId">Id of the guest (current user)</param>
        /// <param name="creditHolderId">Id of the credit card type</param>
        /// <param name="reservationId">Id of the reservation</param>
        /// <param name="succeed">Info about succed in SpreadlyCore</param>
        /// <returns>Id of the new payment method</returns>
        public int SetGuestPaymentMethod(GuestPaymentMethod payment, int userId, int creditHolderId, int reservationId, out bool succeed)
        {
            var response = this._paymentGatewayService.RetainPaymentMethod(payment.ReferenceToken, reservationId);

            User usr = _userService.GetUserByUserId(userId);

            payment.User = usr;
            payment.CreditCardType = _Context.CreditCardTypes.Find(creditHolderId);

            usr.GuestPaymentMethods.Add(payment);

            _Context.SaveChanges();

            succeed = response.Succeded;
            return payment.GuestPaymentMethodID;
        }

        /// <summary>
        /// Adds agreement (as PDF file) to reservation
        /// </summary>
        /// <param name="reservationId"></param>
        /// <param name="file"></param>
        public void AddReservationAgreement(int reservationId, byte[] file)
        {
            Reservation reservation = _Context.Reservations.Where(r => r.ReservationID == reservationId).SingleOrDefault();
            if (reservation != null)
            {
                reservation.Agreement = file;
            }
        }

        /// <summary>
        /// Adds invoice (as PDF file) to reservation
        /// </summary>
        /// <param name="reservationId"></param>
        /// <param name="file"></param>
        public void AddReservationInvoice(int reservationId, byte[] file)
        {
            Reservation reservation = _Context.Reservations.Where(r => r.ReservationID == reservationId).SingleOrDefault();
            if (reservation != null)
            {
                reservation.Invoice = file;
            }
        }

        /// <summary>
        /// Generates payment record for reservation
        /// </summary>
        /// <param name="resv">Reservation item</param>
        /// <param name="paymentAmount">Amount to pay</param>
        public void AddPaymentTransaction(Reservation resv, decimal paymentAmount)
        {
            ReservationTransaction paymentTransaction = new ReservationTransaction()
            {
                Amount = paymentAmount,
                Charge = false,
                Payment = true,
                Reservation = resv,
                TransactionCode = _Context.TransactionCodes.Find(((int)TransactionCodes.Payment)),
                TransactionDate = DateTime.Now,
            };
            paymentTransaction.SetNameValue(Common.Resources.Common.Booking_Payment);

            resv.ReservationTransactions.Add(paymentTransaction);

            //we have to do recalculation of the trip balance
            decimal calculatedTripBalance = 0;

            foreach (var tran in resv.ReservationTransactions)
                calculatedTripBalance += tran.Payment == true ? tran.Amount : -tran.Amount;

            resv.TripBalance = calculatedTripBalance;
        }

        /// <summary>
        /// Set number of guests in reservation
        /// </summary>
        /// <param name="numberOfGuests">Number of guests</param>
        /// <param name="reservationId">Id of the reservation</param>
        public void UpdateReservationGuestNumber(int numberOfGuests, int reservationId)
        {
            Reservation resv = _Context.Reservations.Where(r => r.ReservationID == reservationId).SingleOrDefault();
            resv.NumberOfGuests = numberOfGuests;
        }

        public bool CheckUnitRatesForPeriod(int unitId, DateTime dateArrival, DateTime dateDeparture)
        {
            return _Context.Units.Where(u => u.UnitID.Equals(unitId)).Select(u => OTPEntities.CheckRatesForPeriod(unitId, dateArrival, dateDeparture)).First();
        }


        #region PrivateMembers

        /// <summary>
        /// Calculates amount to be authorized from guest accound
        /// </summary>
        /// <param name="fullAmount">Full amount to pay</param>
        /// <param name="arrivalDate">Guests arrival date</param>
        /// <returns>Calculated amount</returns>
        public decimal CalculateAmountToAuthorize(decimal fullAmount, DateTime arrivalDate)
        {
            int firstPeriodDays = Int32.Parse(this._settingsService.GetSettingValue(SettingKeyName.PaymentPolicyFirstStepDays));
            int secondPeriodDays = Int32.Parse(this._settingsService.GetSettingValue(SettingKeyName.PaymentPolicySecondStepDays));
            decimal firstPeriodDaysPercent = (decimal)Int32.Parse(this._settingsService.GetSettingValue(SettingKeyName.PaymentPolicyFirstStepDaysGreaterThanPercentToAuthorize)) / 100;
            decimal secondPeriodDaysPercent = (decimal)Int32.Parse(this._settingsService.GetSettingValue(SettingKeyName.PaymentPolicySecondStepDaysGreaterThanPercentToAuthorize)) / 100;

            decimal calculatedAmount = 0;

            if (arrivalDate > DateTime.Now.AddDays(firstPeriodDays))
                calculatedAmount = fullAmount * firstPeriodDaysPercent;
            else if (arrivalDate < DateTime.Now.AddDays(firstPeriodDays) && arrivalDate > DateTime.Now.AddDays(secondPeriodDays))
                calculatedAmount = fullAmount * secondPeriodDaysPercent;
            else
                calculatedAmount = fullAmount;

            return calculatedAmount;
        }

        /// <summary>
        /// Generates transaction records for reservations
        /// </summary>
        /// <param name="resv">Reservation item</param>
        /// <param name="invoiceDetails">Calculated invoice for reservation</param>
        public void AddReservationTransactions(Reservation resv, PropertyDetailsInvoice invoiceDetails)
        {
            //add due payment data for accomodation and taxes
            #region Per Accomodation
            int transactionCodeTypeDuePayment = (int)TransactionCodes.DuePayment;

            var transactionCodeDuePayment = _Context.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeTypeDuePayment)).SingleOrDefault();

            ReservationDetail resvDetailAccomodation = new ReservationDetail()
            {
                Reservation = resv,
                StartDate = resv.DateArrival,
                EndDate = resv.DateDeparture,
                Type = (int)ReservationDetailsType.Stay,
                BasePrice = invoiceDetails.PricePerNight,
                Base = (int)ReservationDetailsBase.PerNight,
                LinePrice = invoiceDetails.TotalAccomodation
            };

            ReservationTransaction resvTransactionAccomodation = new ReservationTransaction()
            {
                Amount = invoiceDetails.TotalAccomodation,
                Charge = true,
                Payment = false,
                Reservation = resv,
                ReservationDetail = resvDetailAccomodation,
                TransactionCode = transactionCodeDuePayment,
                TransactionDate = DateTime.Now
            };
            resvTransactionAccomodation.SetNameValue( Common.Resources.Common.Booking_Accomodation);

            resv.ReservationDetails.Add(resvDetailAccomodation);
            resv.ReservationTransactions.Add(resvTransactionAccomodation);
            resvDetailAccomodation.ReservationTransactions.Add(resvTransactionAccomodation);
            #endregion

            #region Per Tax
            int taxTransactionCode = (int)TransactionCodes.Tax;
            var taxTransactionCodeObject = _Context.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(taxTransactionCode)).SingleOrDefault();

            foreach (var tax in invoiceDetails.TaxList)
            {
                ReservationTransaction resvTransactionTax = new ReservationTransaction()
                {
                    Amount = tax.CalculatedValue,
                    Charge = true,
                    Payment = false,
                    Reservation = resv,
                    TransactionCode = taxTransactionCodeObject,
                    TransactionDate = DateTime.Now
                };
                resvTransactionTax.Caption_i18n = tax.Name_i18n;
                resv.ReservationTransactions.Add(resvTransactionTax);
            }
            #endregion

            #region Per 7 Day Discount
            int transactionCodeTypeDiscount = (int)TransactionCodes.SevenDayDiscount;

            var transactionCodeDiscount = _Context.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeTypeDiscount)).SingleOrDefault();

            ReservationTransaction resvTransaction7DayDiscount = new ReservationTransaction()
            {
                Amount = invoiceDetails.TotalSevenDaysDiscount,
                Charge = true,
                Payment = false,
                Reservation = resv,
                ReservationDetail = resvDetailAccomodation,
                TransactionCode = transactionCodeDiscount,
                TransactionDate = DateTime.Now
            };
            resvTransaction7DayDiscount.SetNameValue(Common.Resources.Common.Booking_7DayDiscount);
            resv.ReservationTransactions.Add(resvTransaction7DayDiscount);
            #endregion
        }

        /// <summary>
        /// Generates new ConfirmationId for reservation
        /// </summary>
        /// <returns></returns>
        private string GenerateConfirmationId()
        {
            var confirmationId = Guid.NewGuid().ToString().Substring(0, 6);

            int numberOfExisting = _Context.Reservations.Where(r => r.ConfirmationID.Equals(confirmationId)).Select(s => s).Count();

            while (numberOfExisting > 0) // search for the free confirmation ID - not yet existing in DB
            {
                confirmationId = Guid.NewGuid().ToString().Substring(0, 6);
                numberOfExisting = _Context.Reservations.Where(r => r.ConfirmationID.Equals(confirmationId)).Select(s => s).Count();
            }
            return confirmationId.ToUpper();
        }

        /// <summary>
        /// Helper method to log EVS system returned error data
        /// </summary>
        /// <param name="eventItem"></param>
        private void LogEVSEvents(IEnumerable<Event> eventItem, User usr)
        {
            string evsErrorPattern = "EVS Error code={0}, message={1}";

            foreach (var error in eventItem)
            {
                if (error.EventData == null) continue;
                string errorMsg = String.Format(evsErrorPattern, error.EventData.Code, error.EventData.Message);
                LogEvent(errorMsg, EventCategory.Warning, usr);
            }
        }

        /// <summary>
        /// Helper method for logging events to log location
        /// </summary>
        /// <param name="message">Event message</param>
        /// <param name="category">Type of the event</param>
        private void LogEvent(string message, EventCategory category, User usr)
        {
            try
            {
                EVSServiceErrorEmail email = new EVSServiceErrorEmail(this._settingsService.GetSettingValue(SettingKeyName.EVSFailNotifyEmail))
                {
                    Message = message,
                    UserId = usr.UserID.ToString(),
                    UserName = usr.FullName,
                };
                this._messageService.AddEmail(email, CultureCode.en_US);
            }
            finally
            {
                this._eventLogService.LogError(message, DateTime.Now, category.ToString(), EventLogSource.EVS);
            }
        }

        /// <summary>
        /// Saves verification details info with xml message to DB
        /// </summary>
        /// <param name="usr">User</param>
        /// <param name="IsVerified"></param>
        /// <param name="evsResponse"></param>
        private void SetVerificationDetails(User usr, bool IsVerified, IEVSResponse evsResponse)
        {
            string responeXML = string.Empty;

            XmlSerializer xmlSerializer = new XmlSerializer(evsResponse.GetType());
            using (StringWriter sw = new StringWriter())
            {
                xmlSerializer.Serialize(sw, evsResponse);
                responeXML = sw.ToString();
            }

            var identValidation = new IdentityVerificationDetail()
            {
                User = usr,
                VerificationDate = DateTime.Now,
                VerificationPositive = IsVerified,
                VerificationDetails = responeXML
            };
            usr.IdentityVerificationDetails.Add(identValidation);
        }

        #endregion

    }
}
