﻿using BusinessLogic.ServiceContracts;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace BusinessLogic.Services
{    
    /// <summary>
    /// Implementation of the IContextService (storing contextual information) that uses HttpContext. For use only in web environment.
    /// </summary>
    public class HttpContextService : IContextService
    {
        private const string DATABASE_CONTEXT_KEY = "EFContext";

        public object Current {
            get
            {
                return HttpContext.Current;
            }
        }

        public object GetFromContext(string key)
        {
            return HttpContext.Current.Items[key];
        }

        public void AddToContext(string key, object value)
        {
            HttpContext.Current.Items[key] = value;
        }

        public OTPEntities GetDBContext()
        {
            return this.GetFromContext(DATABASE_CONTEXT_KEY) as OTPEntities;
        }

        public void RemoveFromContext(string key)
        {
            if (HttpContext.Current.Items[key] != null)
            {
                HttpContext.Current.Items[key] = null;
            }
        }
    }
}
