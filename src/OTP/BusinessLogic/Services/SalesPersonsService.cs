﻿using BusinessLogic.ServiceContracts;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Service class for SalesPersons DB table, performing basic CRUD operations
    /// </summary>
    public class SalesPersonsService : BaseService, ISalesPersonsService
    {
        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="SalesPersonsService"/> class.
        /// </summary>
        /// <param name="contextService">Services context</param>
        public SalesPersonsService(IContextService contextService)
            : base(contextService) 
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SalesPersonsService"/> class.
        /// </summary>
        /// <param name="context">EF context</param>
        public SalesPersonsService(OTPEntities context)
            : base(context)
        { }

        #endregion

        #region ISalesPersonsService implementation

        /// <summary>
        /// Gets SalesPerson by its Id
        /// </summary>
        /// <param name="id">SalesPerson's Id</param>
        /// <returns>SalesPerson retrieved</returns>
        public SalesPerson GetSalesPersonById(int id)
        {
            try
            {
                return this._Context.SalesPersons.Where(sp => sp.SalesPersonId == id).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Can't retrive Sales Person with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Adds Sales Person
        /// </summary>
        /// <param name="salesPerson">Sales Person to add</param>
        public void AddSalesPerson(SalesPerson salesPerson)
        {
            try
            {
                this._Context.SalesPersons.Add(salesPerson);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Can't add Sales Person", ex);
            }
        }

        /// <summary>
        /// Deletes Sales Person from DB table by its Id
        /// </summary>
        /// <param name="id">Sales Person's Id</param>
        public void DeleteSalesPerson(int id)
        {
            try
            {
                SalesPerson salesPerson = GetSalesPersonById(id);
                this._Context.SalesPersons.Remove(salesPerson);
                this._Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Can't delete Sales Person with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets all Sales Person from DB table
        /// </summary>
        /// <returns>Collection of DB Sales Persons</returns>
        public IEnumerable<SalesPerson> GetSalesPersons()
        {
            return this._Context.SalesPersons.AsEnumerable().OrderBy(sp => sp.LastName);
        }

        #endregion

    }
}
