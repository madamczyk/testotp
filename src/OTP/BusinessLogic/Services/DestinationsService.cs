﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Services class for managing destinations. Contains basic CRUD operations.
    /// </summary>
    public class DestinationsService : BaseService, IDestinationsService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DestinationsService"/> class.
        /// </summary>
        public DestinationsService(IConfigurationService configurationService, IContextService httpContextService)
            : base(httpContextService, new Dictionary<Type, object>() {{ 
                typeof(IConfigurationService), configurationService }})
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DestinationsService"/> class.
        /// </summary>
        public DestinationsService(IConfigurationService configurationService, OTPEntities ctx)
            : base(new Dictionary<Type, object>() {{ 
                typeof(IConfigurationService), configurationService }}, ctx)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DestinationsService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public DestinationsService(OTPEntities ctx)
            : base(ctx)
        { }

        /// <summary>
        /// Gets all destinations
        /// </summary>
        /// <returns>List of all languages</returns>
        public IEnumerable<Destination> GetDestinations()
        {
            var list =
                _Context.AutoRetryQuery<IEnumerable<Destination>>(() =>
                _Context.Destinations.AsEnumerable().OrderBy(p => p.DestinationNameCurrentLanguage));
            return list;
        }

        /// <summary>
        /// Gets top X, active destinations ordered by number of their properties
        /// </summary>
        /// <param name="numberOfDestinationsToTake">Number of destinations to take</param>
        /// <returns>List of top x destinations</returns>
        public IEnumerable<Destination> GetTopXDestinationsByPropertiesCount(int numberOfDestinationsToTake)
        {
            return
                _Context.AutoRetryQuery<IEnumerable<Destination>>(() =>
                _Context.Destinations.Where(d => d.Active == true).OrderByDescending(d => d.Properties.Count()).Take(numberOfDestinationsToTake)).AsEnumerable();
        }

        /// <summary>
        /// Gets all matches destinations which are active
        /// </summary>
        /// <returns>List of all languages</returns>
        public IEnumerable<Destination> GetMatchesDestinations(string destinationPartName)
        {
            var list =
                _Context.AutoRetryQuery<IEnumerable<Destination>>(() =>
                _Context.Destinations.AsEnumerable().Where(p => p.Active == true && p.DestinationNameCurrentLanguage.ToLower().StartsWith(destinationPartName.ToLower())).OrderBy(p => p.DestinationNameCurrentLanguage));
            return list;
        }

        /// <summary>
        /// Gets destination by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Destination GetDestinationById(int id)
        {
            return 
                _Context.AutoRetryQuery<Destination>( () =>
                _Context.Destinations.Where(d => d.DestinationID == id).SingleOrDefault());
        }

        /// <summary>
        /// Add Destination
        /// </summary>
        public void AddDestination(Destination d)
        {
            try
            {
                _Context.Destinations.Add(d);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Destination", ex);
            }
        }

        /// <summary>
        /// Delete Destination
        /// </summary>
        /// <param name="id">internal id of the Destination</param>
        public void DeleteDestination(int id)
        {
            try
            {
                Destination destination = this.GetDestinationById(id);
                _Context.Destinations.Remove(destination);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Destination with id {0}", id), ex);
            }
        }

        public bool HasAnyDependencies(int id)
        {
            return _Context.Taxes.Any(t => t.Destination.DestinationID == id) ||
                _Context.ServiceProvidersDestinations.Any(spd => spd.Destination.DestinationID == id) ||
                _Context.Properties.Any(p => p.Destination.DestinationID == id);
        }
    }
}
