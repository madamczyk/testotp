﻿using BusinessLogic.Objects;
using BusinessLogic.ServiceContracts;
using Common.Exceptions;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Serialization;

namespace BusinessLogic.Services
{
    public class SendGridEmailService : IEmailService
    {
        #region Ctor

        public SendGridEmailService()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method sends email using SendGrid WebAPI
        /// </summary>
        /// <param name="dbMessage">Message from the database</param>
        /// <param name="account">Account used to send emails</param>
        /// <param name="attachments">Attachments for the email</param>
        public void SendEmail(Message dbMessage, EmailAccount account, IEnumerable<Attachment> attachments = null)
        {
            char[] emailAddressSeparators = ",".ToCharArray();
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();

            parameters.Add(new KeyValuePair<string, string>("api_user", HttpUtility.UrlEncode(account.UserName)));
            parameters.Add(new KeyValuePair<string, string>("api_key", HttpUtility.UrlEncode(account.Password)));

            parameters.Add(new KeyValuePair<string, string>("from", HttpUtility.UrlEncode(dbMessage.AddressFrom)));
            parameters.Add(new KeyValuePair<string, string>("fromname", HttpUtility.UrlEncode(dbMessage.DisplayFrom)));

            if (!String.IsNullOrWhiteSpace(dbMessage.ReplyTo))
                parameters.Add(new KeyValuePair<string, string>("replyto", HttpUtility.UrlEncode(dbMessage.ReplyTo)));

            foreach (var to in dbMessage.Recipients.Split(emailAddressSeparators, StringSplitOptions.RemoveEmptyEntries).ToList())
                parameters.Add(new KeyValuePair<string, string>("to[]", HttpUtility.UrlEncode(to)));

            if (!String.IsNullOrWhiteSpace(dbMessage.CC))
                foreach (var to in dbMessage.CC.Split(emailAddressSeparators, StringSplitOptions.RemoveEmptyEntries).ToList())
                    parameters.Add(new KeyValuePair<string, string>("to[]", HttpUtility.UrlEncode(to))); // no CC parameter - http://sendgrid.com/docs/API_Reference/Web_API/mail.html

            if (!String.IsNullOrWhiteSpace(dbMessage.BCC))
                foreach (var to in dbMessage.BCC.Split(emailAddressSeparators, StringSplitOptions.RemoveEmptyEntries).ToList())
                    parameters.Add(new KeyValuePair<string, string>("bcc[]", HttpUtility.UrlEncode(to)));

            string body = EmbedImages(dbMessage.Body, parameters);

            parameters.Add(new KeyValuePair<string, string>("subject", dbMessage.Subject));
            parameters.Add(new KeyValuePair<string, string>("html", HttpUtility.UrlEncode(body)));

            AttachFiles(attachments, parameters);

            try
            {
                //Create Request
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(account.Server);
                myHttpWebRequest.Method = "POST";
                myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";

                // Create a new write stream for the POST body
                StreamWriter streamWriter = new StreamWriter(myHttpWebRequest.GetRequestStream());

                // Prepare parameters
                string parametersString = String.Join("&", parameters.Select(p => p.Key + "=" + p.Value).ToArray());

                // Write the parameters to the stream
                streamWriter.Write(parametersString);
                streamWriter.Flush();
                streamWriter.Close();

                // Get the response
                WebAPIResponse response = GetWebAPIResponse((HttpWebResponse)myHttpWebRequest.GetResponse());

                // result should be success
                if (response.Message != "success")
                    throw new Exception("SendGrid: WebAPI call result was received, but message is wrong, Got" + response.Message);
            }
            catch (WebException ex) // result is error
            {
                // Catch any execptions and gather the response
                WebAPIResponse response = GetWebAPIResponse((HttpWebResponse)ex.Response);

                throw new EmailCriticalException("SendGrid: WebAPI call was unsuccessful: " + response.Message + Environment.NewLine + "Errors received:" + Environment.NewLine + String.Join(Environment.NewLine, response.Errors), ex);
            }
        }

        #endregion

        #region Private methods

        private WebAPIResponse GetWebAPIResponse(HttpWebResponse response)
        {
            // Create a new read stream for the response body and read it
            StreamReader streamReader = new StreamReader(response.GetResponseStream());
            XmlSerializer serializer = new XmlSerializer(typeof(WebAPIResponse));
            
            return (WebAPIResponse)serializer.Deserialize(streamReader);
        }

        private void AttachFiles(IEnumerable<Attachment> attachments, List<KeyValuePair<string, string>> parameters)
        {
            DocumentType docType;
            string name, data;
            foreach (var att in attachments)
            {
                docType = (DocumentType)att.Type;

                switch (docType)
                {
                    case DocumentType.PDF:
                        data = HttpUtility.UrlEncode(att.Content);
                        name = att.Title + ".pdf";
                        break;

                    default:
                        throw new ArgumentException("DocumentType of given Attachment is not supported");
                }

                parameters.Add(new KeyValuePair<string, string>("files[" + name + "]", data));
            }
        }

        private string EmbedImages(string body, List<KeyValuePair<string, string>> parameters)
        {
            MatchCollection matches;
            MemoryStream ms;
            Image image;
            string fileId, fileData;
            string imgPattern = "src=\"cid:[^\"]*\"";
            string externalImgPattern = "src=\"ecid:[^\"]*\"";

            // images from resources - logos
            matches = Regex.Matches(body, imgPattern);
            foreach (Match m in matches)
            {
                fileId = m.Value.Replace("src=\"cid:", "").Replace("\"", "");
                if (parameters.Where(p => p.Key == "files[" + fileId + ".png]").Count() > 0)
                    continue;

                image = (Image)Common.Resources.Logos.ResourceManager.GetObject(fileId);
                if (image == null)
                    continue;

                ms = new MemoryStream();
                image.Save(ms, ImageFormat.Png);
                fileData = HttpUtility.UrlEncode(ms.ToArray());

                parameters.Add(new KeyValuePair<string, string>("files[" + fileId + ".png]", fileData));
                parameters.Add(new KeyValuePair<string, string>("content[" + fileId + ".png]", fileId));
            }

            string fileExtension, fileUrl;

            // external images
            matches = Regex.Matches(body, externalImgPattern);
            foreach (Match m in matches)
            {
                fileUrl = m.Value.Replace("src=\"ecid:", "").Replace("\"", "");
                fileExtension = fileUrl.Substring(fileUrl.LastIndexOf("."));
                fileId = Guid.NewGuid().ToString();

                image = Image.FromStream(GetWebResponse(fileUrl));

                ms = new MemoryStream();
                image.Save(ms, ImageFormat.Png);
                fileData = HttpUtility.UrlEncode(ms.ToArray());

                parameters.Add(new KeyValuePair<string, string>("files[" + fileId + ".png]", fileData));
                parameters.Add(new KeyValuePair<string, string>("content[" + fileId + ".png]", fileId));

                body = body.Replace(m.Value,
                        string.Format("src=\"cid:{0}\"",
                        fileId));
            }

            return body;
        }

        private Stream GetWebResponse(string imgUrl)
        {
            // Create a 'WebRequest' object with the specified url. 
            WebRequest webRequest = WebRequest.Create(imgUrl);

            // Send the 'WebRequest' and wait for response.
            WebResponse webResponse = webRequest.GetResponse();

            // Obtain a 'Stream' object associated with the response object.
            Stream receiveStream = webResponse.GetResponseStream();

            return receiveStream;
        }

        #endregion
    }
}
