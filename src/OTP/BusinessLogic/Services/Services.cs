﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data;
using BusinessLogic.Enums;
using System.Data.Entity.Validation;
using System.Data.Objects.DataClasses;
using BusinessLogic.ServiceContracts;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace BusinessLogic.Services
{
    public class Services : IDisposable
    {
        #region Private Fields

        private OTPEntities _context;
        private readonly IAuthorizationService _authorizationService;
        private readonly IUserService _usersService;
        private readonly IRolesService _rolesService;
        private readonly IConfigurationService _configurationService;
        private readonly IDestinationsService _destinationService;
        private readonly IAmenityService _amenitiesService;
        private readonly ITagsService _tagsService;
        private readonly ITaxesService _taxesService;
        private readonly IPropertyTypesService _propertyTypeService;
        private readonly IStateService _stateService;
        private readonly IMessageService _messageService;
        private readonly IStorageQueueService _storageQueueService;
        private readonly IStorageTableService _storageTableService;
        private readonly ISettingsService _settingsService;
        private readonly IEmailService _emailService;
        private readonly IPropertiesService _propertiesService;
        private readonly ISiteMapService _siteMapService;
        private readonly IEventLogService _eventLogService;
        private readonly IAmenityGroupsService _amenityGroupsService;
        private readonly IUnitTypesService _unitTypeService;
		private readonly IUnitsService _unitsService;
        private readonly IPropertyAddOnsService _propertyAddOnsService;
        private readonly IBlobService _blobService;
        private readonly IAppliancesService _appliancesService;
        private readonly IDictionaryCountryService _dictionaryCountryService;
        private readonly IScheduledVisitsService _scheduledVisitService;
        private readonly ITagGroupsService _tagGroupsService;
        private readonly IBookingService _bookingService;
        private readonly IReservationsService _reservationsService;
        private readonly IDocumentService _pdfDocumentService;
        private readonly IScheduledTasksService _scheduledTasksService;
        private readonly ITaskSchedulerEventLogService _taskSchedulerEventLogService;
        private readonly IPaymentGatewayService _paymentGatewayService;
        private readonly IPaymentGatewayOperationLogService _paymentGatewayOperationLogService;
        private readonly IDictionaryCultureService _dictionaryCultureService;
        private readonly ISalesPersonsService _salesPersonsService;
        private readonly IZohoCrmService _zohoCrmService;
        private readonly ICrmOperationLogsService _crmOperationLogsService;

        #endregion

        #region Services

        /// <summary>
        /// Gets the entity context
        /// </summary>
        internal OTPEntities Context 
        {
            get { return _context; }
        }

        public IUnitsService UnitsService
        {
            get { return _unitsService; }
        }

        /// <summary>
        /// Gets the authorization service
        /// </summary>
        public IAuthorizationService AuthorizationService
        {
            get { return _authorizationService; }
        }

        /// <summary>
        /// Gets the users service
        /// </summary>
        public IUserService UsersService
        {
            get { return _usersService; }
        }

        /// <summary>
        /// Gets the language dictionary service
        /// </summary>
        public IRolesService RolesService
        {
            get { return _rolesService; }
        }

        /// <summary>
        /// Gets the destination service
        /// </summary>
        public IDestinationsService DestinationService
        {
            get { return _destinationService; }
        }

        /// <summary>
        /// Gets the amenity service
        /// </summary>
        public IAmenityService AmenityService 
        { 
            get { return _amenitiesService; } 
        }

        /// <summary>
        /// Gets the tags service
        /// </summary>
        public ITagsService TagsService
        {
            get { return _tagsService; }
        }

        /// <summary>
        /// Gets the tags service
        /// </summary>
        public ITaxesService TaxesService
        {
            get { return _taxesService; }
        }

        /// <summary>
        /// Gets the property type service
        /// </summary>
        public IPropertyTypesService PropertyTypeService
        {
            get { return _propertyTypeService; }
        }

        /// <summary>
        /// Gets the state service
        /// </summary>
        public IStateService StateService
        {
            get { return _stateService; }
        }

        /// <summary>
        /// Gets the message service
        /// </summary>
        public IMessageService MessageService
        {
            get { return _messageService; }
        }

        /// <summary>
        /// Gets the storage queue service
        /// </summary>
        public IStorageQueueService StorageQueueService
        {
            get { return _storageQueueService; }
        }

        /// <summary>
        /// Gets the table service
        /// </summary>
        public IStorageTableService StorageTableService
        {
            get { return _storageTableService; }
        }

        /// <summary>
        /// Gets the configuration service
        /// </summary>
        public IConfigurationService ConfigurationService
        {
            get { return _configurationService; }
        }

        /// <summary>
        /// Gets the settings service
        /// </summary>
        public ISettingsService SettingsService
        {
            get { return _settingsService; }
        }

        /// <summary>
        /// Gets the email service
        /// </summary>
        public IEmailService EmailService
        {
            get { return _emailService; }
        }

        /// <summary>
        /// Gets the properties service
        /// </summary>
        public IPropertiesService PropertiesService
        {
            get { return _propertiesService; }
        }

        /// <summary>
        /// Gets the intranet site map service
        /// </summary>
        public ISiteMapService SiteMapService
        {
            get { return _siteMapService; }
        }

        /// <summary>
        /// Gets the event log service
        /// </summary>
        public IEventLogService EventLogService
        {
            get { return _eventLogService; }
        }

        /// <summary>
        /// Gets amenity group service
        /// </summary>
        public IAmenityGroupsService AmenityGroupsService
        {
            get { return _amenityGroupsService; }
        }

        /// <summary>
        /// Gets unit types service
        /// </summary>
        public IUnitTypesService UnitTypesService
        {
            get { return _unitTypeService; }
        }

        public IPropertyAddOnsService PropertyAddOnsService
        {
            get { return _propertyAddOnsService; }
        }

        public IBlobService BlobService
        {
            get { return _blobService;  }
        }

        public IAppliancesService AppliancesService
        {
            get { return _appliancesService; }
        }

        public IDictionaryCountryService DictionaryCountryService
        {
            get { return _dictionaryCountryService; }
        }

        public IScheduledVisitsService ScheduledVisitService
        {
            get { return _scheduledVisitService; }
        }

        public ITagGroupsService TagGroupsService
        {
            get { return _tagGroupsService;}
        }

        public IBookingService BookingService
        {
            get { return _bookingService; }
        }

        public IDocumentService PdfDocumentService
        {
            get { return _pdfDocumentService; }
        }
        
        public IReservationsService ReservationsService
        {
            get { return _reservationsService; }
        }
        
        public IScheduledTasksService ScheduledTasksService
        {
            get { return _scheduledTasksService; }
        }

        public ITaskSchedulerEventLogService TaskSchedulerEventLogService
        {
            get { return _taskSchedulerEventLogService; }
        }

        public IPaymentGatewayService PaymentGatewayService
        {
            get { return _paymentGatewayService; }
        }

        public IPaymentGatewayOperationLogService PaymentGatewayOperationLogService
        {
            get { return _paymentGatewayOperationLogService; }
        }

        public IDictionaryCultureService DictionaryCultureService
        {
            get { return _dictionaryCultureService; }
        }

        public ISalesPersonsService SalesPersonsService
        {
            get { return _salesPersonsService; }
        }

        public IZohoCrmService ZohoCrmService
        {
            get { return _zohoCrmService; }
        }

        public ICrmOperationLogsService CrmOpertionLogsService
        {
            get
            {
                return _crmOperationLogsService;
            }
        }

        #endregion

        #region Constructor
       
        public Services()
        {
            this._context = new OTPEntities(this._configurationService.GetKeyValue(CloudConfigurationKeys.DBConnectionString));
        }
        
        /// <summary>
        /// Initialize a new instance of the <see cref="Services"/> class.
        /// </summary>
        public Services (string connString)
	    {
            this._context = CreateContext(connString);

            //Initalize services
            this._configurationService = new ConfigurationService(this._context);
            this._settingsService = new SettingsService(this._context);
            this._messageService = new MessageService(new StorageQueueService(new ConfigurationService(this._context), new HttpContextService()), new SettingsService(this._context), this._context);
            this._crmOperationLogsService = new CrmOperationLogsService(this._context);
            this._zohoCrmService = new ZohoCrmService(this._context, new SettingsService(this._context), new CrmOperationLogsService(this._context));
            this._authorizationService = new AuthorizationService(new UsersService(this._context, new ZohoCrmService(this._context, new SettingsService(this._context), new CrmOperationLogsService(this._context)), new MessageService(new StorageQueueService(new ConfigurationService(this._context), new HttpContextService()), new SettingsService(this._context), this._context), _settingsService), this._context);
            this._usersService = new UsersService(this._context, this._zohoCrmService, this._messageService, _settingsService);
            this._rolesService = new RolesService(this._context);
            this._destinationService = new DestinationsService(new ConfigurationService(this._context), this._context);
            this._amenitiesService = new AmenityService(this._context);
            this._tagsService = new TagsService(this._context);
            this._taxesService = new TaxesService(this._context);
            this._propertyTypeService = new PropertyTypeService(this._context);
            this._stateService = new StateService(new HttpContextService());
            this._storageQueueService = new StorageQueueService(new ConfigurationService(this._context), new HttpContextService());
            this._storageTableService = new StorageTableService(new ConfigurationService(this._context), new HttpContextService());
            this._blobService = new BlobService(new ConfigurationService(this._context), new HttpContextService());
            this._emailService = new SendGridEmailService();
            this._propertiesService = new PropertiesServices(this._context, this._configurationService, this._blobService, this._settingsService);
            this._siteMapService = new SiteMapService(this._context);
            this._eventLogService = new EventLogService(this._context);
            this._amenityGroupsService = new AmenityGroupsService(this._context);
            this._unitTypeService = new UnitTypesService(this._context);
            this._unitsService = new UnitsService(this._context);
            this._propertyAddOnsService = new PropertyAddOnsService(this._context);
            this._appliancesService = new AppliancesService(this._context);
            this._dictionaryCountryService = new DictionaryCountryService(this._context);
            this._scheduledVisitService = new ScheduledVisitService(this._context);
            this._tagGroupsService = new TagGroupsService(this._context);
            this._paymentGatewayService = new PaymentGatewayService(this._context, this._eventLogService, this._settingsService, this._messageService);
            this._bookingService = new BookingService(this._paymentGatewayService, this._messageService, this._settingsService,this._context);
            this._reservationsService = new ReservationsService(this._context, this._configurationService, this._blobService, this._bookingService, this._settingsService, this._messageService, this._unitsService);
            this._pdfDocumentService = new PdfDocumentService(this._context);
            this._scheduledTasksService = new ScheduledTasksService(this._context);
            this._taskSchedulerEventLogService = new TaskSchedulerEventLogService(this._context);            
            this._paymentGatewayOperationLogService = new PaymentGatewayOperationLogService(this._context);
            this._dictionaryCultureService = new DictionaryCultureService(this._context);
            this._salesPersonsService = new SalesPersonsService(this._context);
        }

        #endregion

        #region Methods

        public OTPEntities CreateContext(string connString)
        {
            return new OTPEntities(string.IsNullOrEmpty(connString) ? RoleEnvironment.GetConfigurationSettingValue(CloudConfigurationKeys.DBConnectionString.ToString()) : connString);
        }
        
        /// <summary>
        /// Loads entity object's property by it's name
        /// </summary>
        /// <param name="entity">EF entity object</param>
        /// <param name="propertyName">Property name to be loaded</param>
        public void LoadEntityProperty(object entity, string propertyName)
        {
            _context.Entry(entity).Reference(propertyName).Load();
        }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        public void SaveChanges()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                //throw new Exception("Can't save context changes", dbEx);

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        throw new Exception(string.Format("Can't save context changes. Reason: {0}", validationError.ErrorMessage), dbEx);
                    }
                }
            }
        }

        public void DisposeContext()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = CreateContext(null);
            }            
        }

        public void Dispose()
        {
            DisposeContext();
        }

        #endregion
    }
}
