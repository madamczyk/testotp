﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class SiteMapService : BaseService, ISiteMapService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AmenityService"/> class.
        /// </summary>
        public SiteMapService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteMapService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public SiteMapService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Gets root node.
        /// </summary>
        /// <returns>Root node</returns>
        public IntranetSiteMapAction GetRootAction()
        {
            return _Context.IntranetSiteMapActions.Where(ob => ob.ParentIntranetSiteMapAction == null).FirstOrDefault();
        }
    }
}
