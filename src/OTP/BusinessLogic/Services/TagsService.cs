﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class TagsService : BaseService, ITagsService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TagsService"/> class.
        /// </summary>
        public TagsService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="TagsService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public TagsService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Get Tag by its internal Id
        /// </summary>
        /// <param name="id">internal id of the Tag</param>
        /// <returns>Tag retrieved</returns>
        public Tag GetTagById(int id)
        {
            try
            {
                return this._Context.Tags.Where(p => p.TagID == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get Tag with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Add Tag
        /// </summary>
        /// <param name="p">Tag to add</param>
        public void AddTag(Tag p)
        {
            try
            {
                _Context.Tags.Add(p);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Tag", ex);
            }
        }

        /// <summary>
        /// Delete Tag
        /// </summary>
        /// <param name="id">internal id of the Tag</param>
        public void DeleteTag(int id)
        {
            try
            {
                Tag tag = this.GetTagById(id);
                _Context.Tags.Remove(tag);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Tag with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets all tags
        /// </summary>
        /// <returns>List of all tags</returns>
        public IEnumerable<Tag> GetTags()
        {
            var list = _Context.Tags.AsEnumerable().OrderBy(t => t.NameCurrentLanguage);
            return list;
        }
    }
}
