﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using DataAccess.Enums;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class RolesService : BaseService, IRolesService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RolesService"/> class.
        /// </summary>
        public RolesService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RolesService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public RolesService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Gets all roles
        /// </summary>
        /// <returns>List of all roles</returns>
        public IEnumerable<Role> GetRoles()
        {
            return _Context.Roles.AsEnumerable().OrderBy(p => p.Name);
        }


        /// <summary>
        /// Gets Role by id
        /// </summary>
        /// <param name="roleId">Role Id</param>
        /// <returns></returns>
        public Role GetRoleById(int roleId)
        {
            return _Context.Roles.AsEnumerable().Where(l => l.RoleId == roleId).SingleOrDefault();
        }

        /// <summary>
        /// Gets Role by RoleLevel
        /// </summary>
        /// <param name="roleLevel">Role Level</param>
        /// <returns></returns>
        public Role GetRoleByRoleLevel(RoleLevel roleLevel)
        {
            return _Context.Roles.AsEnumerable().Where(l => l.RoleLevel == (int)roleLevel).SingleOrDefault();
        }

        /// <summary>
        /// Adds user to role
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roleLevel"></param>
        /// <returns></returns>
        public UserRole AddUserToRole(User user, RoleLevel roleLevel)
        {
            Role role = GetRoleByRoleLevel(roleLevel);
            if (role != null)
            {
                return new UserRole()
                {
                    Role = role,
                    User = user
                };
            }
            return null;
        }
    }
}
