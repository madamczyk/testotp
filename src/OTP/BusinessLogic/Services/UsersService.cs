﻿using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.Objects.ZohoCrm;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BusinessLogic.Services
{
    public class UsersService : BaseService, IUserService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UsersService"/> class.
        /// </summary>
        public UsersService(IContextService httpContextService, 
            IZohoCrmService zohoService, 
            IMessageService messageService, 
            ISettingsService settingsService)
            : base(httpContextService, new Dictionary<Type, object>
            {
                {typeof(IZohoCrmService), zohoService},
                {typeof(IMessageService), messageService},
                {typeof(ISettingsService), settingsService}
            })
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersService"/> class.
		/// </summary>
        /// <param name="ctx">EF context.</param>
        public UsersService(OTPEntities ctx, 
            IZohoCrmService zohoService, 
            IMessageService messageService,
            ISettingsService settingsService)
            : base(new Dictionary<Type, object>
            {
                {typeof(IZohoCrmService), zohoService},
                {typeof(IMessageService), messageService},
                {typeof(ISettingsService), settingsService}
            }, ctx)
		{ }

        /// <summary>
        /// Gets all products
        /// </summary>
        /// <returns>List of all users present in DB</returns>
        public IQueryable<User> GetUsers()
        {
            return _Context.Users.AsQueryable().OrderBy(p => p.Lastname);
        }

        /// <summary>
        /// Gets user by email.
        /// </summary>
        /// <param name="login">User login</param>
        /// <returns>User with specified login</returns>
        public User GetUserByEmail(string email)
        {
            return _Context.Users.Where(u => u.email == email).FirstOrDefault();
        }
        
        /// <summary>
        /// Add user
        /// </summary>
        /// <returns>user</returns>
        public void AddUser(User u)
        {
            try
            {
                _Context.Users.Add(u);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add user", ex);
            }
        }

        /// <summary>
        /// Add homeowner to CRM and DataBase
        /// </summary>
        /// <param name="u">User in homeowner role</param>
        public void AddHomeowner(User u)
        {
            try
            {
                _Context.Users.Add(u);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add user", ex);
            }

            // Return if zoho authorization token doesn't exist
            if (string.IsNullOrWhiteSpace(_settingsService.GetSettingValue(Enums.SettingKeyName.ZohoCrmAuthorizationToken)))
            {
                return;
            }

            ZohoCrmResponse response = _zohoCrmService.AddZohoCrmLead(u);
            ZohoCrmFieldLabel field = response.Result != null ? response.Result.RecordDetail.Fields.SingleOrDefault(f => f.ValAttribute == "Id") : null;

            if (response.Error != null || field == null) // Add lead error
            {
                CrmAddLeadOperationErrorEmail email = new CrmAddLeadOperationErrorEmail(_settingsService.GetSettingValue(Enums.SettingKeyName.EmailAccountOTPSupport));
                email.Address1 = u.Address1;
                email.Address2 = u.Address2;
                email.FirstName = u.Firstname;
                email.LastName = u.Lastname;
                _messageService.AddEmail(email, CultureCode.en_US);
            }
            else
            {
                ZohoCrmConversionResponse convertResponse = null;

                if (field != null)
                {
                    convertResponse = _zohoCrmService.ConvertZohoCrmLeadToAccount(field.Value, u);
                    if (convertResponse == null)    // Conversion error
                    {
                        CrmConvertLeadOperationErrorEmail email = new CrmConvertLeadOperationErrorEmail(_settingsService.GetSettingValue(Enums.SettingKeyName.EmailAccountOTPSupport));
                        email.Address1 = u.Address1;
                        email.Address2 = u.Address2;
                        email.FirstName = u.Firstname;
                        email.LastName = u.Lastname;
                        _messageService.AddEmail(email);
                    }
                }
            }
        }

        /// <summary>
        /// Changes password for user specified in userId param
        /// </summary>
        /// <param name="userId">User's id to change password</param>
        /// <param name="hashedOldPassword">plain old password</param>
        /// <param name="hashedNewPassword">plain new password</param>
        public void ChangePassword(int userId, string oldPassword, string newPassword)
        {
            User user = _Context.Users.SingleOrDefault(u => u.UserID == userId);

            string hashedOldPassword = Common.MD5Helper.Encode(oldPassword);
            string hashedNewPassword = Common.MD5Helper.Encode(newPassword);

            if (user == null)
            {
                throw new ApplicationException("User does not exist in database");
            }

            if (string.Compare(hashedOldPassword, user.Password) != 0)
            {
                throw new ApplicationException("Your actual password is invalid");
            }

            user.Password = hashedNewPassword;
            _Context.SaveChanges();

        }

        /// <summary>
        /// Get all users with specified role
        /// </summary>
        /// <param name="role">Role name</param>
        /// <returns>List of users</returns>
        public IEnumerable<User> GetUsersByRole(RoleLevel role)
        {
            return _Context.UserRoles.Where(ur => ur.Role.RoleLevel == (int)role).Select(r => r.User).OrderBy(u => u.UserID).AsEnumerable();
        }

        /// <summary>
        /// Checks if given password satisfy the conditions.
        /// </summary>
        /// <param name="pass">New password to check</param>
        /// <returns>True - if conditions are satisfied.</returns>
        public bool IsPasswordValid(string password)
        {
            Match strongPasswordMatch1 = Regex.Match(password, @"^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[;@#$%^&~,`\\""'<>^$.?*!+:=()\[\]{}|\\/]).*$");
            Match strongPasswordMatch2 = Regex.Match(password, @"^.*(?=.*\d)(?=.*[A-Z])(?=.*[;@#$%^&~,`\\""'<>^$.?*!+:=()\[\]{}|\\/]).*$");
            Match strongPasswordMatch3 = Regex.Match(password, @"^.*(?=.*\d)(?=.*[a-z])(?=.*[;@#$%^&~,`\\""'<>^$.?*!+:=()\[\]{}|\\/]).*$");
            Match strongPasswordMatch4 = Regex.Match(password, @"^.*(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$");

            if (!string.IsNullOrWhiteSpace(password) && password.Length >= 8 && (strongPasswordMatch1.Success || strongPasswordMatch2.Success || strongPasswordMatch3.Success
                || strongPasswordMatch4.Success))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets user by activation token
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserRole GetUserRoleByActivationToken(string tokenId)
        {
            return _Context.UserRoles.Where(u => u.ActivationToken == tokenId).SingleOrDefault();
        }

        /// <summary>
        /// Gets user by id
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public User GetUserByUserId(int p)
        {
            return _Context.Users.Where(u => u.UserID == p).FirstOrDefault();
        }

        /// <summary>
        /// Check, whether user has any dependencies
        /// </summary>
        /// <param name="userId">internal ID of the user</param>
        /// <returns></returns>
        public bool HasAnyDependencies(int userId)
        {
            return _Context.Properties.Any(p => p.User.UserID == userId) ||
                _Context.GuestPaymentMethods.Any(gpm => gpm.User.UserID == userId) ||
                _Context.GuestReviews.Any(gr => gr.User.UserID == userId) ||
                _Context.Reservations.Any(r => r.User.UserID == userId) ||
                _Context.ReservationBillingAddresses.Any(rba => rba.User.UserID == userId) ||
                _Context.PropertyAssignedManagers.Any(pam => pam.User.UserID == userId);
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="id">internal id of the user</param>
        public void DeleteUserByRoleId(int id)
        {
            try
            {
                User user = this.GetUserByUserRoleId(id).User;
                
                _Context.Users.Remove(user);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete User with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Delete User Role
        /// </summary>
        /// <param name="id">internal id of the user role</param>
        public void DeleteUserRoleByRoleId(int id)
        {
            try
            {
                UserRole userRole = _Context.UserRoles.Where(ur => ur.UserRoleId == id).SingleOrDefault();
                _Context.UserRoles.Remove(userRole);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete User Role with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="id">internal id of the user</param>
        public void DeleteUserById(int id)
        {
            try
            {
                User user = this.GetUserByUserId(id);

                _Context.Users.Remove(user);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete User with id {0}", id), ex);
            }
        }

        public IEnumerable<GuestPaymentMethod> GetInactiveGuestPaymentMethods(int days)
        {
            var inactivePeriod = DateTime.Now.AddDays(-days);

            return _Context.GuestPaymentMethods
                .Where(gpm => !gpm.Reservations.Any(r => r.DateDeparture >= inactivePeriod))
                .ToList();
        }

        public void RemoveGuestPaymentMethod(int id, int userId)
        {
            try
            {
                User user = this.GetUserByUserId(userId);
                GuestPaymentMethod paymentMethod = user.GuestPaymentMethods.FirstOrDefault(m => m.GuestPaymentMethodID == id);

                //removing reservation connections to guest payment method
                foreach (var reservation in paymentMethod.Reservations)
                {
                    reservation.GuestPaymentMethod = null;
                }
                paymentMethod.Reservations.Clear();

                _Context.GuestPaymentMethods.Remove(paymentMethod);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete GuestPaymentMethod with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets all role levels for user by user id
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleLevel> GetRoleLevelsForUser(int userId, bool onlyActive = false)
        {
            IEnumerable<int> userRoles = new List<int>();
            var query = _Context.UserRoles.Where(userRole => userRole.User.UserID == userId);
            if(onlyActive) {
                int activeRoleStatus = (int)UserStatus.Active;
                query = query.Where(userRole => userRole.Status.Equals(activeRoleStatus));
            }
            userRoles = query.Select(r => r.Role.RoleLevel).ToList();

            return userRoles.Select(r => (RoleLevel)r);
        }

        /// <summary>
        /// Gets user role by level
        /// </summary>
        /// <param name="roleLevel">Role level</param>
        /// <returns>UserRole if exists; null if there is no such role for user</returns>
        public UserRole GetUserRoleByLevel(User user, RoleLevel roleLevel)
        {
            int roleLevelInt = (int)roleLevel;
            return user.UserRoles.Where(r => r.Role.RoleLevel == roleLevelInt && r.User.UserID == user.UserID).SingleOrDefault();
        }

        /// <summary>
        /// Gets all user roles by role level
        /// </summary>
        /// <param name="roleLevel"></param>
        /// <returns></returns>
        public IEnumerable<UserRole> GetUserRolesByRoleLevel(RoleLevel roleLevel)
        {
            int roleId = (int)roleLevel;
            return _Context.UserRoles.Where(ur => ur.Role.RoleLevel == roleId).AsEnumerable();
        }

        /// <summary>
        /// Gets user role by id
        /// </summary>
        /// <param name="id">User Role id</param>
        /// <returns></returns>
        public UserRole GetUserByUserRoleId(int id)
        {
            return _Context.UserRoles.Where(ur => ur.UserRoleId == id).FirstOrDefault();
        }

        /// <summary>
        /// Gets user roles by user id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UserRole> GetUserRolesByUserId(int userId)
        {
            return _Context.UserRoles.Where(ur => ur.User.UserID == userId).AsEnumerable();
        }

        /// <summary>
        /// Gets data from newUser and save its in userFromDb
        /// </summary>
        /// <param name="userFromDb"></param>
        /// <param name="newUser"></param>
        /// <returns></returns>
        public User MergeUserData(User userFromDb, User newUser, bool overwrite = false)
        {
            userFromDb.Lastname = GetNewValue<string>(userFromDb.Lastname, newUser.Lastname, overwrite);
            userFromDb.Firstname = GetNewValue<string>(userFromDb.Firstname, newUser.Firstname, overwrite);
            userFromDb.Address1 = GetNewValue<string>(userFromDb.Address1, newUser.Address1, overwrite);
            userFromDb.Address2 = GetNewValue<string>(userFromDb.Address2, newUser.Address2, overwrite);
            userFromDb.State = GetNewValue<string>(userFromDb.State, newUser.State, overwrite);
            userFromDb.ZIPCode = GetNewValue<string>(userFromDb.ZIPCode, newUser.ZIPCode, overwrite);
            userFromDb.City = GetNewValue<string>(userFromDb.City, newUser.City, overwrite);
            userFromDb.CellPhone = GetNewValue<string>(userFromDb.CellPhone, newUser.CellPhone, overwrite);
            userFromDb.LandLine = GetNewValue<string>(userFromDb.LandLine, newUser.LandLine, overwrite);
            userFromDb.DriverLicenseNbr = GetNewValue<string>(userFromDb.DriverLicenseNbr, newUser.DriverLicenseNbr, overwrite);
            userFromDb.PassportNbr = GetNewValue<string>(userFromDb.PassportNbr, newUser.PassportNbr, overwrite);
            userFromDb.SocialsecurityNbr = GetNewValue<string>(userFromDb.SocialsecurityNbr, newUser.SocialsecurityNbr, overwrite);
            userFromDb.DirectDepositInfo = GetNewValue<string>(userFromDb.DirectDepositInfo, newUser.DirectDepositInfo, overwrite);
            userFromDb.AcceptedTCsInitials = GetNewValue<string>(userFromDb.AcceptedTCsInitials, newUser.AcceptedTCsInitials, overwrite);
            userFromDb.BankName = GetNewValue<string>(userFromDb.BankName, newUser.BankName, overwrite);
            userFromDb.RoutingNumber = GetNewValue<string>(userFromDb.RoutingNumber, newUser.RoutingNumber, overwrite);
            userFromDb.AccountNumber = GetNewValue<string>(userFromDb.AccountNumber, newUser.AccountNumber, overwrite);
            if (userFromDb.DictionaryCountry == null)
            {
                userFromDb.DictionaryCountry = _Context.DictionaryCountries.Where(c => c.CountryId == newUser.DictionaryCountry.CountryId).SingleOrDefault();
            }
            return userFromDb;
        }

        /// <summary>
        /// Compare if oryginal value is not empty and returns new value
        /// </summary>
        /// <typeparam name="T">Type of data</typeparam>
        /// <param name="origValue">Oryginal value</param>
        /// <param name="newValue">New value</param>
        /// <returns>Oryginal value if is not null; New value if oryginal value is empty</returns>
        private T GetNewValue<T>(object origValue, object newValue, bool useNew)
        {
            if(useNew)
                return (T)newValue;

            if (origValue != null && origValue.ToString() != string.Empty)
                return (T)origValue; 

            return (T)newValue;
        }

        /// <summary>
        /// Gets user object by given zip code. 
        /// Returned user is the first from the list of all managers which are assigned to the zip code (by zip code range)
        /// </summary>
        /// <param name="zipCode">Zip code as number (00000 - 99999)</param>
        /// <returns>User object</returns>
        public UserRole GetManagerRoleForZipCode(string zipCode)
        {
            UserRole preferedManagerRole = null;
            int zipCodeInt = int.Parse(zipCode);
            var zipCodeRanges = _Context.ZipCodeRanges.ToList();
            foreach (var zc in zipCodeRanges)
            {
                var start = int.Parse(zc.ZipCodeStart);
                var end = int.Parse(zc.ZipCodeEnd);
                if (zipCodeInt >= start && zipCodeInt <= end)
                {
                    User user = null;
                    if ((user = zc.ManagerZipCodeRanges.Select(users => users.UserRole.User).FirstOrDefault()) != null)
                    {
                        preferedManagerRole = user.UserRoles.Where(ur => ur.Role.RoleLevel == (int)RoleLevel.Manager).SingleOrDefault();
                    }
                    break;
                }
            }
            return preferedManagerRole;
        }

        /// <summary>
        /// Retrieve all zip code ranges for user role 
        /// </summary>
        /// <param name="userRoleId">User Role Id</param>
        /// <returns></returns>
        public IEnumerable<ManagerZipCodeRange> GetZipCodesRangesForUserRole(int userRoleId)
        {
            return _Context.ManagerZipCodeRanges.Where(m => m.UserRole.UserRoleId == userRoleId).AsEnumerable();
        }

        /// <summary>
        /// Gets all zip code ranges from DB
        /// </summary>
        /// <returns>The list of zip code ranges</returns>
        public IEnumerable<ZipCodeRange> GetZipCodeRanges()
        {
            return _Context.ZipCodeRanges.OrderBy(z => z.ZipCodeStart).AsEnumerable();
        }

        /// <summary>
        /// Add zip code manager role for user role
        /// </summary>
        /// <param name="zipCodeRangeId">Id of the zip code range</param>
        /// <param name="userRoleId">Internal User Role Id</param>
        public void AddZipCodeRangeToManager(int zipCodeRangeId, int userRoleId)
        {
            var zipCodeRange = _Context.ZipCodeRanges.Where(z => z.ZipCodeRangeId == zipCodeRangeId).SingleOrDefault();
            var userRole = _Context.UserRoles.Where(r => r.UserRoleId == userRoleId).SingleOrDefault();

            var zipCodeManager = new ManagerZipCodeRange()
            {
                UserRole = userRole,
                ZipCodeRange = zipCodeRange
            };
            _Context.ManagerZipCodeRanges.Add(zipCodeManager);
        }

        /// <summary>
        /// Removes zip code manager by internal Id
        /// </summary>
        /// <param name="id">Zip code manager id</param>
        public void DeleteZipCodeManager(int id)
        {
            var manager = _Context.ManagerZipCodeRanges.Where(m => m.ManagerZipCodeRangeId == id).SingleOrDefault();
            if (manager != null)
            {
                _Context.ManagerZipCodeRanges.Remove(manager);
            }
        }

        /// <summary>
        /// Gets the zip code range by its internal id
        /// </summary>
        /// <param name="id">ID of the zip code range</param>
        /// <returns></returns>
        public ZipCodeRange GetZipCodeRangeById(int id)
        {
            return _Context.ZipCodeRanges.Where(zcr => zcr.ZipCodeRangeId == id).FirstOrDefault();
        }

        /// <summary>
        /// Add zip code range
        /// </summary>
        /// <param name="obj">Zip code range to add</param>
        public void AddZipCodeRange(ZipCodeRange obj)
        {
            try
            {
                _Context.ZipCodeRanges.Add(obj);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add zip code range", ex);
            }
        }

        /// <summary>
        /// Delete zip code range by its internal id
        /// </summary>
        /// <param name="id">ID of the zip code range</param>
        public void DeleteZipCodeRangeById(int id)
        {
            try
            {
                ZipCodeRange range = this.GetZipCodeRangeById(id);
                _Context.ZipCodeRanges.Remove(range);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Zip Code Range with id {0}", id), ex);
            }
        }

        public bool ZipCodeRangeHasAnyDependencies(int id)
        {
            return _Context.ManagerZipCodeRanges.Any(mzcr => mzcr.ZipCodeRange.ZipCodeRangeId == id);
        }

        public bool HaseAnyDependenciesForUserRole(int userRoleId)
        {
            return _Context.PropertyAssignedManagers.Any(m => m.Role.RoleId == userRoleId);
        }
    }
}
