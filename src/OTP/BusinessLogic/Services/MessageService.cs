﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.ServiceContracts;
using DataAccess.Enums;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.Objects.Templates.Document;
using System.IO;
using BusinessLogic.Enums;
using System.Data.Common;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Functionalities of adding notifications to the database that should be then processed and sent to users by the Intranet Processor.
    /// </summary>
    public class MessageService : BaseTemplateService, IMessageService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageService"/> class.
        /// </summary>
        public MessageService(IStorageQueueService storageQueueService, IDocumentService docService, IContextService httpContextService, ISettingsService settingsService)
            : base(httpContextService, new Dictionary<Type, object>()
            {
                { typeof(IStorageQueueService), storageQueueService },
                { typeof(IDocumentService), docService },
                { typeof(ISettingsService), settingsService }
            })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageService"/> class.
        /// </summary>
        public MessageService(IStorageQueueService storageQueueService, OTPEntities ctx)
            : base(new Dictionary<Type, object>()
            {
                { typeof(IStorageQueueService),  storageQueueService }
            }, ctx)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageService"/> class.
        /// </summary>
        public MessageService(IStorageQueueService storageQueueService, IContextService ctx)
            : base(ctx, new Dictionary<Type, object>()
            {
                { typeof(IStorageQueueService),  storageQueueService }
            })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageService"/> class.
        /// </summary>
        public MessageService(IStorageQueueService storageQueueService, ISettingsService settingsService, OTPEntities ctx)
            : base(new Dictionary<Type, object>()
            {
                { typeof(IStorageQueueService),  storageQueueService },
                { typeof(ISettingsService),  settingsService }
            }, ctx)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageService"/> class.
        /// </summary>
        public MessageService(IContextService ctx, ISettingsService settingsService)
            : base(ctx, new Dictionary<Type, object>()
            {
                { typeof(ISettingsService),  settingsService }
            })
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public MessageService(OTPEntities ctx)
            : base(ctx)
        { }

        /// <summary>
        /// Gets message by id
        /// </summary>
        /// <param name="id">Message Id</param>
        /// <returns>Message</returns>
        public Message GetById(int id)
        {
            return
                _Context.AutoRetryQuery<Message>(() =>
                _Context.Messages.Where(m => m.MessageId == id).SingleOrDefault());
        }

        /// <summary>
        /// Returns messages which needs to be sent
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Message> GetMessagesToSend()
        {
            return _Context.Messages.Where(m => m.Status == (int)MessageStatus.New).ToList();
        }

        /// <summary>
        /// Saves the specified message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public Message Add(Message message)
        {
            _Context.Messages.Add(message);
            this._Context.SaveChanges();
            return message;
        }

        /// <summary>
        /// Updates the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Update(Message message)
        {
            _Context.SaveChanges();
        }

        /// <summary>
        /// Gets all attachments for message
        /// </summary>
        /// <param name="p">Message Id</param>
        /// <returns>List of attachments</returns>
        public IEnumerable<Attachment> GetAttachmentsByMessageId(int p)
        {
            return _Context.Attachments.Where(a => a.Message.MessageId == p).AsEnumerable();
        }

        /// <summary>
        /// Gets all messages
        /// </summary>
        /// <returns>List of messages</returns>
        public IEnumerable<Message> GetMessages()
        {
            return _Context.Messages.AsEnumerable();
        }

        /// <summary>
        /// Gets message by id
        /// </summary>
        /// <param name="messageId">Internal message id</param>
        /// <returns>Message object; Null if not exists</returns>
        public Message GetMessageById(int messageId)
        {
            return _Context.Messages.Where(m => m.MessageId == messageId).SingleOrDefault();
        }

        public void AddEmail(BaseEmail email, string language = null, List<BaseDocument> attachments = null, Attachment invoiceData = null)
        {
            StringBuilder recipients = new StringBuilder();
            StringBuilder CCList = new StringBuilder();
            StringBuilder ReplyToList = new StringBuilder();

            language = CultureCode.en_US;

            //default culture code
            //if (language == null)
            //{
            //    language = CultureCode.en_US;
            //}

            //get template
            Template emailTemplate = base.GetTemplate(email, language);
            StringBuilder sb = new StringBuilder();
            sb.Append(emailTemplate.Content);

            //set parameters
            string body = base.CompleteTemplate(sb.ToString(), email.Parameters);
            string subject = base.CompleteTemplate(String.IsNullOrEmpty(email.Subject) ? emailTemplate.Title : email.Subject, email.Parameters);

            //add recepient address
            foreach (string addr in email.RecipientAddresses)
            {
                recipients.Append(addr.Trim());
                recipients.Append(",");
            }
            recipients.Remove(recipients.Length - 1, 1);

            //add cc address
            if (email.CcAddresses != null)
            {
                foreach (string addr in email.CcAddresses)
                {
                    CCList.Append(addr);
                    CCList.Append(",");
                }
                if (CCList.Length > 0)
                {
                    CCList.Remove(CCList.Length - 1, 1);
                }
            }

            //add replyTo addresses
            if (email.ReplyToAddresses != null && email.ReplyToAddresses.Count > 0)
            {
                foreach (string addr in email.ReplyToAddresses)
                {
                    ReplyToList.Append(addr);
                    ReplyToList.Append(",");
                }
            }

            if (ReplyToList.Length > 0)
            {
                ReplyToList.Remove(ReplyToList.Length - 1, 1);
            }

            var sender = GetSenderSettings(email);
            Message mail = new Message
            {
                Recipients = recipients.ToString(),
                Created = DateTime.Now,
                Subject = subject,
                Body = body,
                Status = (int)MessageStatus.New,
                ReplyTo = ReplyToList.Length > 0 ? ReplyToList.ToString() : null,
                CC = CCList.Length > 0 ? CCList.ToString() : null,
                Type = emailTemplate.Type,
                DisplayFrom = sender.Value,
                AddressFrom = sender.Key
            };
            if (attachments != null)
            {
                foreach (BaseDocument att in attachments)
                {
                    DataAccess.Attachment attachment = new DataAccess.Attachment();
                    attachment.Content = GetFileContent(att, language);
                    attachment.Message = mail;
                    attachment.Type = (int)att.DocumentType;

                    string title = CompleteTemplate(base.GetTemplate(att, language).Title, att.Parameters);
                    attachment.Title = title;
                    _Context.Attachments.Add(attachment);
                }
            }

            if (invoiceData != null)
            {
                invoiceData.Message = mail;
                _Context.Attachments.Add(invoiceData);
            }
            _Context.Messages.Add(mail);
            _Context.SaveChanges();
        }

        public Template GetTemplateByTemplateType(TemplateType notificationType, string language)
        {
            return base.GetTemplateByType(notificationType, language);
        }

        private byte[] GetFileContent(BaseDocument document, string language)
        {
            Stream stream = this._documentService.CreateDocument(document, language);
            if (stream == null)
                return null;
            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            return ms.ToArray();
        }

        private KeyValuePair<string, string> GetSenderSettings(BaseEmail baseEmail)
        {
            var defaultSenderSetting = this._settingsService.GetSettingValue(
                string.Format("{0}.{1}", SettingKeyName.EmailSender, MessageSender.Default.ToString()));

            var accountConnectionString = this._settingsService.GetSettingValue(
                string.Format("{0}.{1}", SettingKeyName.EmailSender, baseEmail.Sender.ToString()));

            var defaultSetting = new DbConnectionStringBuilder();
            defaultSetting.ConnectionString = defaultSenderSetting;

            var specificSetting = new DbConnectionStringBuilder();
            specificSetting.ConnectionString = accountConnectionString;

            string key = specificSetting.ContainsKey("AddressFrom") ? (string)specificSetting["AddressFrom"] : (string)defaultSetting["AddressFrom"];
            string value = specificSetting.ContainsKey("DisplayFrom") ? (string)specificSetting["DisplayFrom"] : (string)defaultSetting["DisplayFrom"];

            if (!string.IsNullOrWhiteSpace(baseEmail.AddressFrom))
            {
                key = baseEmail.AddressFrom;
            }

            if (!string.IsNullOrWhiteSpace(baseEmail.DisplayFrom))
            {
                value = baseEmail.DisplayFrom;
            }

            return new KeyValuePair<string, string>(key, value);
        }
    }
}
