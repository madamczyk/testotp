﻿using BusinessLogic.Enums;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class StorageQueueService : BaseService, IStorageQueueService
    {
        private readonly string _connectionString;
        private readonly string _queueName;

        private static CloudStorageAccount _account;
        private static CloudQueueClient _queueClient;
        private static CloudQueue _queue;

        public StorageQueueService(IContextService httpContextService)
            : base(httpContextService)
        {
            _connectionString = _configurationService.GetKeyValue(CloudConfigurationKeys.StorageConnectionString);
            _queueName = _configurationService.GetKeyValue(CloudConfigurationKeys.MessageQueueName);

            InitializeQueue();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageQueueService"/> class.
        /// </summary>
        public StorageQueueService(IConfigurationService configurationService, IContextService httpContextService)
            : base(httpContextService, new Dictionary<Type, object>() {{ 
                typeof(IConfigurationService), configurationService }})
        {
            _connectionString = _configurationService.GetKeyValue(CloudConfigurationKeys.StorageConnectionString);
            _queueName = _configurationService.GetKeyValue(CloudConfigurationKeys.MessageQueueName);

            InitializeQueue();
        }

        /// <summary>
        /// Initializes the queue.
        /// </summary>
        private void InitializeQueue()
        {            
            // Turning Nagle Algorithm off improves queue write performance if message size is less
            // than approximately 1000 bytes.
            ServicePointManager.UseNagleAlgorithm = false;
            if (CloudStorageAccount.TryParse(_connectionString, out _account))
            {
                _queueClient = _account.CreateCloudQueueClient();
                _queueClient.RetryPolicy = RetryPolicies.Retry(3, TimeSpan.FromSeconds(2));

                _queue = _queueClient.GetQueueReference(_queueName);
                _queue.CreateIfNotExist();
            }            
        }

        /// <summary>
        /// Adds the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void AddMessage(string message)
        {
            _queue.AddMessage(new CloudQueueMessage(message));
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <param name="visibilityTimeout">The visibility timeout.</param>
        /// <returns></returns>
        public CloudQueueMessage GetMessage(TimeSpan visibilityTimeout)
        {
            return _queue.GetMessage(visibilityTimeout);
        }

        /// <summary>
        /// Update the message.
        /// </summary>
        /// <param name="visibilityTimeout">The visibility timeout.</param>
        /// <returns></returns>
        public void UpdateVisibility(CloudQueueMessage cloudQueueMessage, TimeSpan visibilityTimeout)
        {
            _queue.UpdateMessage(cloudQueueMessage, visibilityTimeout, MessageUpdateFields.Visibility);
        }

        /// <summary>
        /// Deletes the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void DeleteMessage(CloudQueueMessage message)
        {
            _queue.DeleteMessage(message);
        }

        /// <summary>
        /// Gets the size of the queue.
        /// </summary>
        /// <returns></returns>
        public int GetQueueSize()
        {
            return _queue.RetrieveApproximateMessageCount();
        }
    }
}
