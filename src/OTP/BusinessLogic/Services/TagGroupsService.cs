﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    public class TagGroupsService : BaseService, ITagGroupsService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TagGroupsService"/> class.
        /// </summary>
        /// <param name="services">The services.</param>
        public TagGroupsService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="TagGroupsService"/> class.
        /// </summary>
        /// <param name="ctx">EF Context</param>
        public TagGroupsService(OTPEntities ctx)
            : base(ctx)
        { }

        /// <summary>
        /// Get Tag Group by its internal Id
        /// </summary>
        /// <param name="id">internal id of the Tag Group</param>
        /// <returns>Tag retrieved</returns>
        public TagGroup GetTagGroupById(int id)
        {
            try
            {
                return this._Context.TagGroups.Where(p => p.TagGroupId == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get Tag Group with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets all tag groups
        /// </summary>
        /// <returns>List of all tag groups</returns>
        public IEnumerable<TagGroup> GetTagGroups()
        {
            var list =
                _Context.AutoRetryQuery<IEnumerable<TagGroup>>(() =>
                _Context.TagGroups.Include("Tags").AsEnumerable().OrderBy(t => t.NameCurrentLanguage));
            return list;
        }

        /// <summary>
        /// Delete Tag Group
        /// </summary>
        /// <param name="id">internal id of the Tag Group</param>
        public void DeleteTagGroup(int id)
        {
            try
            {
                TagGroup tagGroup = this.GetTagGroupById(id);
                _Context.TagGroups.Remove(tagGroup);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Tag Group with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Add Tag Group
        /// </summary>
        /// <param name="p">Tag Group to add</param>
        public void AddTagGroup(TagGroup t)
        {
            try
            {
                _Context.TagGroups.Add(t);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Tag Group", ex);
            }
        }

        /// <summary>
        /// Find the last position in the list of tag groups, increment it and return. If no tag groups present, return 1.
        /// </summary>
        /// <returns>position for new tag group</returns>
        public int GetPositionForNewTagGroup()
        {
            try
            {
                var list = _Context.TagGroups.ToList();

                if (list.Count == 0)
                    return 1;
                else
                    return list.Max(ag => ag.Position) + 1;
            }
            catch (Exception ex)
            {
                throw new Exception("Can't get new position for tag group", ex);
            }
        }

        /// <summary>
        /// Change the position of tag group with chosen id by given number of positions
        /// </summary>
        /// <param name="tagGroupId">internal Id of tag group</param>
        /// <param name="difference">difference between current and new position. This value is automatically changed to fit the range of (1, number of groups + 1)</param>
        public void ChangeTagGroupPosition(int tagGroupId, int difference)
        {
            IEnumerable<TagGroup> list;
            TagGroup tagGroup;

            list = GetTagGroups();

            if (list.Count() <= 1) // return, if no items in the list or just one item
                return;

            try
            {
                tagGroup = list.Where(ag => ag.TagGroupId == tagGroupId).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't find Tag Group with id {0}", tagGroupId), ex);
            }

            TagGroup modifiedTagGroup;
            int oldPosition, newPosition;

            oldPosition = tagGroup.Position;
            newPosition = Math.Max(1, Math.Min(list.Count(), oldPosition + difference));

            if (oldPosition == newPosition) // return, if new position is same as the old one
                return;

            try
            {
                modifiedTagGroup = list.Where(ag => ag.Position == newPosition).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't find Tag Group with position {0}", tagGroupId), ex);
            }

            tagGroup.Position = newPosition;
            modifiedTagGroup.Position = oldPosition;

            try
            {
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't save changes after position change", tagGroupId), ex);
            }
        }
    }
}
