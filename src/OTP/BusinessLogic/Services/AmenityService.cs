﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using BusinessLogic.ServiceContracts;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Services class for managing amenities. Contains basic CRUD operations.
    /// </summary>
    public class AmenityService : BaseService, IAmenityService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AmenityService"/> class.
        /// </summary>
        public AmenityService(IContextService httpContextService)
            : base(httpContextService)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AmenityService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
        public AmenityService(OTPEntities ctx)
            : base(ctx)
		{ }

        /// <summary>
        /// Get Amenity by its internal Id
        /// </summary>
        /// <param name="id">internal id of the Amenity</param>
        /// <returns>Amenity retrieved</returns>
        public Amenity GetAmenityById(int id)
        {
            try
            {
                return this._Context.Amenities.Where(p => p.AmenityID == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get Amenity with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Add Amenity
        /// </summary>
        /// <param name="p">Amenity to add</param>
        public void AddAmenity(Amenity p)
        {
            try
            {
                _Context.Amenities.Add(p);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't add Amenity", ex);
            }
        }

        /// <summary>
        /// Delete Amenity
        /// </summary>
        /// <param name="id">internal id of the Amenity</param>
        public void DeleteAmenity(int id)
        {
            try
            {
                Amenity amenity = this.GetAmenityById(id);
                _Context.Amenities.Remove(amenity);

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't delete Amenity with id {0}", id), ex);
            }
        }

        /// <summary>
        /// Gets all amenities
        /// </summary>
        /// <returns>List of all amenities</returns>
        public IEnumerable<Amenity> GetAmenities()
        {
            var list = _Context.Amenities.AsEnumerable().OrderBy(p => p.TitleCurrentLanguage);
            return list;
        }
    }
}
