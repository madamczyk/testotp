﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using DataAccess;
using System.Configuration;
using BusinessLogic.Managers;
using System;
using DataAccess.Enums;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Enums;
using System.Web.Security;

namespace BusinessLogic.Services
{
    /// <summary>
    /// Services class for managing user authentication. Contains basic operations for validating the user and generating a temporary password.
    /// </summary>
	public class AuthorizationService : BaseService, IAuthorizationService
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationsService"/> class.
        /// </summary>
        public AuthorizationService(IUserService userService, IContextService httpContextService)
            : base(httpContextService, new Dictionary<Type, object>() {{ 
                typeof(IUserService), userService }})
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationsService"/> class.
        /// </summary>
        public AuthorizationService(IUserService userService, OTPEntities ctx)
            : base(new Dictionary<Type, object>() {{ 
                typeof(IUserService), userService }}, ctx)
        { }

        /// <summary>
		/// Initializes a new instance of the <see cref="AuthorizationsService"/> class.
		/// </summary>
        /// <param name="ctx">EF Context</param>
		public AuthorizationService(OTPEntities ctx)
            : base(ctx)
		{ }

		/// <summary>
		/// Validates the user login.
		/// </summary>
		/// <param name="login">The login.</param>
		/// <param name="password">The password.</param>
        /// <returns>True if the password is correct, false otherwise.</returns>
		public bool ValidateUserLogin(string email, string password)
		{
            User user = this._userService.GetUserByEmail(email);
            if (user == null || user.Password.ToUpper() != Common.MD5Helper.Encode(password))
                return false;
            else
                return true;
		}

        /// <summary>
        /// Gets user by id number.
        /// </summary>
        /// <param name="userID">User id</param>
        /// <returns>User with specified id</returns>
        public User GetUserById(int userID)
        {
            return _Context.Users.Where(u => u.UserID == userID).FirstOrDefault();
        }        
        
        /// <summary>
        /// Generates temporary password in format [a-Z]{4}[0-9]{4}
        /// </summary>
        /// <returns> Generated string </returns>
        public string GeneratePassword()
        {
            //this code should be same in BO and FO
            String password = String.Empty;
            Random random = new Random();

            for (int i = 0; i < 4; i++)
            {
                password += ((char)((short)'a' + random.Next(26)));
            }
            for (int i = 0; i < 4; i++)
            {
                password += random.Next(0, 9);
            }
            return password;
        }

        public void SingOutUser()
        {
            FormsAuthentication.SignOut();
        }
	}
}
