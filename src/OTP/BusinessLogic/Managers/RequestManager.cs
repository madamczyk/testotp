﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Services;
using System.Web;
using DataAccess;
using System.Data.Objects.DataClasses;
using System.Data.Entity.Validation;

namespace BusinessLogic.Managers
{
    public class RequestManager
    {
        /// <summary>
        /// Provides access to BL services object.
        /// New object is created for every server request.
        /// </summary>
        /// <value>The services.</value>
        public static BusinessLogic.Services.Services Services
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Items["Services"] as BusinessLogic.Services.Services == null)
                        HttpContext.Current.Items["Services"] = new BusinessLogic.Services.Services(null);

                    return HttpContext.Current.Items["Services"] as BusinessLogic.Services.Services;
                }
                return new Services.Services(null);
            }
            set
            {
                HttpContext.Current.Items["Services"] = null;
            }

        }

        public static void SaveChanges()
        {
            OTPEntities ctx = HttpContext.Current.Items["EFContext"] as OTPEntities;
            if (ctx != null)
                ctx.SaveChanges();

        }

        public static void DisposeContext()
        {
            OTPEntities ctx = HttpContext.Current.Items["EFContext"] as OTPEntities;
            if (ctx != null)
            {
                ctx.Dispose();
                HttpContext.Current.Items["EFContext"] = null;
                HttpContext.Current.Items["Services"] = new BusinessLogic.Services.Services(null);
            }
        }
    }
}
