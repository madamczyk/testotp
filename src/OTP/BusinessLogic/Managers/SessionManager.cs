﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using DataAccess;
using System.Web.Security;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Objects;
using BusinessLogic.Objects.Session;

namespace BusinessLogic.Managers
{
    public static class SessionManager
    {
        public static string SessionToken
        {
            get
            {
                return (string)System.Web.HttpContext.Current.Session["SessionToken"];
            }
            set
            {
                HttpContext.Current.Session["SessionToken"] = value;
            }
        }

        public static bool IsAuthenticated()
        {
            if(HttpContext.Current.Request.IsAuthenticated)
            {
                if (CurrentUser == null)
                {
                    //CurrentUser = userService.GetUserByEmail(HttpContext.Current.User.Identity.Name);
                }
                if (CurrentUser == null || CurrentUser.LoggedInRole == null)
                {
                    FormsAuthentication.SignOut();
                    return false;
                }
                return true;
            }
            return false;
        }

        public static SessionUser CurrentUser
        {
            get
            {
                return System.Web.HttpContext.Current.Session["CurrentUser"] as SessionUser;
            }
            set
            {
                HttpContext.Current.Session["CurrentUser"] = value;                
            }
        }

        public static string ReturnUrl
        {
            get
            {
                return (string)HttpContext.Current.Session["SearchReturnUrl"];
            }
            set
            {
                HttpContext.Current.Session["SearchReturnUrl"] = string.Format("/#" + value);
            }
        }
    }
}
