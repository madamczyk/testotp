﻿using BusinessLogic.Objects.ZohoCrm;
using DataAccess;

namespace BusinessLogic.ServiceContracts
{
    public interface IZohoCrmService
    {

        /// <summary>
        /// Gets authentication token from ZOHO API
        /// </summary>
        /// <param name="zohoUsername">Zoho username</param>
        /// <param name="zohoPassword">Zoho password</param>
        /// <returns>Authentication token retrived from ZOHO API</returns>
        ZohoCrmAuthentication GetAuthenticationToken(string zohoUsername, string zohoPassword);

        /// <summary>
        /// Adds Lead to Zoho CRM
        /// </summary>
        /// <param name="lead">User to be added</param>
        /// <returns>Zoho response</returns>
        ZohoCrmResponse AddZohoCrmLead(User user);

        /// <summary>
        /// Converts lead into account and contant in Zoho CRM
        /// </summary>
        /// <param name="leadId">Lead Id to be converted into account</param>
        /// <param name="user">User on which conversion is performed</param>
        /// <returns>Zoho conversio response</returns>
        ZohoCrmConversionResponse ConvertZohoCrmLeadToAccount(string leadId, User user);

        /// <summary>
        /// Deletes Account by it's id
        /// </summary>
        /// <param name="recordId">Account Id</param>
        /// <returns>Zoho response</returns>
        ZohoCrmResponse DeleteZohoCrmAccountRecord(string recordId);

        /// <summary>
        /// Deletes Contact by it's id
        /// </summary>
        /// <param name="recordId">Contact Id</param>
        /// <returns>Zoho response</returns>
        ZohoCrmResponse DeleteZohoCrmContactRecord(string recordId);

        /// <summary>
        /// Deletes Lead by it's id
        /// </summary>
        /// <param name="recordId">Lead Id</param>
        /// <returns>Zoho response</returns>
        ZohoCrmResponse DeleteZohoCrmLeadRecord(string recordId);
    }
}
