﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IDictionaryCountryService
    {
        IEnumerable<DictionaryCountry> GetCountries();
        DictionaryCountry GetCountryById(int countryId);
        DictionaryCountry GetCountryByCountryCode(string countryCode);
    }
}
