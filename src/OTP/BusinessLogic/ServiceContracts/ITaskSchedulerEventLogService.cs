﻿using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface ITaskSchedulerEventLogService
    {
        int CreateTaskInstance(int scheduledTaskId);
        void UpdateTaskInstanceStatus(int taskInstanceId, TaskExecutionResult taskExecutionResult);
        void AddTaskInstance(TaskInstance taskInstance);
        void AddTaskInstanceDetail(TaskInstanceDetail taskInstanceDetail);
        void LogTaskInstanceError(int taskInstanceId, System.Exception exception);
        void LogTaskInstanceError(int taskInstanceId, string errorMessage);
        void LogTaskInstanceInfo(int taskInstanceId, string message);
        IEnumerable<TaskInstanceDetail> GetTaskInstanceDetails();
        IEnumerable<TaskInstanceDetail> GetTaskInstanceDetailsByTaskId(int taskId);
        TaskInstanceDetail GetTaskInstanceDetailById(int id);
        TaskInstance GetTaskInstanceById(int id);
        void DeleteTaskInstance(int id);
        void DeleteTaskInstanceDetail(int id);
    }
}
