﻿using BusinessLogic.Objects.Templates.Document;
using BusinessLogic.Objects.Templates.Email;
using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IMessageService
    {
        Message GetById(int id);
        IEnumerable<Message> GetMessagesToSend();
        Message Add(Message message);
        /// <summary>
        /// Add email to the database. Email will be processed and send by intranet processor
        /// </summary>
        /// <param name="email">Email object</param>
        /// <param name="cultureCode">Culture Code. If null culture will be set to 'en-US'</param>
        /// <param name="attachments">List of attachments</param>
        /// <param name="invoiceData">Invoice file</param>
        void AddEmail(BaseEmail email, string cultureCode = null, List<BaseDocument> attachments = null, Attachment invoiceData = null);
        void Update(Message message);
        IEnumerable<Attachment> GetAttachmentsByMessageId(int p);
        IEnumerable<Message> GetMessages();
        Message GetMessageById(int messageId);
        Template GetTemplateByTemplateType(TemplateType notificationType, string cultureCode);
    }
}
