﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface ITaxesService
    {
        Tax GetTaxById(int id);
        void AddTax(Tax p);
        IEnumerable<Tax> GetTaxes();
        IEnumerable<Tax> GetTaxesForDestination(int destinationId);
        void DeleteTax(int id);
    }
}
