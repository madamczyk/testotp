﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace BusinessLogic.ServiceContracts
{
    public interface IScheduledTasksService
    {
        void UpdateScheduledTask(ScheduledTask scheduledTask);
        void DeleteScheduledTask(int scheduledTaskId);
        void AddScheduledTask(ScheduledTask scheduledTask);
        IEnumerable<ScheduledTask> GetScheduledTasksToExecute();
        IEnumerable<ScheduledTask> GetScheduledTasks();
        ScheduledTask GetScheduledTaskById(int id);
    }
}
