﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IDictionaryCultureService
    {
        IEnumerable<DictionaryCulture> GetCultures();
        DictionaryCulture GetCultureById(int cultureId);
        DictionaryCulture GetCultureByCode(string code);
    }
}
