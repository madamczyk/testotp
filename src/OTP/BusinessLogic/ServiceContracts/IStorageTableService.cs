﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IStorageTableService
    {
        void AddSubscriber(string email);
    }
}
