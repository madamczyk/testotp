﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IDestinationsService
    {
        IEnumerable<Destination> GetDestinations();
        IEnumerable<Destination> GetTopXDestinationsByPropertiesCount(int numberOfDestinationsToTake);
        IEnumerable<Destination> GetMatchesDestinations(string destinationPartName);
        Destination GetDestinationById(int id);
        void AddDestination(Destination d);
        void DeleteDestination(int id);
        bool HasAnyDependencies(int id);
    }
}
