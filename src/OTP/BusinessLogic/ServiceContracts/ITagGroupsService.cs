﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface ITagGroupsService
    {
        TagGroup GetTagGroupById(int id);
        IEnumerable<TagGroup> GetTagGroups();
        void DeleteTagGroup(int id);
        void AddTagGroup(TagGroup t);
        int GetPositionForNewTagGroup();
        void ChangeTagGroupPosition(int tagGroupId, int difference);
    }
}
