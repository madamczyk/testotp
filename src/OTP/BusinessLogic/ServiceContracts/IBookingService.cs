﻿using BusinessLogic.EVS;
using BusinessLogic.Objects;
using BusinessLogic.PaymentGateway;
using DataAccess;
using DataAccess.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IBookingService
    {
        /// <summary>
        /// Generates invoce data for booking process and information purposes
        /// </summary>
        /// <param name="propertyId">Id of the property</param>
        /// <param name="unitId">Id of the unit</param>
        /// <param name="dateFrom">Reservation start day</param>
        /// <param name="dateUntil">Reservation end day</param>
        /// <returns></returns>
        PropertyDetailsInvoice CalculateInvoice(int propertyId, int unitId, DateTime dateFrom, DateTime dateUntil);
        /// <summary>
        /// Performs Tentative booking on the currently selected unit for defined period of time
        /// </summary>
        /// <param name="bookingInfo">Booking trip base information</param>
        /// <param name="usr">User for which booking should be performed</param>
        /// <returns>Id of the created reservation</returns>
        int DoTemporaryReservartion(BookingInfoModel bookingInfo, User usr);
        /// <summary>
        /// Performs user roles blockage after wrong ID Verification process
        /// </summary>
        /// <param name="usr">Currently logged user</param>
        void BlockUserAfterWrongIDVerification(User usr);
        /// <summary>
        /// Sets User verification hisotry data
        /// </summary>
        /// <param name="usr">Logged user</param>
        /// <param name="IsVerified">Verification result</param>
        /// <param name="evsResponse">Response from EVS system</param>
        void SetVerificationInfo(User usr, bool IsVerified, IEVSResponse evsResponse);
        /// <summary>
        /// Retrieves all the supported credtit card types
        /// </summary>
        /// <returns>List of the credit card types</returns>
        List<DataAccess.CreditCardType> GetCreditCardTypes();
        /// <summary>
        /// Saves PaymentData to DB
        /// </summary>
        /// <param name="reservationId">Id of the reservation</param>
        /// <param name="userId">Id of the current logged user</param>
        /// <param name="address">Billing address of the reservation</param>        
        /// <param name="guestPaymentId">Id of the user credit card</param>
        /// <returns>Result of the payment opertaion</returns>
        PaymentResult PerformPayment(int reservationId, int userId, ReservationBillingAddress address, int guestPaymentId);
        /// <summary>
        /// Retrieves all user used credit cards data
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <returns>List of the saved user credit cards</returns>
        List<GuestPaymentMethod> GetUserCreditCardTypes(int userId);
        /// <summary>
        /// Checks whenever Unit is available for Booking
        /// </summary>
        /// <param name="bookModel">Trip details</param>
        /// <param name="reservationId">Reservation that should be excluded from check</param>
        /// <returns>True if available, false in other case</returns>
        bool IsUnitAvailable(BookingInfoModel bookModel, int? reservationId = null);
        /// <summary>
        /// Contacts EVS Service provider to get user verification questions
        /// </summary>
        /// <param name="userId">ID of the user to verify</param>
        /// <returns>Verification service answer</returns>
        IEVSResponse GetEVSQuestions(Int32 userId);
        /// <summary>
        /// Contacts EVS Service provider to get user verification data
        /// </summary>
        /// <param name="userId">ID of the user to verify</param>
        /// <returns>Verification service answer</returns>
        IEVSResponse GetEVSInternational(Int32 userId);
        /// <summary>
        /// Add new Guest Payment Method to DB
        /// </summary>
        /// <param name="payment">Guest payment data</param>
        /// <param name="userId">Id of the guest (current user)</param>
        /// <param name="creditHolderId">Id of the credit card type</param>
        /// <param name="reservationId">Id of the reservation</param>
        /// <param name="succeed">Info about succed in SpreadlyCore</param>
        /// <returns>Id of the new payment method</returns>
        int SetGuestPaymentMethod(GuestPaymentMethod payment, int userId, int creditHolderId, int reservationId, out bool succeed);
        /// <summary>
        /// Remove guest credit card from DB
        /// </summary>
        /// <param name="guestPaymentId">Id of the payment method</param>
        void RemoveGuestPaymentMethod(int guestPaymentId);
        /// <summary>
        /// Generates payment record for reservation
        /// </summary>
        /// <param name="resv">Reservation item</param>
        /// <param name="paymentAmount">Amount to pay</param>
        void AddPaymentTransaction(Reservation resv, decimal paymentAmount);
        /// <summary>
        /// Adds agreement file to the reservation
        /// </summary>
        /// <param name="reservationId">Id of the reservation</param>
        /// <param name="file">File content of agreement</param>
        void AddReservationAgreement(int reservationId, byte[] file);
        /// <summary>
        /// Adds invoice file to the reservation
        /// </summary>
        /// <param name="reservationId">Id of the reservation</param>
        /// <param name="file">File content of invoice</param>
        void AddReservationInvoice(int reservationId, byte[] file);
        /// <summary>
        /// Set number of guests in reservation
        /// </summary>
        /// <param name="numberOfGuests">Number of guests</param>
        void UpdateReservationGuestNumber(int numberOfGuests, int reservationId);
        /// <summary>
        /// Generates transaction records for reservations.
        /// </summary>
        /// <param name="resv">Reservation item.</param>
        /// <param name="invoiceDetails">Calculated invoice for reservation.</param>
        void AddReservationTransactions(Reservation resv, PropertyDetailsInvoice invoiceDetails);
        /// <summary>
        /// Calculates amount to be authorized from guest accound.
        /// </summary>
        /// <param name="fullAmount">Full amount to pay.</param>
        /// <param name="arrivalDate">Guests arrival date.</param>
        /// <returns>Calculated amount.</returns>
        decimal CalculateAmountToAuthorize(decimal fullAmount, DateTime arrivalDate);
        /// <summary>
        /// Checks if reservesion period has defined rates.
        /// </summary>
        /// <param name="unitId">Id of unit.</param>
        /// <param name="dateArrival">Arrival date.</param>
        /// <param name="dateDeparture">Departure date.</param>
        /// <returns></returns>
        bool CheckUnitRatesForPeriod(int unitId, DateTime dateArrival, DateTime dateDeparture);
        /// <summary>
        /// Calculates amount to be authorized for the SecurityDeposit.
        /// </summary>
        /// <param name="reservation">Reservation to calculate amount for.</param>
        void CalculateSecurityDeposit(Reservation reservation);
        /// <summary>
        /// Aquires reservation security deposit amount.
        /// </summary>
        /// <param name="reservation">Reservation on behalf which security deposit will be aquired.</param>
        /// <returns></returns>
        TransactionResponse AquireSecurityDeposit(Reservation reservation);
        /// <summary>
        /// Sends alert email if there is no funds to take security deposit.
        /// </summary>
        /// <param name="reservation">Reservation data.</param>
        void SendSecurityDepositAlertEmail(Reservation reservation);
    }
}
