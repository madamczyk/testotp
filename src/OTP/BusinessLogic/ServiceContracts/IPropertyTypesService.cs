﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IPropertyTypesService
    {
        PropertyType GetPropertyTypeById(int id);
        void AddPropertyType(PropertyType p);
        void DeletePropertyType(int id);
        IEnumerable<PropertyType> GetPropertyTypes();
    }
}
