﻿using BusinessLogic.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IConfigurationService
    {
        string GetKeyValue(CloudConfigurationKeys key);
        bool IsCloudEnvironmentAvailable();
        bool IsCloudEnvironmentEmulated();
    }
}
