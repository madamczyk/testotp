﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IScheduledVisitsService
    {
        IEnumerable<ScheduledVisit> GetVisits();
        IEnumerable<ScheduledVisit> GetVisitsByUserId(int id);
        ScheduledVisit GetVisitById(int id);
        void AddVisit(ScheduledVisit v);
        void DeleteVisit(int id);
    }
}
