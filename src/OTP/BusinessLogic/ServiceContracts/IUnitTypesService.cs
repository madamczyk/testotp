﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IUnitTypesService
    {
        IEnumerable<UnitType> GetUnitTypesByPropertyId(int id);
        UnitType GetUnitTypeById(int id);
        void AddUnitType(UnitType unitType);
        void DeleteUnitType(int id);
        IEnumerable<UnitType7dayDiscounts> Get7DayUnitTypeDiscounts(int unitTypeId);
        UnitType7dayDiscounts Get7DayUnitTypeDiscountById(int id);
        void AddUnitType7DayDiscount(UnitType7dayDiscounts discount);
        void DeleteUnitType7DaysDiscount(int discountId);
        UnitType7dayDiscounts GetUnitType7DaysDiscount(int discountId);
        UnitTypeMLO GetUnitTypeMlosById(int id);
        void DeleteUnitTypeMlos(int mlosID);
        void AddUnitTypeMlos(UnitTypeMLO mlos);
        IEnumerable<UnitTypeMLO> GetUnitTypeMlos(int unitTypeId);
        IEnumerable<UnitTypeCTA> GetUnitTypeCta(int p);
        void AddUnitTypeCta(UnitTypeCTA cta);
        UnitTypeCTA GetUnitTypeCtaById(int id);
        IEnumerable<UnitTypeCTA> GetUnitTypeCtaByDate(int unitId, DateTime dateArrival);
        void DeleteUnitTypeCta(int ctaId);
        /// <summary>
        /// Returns unit type by unit id
        /// </summary>
        /// <param name="unitId">The unit id</param>
        /// <returns></returns>
        UnitType GetUnitTypeMlosByUnitId(int unitId);
        UnitType GetUnitTypeForPropertyByCode(int propertyId, string code);
    }
}
