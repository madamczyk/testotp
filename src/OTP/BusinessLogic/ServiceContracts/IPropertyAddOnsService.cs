﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IPropertyAddOnsService
    {
        IEnumerable<PropertyAddOn> GetAddOnsByPropertyId(int pid);
        PropertyAddOn GetById(int id);
        void Add(PropertyAddOn addOn);
        void DeleteAddOn(int id);
    }
}
