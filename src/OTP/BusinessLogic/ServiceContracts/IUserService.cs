﻿using DataAccess;
using DataAccess.Enums;
using BusinessLogic.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.ServiceContracts
{
    public interface IUserService
    {
        IQueryable<User> GetUsers();
        User GetUserByEmail(string email);
        void AddUser(User u);
        void AddHomeowner(User u);
        IEnumerable<User> GetUsersByRole(RoleLevel role);
        void ChangePassword(int userId, string oldPassword, string newPassword);
        bool IsPasswordValid(string password);
        User GetUserByUserId(int p);
        bool HasAnyDependencies(int userId);
        void DeleteUserByRoleId(int id);
        void DeleteUserById(int id);
        IEnumerable<RoleLevel> GetRoleLevelsForUser(int userId, bool onlyActive = false);        
        IEnumerable<UserRole> GetUserRolesByRoleLevel(RoleLevel roleLevel);
        UserRole GetUserRoleByActivationToken(string activationToken);
        UserRole GetUserRoleByLevel(User u, RoleLevel rl);
        UserRole GetUserByUserRoleId(int id);
        IEnumerable<UserRole> GetUserRolesByUserId(int userId);
        User MergeUserData(User target, User source, bool overwrite = false);
        void RemoveGuestPaymentMethod(int id,int userId);
        IEnumerable<GuestPaymentMethod> GetInactiveGuestPaymentMethods(int days);
        UserRole GetManagerRoleForZipCode(string zipCode);
        IEnumerable<ManagerZipCodeRange> GetZipCodesRangesForUserRole(int userId);
        IEnumerable<ZipCodeRange> GetZipCodeRanges();
        void AddZipCodeRangeToManager(int zipCodeRangeId, int userRoleId);
        void DeleteZipCodeManager(int id);
        ZipCodeRange GetZipCodeRangeById(int id);
        void AddZipCodeRange(ZipCodeRange obj);
        void DeleteZipCodeRangeById(int id);
        bool ZipCodeRangeHasAnyDependencies(int id);
        /// <summary>
        /// Checks if user role is related to other objects in database: for instance Zip Code Manager
        /// </summary>
        /// <param name="userRoleId">User Role ID</param>
        /// <returns>True - user role has any dependencies; False - user object hasn't any dependencies</returns>
        bool HaseAnyDependenciesForUserRole(int userRoleId);
        void DeleteUserRoleByRoleId(int id);
    }
}
