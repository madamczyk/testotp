﻿using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IEventLogService
    {
        void LogError(string message, DateTime date, string category, EventLogSource eventSource);
        void AddEventLog(EventLog eventLog);
        EventLog GetLastError();
        IEnumerable<EventLog> GetEventLogs();
        EventLog GetEventLogById(int id);
        IEnumerable<EventLog> GetEventLogsForDay(DateTime day);
        void DeleteEventLogById(int id);
    }
}
