﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.CustomObjects;
using BusinessLogic.Objects.Templates.Document;
using DataAccess.Enums;
using System.Globalization;

namespace BusinessLogic.ServiceContracts
{
    public interface IReservationsService
    {

        void AddReservation(Reservation reservation);
        Reservation GetReservationById(int id);
        IEnumerable<ReservationForUser> GetReservationsByUserId(int userId);
        IEnumerable<ReservationForUser> GetCurrentReservationsByUserId(int p);
        /// <summary>
        /// Returns all reservations in the database
        /// </summary>
        /// <returns></returns>
        IEnumerable<Reservation> GetReservations();
        /// <summary>
        /// Returns reservation details data for property
        /// </summary>
        /// <param name="propertyId">Id of the property</param>
        /// <param name="excludedStatuses">List of statuses in which reservations will not be returned</param>
        /// <returns></returns>
        IEnumerable<ReservationForHomeOwner> GetReservationsForProperty(int propertyId, List<BookingStatus> excludedStatuses = null);
        /// <summary>
        /// Returns reservation details data for Homeowner
        /// </summary>
        /// <param name="userId">Id of the homeowner user</param>
        /// <param name="propertyId">Id of the property</param>
        /// <returns></returns>
        IEnumerable<ReservationForHomeOwner> GetReservationTransactionsForHomeOwner(int userId, int? propertyId);
        /// <summary>
        /// Returns reservation details data for property
        /// </summary>
        /// <param name="unitId">Id of the unit</param>
        /// <returns></returns>
        IEnumerable<ReservationForHomeOwner> GetReservationsForUnit(int unitId);
        /// <summary>
        /// Returns not finalized reservations(with status "Tenative Booking" and older than 15 minutes) and canceled reservations
        /// </summary>
        /// <returns></returns>
        IEnumerable<Reservation> GetNotFinalizedOrCanceledReservations();
        /// <summary>
        /// Returns finalized reservations which happend 24 hours ealier.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Reservation> GetBookedReservationsAfter24Hours();
        /// <summary>
        /// Returns reservations X days upfront reservations which upcoming (X days or less from arrival). For payment capture.
        /// Default X: 30 days
        /// </summary>
        /// <returns></returns>
        IEnumerable<Reservation> GetUpcomingXDaysUpfrontReservationsForPayment(int days = 30);
        /// <summary>
        /// Returns reservations X days upfront reservations which upcoming (X days or less from arrival). 
        /// Default X: 30 days
        /// </summary>
        /// <returns></returns>
        IEnumerable<Reservation> GetUpcomingXDaysUpfrontReservations(int days = 30);
        /// <summary>
        /// Deletes a tenative reservation with all related information 
        /// </summary>
        /// <param name="reservationId"></param>
        void DeleteTentativeReservation(int reservationId);
        /// <summary>
        /// Change reservation dates logic with: payment gateway handling, invoice recalculation, guest email notification
        /// </summary>
        /// <param name="reservationId">Id of the guest reservation</param>
        /// <param name="dateFrom">New arrival date of the vacation</param>
        /// <param name="dateUntil">New departure date of the vacation</param>
        void ChangeReservationDate(int reservationId, DateTime dateFrom, DateTime dateUntil);
        /// <summary>
        /// Generates agreement document for the reservation
        /// </summary>
        /// <param name="newReservation">Reservation for which to create agreement</param>
        /// <param name="invoiceDetails">Details of the invoice</param>
        /// <returns></returns>
        UserAgreement GenerateAgreementDocument(Reservation newReservation, PropertyDetailsInvoice invoiceDetails);
        /// <summary>
        /// Generates invoice document
        /// </summary>
        /// <param name="newReservation"></param>
        /// <param name="invoiceDetails"></param>
        /// <returns></returns>
        Invoice GenerateInvoiceDocument(Reservation newReservation, PropertyDetailsInvoice invoiceDetails);
        string CancellationPolicy(DateTime start, DateTime bookingDate, CultureInfo propertyCultureInfo);
        /// <summary>
        /// Generates Trip details document based on given data
        /// </summary>
        /// <param name="newReservation">Reservation data</param>
        /// <returns>instance of TripDetails class</returns>
        TripDetails GenerateTripDetailsDocument(Reservation newReservation, PropertyDetailsInvoice invoiceDetails);
        /// <summary>
        /// Sends Trip details confirmation emails to owner and guest with invoice, tripdetails and agreement documents
        /// </summary>
        /// <param name="emailSubject">Subject of the email</param>
        /// <param name="newReservation">Reservation object</param>
        /// <param name="newReservationInvoiceDetails">Invoice details</param>
        /// <param name="agreement">Agreement doc</param>
        /// <param name="invoice">Invoice doc</param>
        /// <param name="tripDetails">Trip details doc</param>
        void SendTripDetailEmails(string emailSubject, Reservation newReservation, PropertyDetailsInvoice newReservationInvoiceDetails, UserAgreement agreement, Invoice invoice, TripDetails tripDetails, UserAgreementDateChange agreementDateChangeDoc = null);
        /// <summary>
        /// Search for the most restrictive arrival date ( the closesst to today )
        /// </summary>
        /// <param name="currentReservation">Currently active reservation</param>
        /// <param name="newArrivalDate">New arrival date</param>
        /// <returns></returns>
        DateTime FindMostRestrictiveArrivalDateForComparision(Reservation currentReservation, DateTime newArrivalDate);
        /// <summary>
        /// Find most expensive reservation in reservation history
        /// </summary>
        /// <param name="currentReservation">Current reservation node ( for start )</param>
        /// <returns>Most Expensive reservation node</returns>
        Reservation FindMostExpensiveReservation(Reservation currentReservation);
        /// <summary>
        /// Find root reservation in reservation hierarchy
        /// </summary>
        /// <param name="currentReservation">Current reservation node ( for start )</param>
        /// <returns>Root reservation node</returns>
        Reservation FindOriginalReservation(Reservation currentReservation);
        /// <summary>
        /// Get Key Management data for Homeowner
        /// </summary>
        /// <param name="ownerId">Homeowner id</param>
        /// <returns></returns>
        List<KeyManagementData> GetKeyManagementReservationsByHomeowner(int ownerId);
        /// <summary>
        /// Get Key Management data for Property Manager
        /// </summary>
        /// <param name="ownerId">Property manager id</param>
        /// <param name="managerRole">Manager role type</param>
        /// <returns></returns>
        List<KeyManagementData> GetKeyManagementReservationsByManagerRole(int userId, RoleLevel managerRole);
        /// <summary>
        /// Returns finalized reservations (with status is "Checked in") which departure date will occured within X days in future.
        /// </summary>
        /// <param name="days">Number of days in future when reservation will be finished (status 'Checked Out').</param>
        /// <returns></returns>
        IEnumerable<Reservation> GetCompletedReservationsWithinXDays(int days);
        /// <summary>
        /// Save arrival time for reservation (based on enum value from ArrivalTimeRange)
        /// </summary>
        /// <param name="reservationId">Reservation ID</param>
        /// <param name="reservationToken">Link Authorization Token for reservation</param>
        /// <param name="arrivalTime">Arrival time - enum value</param>
        /// <returns></returns>
        bool? SetNewArrivalTimeForReservation(int reservationId, string reservationToken, int arrivalTime);
        /// <summary>
        /// Get reservation by Token (Guid format)
        /// </summary>
        /// <param name="reservationToken"></param>
        /// <returns></returns>
        Reservation GetReservationByLinkToken(string reservationToken);
        /// <summary>
        /// Set flag 'Guest confirm  returning keys' to 'true' for reservation
        /// </summary>
        /// <param name="reservationId">Reservation ID</param>
        void DropOffKeyByGuest(int reservationId);
        /// <summary>
        /// Retrieves reservations owned by concrete Homeowner
        /// </summary>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        IQueryable<Reservation> GetReservationsByHomeowner(int ownerId);
        /// <summary>
        /// Retrieves reservations owned by concrete Manager Role
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="managerRole"></param>
        /// <returns></returns>
        IQueryable<Reservation> GetReservationsByManagerRole(int userId, RoleLevel managerRole);
        /// <summary>
        /// Sends key lockbox email informations to guest
        /// </summary>
        /// <param name="reservation"></param>
        void SendCheckInLockboxEmail(Reservation reservation);
        /// <summary>
        /// Sends information about guest visit to key handler
        /// </summary>
        /// <param name="reservation"></param>
        void SendCheckInPickUpKeyHandlerEmail(Reservation reservation);
        /// <summary>
        /// Sets reservation status
        /// </summary>
        /// <param name="status">Booking status</param>
        /// <param name="reservationId">Reservation ID</param>
        void SetReservationStatus(BookingStatus status, int reservationId);
        /// <summary>
        /// Gets reservations about certain status
        /// </summary>
        /// <returns></returns>
        IEnumerable<Reservation> GetRervationsByBookingStatus(BookingStatus status);
        /// <summary>
        /// Retrieves reservations that need to be authorized for security deposit.
        /// </summary>
        /// <returns>List of reservations.</returns>
        IEnumerable<Reservation> GetSecurityDepositRetryReservations();
    }
}
