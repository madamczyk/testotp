﻿using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IPaymentGatewayOperationLogService
    {
        void AddOperationLog(PaymentGatewayOperationsLog operationLog);
        IEnumerable<PaymentGatewayOperationsLog> GetPaymentGatewayOperationsLogs();
        PaymentGatewayOperationsLog GetPaymentGatewayOperationsLogById(int id);
        void DeletePaymentGatewayOperationsLogById(int id);
    }
}
