﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IAmenityGroupsService : IService
    {
        AmenityGroup GetAmenityGroupById(int id);
        void AddAmenityGroup(AmenityGroup p);
        void DeleteAmenityGroup(int id);
        IEnumerable<AmenityGroup> GetAmenityGroups();
        AmenityGroup GetOtherAmenityGroupById();
        void ChangeAmenityGroupPosition(int amenityGroupId, int difference);
    }
}
