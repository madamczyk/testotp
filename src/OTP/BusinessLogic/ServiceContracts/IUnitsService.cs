﻿using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IUnitsService
    {
        Unit GetUnitById(int unitId);
        IEnumerable<UnitTypeMLO> GetUnitMlos(int unitId, DateTime? startDate, DateTime? dateEnd);
        IEnumerable<Unit> GetUnitsByPropertyId(int pid);
        void AddUnit(Unit unit);
        IEnumerable<UnitStaticContentResult> GetUnitStaticContentInfo(int unitId);
        IEnumerable<Appliance> GetUnitAppliances(int unitId);
        void DeleteUnit(int unitId);
        IEnumerable<UnitStaticContent> GetUnitStaticContentByType(int unitId, UnitStaticContentType propertyStaticContentType);
        void RemoveStaticContent(int itemId);
        UnitStaticContent GetUnitStaticContentById(int itemId);
        void AddUnitStaticContent(UnitStaticContent obj);
        IEnumerable<UnitRate> GetUnitRates(int unitID);
        decimal? GetUnitPrice(int unitId, DateTime? dateFrom, DateTime? dateTo);
        decimal? GetUnitPriceAccomodation(int unitId, DateTime? dateFrom, DateTime? dateTo);
        UnitRate GetUnitRateById(int unitRateId);
        void DeleteUnitRate(int unitRateId);
        void AddUnitRate(UnitRate rate);
        IEnumerable<UnitAppliancy> GetUnitApplianciesByUnitId(int unitId);
        void AddUnitAppliance(UnitAppliancy appliance);
        UnitAppliancy GetUnitApplianceById(int id);
        void DeleteUnitAppliance(int appId);
        IEnumerable<UnitInvBlocking> GetUnitBlockings(int unitId);
        void DeleteUnitBlockage(int unitBlId);
        UnitInvBlocking GetUnitBlockageById(int unitBlId);
        void AddUnitBlockade(UnitInvBlocking unitBlockade);
        IEnumerable<UnitAvailabilityResult> GetUnitAvailability(int unitId, DateTime dateFrom, DateTime dateUntil);
        void DeleteUnitBlockageByReservationId(int reservationId);
        /// <summary>
        /// Same as <see cref="DeleteUnitBlockageByReservationId"/> but immediately saves data do database
        /// </summary>
        /// <param name="reservationId">Id of the reservation</param>
        void PersistDeleteUnitBlockageByReservationId(int reservationId);
        /// <summary>
        /// Gets unit blockings within specific date range
        /// </summary>
        /// <param name="unitId">The unit id</param>
        /// <param name="dateFrom">The date from</param>
        /// <param name="dateUntil">The date until</param>
        /// <returns></returns>
        IEnumerable<UnitInvBlocking> GetUnitBlockings(int unitId, DateTime dateFrom, DateTime dateUntil);
        /// <summary>
        /// Gets unit blockings within specific date range
        /// Additonal option available which lets u determine wether exclude reservation unit blockades
        /// </summary>
        /// <param name="unitId">The unit id</param>
        /// <param name="dateFrom">The date from</param>
        /// <param name="dateUntil">The date until</param>
        /// <param name="excludeReservation">Flag which determinse wether exclude reservation unit blockades</param>
        /// <returns></returns>
        IEnumerable<UnitInvBlocking> GetUnitBlockings(int unitId, DateTime dateFrom, DateTime dateUntil, bool excludeReservation);
        /// <summary>
        /// Gets unit rates for a specific unit id within date range
        /// </summary>
        /// <param name="unitId">The id of the Unit</param>
        /// <param name="dateFrom">The date from</param>
        /// <param name="dateUntil">The date until</param>
        /// <returns></returns>
        IEnumerable<UnitRate> GetUnitRates(int unitId, DateTime dateFrom, DateTime dateTo);
        /// <summary>
        /// Gets cta's within specific date range
        /// </summary>
        /// <param name="unitId">The unit id</param>
        /// <param name="dateFrom">The date from</param>
        /// <param name="dateUntil">The date until</param>
        /// <returns></returns>
        IEnumerable<UnitTypeCTA> GetCTAs(int unitId, DateTime dateFrom, DateTime dateUntil);
        /// <summary>
        /// Adds a new unit blockade and adjust other unit blockades. 
        /// If adding blockade with "available" unit blockade type, it needs to adjust other blockades within unit blockade's date range
        /// </summary>
        /// <param name="newUnitBlockage">Blocking object</param>
        void AddAndAdjustUnitBlockade(UnitInvBlocking newUnitBlockage);
        /// <summary>
        /// Update unit rates. Validating if rate periods overlap each other.
        /// </summary>
        /// <param name="unitRatesToUpdate"></param>
        void UpdateUnitRates(int unitId, UnitRate unitRatesToUpdate, int userId);
        /// <summary>
        /// Delete unit rates by ids.
        /// </summary>
        /// <param name="unitRateIds">Collection of unit rate ids</param>
        void DeleteUnitRates(IEnumerable<int> unitRateIds);
        /// <summary>
        /// Update mloses. Validating if rate periods overlap each other.
        /// </summary>
        /// <param name="mlosesToUpdate"></param>
        void UpdateMloses(int unitId, IEnumerable<UnitTypeMLO> mlosesToUpdate);
        /// <summary>
        /// Delete unit rates by ids.
        /// </summary>
        /// <param name="mlosIds">Collection of mlos ids</param>
        void DeleteMloses(IEnumerable<int> mlosIds);
        /// <summary>
        /// Delete ctas by ids.
        /// </summary>
        /// <param name="unitRateIds">Collection of cta ids</param>
        void DeleteCtas(IEnumerable<int> ctasToIds);
        /// <summary>
        /// Update ctas. Validating if ctas periods overlap each other.
        /// </summary>
        /// <param name="ctasToUpdate"></param>
        void UpdateCtas(int unitId, IEnumerable<UnitTypeCTA> ctasToUpdate);
        /// <summary>
        /// Retrieves units by its owner id
        /// </summary>
        /// <param name="ownerId">Id of the Homeowner</param>
        /// <param name="onlyAvailable">Retrieve only acvailable units</param>
        /// <returns></returns>
        IEnumerable<Unit> GetUnitsByOwnerId(int ownerId, bool onlyAvailable = false);
        Unit GetUnitForPropertyByCode(int propertyId, string code);
        /// <summary>
        /// Retrieves units by its Property Manager id
        /// </summary>
        /// <param name="managerId">Id of the manager</param>
        /// <param name="onlyAvailable">Retrieve only acvailable units</param>
        /// <param name="roleLevel">Assigned manager role</param>
        /// <returns></returns>
        IEnumerable<Unit> GetUnitsByManagerId(int managerId, RoleLevel roleLevel, bool onlyAvailable = false);
        UnitInvBlocking GetUnitBlockageByReservationId(int reservationId);
    }
}
