﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IAppliancesService
    {
        IEnumerable<ApplianceGroup> GetApplianceGroups();
        Appliance GetApplianceById(int applianceId);
        IEnumerable<Appliance> GetApplianceByGroupId(int applianceGroupId);
        IEnumerable<Appliance> GetAppliances();
    }
}
