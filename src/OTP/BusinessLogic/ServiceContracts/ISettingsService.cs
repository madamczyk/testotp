﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface ISettingsService
    {
        IEnumerable<OTP_Settings> GetSettings();
        string GetSettingValue(string key);
        IEnumerable<OTP_Settings> GetAllSettingsByPrefix(string prefix);

    }
}
