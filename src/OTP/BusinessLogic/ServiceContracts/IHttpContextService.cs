﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IContextService
    {
        object Current { get; }
        object GetFromContext(string key);
        void AddToContext(string key, object value);
        void RemoveFromContext(string key);
        OTPEntities GetDBContext();
    }
}
