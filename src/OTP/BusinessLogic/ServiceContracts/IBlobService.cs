﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IBlobService
    {
        void InitContainer(string name);
        string AddFile(Stream s, int id, string fileName, string contentType = "");
        void DeleteFile(int id, string fileName);
        Stream DownloadFileByUrl(string fileRelativePath, string container);
        bool FileExists(int id, string fileName);
        List<string> GetFiles();
        bool DeleteFile(string fileName, bool removeContainerName = false);
    }
}
