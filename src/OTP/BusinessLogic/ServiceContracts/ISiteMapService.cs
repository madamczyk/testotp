﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface ISiteMapService
    {
        IntranetSiteMapAction GetRootAction();
    }
}
