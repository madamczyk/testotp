﻿using DataAccess;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IRolesService
    {
        IEnumerable<Role> GetRoles();
        Role GetRoleById(int roleId);
        Role GetRoleByRoleLevel(RoleLevel roleLevel);
        UserRole AddUserToRole(User user, RoleLevel roleLevel);
    }
}
