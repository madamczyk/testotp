﻿using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using BusinessLogic.Objects.Templates.Document;

namespace BusinessLogic.ServiceContracts
{
    public interface IDocumentService
    {
        Stream CreateDocument(BaseDocument document, string cultureCode);
        byte[] CreateDoc(BaseDocument document, string cultureCode);
    }
}
