﻿using BusinessLogic.Objects;
using BusinessLogic.Objects.Session;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IStateService
    {
        SessionUser CurrentUser { get; set; }

        void AddToSession(string key, object value);
        T GetFromSession<T>(string key);
        void RemoveFromSession(string key);
        bool IsNullInSession(string key);
        bool IsAuthenticated();
        void InitCache();
        void AddToCache(string key, object value, int secondsCount);
        bool IsNullInCache(string key);
        T GetFromCache<T>(string key);
        void RemoveFromCache(string key);

    }
}
