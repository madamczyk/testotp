﻿using BusinessLogic.Objects;
using BusinessLogic.Objects.StaticContentRepresentation;
using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace BusinessLogic.ServiceContracts
{
    public interface IPropertiesService
    {
        Property GetPropertyById(int id, bool onlyAvailable = false);
        /// <summary>
        /// Get Property by its internal Id
        /// </summary>
        /// <param name="id">internal id of the property</param>
        /// <param name="numberOfUnits">Output parameter - number of units connected to property</param>
        /// <param name="onlyAvailable">True if tak only active property</param>
        /// <returns>Retrieved property</returns>
        Property GetPropertyById(int id, out int numberOfUnits, bool onlyAvailable = false);
        IEnumerable<Property> GetPropertiesByOwnerId(int id, bool onlyAvailable = false);
        IEnumerable<String> GetPropertyImagesByPropertyId(int id, PropertyStaticContentType imageType);
        void AddProperty(Property p);
        IEnumerable<PropertiesSearchResult> SearchProperties(int? destinationId = null, DateTime? dateFrom = null, DateTime? dateTo = null, int? numberOfGuests = null, int? numberOfBathrooms = null, int? numberOfBedrooms = null, string propertyTypes = null, string tags = null, string amenities = null, string languageCode = null, decimal? beginLatitude = null, decimal? endLatitude = null, decimal? beginLongitude = null, decimal? endLongitude = null);
        bool HasAnyUseTag(int tagId);
        bool HasAnyUsePropertyType(int propertyTypeId);
        bool HasAnyUseAmenity(int amenityId);
        IEnumerable<Property> GetProperties();
        bool HasAnyReservation(int propertyID);
        int GetPropertyIdByUnitId(int unitID);
        int? GetPropertyReviewRate(int propertyId);
        string GetPropertySquareFootage(int propertyId);
        IEnumerable<PropertyStaticContentResult> GetPropertyStaticContentInfo(int propertyId);
        IEnumerable<GuestReview> GetPropertyGuestReviews(int propertyId);
        IEnumerable<PropertyStaticContent> GetPropertyStaticContentByType(int propertyId, PropertyStaticContentType propertyStaticContentType);
        void RemoveStaticContent(int itemId);
        PropertyStaticContent GetPropertyStaticContentById(int id);
        void AddPropertyStaticContent(PropertyStaticContent obj);
        IEnumerable<PropertyStaticContent> GetPropertyStaticContentByType(int pid, List<PropertyStaticContentType> cts);
        void AddPropertyAmenity(PropertyAmenity propAmenity);
        void AddPropertyTag(PropertyTag propTag);
        PropertyAmenity GetPropertyAmenityById(int id);
        PropertyTag GetPropertyTagById(int id);
        void RemovePropertyAmenity(int id);
        void RemovePropertyAmenitiesByPropertyId(int propertyId);
        void RemovePropertyTag(int id);
        PropertyAmenity GetPropertyAmenity(int pid, int amId);
        PropertyTag GetPropertyTag(int pid, int tagId);
        IEnumerable<PropertyAmenity> GetPropertyAmenitiesByPropertyId(int pid);
        IEnumerable<int> GetAmenityGroupsByPropertyId(int pid);
        void RemoveProperty(int id);
        IEnumerable<FloorPlan> GetPropertyFloorPlans(int propertyID);
        PropertyStaticContent AddPropertyFloorPlan(int propertyID, FloorPlan floorPlan);
        void EditPropertyFloorPlan(int floorPlanID, FloorPlan floorPlan);
        void AddPanoramaToPropertyFloorPlan(int floorPlanID, Panorama panorama);
        void EditPropertyFloorPlansPanorama(int floorPlanID, Panorama panorama);
        void DeletePropertyFloorPlansPanorama(int floorPlanID, int panoramaID);
        void DeletePropertyFloorPlan(int floorPlanID);
        IEnumerable<T> GetPropertyStaticContentByType<T>(int propertyId, PropertyStaticContentType propertyStaticContentType);
        void AddPropertyStaticContent<T>(object staticContentObject, PropertyStaticContentType type, int propertyId);
        decimal CalculatePropertyCommission(DateTime dateFrom, DateTime dateTo, decimal total, int propertyId, int lengthOfStay);
        IEnumerable<DirectionToAirport> GetPropertyDirectionsToAirport(int propertyID);
        SearchFinalScoreDetails GetSearchScoreDetails(int unitId, int? numberOfGuests = null, int? numberOfBathrooms = null, int? numberOfBedrooms = null, string propertyTypes = null, string tags = null, string amenities = null);
        PropertyRawData GetPropertyRawDataById(int id);
        IEnumerable<PropertyRawData> GetPropertyRawDatas(int propertyId);
        void AddPropertyRawData(PropertyRawData p);
        void DeletePropertyRawData(int id);
        IEnumerable<PropertyTag> GetPropertyTagsByPropertyId(int pid);
        void AddPropertyManager(int userId, int propertyId, RoleLevel managerRole);
        PropertyAssignedManager GetPropertyManagerByType(int propertyId, RoleLevel managerType);
        void DeleteManagerFromProperty(int propertyAssignedManagerId);
        ContentUpdateRequest GetContentUpdateRequestById(int id);
        ContentUpdateRequestDetail GetContentUpdateRequestDetailById(int id);
        IEnumerable<ContentUpdateRequest> GetContentUpdateRequests();
        IEnumerable<ContentUpdateRequestDetail> GetContentUpdateRequestDetailsByRequestId(int requestId);
        void AddContentUpdateRequest(ContentUpdateRequest p);
        void AddContentUpdateRequestDetail(ContentUpdateRequestDetail p);
        void DeleteContentUpdateRequest(int id);
        void DeleteContentUpdateRequestDetail(int id);
        void UploadFileToStorage(HttpRequestBase request, List<UploadFilesResult> statuses, int propertyId);
        int AddContentUpdateRequestDetail(ContentUpdateRequest newRequest, ContentUpdateRequestDetailContentType contentUpdateRequestDetailContentType, string newDetailText);
        Property GetPropertyByCode(string code);
        /// <summary>
        /// Get list of properties by assigned manager
        /// </summary>
        /// <param name="managerId">Manager id</param>
        /// <param name="roleLevel">Manager assigned role</param>
        /// <param name="onlyAvailable">True if only enabled properties</param>
        /// <returns></returns>
        IEnumerable<Property> GetPropertiesByManager(int managerId, RoleLevel roleLevel, bool onlyAvailable = false);
        DateTime GetPropertyLocalTime(DateTime utcDateTime, int propertyId);
        IEnumerable<PropertyRawData> GetPropertyRawDatas();
        IEnumerable<ContentUpdateRequestDetail> GetContentUpdateRequestDetails();
        IEnumerable<PropertyStaticContent> GetPropertyStaticContents(List<PropertyStaticContentType> includeTypes = null);
        /// <summary>
        /// Gets property by it's sign off token - for homeowner
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Property GetPropertyBySingOffToken(string id);
    }
}
