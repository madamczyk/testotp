﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IAmenityService
    {
        Amenity GetAmenityById(int id);
        void AddAmenity(Amenity p);
        void DeleteAmenity(int id);
        IEnumerable<Amenity> GetAmenities();
    }
}
