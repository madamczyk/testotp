﻿using Microsoft.WindowsAzure.StorageClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IStorageQueueService
    {
        void AddMessage(string message);
        CloudQueueMessage GetMessage(TimeSpan visibilityTimeout);
        void UpdateVisibility(CloudQueueMessage cloudQueueMessage, TimeSpan visibilityTimeout);
        void DeleteMessage(CloudQueueMessage message);
        int GetQueueSize();
    }
}
