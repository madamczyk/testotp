﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface IAuthorizationService
    {
        bool ValidateUserLogin(string email, string password);
        User GetUserById(int userID);
        string GeneratePassword();
        void SingOutUser();
    }
}
