﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface ISalesPersonsService
    {
        SalesPerson GetSalesPersonById(int id);
        void AddSalesPerson(SalesPerson salesPerson);
        void DeleteSalesPerson(int id);
        IEnumerable<SalesPerson> GetSalesPersons();
    }
}
