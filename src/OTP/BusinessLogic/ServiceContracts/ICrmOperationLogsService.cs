﻿using DataAccess;
using System.Collections.Generic;

namespace BusinessLogic.ServiceContracts
{
    public interface ICrmOperationLogsService
    {
        /// <summary>
        /// Adds Crm operation log
        /// </summary>
        /// <param name="operationLog">Log object</param>
        void AddOperationLog(CrmOperationLog operationLog);

        /// <summary>
        /// Gets list of Crm operations logs
        /// </summary>
        /// <returns>List with Crm logs</returns>
        IEnumerable<CrmOperationLog> GetCrmOperationsLog();

        /// <summary>
        /// Gets operation log with specified Id
        /// </summary>
        /// <param name="id">Log's id</param>
        /// <returns>Log record with specified Id</returns>
        CrmOperationLog GetCrmOperationLogsById(int id);

        /// <summary>
        /// Deletes log with specified Id
        /// </summary>
        /// <param name="id">Log's id</param>
        void DeleteCrmOperationsLogById(int id);
    }
}
