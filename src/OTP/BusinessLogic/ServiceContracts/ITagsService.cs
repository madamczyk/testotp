﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.ServiceContracts
{
    public interface ITagsService
    {
        Tag GetTagById(int id);
        void AddTag(Tag p);
        void DeleteTag(int id);
        IEnumerable<Tag> GetTags();
    }
}
