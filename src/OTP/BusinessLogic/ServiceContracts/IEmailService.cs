﻿using System.Collections.Generic;

namespace BusinessLogic.ServiceContracts
{
    public interface IEmailService
    {
        void SendEmail(DataAccess.Message dbMessage, BusinessLogic.Objects.EmailAccount account, IEnumerable<DataAccess.Attachment> attachments = null);
    }
}
