﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Enums
{
    public static class SettingKeyName
    {
        public const string ExtranetURL = "ExtranetHost";

        #region Property Settings

        public const string PropertyStandardCommision = "Properties.StandardCommission";
        public const string MaxFileSize = "Properties.MaxFileSizeInBytes";

        #endregion

        #region Mail Settings

        public const string EmailServerAccount = "Email.ServerAccount";
        public const string EmailSender = "Email.Sender";

        #endregion

        #region Fuzzy Search Settings

        public const string SearchDimensionWeightGuests = "FuzzySearch.DimensionWeight.Guests";
        public const string SearchDimensionWeightBathrooms = "FuzzySearch.DimensionWeight.Bathrooms";
        public const string SearchDimensionWeightBedrooms = "FuzzySearch.DimensionWeight.Bedrooms";
        public const string SearchDimensionWeightAmenities = "FuzzySearch.DimensionWeight.Amenities";
        public const string SearchDimensionWeightPropertyTypes = "FuzzySearch.DimensionWeight.PropertyTypes";
        public const string SearchDimensionWeightTags = "FuzzySearch.DimensionWeight.Tags";
        public const string SearchDimensionWeightThreshold = "FuzzySearch.Threshold";

        #region Tolerance parameters of DS functions for all of filters

        //guests
        public const string SearchParamGuestsMax = "FuzzySearch.Parameters.GuestsMax";
        public const string SearchParamGuestsMin = "FuzzySearch.Parameters.GuestsMin";

        //bathrooms                                   
        public const string SearchParamBathroomsMax = "FuzzySearch.Parameters.BathroomsMax";
        public const string SearchParamBathroomsMin = "FuzzySearch.Parameters.BathroomsMin";

        //bedrooms                                    
        public const string SearchParamBedroomsMax = "FuzzySearch.Parameters.BedroomsMax";
        public const string SearchParamBedroomsMin = "FuzzySearch.Parameters.BedroomsMin";

        //amenities
        public const string SearchParamAmenitiesMax = "FuzzySearch.Parameters.AmenitiesMax";
        public const string SearchParamAmenitiesMin = "FuzzySearch.Parameters.AmenitiesMin";

        //tags
        public const string SearchParamTagsMax = "FuzzySearch.Parameters.TagsMax";
        public const string SearchParamTagsMin = "FuzzySearch.Parameters.TagsMin";

        //property types
        public const string SearchParamPropertyTypesMax = "FuzzySearch.Parameters.PropertyTypesMax";
        public const string SearchParamPropertyTypesMin = "FuzzySearch.Parameters.PropertyTypesMin";

        #endregion
        #endregion

        #region EVS Settings
        public const string EVSUser = "EVS.User";
        public const string EVSPassword = "EVS.Password";
        public const string EVSFailNotifyEmail = "EVS.FailNotifyEmail";
        #endregion

        #region PaymentGateWay
        public const string PaymentAuthToken = "Payment.ApiAuthToken";
        public const string PaymentRedirectURL = "Payment.RedirectURL";
        public const string PaymentApiLogin = "Payment.ApiLogin";
        public const string PaymentApiSecret = "Payment.ApiSecret";
        public const string PaymentGatewayToken = "Payment.GatewayToken";
        #endregion

        #region PaymentPolicy
        public const string PaymentPolicyFirstStepDays = "Payment.Policy.FirstStepDays"; // example 30 days
        public const string PaymentPolicyFirstStepDaysGreaterThanPercentToAuthorize = "Payment.Policy.FirstStepDays.GreaterThan.PercentToAuthorize"; // if  arrivalDate > 30 days from now take this
        public const string PaymentPolicySecondStepDays = "Payment.Policy.SecondStepDays"; // example 15 days
        public const string PaymentPolicySecondStepDaysGreaterThanPercentToAuthorize = "Payment.Policy.SecondStepDays.GreaterThan.PercentToAuthorize"; // if  arrivalDate > 15 days from now && arrival date < 30 take this
        #endregion

        #region OTPEmailAccounts
        public const string EmailAccountHomeownersTicketSystem = "EmailAccount.HomeownersTicketSystem";
        public const string EmailAccountOTPSupport = "EmailAccount.OTPSupport";
        #endregion

        #region TicketSystem
        public const string TicketSystemJoinEmailAddress = "TicketSystem.JoinEmailAddress";
        #endregion
        #region Example Property
        public const string ExamplePropertyId = "ExampleProperty.Id";
        #endregion

        #region Event Log Summary Settings
        public const string EventLogSummaryRecipients = "ScheduledTasks.EventLogSummary.Recipients";
        public const string EventLogSummaryDeploymentType = "DeploymentType";
        #endregion

        #region Containers
        public const string ContainersProperties = "Containers.Properties";
        public const string ContainersContentUpdateRequests = "Containers.ContentUpdateRequests";
        public const string ContainersPropertyDawData = "Containers.PropertyDawData";
        #endregion

        #region Reservations        
        public const string ReservationCompleteAfterDaysOfDeparture = "Reservation.CompletedDaysAfterDeparture";
        #endregion

        #region Zoho CRM

        public const string ZohoCrmAuthorizationToken = "ZohoCrm.AuthorizationToken";

        #endregion

        #region WebAnalitics
        public const string WebAnaliticsGoogleTrackingId = "WebAnalitics.GoogleTrackingId";
        #endregion
    }
}

