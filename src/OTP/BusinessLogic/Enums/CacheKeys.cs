﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Enums
{
    /// <summary>
    /// Keys being used by global cache
    /// </summary>
    public enum CacheKeys
    {
        GoogleTrackingId
    }
}
