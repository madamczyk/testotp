﻿using BusinessLogic.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Helpers
{
    public class DateRangeHelper
    {
        /// <summary>
        /// Determines wether 2 date ranges overlap each other
        /// </summary>
        /// <param name="dateRange1">First date range</param>
        /// <param name="dateRange2">Second date range</param>
        /// <returns></returns>
        public static bool HaveDateRangesOverlap(DateRange dateRange1, DateRange dateRange2)
        {
            return ((dateRange1.DateFrom <= dateRange2.DateUntil) && (dateRange1.DateUntil >= dateRange2.DateFrom));
        }

        /// <summary>
        /// Determines wether periods overlap one another
        /// </summary>
        /// <param name="dateRanges">Collection of date ranges</param>
        /// <returns></returns>
        public static bool HaveDateRangesOverlap(params DateRange[] dateRanges) 
        {
            for (int i = 0; i < dateRanges.Count(); i++)
            {
                for (int j = 0; j < dateRanges.Count(); j++)
                {
                    if (i == j)
                        continue;

                    if(HaveDateRangesOverlap(dateRanges[i], dateRanges[j]))
                        return true;
                }
            }

            return false;
        }
    }
}
