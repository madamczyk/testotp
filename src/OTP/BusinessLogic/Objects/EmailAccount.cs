﻿using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects
{
    /// <summary>
    /// Class containing information about outgoing email account
    /// </summary>
    public class EmailAccount
    {
        public string Server { get; set; }
        public int? Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool? SSL { get; set; }
        public string DisplayFrom { get; set; }
        public string AddressFrom { get; set; }

        public EmailAccount()
        {

        }
    }
}
