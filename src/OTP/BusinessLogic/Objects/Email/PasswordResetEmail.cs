﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Email
{
    public class PasswordResetEmail : BaseEmail
    {
        #region Parameters        

        public string UserFirstName
        {
            get { return Parameters["UserFirstName"]; }
            set { Parameters["UserFirstName"] = value; }
        }

        public string TempPassword
        {
            get { return Parameters["TempPassword"]; }
            set { Parameters["TempPassword"] = value; }
        }
        
		#endregion

		#region Ctor

        public PasswordResetEmail(List<string> recipientAddress)
			: base(recipientAddress)
		{
		}

        public PasswordResetEmail(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
        }

		#endregion
    }
}
