﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Email
{
    public class OwnerActivationEmail : BaseEmail
    {
        #region Parameters        

        public string UserFirstName
        {
            get { return Parameters["UserFirstName"]; }
            set { Parameters["UserFirstName"] = value; }
        }

        public string UserLogin
        {
            get { return Parameters["UserLogin"]; }
            set { Parameters["UserLogin"] = value; }
        }

        public string UserPassword
        {
            get { return Parameters["UserPassword"]; }
            set { Parameters["UserPassword"] = value; }
        }

        public string ActivationLinkUrl
        {
            get { return Parameters["ActivationLinkUrl"]; }
            set { Parameters["ActivationLinkUrl"] = value; }
        }

        public string ActivationLinkTitle
        {
            get { return Parameters["ActivationLinkTitle"]; }
            set { Parameters["ActivationLinkTitle"] = value; }
        }
        
		#endregion

		#region Ctor

        public OwnerActivationEmail(List<string> recipientAddress)
			: base(recipientAddress)
		{
		}

        public OwnerActivationEmail(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
        }

		#endregion
	}
}
