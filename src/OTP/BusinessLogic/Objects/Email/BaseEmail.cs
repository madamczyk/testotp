﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Email
{
    public abstract class BaseEmail
    {
        #region Fields

        private List<string> _recipientAddresses;
        private List<string> _ccAddresses;
        private List<string> _replyToAddresses;
        
        #endregion

        #region Parameters

        /// <summary>
        /// Gets or sets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public IDictionary<string, string> Parameters { get; private set; }

        /// <summary>
        /// Gets or sets the recipient e-mail addresses.
        /// </summary>
        /// <value>The recipient e-mail address.</value>
        public List<string> RecipientAddresses
        {
            get { return _recipientAddresses; }
            set
            {
                if (value == null || ((List<string>)value).Count == 0)
                    throw new ArgumentException("Provided e-mail addresses can't be null.", "RecipientAddress");

                foreach (string addr in value)
                {
                    if (string.IsNullOrEmpty(addr))
                        throw new ArgumentException("Provided e-mail address can't be null.", "RecipientAddress");
                }
                _recipientAddresses = value;
            }
        }

        /// <summary>
        /// Name of the recipient
        /// </summary>
        public string RecipientName { get; set; }

        /// <summary>
        /// Gets or sets the recipient e-mail addresses.
        /// </summary>
        /// <value>The recipient e-mail address.</value>
        public List<string> CcAddresses
        {
            get { return _ccAddresses; }
            set
            {
                if (value == null || ((List<string>)value).Count == 0)
                    throw new ArgumentException("Provided e-mail addresses can't be null.", "CcAddress");

                foreach (string addr in value)
                {
                    if (string.IsNullOrEmpty(addr))
                        throw new ArgumentException("Provided e-mail address can't be null.", "CcAddress");
                }

                _ccAddresses = value;
            }
        }

        /// <summary>
        /// Gets or sets the replyTo e-mail addresses.
        /// </summary>
        /// <value>The replyTo e-mail address.</value>
        public List<string> ReplyToAddresses
        {
            get
            {
                if (_replyToAddresses == null)
                {
                    _replyToAddresses = new List<string>();
                }
                return _replyToAddresses;
            }
            set
            {
                if (value == null || ((List<string>)value).Count == 0)
                    throw new ArgumentException("Provided e-mail addresses can't be null.", "CcAddress");

                foreach (string addr in value)
                {
                    if (string.IsNullOrEmpty(addr))
                        throw new ArgumentException("Provided e-mail address can't be null.", "CcAddress");
                }

                _replyToAddresses = value;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseEmail"/> class.
        /// </summary>
        /// <param name="recipientAddresses">The recipient address.</param>
        /// <param name="ccAddresses">The cc address.</param>
        public BaseEmail(List<string> recipientAddresses, List<string> ccAddresses, List<string> replyToAddresses)
        {
            RecipientAddresses = recipientAddresses;
            CcAddresses = ccAddresses;
            ReplyToAddresses = replyToAddresses;

            Parameters = new Dictionary<string, string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseEmail"/> class.
        /// </summary>
        /// <param name="recipientAddresses">The recipient address.</param>
        /// <param name="ccAddresses">The cc address.</param>
        public BaseEmail(List<string> recipientAddresses, List<string> ccAddresses)
        {
            RecipientAddresses = recipientAddresses;
            CcAddresses = ccAddresses;

            Parameters = new Dictionary<string, string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseEmail"/> class.
        /// </summary>
        /// <param name="recipientAddresses">The recipient address.</param>
        public BaseEmail(List<string> recipientAddresses)
        {
            RecipientAddresses = recipientAddresses;
            Parameters = new Dictionary<string, string>();
        }

        #endregion
    }
}
