﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects
{
    /// <summary>
    /// Helper class for calculating 7 Days Discount for invoice
    /// </summary>
    public class Invoice7DayDiscount
    {
        /// <summary>
        /// Number of vacation days that are in the discount period
        /// </summary>
        public int NumberOfDiscountDays { get; set; }
        /// <summary>
        /// Overall number of discount days (number of days from begin to end)
        /// </summary>
        public int NumberOfDiscountPeriodDays { get; set; }
        /// <summary>
        /// Percent value of the discount
        /// </summary>
        public decimal DiscountValue { get; set; }
    }
}
