﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BusinessLogic.EVS;

namespace BusinessLogic
{
    [Serializable]
    [XmlRoot("PlatformRequest")]
    public class EVSRequest
    {
        [XmlElement("Credentials")]
        public Credentials UserCredentials {get; set;}
        [XmlElement("CustomerReference")]
        public string CustomerReference { get; set; }
        [XmlElement("Identity")]
        public UserIdentity Identity { get; set; }
    }
}
