﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.EVS
{
    [Serializable]
    public class Product
    {
        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlAttribute("version")]
        public string Version { get; set; }
    }
}
