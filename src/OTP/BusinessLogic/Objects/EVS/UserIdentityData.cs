﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.EVS
{
    [Serializable]
    public class UserIdentityData
    {
        [XmlElement("PrimaryResult")]
        public string PrimaryResult { get; set; }
        [XmlElement("CheckpointScore")]
        public string CheckpointScore { get; set; }
        [XmlElement("AuthenticationScore")]
        public string AuthenticationScore { get; set; }
        [XmlElement("AddressVerificationResult")]
        public DataVerificationResult AddressVerificationResult { get; set; }
        [XmlElement("AddressTypeResult")]
        public DataVerificationResult AddressTypeResult { get; set; }
        [XmlElement("AddressHighRiskResult")]
        public DataVerificationResult AddressHighRiskResult { get; set; }
        [XmlElement("PhoneVerificationResult")]
        public DataVerificationResult PhoneVerificationResult { get; set; }
        [XmlElement("PhoneHighRiskResult")]
        public DataVerificationResult PhoneHighRiskResult { get; set; }
        [XmlElement("ChangeOfAddressResult")]
        public DataVerificationResult ChangeOfAddressResult { get; set; }
        [XmlElement("DriverLicenseResult")]
        public DataVerificationResult DriverLicenseResult { get; set; }
        [XmlElement("SocialSecurityNumberResult")]
        public DataVerificationResult SocialSecurityNumberResult { get; set; }
        [XmlElement("DateOfBirthResult")]
        public DataVerificationResult DateOfBirthResult { get; set; }
        [XmlArray("Questions")]
        public Question[] Questions { get; set; }

    }
}
