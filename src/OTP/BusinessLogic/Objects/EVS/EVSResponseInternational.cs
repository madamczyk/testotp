﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.EVS
{
    [Serializable]
    [XmlRoot("PlatformResponse")]    
    public class EVSResponseInternational : IEVSResponse
    {
        [XmlElement("TransactionDetails")]
        public TransactionDetails TransactionDetails { get; set; }
        [XmlElement("Response")]
        public ResponseResult ResponseResult { get; set; }
    }
}
