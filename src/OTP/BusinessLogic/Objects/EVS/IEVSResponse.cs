﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.EVS
{
    public interface IEVSResponse
    {
        TransactionDetails TransactionDetails { get; set; }
    }
}
