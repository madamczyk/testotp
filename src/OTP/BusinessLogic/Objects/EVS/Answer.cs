﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace BusinessLogic.EVS
{
    [Serializable]
    public class Answer
    {        
        [XmlText]
        public String AnswerText { get; set; }
        [XmlAttribute("correct")]
        public Boolean IsCorrect { get; set; }
    }
}