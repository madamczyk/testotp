﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace BusinessLogic.EVS
{
    [Serializable]
    public class Question
    {
        [XmlAttribute("text")]
        public String QuestionText { get; set; }
        [XmlAttribute("type")]
        public Int32 QuestionType { get; set; }
        [XmlElement("Answer")]
        public Answer[] Answers { get; set; }
    }
}