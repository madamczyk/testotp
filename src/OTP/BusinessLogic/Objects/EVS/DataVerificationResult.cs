﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.EVS
{
    [Serializable]
    public class DataVerificationResult
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
        [XmlText]
        public string ResultText { get; set; }
    }
}
