﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.EVS
{
    [Serializable]
    public class ResponseResult
    {
        [XmlElement("Result")]
        public DataVerificationResult Result { get; set; }
    }
}
