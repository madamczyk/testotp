﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BusinessLogic.EVS
{
    [ServiceContract]
    [XmlSerializerFormat]
    public interface IEVSContractInternational
    {
        [OperationContract]
        EVSResponseInternational International(EVSRequest request);    
    }
}
