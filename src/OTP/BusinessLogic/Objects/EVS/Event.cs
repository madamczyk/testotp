﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.EVS
{
    [Serializable]
    public class Event
    {
        [XmlElement("Error")]
        public EventData EventData { get; set; }
    }

    [Serializable]
    public class EventData
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
        [XmlAttribute("message")]
        public string Message { get; set; }
    }
}
