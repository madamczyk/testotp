﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BusinessLogic.EVS
{
    [ServiceContract]
    [XmlSerializerFormat]
    public interface IEVSContract
    {
        [OperationContract]
        EVSResponse Consumer(EVSRequest reqest);        
    }
}
