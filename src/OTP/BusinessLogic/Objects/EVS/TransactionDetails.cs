﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.EVS
{
    [Serializable]
    public class TransactionDetails
    {
        [XmlElement("TransactionId")]
        public string TransactionId { get; set; }
        [XmlElement("TransactionDate")]
        public string TransactionDate { get; set; }
        [XmlElement("Product")]
        public Product Product { get; set; }
        [XmlElement("CustomerReference")]
        public string CustomerReference {get; set;}
        [XmlElement("DataProviderDuration")]
        public string DataProviderDuration { get; set; }
        [XmlElement("TotalDuration")]
        public string TotalDuration { get; set; }
        [XmlElement("Errors")]
        public Event[] Errors {get; set;}
        [XmlElement("Warnings")]
        public Event[] Warnings { get; set; }
    }
}
