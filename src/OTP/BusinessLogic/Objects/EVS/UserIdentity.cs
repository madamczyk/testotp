﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.EVS
{
    public class UserIdentity
    {
        [XmlElement("FirstName")]
        public string FirstName { get; set; }
        [XmlElement("MiddleName")]
        public string MiddleName { get; set; }
        [XmlElement("LastName")]
        public string LastName { get; set; }
        [XmlElement("Generation")]
        public string Generation { get; set; }
        [XmlElement("Ssn")]
        public string Ssn { get; set; }
        [XmlElement("Street")]
        public string Street { get; set; }
        [XmlElement("City")]
        public string City { get; set; }
        [XmlElement("State")]
        public string State { get; set; }
        [XmlElement("ZipCode")]
        public string ZipCode { get; set; }
        [XmlElement("DateOfBirth")]
        public string DateOfBirth { get; set; }
        [XmlElement("DriverLicenseNumber")]
        public string DriverLicenseNumber { get; set; }
        [XmlElement("DriverLicenseState")]
        public string DriverLicenseState { get; set; }
        [XmlElement("SpouseFirstName")]
        public string SpouseFirstName { get; set; }
        [XmlElement("PhoneNumber")]
        public string PhoneNumber { get; set; }

        //Required by Internatinal service
        [XmlElement("CountryCode")]
        public string CountryCode { get; set; }
        [XmlElement("Gender")]
        public string Gender { get; set; }
    }
}
