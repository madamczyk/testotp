﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects
{
    /// <summary>
    /// Helper class for storing date range
    /// </summary>
    public class DateRange
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateUntil { get; set; }
    }
}
