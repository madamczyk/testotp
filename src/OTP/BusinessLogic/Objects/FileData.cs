﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects
{
    /// <summary>
    /// Object stores basic information about file and file stream
    /// </summary>
    public class FileData
    {
        /// <summary>
        /// File name without extension
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// File extension
        /// </summary>
        public string FileExtension { get; set; }

        /// <summary>
        /// File source
        /// </summary>
        public Stream InputStream { get; set; }

        /// <summary>
        /// Returns file name with extension
        /// </summary>
        /// <returns></returns>
        public string GetFileNameWithExtension()
        {
            return String.Format("{0}.{1}", FileName, FileExtension);
        }
    }
}
