﻿using BusinessLogic.PaymentGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects
{
    [Serializable]
    public class PaymentResult
    {
        public string BookingConfirmationID { get; set; }
        public bool PaymentGatewayResult { get; set; }
        public TransactionResponse GatewayResponse { get; set; }

        public PaymentResult()
        {
            PaymentGatewayResult = false;
            GatewayResponse = new TransactionResponse();
            
        }
    }
}
