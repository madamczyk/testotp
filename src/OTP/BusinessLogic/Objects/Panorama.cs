﻿using Common.Serialization;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects
{
    [Serializable]
    public class Panorama
    {
        /// <summary>
        /// Panorama id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// X position on image
        /// </summary>
        public int OffsetX { get; set; }

        /// <summary>
        /// Y position on image
        /// </summary>
        public int OffsetY { get; set; }

        /// <summary>
        /// Absolute path to azure blob
        /// </summary>
        public string PanoramaPath { get; set; }

        /// <summary>
        /// Panorama name
        /// </summary>
        public string Name_i18n { get; set; }

        #region Name i18n Wrapper
        private i18nString nameInternal;

        public string PanoramaNameCurrentLanguage { get; set; }

        public i18nString PanoramaName
        {
            get
            {
                if (nameInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Name_i18n))
                    {
                        nameInternal = (i18nString)SerializationHelper.DeserializeObject(this.Name_i18n, typeof(i18nString));
                    }
                    else
                    {
                        nameInternal = new i18nString();
                    }
                }

                return nameInternal;
            }
        }

        public void SetPanoramaNameValue(string content, string code = null)
        {
            if (nameInternal == null)
                nameInternal = new i18nString();
            this.nameInternal.SetValue(code, content);
            this.Name_i18n = SerializationHelper.SerializeObject(nameInternal);
        }

        #endregion
    }

}
