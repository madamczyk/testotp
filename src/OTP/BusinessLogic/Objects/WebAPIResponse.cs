﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace BusinessLogic.Objects
{
    [Serializable]
    [XmlRoot("result")]
    public class WebAPIResponse
    {
        [XmlElement("message")]
        public string Message { get; set; }

        [XmlArray("errors")]
        [XmlArrayItem("error")]
        public string[] Errors;
    } 
}
