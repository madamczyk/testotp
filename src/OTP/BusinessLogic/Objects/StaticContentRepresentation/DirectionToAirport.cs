﻿using Common.Serialization;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.StaticContentRepresentation
{
    /// <summary>
    /// FloorPlan object is designed to reflect the xml with the PropertyStaticContent equal to <see cref="PropertyStaticContentType.DirectionToAirport"/>
    /// </summary>
    public class DirectionToAirport
    {
        /// <summary>
        /// Direction name
        /// </summary>
        public string Name_i18n { get; set; }

        public string DirectionLink { get; set; }

        #region Name i18n Wrapper
        private i18nString nameInternal;

        public i18nString Name
        {
            get
            {
                if (nameInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Name_i18n))
                    {
                        nameInternal = (i18nString)SerializationHelper.DeserializeObject(this.Name_i18n, typeof(i18nString));
                    }
                    else
                    {
                        nameInternal = new i18nString();
                    }
                }

                return nameInternal;
            }
        }

        public void SetName(string content, string code = null)
        {
            if (nameInternal == null)
                nameInternal = new i18nString();
            this.nameInternal.SetValue(code, content);
            this.Name_i18n = SerializationHelper.SerializeObject(nameInternal);
        }

        #endregion

    }
}
