﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.CustomObjects;
using Common.Serialization;
using DataAccess.Enums;
using DataAccess.ObjectExtensions.i18n;

namespace BusinessLogic.Objects.StaticContentRepresentation
{
    /// <summary>
    /// FloorPlan object is designed to reflect the xml with the PropertyStaticContent equal to <see cref="PropertyStaticContentType.FloorPlan"/>
    /// </summary>
    [Serializable]
    public class FloorPlan
    {
        /// <summary>
        /// Floor plan name
        /// </summary>
        public string Name_i18n { get; set; }

        /// <summary>
        /// Collection of panoramas
        /// </summary>
        public Panorama[] Panoramas { get; set; }

        /// <summary>
        /// Absolute azure storage path to original floor plan
        /// </summary>
        public string OriginalImagePath { get; set; }

        /// <summary>
        /// Absolute azure storage path to converted floor plan
        /// </summary>
        public string DisplayImagePath { get; set; }

        #region Name i18n Wrapper
        private i18nString nameInternal;

        public string FloorPlanNameCurrentLanguage { get; set; }

        public i18nString FloorPlanName
        {
            get
            {
                if (nameInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Name_i18n))
                    {
                        nameInternal = (i18nString)SerializationHelper.DeserializeObject(this.Name_i18n, typeof(i18nString));
                    }
                    else
                    {
                        nameInternal = new i18nString();
                    }
                }

                return nameInternal;
            }
        }

        public void SetFloorPlanNameValue(string content, string code = null)
        {
            if (nameInternal == null)
                nameInternal = new i18nString();
            this.nameInternal.SetValue(code, content);
            this.Name_i18n = SerializationHelper.SerializeObject(nameInternal);
        }

        #endregion
    }
}
