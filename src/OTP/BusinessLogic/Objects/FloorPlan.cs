﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.CustomObjects;
using Common.Serialization;
using DataAccess.Enums;

namespace BusinessLogic.Objects
{
    [Serializable]
    public class FloorPlan
    {
        /// <summary>
        /// Floor plan name
        /// </summary>
        public string Name_i18n { get; set; }

        /// <summary>
        /// Determines wether floor plan should be default
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Collection of panoramas
        /// </summary>
        public Panorama[] Panoramas { get; set; }

        /// <summary>
        /// Absolute azure storage path to original floor plan
        /// </summary>
        public string OriginalImagePath { get; set; }

        /// <summary>
        /// Absolute azure storage path to converted floor plan
        /// </summary>
        public string DisplayImagePath { get; set; }

        #region Name i18n Wrapper
        private i18nString nameInternal;

        public i18nString FloorPlanName
        {
            get
            {
                if (nameInternal == null)
                {
                    if (!string.IsNullOrEmpty(this.Name_i18n))
                    {
                        nameInternal = (i18nString)SerializationHelper.DeserializeObject(this.Name_i18n, typeof(i18nString));
                    }
                    else
                    {
                        nameInternal = new i18nString();
                    }
                }

                return nameInternal;
            }
        }

        public void SetFloorPlanNameValue(LanguageCode code, string content)
        {
            if (nameInternal == null)
                nameInternal = new i18nString();
            this.nameInternal.SetValue(code, content);
            this.Name_i18n = SerializationHelper.SerializeObject(nameInternal);
        }

        #endregion
    }
}
