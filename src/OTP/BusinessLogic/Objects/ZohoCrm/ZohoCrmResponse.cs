﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.Objects.ZohoCrm
{
    /// <example>
    /// Success response
    /// <?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n
    /// <response uri=\"/crm/private/xml/Leads/insertRecords\">
	///    <result>
	///	    <message>Record(s) added successfully</message>
	///	    <recorddetail>
	///		    <FL val=\"Id\">742988000000074003</FL>
	///		    <FL val=\"Created Time\">2013-03-01 08:48:11</FL>
	///		    <FL val=\"Modified Time\">2013-03-01 08:48:11</FL>
	///		    <FL val=\"Created By\"><![CDATA[Michael Heinze]]></FL>
	///		    <FL val=\"Modified By\"><![CDATA[Michael Heinze]]></FL>
	///	    </recorddetail>
	///    </result>
    ///</response>
    ///
    /// Error response
    /// <response>
    ///   <error>
    ///     <code>4834</code>
    ///     <message>Invalid Ticket Id</message>
    ///   </error>
    /// </response>
    /// </example>
    [Serializable]
    [XmlRoot("response")]
    public class ZohoCrmResponse
    {
        [XmlElement("error")]
        public ZohoCrmResult Error { get; set; }

        [XmlElement("result")]
        public ZohoCrmResult Result { get; set; }

        [XmlAttribute("uri")]
        public string Uri { get; set; }
    }

    public class ZohoCrmResult
    {
        [XmlElement("code")]
        public string Code { get; set; }
        
        [XmlElement("message")]
        public string Message { get; set; }

        [XmlElement("recorddetail")]
        public ZohoCrmFieldsGroup RecordDetail { get; set; }
    }
}
