﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.Objects.ZohoCrm
{
    [Serializable]
    [XmlRoot("Potentials")]
    public class ZohoCrmPotential
    {
        [XmlElement("row")]
        public List<ZohoCrmPotentialRow> Rows { get; set; }
    }

    public class ZohoCrmPotentialRow
    {
        [XmlElement("option")]
        public List<ZohoCrmFieldLabel> Fields { get; set; }

        [XmlAttribute("no")]
        public string no { get; set; }
    }
}
