﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
using System.ComponentModel;

namespace BusinessLogic.Objects.ZohoCrm
{
     
    /// <example>
    /// <Leads>
    ///   <row no="1">
    ///     <FL val="Lead Source">Web Download</FL>
    ///     <FL val="Company">Your Company</FL>
    ///     <FL val="First Name">Hannah</FL>
    ///     <FL val="Last Name">Smith</FL>
    ///     <FL val="Email">testing@testing.com</FL>
    ///     <FL val="Title">Manager</FL>
    ///     <FL val="Phone">1234567890</FL>
    ///     <FL val="Home Phone">0987654321</FL>
    ///     <FL val="Other Phone">1212211212</FL>
    ///     <FL val="Fax">02927272626</FL>
    ///     <FL val="Mobile">292827622</FL>
    ///   </row>
    /// </Leads>
    /// </example>
    [Serializable]
    [XmlRoot("Leads")]
    public class ZohoCrmLead
    {
        [XmlElement("row")]
        public List<ZohoCrmFieldsGroup> Rows { get; set; }
    }
}
