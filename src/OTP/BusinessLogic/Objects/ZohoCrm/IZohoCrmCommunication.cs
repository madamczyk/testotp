﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.ZohoCrm
{
    public interface IZohoCrmCommunication
    {
        /// <summary>
        /// Send request to ZOHO CRM on specified URL and request parametrs based on HTTP POST action
        /// </summary>
        /// <param name="url">ZOHO CRM API service URL</param>
        /// <param name="requestParameters">Request parameters.</param>
        /// <returns>Returns raw response from ZOHO API - it needs to be parsed</returns>
        string SendRequest(string url, string requestParameters);
    }
}
