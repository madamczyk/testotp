﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.Objects.ZohoCrm
{
    /*
        <?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n
        * <success>
        * <Contact param=\"id\">742988000000085003</Contact>
        * <Account param=\"id\">742988000000085001</Account>
        * </success>\r\n"
    */

    [XmlRoot("success")]
    public class ZohoCrmConversionResponse
    {
        [XmlElement("Account")]
        public string Account { get; set; }

        [XmlElement("Contact")]
        public string Contact { get; set; }
    }
}
