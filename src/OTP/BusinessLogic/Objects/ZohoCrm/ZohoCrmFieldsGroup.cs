﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BusinessLogic.Objects.ZohoCrm
{
    public class ZohoCrmFieldsGroup
    {
        [XmlElement("FL")]
        public List<ZohoCrmFieldLabel> Fields { get; set; }

        [XmlAttribute("no")]
        public string no { get; set; }
    }
}
