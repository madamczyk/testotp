﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace BusinessLogic.Objects.ZohoCrm
{
    public class ZohoCrmCommunication : IZohoCrmCommunication
    {
        public string SendRequest(string url, string requestParameters)
        {
            WebRequest webRequest = WebRequest.Create(url);
            webRequest.Method = "POST";

            byte[] byteArray = Encoding.UTF8.GetBytes(requestParameters);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = byteArray.Length;

            using (Stream dataStream = webRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            using (WebResponse response = webRequest.GetResponse())
            using (Stream dataStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(dataStream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
