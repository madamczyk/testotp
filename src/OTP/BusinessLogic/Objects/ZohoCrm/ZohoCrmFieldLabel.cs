﻿using System.Xml.Serialization;

namespace BusinessLogic.Objects.ZohoCrm
{
    public class ZohoCrmFieldLabel
    {
        [XmlAttribute("val")]
        public string ValAttribute { get; set; }

        [XmlText]
        public string Value { get; set; }
    }
}
