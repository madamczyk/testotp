﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.ZohoCrm
{
    public class ZohoCrmConfiguration
    {
        public string ZohoAuthorizationToken { get; set; }
        public string ZohoPassword { get; set; }
        public string ZohoUsername { get; set; }
    }
}
