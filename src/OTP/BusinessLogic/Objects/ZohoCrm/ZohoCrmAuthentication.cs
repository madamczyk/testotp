﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.ZohoCrm
{
    public class ZohoCrmAuthentication
    {
        public bool Result { get; set; }
        public string TimeCreated { get; set; }
        public string Token { get; set; }
    }
}
