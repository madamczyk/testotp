﻿using System;
using System.Collections.Generic;
using System.Data.Services.Common;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects
{
    [DataServiceEntityAttribute]
    public class Subscriber
    {
        public virtual string PartitionKey { get; set; }    
        
        public virtual string RowKey { get; set; }    
        
        public DateTime Timestamp { get; set; }

        public string Email { get; set; }
    }
}
