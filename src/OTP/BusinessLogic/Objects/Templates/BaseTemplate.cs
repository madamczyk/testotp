﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates
{
    public abstract class BaseTemplate
    {
        #region Parameters

        /// <summary>
        /// Gets or sets the parameters which will be replaces by values in templates
        /// </summary>
        /// <value>The parameters.</value>
        public IDictionary<string, string> Parameters 
        { 
            get; 
            set; 
        }

        #endregion

        #region Ctor

        public BaseTemplate()
        {
            this.Parameters = new Dictionary<string, string>();
        }

        #endregion
    }
}
