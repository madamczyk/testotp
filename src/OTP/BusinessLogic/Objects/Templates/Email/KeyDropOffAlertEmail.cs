﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class KeyDropOffAlertEmail : BaseEmail
    {
        #region Parameters

        public string UploadManagerFirstName
        {
            get { return Parameters["UploadManagerFirstName"]; }
            set { Parameters["UploadManagerFirstName"] = value; }
        }

        public string DepartureDate
        {
            get { return Parameters["DepartureDate"]; }
            set { Parameters["DepartureDate"] = value; }
        }

        public string GuestFullName
        {
            get { return Parameters["GuestFullName"]; }
            set { Parameters["GuestFullName"] = value; }
        }

        public string ReservationConfirmationId
        {
            get { return Parameters["ReservationConfirmationId"]; }
            set { Parameters["ReservationConfirmationId"] = value; }
        }

        public string PropertyName
        {
            get { return Parameters["PropertyName"]; }
            set { Parameters["PropertyName"] = value; }
        }

        public string PropertyAddress
        {
            get { return Parameters["PropertyAddress"]; }
            set { Parameters["PropertyAddress"] = value; }
        }

        #endregion

        #region Ctor

        public KeyDropOffAlertEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        public KeyDropOffAlertEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        #endregion
    }
}
