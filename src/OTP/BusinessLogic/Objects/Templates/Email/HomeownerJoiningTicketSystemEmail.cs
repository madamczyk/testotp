﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class HomeownerJoiningTicketSystemEmail : BaseEmail
    {
        #region Parameters

        public string FirstName
        {
            get { return Parameters["FirstName"]; }
            set { Parameters["FirstName"] = value; }
        }

        public string LastName
        {
            get { return Parameters["LastName"]; }
            set { Parameters["LastName"] = value; }
        }

        public string Initials
        {
            get { return Parameters["Initials"]; }
            set { Parameters["Initials"] = value; }
        }

        public string Email
        {
            get { return Parameters["Email"]; }
            set { Parameters["Email"] = value; }
        }

        public string PhoneNumber
        {
            get { return Parameters["PhoneNumber"]; }
            set { Parameters["PhoneNumber"] = value; }
        }

        public string AddressLine1
        {
            get { return Parameters["AddressLine1"]; }
            set { Parameters["AddressLine1"] = value; }
        }

        public string AddressLine2
        {
            get { return Parameters["AddressLine2"]; }
            set { Parameters["AddressLine2"] = value; }
        }

        public string City
        {
            get { return Parameters["City"]; }
            set { Parameters["City"] = value; }
        }

        public string State
        {
            get { return Parameters["State"]; }
            set { Parameters["State"] = value; }
        }

        public string ZipCode
        {
            get { return Parameters["ZipCode"]; }
            set { Parameters["ZipCode"] = value; }
        }

        public string Country
        {
            get { return Parameters["Country"]; }
            set { Parameters["Country"] = value; }
        }

        public string PreferredDates
        {
            get { return Parameters["PreferredDates"]; }
            set { Parameters["PreferredDates"] = value; }
        }

        public string PreferredTimeFrom
        {
            get { return Parameters["PreferredTimeFrom"]; }
            set { Parameters["PreferredTimeFrom"] = value; }
        }

        public string PreferredTimeTo
        {
            get { return Parameters["PreferredTimeTo"]; }
            set { Parameters["PreferredTimeTo"] = value; }
        }

        public string FullName
        {
            get { return Parameters["FullName"]; }
            set { Parameters["FullName"] = value; }
        }
        
		#endregion

		#region Ctor

        public HomeownerJoiningTicketSystemEmail(List<string> recipientAddress)
			: base(recipientAddress)
		{
            base.Sender = DataAccess.Enums.MessageSender.Homeowner;
		}

        public HomeownerJoiningTicketSystemEmail(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
            base.Sender = DataAccess.Enums.MessageSender.Homeowner;
        }

		#endregion
	}
}
