﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class UserAccountAdded : BaseEmail
    {
        #region Parameters

        public string UserFirstName
        {
            get { return Parameters["UserFirstName"]; }
            set { Parameters["UserFirstName"] = value; }
        }

        public string UserLogin
        {
            get { return Parameters["UserLogin"]; }
            set { Parameters["UserLogin"] = value; }
        }

        public string UserPassword
        {
            get { return Parameters["UserPassword"]; }
            set { Parameters["UserPassword"] = value; }
        }
        
		#endregion

		#region Ctor

        public UserAccountAdded(List<string> recipientAddress)
			: base(recipientAddress)
		{
            base.Sender = DataAccess.Enums.MessageSender.Default;
		}

        public UserAccountAdded(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
            base.Sender = DataAccess.Enums.MessageSender.Default;
        }

		#endregion
	}
}
