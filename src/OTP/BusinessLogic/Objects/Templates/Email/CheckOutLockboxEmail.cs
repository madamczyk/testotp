﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class CheckOutLockboxEmail : BaseEmail
    {
        #region Parameters

        public string GuestFirstName
        {
            get { return Parameters["GuestFirstName"]; }
            set { Parameters["GuestFirstName"] = value; }
        }

        public string DepartureDate
        {
            get { return Parameters["DepartureDate"]; }
            set { Parameters["DepartureDate"] = value; }
        }

        public string LockboxCode
        {
            get { return Parameters["LockboxCode"]; }
            set { Parameters["LockboxCode"] = value; }
        }

        public string LockboxLocation
        {
            get { return Parameters["LockboxLocation"]; }
            set { Parameters["LockboxLocation"] = value; }
        }

        public string CheckOutLinkUrl
        {
            get { return Parameters["CheckOutLinkUrl"]; }
            set { Parameters["CheckOutLinkUrl"] = value; }
        }

        #endregion

        #region Ctor

        public CheckOutLockboxEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        public CheckOutLockboxEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        #endregion
    }
}
