﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class ReservationCancelationConfirmationEmail : BaseEmail
    {
        #region Parameters

        public string UserFirstName
        {
            get { return Parameters["UserFirstName"]; }
            set { Parameters["UserFirstName"] = value; }
        }

        public string ConfirmationNumber
        {
            get { return Parameters["ConfirmationNumber"]; }
            set { Parameters["ConfirmationNumber"] = value; }
        }

        public string NameOfPlace
        {
            get { return Parameters["NameOfPlace"]; }
            set { Parameters["NameOfPlace"] = value; }
        }

        public string FromDate
        {
            get { return Parameters["FromDate"]; }
            set { Parameters["FromDate"] = value; }
        }

        public string ToDate
        {
            get { return Parameters["ToDate"]; }
            set { Parameters["ToDate"] = value; }
        }

        #endregion

        #region Ctor

        public ReservationCancelationConfirmationEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        public ReservationCancelationConfirmationEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        #endregion
    }
}
