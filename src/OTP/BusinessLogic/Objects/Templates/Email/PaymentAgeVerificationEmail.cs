﻿using BusinessLogic.Objects.Templates.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Email
{
    public class PaymentAgeVerificationEmail : BaseEmail
    {
        #region Parameters

        public string UserName
        {
            get { return Parameters["UserName"]; }
            set { Parameters["UserName"] = value; }
        }

        public string BirthDate
        {
            get { return Parameters["BirthDate"]; }
            set { Parameters["BirthDate"] = value; }
        }

		#endregion

		#region Ctor

        public PaymentAgeVerificationEmail(List<string> recipientAddress)
			: base(recipientAddress)
		{
		}

        public PaymentAgeVerificationEmail(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
        }

		#endregion
	}
}
