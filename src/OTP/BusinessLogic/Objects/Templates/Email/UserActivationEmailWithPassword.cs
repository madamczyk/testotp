﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Email
{
    public class UserActivationEmailWithPassword : BaseEmail
    {
        #region Parameters

        public string UserFirstName
        {
            get { return Parameters["UserFirstName"]; }
            set { Parameters["UserFirstName"] = value; }
        }

        public string ActivationLinkUrl
        {
            get { return Parameters["ActivationLinkUrl"]; }
            set { Parameters["ActivationLinkUrl"] = value; }
        }

        public string ActivationLinkTitle
        {
            get { return Parameters["ActivationLinkTitle"]; }
            set { Parameters["ActivationLinkTitle"] = value; }
        }

        public string TempPassword
        {
            get { return Parameters["TempPassword"]; }
            set { Parameters["TempPassword"] = value; }
        }
        
		#endregion

		#region Ctor

        public UserActivationEmailWithPassword(List<string> recipientAddress)
			: base(recipientAddress)
		{
		}

        public UserActivationEmailWithPassword(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
        }

		#endregion
	}
}
