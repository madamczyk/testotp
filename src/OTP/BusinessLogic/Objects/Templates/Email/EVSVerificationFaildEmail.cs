﻿using BusinessLogic.Objects.Templates.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Email
{
    public class EVSVerificationFaildEmail : BaseEmail
    {
        #region Parameters

        public string UserName
        {
            get { return Parameters["UserName"]; }
            set { Parameters["UserName"] = value; }
        }

        public string UserId
        {
            get { return Parameters["UserId"]; }
            set { Parameters["UserId"] = value; }
        }

		#endregion

		#region Ctor

        public EVSVerificationFaildEmail(List<string> recipientAddress)
			: base(recipientAddress)
		{
		}

        public EVSVerificationFaildEmail(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
        }

		#endregion
	}
}
