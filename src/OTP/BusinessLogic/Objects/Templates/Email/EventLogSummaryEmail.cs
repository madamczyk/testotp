﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Email
{
    public class EventLogSummaryEmail : BaseEmail
    {
        #region Parameters

        public string Source
        {
            get { return Parameters["Source"]; }
            set { Parameters["Source"] = value; }
        }

        public string SummaryDay
        {
            get { return Parameters["SummaryDay"]; }
            set { Parameters["SummaryDay"] = value; }
        }

        public string NumberOfErrors
        {
            get { return Parameters["NumberOfErrors"]; }
            set { Parameters["NumberOfErrors"] = value; }
        }

        public string NumberOfWarnings
        {
            get { return Parameters["NumberOfWarnings"]; }
            set { Parameters["NumberOfWarnings"] = value; }
        }

        public string DeploymentType
        {
            get { return Parameters["DeploymentType"]; }
            set { Parameters["DeploymentType"] = value; }
        }

		#endregion

		#region Ctor

        public EventLogSummaryEmail(List<string> recipientAddress)
			: base(recipientAddress)
		{
		}

        public EventLogSummaryEmail(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
        }

		#endregion
    }
}
