﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Email
{
    public class PasswordResetBookingEmail : BaseEmail
    {
        #region Parameters

        public string UserFirstName
        {
            get { return Parameters["UserFirstName"]; }
            set { Parameters["UserFirstName"] = value; }
        }

        public string TempPassword
        {
            get { return Parameters["TempPassword"]; }
            set { Parameters["TempPassword"] = value; }
        }

        public string LinkUrl
        {
            get { return Parameters["LinkUrl"]; }
            set { Parameters["LinkUrl"] = value; }
        }



        #endregion

        #region Ctor

        public PasswordResetBookingEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
        }

        public PasswordResetBookingEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
        }

        #endregion
    }
}
