﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class ReservationConfirmationForGuest : BaseEmail
    {
        #region Parameters

        public string GuestFirstName
        {
            get { return Parameters["GuestFirstName"]; }
            set { Parameters["GuestFirstName"] = value; }
        }

        public string ConfirmationCode
        {
            get { return Parameters["ConfirmationCode"]; }
            set { Parameters["ConfirmationCode"] = value; }
        }

        public string GuestFullName
        {
            get { return Parameters["GuestFullName"]; }
            set { Parameters["GuestFullName"] = value; }
        }

        public string GuestsNumber
        {
            get { return Parameters["GuestsNumber"]; }
            set { Parameters["GuestsNumber"] = value; }
        }

        public string CheckIn
        {
            get { return Parameters["CheckIn"]; }
            set { Parameters["CheckIn"] = value; }
        }

        public string CheckOut
        {
            get { return Parameters["CheckOut"]; }
            set { Parameters["CheckOut"] = value; }
        }

        public string NightsNumber
        {
            get { return Parameters["NightsNumber"]; }
            set { Parameters["NightsNumber"] = value; }
        }

        public string CheckInTime
        {
            get { return Parameters["CheckInTime"]; }
            set { Parameters["CheckInTime"] = value; }
        }

        public string CheckOutTime
        {
            get { return Parameters["CheckOutTime"]; }
            set { Parameters["CheckOutTime"] = value; }
        }

        public string BookedDate
        {
            get { return Parameters["BookedDate"]; }
            set { Parameters["BookedDate"] = value; }
        }

        public string PropertyAddress
        {
            get { return Parameters["PropertyAddress"]; }
            set { Parameters["PropertyAddress"] = value; }
        }

        public string PropertyCountry
        {
            get { return Parameters["PropertyCountry"]; }
            set { Parameters["PropertyCountry"] = value; }
        }

        public string DateBeforeArrival
        {
            get { return Parameters["DateBeforeArrival"]; }
            set { Parameters["DateBeforeArrival"] = value; }
        }

        public string Directions
        {
            get { return Parameters["Directions"]; }
            set { Parameters["Directions"] = value; }
        }

        public string MaxNoOfGuests
        {
            get { return Parameters["MaxNoOfGuests"]; }
            set { Parameters["MaxNoOfGuests"] = value; }
        }

        public string OnStreetParkingAvailable
        {
            get { return Parameters["OnStreetParkingAvailable"]; }
            set { Parameters["OnStreetParkingAvailable"] = value; }
        }

        public string Latitude
        {
            get { return Parameters["Latitude"]; }
            set { Parameters["Latitude"] = value; }
        }

        public string Longitude
        {
            get { return Parameters["Longitude"]; }
            set { Parameters["Longitude"] = value; }
        }

        public string GuestEmail
        {
            get { return Parameters["GuestEmail"]; }
            set { Parameters["GuestEmail"] = value; }
        }

        public string PropertyName
        {
            get { return Parameters["PropertyName"]; }
            set { Parameters["PropertyName"] = value; }
        }

        public string CancellationPolicy
        {
            get { return Parameters["CancellationPolicy"]; }
            set { Parameters["CancellationPolicy"] = value; }
        }

        public string OTPFrontEnd
        {
            get { return Parameters["OTPFrontEnd"]; }
            set { Parameters["OTPFrontEnd"] = value; }
        }

        public string CardinalityPerson
        {
            get { return Parameters["CardinalityPerson"]; }
            set { Parameters["CardinalityPerson"] = value; }
        }

        public string TermsOfServiceHref
        {
            get { return Parameters["TermsOfServiceHref"]; }
            set { Parameters["TermsOfServiceHref"] = value; }
        }

		#endregion

		#region Ctor

        public ReservationConfirmationForGuest(List<string> recipientAddress)
			: base(recipientAddress)
		{
            base.Sender = DataAccess.Enums.MessageSender.Support;
		}

        public ReservationConfirmationForGuest(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

		#endregion
	}
}
