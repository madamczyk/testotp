﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class CrmAddLeadOperationErrorEmail : BaseEmail
    {
        #region Parameters

        public string FirstName
        {
            get { return Parameters["FirstName"]; }
            set { Parameters["FirstName"] = value; }
        }

        public string LastName
        {
            get { return Parameters["LastName"]; }
            set { Parameters["LastName"] = value; }
        }

        public string Address1
        {
            get { return Parameters["Address1"]; }
            set { Parameters["Address1"] = value; }
        }

        public string Address2
        {
            get { return Parameters["Address2"]; }
            set { Parameters["Address2"] = value; }
        }

        #endregion

        #region Ctor

        public CrmAddLeadOperationErrorEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
        }

        public CrmAddLeadOperationErrorEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
        }

        #endregion
    }
}
