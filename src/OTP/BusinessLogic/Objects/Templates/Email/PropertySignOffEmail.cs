﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class PropertySignOffEmail : BaseEmail
    {
        #region Parameters

        public string UserFirstName
        {
            get { return Parameters["UserFirstName"]; }
            set { Parameters["UserFirstName"] = value; }
        }

        public string PropertyName
        {
            get { return Parameters["PropertyName"]; }
            set { Parameters["PropertyName"] = value; }
        }

        public string PreviewLink
        {
            get { return Parameters["PreviewLink"]; }
            set { Parameters["PreviewLink"] = value; }
        }

        public string PreviewLinkTitle
        {
            get { return Parameters["PreviewLinkTitle"]; }
            set { Parameters["PreviewLinkTitle"] = value; }
        }

        public string ConfirmSignOffLink
        {
            get { return Parameters["ConfirmSignOffLink"]; }
            set { Parameters["ConfirmSignOffLink"] = value; }
        }

        public string ConfirmSignOffLinkTitle
        {
            get { return Parameters["ConfirmSignOffLinkTitle"]; }
            set { Parameters["ConfirmSignOffLinkTitle"] = value; }
        }


        #endregion

        #region Ctor

        public PropertySignOffEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
        }

        public PropertySignOffEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
        }

        #endregion
    }
}
