﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class CheckInLockboxEmail : BaseEmail
    {
        #region Parameters

        public string GuestFirstName
        {
            get { return Parameters["GuestFirstName"]; }
            set { Parameters["GuestFirstName"] = value; }
        }

        public string ArrivalTimeRanges
        {
            get { return Parameters["ArrivalTimeRanges"]; }
            set { Parameters["ArrivalTimeRanges"] = value; }
        }

        public string PropertyName
        {
            get { return Parameters["PropertyName"]; }
            set { Parameters["PropertyName"] = value; }
        }

        public string PropertyAddress
        {
            get { return Parameters["PropertyAddress"]; }
            set { Parameters["PropertyAddress"] = value; }
        }

        public string ArrivalDate
        {
            get { return Parameters["ArrivalDate"]; }
            set { Parameters["ArrivalDate"] = value; }
        }

        public string CheckIn
        {
            get { return Parameters["CheckIn"]; }
            set { Parameters["CheckIn"] = value; }
        }

        public string LockboxCode
        {
            get { return Parameters["LockboxCode"]; }
            set { Parameters["LockboxCode"] = value; }
        }

        public string LockboxLocation
        {
            get { return Parameters["LockboxLocation"]; }
            set { Parameters["LockboxLocation"] = value; }
        }

        public string AlarmCode
        {
            get { return Parameters["AlarmCode"]; }
            set { Parameters["AlarmCode"] = value; }
        }

        public string AlarmLocation
        {
            get { return Parameters["AlarmLocation"]; }
            set { Parameters["AlarmLocation"] = value; }
        }

        public string OtherAccessInformation
        {
            get { return Parameters["OtherAccessInformation"]; }
            set { Parameters["OtherAccessInformation"] = value; }
        }

        public string WifiName
        {
            get { return Parameters["WifiName"]; }
            set { Parameters["WifiName"] = value; }
        }

        public string WifiPassword
        {
            get { return Parameters["WifiPassword"]; }
            set { Parameters["WifiPassword"] = value; }
        }

        public string Latitude
        {
            get { return Parameters["Latitude"]; }
            set { Parameters["Latitude"] = value; }
        }

        public string Longitude
        {
            get { return Parameters["Longitude"]; }
            set { Parameters["Longitude"] = value; }
        }

        public string ExtranetUrl
        {
            get { return Parameters["ExtranetUrl"]; }
            set { Parameters["ExtranetUrl"] = value; }
        }

        public string CheckInLinkUrl
        {
            get { return Parameters["CheckInLinkUrl"]; }
            set { Parameters["CheckInLinkUrl"] = value; }
        }

        #endregion

        #region Ctor

        public CheckInLockboxEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        public CheckInLockboxEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        #endregion
    }
}
