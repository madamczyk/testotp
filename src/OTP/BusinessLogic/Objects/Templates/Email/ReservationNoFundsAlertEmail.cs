﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class ReservationNoFundsAlertEmail : BaseEmail
    {
        #region Parameters

        public string GuestFullName
        {
            get { return Parameters["GuestFullName"]; }
            set { Parameters["GuestFullName"] = value; }
        }

        public string PropertyName
        {
            get { return Parameters["PropertyName"]; }
            set { Parameters["PropertyName"] = value; }
        }

        public string TravelPeriod
        {
            get { return Parameters["TravelPeriod"]; }
            set { Parameters["TravelPeriod"] = value; }
        }

        public string DepositAmount
        {
            get { return Parameters["DepositAmount"]; }
            set { Parameters["DepositAmount"] = value; }
        }

        public string BookingDate
        {
            get { return Parameters["BookingDate"]; }
            set { Parameters["BookingDate"] = value; }
        }

        public string ConfirmID
        {
            get { return Parameters["ConfirmID"]; }
            set { Parameters["ConfirmID"] = value; }
        }
        
        #endregion

        #region Ctor

        public ReservationNoFundsAlertEmail(List<string> recipientAddress)
			: base(recipientAddress)
		{
		}

        public ReservationNoFundsAlertEmail(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
        }

        #endregion
    }
}
