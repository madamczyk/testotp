﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class OwnerJoiningEmail : BaseEmail
    {
        #region Parameters

        public string UserFirstName
        {
            get { return Parameters["UserFirstName"]; }
            set { Parameters["UserFirstName"] = value; }
        }

        public string AccountManagerName
        {
            get { return Parameters["AccountManagerName"]; }
            set { Parameters["AccountManagerName"] = value; }
        }

        public string PhoneNumber
        {
            get { return Parameters["PhoneNumber"]; }
            set { Parameters["PhoneNumber"] = value; }
        }

        public string PreferredDates
        {
            get { return Parameters["PreferredDates"]; }
            set { Parameters["PreferredDates"] = value; }
        }

        public string PreferredTimeFrom
        {
            get { return Parameters["PreferredTimeFrom"]; }
            set { Parameters["PreferredTimeFrom"] = value; }
        }

        public string PreferredTimeTo
        {
            get { return Parameters["PreferredTimeTo"]; }
            set { Parameters["PreferredTimeTo"] = value; }
        }
        #endregion

        #region Ctor

        public OwnerJoiningEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        public OwnerJoiningEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        #endregion
    }
}
