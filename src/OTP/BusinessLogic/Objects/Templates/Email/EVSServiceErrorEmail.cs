﻿using BusinessLogic.Objects.Templates.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Email
{
    public class EVSServiceErrorEmail : BaseEmail
    {
        #region Parameters

        public string UserName
        {
            get { return Parameters["UserName"]; }
            set { Parameters["UserName"] = value; }
        }

        public string UserId
        {
            get { return Parameters["UserId"]; }
            set { Parameters["UserId"] = value; }
        }

        public string Message
        {
            get { return Parameters["Message"]; }
            set { Parameters["Message"] = value; }
        }
		#endregion

		#region Ctor

        public EVSServiceErrorEmail(List<string> recipientAddress)
			: base(recipientAddress)
		{
		}

        public EVSServiceErrorEmail(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
        }

		#endregion
	}
}
