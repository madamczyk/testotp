﻿using BusinessLogic.Objects.Templates.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Email
{
    public class HomeownerContactEmail : BaseEmail
    {
        #region Parameters

        public string Message
        {
            get { return Parameters["Message"]; }
            set { Parameters["Message"] = value; }
        }

        #endregion

		#region Ctor

        public HomeownerContactEmail(List<string> recipientAddress)
			: base(recipientAddress)
		{
		}

        public HomeownerContactEmail(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
        }

		#endregion
	}
}
