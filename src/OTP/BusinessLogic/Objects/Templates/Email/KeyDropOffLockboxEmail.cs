﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class KeyDropOffLockboxEmail : BaseEmail
    {
        #region Parameters

        public string GuestFirstName
        {
            get { return Parameters["GuestFirstName"]; }
            set { Parameters["GuestFirstName"] = value; }
        }

        public string ActivationLinkUrl
        {
            get { return Parameters["ActivationLinkUrl"]; }
            set { Parameters["ActivationLinkUrl"] = value; }
        }

        #endregion

        #region Ctor

        public KeyDropOffLockboxEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        public KeyDropOffLockboxEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        #endregion
    }
}
