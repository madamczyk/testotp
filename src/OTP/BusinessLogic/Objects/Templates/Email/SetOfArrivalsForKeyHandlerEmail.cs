﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class SetOfArrivalsForKeyHandlerEmail : BaseEmail
    {
        #region Parameters

        public string KeyManagerFirstName
        {
            get { return Parameters["KeyManagerFirstName"]; }
            set { Parameters["KeyManagerFirstName"] = value; }
        }

        public string TomorrowDate
        {
            get { return Parameters["TomorrowDate"]; }
            set { Parameters["TomorrowDate"] = value; }
        }

        public string CheckIn
        {
            get { return Parameters["CheckIn"]; }
            set { Parameters["CheckIn"] = value; }
        }

        public string EstimatedArrivalTime
        {
            get { return Parameters["EstimatedArrivalTime"]; }
            set { Parameters["EstimatedArrivalTime"] = value; }
        }

        public string PropertyName
        {
            get { return Parameters["PropertyName"]; }
            set { Parameters["PropertyName"] = value; }
        }

        public string UnitName
        {
            get { return Parameters["UnitName"]; }
            set { Parameters["UnitName"] = value; }
        }

        public string GuestFullName
        {
            get { return Parameters["GuestFullName"]; }
            set { Parameters["GuestFullName"] = value; }
        }

        #endregion

        #region Ctor

        public SetOfArrivalsForKeyHandlerEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        public SetOfArrivalsForKeyHandlerEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        #endregion
    }
}
