﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class ReservationConfirmationForOwner : BaseEmail
    {
        #region Parameters

        public string OwnerFirstName
        {
            get { return Parameters["OwnerFirstName"]; }
            set { Parameters["OwnerFirstName"] = value; }
        }

        public string PropertyName
        {
            get { return Parameters["PropertyName"]; }
            set { Parameters["PropertyName"] = value; }
        }

        public string ConfirmationId
        {
            get { return Parameters["ConfirmationId"]; }
            set { Parameters["ConfirmationId"] = value; }
        }

        public string Booked
        {
            get { return Parameters["Booked"]; }
            set { Parameters["Booked"] = value; }
        }

        public string CheckIn
        {
            get { return Parameters["CheckIn"]; }
            set { Parameters["CheckIn"] = value; }
        }

        public string CheckOut
        {
            get { return Parameters["CheckOut"]; }
            set { Parameters["CheckOut"] = value; }
        }

        public string GuestFullName
        {
            get { return Parameters["GuestFullName"]; }
            set { Parameters["GuestFullName"] = value; }
        }

        public string GrossDue
        {
            get { return Parameters["GrossDue"]; }
            set { Parameters["GrossDue"] = value; }
        }

        public string TransactionFee
        {
            get { return Parameters["TransactionFee"]; }
            set { Parameters["TransactionFee"] = value; }
        }

        public string OwnerNet
        {
            get { return Parameters["OwnerNet"]; }
            set { Parameters["OwnerNet"] = value; }
        }

        public string OTPFrontEnd
        {
            get { return Parameters["OTPFrontEnd"]; }
            set { Parameters["OTPFrontEnd"] = value; }
        }

		#endregion

		#region Ctor

        public ReservationConfirmationForOwner(List<string> recipientAddress)
			: base(recipientAddress)
		{
            base.Sender = DataAccess.Enums.MessageSender.Homeowner;
		}

        public ReservationConfirmationForOwner(string recipientAddress)
            : base(new List<string>() {recipientAddress})
        {
            base.Sender = DataAccess.Enums.MessageSender.Homeowner;
        }

		#endregion
	}
}
