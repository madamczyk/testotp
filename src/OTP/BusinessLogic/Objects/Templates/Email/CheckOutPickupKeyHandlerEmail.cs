﻿using System.Collections.Generic;

namespace BusinessLogic.Objects.Templates.Email
{
    public class CheckOutPickupKeyHandlerEmail : BaseEmail
    {
        #region Parameters

        public string GuestFirstName
        {
            get { return Parameters["GuestFirstName"]; }
            set { Parameters["GuestFirstName"] = value; }
        }

        public string DepartureDate
        {
            get { return Parameters["DepartureDate"]; }
            set { Parameters["DepartureDate"] = value; }
        }

        public string KeyManagerFullName
        {
            get { return Parameters["KeyManagerFullName"]; }
            set { Parameters["KeyManagerFullName"] = value; }
        }

        public string KeyManagerAddress
        {
            get { return Parameters["KeyManagerAddress"]; }
            set { Parameters["KeyManagerAddress"] = value; }
        }

        public string CheckOutLinkUrl
        {
            get { return Parameters["CheckOutLinkUrl"]; }
            set { Parameters["CheckOutLinkUrl"] = value; }
        }

        #endregion

        #region Ctor

        public CheckOutPickupKeyHandlerEmail(List<string> recipientAddress)
            : base(recipientAddress)
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        public CheckOutPickupKeyHandlerEmail(string recipientAddress)
            : base(new List<string>() { recipientAddress })
        {
            base.Sender = DataAccess.Enums.MessageSender.Support;
        }

        #endregion
    }
}
