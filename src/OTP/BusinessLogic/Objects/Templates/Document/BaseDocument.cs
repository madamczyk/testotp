﻿using DataAccess.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Document
{
    public abstract class BaseDocument : BaseTemplate
    {       
        #region Ctor

        /// <summary>
        /// Type of document
        /// </summary>
        public DocumentType DocumentType
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseDocument()
        {

        }

        #endregion
    }
}
