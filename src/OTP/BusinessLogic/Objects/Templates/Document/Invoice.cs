﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Document
{
    public class Invoice : BaseDocument
    {
        #region Fields

        public string PositionsTag = "<tr><td class='alignLeft'>{0}</td><td class='alignRight'>{1}</td><td class='alignCenter'>{2}</td><td class='alignRight'>{3}</td></tr>";

        #endregion

        #region Parameters

        public string GuestFullName
        {
            get { return Parameters["GuestFullName"]; }
            set { Parameters["GuestFullName"] = value; }
        }

        public string GuestAddress1
        {
            get { return Parameters["GuestAddress1"]; }
            set { Parameters["GuestAddress1"] = value; }
        }

        public string GuestAddress2
        {
            get { return Parameters["GuestAddress2"]; }
            set { Parameters["GuestAddress2"] = value; }
        }

        public string PropertyName
        {
            get { return Parameters["PropertyName"]; }
            set { Parameters["PropertyName"] = value; }
        }

        public string PropertyAddress
        {
            get { return Parameters["PropertyAddress"]; }
            set { Parameters["PropertyAddress"] = value; }
        }

        public string ConfirmationId
        {
            get { return Parameters["ConfirmationId"]; }
            set { Parameters["ConfirmationId"] = value; }
        }

        public string InvoicePositions
        {
            get { return Parameters["InvoicePositions"]; }
            set { Parameters["InvoicePositions"] = value; }
        }

        public string Subtotal
        {
            get { return Parameters["Subtotal"]; }
            set { Parameters["Subtotal"] = value; }
        }

        public string SalesAndTaxes
        {
            get { return Parameters["SalesAndTaxes"]; }
            set { Parameters["SalesAndTaxes"] = value; }
        }

        public string Total
        {
            get { return Parameters["Total"]; }
            set { Parameters["Total"] = value; }
        }

        public string CreatedDate
        {
            get { return Parameters["CreatedDate"]; }
            set { Parameters["CreatedDate"] = value; }
        }

        public string InvoiceId
        {
            get { return Parameters["InvoiceId"]; }
            set { Parameters["InvoiceId"] = value; }
        }

        public string SecondPartDue
        {
            get { return Parameters["SecondPartDue"]; }
            set { Parameters["SecondPartDue"] = value; }
        }

        public string SevenDayDiscount
        {
            get { return Parameters["SevenDayDiscount"]; }
            set { Parameters["SevenDayDiscount"] = value; }
        }
       
        public string DuePercent
        {
            get { return Parameters["DuePercent"]; }
            set { Parameters["DuePercent"] = value; }
        }

        public string DueByDate
        {
            get { return Parameters["DueByDate"]; }
            set { Parameters["DueByDate"] = value; }
        }

        public string CancellationPolicy
        {
            get { return Parameters["CancellationPolicy"]; }
            set { Parameters["CancellationPolicy"] = value; }
        }

        public string SecurityDeposit
        {
            get { return Parameters["SecurityDeposit"]; }
            set { Parameters["SecurityDeposit"] = value; }
        }


        #endregion

        #region Ctor

        public Invoice()
        {

        }

        #endregion
    }
}
