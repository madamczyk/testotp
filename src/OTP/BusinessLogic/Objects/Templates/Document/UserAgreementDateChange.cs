﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Document
{
    public class UserAgreementDateChange : BaseDocument
    {
        #region Parameters

        public string ConfirmationCode
        {
            get { return Parameters["ConfirmationCode"]; }
            set { Parameters["ConfirmationCode"] = value; }
        }

        public string OwnerFullName
        {
            get { return Parameters["OwnerFullName"]; }
            set { Parameters["OwnerFullName"] = value; }
        }

        public string GuestFullName
        {
            get { return Parameters["GuestFullName"]; }
            set { Parameters["GuestFullName"] = value; }
        }       

        public string CheckInDate
        {
            get { return Parameters["CheckInDate"]; }
            set { Parameters["CheckInDate"] = value; }
        }

        public string CheckOutDate
        {
            get { return Parameters["CheckOutDate"]; }
            set { Parameters["CheckOutDate"] = value; }
        }       

        public string OwnerInitials
        {
            get { return Parameters["OwnerInitials"]; }
            set { Parameters["OwnerInitials"] = value; }
        }

        public string GuestInitials
        {
            get { return Parameters["GuestInitials"]; }
            set { Parameters["GuestInitials"] = value; }
        }

        #endregion

        #region Ctor

        public UserAgreementDateChange()
        {
        }

        #endregion
    }
}
