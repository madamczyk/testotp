﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Templates.Document
{
    public class UserAgreement : BaseDocument
    {
        #region Parameters

        public string ConfirmationCode
        {
            get { return Parameters["ConfirmationCode"]; }
            set { Parameters["ConfirmationCode"] = value; }
        }

        public string CreatedDate
        {
            get { return Parameters["CreatedDate"]; }
            set { Parameters["CreatedDate"] = value; }
        }

        public string OwnerFullName
        {
            get { return Parameters["OwnerFullName"]; }
            set { Parameters["OwnerFullName"] = value; }
        }

        public string GuestFullName
        {
            get { return Parameters["GuestFullName"]; }
            set { Parameters["GuestFullName"] = value; }
        }

        public string PropertyAddress
        {
            get { return Parameters["PropertyAddress"]; }
            set { Parameters["PropertyAddress"] = value; }
        }

        public string Amenities
        {
            get { return Parameters["Amenities"]; }
            set { Parameters["Amenities"] = value; }
        }

        public string GuestsNumbers
        {
            get { return Parameters["GuestsNumbers"]; }
            set { Parameters["GuestsNumbers"] = value; }
        }

        public string CheckInDate
        {
            get { return Parameters["CheckInDate"]; }
            set { Parameters["CheckInDate"] = value; }
        }

        public string CheckOutDate
        {
            get { return Parameters["CheckOutDate"]; }
            set { Parameters["CheckOutDate"] = value; }
        }

        public string PaymentInfo
        {
            get { return Parameters["PaymentInfo"]; }
            set { Parameters["PaymentInfo"] = value; }
        }

        public string OwnerInitials
        {
            get { return Parameters["OwnerInitials"]; }
            set { Parameters["OwnerInitials"] = value; }
        }

        public string GuestInitials
        {
            get { return Parameters["GuestInitials"]; }
            set { Parameters["GuestInitials"] = value; }
        }

        public string TermsOfServiceHref
        {
            get { return Parameters["TermsOfServiceHref"]; }
            set { Parameters["TermsOfServiceHref"] = value; }
        }        

        #endregion

        #region Ctor

        public UserAgreement()
        {
        }

        #endregion
    }
}
