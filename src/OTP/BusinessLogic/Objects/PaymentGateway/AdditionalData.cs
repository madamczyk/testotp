﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.PaymentGateway
{
    [Serializable]
    public class AdditionalData
    {
        [XmlElement("initials")]
        public string Initials { get; set; }
        [XmlElement("holderId")]
        public string CreditHolderId { get; set; }
        [XmlElement("name")]
        public string FirstName { get; set; }
        [XmlElement("second")]
        public string LastName { get; set; }
        [XmlElement("address")]
        public string Address { get; set; }
        [XmlElement("city")]
        public string City { get; set; }
        [XmlElement("state")]
        public string State { get; set; }
        [XmlElement("zip")]
        public string ZipCode { get; set; }
        [XmlElement("country")]
        public string CountryId { get; set; }
    }
}
