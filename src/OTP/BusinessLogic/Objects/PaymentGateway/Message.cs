﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.Objects.PaymentGateway
{
    [Serializable]
    public class Message
    {
        [XmlAttribute("key")]
        public string MessageKey { get; set; }
        [XmlText]
        public string Text { get; set; }
    }
}
