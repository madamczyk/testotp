﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.PaymentGateway
{
    [Serializable]
    [XmlRoot("transaction")]
    public class TransactionRequest
    {
        [XmlElement("payment_method_token")]
        public string PaymentToken { get; set; }
        [XmlElement("amount")]
        public string Amount { get; set; }
        [XmlElement("currency_code")]
        public string CurrencyCode { get; set; }
    }
}
