﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.PaymentGateway
{
    [Serializable]
    [XmlRoot("payment_method")]
    public class PaymentMethodResponse
    {
        [XmlElement("token")]
        public string Token { get; set; }
        [XmlElement("last_four_digits")]
        public string Last4Digits { get; set; }
        [XmlElement("card_type")]
        public string CardType { get; set; }
        [XmlElement("data")]
        public AdditionalData AdditionalData { get; set; }
        [XmlElement("number")]
        public string CreditCardNumber { get; set; }
        [XmlArray("errors")]
        public string[] Errors {get; set;}
    }
}
