﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.PaymentGateway
{
    [Serializable]
    public class TransactionResponseElement
    {
        [XmlElement("success")]
        public bool Success { get; set; }
        [XmlElement("message")]
        public string Message { get; set; }
        [XmlElement("error_code")]
        public string ErrorCode { get; set; }
        [XmlElement("payment_method")]
        public PaymentMethodResponse PaymentMethod {get; set;}
    }
}
