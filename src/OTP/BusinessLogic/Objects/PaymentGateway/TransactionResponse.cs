﻿using BusinessLogic.Objects.PaymentGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BusinessLogic.PaymentGateway
{
    [Serializable]
    [XmlRoot("transaction")]
    public class TransactionResponse
    {
        [XmlElement("amount")]
        public string Amount { get; set; }
        [XmlElement("on_test_gateway")]        
        public string OnTest { get; set; }
        [XmlElement("currency_code")]
        public string CurrencyCode { get; set; }
        [XmlElement("succeeded")]
        public bool Succeded { get; set; }
        [XmlElement("state")]
        public string State { get; set; }
        [XmlElement("token")]
        public string Token { get; set; }
        [XmlElement("transaction_type")]
        public string TransactionType { get; set; }
        [XmlElement("message")]
        public Message Message { get; set; }
        [XmlElement("gateway_token")]
        public string GatewayToken { get; set; }
        [XmlElement("response")]
        public TransactionResponseElement Response { get; set; }
    }
}
