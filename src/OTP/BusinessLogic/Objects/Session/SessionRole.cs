﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Objects.Session
{
    [Serializable]
    public class SessionRole
    {     
        #region ctor

        public SessionRole()
        {

        }

        public SessionRole(Role role)
        {
            Apply(role);
        }

        #endregion

        #region Properties 

        public int RoleId { get; set; }

        public string Name { get; set; }

        public int RoleLevel { get; set; }

        #endregion

        #region Members

        public void Apply(Role role)
        {
            RoleId = role.RoleId;
            Name = role.Name;
            RoleLevel = role.RoleLevel;
        }

        #endregion
    }
}
