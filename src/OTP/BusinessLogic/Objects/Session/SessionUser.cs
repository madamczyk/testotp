﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace BusinessLogic.Objects.Session
{
    /// <summary>
    /// Class that contains selected user data. Stored in session
    /// </summary>
    [Serializable]
    public class SessionUser
    {
        #region ctor

        public SessionUser()
        {

        }

        public SessionUser(User user)
        {
            Apply(user);
        }

        #endregion

        #region Properties

        public int UserID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public bool IsGeneratedPassword { get; set; }
        
        /// <summary>
        /// Culture code choosed by user
        /// </summary>
        public string CultureCode { get; set; }

        /// <summary>
        /// One of the user's role that the user is logged in
        /// </summary>
        public List<SessionRole> LoggedInRoles { get; set; }

        /// <summary>
        /// Current user role - 
        /// </summary>
        public SessionRole LoggedInRole 
        {
            get
            {
                return LoggedInRoles.FirstOrDefault();
            }
        }

        public string FullName
        {
            get
            {
                return String.Format("{0} {1}", FirstName, LastName);
            }
        }

        #endregion

        #region Members

        public void Apply(User user)
        {
            UserID = user.UserID;
            FirstName = user.Firstname;
            LastName = user.Lastname;
            Email = user.email;
            CultureCode = user.DictionaryCulture.CultureCode;
            IsGeneratedPassword = user.IsGeneratedPassword;

            if (user.LoggedInRoles != null)
            {
                this.LoggedInRoles = new List<SessionRole>();
                Apply(user.LoggedInRoles);
            }
        }

        public void Apply(List<Role> roles)
        {
            foreach (var r in roles)
            {
                LoggedInRoles.Add(new SessionRole(r));
            }
        }

        public void Apply(Role role)
        {
            this.LoggedInRoles = new List<SessionRole>();
            Apply(new List<Role>() { role });
        }

        #endregion
    }
}
