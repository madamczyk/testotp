﻿using Common.Resources;
using DataAccess.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTests.BasicObjectsTests
{
    [TestClass]
    public class ArrivalTimeRangeTest
    {
        [TestMethod]
        public void ArrivalRangeConversionTest()
        {
            // Arrange
            ArrivalTimeRange timeRange2PM = ArrivalTimeRange.From2PM;
            ArrivalTimeRange timeRange4PM = ArrivalTimeRange.From4PM;
            ArrivalTimeRange timeRange6PM = ArrivalTimeRange.From6PM;
            ArrivalTimeRange timeRange8PM = ArrivalTimeRange.From8PM;
            ArrivalTimeRange timeRange10PM = ArrivalTimeRange.From10PM;

            // Act
            string textVal2PM = timeRange2PM.GetTextRepresentation();
            string textVal4PM = timeRange4PM.GetTextRepresentation();
            string textVal6PM = timeRange6PM.GetTextRepresentation();
            string textVal8PM = timeRange8PM.GetTextRepresentation();
            string textVal10PM = timeRange10PM.GetTextRepresentation();

            // Assert
            Assert.AreEqual(ArrivalTimeRangeResource.ArrivalPeriodFrom2PM, textVal2PM);
            Assert.AreEqual(ArrivalTimeRangeResource.ArrivalPeriodFrom4PM, textVal4PM);
            Assert.AreEqual(ArrivalTimeRangeResource.ArrivalPeriodFrom6PM, textVal6PM);
            Assert.AreEqual(ArrivalTimeRangeResource.ArrivalPeriodFrom8PM, textVal8PM);
            Assert.AreEqual(ArrivalTimeRangeResource.ArrivalPeriodFrom10PM, textVal10PM);
        }

        [TestMethod]
        public void ArrivalRangeListConversionTest()
        {
            // Act
            var list = ArrivalTimeRangeExtensions.GetAllValues();

            // Assert
            int expectedNumberOfTimeRanges = 5;

            Assert.AreEqual(expectedNumberOfTimeRanges, list.Count());
            Assert.AreEqual(ArrivalTimeRangeResource.ArrivalPeriodFrom2PM, list[0]);
        }

        [TestMethod]
        public void ArrivalRangeDictionaryConversionTest()
        {
            // Act
            var list = ArrivalTimeRangeExtensions.GetAllValuesWithKeys();

            // Assert
            int expectedNumberOfTimeRanges = 5;

            Assert.AreEqual(expectedNumberOfTimeRanges, list.Count());
            Assert.AreEqual(ArrivalTimeRangeResource.ArrivalPeriodFrom2PM, list[1]);
        }
    }
}
