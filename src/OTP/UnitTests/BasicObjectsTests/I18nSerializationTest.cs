﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccess;
using BusinessLogic.Managers;
using BusinessLogic.Services;
using BusinessLogic.ServiceContracts;
using Moq;
using System.Configuration;

namespace UnitTests.BasicObjectsTests
{
    [TestClass]
    public class I18nSerializationTest
    {
        [TestMethod]
        public void TaxI18nTest()
        {
            #region Setup TaxesService
            var efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(efContext);
            mock.Setup(f => f.Current).Returns(new object());

            ITaxesService taxesService = new TaxesService(mock.Object);
            #endregion

            #region Setup DestinationsService
            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            IDestinationsService destinationsService = new DestinationsService(configurationService, mock.Object);
            #endregion

            //ensure Destination
            Destination d = new Destination();
            d.SetDestinationNameValue("Destination 1");
            d.SetDestinationDescriptionValue("Destination Description");
            destinationsService.AddDestination(d);

            //add tax
            Tax t = new Tax();
            t.Percentage = 23;
            t.Destination = d;
            string taxEn = "Value Added Tax";
            string taxPl = "Podatek od towarów i usług";
            t.SetNameValue(taxEn, DataAccess.Enums.CultureCode.en_US);
            t.SetNameValue(taxPl, DataAccess.Enums.CultureCode.pl_PL);

            taxesService.AddTax(t);
            efContext.SaveChanges();

            int taxId = t.TaxId;

            //retrieve tax and Assert i18n Properties
            t = taxesService.GetTaxById(taxId);
            Assert.AreEqual(taxEn, t.Name.ToString(DataAccess.Enums.CultureCode.en_US));
            Assert.AreEqual(taxPl, t.Name.ToString(DataAccess.Enums.CultureCode.pl_PL));

            //cleanup
            taxesService.DeleteTax(taxId);
            destinationsService.DeleteDestination(d.DestinationID);
        }
    }
}
