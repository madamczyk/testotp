﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic.ServiceContracts;
using DataAccess;
using BusinessLogic.Services;
using Moq;
using System.Configuration;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using Common.Converters;

namespace UnitTests.SearchEngineTests
{
    [TestClass]
    public class PropertySearch
    {
        #region Services

        /// <summary>
        /// Entity Framework context
        /// </summary>
        private OTPEntities _dbContext = null;

        /// <summary>
        // Services
        /// </summary>
        private IDestinationsService _destService = null;
        private IPropertiesService _propertyService = null;
        private IBlobService _blobService = null;
        private ITaxesService _taxesService = null;
        private IUserService _userService = null;
        private IDictionaryCountryService _countryService = null;
         
        private IPropertyTypesService _propertyTypeService = null;
        private IUnitsService _unitService = null;
        private IUnitTypesService _unitTypeService = null;
        private IAmenityService _amenityService = null;
        private ITagsService _tagService = null;
        private ISettingsService _settingsService = null;
        private IStateService _stateService = null;
        private IDictionaryCultureService _dictionaryCultureService = null;
        private IZohoCrmService _zohoCrmService = null;
        private IMessageService _messageService = null;

        #endregion

        #region Fields

        /// <summary>
        /// Blob url retrieve from app.config (for Moq purpose)
        /// </summary>
        private string _blobUrl;

        /// <summary>
        /// Added user
        /// </summary>
        private User _user;

        /// <summary>
        /// Added property
        /// </summary>
        private List<Property> _properties = new List<Property>();

        /// <summary>
        /// Unit Types for properties
        /// </summary>
        private List<UnitType> _unitTypes = new List<UnitType>();

        /// <summary>
        /// Units for properties
        /// </summary>
        private List<Unit> _units = new List<Unit>();

        /// <summary>
        /// Original values of OTP settings from DB
        /// </summary>
        private Dictionary<string, string> _originSettings = null;

        /// <summary>
        /// Current language code
        /// </summary>
        private static readonly string languageCode = "en-US";

        #endregion

        #region Prepare Data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Init Services

            _blobUrl = ConfigurationManager.AppSettings[BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl.ToString()];
            _dbContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var contextService = new Mock<IContextService>();
            contextService.Setup(f => f.GetFromContext("EFContext")).Returns(_dbContext);
            contextService.Setup(f => f.Current).Returns(new object());

            var configService = new Mock<IConfigurationService>();
            configService
                .Setup(f => f.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl))
                .Returns(_blobUrl);

            _settingsService = new SettingsService(contextService.Object);
            _destService = new DestinationsService(configService.Object, contextService.Object);
            _taxesService = new TaxesService(contextService.Object);
            _blobService = new BlobService(contextService.Object);
            _propertyService = new PropertiesServices(configService.Object, _blobService, _settingsService, contextService.Object);
            _userService = new UsersService(contextService.Object, _zohoCrmService, _messageService, _settingsService);
            _countryService = new DictionaryCountryService(contextService.Object);
            _propertyTypeService = new PropertyTypeService(contextService.Object);
            _stateService = new StateService(contextService.Object);
            _unitService = new UnitsService(contextService.Object, _stateService, _unitTypeService);
            _unitTypeService = new UnitTypesService(contextService.Object);
            _amenityService = new AmenityService(contextService.Object);
            _tagService = new TagsService(contextService.Object);
            _dictionaryCultureService = new DictionaryCultureService(contextService.Object);

            #endregion

            #region If there are test data in the data base, 'Search Tests' shouldn' t be executed
            Assert.AreEqual(0, _propertyService.GetProperties().Count(), Messages.DataBaseContainsData);
            #endregion

            #region Add Test Data

            AddProperties();
            _dbContext.SaveChanges();

            #endregion

            #region Save original settings values

            SaveOTPSettings();

            #endregion
        }
        #endregion

        #region Clean Up Data

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (Unit u in _units)
            {
                _unitService.DeleteUnit(u.UnitID);
            }
            _units.Clear();

            foreach (UnitType ut in _unitTypes)
            {
                _unitTypeService.DeleteUnitType(ut.UnitTypeID);
            }
            _unitTypes.Clear();

            foreach (Property p in _properties)
            {
                _propertyService.RemoveProperty(p.PropertyID);
            }
            _properties.Clear();

            if (_originSettings != null)
            {
                ///get changed setting values
                IEnumerable<OTP_Settings> changedSettings = _settingsService.GetSettings();
                foreach (OTP_Settings setting in changedSettings)
                {
                    setting.SettingValue = _originSettings[setting.OTP_SettingID.ToString()];
                }
                _dbContext.SaveChanges();
            }

            //Delete user
            _userService.DeleteUserById(_user.UserID);            

            //Save changes to db
            _dbContext.SaveChanges();
        }

        #endregion

        #region Test Methods

        #region Default search parameters

        #region Search by destination without properties

        [TestMethod]
        public void SearchByNotValidDestinationTest()
        {
            var searchResult = _propertyService.SearchProperties(20, new DateTime(2012, 5, 1), new DateTime(2012, 5, 7), 6, 3, 1, null, null, null, languageCode);
            Assert.AreEqual(0, searchResult.Count());
        }

        #endregion

        #region Search by destination within dates - no free units

        [TestMethod]
        public void SearchByBlockedUnitsTest()
        {
            var searchResult = _propertyService.SearchProperties(6, new DateTime(2012, 5, 2), new DateTime(2012, 5, 10), 6, 3, 1, null, null, null, languageCode);
            Assert.AreEqual(0, searchResult.Count());
        }

        #endregion

        #region Search by destination, guests, baths and bedrooms number

        [TestMethod]
        public void SearchByDestinationTest()
        {
            var searchResult = _propertyService.SearchProperties(1, null, null, 2, 2, 2, null, null, null, languageCode);
            Assert.IsNotNull(searchResult);
            Assert.AreEqual(2, searchResult.Count());
            Assert.AreEqual(true, searchResult.ElementAt(0).IsPricePerNight);

            PropertiesSearchResult result1 = searchResult.ElementAt(0);
            AssertSearchResult(result1, GetPropertyByCode("p2"), GetUnitByCode("p2.u1"));

            PropertiesSearchResult result2 = searchResult.ElementAt(1);
            AssertSearchResult(result2, GetPropertyByCode("p1"), GetUnitByCode("p1.u1"));
        }

        #endregion

        #region Search by dates, guests, bedrooms, bathrooms numbers

        [TestMethod]
        public void SearchByDatesTest()
        {
            var searchResult = _propertyService.SearchProperties(5, new DateTime(2012, 5, 1), new DateTime(2012, 5, 7), 6, 3, 1, null, null, null, languageCode);
            Assert.IsNotNull(searchResult);
            Assert.AreEqual(1, searchResult.Count());
            Assert.AreEqual(false, searchResult.ElementAt(0).IsPricePerNight);

            PropertiesSearchResult result1 = searchResult.ElementAt(0);
            AssertSearchResult(result1, GetPropertyByCode("p5"), GetUnitByCode("p5.u2"));
        }

        #endregion

        #region Search by amenities, tags, property types
        [TestMethod]
        public void SearchByAmenitiesTagsPropertyTypesTest()
        {
            var searchResult = _propertyService.SearchProperties(
                2, 
                new DateTime(2012, 9, 1),
                new DateTime(2012, 9, 9),
                null,
                null,
                2,
                "1".ToSerializedList("PropertyTypes"),
                "5;6;7;8;9".ToSerializedList("Tags"),
                "9;53".ToSerializedList("Amenities"),
                languageCode);
            Assert.IsNotNull(searchResult);
            Assert.AreEqual(2, searchResult.Count());
            Assert.AreEqual(false, searchResult.ElementAt(0).IsPricePerNight);

            PropertiesSearchResult result1 = searchResult.ElementAt(0);
            AssertSearchResult(result1, GetPropertyByCode("p4"), GetUnitByCode("p4.u4"));

            PropertiesSearchResult result2 = searchResult.ElementAt(1);
            AssertSearchResult(result2, GetPropertyByCode("p3"), GetUnitByCode("p3.u1"));
        }
        #endregion

        #region Search by dates, amenities, tags, prop. types + number of guests, baths and bedrooms
        [TestMethod]
        public void SearchUsingAllFiltersTest()
        {
            var searchResult = _propertyService.SearchProperties(
                6,
                new DateTime(2012, 5, 10),
                new DateTime(2012, 5, 30),
                4,
                3,
                6,
                "1;2".ToSerializedList("PropertyTypes"),
                "1;2;3;5;6;8".ToSerializedList("Tags"),
                "1;2;3;4".ToSerializedList("Amenities"),
                languageCode);

            Assert.IsNotNull(searchResult);
            Assert.AreEqual(1, searchResult.Count());
            Assert.AreEqual(false, searchResult.ElementAt(0).IsPricePerNight);

            PropertiesSearchResult result2 = searchResult.ElementAt(0);
            AssertSearchResult(result2, GetPropertyByCode("p6"), GetUnitByCode("p6.u1"));
        }

        #endregion

        #endregion

        #region Custom search parameters

        [TestMethod]
        public void SearchUsingCustomThresholdTest()
        {
            #region Perform Search
            var searchResult = _propertyService.SearchProperties(
                6,
                new DateTime(2012, 12, 10),
                new DateTime(2012, 12, 20),
                2,
                2,
                2,
                "1".ToSerializedList("PropertyTypes"),
                "1;2;3;".ToSerializedList("Tags"),
                "1;2;3".ToSerializedList("Amenities"),
                languageCode);
            #endregion

            #region Test search results

            Assert.IsNotNull(searchResult);
            Assert.AreEqual(3, searchResult.Count());
            Assert.AreEqual(false, searchResult.ElementAt(0).IsPricePerNight);

            PropertiesSearchResult result2 = searchResult.ElementAt(0);
            AssertSearchResult(result2, GetPropertyByCode("p6"), GetUnitByCode("p6.u1"));

            result2 = searchResult.ElementAt(1);
            AssertSearchResult(result2, GetPropertyByCode("p7"), GetUnitByCode("p7.u1"));
            
            result2 = searchResult.ElementAt(2);
            AssertSearchResult(result2, GetPropertyByCode("p8"), GetUnitByCode("p8.u1"));
            
            #endregion

            #region Threshold = 9.5

            _settingsService.GetSettings().Where(S => S.SettingCode == "FuzzySearch.Threshold").First().SettingValue = "9.50";
            _dbContext.SaveChanges();

            #endregion

            #region Perform Search
            searchResult = null;
            searchResult = _propertyService.SearchProperties(
                6,
                new DateTime(2012, 12, 10),
                new DateTime(2012, 12, 20),
                2,
                2,
                2,
                "1".ToSerializedList("PropertyTypes"),
                "1;2;3;".ToSerializedList("Tags"),
                "1;2;3".ToSerializedList("Amenities"),
                languageCode);
            #endregion

            #region Test search result

            Assert.IsNotNull(searchResult);
            Assert.AreEqual(1, searchResult.Count());
            Assert.AreEqual(false, searchResult.ElementAt(0).IsPricePerNight);
            
            result2 = searchResult.ElementAt(0);
            AssertSearchResult(result2, GetPropertyByCode("p6"), GetUnitByCode("p6.u1"));

            #endregion
        }

        [TestMethod]
        public void SearchUsingCustomDimensionScoresTest()
        {
            #region Perform Search
            var searchResult = _propertyService.SearchProperties(
                6,
                new DateTime(2013, 12, 01),
                new DateTime(2013, 12, 31),
                3,
                2,
                2,
                "1;2".ToSerializedList("PropertyTypes"),
                null,
                "3;5;6;7".ToSerializedList("Amenities"),
                languageCode);
            #endregion

            #region Test search results

            Assert.IsNotNull(searchResult);
            Assert.AreEqual(3, searchResult.Count());
            Assert.AreEqual(false, searchResult.ElementAt(0).IsPricePerNight);

            PropertiesSearchResult result2 = searchResult.ElementAt(0);
            AssertSearchResult(result2, GetPropertyByCode("p6"), GetUnitByCode("p6.u1"));

            result2 = searchResult.ElementAt(1);
            AssertSearchResult(result2, GetPropertyByCode("p7"), GetUnitByCode("p7.u1"));

            result2 = searchResult.ElementAt(2);
            AssertSearchResult(result2, GetPropertyByCode("p8"), GetUnitByCode("p8.u1"));

            #endregion

            #region Dimension Score - change values

            //FuzzySearch.DimensionWeight.Guests
            //FuzzySearch.DimensionWeight.Bathrooms
            //FuzzySearch.DimensionWeight.Bedrooms
            //FuzzySearch.DimensionWeight.Amenities
            //FuzzySearch.DimensionWeight.PropertyTypes
            //FuzzySearch.DimensionWeight.Tags

            _settingsService.GetSettings().Where(S => S.SettingCode == "FuzzySearch.DimensionWeight.Guests").First().SettingValue = "60";
            _settingsService.GetSettings().Where(S => S.SettingCode == "FuzzySearch.DimensionWeight.Bathrooms").First().SettingValue = "5";
            _settingsService.GetSettings().Where(S => S.SettingCode == "FuzzySearch.DimensionWeight.Bedrooms").First().SettingValue = "5";
            _settingsService.GetSettings().Where(S => S.SettingCode == "FuzzySearch.DimensionWeight.Amenities").First().SettingValue = "20";
            _settingsService.GetSettings().Where(S => S.SettingCode == "FuzzySearch.DimensionWeight.PropertyTypes").First().SettingValue = "5";
            _settingsService.GetSettings().Where(S => S.SettingCode == "FuzzySearch.DimensionWeight.Tags").First().SettingValue = "5";
            _dbContext.SaveChanges();

            #endregion

            #region Perform Search
            searchResult = null;
            searchResult = _propertyService.SearchProperties(
                6,
                new DateTime(2013, 12, 01),
                new DateTime(2013, 12, 31),
                3,
                2,
                2,
                "1;2".ToSerializedList("PropertyTypes"),
                null,
                "3;5;6;7".ToSerializedList("Amenities"),
                languageCode);
            #endregion

            #region Test search result

            Assert.IsNotNull(searchResult);
            Assert.AreEqual(3, searchResult.Count());
            Assert.AreEqual(false, searchResult.ElementAt(0).IsPricePerNight);

            result2 = searchResult.ElementAt(0);
            AssertSearchResult(result2, GetPropertyByCode("p6"), GetUnitByCode("p6.u1"));

            result2 = searchResult.ElementAt(1);
            AssertSearchResult(result2, GetPropertyByCode("p7"), GetUnitByCode("p7.u1"));

            result2 = searchResult.ElementAt(2);
            AssertSearchResult(result2, GetPropertyByCode("p8"), GetUnitByCode("p8.u1"));

            #endregion
        }

        [TestMethod]
        public void SearchUsingCustomFSBedroomsParameterTest()
        {
            #region Perform Search
            var searchResult = _propertyService.SearchProperties(
                7,
                new DateTime(2013, 12, 01),
                new DateTime(2013, 12, 31),
                6,
                7,
                5,
                "19;23".ToSerializedList("PropertyTypes"),
                "20".ToSerializedList("Tags"),
                "21;".ToSerializedList("Amenities"),
                languageCode);
            #endregion

            #region Test search results

            Assert.IsNotNull(searchResult);
            Assert.AreEqual(1, searchResult.Count());
            Assert.AreEqual(false, searchResult.ElementAt(0).IsPricePerNight);

            PropertiesSearchResult result2 = searchResult.ElementAt(0);
            AssertSearchResult(result2, GetPropertyByCode("p9"), GetUnitByCode("p9.u2"));

            #endregion

            #region Fuzzy Search - change P and Q params for line function

            //other params have default values = 2
            _settingsService.GetSettings().Where(S => S.SettingCode == "FuzzySearch.Parameters.BedroomsMax").First().SettingValue = "5";
            _settingsService.GetSettings().Where(S => S.SettingCode == "FuzzySearch.Parameters.BedroomsMin").First().SettingValue = "8";
            _dbContext.SaveChanges();

            #endregion

            #region Perform Search
            searchResult = _propertyService.SearchProperties(
                7,
                new DateTime(2013, 12, 01),
                new DateTime(2013, 12, 31),
                6,
                7,
                5,
                "19;23".ToSerializedList("PropertyTypes"),
                "20".ToSerializedList("Tags"),
                "21;".ToSerializedList("Amenities"),
                languageCode);
            #endregion

            #region Test search results

            Assert.IsNotNull(searchResult);
            Assert.AreEqual(2, searchResult.Count());
            Assert.AreEqual(false, searchResult.ElementAt(0).IsPricePerNight);

            result2 = searchResult.ElementAt(0);
            AssertSearchResult(result2, GetPropertyByCode("p9"), GetUnitByCode("p9.u2"));

            result2 = searchResult.ElementAt(1);
            AssertSearchResult(result2, GetPropertyByCode("p10"), GetUnitByCode("p10.u1"));

            #endregion
        }

        #endregion

        #endregion

        #region Private Methods

        /// <summary>
        /// Asserts 
        /// </summary>
        /// <param name="result">Search result</param>
        /// <param name="property">Property object</param>
        /// <param name="unit">Unit object</param>
        private void AssertSearchResult(PropertiesSearchResult result, Property property, Unit unit)
        {
            //full image for property
            int fullImageCode = (int)PropertyStaticContentType.FullScreen;
            PropertyStaticContent fullImage = property.PropertyStaticContents.Where(sc => sc.ContentCode == fullImageCode).FirstOrDefault();
            string imgUrl = string.Format("{0}{1}", _blobUrl, fullImage != null ? fullImage.ContentValue : "");

            //thumbnails
            int thumbsCode = (int)PropertyStaticContentType.ListView;
            var thumbs = property.PropertyStaticContents.Where(sc => sc.ContentCode == thumbsCode).AsEnumerable();
            if (thumbs.Count() > 0)
            {
                foreach (var thumbUrl in thumbs.Select(sc => string.Format("{0}{1}", _blobUrl, sc.ContentValue)))
                {
                    Assert.IsTrue(result.Thumbnails.Contains(thumbUrl));
                }
            }
            Assert.AreEqual(unit.UnitID, result.Id);
            Assert.AreEqual(thumbs.Count(), result.Thumbnails.Count());
            Assert.AreEqual(unit.MaxNumberOfBathrooms, result.Bathrooms);
            Assert.AreEqual(unit.MaxNumberOfBedRooms, result.Bedrooms);
            Assert.AreEqual(unit.MaxNumberOfGuests, result.Sleeps);            
            Assert.AreEqual(property.Short_Description.ToString(), result.Description);
            Assert.AreEqual(imgUrl, result.Image);
            Assert.AreEqual(property.PropertyName.ToString(), result.Title);
        }

        /// <summary>
        /// Get property by code from local Property List
        /// </summary>
        /// <param name="code">Code name</param>
        /// <returns>Property object from local list</returns>
        private Property GetPropertyByCode(string code)
        {
            return _properties.Where(p => p.PropertyCode == code).FirstOrDefault();
        }

        /// <summary>
        /// Get unit from local units list
        /// </summary>
        /// <param name="code"></param>
        /// <returns>The Unit object</returns>
        private Unit GetUnitByCode(string code)
        {
            return _units.Where(u => u.UnitCode == code).FirstOrDefault();
        }

        /// <summary>
        /// Adds sample properties to DB for testing purposes
        /// </summary>
        private void AddProperties()
        {
            #region Local fields
            Property property;
            Unit unit;
            UnitType unitType;
            PropertyStaticContent psc;
            #endregion

            #region Add user
            _userService.AddUser(_user = this.AddUser());
            #endregion

            #region Property 1 Dest 1
            property = new Property();
            property.Destination = _destService.GetDestinationById(1);
            property.Address1 = "address line 1";
            property.ZIPCode = "zip code";
            property.State = "state";
            property.City = "City";
            property.PropertyLive = true;
            property.SetLongDescriptionValue("Long Description");
            property.SetPropertyNameValue("Property Name p1");
            property.SetShortDescriptionValue("Short Description");            
            property.DictionaryCountry = _countryService.GetCountryById(1);
            property.PropertyCode = "p1";
            property.PropertyType = _propertyTypeService.GetPropertyTypeById(1);
            property.User = _user;
            property.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            property.TimeZone = "Eastern Standard Time";
            _propertyService.AddProperty(property);
            _properties.Add(property);

            //Unit Types
            unitType = this.AddUnitTypeForProperty(property);
            _unitTypeService.AddUnitType(unitType);
            _unitTypes.Add(unitType);

            //Units
            unit = this.AddUnitForProperty(property, unitType, 1, 2, 3, "p1.u1");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            UnitRate ur = new UnitRate();
            ur.DailyRate = 245;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            unit = this.AddUnitForProperty(property, unitType, 5, 2, 5, "p1.u2");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 345;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Full image
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.FullScreen;
            psc.ContentValue = "/imageFullP1.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Thumbnails
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP1.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Property Amenities
            AddPropertyAmenities(property, 1);
            AddPropertyAmenities(property, 2);
            AddPropertyAmenities(property, 3);
            AddPropertyAmenities(property, 4);

            //Property Tags
            AddPropertyTag(property, 1);
            AddPropertyTag(property, 2);
            AddPropertyTag(property, 3);

            #endregion

            #region Property 2 Dest 1
            property = new Property();
            property.Destination = _destService.GetDestinationById(1);
            property.Address1 = "address line 1";
            property.ZIPCode = "zip code";
            property.State = "state";
            property.City = "City";
            property.PropertyLive = true;
            property.SetLongDescriptionValue("Long Description");
            property.SetPropertyNameValue("Property Name p2");
            property.SetShortDescriptionValue("Short Description");
            property.DictionaryCountry = _countryService.GetCountryById(1);
            property.PropertyCode = "p2";
            property.PropertyType = _propertyTypeService.GetPropertyTypeById(1);
            property.User = _user;
            property.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            property.TimeZone = "Eastern Standard Time";
            _propertyService.AddProperty(property);
            _properties.Add(property);

            //Unit type
            unitType = this.AddUnitTypeForProperty(property);
            _unitTypeService.AddUnitType(unitType);
            _unitTypes.Add(unitType);

            //Units
            unit = this.AddUnitForProperty(property, unitType, 2, 2, 2, "p2.u1");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Full image
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.FullScreen;
            psc.ContentValue = "/imageFullP2.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Thumbnails
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP2.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Property Amenities
            AddPropertyAmenities(property, 1);
            AddPropertyAmenities(property, 2);
            AddPropertyAmenities(property, 3);
            AddPropertyAmenities(property, 4);

            //Property Tags
            AddPropertyTag(property, 1);
            AddPropertyTag(property, 2);
            AddPropertyTag(property, 3);
            #endregion

            #region Property 3 Dest 2
            property = new Property();
            property.Destination = _destService.GetDestinationById(2);
            property.Address1 = "address line 1";
            property.ZIPCode = "zip code";
            property.State = "state";
            property.City = "City";
            property.PropertyLive = true;
            property.SetLongDescriptionValue("Long Description");
            property.SetPropertyNameValue("Property Name p3");
            property.SetShortDescriptionValue("Short Description");
            property.DictionaryCountry = _countryService.GetCountryById(1);
            property.PropertyCode = "p3";
            property.PropertyType = _propertyTypeService.GetPropertyTypeById(1);
            property.User = _user;
            property.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            property.TimeZone = "Eastern Standard Time";
            _propertyService.AddProperty(property);
            _properties.Add(property);

            //Unit type
            unitType = this.AddUnitTypeForProperty(property);
            _unitTypeService.AddUnitType(unitType);
            _unitTypes.Add(unitType);

            //Units
            unit = this.AddUnitForProperty(property, unitType, 7, 5, 1, "p3.u1");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Full image
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.FullScreen;
            psc.ContentValue = "/imageFullP3.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Thumbnails
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP32.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Property Amenities
            AddPropertyAmenities(property, 3);
            AddPropertyAmenities(property, 4);
            AddPropertyAmenities(property, 6);
            AddPropertyAmenities(property, 8);
            AddPropertyAmenities(property, 9);
            AddPropertyAmenities(property, 14);

            //Property Tags
            AddPropertyTag(property, 5);
            AddPropertyTag(property, 6);
            AddPropertyTag(property, 12);
            #endregion

            #region Property 4 Dest 2
            property = new Property();
            property.Destination = _destService.GetDestinationById(2);
            property.Address1 = "address line 1";
            property.ZIPCode = "zip code";
            property.State = "state";
            property.City = "City";
            property.PropertyLive = true;
            property.SetLongDescriptionValue("Long Description");
            property.SetPropertyNameValue("Property Name p4");
            property.SetShortDescriptionValue("Short Description");
            property.DictionaryCountry = _countryService.GetCountryById(1);
            property.PropertyCode = "p4";
            property.PropertyType = _propertyTypeService.GetPropertyTypeById(1);
            property.User = _user;
            property.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            property.TimeZone = "Eastern Standard Time";
            _propertyService.AddProperty(property);
            _properties.Add(property);

            //Unit type
            unitType = this.AddUnitTypeForProperty(property);
            _unitTypeService.AddUnitType(unitType);
            _unitTypes.Add(unitType);

            //Units
            unit = this.AddUnitForProperty(property, unitType, 7, 5, 1, "p4.u1");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            unit = this.AddUnitForProperty(property, unitType, 7, 4, 2, "p4.u2");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            unit = this.AddUnitForProperty(property, unitType, 3, 3, 1, "p4.u3");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            unit = this.AddUnitForProperty(property, unitType, 1, 3, 2, "p4.u4");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Full image
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.FullScreen;
            psc.ContentValue = "/imageFullP4.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Thumbnails
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP4.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP4-2.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Property Amenities;
            AddPropertyAmenities(property, 9);
            AddPropertyAmenities(property, 14);

            //Property Tags
            AddPropertyTag(property, 6);
            AddPropertyTag(property, 9);
            #endregion

            #region Property 5 Dest 5
            property = new Property();
            property.Destination = _destService.GetDestinationById(5);
            property.Address1 = "address line 1";
            property.ZIPCode = "zip code";
            property.State = "state";
            property.City = "City";
            property.PropertyLive = true;
            property.SetLongDescriptionValue("Long Description");
            property.SetPropertyNameValue("Property Name p5");
            property.SetShortDescriptionValue("Short Description");
            property.DictionaryCountry = _countryService.GetCountryById(1);
            property.PropertyCode = "p5";
            property.PropertyType = _propertyTypeService.GetPropertyTypeById(1);
            property.User = _user;
            property.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            property.TimeZone = "Eastern Standard Time";
            _propertyService.AddProperty(property);
            _properties.Add(property);

            //Unit type
            unitType = this.AddUnitTypeForProperty(property);
            _unitTypeService.AddUnitType(unitType);
            _unitTypes.Add(unitType);

            //Units
            unit = this.AddUnitForProperty(property, unitType, 6, 3, 1, "p5.u1");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Unit Inv blocking
            UnitInvBlocking unitBlock = new UnitInvBlocking();
            unitBlock.DateFrom = new DateTime(2012, 5, 2);
            unitBlock.DateUntil = new DateTime(2012, 5, 8);
            unitBlock.Type = 0;
            unitBlock.Unit = unit;
            _unitService.AddUnitBlockade(unitBlock);

            unit = this.AddUnitForProperty(property, unitType, 3, 4, 3, "p5.u2");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 45;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Full image
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.FullScreen;
            psc.ContentValue = "/imageFullP5.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Thumbnails
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP5.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Property Amenities;
            AddPropertyAmenities(property, 6);

            //Property Tags
            AddPropertyTag(property, 9);
            #endregion

            #region Property 6 Dest 6
            property = new Property();
            property.Destination = _destService.GetDestinationById(6);
            property.Address1 = "address line 1";
            property.ZIPCode = "zip code";
            property.State = "state";
            property.City = "City";
            property.PropertyLive = true;
            property.SetLongDescriptionValue("Long Description");
            property.SetPropertyNameValue("Property Name p6");
            property.SetShortDescriptionValue("Short Description");
            property.DictionaryCountry = _countryService.GetCountryById(1);
            property.PropertyCode = "p6";
            property.PropertyType = _propertyTypeService.GetPropertyTypeById(1);
            property.User = _user;
            property.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            property.TimeZone = "Eastern Standard Time";
            _propertyService.AddProperty(property);
            _properties.Add(property);

            //Unit type
            unitType = this.AddUnitTypeForProperty(property);
            _unitTypeService.AddUnitType(unitType);
            _unitTypes.Add(unitType);

            //Units
            unit = this.AddUnitForProperty(property, unitType, 2, 2, 2, "p6.u1");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Unit Inv blocking
            UnitInvBlocking unitBlock1 = new UnitInvBlocking();
            unitBlock1.DateFrom = new DateTime(2012, 5, 2);
            unitBlock1.DateUntil = new DateTime(2012, 5, 10);            
            unitBlock1.Type = 0;
            unitBlock1.Unit = unit;
            _unitService.AddUnitBlockade(unitBlock1);

            //Full image
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.FullScreen;
            psc.ContentValue = "/imageFullP6.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Thumbnails
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP6_2.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Property Amenities
            AddPropertyAmenities(property, 1);
            AddPropertyAmenities(property, 2);
            AddPropertyAmenities(property, 3);
            AddPropertyAmenities(property, 4);

            //Property Tags
            AddPropertyTag(property, 1);
            AddPropertyTag(property, 2);
            AddPropertyTag(property, 3);
            #endregion

            #region Property 7 Dest 6
            property = new Property();
            property.Destination = _destService.GetDestinationById(6);
            property.Address1 = "address line 1";
            property.ZIPCode = "zip code";
            property.State = "state";
            property.City = "City";
            property.PropertyLive = true;
            property.SetLongDescriptionValue("Long Description");
            property.SetPropertyNameValue("Property Name p7");
            property.SetShortDescriptionValue("Short Description");
            property.DictionaryCountry = _countryService.GetCountryById(1);
            property.PropertyCode = "p7";
            property.PropertyType = _propertyTypeService.GetPropertyTypeById(1);
            property.User = _user;
            property.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            property.TimeZone = "Eastern Standard Time";
            _propertyService.AddProperty(property);
            _properties.Add(property);

            //Unit type
            unitType = this.AddUnitTypeForProperty(property);
            _unitTypeService.AddUnitType(unitType);
            _unitTypes.Add(unitType);

            //Units
            unit = this.AddUnitForProperty(property, unitType, 2, 2, 2, "p7.u1");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Unit Inv blocking
            UnitInvBlocking unitBlock2 = new UnitInvBlocking();
            unitBlock2.DateFrom = new DateTime(2012, 5, 4);
            unitBlock2.DateUntil = new DateTime(2012, 5, 17);            
            unitBlock2.Type = 0;
            unitBlock2.Unit = unit;
            _unitService.AddUnitBlockade(unitBlock2);

            //Full image
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.FullScreen;
            psc.ContentValue = "/imageFullP7.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Thumbnails
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP7.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Property Amenities
            AddPropertyAmenities(property, 1);           
            AddPropertyAmenities(property, 3);
            AddPropertyAmenities(property, 4);

            //Property Tags
            AddPropertyTag(property, 1);
            AddPropertyTag(property, 2);
            AddPropertyTag(property, 3);
            #endregion

            #region Property 8 Dest 6 Property Type 2
            property = new Property();
            property.Destination = _destService.GetDestinationById(6);
            property.Address1 = "address line 1";
            property.ZIPCode = "zip code";
            property.State = "state";
            property.City = "City";
            property.PropertyLive = true;
            property.SetLongDescriptionValue("Long Description");
            property.SetPropertyNameValue("Property Name p8");
            property.SetShortDescriptionValue("Short Description");
            property.DictionaryCountry = _countryService.GetCountryById(1);
            property.PropertyCode = "p8";
            property.PropertyType = _propertyTypeService.GetPropertyTypeById(2);
            property.User = _user;
            property.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            property.TimeZone = "Eastern Standard Time";
            _propertyService.AddProperty(property);
            _properties.Add(property);

            //Unit type
            unitType = this.AddUnitTypeForProperty(property);
            _unitTypeService.AddUnitType(unitType);
            _unitTypes.Add(unitType);

            //Units
            unit = this.AddUnitForProperty(property, unitType, 4, 2, 5, "p8.u1");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Unit Inv blocking
            unitBlock2 = new UnitInvBlocking();
            unitBlock2.DateFrom = new DateTime(2012, 4, 1);
            unitBlock2.DateUntil = new DateTime(2012, 5, 30);
            unitBlock2.Type = 0;
            unitBlock2.Unit = unit;
            _unitService.AddUnitBlockade(unitBlock2);

            unitBlock2 = new UnitInvBlocking();
            unitBlock2.DateFrom = new DateTime(2012, 6, 1);
            unitBlock2.DateUntil = new DateTime(2012, 6, 25);
            unitBlock2.Type = 0;
            unitBlock2.Unit = unit;
            _unitService.AddUnitBlockade(unitBlock2);

            //Full image
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.FullScreen;
            psc.ContentValue = "/imageFullP8.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Thumbnails
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP8.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Property Amenities
            AddPropertyAmenities(property, 6);
            AddPropertyAmenities(property, 7);
            AddPropertyAmenities(property, 8);
            AddPropertyAmenities(property, 9);
            AddPropertyAmenities(property, 10);
            AddPropertyAmenities(property, 17);
            AddPropertyAmenities(property, 19);

            //Property Tags
            AddPropertyTag(property, 5);
            AddPropertyTag(property, 7);
            AddPropertyTag(property, 17);
            AddPropertyTag(property, 19);
            AddPropertyTag(property, 25);
            #endregion

            #region Property 9 Dest 7
            property = new Property();
            property.Destination = _destService.GetDestinationById(7);
            property.Address1 = "address line 1";
            property.ZIPCode = "zip code";
            property.State = "state";
            property.City = "City";
            property.PropertyLive = true;
            property.SetLongDescriptionValue("Long Description");
            property.SetPropertyNameValue("Property Name p9");
            property.SetShortDescriptionValue("Short Description");            
            property.DictionaryCountry = _countryService.GetCountryById(1);
            property.PropertyCode = "p9";
            property.PropertyType = _propertyTypeService.GetPropertyTypeById(1);
            property.User = _user;
            property.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            property.TimeZone = "Eastern Standard Time";
            _propertyService.AddProperty(property);
            _properties.Add(property);

            //Unit Types
            unitType = this.AddUnitTypeForProperty(property);
            _unitTypeService.AddUnitType(unitType);
            _unitTypes.Add(unitType);

            //Units
            unit = this.AddUnitForProperty(property, unitType, 1, 2, 3, "p9.u1");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            unit = this.AddUnitForProperty(property, unitType, 6, 4, 3, "p9.u2");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Full image
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.FullScreen;
            psc.ContentValue = "/imageFullP9.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Thumbnails
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP9.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Property Amenities
            AddPropertyAmenities(property, 1);
            AddPropertyAmenities(property, 2);
            AddPropertyAmenities(property, 3);
            AddPropertyAmenities(property, 4);

            //Property Tags
            AddPropertyTag(property, 1);
            AddPropertyTag(property, 2);
            AddPropertyTag(property, 3);

            #endregion

            #region Property 10 Dest 7
            property = new Property();
            property.Destination = _destService.GetDestinationById(7);
            property.Address1 = "address line 1";
            property.ZIPCode = "zip code";
            property.State = "state";
            property.City = "City";
            property.PropertyLive = true;
            property.SetLongDescriptionValue("Long Description");
            property.SetPropertyNameValue("Property Name p10");
            property.SetShortDescriptionValue("Short Description");
            property.DictionaryCountry = _countryService.GetCountryById(1);
            property.PropertyCode = "p10";
            property.PropertyType = _propertyTypeService.GetPropertyTypeById(1);
            property.User = _user;
            property.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            property.TimeZone = "Eastern Standard Time";
            _propertyService.AddProperty(property);
            _properties.Add(property);

            //Unit type
            unitType = this.AddUnitTypeForProperty(property);
            _unitTypeService.AddUnitType(unitType);
            _unitTypes.Add(unitType);

            //Units
            unit = this.AddUnitForProperty(property, unitType, 3, 5, 2, "p10.u1");
            _unitService.AddUnit(unit);
            _units.Add(unit);

            ur = new UnitRate();
            ur.DailyRate = 2145;
            ur.DateFrom = DateTime.Today.Date.AddYears(-4);
            ur.DateUntil = DateTime.Today.Date.AddYears(2).Date;
            ur.Unit = unit;
            _unitService.AddUnitRate(ur);

            //Full image
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.FullScreen;
            psc.ContentValue = "/imageFullP10.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Thumbnails
            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP10.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP10.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            psc = new PropertyStaticContent();
            psc.ContentCode = (int)PropertyStaticContentType.ListView;
            psc.ContentValue = "/imageThumbP10.png";
            psc.Property = property;
            _propertyService.AddPropertyStaticContent(psc);

            //Property Amenities
            AddPropertyAmenities(property, 1);
            AddPropertyAmenities(property, 2);
            AddPropertyAmenities(property, 3);
            AddPropertyAmenities(property, 4);

            //Property Tags
            AddPropertyTag(property, 1);
            AddPropertyTag(property, 2);
            AddPropertyTag(property, 3);
            #endregion
        }
        
        /// <summary>
        /// Creates sample user object
        /// </summary>
        /// <returns>The User</returns>
        private User AddUser()
        {
            User user = new User();
            user.Firstname = "Search User First Name";
            user.Lastname = "Search User Last Name";
            user.email = "Search User Email Address";
            user.Password = "Search User Password";
            user.IsGeneratedPassword = false;
            user.DictionaryCulture = _dictionaryCultureService.GetCultures().First();
            return user;
        }
        
        /// <summary>
        /// Adds unit type for property object
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private UnitType AddUnitTypeForProperty(Property p)
        {
            UnitType unitType = new UnitType();
            unitType.SetDescValue("Description");
            unitType.SetTitleValue("Title");
            unitType.UnitTypeCode = "Code";
            unitType.Property = p;
            return unitType;
        }

        /// <summary>
        /// Adds unit for property
        /// </summary>
        /// <param name="p">Property object</param>
        /// <param name="ut">Unit Type</param>
        /// <param name="guests">Number of guests</param>
        /// <param name="baths">Number of bathrooms</param>
        /// <param name="bedrooms">Number of bedrooms</param>
        /// <param name="code">Unit code</param>
        /// <returns></returns>
        private Unit AddUnitForProperty(Property p, UnitType ut, int guests, int baths, int bedrooms, string code)
        {
            Unit unit = new Unit();
            unit.UnitCode = code;
            unit.SetDescValue("Description");
            unit.SetTitleValue("Title");
            unit.MaxNumberOfBathrooms = baths;
            unit.MaxNumberOfGuests = guests;
            unit.MaxNumberOfBedRooms= bedrooms;
            unit.UnitType = ut;
            unit.Property = p;
            return unit;
        }

        /// <summary>
        /// Adds amienity for property
        /// </summary>
        /// <param name="p">Property</param>
        /// <param name="amentiyId">Amenity ID</param>
        private void AddPropertyAmenities(Property p, int amentiyId)
        {
            PropertyAmenity pa = new PropertyAmenity();
            pa.Amenity = _amenityService.GetAmenityById(amentiyId);
            pa.SetDetailsValue("amenity details");
            pa.Property = p;
            _propertyService.AddPropertyAmenity(pa);
        }

        /// <summary>
        /// Adds property tag for property
        /// </summary>
        /// <param name="p">Property</param>
        /// <param name="tagId">Tag ID</param>
        private void AddPropertyTag(Property p, int tagId)
        {
            PropertyTag pt = new PropertyTag();
            pt.Tag = _tagService.GetTagById(tagId);
            pt.Property = p;
            _propertyService.AddPropertyTag(pt);
        }

        /// <summary>
        /// Save original OTP Setting values
        /// </summary>
        private void SaveOTPSettings()
        {
            //change search parameters
            _originSettings = new Dictionary<string, string>();
            foreach (var setting in _settingsService.GetSettings())
            {
                _originSettings.Add(setting.OTP_SettingID.ToString(), setting.SettingValue);
            }
        }

        #endregion
    }
}
