﻿using System;
using BusinessLogic.Objects.Templates.Document;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccess;
using DataAccess.Enums;
using DataAccess.CustomObjects;
using BusinessLogic.PaymentGateway;
using BusinessLogic.Objects;

namespace UnitTests
{
    public static class ExtendedAssert
    {
        public static void Throws<T>(Action action) where T : Exception
        {
            bool exceptionCaught = false;

            try
            {
                action.Invoke();
            }
            catch (T)
            {
                exceptionCaught = true;
            }

            if (!exceptionCaught)
                throw new AssertFailedException(String.Format(Messages.ExpectedException, typeof(T)));
        }

        public static void ThrowsdbUpdateException(Action action, string foreignKey)
        {
            string pattern = ".*participate in the '([^']*)' relationship.*";

            try
            {
                action.Invoke();
            }
            catch (Exception ex)
            {
                if (ex.InnerException is DbUpdateException || ex is DbUpdateException)
                {
                    DbUpdateException dbex = (DbUpdateException)(ex.InnerException is DbUpdateException ? ex.InnerException : ex);
                    Match match = Regex.Match(dbex.Message, pattern);

                    if (!match.Success)
                        throw new AssertFailedException("An exception of proper type was caught, but couldn't find matches for regular expression.", dbex);
                    else
                    {
                        if (match.Groups[1].Value != foreignKey)
                            throw new AssertFailedException("An exception of proper type was caught, but couldn't match the FK. Foreign key: " + foreignKey + ", actual foreign key: " + match.Groups[1].Value, dbex);
                    }
                }
                else
                    throw new AssertFailedException(String.Format(Messages.WrongException, typeof(DbUpdateException), ex.InnerException != null ? ex.InnerException.GetType() : ex.GetType()), ex);
            }
        }

        public static void ThrowsdbEntityValidationExcepion(Action action, string field)
        {
            bool exceptionCaught = false;

            try
            {
                action.Invoke();
            }
            catch (Exception ex)
            {
                if (ex.InnerException is DbEntityValidationException || ex is DbEntityValidationException)
                {
                    DbEntityValidationException dbex = (DbEntityValidationException)(ex.InnerException is DbEntityValidationException ? ex.InnerException : ex);
                    exceptionCaught = true;

                    if (dbex.EntityValidationErrors.Count() != 1)
                        throw new AssertFailedException("An exception of proper type was caught, but contained more than one entity validation error. Field: " + field, dbex);
                    else if (dbex.EntityValidationErrors.First().ValidationErrors.Count != 1)
                        throw new AssertFailedException("An exception of proper type was caught, but contained more than one validation error for the entity. Field: " + field, dbex);
                    else if (dbex.EntityValidationErrors.First().ValidationErrors.First().PropertyName != field)
                        throw new AssertFailedException("An exception of proper type was caught, but was not related to chosen field. Chosen Field: " + field + ", actual field: " + dbex.EntityValidationErrors.First().ValidationErrors.First().PropertyName, dbex);
                }
                else
                    throw new AssertFailedException(String.Format(Messages.WrongException, typeof(DbEntityValidationException), ex.InnerException != null ? ex.InnerException.GetType() : ex.GetType()), ex);
            }

            if (!exceptionCaught)
                throw new AssertFailedException(String.Format(Messages.ExpectedExceptionForField, typeof(DbEntityValidationException), field));
        }

        public static void AreEqual(AmenityGroup expected, AmenityGroup actual)
        {
            Assert.AreEqual(expected.AmenityGroupId, actual.AmenityGroupId);

            Assert.AreEqual(expected.AmenityIcon, actual.AmenityIcon);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Name_i18n, actual.Name_i18n);
            Assert.AreEqual(expected.NameCurrentLanguage, actual.NameCurrentLanguage);

            Assert.AreEqual(expected.Amenities, actual.Amenities);
        }

        public static void AreEqual(Amenity expected, Amenity actual)
        {
            Assert.AreEqual(expected.AmenityID, actual.AmenityID);

            Assert.AreEqual(expected.AmenityCode, actual.AmenityCode);
            Assert.AreEqual(expected.Countable, actual.Countable);

            Assert.AreEqual(expected.Title, actual.Title);
            Assert.AreEqual(expected.TitleCurrentLanguage, actual.TitleCurrentLanguage);
            Assert.AreEqual(expected.AmenityTitle_i18n, actual.AmenityTitle_i18n);

            Assert.AreEqual(expected.Description, actual.Description);
            Assert.AreEqual(expected.DescriptionCurrentLanguage, actual.DescriptionCurrentLanguage);
            Assert.AreEqual(expected.Description_i18n, actual.Description_i18n);

            AreEqual(expected.AmenityGroup, actual.AmenityGroup);

            Assert.AreEqual(expected.PropertyAmenities, actual.PropertyAmenities);
        }

        public static void AreEqual(PropertyStaticContent expected, PropertyStaticContent actual)
        {
            Assert.AreEqual(expected.PropertyStaticContentId, actual.PropertyStaticContentId);

            Assert.AreEqual(expected.ContentCode, actual.ContentCode);
            Assert.AreEqual(expected.ContentValue, actual.ContentValue);

            Assert.AreEqual(expected.ContentValueInternational, actual.ContentValueInternational);
            Assert.AreEqual(expected.ContentValueCurrentLanguage, actual.ContentValueCurrentLanguage);
            Assert.AreEqual(expected.ContentValue_i18n, actual.ContentValue_i18n);

            AreEqual(expected.Property, actual.Property);
        }

        public static void AreEqual(UnitStaticContent expected, UnitStaticContent actual)
        {
            Assert.AreEqual(expected.UnitStaticContentId, actual.UnitStaticContentId);

            Assert.AreEqual(expected.ContentCode, actual.ContentCode);
            Assert.AreEqual(expected.ContentValue, actual.ContentValue);

            Assert.AreEqual(expected.ContentValueInternational, actual.ContentValueInternational);
            Assert.AreEqual(expected.ContentValueCurrentLanguage, actual.ContentValueCurrentLanguage);
            Assert.AreEqual(expected.ContentValue_i18n, actual.ContentValue_i18n);

            AreEqual(expected.Unit, actual.Unit);
        }

        public static void AreEqual(PropertyAmenity expected, PropertyAmenity actual)
        {
            Assert.AreEqual(expected.PropertyAmenityID, actual.PropertyAmenityID);

            Assert.AreEqual(expected.Quantity, actual.Quantity);

            Assert.AreEqual(expected.Details, actual.Details);
            Assert.AreEqual(expected.DetailsCurrentLanguage, actual.DetailsCurrentLanguage);
            Assert.AreEqual(expected.Details_i18n, actual.Details_i18n);

            AreEqual(expected.Property, actual.Property);
            AreEqual(expected.Amenity, actual.Amenity);
        }

        public static void AreEqual(TagGroup expected, TagGroup actual)
        {
            Assert.AreEqual(expected.TagGroupId, actual.TagGroupId);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.NameCurrentLanguage, actual.NameCurrentLanguage);
            Assert.AreEqual(expected.Name_i18n, actual.Name_i18n);

            Assert.AreEqual(expected.Tags, actual.Tags);
        }

        public static void AreEqual(Tag expected, Tag actual)
        {
            Assert.AreEqual(expected.TagID, actual.TagID);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.NameCurrentLanguage, actual.NameCurrentLanguage);
            Assert.AreEqual(expected.Tag_i18n, actual.Tag_i18n);

            AreEqual(expected.TagGroup, actual.TagGroup);

            Assert.AreEqual(expected.PropertyTags, actual.PropertyTags);
        }

        public static void AreEqual(PropertyTag expected, PropertyTag actual)
        {
            Assert.AreEqual(expected.PropertyTagID, actual.PropertyTagID);

            AreEqual(expected.Property, actual.Property);
            AreEqual(expected.Tag, actual.Tag);
        }

        public static void AreEqual(User expected, User actual)
        {
            Assert.AreEqual(expected.UserID, actual.UserID);

            Assert.AreEqual(expected.Lastname, actual.Lastname);
            Assert.AreEqual(expected.Firstname, actual.Firstname);
            Assert.AreEqual(expected.Address1, actual.Address1);
            Assert.AreEqual(expected.Address2, actual.Address2);
            Assert.AreEqual(expected.State, actual.State);
            Assert.AreEqual(expected.ZIPCode, actual.ZIPCode);
            Assert.AreEqual(expected.City, actual.City);
            Assert.AreEqual(expected.CellPhone, actual.CellPhone);
            Assert.AreEqual(expected.LandLine, actual.LandLine);
            Assert.AreEqual(expected.email, actual.email);
            Assert.AreEqual(expected.DriverLicenseNbr, actual.DriverLicenseNbr);
            Assert.AreEqual(expected.PassportNbr, actual.PassportNbr);
            Assert.AreEqual(expected.SocialsecurityNbr, actual.SocialsecurityNbr);
            Assert.AreEqual(expected.DirectDepositInfo, actual.DirectDepositInfo);
            Assert.AreEqual(expected.SecurityQuestion, actual.SecurityQuestion);
            Assert.AreEqual(expected.SecurityAnswer, actual.SecurityAnswer);
            Assert.AreEqual(expected.AcceptedTCs, actual.AcceptedTCs);
            Assert.AreEqual(expected.AcceptedTCsInitials, actual.AcceptedTCsInitials);
            Assert.AreEqual(expected.Password, actual.Password);
            Assert.AreEqual(expected.IsGeneratedPassword, actual.IsGeneratedPassword);
            Assert.AreEqual(expected.VerificationPositive, actual.VerificationPositive);
            Assert.AreEqual(expected.SendMePromotions, actual.SendMePromotions);
            Assert.AreEqual(expected.SendInfoFavoritePlaces, actual.SendInfoFavoritePlaces);
            Assert.AreEqual(expected.SendNews, actual.SendNews);
            Assert.AreEqual(expected.Gender, actual.Gender);

            if (expected.DictionaryCountry != null && actual.DictionaryCountry != null)
                AreEqual(expected.DictionaryCountry, actual.DictionaryCountry);
            else if (expected.DictionaryCountry != null || actual.DictionaryCountry != null)
                Assert.Fail();

            Assert.AreEqual(expected.GuestPaymentMethods, actual.GuestPaymentMethods);
            Assert.AreEqual(expected.GuestReviews, actual.GuestReviews);
            Assert.AreEqual(expected.IdentityVerificationDetails, actual.IdentityVerificationDetails);
            Assert.AreEqual(expected.LoggedInRole, actual.LoggedInRole);
            Assert.AreEqual(expected.Properties, actual.Properties);
            Assert.AreEqual(expected.ReservationBillingAddresses, actual.ReservationBillingAddresses);
            Assert.AreEqual(expected.Reservations, actual.Reservations);
            Assert.AreEqual(expected.ScheduledVisits, actual.ScheduledVisits);
            Assert.AreEqual(expected.UserRoles, actual.UserRoles);
        }

        public static void AreEqual(PropertyType expected, PropertyType actual)
        {
            Assert.AreEqual(expected.PropertyTypeId, actual.PropertyTypeId);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.NameCurrentLanguage, actual.NameCurrentLanguage);
            Assert.AreEqual(expected.Name_i18n, actual.Name_i18n);

            Assert.AreEqual(expected.Properties, actual.Properties);
        }

        public static void AreEqual(UserRole expected, UserRole actual)
        {
            Assert.AreEqual(expected.UserRoleId, actual.UserRoleId);

            Assert.AreEqual(expected.ActivationToken, actual.ActivationToken);
            Assert.AreEqual(expected.Status, actual.Status);

            AreEqual(expected.User, actual.User);
            AreEqual(expected.Role, actual.Role);
        }

        public static void AreEqual(Role expected, Role actual)
        {
            Assert.AreEqual(expected.RoleId, actual.RoleId);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.RoleLevel, actual.RoleLevel);

            Assert.AreEqual(expected.RoleIntranetSiteMapActions, actual.RoleIntranetSiteMapActions);
            Assert.AreEqual(expected.UserRoles, actual.UserRoles);
        }

        public static void AreEqual(CrmOperationLog expected, CrmOperationLog actual)
        {
            Assert.AreEqual(expected.CrmRequest, actual.CrmRequest);
            Assert.AreEqual(expected.CrmResponse, actual.CrmResponse);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Message, actual.Message);
            Assert.AreEqual(expected.OperationType, actual.OperationType);
            Assert.AreEqual(expected.RowDate, actual.RowDate);

            if (expected.User != null && actual.User != null)
            {
                AreEqual(expected.User, actual.User);
            }
        }

        public static void AreEqual(DictionaryCountry expected, DictionaryCountry actual)
        {
            Assert.AreEqual(expected.CountryId, actual.CountryId);

            Assert.AreEqual(expected.CountryCode2Letters, actual.CountryCode2Letters);
            Assert.AreEqual(expected.CountryCode3Letters, actual.CountryCode3Letters);

            Assert.AreEqual(expected.CountryName, actual.CountryName);
            Assert.AreEqual(expected.CountryNameCurrentLanguage, actual.CountryNameCurrentLanguage);
            Assert.AreEqual(expected.CountryName_i18n, actual.CountryName_i18n);

            Assert.AreEqual(expected.Properties, actual.Properties);
            Assert.AreEqual(expected.Users, actual.Users);
            Assert.AreEqual(expected.ReservationBillingAddresses, actual.ReservationBillingAddresses);
        }

        public static void AreEqual(Destination expected, Destination actual)
        {
            Assert.AreEqual(expected.DestinationID, actual.DestinationID);

            Assert.AreEqual(expected.DestinationName, actual.DestinationName);
            Assert.AreEqual(expected.DestinationNameCurrentLanguage, actual.DestinationNameCurrentLanguage);
            Assert.AreEqual(expected.Destination_i18n, actual.Destination_i18n);

            Assert.AreEqual(expected.DestinationDescription, actual.DestinationDescription);
            Assert.AreEqual(expected.DestinationDescriptionCurrentLanguage, actual.DestinationDescriptionCurrentLanguage);
            Assert.AreEqual(expected.Description_i18n, actual.Description_i18n);

            Assert.AreEqual(expected.Properties, actual.Properties);
            Assert.AreEqual(expected.ServiceProvidersDestinations, actual.ServiceProvidersDestinations);
            Assert.AreEqual(expected.Taxes, actual.Taxes);
        }

        public static void AreEqual(Property expected, Property actual)
        {
            Assert.AreEqual(expected.PropertyID, actual.PropertyID);

            Assert.AreEqual(expected.Address1, actual.Address1);
            Assert.AreEqual(expected.Address2, actual.Address2);
            Assert.AreEqual(expected.State, actual.State);
            Assert.AreEqual(expected.ZIPCode, actual.ZIPCode);
            Assert.AreEqual(expected.City, actual.City);
            Assert.AreEqual(expected.PropertyCode, actual.PropertyCode);
            Assert.AreEqual(expected.Latitude, actual.Latitude);
            Assert.AreEqual(expected.Longitude, actual.Longitude);
            Assert.AreEqual(expected.Altitude, actual.Altitude);
            Assert.AreEqual(expected.PropertyLive, actual.PropertyLive);
            Assert.AreEqual(expected.StandardCommission, actual.StandardCommission);
            Assert.AreEqual(expected.CheckIn, actual.CheckIn);
            Assert.AreEqual(expected.CheckOut, actual.CheckOut);

            Assert.AreEqual(expected.PropertyName, actual.PropertyName);
            Assert.AreEqual(expected.PropertyNameCurrentLanguage, actual.PropertyNameCurrentLanguage);
            Assert.AreEqual(expected.PropertyName_i18n, actual.PropertyName_i18n);

            Assert.AreEqual(expected.Short_Description, actual.Short_Description);
            Assert.AreEqual(expected.Short_DescriptionCurrentLanguage, actual.Short_DescriptionCurrentLanguage);
            Assert.AreEqual(expected.Short_Description_i18n, actual.Short_Description_i18n);

            Assert.AreEqual(expected.Long_Description, actual.Long_Description);
            Assert.AreEqual(expected.Long_DescriptionCurrentLanguage, actual.Long_DescriptionCurrentLanguage);
            Assert.AreEqual(expected.Long_Description_i18n, actual.Long_Description_i18n);

            AreEqual(expected.User, actual.User);
            AreEqual(expected.Destination, actual.Destination);
            AreEqual(expected.DictionaryCountry, actual.DictionaryCountry);
            AreEqual(expected.PropertyType, actual.PropertyType);

            Assert.AreEqual(expected.ContentUpdateRequests, actual.ContentUpdateRequests);
            Assert.AreEqual(expected.GuestReviews, actual.GuestReviews);
            Assert.AreEqual(expected.PropertyAddOns, actual.PropertyAddOns);
            Assert.AreEqual(expected.PropertyCommissionOverrides, actual.PropertyCommissionOverrides);
            Assert.AreEqual(expected.PropertyServiceProviders, actual.PropertyServiceProviders);
            Assert.AreEqual(expected.PropertySettings, actual.PropertySettings);
            Assert.AreEqual(expected.PropertyStaticContents, actual.PropertyStaticContents);
            Assert.AreEqual(expected.PropertySupportedLanguages, actual.PropertySupportedLanguages);
            Assert.AreEqual(expected.PropertyTags, actual.PropertyTags);
            Assert.AreEqual(expected.Reservations, actual.Reservations);
            Assert.AreEqual(expected.Units, actual.Units);
            Assert.AreEqual(expected.UnitTypes, actual.UnitTypes);
            Assert.AreEqual(expected.PropertyRawDatas, actual.PropertyRawDatas);
        }

        public static void AreEqual(SalesPerson expected, SalesPerson actual)
        {
            Assert.AreEqual(expected.SalesPersonId, actual.SalesPersonId);

            Assert.AreEqual(expected.FirstName, actual.FirstName);
            Assert.AreEqual(expected.LastName, actual.LastName);

            Assert.AreEqual(expected.CommisionDate, actual.CommisionDate);
            Assert.AreEqual(expected.Percentage, actual.Percentage);
        }

        public static void AreEqual(UnitType expected, UnitType actual)
        {
            Assert.AreEqual(expected.UnitTypeID, actual.UnitTypeID);

            Assert.AreEqual(expected.UnitTypeCode, actual.UnitTypeCode);

            Assert.AreEqual(expected.UnitTypeTitle, actual.UnitTypeTitle);
            Assert.AreEqual(expected.UnitTypeTitleCurrentLanguage, actual.UnitTypeTitleCurrentLanguage);
            Assert.AreEqual(expected.UnitTypeTitle_i18n, actual.UnitTypeTitle_i18n);

            Assert.AreEqual(expected.UnitTypeDesc, actual.UnitTypeDesc);
            Assert.AreEqual(expected.UnitTypeDescCurrentLanguage, actual.UnitTypeDescCurrentLanguage);
            Assert.AreEqual(expected.UnitTypeDesc_i18n, actual.UnitTypeDesc_i18n);

            AreEqual(expected.Property, actual.Property);

            Assert.AreEqual(expected.Reservations, actual.Reservations);
            Assert.AreEqual(expected.Units, actual.Units);
            Assert.AreEqual(expected.UnitType7dayDiscounts, actual.UnitType7dayDiscounts);
            Assert.AreEqual(expected.UnitTypeCTAs, actual.UnitTypeCTAs);
            Assert.AreEqual(expected.UnitTypeMLOS, actual.UnitTypeMLOS);
        }

        public static void AreEqual(Unit expected, Unit actual)
        {
            Assert.AreEqual(expected.UnitID, actual.UnitID);

            Assert.AreEqual(expected.UnitCode, actual.UnitCode);
            Assert.AreEqual(expected.CleaningStatus, actual.CleaningStatus);
            Assert.AreEqual(expected.MaxNumberOfBathrooms, actual.MaxNumberOfBathrooms);
            Assert.AreEqual(expected.MaxNumberOfBedRooms, actual.MaxNumberOfBedRooms);
            Assert.AreEqual(expected.MaxNumberOfGuests, actual.MaxNumberOfGuests);

            Assert.AreEqual(expected.UnitTitle, actual.UnitTitle);
            Assert.AreEqual(expected.UnitTitleCurrentLanguage, actual.UnitTitleCurrentLanguage);
            Assert.AreEqual(expected.UnitTitle_i18n, actual.UnitTitle_i18n);

            Assert.AreEqual(expected.UnitDesc, actual.UnitDesc);
            Assert.AreEqual(expected.UnitDescCurrentLanguage, actual.UnitDescCurrentLanguage);
            Assert.AreEqual(expected.UnitDesc_i18n, actual.UnitDesc_i18n);

            AreEqual(expected.UnitType, actual.UnitType);
            AreEqual(expected.Property, actual.Property);

            Assert.AreEqual(expected.Reservations, actual.Reservations);
            Assert.AreEqual(expected.UnitAppliancies, actual.UnitAppliancies);
            Assert.AreEqual(expected.UnitInvBlockings, actual.UnitInvBlockings);
            Assert.AreEqual(expected.UnitRates, actual.UnitRates);
            Assert.AreEqual(expected.UnitStaticContents, actual.UnitStaticContents);
        }

        public static void AreEqual(Appliance expected, Appliance actual)
        {
            Assert.AreEqual(expected.ApplianceId, actual.ApplianceId);

            Assert.AreEqual(expected.Type, actual.Type);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.NameCurrentLanguage, actual.NameCurrentLanguage);
            Assert.AreEqual(expected.Name_i18n, actual.Name_i18n);

            AreEqual(expected.ApplianceGroup, actual.ApplianceGroup);

            Assert.AreEqual(expected.UnitAppliancies, actual.UnitAppliancies);
        }

        public static void AreEqual(UnitAppliancy expected, UnitAppliancy actual)
        {
            Assert.AreEqual(expected.UnitApplianceId, actual.UnitApplianceId);

            AreEqual(expected.Unit, actual.Unit);
            AreEqual(expected.Appliance, actual.Appliance);
        }

        public static void AreEqual(UnitInvBlocking expected, UnitInvBlocking actual)
        {
            Assert.AreEqual(expected.UnitInvBlockingID, actual.UnitInvBlockingID);

            Assert.AreEqual(expected.DateUntil, actual.DateUntil);
            Assert.AreEqual(expected.DateFrom, actual.DateFrom);
            Assert.AreEqual(expected.Type, actual.Type);

            if (expected.Reservation != null && actual.Reservation != null)
                AreEqual(expected.Reservation, actual.Reservation);
            else if (expected.Reservation != null || actual.Reservation != null)
                Assert.Fail();

            AreEqual(expected.Unit, actual.Unit);
        }

        public static void AreEqual(Reservation expected, Reservation actual)
        {
            Assert.AreEqual(expected.ReservationID, actual.ReservationID);

            Assert.AreEqual(expected.BookingDate, actual.BookingDate);
            Assert.AreEqual(expected.BookingStatus, actual.BookingStatus);
            Assert.AreEqual(expected.ConfirmationID, actual.ConfirmationID);
            Assert.AreEqual(expected.DateArrival, actual.DateArrival);
            Assert.AreEqual(expected.DateDeparture, actual.DateDeparture);
            Assert.AreEqual(expected.TotalPrice, actual.TotalPrice);
            Assert.AreEqual(expected.TripBalance, actual.TripBalance);

            Assert.AreEqual(expected.DictionaryCulture, actual.DictionaryCulture);
            AreEqual(expected.Property, actual.Property);
            AreEqual(expected.Unit, actual.Unit);
            AreEqual(expected.UnitType, actual.UnitType);
            AreEqual(expected.User, actual.User);

            Assert.AreEqual(expected.GuestReviews, actual.GuestReviews);
            Assert.AreEqual(expected.ReservationBillingAddresses, actual.ReservationBillingAddresses);
            Assert.AreEqual(expected.ReservationDetails, actual.ReservationDetails);
            Assert.AreEqual(expected.ReservationTransactions, actual.ReservationTransactions);
            Assert.AreEqual(expected.UnitInvBlockings, actual.UnitInvBlockings);
        }

        public static void AreEqual(UnitRate expected, UnitRate actual)
        {
            Assert.AreEqual(expected.UnitRateID, actual.UnitRateID);

            Assert.AreEqual(expected.DateUntil, actual.DateUntil);
            Assert.AreEqual(expected.DateFrom, actual.DateFrom);
            Assert.AreEqual(expected.DailyRate, actual.DailyRate);

            AreEqual(expected.Unit, actual.Unit);
        }

        public static void AreEqual(ApplianceGroup expected, ApplianceGroup actual)
        {
            Assert.AreEqual(expected.ApplianceGroupId, actual.ApplianceGroupId);

            Assert.AreEqual(expected.GroupName, actual.GroupName);
            Assert.AreEqual(expected.GroupNameCurrentLanguage, actual.GroupNameCurrentLanguage);
            Assert.AreEqual(expected.Name_i18n, actual.Name_i18n);

            Assert.AreEqual(expected.Appliances, actual.Appliances);
        }

        public static void AreEqual(Template expected, Template actual)
        {
            Assert.AreEqual(expected.TemplateId, actual.TemplateId);

            Assert.AreEqual(expected.Type, actual.Type);
            Assert.AreEqual(expected.Title, actual.Title);
            Assert.AreEqual(expected.Content, actual.Content);

            AreEqual(expected.DictionaryCulture, actual.DictionaryCulture);
        }

        public static void AreEqual(DictionaryCulture expected, DictionaryCulture actual)
        {
            Assert.AreEqual(expected.CultureCode, actual.CultureCode);
            Assert.AreEqual(expected.CultureId, actual.CultureId);
            Assert.AreEqual(expected.CultureName, actual.CultureName);
            Assert.AreEqual(expected.CultureNameCurrentLanguage, actual.CultureNameCurrentLanguage);
        }

        public static void AreEqual(UnitAvailabilityResult expected, UnitAvailabilityResult actual)
        {
            Assert.AreEqual(expected.Date, actual.Date);
            Assert.AreEqual(expected.IsBlocked, actual.IsBlocked);
            Assert.AreEqual(expected.Mlos, actual.Mlos);
            Assert.AreEqual(expected.Price, actual.Price);
        }

        public static void AreEqual(UnitType7dayDiscounts expected, UnitType7dayDiscounts actual)
        {
            Assert.AreEqual(expected.UnitType7dayID, actual.UnitType7dayID);
            Assert.AreEqual(expected.DateFrom, actual.DateFrom);
            Assert.AreEqual(expected.DateUntil, actual.DateUntil);
            Assert.AreEqual(expected.Discount, actual.Discount);
            AreEqual(expected.UnitType, actual.UnitType);
        }

        public static void AreEqual(UnitTypeCTA expected, UnitTypeCTA actual)
        {
            Assert.AreEqual(expected.UnitTypeCTAID, actual.UnitTypeCTAID);
            Assert.AreEqual(expected.DateFrom, actual.DateFrom);
            Assert.AreEqual(expected.DateUntil, actual.DateUntil);
            AreEqual(expected.UnitType, actual.UnitType);
        }

        public static void AreEqual(UnitTypeMLO expected, UnitTypeMLO actual)
        {
            //Assert.AreEqual(expected.UnitTypeMLOSID, actual.UnitTypeMLOSID);
            Assert.AreEqual(expected.DateFrom, actual.DateFrom);
            Assert.AreEqual(expected.DateUntil, actual.DateUntil);
            Assert.AreEqual(expected.MLOS, actual.MLOS);
            AreEqual(expected.UnitType, actual.UnitType);
        }

        public static void AreEqual(EventLog expected, EventLog actual)
        {
            Assert.AreEqual(expected.Id, actual.Id);

            Assert.AreEqual(expected.IdString, actual.IdString);
            Assert.AreEqual(expected.CorrelationIdString, actual.CorrelationIdString);

            Assert.AreEqual(expected.CorrelationId, actual.CorrelationId);
            Assert.AreEqual(expected.Timestamp, actual.Timestamp);
            Assert.AreEqual(expected.Category, actual.Category);
            Assert.AreEqual(expected.Source, actual.Source);
            Assert.AreEqual(expected.Message, actual.Message);
            Assert.AreEqual(expected.RelatedObjectId, actual.RelatedObjectId);
        }

        public static void AreEqual(PaymentGatewayOperationsLog expected, PaymentGatewayOperationsLog actual)
        {
            Assert.AreEqual(expected.Id, actual.Id);

            Assert.AreEqual(expected.OperationType, actual.OperationType);
            Assert.AreEqual(expected.Message, actual.Message);
            Assert.AreEqual(expected.GatewayResponse, actual.GatewayResponse);
            Assert.AreEqual(expected.RowDate, actual.RowDate);

            AreEqual(expected.Reservation, actual.Reservation);
        }

        public static void AreEqual(ScheduledTask expected, ScheduledTask actual)
        {
            Assert.AreEqual(expected.ScheduledTaskId, actual.ScheduledTaskId);

            Assert.AreEqual(expected.ScheduledTaskType, actual.ScheduledTaskType);
            Assert.AreEqual(expected.ScheduleType, actual.ScheduleType);
            Assert.AreEqual(expected.ScheduleIntervalType, actual.ScheduleIntervalType);
            Assert.AreEqual(expected.TimeInterval, actual.TimeInterval);
            Assert.AreEqual(expected.ScheduleTimestamp, actual.ScheduleTimestamp);
            Assert.AreEqual(expected.LastExecutionTime, actual.LastExecutionTime);
            Assert.AreEqual(expected.NextPlannedExecutionTime, actual.NextPlannedExecutionTime);
            Assert.AreEqual(expected.TaskCompleted, actual.TaskCompleted);

            Assert.AreEqual(expected.TaskInstances, actual.TaskInstances);
        }

        public static void AreEqual(TaskInstance expected, TaskInstance actual)
        {
            Assert.AreEqual(expected.Id, actual.Id);

            Assert.AreEqual(expected.ExecutionTimestamp, actual.ExecutionTimestamp);
            Assert.AreEqual(expected.ExecutionResult, actual.ExecutionResult);

            AreEqual(expected.ScheduledTask, actual.ScheduledTask);

            Assert.AreEqual(expected.TaskInstanceDetails, actual.TaskInstanceDetails);
        }

        public static void AreEqual(TaskInstanceDetail expected, TaskInstanceDetail actual)
        {
            Assert.AreEqual(expected.Id, actual.Id);

            Assert.AreEqual(expected.Category, actual.Category);
            Assert.AreEqual(expected.Message, actual.Message);

            AreEqual(expected.TaskInstance, actual.TaskInstance);
        }

        public static void AreEqual(PropertyRawData expected, PropertyRawData actual)
        {
            Assert.AreEqual(expected.PropertyRawDataRecordID, actual.PropertyRawDataRecordID);

            Assert.AreEqual(expected.RecordName, actual.RecordName);
            Assert.AreEqual(expected.RawRecordContent, actual.RawRecordContent);
            Assert.AreEqual(expected.RawRecordType, actual.RawRecordType);
            Assert.AreEqual(expected.Description, actual.Description);

            AreEqual(expected.Property, actual.Property);
        }

        public static void AreEqual(ContentUpdateRequest expected, ContentUpdateRequest actual)
        {
            Assert.AreEqual(expected.ContentUpdateRequestId, actual.ContentUpdateRequestId);

            Assert.AreEqual(expected.RequestDateTime, actual.RequestDateTime);
            Assert.AreEqual(expected.Description, actual.Description);

            AreEqual(expected.Property, actual.Property);

            Assert.AreEqual(expected.ContentUpdateRequestDetails, actual.ContentUpdateRequestDetails);
        }

        public static void AreEqual(ContentUpdateRequestDetail expected, ContentUpdateRequestDetail actual)
        {
            Assert.AreEqual(expected.ContentUpdateRequestDetailId, actual.ContentUpdateRequestDetailId);

            Assert.AreEqual(expected.ContentCode, actual.ContentCode);
            Assert.AreEqual(expected.ContentValue, actual.ContentValue);
            Assert.AreEqual(expected.ContentValueXml, actual.ContentValueXml);

            AreEqual(expected.ContentUpdateRequest, actual.ContentUpdateRequest);
        }

        public static void AreEqual(Tax expected, Tax actual)
        {
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.CalculatedValue, actual.CalculatedValue);
            Assert.AreEqual(expected.Percentage, actual.Percentage);
            Assert.AreEqual(expected.Name_i18n, actual.Name_i18n);
            Assert.AreEqual(expected.NameCurrentLanguage, actual.NameCurrentLanguage);
        }

        public static void AreEqual(PropertyAddOn expected, PropertyAddOn actual)
        {
            Assert.AreEqual(expected.AddOnDesc, actual.AddOnDesc);
            Assert.AreEqual(expected.AddOnDescCurrentLanguage, actual.AddOnDescCurrentLanguage);
            Assert.AreEqual(expected.IsMandatory, actual.IsMandatory);
            Assert.AreEqual(expected.Price, actual.Price);
            Assert.AreEqual(expected.PropertyAddOnID, actual.PropertyAddOnID);
            Assert.AreEqual(expected.TotalPrice, actual.TotalPrice);
            Assert.AreEqual(expected.Unit, actual.Unit);
            Assert.AreEqual(expected.AddOnTitle, actual.AddOnTitle);
        }

        public static void AreEqual(BusinessLogic.PaymentGateway.AdditionalData expected, BusinessLogic.PaymentGateway.AdditionalData actual)
        {
            Assert.AreEqual(expected.Address, actual.Address);
            Assert.AreEqual(expected.City, actual.City);
            Assert.AreEqual(expected.CountryId, actual.CountryId);
            Assert.AreEqual(expected.CreditHolderId, actual.CreditHolderId);
            Assert.AreEqual(expected.FirstName, actual.FirstName);
            Assert.AreEqual(expected.Initials, actual.Initials);
            Assert.AreEqual(expected.LastName, actual.LastName);
            Assert.AreEqual(expected.State, actual.State);
            Assert.AreEqual(expected.ZipCode, actual.ZipCode);
        }

        public static void AreEqual(PaymentMethodResponse expected, PaymentMethodResponse actual)
        {
            Assert.AreEqual(expected.CardType, actual.CardType);
            Assert.AreEqual(expected.CreditCardNumber, actual.CreditCardNumber);
            Assert.AreEqual(expected.Last4Digits, actual.Last4Digits);
            Assert.AreEqual(expected.Token, actual.Token);
            AreEqual(expected.AdditionalData, actual.AdditionalData);
            
            for (int i = 0; i < expected.Errors.Length; i++)
            {
                Assert.AreEqual(expected.Errors[i], actual.Errors[i]);
            }
        }

        public static void AreEqual(ZipCodeRange expected, ZipCodeRange actual)
        {
            Assert.AreEqual(expected.ZipCodeRangeId, actual.ZipCodeRangeId);

            Assert.AreEqual(expected.ZipCodeStart, actual.ZipCodeStart);
            Assert.AreEqual(expected.ZipCodeEnd, actual.ZipCodeEnd);

            Assert.AreEqual(expected.ZipCodeRangeString, actual.ZipCodeRangeString);

            Assert.AreEqual(expected.ManagerZipCodeRanges, actual.ManagerZipCodeRanges);
        }

        public static void AreEqual(TransactionResponse expected, TransactionResponse actual)
        {
            Assert.AreEqual(expected.Amount, actual.Amount);
            Assert.AreEqual(expected.CurrencyCode, actual.CurrencyCode);
            Assert.AreEqual(expected.Message.MessageKey, actual.Message.MessageKey);
            Assert.AreEqual(expected.Message.Text, actual.Message.Text);
            Assert.AreEqual(expected.OnTest, actual.OnTest);
            Assert.AreEqual(expected.Response.ErrorCode, actual.Response.ErrorCode);
            Assert.AreEqual(expected.Response.Message, actual.Response.Message);
            Assert.AreEqual(expected.Response.Success, actual.Response.Success);
            Assert.AreEqual(expected.Response.PaymentMethod, actual.Response.PaymentMethod);
            Assert.AreEqual(expected.State, actual.State);
            Assert.AreEqual(expected.Succeded, actual.Succeded);
            Assert.AreEqual(expected.TransactionType, actual.TransactionType);
        }

        public static void AreEqual(ReservationForUser expected, ReservationForUser actual)
        {
            Assert.AreEqual(expected.BookingDate.ToLongDateString(), actual.BookingDate.ToLongDateString());
            Assert.AreEqual(expected.BookingStatus, actual.BookingStatus);
            Assert.AreEqual(expected.DateArrival.ToLongDateString(), actual.DateArrival.ToLongDateString());
            Assert.AreEqual(expected.DateDeparture.ToLongDateString(), actual.DateDeparture.ToLongDateString());
            Assert.AreEqual(expected.PropertyID, actual.PropertyID);
            Assert.AreEqual(expected.ReservationID, actual.ReservationID);
            for (int i = 0; i < expected.Thumbnails.Count; i++)
                Assert.AreEqual(expected.Thumbnails[i], actual.Thumbnails[i]);
            Assert.AreEqual(expected.TotalPrice, actual.TotalPrice);
            Assert.AreEqual(expected.TripBalance, actual.TripBalance);
            Assert.AreEqual(expected.ConfirmationID, actual.ConfirmationID);
            AreEqual(expected.Property, actual.Property);
        }

        public static void AreEqual(ReservationForHomeOwner expected, ReservationForHomeOwner actual)
        {
            Assert.AreEqual(expected.Booked.ToLongDateString(), actual.Booked.ToLongDateString());
            Assert.AreEqual(expected.BookingStatus, actual.BookingStatus);
            Assert.AreEqual(expected.CheckIn.ToLongDateString(), actual.CheckIn.ToLongDateString());
            Assert.AreEqual(expected.CheckOut.ToLongDateString(), actual.CheckOut.ToLongDateString());
            Assert.AreEqual(expected.ConfirmationId, actual.ConfirmationId);
            Assert.AreEqual(expected.CultureCode, actual.CultureCode);
            Assert.AreEqual(expected.GrossDue, actual.GrossDue);
            Assert.AreEqual(expected.OwnerNet, actual.OwnerNet);
            Assert.AreEqual(expected.PropertyId, actual.PropertyId);
            Assert.AreEqual(expected.PropertyName, actual.PropertyName);
            Assert.AreEqual(expected.TaxValue, actual.TaxValue);
            Assert.AreEqual(expected.TransactionFee, actual.TransactionFee);
            Assert.AreEqual(expected.UnitId, actual.UnitId);
            AreEqual(expected.User, actual.User);
        }

        public static void AreEqual(UserAgreement expected, UserAgreement actual)
        {
            Assert.AreEqual(expected.Amenities, actual.Amenities);
            Assert.AreEqual(expected.CheckInDate, actual.CheckInDate);
            Assert.AreEqual(expected.CheckOutDate, actual.CheckOutDate);
            Assert.AreEqual(expected.ConfirmationCode, actual.ConfirmationCode);
            Assert.AreEqual(expected.CreatedDate, actual.CreatedDate);
            Assert.AreEqual((int) expected.DocumentType, (int) actual.DocumentType);
            Assert.AreEqual(expected.GuestFullName, actual.GuestFullName);
            Assert.AreEqual(expected.GuestInitials, actual.GuestInitials);
            Assert.AreEqual(expected.GuestsNumbers, actual.GuestsNumbers);
            Assert.AreEqual(expected.OwnerFullName, actual.OwnerFullName);
            Assert.AreEqual(expected.OwnerInitials, actual.OwnerInitials);
            //Assert.AreEqual(expected.PaymentInfo, actual.PaymentInfo);
            Assert.AreEqual(expected.PropertyAddress, actual.PropertyAddress);
            Assert.AreEqual(expected.TermsOfServiceHref, actual.TermsOfServiceHref);
        }

        public static void AreEqual(Invoice expected, Invoice actual)
        {
            //Assert.AreEqual(expected.CancellationPolicy, actual.CancellationPolicy);
            Assert.AreEqual(expected.ConfirmationId, actual.ConfirmationId);
            Assert.AreEqual(expected.CreatedDate, actual.CreatedDate);
            Assert.AreEqual(expected.DocumentType, actual.DocumentType);
            Assert.AreEqual(expected.DueByDate, actual.DueByDate);
            Assert.AreEqual(expected.DuePercent, actual.DuePercent);
            Assert.AreEqual(expected.GuestAddress1, actual.GuestAddress1);
            Assert.AreEqual(expected.GuestAddress2, actual.GuestAddress2);
            Assert.AreEqual(expected.GuestFullName, actual.GuestFullName);
            Assert.AreEqual(expected.InvoiceId, actual.InvoiceId);
            Assert.AreEqual(expected.InvoicePositions, actual.InvoicePositions);
            Assert.AreEqual(expected.PropertyAddress, actual.PropertyAddress);
            Assert.AreEqual(expected.PropertyName, actual.PropertyName);
            Assert.AreEqual(expected.SalesAndTaxes, actual.SalesAndTaxes);
            Assert.AreEqual(expected.SecondPartDue, actual.SecondPartDue);
            //Assert.AreEqual(expected.SecurityDeposit, actual.SecurityDeposit);
            Assert.AreEqual(expected.SevenDayDiscount, actual.SevenDayDiscount);
            Assert.AreEqual(expected.Subtotal, actual.Subtotal);
            Assert.AreEqual(expected.Total, actual.Total);
        }

        public static void AreEqual(TripDetails expected, TripDetails actual)
        {
            Assert.AreEqual(expected.BookedDate, actual.BookedDate);
            //Assert.AreEqual(expected.CancellationPolicy, actual.CancellationPolicy);
            Assert.AreEqual(expected.CardinalityPeron, actual.CardinalityPeron);
            Assert.AreEqual(expected.CheckIn, actual.CheckIn);
            Assert.AreEqual(expected.CheckInTime, actual.CheckInTime);
            Assert.AreEqual(expected.CheckOut, actual.CheckOut);
            Assert.AreEqual(expected.CheckOutTime, actual.CheckOutTime);
            Assert.AreEqual(expected.ConfirmationCode, actual.ConfirmationCode);
            Assert.AreEqual(expected.DateBeforeArrival, actual.DateBeforeArrival);
            Assert.AreEqual(expected.Directions, actual.Directions);
            Assert.AreEqual(expected.DocumentType.ToString(), actual.DocumentType.ToString());
            Assert.AreEqual(expected.GuestEmail, actual.GuestEmail);
            Assert.AreEqual(expected.GuestFullName, actual.GuestFullName);
            Assert.AreEqual(expected.GuestsNumber, actual.GuestsNumber);
            Assert.AreEqual(expected.Latitude, actual.Latitude);
            Assert.AreEqual(expected.Longitude, actual.Longitude);
            Assert.AreEqual(expected.MaxNoOfGuests, actual.MaxNoOfGuests);
            Assert.AreEqual(expected.NightsNumber, actual.NightsNumber);
            Assert.AreEqual(expected.OnStreetParkingAvailable, actual.OnStreetParkingAvailable);
            Assert.AreEqual(expected.PropertyAddress, actual.PropertyAddress);
            Assert.AreEqual(expected.PropertyCountry, actual.PropertyCountry);
            Assert.AreEqual(expected.PropertyName, actual.PropertyName);
        }

        public static void AreEqual(DataAccess.CreditCardType expected, DataAccess.CreditCardType actual)
        {
            Assert.AreEqual(expected.CreditCardIcon, actual.CreditCardIcon);
            Assert.AreEqual(expected.CreditCardTypeId, actual.CreditCardTypeId);
            Assert.AreEqual(expected.Name, actual.Name);
        }

        public static void AreEqual(GuestPaymentMethod expected, GuestPaymentMethod actual)
        {
            Assert.AreEqual(expected.CreditCardLast4Digits, actual.CreditCardLast4Digits);
            Assert.AreEqual(expected.GuestPaymentMethodID, actual.GuestPaymentMethodID);
            Assert.AreEqual(expected.ReferenceToken, actual.ReferenceToken);
            AreEqual(expected.User, actual.User);
            AreEqual(expected.CreditCardType, actual.CreditCardType);
        }

        public static void AreEqual(PropertyAssignedManager expected, PropertyAssignedManager actual)
        {
            Assert.AreEqual(expected.PropertyAssignedManagerId, actual.PropertyAssignedManagerId);
            Assert.AreEqual(expected.Role, actual.Role);
            AreEqual(expected.Property, actual.Property);
            AreEqual(expected.User, actual.User);
        }

        public static void AreEqual(Panorama expected, Panorama actual)
        {
            Assert.AreEqual(expected.OffsetX, actual.OffsetX);
            Assert.AreEqual(expected.OffsetY, actual.OffsetY);
            Assert.AreEqual(expected.PanoramaName.i18nContent.First().Content, actual.PanoramaName.i18nContent.First().Content);
            Assert.AreEqual(expected.PanoramaName.i18nContent.First().Code, actual.PanoramaName.i18nContent.First().Code);
            Assert.AreEqual(expected.PanoramaNameCurrentLanguage, actual.PanoramaNameCurrentLanguage);
            Assert.AreEqual(expected.PanoramaPath, actual.PanoramaPath);
        }
    }
}
