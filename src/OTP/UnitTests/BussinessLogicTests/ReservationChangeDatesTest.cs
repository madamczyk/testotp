﻿using BusinessLogic.Objects.Templates.Document;
using BusinessLogic.PaymentGateway;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using Common.Exceptions;
using Common.Exceptions.ReservationDateChange;
using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace UnitTests.BussinessLogicTests
{
    [TestClass]
    public class ReservationChangeDatesTest
    {
        #region Fields
        private User _testUser;
        private Property _testProperty;
        private Unit _testUnit;
        private UnitType _testUnitType;
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;

        private Reservation currentReservation;
        private UnitInvBlocking currentUnitBlocking;
        private Destination _testDestination;
        private DictionaryCountry _testCountry;
        private PropertyType _testPropertyType;
        private DictionaryCulture _testCulture;
        private GuestPaymentMethod _testPaymentMethod;

        private DateTime dateArrivalAllowed = DateTime.Now.AddDays(70);
        private DateTime dateDepartureAllowed = DateTime.Now.AddDays(80);

        private DateTime dateArrivalLessThan30DaysAllowed = DateTime.Now.AddDays(15);
        private DateTime dateDepartureLessThan30DaysAllowed = DateTime.Now.AddDays(25);

        private DateTime dateArrivalDisabled = DateTime.Now.AddDays(100);
        private DateTime dateDepartureDisabled = DateTime.Now.AddDays(110);

        private PropertyDetailsInvoice newReservationGreaterInvoiceData = new PropertyDetailsInvoice()
        {
            InvoiceTotal = 4000,
            Culture = new System.Globalization.CultureInfo("en-US")
        };

        #endregion

        #region Services
        private IReservationsService _reservationService;
        private IDestinationsService _destinationService;
        private IDictionaryCountryService _countryService;
        private IPropertyTypesService _propertyTypeService;
        private ISettingsService _settingsService;
        private IMessageService _messageService;
        private IDictionaryCultureService _dictionaryCultureService;
        private IStorageQueueService _storageQueueService;
        private IBookingService _bookingService;
        private IZohoCrmService _zohoCrmService = null;
        private ICrmOperationLogsService _crmOperationLogsService = null;

        #endregion

        #region Mocks
        private Mock<IBookingService> bookingServiceMock = new Mock<IBookingService>();
        private Mock<IPaymentGatewayService> paymentGatewayMock = new Mock<IPaymentGatewayService>();
        private Mock<IDocumentService> documentServiceMock = new Mock<IDocumentService>();
        private Mock<IPropertiesService> propertiesServiceMock = new Mock<IPropertiesService>();
        private Mock<IContextService> httpContextMock = new Mock<IContextService>();
        private Mock<IConfigurationService> configurationServiceMock = new Mock<IConfigurationService>();
        #endregion

        #region Temporary Variables
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);
            
            httpContextMock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            httpContextMock.Setup(f => f.Current).Returns(new object());

            _settingsService = new SettingsService(httpContextMock.Object);
            _crmOperationLogsService = new CrmOperationLogsService(_efContext);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _storageQueueService = new StorageQueueService(configurationServiceMock.Object, httpContextMock.Object);
            _destinationService = new DestinationsService(configurationServiceMock.Object, httpContextMock.Object);
            _propertyTypeService = new PropertyTypeService(httpContextMock.Object);
            _countryService = new DictionaryCountryService(httpContextMock.Object);
            _messageService = new MessageService(_storageQueueService, documentServiceMock.Object, httpContextMock.Object, _settingsService);
            _dictionaryCultureService = new DictionaryCultureService(httpContextMock.Object);
            IUserService userService = new UsersService(httpContextMock.Object, _zohoCrmService, _messageService, _settingsService);
            IAuthorizationService authorizationService = new AuthorizationService(userService, httpContextMock.Object);
            _bookingService = new BookingService(httpContextMock.Object, propertiesServiceMock.Object, null, null, null, null, null, null, _settingsService, _messageService,
                paymentGatewayMock.Object, null, authorizationService);

            _testCountry = _countryService.GetCountries().First();
            _testDestination = _destinationService.GetDestinations().First();
            _testPropertyType = _propertyTypeService.GetPropertyTypes().First();
            _testCulture = _dictionaryCultureService.GetCultures().First();

            #endregion

            #region Setup data
            _testUser = new User()
            {
                Firstname = "ReservationChange_Firstname",
                Lastname = "ReservationChange_Lastname",
                email = "ReservationChange_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture
            };
            _efContext.Users.Add(_testUser);

            _testProperty = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "ReservationChange_PropertyCode",
                Address1 = "ReservationChange_Address1",
                State = "ReservationChange_State",
                ZIPCode = "ReservationChange_ZipCode",
                City = "ReservationChange_City",
                DictionaryCountry = _testCountry,
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "TimeZone"
            };
            _testProperty.SetPropertyNameValue("ReservationChange_PropertyName");
            _testProperty.SetShortDescriptionValue("ReservationChange_ShortDescription");
            _testProperty.SetLongDescriptionValue("ReservationChange_LongDescription");

            _efContext.Properties.Add(_testProperty);

            _testUnitType = new UnitType()
            {
                UnitTypeCode = "UnitsServiceTest_UnitTypeCode",
                Property = _testProperty
            };

            _testUnitType.SetTitleValue("ReservationChange_UnitTypeTitle");
            _testUnitType.SetDescValue("ReservationChange_UnitTypeDesc");

            _testUnit = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                UnitCode = "UnitsServiceTest_UnitCode"
            };
            _testUnit.SetTitleValue("ReservationChange_UnitTypeTitle");
            _testUnit.SetDescValue("ReservationChange_UnitTypeDesc");
            _efContext.UnitTypes.Add(_testUnitType);


            DateTime dateArrival = DateTime.Now.AddDays(40);
            DateTime dateDeparture = DateTime.Now.AddDays(50);

            currentReservation = new Reservation()
            {
                Unit = _testUnit,
                Property = _testProperty,
                NumberOfGuests = 3,
                ConfirmationID = "PPOPOP",
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                BookingDate = DateTime.Now.AddDays(-10),
                DateArrival = dateArrival,
                DateDeparture = dateDeparture,
                TotalPrice = 3000,
                TransactionFee = 1000,
                TripBalance = -1500,
                User = _testUser,
                UnitType = _testUnitType,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture
            };
            _efContext.Reservations.Add(currentReservation);

            currentUnitBlocking = new UnitInvBlocking()
            {
                DateFrom = dateArrival,
                DateUntil = dateDeparture,
                Reservation = currentReservation,
                Type = (int)UnitBlockingType.Reservation,
                Unit = _testUnit,
            };
            _efContext.UnitInvBlockings.Add(currentUnitBlocking);


            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "XXXX-XXXX-XXXX-1222",
                CreditCardType = _efContext.CreditCardTypes.Where(c => c.CreditCardTypeId.Equals((int)DataAccess.Enums.CreditCardType.visa)).SingleOrDefault(),
                ReferenceToken = "1111",
                User = _testUser
            };

            currentReservation.GuestPaymentMethod = _testPaymentMethod;

            #endregion
            _efContext.SaveChanges();

            #region Setup mocks behaviour

            TransactionResponse response = new TransactionResponse()
            {
                Succeded = true
            };
            paymentGatewayMock.Setup(f => f.AuthorizePayment(It.IsAny<TransactionRequest>(), It.IsAny<int>())).Returns(response);
            paymentGatewayMock.Setup(f => f.Purchase(It.IsAny<TransactionRequest>(), It.IsAny<int>())).Returns(response);

            propertiesServiceMock.Setup(f => f.CalculatePropertyCommission(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<int>())).Returns(300);

            bookingServiceMock.Setup(f => f.IsUnitAvailable(It.Is<BookingInfoModel>(mo => mo.dateTripFrom.Equals(dateArrivalAllowed)), It.IsAny<int?>())).Returns(true);
            bookingServiceMock.Setup(f => f.IsUnitAvailable(It.Is<BookingInfoModel>(mo => mo.dateTripFrom.Equals(dateArrivalLessThan30DaysAllowed)), It.IsAny<int?>())).Returns(true);
            bookingServiceMock.Setup(f => f.IsUnitAvailable(It.Is<BookingInfoModel>(mo => mo.dateTripFrom.Equals(dateDepartureDisabled)), It.IsAny<int?>())).Returns(false);
            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(newReservationGreaterInvoiceData);

            bookingServiceMock.Setup(f => f.AddPaymentTransaction(It.IsAny<Reservation>(), It.IsAny<decimal>())).Callback((Reservation resv, decimal amount) =>
            {
                _bookingService.AddPaymentTransaction(resv, amount);
            });

            bookingServiceMock.Setup(f => f.AddReservationTransactions(It.IsAny<Reservation>(), It.IsAny<PropertyDetailsInvoice>())).Callback((Reservation resv, PropertyDetailsInvoice invoiceDetails) =>
            {
                _bookingService.AddReservationTransactions(resv, invoiceDetails);
            }
                );
            decimal amountToAuthorize = 0;
            bookingServiceMock.Setup(f => f.CalculateAmountToAuthorize(It.IsAny<decimal>(), It.IsAny<DateTime>())).Callback((decimal fullAmount, DateTime arrivalDate) =>
            {
                amountToAuthorize = _bookingService.CalculateAmountToAuthorize(fullAmount, arrivalDate);
            }
            ).Returns(() => amountToAuthorize);

            documentServiceMock.Setup(f => f.CreateDoc(It.IsAny<BaseDocument>(), CultureCode.en_US)).Returns(new byte[128]);
            documentServiceMock.Setup(f => f.CreateDocument(It.IsAny<BaseDocument>(), CultureCode.en_US)).Returns(new MemoryStream());

            _reservationService = new ReservationsService(null, null, httpContextMock.Object, propertiesServiceMock.Object, 
                bookingServiceMock.Object, paymentGatewayMock.Object, documentServiceMock.Object, 
                _messageService, _settingsService, null);

            #endregion
        }



        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            _efContext.Dispose();
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            foreach (var user in _efContext.Users)
            {
                _efContext.Users.Remove(user);
            }
            foreach (var property in _efContext.Properties)
            {
                _efContext.Properties.Remove(property);
            }
            foreach (var unit in _efContext.Units)
            {
                _efContext.Units.Remove(unit);
            }
            foreach (var unitType in _efContext.UnitTypes)
            {
                _efContext.UnitTypes.Remove(unitType);
            }
            foreach (var paymentMethod in _efContext.GuestPaymentMethods)
            {
                _efContext.GuestPaymentMethods.Remove(paymentMethod);
            }

            foreach (var blockage in _efContext.UnitInvBlockings)
            {
                _efContext.UnitInvBlockings.Remove(blockage);
            }

            foreach (var reservation in _efContext.Reservations)
            {
                _efContext.Reservations.Remove(reservation);
            }

            foreach (var transaction in _efContext.ReservationTransactions)
            {
                _efContext.ReservationTransactions.Remove(transaction);
            }

            foreach (var reservationDetails in _efContext.ReservationDetails)
            {
                _efContext.ReservationDetails.Remove(reservationDetails);
            }

            foreach (var tax in _efContext.Taxes)
            {
                _efContext.Taxes.Remove(tax);
            }

            foreach (var attachement in _efContext.Attachments)
            {
                _efContext.Attachments.Remove(attachement);
            }

            foreach (var email in _efContext.Messages)
            {
                _efContext.Messages.Remove(email);
            }

            _efContext.SaveChanges();

        }
        #endregion

        [TestMethod]
        [ExpectedException(typeof(ResvDateChangeDateArrivalException))]
        public void ChangeToDateInPastIsImpossible()
        {
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(10));
        }

        [TestMethod]
        [ExpectedException(typeof(ResvDateChangeUnavailableException))]
        public void ChangeForInvBlockedUnitPeriodIsImpossible()
        {
            //Arrange            
            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalDisabled, dateDepartureDisabled);

            //Assert

        }

        [TestMethod]
        public void FindMostRestrictiveArrivalDate_currentReservation()
        {
            //Arrage
            DateTime mostRestrictiveArrivalDate = DateTime.Now.AddDays(10);

            Reservation mostEarly = new Reservation()
            {
                Unit = _testUnit,
                Property = _testProperty,
                NumberOfGuests = 3,
                ConfirmationID = "PPOPOP",
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                BookingDate = DateTime.Now.AddDays(-10),
                DateArrival = mostRestrictiveArrivalDate,
                DateDeparture = mostRestrictiveArrivalDate.AddDays(10),
                TotalPrice = 3000,
                TransactionFee = 1000,
                TripBalance = -2000,
                User = _testUser,
                UnitType = _testUnitType,
                ParentReservationId = currentReservation.ReservationID,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture
            };

            _efContext.Reservations.Add(mostEarly);
            _efContext.SaveChanges();

            Reservation mostForwarded = new Reservation()
            {
                Unit = _testUnit,
                Property = _testProperty,
                NumberOfGuests = 3,
                ConfirmationID = "PPOPOP",
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                BookingDate = DateTime.Now.AddDays(-10),
                DateArrival = mostRestrictiveArrivalDate.AddDays(70),
                DateDeparture = mostRestrictiveArrivalDate.AddDays(80),
                TotalPrice = 3000,
                TransactionFee = 1000,
                TripBalance = -2000,
                User = _testUser,
                UnitType = _testUnitType,
                ParentReservationId = mostEarly.ReservationID,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture
            };


            _efContext.Reservations.Add(mostForwarded);
            _efContext.SaveChanges();

            //Act
            var actual = _reservationService.FindMostRestrictiveArrivalDateForComparision(mostForwarded, mostForwarded.DateArrival);

            //Assert
            Assert.AreEqual(mostRestrictiveArrivalDate, actual);

        }

        [TestMethod]
        public void FindMostRestrictiveArrivalDate_changeDateReservation()
        {
            //Arrage
            DateTime mostRestrictiveArrivalDate = DateTime.Now.AddDays(10);

            Reservation mostEarly = new Reservation()
            {
                Unit = _testUnit,
                Property = _testProperty,
                NumberOfGuests = 3,
                ConfirmationID = "PPOPOP",
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                BookingDate = DateTime.Now.AddDays(-10),
                DateArrival = mostRestrictiveArrivalDate.AddDays(3),
                DateDeparture = mostRestrictiveArrivalDate.AddDays(10),
                TotalPrice = 3000,
                TransactionFee = 1000,
                TripBalance = -2000,
                User = _testUser,
                UnitType = _testUnitType,
                ParentReservationId = currentReservation.ReservationID,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture
            };

            _efContext.Reservations.Add(mostEarly);
            _efContext.SaveChanges();

            Reservation mostForwarded = new Reservation()
            {
                Unit = _testUnit,
                Property = _testProperty,
                NumberOfGuests = 3,
                ConfirmationID = "PPOPOP",
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                BookingDate = DateTime.Now.AddDays(-10),
                DateArrival = mostRestrictiveArrivalDate.AddDays(70),
                DateDeparture = mostRestrictiveArrivalDate.AddDays(80),
                TotalPrice = 3000,
                TransactionFee = 1000,
                TripBalance = -2000,
                User = _testUser,
                UnitType = _testUnitType,
                ParentReservationId = mostEarly.ReservationID,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture
            };


            _efContext.Reservations.Add(mostForwarded);
            _efContext.SaveChanges();

            //Act
            var actual = _reservationService.FindMostRestrictiveArrivalDateForComparision(mostForwarded, mostRestrictiveArrivalDate);

            //Assert
            Assert.AreEqual(mostRestrictiveArrivalDate, actual);

        }

        /// <summary>
        /// In this scenatio exception should be thrown
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ResvDateChangeExistingChildException))]
        public void ChangeDateForNotTreeLeafReservationImpossible()
        {
            DateTime mostRestrictiveArrivalDate = DateTime.Now.AddDays(10);

            Reservation mostEarly = new Reservation()
            {
                Unit = _testUnit,
                Property = _testProperty,
                NumberOfGuests = 3,
                ConfirmationID = "PPOPOP",
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                BookingDate = DateTime.Now.AddDays(-10),
                DateArrival = mostRestrictiveArrivalDate,
                DateDeparture = mostRestrictiveArrivalDate.AddDays(10),
                TotalPrice = 3000,
                TransactionFee = 1000,
                TripBalance = -2000,
                User = _testUser,
                UnitType = _testUnitType,
                ParentReservationId = currentReservation.ReservationID,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture
            };

            _efContext.Reservations.Add(mostEarly);
            _efContext.SaveChanges();

            Reservation mostForwarded = new Reservation()
            {
                Unit = _testUnit,
                Property = _testProperty,
                NumberOfGuests = 3,
                ConfirmationID = "PPOPOP",
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                BookingDate = DateTime.Now.AddDays(-10),
                DateArrival = mostRestrictiveArrivalDate.AddDays(70),
                DateDeparture = mostRestrictiveArrivalDate.AddDays(80),
                TotalPrice = 3000,
                TransactionFee = 1000,
                TripBalance = -2000,
                User = _testUser,
                UnitType = _testUnitType,
                ParentReservationId = mostEarly.ReservationID,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture
            };


            _efContext.Reservations.Add(mostForwarded);
            _efContext.SaveChanges();

            //Act
            _reservationService.ChangeReservationDate(mostEarly.ReservationID, mostRestrictiveArrivalDate, mostRestrictiveArrivalDate.AddDays(10));

            //Assert
        }

        /// <summary>
        /// In this scenario additional amount to get 50% of the new invoice total should be purchased
        /// </summary>
        [TestMethod]
        public void ChangeForMoreExpensiveReservationMoreThan30DaysAhead()
        {
            //Arrange        
            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 1000
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 4000,
                LengthOfStay = 10,
                PricePerNight = 400,
                TotalAccomodation = 3000,
                TotalTax = 1000,
                SubtotalAccomodationAddons = 3000,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalAllowed, dateDepartureAllowed)).Returns(
                invoice
                );

            decimal differenceToAdditionalPurchaseExpected = (((invoice.InvoiceTotal / 2) + currentReservation.TripBalance) * 100);
            decimal newTripBalanceExpected = -(invoice.InvoiceTotal / 2);
            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalAllowed, dateDepartureAllowed);
            _efContext.SaveChanges();

            //Assert
            Reservation newReservation = _efContext.Reservations.OrderByDescending(o => o.ReservationID).FirstOrDefault();

            #region check for correct values
            Assert.AreEqual(currentReservation.BookingStatus, (int)BookingStatus.DatesChanged);
            Assert.AreEqual(dateArrivalAllowed, newReservation.DateArrival);
            Assert.AreEqual(dateDepartureAllowed, newReservation.DateDeparture);
            Assert.AreEqual(currentReservation.ReservationID, newReservation.ParentReservationId);
            Assert.AreEqual(invoice.InvoiceTotal, newReservation.TotalPrice);
            Assert.AreEqual(2, newReservation.ReservationTransactions.Where(t => t.Payment.Equals(true)).Count());
            Assert.AreEqual(3, newReservation.ReservationTransactions.Where(t => t.Charge.Equals(true)).Count()); // {Accomodation, 7 days discount, Tax }
            Assert.AreEqual(newTripBalanceExpected, newReservation.TripBalance);
            #endregion


            #region correctness of the logic
            //check for correct charging amount ( charge additional data to be 50% of the new invoice value)
            paymentGatewayMock.Verify(f => f.Purchase(It.Is<TransactionRequest>(m => m.Amount.Equals(differenceToAdditionalPurchaseExpected.ToString())), It.IsAny<int>()), Times.Once());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<UserAgreement>(), CultureCode.en_US), Times.Exactly(2));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<Invoice>(), CultureCode.en_US), Times.Exactly(1));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US), Times.Exactly(1));
            #endregion
        }

        /// <summary>
        /// In this scenario there should be no additional charging
        /// </summary>
        [TestMethod]
        public void ChangeForLessExpensiveReservationMoreThan30DaysAhead()
        {
            //Arrange        
            currentReservation.Agreement = new byte[128];
            currentReservation.Invoice = new byte[128];


            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 500
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 2000,
                LengthOfStay = 10,
                PricePerNight = 200,
                TotalAccomodation = 1500,
                TotalTax = 500,
                SubtotalAccomodationAddons = 1500,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalAllowed, dateDepartureAllowed)).Returns(
                invoice
                );

            decimal newTripBalanceExpected = currentReservation.TripBalance;
            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalAllowed, dateDepartureAllowed);
            _efContext.SaveChanges();

            //Assert
            Reservation newReservation = _efContext.Reservations.OrderByDescending(o => o.ReservationID).FirstOrDefault();

            #region check for correct values
            Assert.AreEqual(currentReservation.BookingStatus, (int)BookingStatus.DatesChanged);
            Assert.AreEqual(dateArrivalAllowed, newReservation.DateArrival);
            Assert.AreEqual(dateDepartureAllowed, newReservation.DateDeparture);
            Assert.AreEqual(currentReservation.ReservationID, newReservation.ParentReservationId);
            Assert.AreEqual(currentReservation.TotalPrice, newReservation.TotalPrice); // price should be as the old reservation
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Payment.Equals(true)).Count());
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Charge.Equals(true)).Count()); // { Accomodation }
            Assert.AreEqual(newTripBalanceExpected, newReservation.TripBalance);
            #endregion


            #region correctness of the logic
            //check for correct charging amount ( charge additional data to be 50% of the new invoice value)
            paymentGatewayMock.Verify(f => f.Purchase(It.IsAny<TransactionRequest>(), It.IsAny<int>()), Times.Never());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<UserAgreementDateChange>(), CultureCode.en_US), Times.Exactly(2)); // agrement change doc should be generated
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<Invoice>(), CultureCode.en_US), Times.Never()); // invoice should be take from old reservation
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US), Times.Exactly(1));
            #endregion
        }

        /// <summary>
        /// In this scenario there should be no additional charging
        /// </summary>
        [TestMethod]
        public void ChangeForEqualExpensiveReservationMoreThan30DaysAhead()
        {
            //Arrange        
            currentReservation.Agreement = new byte[128];
            currentReservation.Invoice = new byte[128];


            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 500
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 3000,
                LengthOfStay = 10,
                PricePerNight = 300,
                TotalAccomodation = 2500,
                TotalTax = 500,
                SubtotalAccomodationAddons = 2500,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now,
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalAllowed, dateDepartureAllowed)).Returns(
                invoice
                );

            decimal newTripBalanceExpected = currentReservation.TripBalance;
            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalAllowed, dateDepartureAllowed);
            _efContext.SaveChanges();

            //Assert
            Reservation newReservation = _efContext.Reservations.OrderByDescending(o => o.ReservationID).FirstOrDefault();

            #region check for correct values
            Assert.AreEqual(currentReservation.BookingStatus, (int)BookingStatus.DatesChanged);
            Assert.AreEqual(dateArrivalAllowed, newReservation.DateArrival);
            Assert.AreEqual(dateDepartureAllowed, newReservation.DateDeparture);
            Assert.AreEqual(currentReservation.ReservationID, newReservation.ParentReservationId);
            Assert.AreEqual(currentReservation.TotalPrice, newReservation.TotalPrice); // price should be as the old reservation
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Payment.Equals(true)).Count());
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Charge.Equals(true)).Count()); // { Accomodation }
            Assert.AreEqual(newTripBalanceExpected, newReservation.TripBalance);
            #endregion


            #region correctness of the logic
            //check for correct charging amount ( charge additional data to be 50% of the new invoice value)
            paymentGatewayMock.Verify(f => f.Purchase(It.IsAny<TransactionRequest>(), It.IsAny<int>()), Times.Never());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<UserAgreementDateChange>(), CultureCode.en_US), Times.Exactly(2)); // agrement change doc should be generated
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<Invoice>(), CultureCode.en_US), Times.Never()); // invoice should be take from old reservation
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US), Times.Exactly(1));
            #endregion
        }

        /// <summary>
        /// In this scenatio exception should be thrown on payment gateway negative response
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(PaymentGatewayException))]
        public void ChangeForMoreExpensiveReservationMoreThan30DaysAheadExceptionOnPaymentGatewayError()
        {
            //Arrange  
            paymentGatewayMock.Setup(f => f.Purchase(It.IsAny<TransactionRequest>(), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = false
            });

            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 1000
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 4000,
                LengthOfStay = 10,
                PricePerNight = 400,
                TotalAccomodation = 3000,
                TotalTax = 1000,
                SubtotalAccomodationAddons = 3000,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalAllowed, dateDepartureAllowed)).Returns(
                invoice
                );

            decimal differenceToAdditionalPurchaseExpected = (((invoice.InvoiceTotal / 2) + currentReservation.TripBalance) * 100);
            decimal newTripBalanceExpected = -(invoice.InvoiceTotal / 2);
            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalAllowed, dateDepartureAllowed);            
        }

        /// <summary>
        /// In this scenario new amount should be authorized and old one should be cancelled
        /// </summary>
        [TestMethod]
        public void ChangeForMoreExpensiveReservationLessThan24HoursFromReservation()
        {
            //Arrange    
            currentReservation.BookingStatus = (int)BookingStatus.ReceivedBooking;
            currentReservation.BookingDate = DateTime.Now.AddMinutes(-20);

            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 1000
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 4000,
                LengthOfStay = 10,
                PricePerNight = 400,
                TotalAccomodation = 3000,
                TotalTax = 1000,
                SubtotalAccomodationAddons = 3000,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalAllowed, dateDepartureAllowed)).Returns(
                invoice
                );

            decimal amountToAuthorizeExpected = (this._bookingService.CalculateAmountToAuthorize(invoice.InvoiceTotal, dateArrivalAllowed)) * 100;
            decimal newTripBalanceExpected = -(invoice.InvoiceTotal / 2);
            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalAllowed, dateDepartureAllowed);
            _efContext.SaveChanges();

            //Assert
            Reservation newReservation = _efContext.Reservations.OrderByDescending(o => o.ReservationID).FirstOrDefault();

            #region check for correct values
            Assert.AreEqual(currentReservation.BookingStatus, (int)BookingStatus.DatesChanged);
            Assert.AreEqual(dateArrivalAllowed, newReservation.DateArrival);
            Assert.AreEqual(dateDepartureAllowed, newReservation.DateDeparture);
            Assert.AreEqual(currentReservation.ReservationID, newReservation.ParentReservationId);
            Assert.AreEqual(invoice.InvoiceTotal, newReservation.TotalPrice);
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Payment.Equals(true)).Count());
            Assert.AreEqual(3, newReservation.ReservationTransactions.Where(t => t.Charge.Equals(true)).Count()); // {Accomodation, 7 days discount, Tax }
            Assert.AreEqual(newTripBalanceExpected, newReservation.TripBalance);
            #endregion


            #region correctness of the logic
            //check for correct charging amount ( charge additional data to be 50% of the new invoice value)
            paymentGatewayMock.Verify(f => f.AuthorizePayment(It.Is<TransactionRequest>(m => m.Amount.Equals(amountToAuthorizeExpected.ToString())), It.IsAny<int>()), Times.Once());
            paymentGatewayMock.Verify(f => f.CancelPayment(It.IsAny<string>(), It.IsAny<int>()), Times.Once());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<UserAgreement>(), CultureCode.en_US), Times.Exactly(2));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<Invoice>(), CultureCode.en_US), Times.Exactly(1));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US), Times.Exactly(1));
            #endregion
        }

        /// <summary>
        /// In this scenario new amount should be authorized and old one should be cancelled
        /// </summary>
        [TestMethod]
        public void ChangeForLessExpensiveReservationLessThan24HoursFromReservation()
        {
            //Arrange    
            currentReservation.BookingStatus = (int)BookingStatus.ReceivedBooking;
            currentReservation.BookingDate = DateTime.Now.AddMinutes(-20);

            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 100
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 1000,
                LengthOfStay = 10,
                PricePerNight = 100,
                TotalAccomodation = 900,
                TotalTax = 100,
                SubtotalAccomodationAddons = 900,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalAllowed, dateDepartureAllowed)).Returns(
                invoice
                );

            decimal amountToAuthorizeExpected = (this._bookingService.CalculateAmountToAuthorize(invoice.InvoiceTotal, dateArrivalAllowed)) * 100;
            decimal newTripBalanceExpected = -(invoice.InvoiceTotal / 2);
            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalAllowed, dateDepartureAllowed);
            _efContext.SaveChanges();

            //Assert
            Reservation newReservation = _efContext.Reservations.OrderByDescending(o => o.ReservationID).FirstOrDefault();

            #region check for correct values
            Assert.AreEqual(currentReservation.BookingStatus, (int)BookingStatus.DatesChanged);
            Assert.AreEqual(dateArrivalAllowed, newReservation.DateArrival);
            Assert.AreEqual(dateDepartureAllowed, newReservation.DateDeparture);
            Assert.AreEqual(currentReservation.ReservationID, newReservation.ParentReservationId);
            Assert.AreEqual(invoice.InvoiceTotal, newReservation.TotalPrice);
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Payment.Equals(true)).Count());
            Assert.AreEqual(3, newReservation.ReservationTransactions.Where(t => t.Charge.Equals(true)).Count()); // {Accomodation, 7 days discount, Tax }
            Assert.AreEqual(newTripBalanceExpected, newReservation.TripBalance);
            #endregion


            #region correctness of the logic
            //check for correct charging amount ( charge additional data to be 50% of the new invoice value)
            paymentGatewayMock.Verify(f => f.AuthorizePayment(It.Is<TransactionRequest>(m => m.Amount.Equals(amountToAuthorizeExpected.ToString())), It.IsAny<int>()), Times.Once());
            paymentGatewayMock.Verify(f => f.CancelPayment(It.IsAny<string>(), It.IsAny<int>()), Times.Once());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<UserAgreement>(), CultureCode.en_US), Times.Exactly(2));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<Invoice>(), CultureCode.en_US), Times.Exactly(1));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US), Times.Exactly(1));
            #endregion
        }

        /// <summary>
        /// In this scenario new amount should be authorized and old one should be cancelled
        /// </summary>
        [TestMethod]
        public void ChangeForEqualExpensiveReservationLessThan24HoursFromReservation()
        {
            //Arrange    
            currentReservation.BookingStatus = (int)BookingStatus.ReceivedBooking;
            currentReservation.BookingDate = DateTime.Now.AddMinutes(-20);
            currentReservation.TransactionToken = "TOKEN";

            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 1000
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 3000,
                LengthOfStay = 10,
                PricePerNight = 200,
                TotalAccomodation = 2000,
                TotalTax = 100,
                SubtotalAccomodationAddons = 200,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalAllowed, dateDepartureAllowed)).Returns(
                invoice
                );

            decimal amountToAuthorizeExpected = (this._bookingService.CalculateAmountToAuthorize(invoice.InvoiceTotal, dateArrivalAllowed)) * 100;
            decimal newTripBalanceExpected = -(invoice.InvoiceTotal / 2);
            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalAllowed, dateDepartureAllowed);
            _efContext.SaveChanges();

            //Assert
            Reservation newReservation = _efContext.Reservations.OrderByDescending(o => o.ReservationID).FirstOrDefault();

            #region check for correct values
            Assert.AreEqual(currentReservation.BookingStatus, (int)BookingStatus.DatesChanged);
            Assert.AreEqual(dateArrivalAllowed, newReservation.DateArrival);
            Assert.AreEqual(dateDepartureAllowed, newReservation.DateDeparture);
            Assert.AreEqual(currentReservation.ReservationID, newReservation.ParentReservationId);
            Assert.AreEqual(invoice.InvoiceTotal, newReservation.TotalPrice);
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Payment.Equals(true)).Count());
            Assert.AreEqual(3, newReservation.ReservationTransactions.Where(t => t.Charge.Equals(true)).Count()); // {Accomodation, 7 days discount, Tax }
            Assert.AreEqual(newTripBalanceExpected, newReservation.TripBalance);
            #endregion


            #region correctness of the logic
            //check for correct charging amount ( charge additional data to be 50% of the new invoice value)
            paymentGatewayMock.Verify(f => f.AuthorizePayment(It.Is<TransactionRequest>(m => m.Amount.Equals(amountToAuthorizeExpected.ToString())), It.IsAny<int>()), Times.Once());
            paymentGatewayMock.Verify(f => f.CancelPayment(It.Is<string>(t => t.Equals(currentReservation.TransactionToken)), It.IsAny<int>()), Times.Once());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<UserAgreement>(), CultureCode.en_US), Times.Exactly(2));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<Invoice>(), CultureCode.en_US), Times.Exactly(1));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US), Times.Exactly(1));
            #endregion
        }

        /// <summary>
        /// In this scenario exception should be thrown on payment gateway negative response
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(PaymentGatewayException))]
        public void ChangeForMoreExpensiveReservationLessThan24HoursFromReservationExceptionOnPaymentGatewayError()
        {
            paymentGatewayMock.Setup(f => f.AuthorizePayment(It.IsAny<TransactionRequest>(), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = false
            });

            //Arrange    
            currentReservation.BookingStatus = (int)BookingStatus.ReceivedBooking;
            currentReservation.BookingDate = DateTime.Now.AddMinutes(-20);

            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 1000
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 4000,
                LengthOfStay = 10,
                PricePerNight = 400,
                TotalAccomodation = 3000,
                TotalTax = 1000,
                SubtotalAccomodationAddons = 3000,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalAllowed, dateDepartureAllowed)).Returns(
                invoice
                );

            decimal amountToAuthorizeExpected = (this._bookingService.CalculateAmountToAuthorize(invoice.InvoiceTotal, dateArrivalAllowed)) * 100;
            decimal newTripBalanceExpected = -(invoice.InvoiceTotal / 2);

            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalAllowed, dateDepartureAllowed);
        }

        /// <summary>
        /// In this scenario whole lacing amount should be purchased
        /// </summary>
        [TestMethod]
        public void ChangeForMoreExpensiveReservationMoreThan24HoursLessThan30DaysAhead()
        {
            //Arrange                    
            currentReservation.BookingDate.AddDays(10);

            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 1000
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 4000,
                LengthOfStay = 10,
                PricePerNight = 400,
                TotalAccomodation = 3000,
                TotalTax = 1000,
                SubtotalAccomodationAddons = 3000,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalLessThan30DaysAllowed, dateDepartureLessThan30DaysAllowed)).Returns(
                invoice
                );

            decimal differenceToAdditionalPurchaseExpected = (invoice.InvoiceTotal + currentReservation.TripBalance) * 100;
            decimal newTripBalanceExpected = 0;

            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalLessThan30DaysAllowed, dateDepartureLessThan30DaysAllowed);
            _efContext.SaveChanges();

            //Assert
            Reservation newReservation = _efContext.Reservations.OrderByDescending(o => o.ReservationID).FirstOrDefault();

            #region check for correct values
            Assert.AreEqual(currentReservation.BookingStatus, (int)BookingStatus.DatesChanged);
            Assert.AreEqual(dateArrivalLessThan30DaysAllowed, newReservation.DateArrival);
            Assert.AreEqual(dateDepartureLessThan30DaysAllowed, newReservation.DateDeparture);
            Assert.AreEqual(currentReservation.ReservationID, newReservation.ParentReservationId);
            Assert.AreEqual(invoice.InvoiceTotal, newReservation.TotalPrice);
            Assert.AreEqual(2, newReservation.ReservationTransactions.Where(t => t.Payment.Equals(true)).Count());
            Assert.AreEqual(3, newReservation.ReservationTransactions.Where(t => t.Charge.Equals(true)).Count()); // {Accomodation, 7 days discount, Tax }
            Assert.AreEqual(newTripBalanceExpected, newReservation.TripBalance);
            #endregion


            #region correctness of the logic
            //check for correct charging amount ( charge additional data to be 50% of the new invoice value)
            paymentGatewayMock.Verify(f => f.Purchase(It.Is<TransactionRequest>(m => m.Amount.Equals(differenceToAdditionalPurchaseExpected.ToString())), It.IsAny<int>()), Times.Once());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<UserAgreement>(), CultureCode.en_US), Times.Exactly(2));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<Invoice>(), CultureCode.en_US), Times.Exactly(1));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US), Times.Exactly(1));
            #endregion
        }

        /// <summary>
        /// In this scenario additional 50% should be taken with purchase method
        /// </summary>
        [TestMethod]
        public void ChangeForLessExpensiveReservationMoreThan24HoursLessThan30DaysAhead()
        {
            //Arrange                    
            currentReservation.BookingDate.AddDays(10);
            currentReservation.Agreement = new byte[128];
            currentReservation.Invoice = new byte[128];

            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 500
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 2000,
                LengthOfStay = 10,
                PricePerNight = 200,
                TotalAccomodation = 1500,
                TotalTax = 500,
                SubtotalAccomodationAddons = 1500,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalLessThan30DaysAllowed, dateDepartureLessThan30DaysAllowed)).Returns(
                invoice
                );

            decimal newTripBalanceExpected = 0;
            decimal differenceToAdditionalPurchaseExpected = (currentReservation.TotalPrice + currentReservation.TripBalance) * 100;

            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalLessThan30DaysAllowed, dateDepartureLessThan30DaysAllowed);
            _efContext.SaveChanges();

            //Assert
            Reservation newReservation = _efContext.Reservations.OrderByDescending(o => o.ReservationID).FirstOrDefault();

            #region check for correct values
            Assert.AreEqual(currentReservation.BookingStatus, (int)BookingStatus.DatesChanged);
            Assert.AreEqual(dateArrivalLessThan30DaysAllowed, newReservation.DateArrival);
            Assert.AreEqual(dateDepartureLessThan30DaysAllowed, newReservation.DateDeparture);
            Assert.AreEqual(currentReservation.ReservationID, newReservation.ParentReservationId);
            Assert.AreEqual(currentReservation.TotalPrice, newReservation.TotalPrice);
            Assert.AreEqual(2, newReservation.ReservationTransactions.Where(t => t.Payment.Equals(true)).Count());
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Charge.Equals(true)).Count()); // {Accomodation, 7 days discount, Tax }
            Assert.AreEqual(newTripBalanceExpected, newReservation.TripBalance);
            #endregion


            #region correctness of the logic
            //check for correct charging amount ( charge additional data to be 50% of the new invoice value)
            paymentGatewayMock.Verify(f => f.Purchase(It.Is<TransactionRequest>(t => t.Amount.Equals(differenceToAdditionalPurchaseExpected.ToString())), It.IsAny<int>()), Times.Once());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<UserAgreementDateChange>(), CultureCode.en_US), Times.Exactly(2));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<Invoice>(), CultureCode.en_US), Times.Never());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US), Times.Exactly(1));
            #endregion
        }


        /// <summary>
        /// In this scenation no additional amout should be taken from guest account
        /// </summary>
        [TestMethod]
        public void ChangeForLessExpensiveFromAlreadyPaidReservationMoreThan24HoursLessThan30DaysAhead()
        {
            //Arrange                    
            currentReservation.BookingDate.AddDays(10);
            currentReservation.Agreement = new byte[128];
            currentReservation.Invoice = new byte[128];
            currentReservation.TripBalance = 0;

            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 400
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 1400,
                LengthOfStay = 10,
                PricePerNight = 100,
                TotalAccomodation = 1000,
                TotalTax = 400,
                SubtotalAccomodationAddons = 1000,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 3000,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalLessThan30DaysAllowed, dateDepartureLessThan30DaysAllowed)).Returns(
                invoice
                );

            decimal newTripBalanceExpected = 0;

            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalLessThan30DaysAllowed, dateDepartureLessThan30DaysAllowed);
            _efContext.SaveChanges();

            //Assert
            Reservation newReservation = _efContext.Reservations.OrderByDescending(o => o.ReservationID).FirstOrDefault();

            #region check for correct values
            Assert.AreEqual(currentReservation.BookingStatus, (int)BookingStatus.DatesChanged);
            Assert.AreEqual(dateArrivalLessThan30DaysAllowed, newReservation.DateArrival);
            Assert.AreEqual(dateDepartureLessThan30DaysAllowed, newReservation.DateDeparture);
            Assert.AreEqual(currentReservation.ReservationID, newReservation.ParentReservationId);
            Assert.AreEqual(currentReservation.TotalPrice, newReservation.TotalPrice);
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Payment.Equals(true)).Count());
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Charge.Equals(true)).Count()); // {Accomodation, 7 days discount, Tax }
            Assert.AreEqual(newTripBalanceExpected, newReservation.TripBalance);
            #endregion


            #region correctness of the logic
            //check for correct charging amount ( charge additional data to be 50% of the new invoice value)
            paymentGatewayMock.Verify(f => f.Purchase(It.IsAny<TransactionRequest>(), It.IsAny<int>()), Times.Never());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<UserAgreementDateChange>(), CultureCode.en_US), Times.Exactly(2));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<Invoice>(), CultureCode.en_US), Times.Never());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US), Times.Exactly(1));
            #endregion
        }

        /// <summary>
        /// In this scenario additional 50% should be taken with purchase method
        /// </summary>
        [TestMethod]
        public void ChangeForEqualExpensiveReservationMoreThan24HoursLessThan30DaysAhead()
        {
            //Arrange                    
            currentReservation.BookingDate.AddDays(10);
            currentReservation.Agreement = new byte[128];
            currentReservation.Invoice = new byte[128];

            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 500
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 3000,
                LengthOfStay = 10,
                PricePerNight = 300,
                TotalAccomodation = 2500,
                TotalTax = 500,
                SubtotalAccomodationAddons = 500,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalLessThan30DaysAllowed, dateDepartureLessThan30DaysAllowed)).Returns(
                invoice
                );

            decimal differenceToAdditionalPurchaseExpected = (invoice.InvoiceTotal + currentReservation.TripBalance) * 100;
            decimal newTripBalanceExpected = 0;

            //Act
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalLessThan30DaysAllowed, dateDepartureLessThan30DaysAllowed);
            _efContext.SaveChanges();

            //Assert
            Reservation newReservation = _efContext.Reservations.OrderByDescending(o => o.ReservationID).FirstOrDefault();

            #region check for correct values
            Assert.AreEqual(currentReservation.BookingStatus, (int)BookingStatus.DatesChanged);
            Assert.AreEqual(dateArrivalLessThan30DaysAllowed, newReservation.DateArrival);
            Assert.AreEqual(dateDepartureLessThan30DaysAllowed, newReservation.DateDeparture);
            Assert.AreEqual(currentReservation.ReservationID, newReservation.ParentReservationId);
            Assert.AreEqual(currentReservation.TotalPrice, newReservation.TotalPrice);
            Assert.AreEqual(2, newReservation.ReservationTransactions.Where(t => t.Payment.Equals(true)).Count());
            Assert.AreEqual(1, newReservation.ReservationTransactions.Where(t => t.Charge.Equals(true)).Count()); // {Accomodation, 7 days discount, Tax }
            Assert.AreEqual(newTripBalanceExpected, newReservation.TripBalance);
            #endregion


            #region correctness of the logic
            //check for correct charging amount ( charge additional data to be 50% of the new invoice value)
            paymentGatewayMock.Verify(f => f.Purchase(It.Is<TransactionRequest>(t => t.Amount.Equals(differenceToAdditionalPurchaseExpected.ToString())), It.IsAny<int>()), Times.Once());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<UserAgreementDateChange>(), CultureCode.en_US), Times.Exactly(2));
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<Invoice>(), CultureCode.en_US), Times.Never());
            documentServiceMock.Verify(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US), Times.Exactly(1));
            #endregion
        }



        /// <summary>
        /// Check for transactional of the reservation logic - cannnot invoke save changes on disposed context
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CheckTransactionalOfTheReservationChangeLogic()
        {
            //Arrange    
            currentReservation.BookingStatus = (int)BookingStatus.ReceivedBooking;
            currentReservation.BookingDate = DateTime.Now.AddMinutes(-20);

            Tax testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10,
                CalculatedValue = 1000
            };
            testTax.SetNameValue("Test tax");

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 4000,
                LengthOfStay = 10,
                PricePerNight = 400,
                TotalAccomodation = 3000,
                TotalTax = 1000,
                SubtotalAccomodationAddons = 3000,
                TaxList = new List<Tax>() { testTax }
            };

            int transactionCodeAccomodation = (int)TransactionCodes.DuePayment;
            int transactionCodePayment = (int)TransactionCodes.Payment;

            ReservationTransaction transactionCharge = new ReservationTransaction()
            {
                Amount = 3000,
                Charge = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodeAccomodation)).FirstOrDefault(),
                TransactionDate = DateTime.Now
            };
            transactionCharge.SetNameValue("Test");

            ReservationTransaction transactionPayment = new ReservationTransaction()
            {
                Amount = 1500,
                Payment = true,
                Reservation = currentReservation,
                TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals(transactionCodePayment)).FirstOrDefault(),
                TransactionDate = DateTime.Now

            };
            transactionPayment.SetNameValue("Test");

            _efContext.ReservationTransactions.Add(transactionPayment);
            _efContext.ReservationTransactions.Add(transactionCharge);
            _efContext.Taxes.Add(testTax);

            _efContext.SaveChanges();

            bookingServiceMock.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), dateArrivalAllowed, dateDepartureAllowed)).Returns(
                invoice
                );
            paymentGatewayMock.Setup(f => f.AuthorizePayment(It.IsAny<TransactionRequest>(), It.IsAny<int>())).Throws(new PaymentGatewayException("TEST", null));

            decimal amountToAuthorizeExpected = (this._bookingService.CalculateAmountToAuthorize(invoice.InvoiceTotal, dateArrivalAllowed)) * 100;
            decimal newTripBalanceExpected = -(invoice.InvoiceTotal / 2);
            //Act
            try
            {
                _reservationService.ChangeReservationDate(currentReservation.ReservationID, dateArrivalAllowed, dateDepartureAllowed);
            }
            catch (PaymentGatewayException)
            {
                //nop
            }
            _efContext.SaveChanges();

            //Assert
        }


        [TestMethod]
        [ExpectedException(typeof(ResvDateChangeNoChangeException))]
        public void ChangeToSameDateIsImpossible()
        {
            _reservationService.ChangeReservationDate(currentReservation.ReservationID, currentReservation.DateArrival, currentReservation.DateDeparture);
        }

    }
}
