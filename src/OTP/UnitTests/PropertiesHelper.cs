﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using BusinessLogic.ServiceContracts;
using DataAccess;
using BusinessLogic.Services;
using Moq;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;

namespace UnitTests
{
    static public class PropertiesHelper
    {
        #region Services
        private static ITagsService _tagsService = null;

        private static ITagGroupsService _tagsGroupService = null;

        private static IPropertiesService _propertiesService = null;

        private static IDestinationsService _destinationService = null;
        
        private static IPropertyTypesService _propTypeService = null;

        private static IDictionaryCountryService _dictCountryService = null;

        private static IUserService _userService = null;

        private static IBlobService _blobService = null;

        private static IDictionaryCultureService _dictionaryCultureService = null;

        private static IZohoCrmService _zohoCrmService = null;

        private static IMessageService _messageService = null;

        private static ICrmOperationLogsService _crmOperationLogsService = null;

        private static ISettingsService _settingsService = null;

        #endregion

        /// <summary>
        /// Setups property and returns it, but not saving.
        /// </summary>
        /// <param name="_efContext"></param>
        /// <returns></returns>
        public static Property generateProperty(OTPEntities _efContext, Tag tag, Destination dest)
        {
            #region Setup services
            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _messageService = new MessageService(mock.Object, _settingsService);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _blobService = new BlobService(mock.Object);
            _tagsService = new TagsService(mock.Object);
            _propertiesService = new PropertiesServices(configurationService, _blobService, _settingsService, mock.Object);
            _tagsGroupService = new TagGroupsService(mock.Object);
            _destinationService = new DestinationsService(configurationService, mock.Object);
            _propTypeService = new PropertyTypeService(mock.Object);
            _dictCountryService = new DictionaryCountryService(mock.Object);
            _userService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);
            #endregion

            #region Setup property
            DictionaryCountry referencedCountry = _dictCountryService.GetCountries().First();
            Destination referencedDestination = _destinationService.GetDestinations().First();
            PropertyType referencedPropertyType = _propTypeService.GetPropertyTypes().First();
            TagGroup _testTagsGroup = _tagsGroupService.GetTagGroups().First();
            DictionaryCulture _testCulture = _dictionaryCultureService.GetCultures().First();
            Tag propTag = null;

            User referencedUser = new User()
            {
                Firstname = "FirstName",
                Lastname = "LastName",
                email = "email",
                Password = "Password",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture
            };

            if (tag != null)
            {
                tag.TagGroup = _testTagsGroup;
            }
            else
            {
                propTag = new Tag()
                {
                    TagGroup = _testTagsGroup,
                };
                propTag.SetNameValue("TagName");
            }
            Property prop = new Property()
            {
                Destination = (dest != null) ? dest : referencedDestination,
                PropertyTags = new List<PropertyTag>(),
                DictionaryCountry = referencedCountry,
                PropertyType = referencedPropertyType,
                User = referencedUser,
                Address1 = "adress",
                City = "city",
                ZIPCode = "zcode",
                PropertyCode = "pcode",
                State = " state",
                DictionaryCulture = _testCulture,
                TimeZone = "TZ"
            };

            PropertyTag pTag = new PropertyTag()
            {
                Tag = (tag != null) ? tag : propTag,
                Property = prop,
            };

            prop.PropertyTags.Add(pTag);

            prop.SetLongDescriptionValue("TestLongDescription", DataAccess.Enums.CultureCode.de_DE);
            prop.SetShortDescriptionValue("TestShortDEscription", DataAccess.Enums.CultureCode.en_US);
            prop.SetPropertyNameValue("PropertyName", DataAccess.Enums.CultureCode.en_US);

            _propertiesService.AddProperty(prop);

            return prop;
            #endregion
        }
        
        public static void deletePropertyAndRelated(OTPEntities _efContext, Property prop)
        {
            if (prop != null)
            {
                #region Setup services
                var mock = new Mock<IContextService>();
                mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
                mock.Setup(f => f.Current).Returns(new object());

                IConfigurationService configurationService = new ConfigurationService(mock.Object);
                _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
                _settingsService = new SettingsService(_efContext);
                _messageService = new MessageService(mock.Object, _settingsService);
                _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
                _blobService = new BlobService(mock.Object);
                _tagsService = new TagsService(mock.Object);
                _propertiesService = new PropertiesServices(configurationService, _blobService, _settingsService, mock.Object);
                _tagsGroupService = new TagGroupsService(mock.Object);
                _destinationService = new DestinationsService(configurationService, mock.Object);
                _propTypeService = new PropertyTypeService(mock.Object);
                _dictCountryService = new DictionaryCountryService(mock.Object);
                _userService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
                #endregion

                int uid = prop.User.UserID;
                int tid = prop.PropertyTags.First().Tag.TagID;

                _propertiesService.RemoveProperty(prop.PropertyID);
                _userService.DeleteUserById(uid);
                _tagsService.DeleteTag(tid);
            }
        }
    }
}
