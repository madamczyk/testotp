﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTests
{
    public static class Messages
    {
        public static readonly string NotPresentInCollection = "Not all added {0} present in the collection returned by {1}() method.";
        public static readonly string ExpectedException = "An exception of type {0} was expected, but not thrown.";
        public static readonly string ExpectedExceptionForField = "An exception of type {0} was expected, but not thrown. Field: {1}";
        public static readonly string WrongException = "An exception of type {0} was expected, but {1} was caught.";
        public static readonly string UnexpectedException = "An exception was not expected but exception of type {0} has occured.";
        public static readonly string DeletionShouldNotSucceed = "Deletion of referenced object was succesfull but it shouldn't succeed.";
        public static readonly string DeletionShouldSucceed = "Deletion of referenced object was not succesfull, an exception of type {0} has occured.";
        public static readonly string ObjectIsNull = "An object is null but it shouldn't be.";
        public static readonly string ObjectIsNotNull = "An object is not null but it should be.";
        public static readonly string DataBaseContainsData = "Data base should contain only base data, but there are some other data";
        public static readonly string MethodWasNotCalled = "Method was not called.";
    }
}
