﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class DestinationsServiceTest
    {
        #region Fields
        /// <summary>
        /// destinations used in tests
        /// </summary>
        private List<Destination> _testDestinations = new List<Destination>();
        /// <summary>
        /// properties used in test
        /// </summary>
        private List<Property> _testProperties = new List<Property>();
        /// <summary>
        /// tax for destinations 
        /// </summary>
        private Tax _testTax = null;
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext = null;
        #endregion

        #region Services
        private IDestinationsService _destService = null;
        private IPropertiesService _propService = null;
        private IBlobService _blobService = null;
        private ITaxesService _taxesService = null;
        private IUserService _userService = null;
        private IDictionaryCultureService _dictionaryCultureService = null;
        private IZohoCrmService _zohoCrmService = null;
        private IMessageService _messageService = null;
        private ICrmOperationLogsService _crmOperationLogsService = null;
        private ISettingsService _settingsService = null;
        #endregion

        #region Prepare/Cleanup Data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _messageService = new MessageService(mock.Object, _settingsService);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _destService = new DestinationsService(configurationService, mock.Object);
            _taxesService = new TaxesService(mock.Object);
            _blobService = new BlobService(mock.Object);
            _propService = new PropertiesServices(configurationService, _blobService, _settingsService, mock.Object);
            _userService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);
            #endregion

            _testTax = new Tax()
            {
                Percentage = 20.0m,
                CalculatedValue = 10.0m,
            };
            _testTax.SetNameValue("TestTaxName");
            _taxesService.AddTax(_testTax);
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (Property prop in _testProperties)
            {
                if (_propService.GetPropertyById(prop.PropertyID) != null)
                {
                    PropertiesHelper.deletePropertyAndRelated(_efContext, prop);
                }
            }

            if (_taxesService.GetTaxById(_testTax.TaxId) != null)
                _taxesService.DeleteTax(_testTax.TaxId);

            foreach (Destination dest in _testDestinations)
            {
                if (_destService.GetDestinationById(dest.DestinationID) != null)
                    _destService.DeleteDestination(dest.DestinationID);
            }

            _testDestinations.Clear();
            _testProperties.Clear();
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void GetTopXDestinationsByPropertiesCountTest()
        {
            Destination[] destinations = new Destination[5];
            // high amount of properties to prevent from collisions with destinations already in db
            int[] propPerDestinationCounts = new int[5] {30, 25, 25, 25, 24};

            #region Add destinations
            for (int i = 0; i < 5; i++)
            {
                destinations[i] = new Destination
                {
                    DestinationDescriptionCurrentLanguage = "en",
                    DestinationNameCurrentLanguage = "en",
                    Active = true
                };
                destinations[i].Taxes.Add(_testTax);
                destinations[i].SetDestinationDescriptionValue("destinationdescription" + i);
                destinations[i].SetDestinationNameValue("DestinationName" + i);
                destinations[i].PersistI18nValues();

                _destService.AddDestination(destinations[i]);
                _testDestinations.Add(destinations[i]);
            }
            _efContext.SaveChanges();
            #endregion

            #region Add and connect properties with destinations
            for (int j = 0; j < 5; j++)
            {
                for (int k = 0; k < propPerDestinationCounts[j]; k++)
                {
                    Property prop = PropertiesHelper.generateProperty(_efContext, null, destinations[j]);
                    _testProperties.Add(prop);
                }
            }
            _efContext.SaveChanges();
            #endregion

            #region Test 1
            List<Destination> test1 = _destService.GetTopXDestinationsByPropertiesCount(4).ToList();

            for (int i = 0; i < 4; i++)
            {
                Assert.IsNotNull(test1.ElementAt(i), Messages.ObjectIsNull);
                Assert.AreEqual(destinations[i].DestinationID, test1.ElementAt(i).DestinationID);
                Assert.AreEqual(destinations[i].DestinationName, test1.ElementAt(i).DestinationName);
                Assert.AreEqual(destinations[i].DestinationNameCurrentLanguage, test1.ElementAt(i).DestinationNameCurrentLanguage);
                Assert.AreEqual(destinations[i].DestinationDescriptionCurrentLanguage, test1.ElementAt(i).DestinationDescriptionCurrentLanguage);
                Assert.AreEqual(destinations[i].DestinationDescription, test1.ElementAt(i).DestinationDescription);
                Assert.AreEqual(destinations[i].Description_i18n, test1.ElementAt(i).Description_i18n);
                Assert.AreEqual(destinations[i].Destination_i18n, test1.ElementAt(i).Destination_i18n);
            }
            #endregion

            #region Test 2
            List<Destination> test2 = _destService.GetTopXDestinationsByPropertiesCount(1).ToList();

            Assert.IsNotNull(test2.ElementAt(0), Messages.ObjectIsNull);
            Assert.AreEqual(destinations[0].DestinationID, test2.ElementAt(0).DestinationID);
            Assert.AreEqual(destinations[0].DestinationName, test2.ElementAt(0).DestinationName);
            Assert.AreEqual(destinations[0].DestinationNameCurrentLanguage, test2.ElementAt(0).DestinationNameCurrentLanguage);
            Assert.AreEqual(destinations[0].DestinationDescriptionCurrentLanguage, test2.ElementAt(0).DestinationDescriptionCurrentLanguage);
            Assert.AreEqual(destinations[0].DestinationDescription, test2.ElementAt(0).DestinationDescription);
            Assert.AreEqual(destinations[0].Description_i18n, test2.ElementAt(0).Description_i18n);
            Assert.AreEqual(destinations[0].Destination_i18n, test2.ElementAt(0).Destination_i18n);
            #endregion

            #region Test 3
            List<Destination> test3 = _destService.GetTopXDestinationsByPropertiesCount(5).ToList();

            for (int i = 0; i < 5; i++)
            {
                Assert.IsNotNull(test3.ElementAt(i), Messages.ObjectIsNull);
                Assert.AreEqual(destinations[i].DestinationID, test3.ElementAt(i).DestinationID);
                Assert.AreEqual(destinations[i].DestinationName, test3.ElementAt(i).DestinationName);
                Assert.AreEqual(destinations[i].DestinationNameCurrentLanguage, test3.ElementAt(i).DestinationNameCurrentLanguage);
                Assert.AreEqual(destinations[i].DestinationDescriptionCurrentLanguage, test3.ElementAt(i).DestinationDescriptionCurrentLanguage);
                Assert.AreEqual(destinations[i].DestinationDescription, test3.ElementAt(i).DestinationDescription);
                Assert.AreEqual(destinations[i].Description_i18n, test3.ElementAt(i).Description_i18n);
                Assert.AreEqual(destinations[i].Destination_i18n, test3.ElementAt(i).Destination_i18n);
            }
            #endregion

        }

        [TestMethod]
        public void GetMatchesDestinationsTest()
        {
            String[] testNames = new String[] { "aaaBBBCCCddd",
                                                "aaabbbCCCDDD",
                                                "gggHHHoooPPP",
                                                "gggbbbPPPddd",
                                                "aaaaaaaaaaaa" };

            for (int i = 0; i < 5; i++)
            {
                Destination dest = new Destination()
                {
                    DestinationDescriptionCurrentLanguage = "en",
                    DestinationNameCurrentLanguage = "en",
                    Active = true
                };
                dest.Taxes.Add(_testTax);
                dest.SetDestinationDescriptionValue("destinationdescription");
                dest.SetDestinationNameValue(testNames[i]);
                dest.PersistI18nValues();

                _destService.AddDestination(dest);
                _testDestinations.Add(dest);
            }
            _efContext.SaveChanges();

            List<Destination> test1 = _destService.GetMatchesDestinations("aaa").ToList();
            Assert.AreEqual(3, test1.Count());
            Assert.AreEqual(testNames[0], test1.ElementAt(2).DestinationName.ToString());
            Assert.AreEqual(testNames[1], test1.ElementAt(1).DestinationName.ToString());
            Assert.AreEqual(testNames[4], test1.ElementAt(0).DestinationName.ToString());

            List<Destination> test2 = _destService.GetMatchesDestinations("gggbbb").ToList();
            Assert.AreEqual(1, test2.Count());
            Assert.AreEqual(testNames[3], test2.ElementAt(0).DestinationName.ToString());

            List<Destination> test3 = _destService.GetMatchesDestinations("aaaBBBCCC").ToList();
            Assert.AreEqual(2, test3.Count());
            Assert.AreEqual(testNames[0], test3.ElementAt(1).DestinationName.ToString());
            Assert.AreEqual(testNames[1], test3.ElementAt(0).DestinationName.ToString());
        }

        [TestMethod]
        public void AddDestinationTest()
        {
            Destination expected = new Destination()
            {
                DestinationDescriptionCurrentLanguage = "en",
                DestinationNameCurrentLanguage = "en",
                Active = true
            };
            expected.Taxes.Add(_testTax);
            expected.SetDestinationDescriptionValue("destinationdescription");
            expected.SetDestinationNameValue("DestinationName");
            expected.PersistI18nValues();

            _destService.AddDestination(expected);
            _testDestinations.Add(expected);
            _efContext.SaveChanges();

            Destination actual = _destService.GetDestinationById(expected.DestinationID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(expected.DestinationID, actual.DestinationID);
            Assert.AreEqual(expected.DestinationName, actual.DestinationName);
            Assert.AreEqual(expected.DestinationNameCurrentLanguage, actual.DestinationNameCurrentLanguage);
            Assert.AreEqual(expected.DestinationDescriptionCurrentLanguage, actual.DestinationDescriptionCurrentLanguage);
            Assert.AreEqual(expected.DestinationDescription, actual.DestinationDescription);
            Assert.AreEqual(expected.Description_i18n, actual.Description_i18n);
            Assert.AreEqual(expected.Destination_i18n, actual.Destination_i18n);
            Assert.AreEqual(expected.Taxes.First(), actual.Taxes.First());
        }

        [TestMethod]
        public void AddDestinationRequiredFieldsTest()
        {
            #region Test field DestinationDescriptionValue
            Destination dest1 = new Destination()
            {
                DestinationDescriptionCurrentLanguage = "en",
                DestinationNameCurrentLanguage = "en",
                Active = true
            };
            dest1.Taxes.Add(_testTax);
            dest1.SetDestinationDescriptionValue(null);
            dest1.SetDestinationNameValue("DestinationName");
            dest1.PersistI18nValues();

            try
            {
                _destService.AddDestination(dest1);
                _testDestinations.Add(dest1);
                _efContext.SaveChanges();
            }
            catch (Exception)
            {
                Assert.Fail(Messages.UnexpectedException);
            }
            #endregion

            #region Test field DestinationNameValue
            Destination dest2 = new Destination()
            {
                DestinationDescriptionCurrentLanguage = "en",
                DestinationNameCurrentLanguage = "en",
                Active = true
            };
            dest2.Taxes.Add(_testTax);
            dest2.SetDestinationDescriptionValue("DestinationDescription");
            dest2.SetDestinationNameValue(null);
            dest2.PersistI18nValues();

            try
            {
                _destService.AddDestination(dest2);
                _testDestinations.Add(dest2);
                _efContext.SaveChanges();
            }
            catch (Exception)
            {
                Assert.Fail(Messages.UnexpectedException);
            }
            #endregion
        }

        [TestMethod]
        public void DeleteDestinationTest()
        {
            #region Delete successfully
            Destination deleted = new Destination()
            {
                DestinationDescriptionCurrentLanguage = "en",
                DestinationNameCurrentLanguage = "en",
                Active = true
            };
            deleted.Taxes.Add(_testTax);
            deleted.SetDestinationDescriptionValue("destinationdescription");
            deleted.SetDestinationNameValue("DestinationName");
            deleted.PersistI18nValues();

            _destService.AddDestination(deleted);
            _efContext.SaveChanges();

            _taxesService.DeleteTax(_testTax.TaxId);
            _destService.DeleteDestination(deleted.DestinationID);

            Destination test = _destService.GetDestinationById(deleted.DestinationID);

            Assert.IsNull(test, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced destination deletion

            #endregion
        }

        [TestMethod]
        public void HasAnyDependenciesTest()
        {
            #region Test with Tax
            Destination test1 = new Destination()
            {
                DestinationDescriptionCurrentLanguage = "en",
                DestinationNameCurrentLanguage = "en",
                Active = true
            };
            test1.Taxes.Add(_testTax);
            test1.SetDestinationDescriptionValue("destinationdescription");
            test1.SetDestinationNameValue("DestinationName");
            test1.PersistI18nValues();

            _destService.AddDestination(test1);
            _testDestinations.Add(test1);
            _efContext.SaveChanges();

            Assert.AreEqual(true, _destService.HasAnyDependencies(test1.DestinationID));
            #endregion

            #region Test with Property
            Destination test2 = new Destination()
            {
                DestinationDescriptionCurrentLanguage = "en",
                DestinationNameCurrentLanguage = "en",
                Active = true
            };
            test2.SetDestinationDescriptionValue("destinationdescription");
            test2.SetDestinationNameValue("DestinationName");
            test2.PersistI18nValues();

            _destService.AddDestination(test2);
            _testDestinations.Add(test2);
            _efContext.SaveChanges();

            Property prop = PropertiesHelper.generateProperty(_efContext, null, test2);
            _testProperties.Add(prop);
            _efContext.SaveChanges();

            Assert.AreEqual(true, _destService.HasAnyDependencies(test2.DestinationID));
            #endregion

            #region Test with no dependency
            Destination test3 = new Destination()
            {
                DestinationDescriptionCurrentLanguage = "en",
                DestinationNameCurrentLanguage = "en",
                Active = true
            };
            test3.SetDestinationDescriptionValue("destinationdescription");
            test3.SetDestinationNameValue("DestinationName");
            test3.PersistI18nValues();

            _destService.AddDestination(test3);
            _testDestinations.Add(test3);
            _efContext.SaveChanges();

            Assert.AreEqual(false, _destService.HasAnyDependencies(test3.DestinationID));
            #endregion
        }
        #endregion
    }
}
