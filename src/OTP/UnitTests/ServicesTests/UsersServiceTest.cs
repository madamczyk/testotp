﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using DataAccess.CustomObjects;
using BusinessLogic.Services;
using Moq;
using System.Configuration;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class UsersServiceTest
    {
        #region Fields
        /// <summary>
        /// list of units used in test methods
        /// </summary>
        private List<User> _testUsers = new List<User>();
        private List<ZipCodeRange> _testRanges = new List<ZipCodeRange>();
        private List<Reservation> _testReservations = new List<Reservation>();
        private List<GuestPaymentMethod> _testPaymentMethods = new List<GuestPaymentMethod>();

        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;

        /// <summary>
        /// test objects used in methods
        /// </summary>
        private Destination _testDestination;
        private DictionaryCountry _testCountry;
        private PropertyType _testPropertyType;
        private Role _testRole;
        private DictionaryCulture _testCulture;
        private Property _testProperty;
        private Unit _testUnit;
        private UnitType _testUnitType;
        #endregion

        #region Services

        private IPropertiesService _propertiesService;
        private IDestinationsService _destinationService;
        private IDictionaryCountryService _countryService;
        private IPropertyTypesService _propertyTypeService;
        private IUserService _userService;
        private IRolesService _rolesService;
        private IConfigurationService _configurationService;
        private IBlobService _blobService;
        private IDictionaryCultureService _dictionaryCultureService;
        private IBookingService _bookingService = null;
        private IReservationsService _reservationsService = null;
        private IUnitTypesService _unitTypesService = null;
        private IUnitsService _unitService = null;
        private IZohoCrmService _zohoCrmService = null;
        private ICrmOperationLogsService _crmOperationLogsService = null;
        private ISettingsService _settingsService = null;

        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IMessageService messageService = new MessageService(mock.Object, _settingsService);

            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _blobService = new BlobService(mock.Object);
            _propertiesService = new PropertiesServices(_configurationService, _blobService, _settingsService, mock.Object);
            _destinationService = new DestinationsService(_configurationService, mock.Object);
            _propertyTypeService = new PropertyTypeService(mock.Object);
            _countryService = new DictionaryCountryService(mock.Object);
            _userService = new UsersService(mock.Object, _zohoCrmService, messageService, _settingsService);
            _rolesService = new RolesService(mock.Object);
            _configurationService = new ConfigurationService(mock.Object);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);

            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            var authenticationService = new Mock<IAuthorizationService>();

            authenticationService.Setup(f => f.SingOutUser()).Callback(() => { });

            _propertiesService = new PropertiesServices(configurationService, _blobService, _settingsService, mock.Object);
            var _propertyAddOnsService = new PropertyAddOnsService(mock.Object);
            var _taxesService = new TaxesService(mock.Object);
            var _logService = new EventLogService(mock.Object);
            var _messageService = new MessageService(mock.Object, _settingsService);
            var _stateService = new Mock<IStateService>();

            _unitTypesService = new UnitTypesService(mock.Object);
            var _paymentGatewayService = new PaymentGatewayService(mock.Object, _logService, _settingsService, _messageService);
            _unitService = new UnitsService(mock.Object, _stateService.Object, _unitTypesService);
            var _documentService = new PdfDocumentService(mock.Object, _settingsService); ;

            _bookingService = new BookingService(mock.Object, _propertiesService, _propertyAddOnsService, _taxesService,
                                    _unitService, _userService, _stateService.Object, _logService, _settingsService,
                                    _messageService, _paymentGatewayService, _unitTypesService, authenticationService.Object);
            _reservationsService = new ReservationsService(configurationService, _blobService, mock.Object, _propertiesService, _bookingService, _paymentGatewayService, _documentService, _messageService, _settingsService, _unitService);
            #endregion

            #region Setup data

            _testCountry = _countryService.GetCountries().First();
            _testDestination = _destinationService.GetDestinations().First();
            _testPropertyType = _propertyTypeService.GetPropertyTypes().First();
            _testRole = _rolesService.GetRoles().First();
            _testCulture = _dictionaryCultureService.GetCultures().First();

            User _testUser = new User()
            {
                Lastname = "BookingServiceTest_Lastname",
                Firstname = "BookingServiceTest_Firstname",
                Address1 = "BookingServiceTest_Address1",
                Address2 = "BookingServiceTest_Address2",
                State = "BookingServiceTest_State",
                ZIPCode = "BookingServiceTest_ZIPCode",
                City = "BookingServiceTest_City",
                DictionaryCountry = _testCountry,
                CellPhone = "BookingServiceTest_CellPhone",
                LandLine = "BookingServiceTest_LandLine",
                email = "BookingServiceTest_email",
                DriverLicenseNbr = "BookingServiceTest_DriverLicenseNbr",
                PassportNbr = "BookingServiceTest_PassportNbr",
                SocialsecurityNbr = "BookingServiceTest_SocialsecurityNbr",
                DirectDepositInfo = "BookingServiceTest_DirectDepositInfo",
                SecurityQuestion = "BookingServiceTest_SecurityQuestion",
                SecurityAnswer = "BookingServiceTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "BookingServiceTest_AcceptedTCsInitials",
                Password = "BookingServiceTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(_testUser);
            _testUsers.Add(_testUser);

            _testProperty = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "UsersServiceTest_PropertyCode",
                Address1 = "UsersServiceTest_Address1",
                Address2 = "UsersServiceTest_Address2",
                State = "UsersServiceTest_State",
                ZIPCode = "UsersServiceTest_ZipCode",
                City = "UsersServiceTest_City",
                DictionaryCountry = _testCountry,
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                PropertyLive = true,
                PropertyStaticContents = new List<DataAccess.PropertyStaticContent>(),
                TimeZone = "TZ"
            };

            _testProperty.PropertyStaticContents.Add(new PropertyStaticContent() { ContentCode = 15, ContentValue = "<i18nString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><i18nContent><i18nPair><Code>en</Code><Content>iiii</Content></i18nPair></i18nContent></i18nString>" });
            _testProperty.SetPropertyNameValue("UsersServiceTest_PropertyName");
            _testProperty.SetShortDescriptionValue("UsersServiceTest_ShortDescription");
            _testProperty.SetLongDescriptionValue("UsersServiceTest_LongDescription");
            _propertiesService.AddProperty(_testProperty);

            _testUnitType = new UnitType()
            {
                UnitTypeCode = "UsersServiceTest_UnitTypeCode",
                Property = _testProperty
            };
            _testUnitType.SetTitleValue("UsersServiceTest_UnitTypeTitle");
            _testUnitType.SetDescValue("UsersServiceTest_UnitTypeDesc");
            _unitTypesService.AddUnitType(_testUnitType);

            _testUnit = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                UnitCode = "UsersServiceTest_UnitCode"
            };
            _testUnit.SetTitleValue("UsersServiceTest_UnitTypeTitle");
            _testUnit.SetDescValue("UsersServiceTest_UnitTypeDesc");
            _unitService.AddUnit(_testUnit);



            _efContext.SaveChanges();
            #endregion
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (Reservation r in _testReservations)
            {
                if (_reservationsService.GetReservationById(r.ReservationID) != null)
                    DataAccessHelper.RemoveReservation(_efContext, r);
            }
            _testReservations.Clear();

            _unitService.DeleteUnit(_testUnit.UnitID);
            _unitTypesService.DeleteUnitType(_testUnitType.UnitTypeID);
            _propertiesService.RemoveProperty(_testProperty.PropertyID);

            foreach (GuestPaymentMethod gpm in _testPaymentMethods)
            {
                if (DataAccessHelper.GetGuestPaymentMethod(_efContext, gpm.GuestPaymentMethodID) != null)
                    _userService.RemoveGuestPaymentMethod(gpm.GuestPaymentMethodID, _testUsers.First().UserID);
            }
            _testPaymentMethods.Clear();

            foreach (User u in _testUsers)
            {
                if (_userService.GetUserByUserId(u.UserID) != null)
                    _userService.DeleteUserById(u.UserID);
            }
            _testUsers.Clear();

            foreach (ZipCodeRange zcr in _testRanges)
            {
                if (_userService.GetZipCodeRangeById(zcr.ZipCodeRangeId) != null)
                    _userService.DeleteZipCodeRangeById(zcr.ZipCodeRangeId);
            }
            _testRanges.Clear();


        }
        #endregion

        #region Test methods
        [TestMethod]
        public void GetUsersTest()
        {
            User[] newUsers = new User[5];
            Dictionary<int, bool> newUsersFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newUsers[i] = new User()
                {
                    Lastname = "GetUsersTest_Lastname_" + i,
                    Firstname = "GetUsersTest_Firstname_" + i,
                    Address1 = "GetUsersTest_Address1_" + i,
                    Address2 = "GetUsersTest_Address2_" + i,
                    State = "GetUsersTest_State_" + i,
                    ZIPCode = "GetUsersTest_ZIPCode_" + i,
                    City = "GetUsersTest_City_" + i,
                    DictionaryCountry = _testCountry,
                    CellPhone = "GetUsersTest_CellPhone_" + i,
                    LandLine = "GetUsersTest_LandLine_" + i,
                    email = "GetUsersTest_email_" + i,
                    DriverLicenseNbr = "GetUsersTest_DriverLicenseNbr_" + i,
                    PassportNbr = "GetUsersTest_PassportNbr_" + i,
                    SocialsecurityNbr = "GetUsersTest_SocialsecurityNbr_" + i,
                    DirectDepositInfo = "GetUsersTest_DirectDepositInfo_" + i,
                    SecurityQuestion = "GetUsersTest_SecurityQuestion_" + i,
                    SecurityAnswer = "GetUsersTest_SecurityAnswer_" + i,
                    AcceptedTCs = DateTime.Now,
                    AcceptedTCsInitials = "GetUsersTest_AcceptedTCsInitials_" + i,
                    Password = "GetUsersTest_Password_" + i,
                    IsGeneratedPassword = true,
                    VerificationPositive = true,
                    SendMePromotions = true,
                    SendInfoFavoritePlaces = true,
                    SendNews = true,
                    Gender = "m",
                    DictionaryCulture = _testCulture
                };

                _userService.AddUser(newUsers[i]);
                _efContext.SaveChanges();

                newUsersFound.Add(newUsers[i].UserID, false);

                _testUsers.Add(newUsers[i]);
            }

            User expected;
            var actual = _userService.GetUsers();
            foreach (var actualObj in actual)
            {
                if (newUsersFound.ContainsKey(actualObj.UserID))
                {
                    expected = newUsers.Where(u => u.UserID == actualObj.UserID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newUsersFound[actualObj.UserID] = true;
                }
            }

            if (newUsersFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "users", "GetUsers"));
        }

        [TestMethod]
        public void GetUserByEmailTest()
        {
            User expected = new User()
            {
                Lastname = "GetUserByEmailTest_Lastname",
                Firstname = "GetUserByEmailTest_Firstname",
                Address1 = "GetUserByEmailTest_Address1",
                Address2 = "GetUserByEmailTest_Address2",
                State = "GetUserByEmailTest_State",
                ZIPCode = "GetUserByEmailTest_ZIPCode",
                City = "GetUserByEmailTest_City",
                DictionaryCountry = _testCountry,
                CellPhone = "GetUserByEmailTest_CellPhone",
                LandLine = "GetUserByEmailTest_LandLine",
                email = "GetUserByEmailTest_email",
                DriverLicenseNbr = "GetUserByEmailTest_DriverLicenseNbr",
                PassportNbr = "GetUserByEmailTest_PassportNbr",
                SocialsecurityNbr = "GetUserByEmailTest_SocialsecurityNbr",
                DirectDepositInfo = "GetUserByEmailTest_DirectDepositInfo",
                SecurityQuestion = "GetUserByEmailTest_SecurityQuestion",
                SecurityAnswer = "GetUserByEmailTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "GetUserByEmailTest_AcceptedTCsInitials",
                Password = "GetUserByEmailTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                DictionaryCulture = _testCulture
            };

            _userService.AddUser(expected);
            _efContext.SaveChanges();

            _testUsers.Add(expected);

            var actual = _userService.GetUserByEmail(expected.email);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetUsersByRoleTest()
        {
            UserRole tmpUserRole;
            User[] newUsers = new User[5];
            Dictionary<int, bool> newUsersFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newUsers[i] = new User()
                {
                    Lastname = "GetUsersByRoleTest_Lastname_" + i,
                    Firstname = "GetUsersByRoleTest_Firstname_" + i,
                    Address1 = "GetUsersByRoleTest_Address1_" + i,
                    Address2 = "GetUsersByRoleTest_Address2_" + i,
                    State = "GetUsersByRoleTest_State_" + i,
                    ZIPCode = "GetUsersByRoleTest_ZIPCode_" + i,
                    City = "GetUsersByRoleTest_City_" + i,
                    DictionaryCountry = _testCountry,
                    CellPhone = "GetUsersByRoleTest_CellPhone_" + i,
                    LandLine = "GetUsersByRoleTest_LandLine_" + i,
                    email = "GetUsersByRoleTest_email_" + i,
                    DriverLicenseNbr = "GetUsersByRoleTest_DriverLicenseNbr_" + i,
                    PassportNbr = "GetUsersByRoleTest_PassportNbr_" + i,
                    SocialsecurityNbr = "GetUsersByRoleTest_SocialsecurityNbr_" + i,
                    DirectDepositInfo = "GetUsersByRoleTest_DirectDepositInfo_" + i,
                    SecurityQuestion = "GetUsersByRoleTest_SecurityQuestion_" + i,
                    SecurityAnswer = "GetUsersByRoleTest_SecurityAnswer_" + i,
                    AcceptedTCs = DateTime.Now,
                    AcceptedTCsInitials = "GetUsersByRoleTest_AcceptedTCsInitials_" + i,
                    Password = "GetUsersByRoleTest_Password_" + i,
                    IsGeneratedPassword = true,
                    VerificationPositive = true,
                    SendMePromotions = true,
                    SendInfoFavoritePlaces = true,
                    SendNews = true,
                    Gender = "m",
                    DictionaryCulture = _testCulture
                };
                _userService.AddUser(newUsers[i]);

                tmpUserRole = _rolesService.AddUserToRole(newUsers[i], (RoleLevel)_testRole.RoleLevel);
                Assert.IsNotNull(tmpUserRole, Messages.ObjectIsNull);

                newUsers[i].UserRoles.Add(tmpUserRole);

                _efContext.SaveChanges();

                newUsersFound.Add(newUsers[i].UserID, false);

                _testUsers.Add(newUsers[i]);
            }

            User expected;
            var actual = _userService.GetUsersByRole((RoleLevel)_testRole.RoleLevel);
            foreach (var actualObj in actual)
            {
                if (newUsersFound.ContainsKey(actualObj.UserID))
                {
                    expected = newUsers.Where(u => u.UserID == actualObj.UserID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newUsersFound[actualObj.UserID] = true;
                }
            }

            if (newUsersFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "users", "GetUsersByRole"));
        }

        [TestMethod]
        public void IsPasswordValidTest()
        {
            string password;
            bool isValid;

            #region Test 1
            password = "";
            isValid = _userService.IsPasswordValid(password);
            Assert.IsFalse(isValid);
            #endregion

            #region Test 2
            password = "   ";
            isValid = _userService.IsPasswordValid(password);
            Assert.IsFalse(isValid);
            #endregion

            #region Test 3
            password = "abcde";
            isValid = _userService.IsPasswordValid(password);
            Assert.IsFalse(isValid);
            #endregion

            #region Test 4
            password = "a#bcd!e";
            isValid = _userService.IsPasswordValid(password);
            Assert.IsFalse(isValid);
            #endregion

            #region Test 5
            password = "a#bC@d!e&";
            isValid = _userService.IsPasswordValid(password);
            Assert.IsTrue(isValid);
            #endregion

            // TODO : add some more tests
        }

        [TestMethod]
        public void AddUserAndGetUserByUserId()
        {
            User expected = new User()
            {
                Lastname = "AddUserAndGetUserByUserId_Lastname",
                Firstname = "AddUserAndGetUserByUserId_Firstname",
                Address1 = "AddUserAndGetUserByUserId_Address1",
                Address2 = "AddUserAndGetUserByUserId_Address2",
                State = "AddUserAndGetUserByUserId_State",
                ZIPCode = "AddUserAndGetUserByUserId_ZIPCode",
                City = "AddUserAndGetUserByUserId_City",
                DictionaryCountry = _testCountry,
                CellPhone = "AddUserAndGetUserByUserId_CellPhone",
                LandLine = "AddUserAndGetUserByUserId_LandLine",
                email = "AddUserAndGetUserByUserId_email",
                DriverLicenseNbr = "AddUserAndGetUserByUserId_DriverLicenseNbr",
                PassportNbr = "AddUserAndGetUserByUserId_PassportNbr",
                SocialsecurityNbr = "AddUserAndGetUserByUserId_SocialsecurityNbr",
                DirectDepositInfo = "AddUserAndGetUserByUserId_DirectDepositInfo",
                SecurityQuestion = "AddUserAndGetUserByUserId_SecurityQuestion",
                SecurityAnswer = "AddUserAndGetUserByUserId_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "AddUserAndGetUserByUserId_AcceptedTCsInitials",
                Password = "AddUserAndGetUserByUserId_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                DictionaryCulture = _testCulture
            };

            _userService.AddUser(expected);
            _efContext.SaveChanges();

            _testUsers.Add(expected);

            var actual = _userService.GetUserByUserId(expected.UserID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddUserRequiredFieldsTest()
        {
            User user;

            #region Test field Lastname
            user = new User()
            {
                Firstname = "AddUserRequiredFieldsTest_Firstname_Lastname",
                Address1 = "AddUserRequiredFieldsTest_Address1_Lastname",
                Address2 = "AddUserRequiredFieldsTest_Address2_Lastname",
                State = "AddUserRequiredFieldsTest_State_Lastname",
                ZIPCode = "AddUserRequiredFieldsTest_ZIPCode_Lastname",
                City = "AddUserRequiredFieldsTest_City_Lastname",
                DictionaryCountry = _testCountry,
                CellPhone = "AddUserRequiredFieldsTest_CellPhone_Lastname",
                LandLine = "AddUserRequiredFieldsTest_LandLine_Lastname",
                email = "AddUserRequiredFieldsTest_email_Lastname",
                DriverLicenseNbr = "AddUserRequiredFieldsTest_DriverLicenseNbr_Lastname",
                PassportNbr = "AddUserRequiredFieldsTest_PassportNbr_Lastname",
                SocialsecurityNbr = "AddUserRequiredFieldsTest_SocialsecurityNbr_Lastname",
                DirectDepositInfo = "AddUserRequiredFieldsTest_DirectDepositInfo_Lastname",
                SecurityQuestion = "AddUserRequiredFieldsTest_SecurityQuestion_Lastname",
                SecurityAnswer = "AddUserRequiredFieldsTest_SecurityAnswer_Lastname",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "AURFT_AcceptedTCsInitials_Lastname",
                Password = "AddUserRequiredFieldsTest_Password_Lastname",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Now,
                BankName = "AddUserRequiredFieldsTest_BankName_Lastname",
                RoutingNumber = "AddUserRequiredFieldsTest_RoutingNumber_Lastname",
                AccountNumber = "AddUserRequiredFieldsTest_AccountNumber_Lastname",
                DictionaryCulture = _testCulture
            };

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _userService.AddUser(user);
			}, "Lastname");
            _efContext.Users.Remove(user);
            _efContext.SaveChanges();
            #endregion

            #region Test field Firstname
            user = new User()
            {
                Lastname = "AddUserRequiredFieldsTest_Lastname_Firstname",
                Address1 = "AddUserRequiredFieldsTest_Address1_Firstname",
                Address2 = "AddUserRequiredFieldsTest_Address2_Firstname",
                State = "AddUserRequiredFieldsTest_State_Firstname",
                ZIPCode = "AddUserRequiredFieldsTest_ZIPCode_Firstname",
                City = "AddUserRequiredFieldsTest_City_Firstname",
                DictionaryCountry = _testCountry,
                CellPhone = "AddUserRequiredFieldsTest_CellPhone_Firstname",
                LandLine = "AddUserRequiredFieldsTest_LandLine_Firstname",
                email = "AddUserRequiredFieldsTest_email_Firstname",
                DriverLicenseNbr = "AddUserRequiredFieldsTest_DriverLicenseNbr_Firstname",
                PassportNbr = "AddUserRequiredFieldsTest_PassportNbr_Firstname",
                SocialsecurityNbr = "AddUserRequiredFieldsTest_SocialsecurityNbr_Firstname",
                DirectDepositInfo = "AddUserRequiredFieldsTest_DirectDepositInfo_Firstname",
                SecurityQuestion = "AddUserRequiredFieldsTest_SecurityQuestion_Firstname",
                SecurityAnswer = "AddUserRequiredFieldsTest_SecurityAnswer_Firstname",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "AURFT_AcceptedTCsInitials_Firstname",
                Password = "AddUserRequiredFieldsTest_Password_Firstname",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Now,
                BankName = "AddUserRequiredFieldsTest_BankName_Firstname",
                RoutingNumber = "AddUserRequiredFieldsTest_RoutingNumber_Firstname",
                AccountNumber = "AddUserRequiredFieldsTest_AccountNumber_Firstname",
                DictionaryCulture = _testCulture
            };

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _userService.AddUser(user);
			}, "Firstname");
            _efContext.Users.Remove(user);
            _efContext.SaveChanges();
            #endregion

            #region Test field Address1
            // Address1 is nullable
            #endregion

            #region Test field Address2
            // Address2 is nullable
            #endregion

            #region Test field State
            // State is nullable
            #endregion

            #region Test field ZIPCode
            // ZIPCode is nullable
            #endregion

            #region Test field City
            // City is nullable
            #endregion

            #region Test field CountryId
            user = new User()
            {
                Lastname = "AddUserRequiredFieldsTest_Lastname_CountryId",
                Firstname = "AddUserRequiredFieldsTest_Firstname_CountryId",
                Address1 = "AddUserRequiredFieldsTest_Address1_CountryId",
                Address2 = "AddUserRequiredFieldsTest_Address2_CountryId",
                State = "AddUserRequiredFieldsTest_State_CountryId",
                ZIPCode = "AddUserRequiredFieldsTest_ZIPCode_CountryId",
                City = "AddUserRequiredFieldsTest_City_CountryId",
                CellPhone = "AddUserRequiredFieldsTest_CellPhone_CountryId",
                LandLine = "AddUserRequiredFieldsTest_LandLine_CountryId",
                email = "AddUserRequiredFieldsTest_email_CountryId",
                DriverLicenseNbr = "AddUserRequiredFieldsTest_DriverLicenseNbr_CountryId",
                PassportNbr = "AddUserRequiredFieldsTest_PassportNbr_CountryId",
                SocialsecurityNbr = "AddUserRequiredFieldsTest_SocialsecurityNbr_CountryId",
                DirectDepositInfo = "AddUserRequiredFieldsTest_DirectDepositInfo_CountryId",
                SecurityQuestion = "AddUserRequiredFieldsTest_SecurityQuestion_CountryId",
                SecurityAnswer = "AddUserRequiredFieldsTest_SecurityAnswer_CountryId",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "AURFT_AcceptedTCsInitials_CountryId",
                Password = "AddUserRequiredFieldsTest_Password_CountryId",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Now,
                BankName = "AddUserRequiredFieldsTest_BankName_CountryId",
                RoutingNumber = "AddUserRequiredFieldsTest_RoutingNumber_CountryId",
                AccountNumber = "AddUserRequiredFieldsTest_AccountNumber_CountryId",
                DictionaryCulture = _testCulture
            };

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _userService.AddUser(user);
			}, "FK_Users_DictionaryCountries");
            _efContext.Users.Remove(user);
            _efContext.SaveChanges();
            #endregion

            #region Test field CellPhone
            // CellPhone is nullable
            #endregion

            #region Test field LandLine
            // LandLine is nullable
            #endregion

            #region Test field email
            user = new User()
            {
                Lastname = "AddUserRequiredFieldsTest_Lastname_email",
                Firstname = "AddUserRequiredFieldsTest_Firstname_email",
                Address1 = "AddUserRequiredFieldsTest_Address1_email",
                Address2 = "AddUserRequiredFieldsTest_Address2_email",
                State = "AddUserRequiredFieldsTest_State_email",
                ZIPCode = "AddUserRequiredFieldsTest_ZIPCode_email",
                City = "AddUserRequiredFieldsTest_City_email",
                DictionaryCountry = _testCountry,
                CellPhone = "AddUserRequiredFieldsTest_CellPhone_email",
                LandLine = "AddUserRequiredFieldsTest_LandLine_email",
                DriverLicenseNbr = "AddUserRequiredFieldsTest_DriverLicenseNbr_email",
                PassportNbr = "AddUserRequiredFieldsTest_PassportNbr_email",
                SocialsecurityNbr = "AddUserRequiredFieldsTest_SocialsecurityNbr_email",
                DirectDepositInfo = "AddUserRequiredFieldsTest_DirectDepositInfo_email",
                SecurityQuestion = "AddUserRequiredFieldsTest_SecurityQuestion_email",
                SecurityAnswer = "AddUserRequiredFieldsTest_SecurityAnswer_email",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "AURFT_AcceptedTCsInitials_email",
                Password = "AddUserRequiredFieldsTest_Password_email",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Now,
                BankName = "AddUserRequiredFieldsTest_BankName_email",
                RoutingNumber = "AddUserRequiredFieldsTest_RoutingNumber_email",
                AccountNumber = "AddUserRequiredFieldsTest_AccountNumber_email",
                DictionaryCulture = _testCulture
            };

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _userService.AddUser(user);
			}, "email");
            _efContext.Users.Remove(user);
            _efContext.SaveChanges();
            #endregion

            #region Test field DriverLicenseNbr
            // DriverLicenseNbr is nullable
            #endregion

            #region Test field PassportNbr
            // PassportNbr is nullable
            #endregion

            #region Test field SocialsecurityNbr
            // SocialsecurityNbr is nullable
            #endregion

            #region Test field DirectDepositInfo
            // DirectDepositInfo is nullable
            #endregion

            #region Test field SecurityQuestion
            // SecurityQuestion is nullable
            #endregion

            #region Test field SecurityAnswer
            // SecurityAnswer is nullable
            #endregion

            #region Test field LanguageId
            user = new User()
            {
                Lastname = "AddUserRequiredFieldsTest_Lastname_LanguageId",
                Firstname = "AddUserRequiredFieldsTest_Firstname_LanguageId",
                Address1 = "AddUserRequiredFieldsTest_Address1_LanguageId",
                Address2 = "AddUserRequiredFieldsTest_Address2_LanguageId",
                State = "AddUserRequiredFieldsTest_State_LanguageId",
                ZIPCode = "AddUserRequiredFieldsTest_ZIPCode_LanguageId",
                City = "AddUserRequiredFieldsTest_City_LanguageId",
                CellPhone = "AddUserRequiredFieldsTest_CellPhone_LanguageId",
                LandLine = "AddUserRequiredFieldsTest_LandLine_LanguageId",
                email = "AddUserRequiredFieldsTest_email_LanguageId",
                DriverLicenseNbr = "AddUserRequiredFieldsTest_DriverLicenseNbr_LanguageId",
                PassportNbr = "AddUserRequiredFieldsTest_PassportNbr_LanguageId",
                SocialsecurityNbr = "AddUserRequiredFieldsTest_SocialsecurityNbr_LanguageId",
                DirectDepositInfo = "AddUserRequiredFieldsTest_DirectDepositInfo_LanguageId",
                SecurityQuestion = "AddUserRequiredFieldsTest_SecurityQuestion_LanguageId",
                SecurityAnswer = "AddUserRequiredFieldsTest_SecurityAnswer_LanguageId",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "AURFT_AcceptedTCsInitials_LanguageId",
                Password = "AddUserRequiredFieldsTest_Password_LanguageId",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Now,
                BankName = "AddUserRequiredFieldsTest_BankName_LanguageId",
                RoutingNumber = "AddUserRequiredFieldsTest_RoutingNumber_LanguageId",
                AccountNumber = "AddUserRequiredFieldsTest_AccountNumber_LanguageId",
                DictionaryCulture = _testCulture
            };

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _userService.AddUser(user);
			}, "FK_Guests_DictionaryLanguages");
            _efContext.Users.Remove(user);
            _efContext.SaveChanges();
            #endregion

            #region Test field AcceptedTCs
            // AcceptedTCs is of type DateTime and cannot be nullified
            #endregion

            #region Test field AcceptedTCsInitials
            // AcceptedTCsInitials is nullable
            #endregion

            #region Test field Password
            user = new User()
            {
                Lastname = "AddUserRequiredFieldsTest_Lastname_Password",
                Firstname = "AddUserRequiredFieldsTest_Firstname_Password",
                Address1 = "AddUserRequiredFieldsTest_Address1_Password",
                Address2 = "AddUserRequiredFieldsTest_Address2_Password",
                State = "AddUserRequiredFieldsTest_State_Password",
                ZIPCode = "AddUserRequiredFieldsTest_ZIPCode_Password",
                City = "AddUserRequiredFieldsTest_City_Password",
                CellPhone = "AddUserRequiredFieldsTest_CellPhone_Password",
                LandLine = "AddUserRequiredFieldsTest_LandLine_Password",
                email = "AddUserRequiredFieldsTest_email_Password",
                DriverLicenseNbr = "AddUserRequiredFieldsTest_DriverLicenseNbr_Password",
                PassportNbr = "AddUserRequiredFieldsTest_PassportNbr_Password",
                SocialsecurityNbr = "AddUserRequiredFieldsTest_SocialsecurityNbr_Password",
                DirectDepositInfo = "AddUserRequiredFieldsTest_DirectDepositInfo_Password",
                SecurityQuestion = "AddUserRequiredFieldsTest_SecurityQuestion_Password",
                SecurityAnswer = "AddUserRequiredFieldsTest_SecurityAnswer_Password",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "AURFT_AcceptedTCsInitials_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Now,
                BankName = "AddUserRequiredFieldsTest_BankName_Password",
                RoutingNumber = "AddUserRequiredFieldsTest_RoutingNumber_Password",
                AccountNumber = "AddUserRequiredFieldsTest_AccountNumber_Password",
                DictionaryCulture = _testCulture
            };

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _userService.AddUser(user);
			}, "Password");
            _efContext.Users.Remove(user);
            _efContext.SaveChanges();
            #endregion

            #region Test field IsGeneratedPassword
            // IsGeneratedPassword is of type bool and cannot be nullified
            #endregion

            #region Test field VerificationPositive
            // VerificationPositive is nullable
            #endregion

            #region Test field SendMePromotions
            // SendMePromotions is nullable
            #endregion

            #region Test field SendInfoFavoritePlaces
            // SendInfoFavoritePlaces is nullable
            #endregion

            #region Test field SendNews
            // SendNews is nullable
            #endregion

            #region Test field Gender
            // Gender is nullable
            #endregion

            #region Test field BirthDate
            // Gender is nullable
            #endregion

            #region Test field BankName
            // Gender is nullable
            #endregion

            #region Test field RoutingNumber
            // Gender is nullable
            #endregion

            #region Test field AccountNumber
            // Gender is nullable
            #endregion
        }

        [TestMethod]
        public void ChangeUserPassword()
        {
            string userPassword = "ChangeUserPassword_Password";

            User user = new User()
            {
                Lastname = "ChangeUserPassword_Lastname",
                Firstname = "ChangeUserPassword_Firstname",
                email = "ChangeUserPassword_Email",
                Password = Common.MD5Helper.Encode(userPassword),
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };

            _testUsers.Add(user);
            _userService.AddUser(user);
            _efContext.SaveChanges();

            var dbUser = _userService.GetUserByUserId(user.UserID);
            Assert.IsNotNull(dbUser);

            string newPassword = "ChangeUserPassword_NewPassword";
            string hashedNewPassword = Common.MD5Helper.Encode(newPassword);

            try
            {
                _userService.ChangePassword(dbUser.UserID, userPassword, newPassword);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

            dbUser = _userService.GetUserByUserId(user.UserID);
            Assert.IsNotNull(dbUser);

            Assert.AreEqual(dbUser.Password, hashedNewPassword);
        }

        [TestMethod]
        public void GetUserRoleByActivationTokenTest()
        {
            UserRole expected;
            User user = new User()
            {
                Lastname = "GetUserRoleByActivationTokenTest_Lastname",
                Firstname = "GetUserRoleByActivationTokenTest_Firstname",
                email = "GetUserRoleByActivationTokenTest_email",
                Password = "GetUserRoleByActivationTokenTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(user);

            expected = _rolesService.AddUserToRole(user, (RoleLevel)_testRole.RoleLevel);
            Assert.IsNotNull(expected, Messages.ObjectIsNull);

            expected.ActivationToken = Guid.NewGuid().ToString();
            expected.Status = 666;
            user.UserRoles.Add(expected);

            _efContext.SaveChanges();

            _testUsers.Add(user);

            var actual = _userService.GetUserRoleByActivationToken(expected.ActivationToken);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void HasAnyDependenciesTest()
        {
            bool hasAnyDependencies;

            User user = new User()
            {
                Lastname = "HasAnyDependenciesTest_Lastname",
                Firstname = "HasAnyDependenciesTest_Firstname",
                email = "HasAnyDependenciesTest_email",
                Password = "HasAnyDependenciesTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(user);
            _efContext.SaveChanges();

            _testUsers.Add(user);

            #region No dependencies
            hasAnyDependencies = _userService.HasAnyDependencies(user.UserID);

            Assert.IsFalse(hasAnyDependencies);
            #endregion

            #region Property dependency
            Property property = new Property()
            {
                User = user,
                Destination = _testDestination,
                PropertyCode = "HasAnyDependenciesTest_PropertyCode",
                Address1 = "HasAnyDependenciesTest_Address1",
                State = "HasAnyDependenciesTest_State",
                ZIPCode = "HasAnyDependenciesTest_ZipCode",
                City = "HasAnyDependenciesTest_City",
                DictionaryCountry = _testCountry,
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "TZ"
            };
            property.SetPropertyNameValue("HasAnyDependenciesTest_PropertyName");
            property.SetShortDescriptionValue("HasAnyDependenciesTest_ShortDescription");
            property.SetLongDescriptionValue("HasAnyDependenciesTest_LongDescription");

            _propertiesService.AddProperty(property);
            _efContext.SaveChanges();

            hasAnyDependencies = _userService.HasAnyDependencies(user.UserID);

            Assert.IsTrue(hasAnyDependencies);
            #endregion

            _propertiesService.RemoveProperty(property.PropertyID);
        }

        [TestMethod]
        public void DeleteUserByIdTest()
        {
            #region Delete successfully
            User deleted = new User()
            {
                Lastname = "DeleteUserByIdTest_Lastname_Successfully",
                Firstname = "DeleteUserByIdTest_Firstname_Successfully",
                email = "DeleteUserByIdTest_email_Successfully",
                Password = "DeleteUserByIdTest_Password_Successfully",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(deleted);
            _efContext.SaveChanges();

            _userService.DeleteUserById(deleted.UserID);

            var dbUser = _userService.GetUserByUserId(deleted.UserID);

            Assert.IsNull(dbUser, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced user deletion
            // TODO : check when user is referenced by GuestReview
            // TODO : check when user is referenced by Reservation
            // TODO : check when user is referenced by ReservationBillingAddress
            User referencedUser = new User()
            {
                Lastname = "DeleteUserByIdTest_Lastname_Referenced",
                Firstname = "DeleteUserByIdTest_Firstname_Referenced",
                email = "DeleteUserByIdTest_email_Referenced",
                Password = "DeleteUserByIdTest_Password_Referenced",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(referencedUser);

            Property _refencedProperty = new Property()
            {
                User = referencedUser,
                Destination = _testDestination,
                PropertyCode = "DeleteUserByIdTest_PropertyCode_Referenced",
                Address1 = "DeleteUserByIdTest_Address1_Referenced",
                State = "DeleteUserByIdTest_State_Referenced",
                ZIPCode = "DeleteUserByIdTest_ZipCode_Referenced",
                City = "DeleteUserByIdTest_City_Referenced",
                DictionaryCountry = _testCountry,
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "TZ"
            };
            _refencedProperty.SetPropertyNameValue("DeleteUserByIdTest_PropertyName_Referenced");
            _refencedProperty.SetShortDescriptionValue("DeleteUserByIdTest_ShortDescription_Referenced");
            _refencedProperty.SetLongDescriptionValue("DeleteUserByIdTest_LongDescription_Referenced");

            _propertiesService.AddProperty(_refencedProperty);
            _efContext.SaveChanges();

            bool exceptionCaught = false;
            try
            {
                _userService.DeleteUserById(referencedUser.UserID);
            }
            catch (Exception)
            {
                exceptionCaught = true;
            }

            _propertiesService.RemoveProperty(_refencedProperty.PropertyID);

            if (!exceptionCaught)
                Assert.Fail(Messages.DeletionShouldNotSucceed);
            #endregion
        }

        [TestMethod]
        public void DeleteUserByRoleIdTest()
        {
            #region Delete successfully
            UserRole deletedUserRole;
            User deletedUser = new User()
            {
                Lastname = "DeleteUserByRoleIdTest_Lastname_Successfully",
                Firstname = "DeleteUserByRoleIdTest_Firstname_Successfully",
                email = "DeleteUserByRoleIdTest_email_Successfully",
                Password = "DeleteUserByRoleIdTest_Password_Successfully",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(deletedUser);

            deletedUserRole = _rolesService.AddUserToRole(deletedUser, (RoleLevel)_testRole.RoleLevel);
            Assert.IsNotNull(deletedUserRole, Messages.ObjectIsNull);

            deletedUserRole.ActivationToken = Guid.NewGuid().ToString();
            deletedUserRole.Status = 666;
            deletedUser.UserRoles.Add(deletedUserRole);

            _efContext.SaveChanges();

            _userService.DeleteUserByRoleId(deletedUserRole.UserRoleId);

            var dbUser = _userService.GetUserByUserId(deletedUser.UserID);

            Assert.IsNull(dbUser, Messages.ObjectIsNotNull);
            #endregion
            
            #region Check referenced user deletion
            // TODO : check when user is referenced by GuestReview
            // TODO : check when user is referenced by Reservation
            // TODO : check when user is referenced by ReservationBillingAddress
            UserRole referencedUserRole;
            User referencedUser = new User()
            {
                Lastname = "DeleteUserByRoleIdTest_Lastname_Referenced",
                Firstname = "DeleteUserByRoleIdTest_Firstname_Referenced",
                email = "DeleteUserByRoleIdTest_email_Referenced",
                Password = "DeleteUserByRoleIdTest_Password_Referenced",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(referencedUser);

            referencedUserRole = _rolesService.AddUserToRole(referencedUser, (RoleLevel)_testRole.RoleLevel);
            Assert.IsNotNull(referencedUserRole, Messages.ObjectIsNull);

            referencedUserRole.ActivationToken = Guid.NewGuid().ToString();
            referencedUserRole.Status = 666;
            referencedUser.UserRoles.Add(referencedUserRole);

            Property _refencedProperty = new Property()
            {
                User = referencedUser,
                Destination = _testDestination,
                PropertyCode = "DeleteUserByRoleIdTest_PropertyCode_Referenced",
                Address1 = "DeleteUserByRoleIdTest_Address1_Referenced",
                State = "DeleteUserByRoleIdTest_State_Referenced",
                ZIPCode = "DeleteUserByRoleIdTest_ZipCode_Referenced",
                City = "DeleteUserByRoleIdTest_City_Referenced",
                DictionaryCountry = _testCountry,
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "TZ"
            };
            _refencedProperty.SetPropertyNameValue("DeleteUserByRoleIdTest_PropertyName_Referenced");
            _refencedProperty.SetShortDescriptionValue("DeleteUserByRoleIdTest_ShortDescription_Referenced");
            _refencedProperty.SetLongDescriptionValue("DeleteUserByRoleIdTest_LongDescription_Referenced");

            _propertiesService.AddProperty(_refencedProperty);
            _efContext.SaveChanges();

            bool exceptionCaught = false;
            try
            {
                _userService.DeleteUserByRoleId(referencedUserRole.UserRoleId);
            }
            catch (Exception)
            {
                exceptionCaught = true;
            }

            _propertiesService.RemoveProperty(_refencedProperty.PropertyID);

            if (!exceptionCaught)
                Assert.Fail(Messages.DeletionShouldNotSucceed);
            #endregion
        }

        [TestMethod]
        public void GetRoleLevelsForUserTest()
        {
            UserRole tmpUserRole;
            RoleLevel[] roleLevels = (RoleLevel[]) Enum.GetValues(typeof(RoleLevel));
            Dictionary<RoleLevel, bool> roleLevelsFound = new Dictionary<RoleLevel, bool>();

            User user = new User()
            {
                Lastname = "GetRoleLevelsForUserTest_Lastname",
                Firstname = "GetRoleLevelsForUserTest_Firstname",
                email = "GetRoleLevelsForUserTest_email",
                Password = "GetRoleLevelsForUserTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(user);
            _testUsers.Add(user);

            foreach (RoleLevel rl in roleLevels)
            {
                tmpUserRole = _rolesService.AddUserToRole(user, rl);
                Assert.IsNotNull(tmpUserRole, Messages.ObjectIsNull);

                user.UserRoles.Add(tmpUserRole);
                roleLevelsFound.Add(rl, false);
            }

            _efContext.SaveChanges();

            var actual = _userService.GetRoleLevelsForUser(user.UserID);
            foreach (var actualObj in actual)
            {
                if (roleLevelsFound.ContainsKey(actualObj))
                    roleLevelsFound[actualObj] = true;
            }

            if (roleLevelsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "role levels for user", "GetRoleLevelsForUser"));
        }

        [TestMethod]
        public void GetUserRoleByLevelTest()
        {
            UserRole expected;
            User user = new User()
            {
                Lastname = "GetUserRoleByLevelTest_Lastname",
                Firstname = "GetUserRoleByLevelTest_Firstname",
                email = "GetUserRoleByLevelTest_email",
                Password = "GetUserRoleByLevelTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(user);
            _testUsers.Add(user);

            expected = _rolesService.AddUserToRole(user, (RoleLevel)_testRole.RoleLevel);
            Assert.IsNotNull(expected, Messages.ObjectIsNull);

            expected.ActivationToken = Guid.NewGuid().ToString();
            expected.Status = 666;
            user.UserRoles.Add(expected);

            _efContext.SaveChanges();

            var actual = _userService.GetUserRoleByLevel(user, (RoleLevel)_testRole.RoleLevel);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetUserRolesByRoleLevelTest()
        {
            UserRole[] userRoles = new UserRole[5];
            Dictionary<int, bool> userRolesFound = new Dictionary<int,bool>();
            User tmpUser;

            for (int i = 0; i < 5; ++i)
            {
                tmpUser = new User()
                {
                    Lastname = "GetUserRolesByRoleLevelTest_Lastname",
                    Firstname = "GetUserRolesByRoleLevelTest_Firstname",
                    email = "GetUserRolesByRoleLevelTest_email",
                    Password = "GetUserRolesByRoleLevelTest_Password",
                    IsGeneratedPassword = true,
                    DictionaryCulture = _testCulture
                };
                _userService.AddUser(tmpUser);
                _testUsers.Add(tmpUser);

                userRoles[i] = _rolesService.AddUserToRole(tmpUser, (RoleLevel)_testRole.RoleLevel);
                Assert.IsNotNull(userRoles[i], Messages.ObjectIsNull);

                userRoles[i].ActivationToken = Guid.NewGuid().ToString();
                userRoles[i].Status = 666;
                tmpUser.UserRoles.Add(userRoles[i]);

                _efContext.SaveChanges();

                userRolesFound.Add(userRoles[i].UserRoleId, false);

                _testUsers.Add(tmpUser);
            }

            UserRole expected;
            var actual = _userService.GetUserRolesByRoleLevel((RoleLevel)_testRole.RoleLevel);
            foreach (var actualObj in actual)
            {
                if (userRolesFound.ContainsKey(actualObj.UserRoleId))
                {
                    expected = userRoles.Where(ur => ur.UserRoleId == actualObj.UserRoleId).FirstOrDefault();
                    
                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    userRolesFound[actualObj.UserRoleId] = true;
                }
            }

            if (userRolesFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "user roles", "GetUserRolesByRoleLevel"));
        }

        [TestMethod]
        public void GetUserByUserRoleIdTest()
        {
            UserRole expected;
            User user = new User()
            {
                Lastname = "GetUserByUserRoleIdTest_Lastname",
                Firstname = "GetUserByUserRoleIdTest_Firstname",
                email = "GetUserByUserRoleIdTest_email",
                Password = "GetUserByUserRoleIdTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(user);
            _testUsers.Add(user);

            expected = _rolesService.AddUserToRole(user, (RoleLevel)_testRole.RoleLevel);
            Assert.IsNotNull(expected, Messages.ObjectIsNull);

            expected.ActivationToken = Guid.NewGuid().ToString();
            expected.Status = 666;
            user.UserRoles.Add(expected);

            _efContext.SaveChanges();

            var actual = _userService.GetUserByUserRoleId(expected.UserRoleId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetUserRolesByUserIdTest()
        {
            RoleLevel[] roleLevels = (RoleLevel[])Enum.GetValues(typeof(RoleLevel));
            UserRole[] userRoles = new UserRole[roleLevels.Count()];
            Dictionary<int, bool> userRolesFound = new Dictionary<int, bool>();

            User user = new User()
            {
                Lastname = "GetUserRolesByUserIdTest_Lastname",
                Firstname = "GetUserRolesByUserIdTest_Firstname",
                email = "GetUserRolesByUserIdTest_email",
                Password = "GetUserRolesByUserIdTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(user);
            _testUsers.Add(user);

            for (int i = 0; i < roleLevels.Count(); ++i)
            {
                userRoles[i] = _rolesService.AddUserToRole(user, roleLevels[i]);
                Assert.IsNotNull(userRoles[i], Messages.ObjectIsNull);

                user.UserRoles.Add(userRoles[i]);

                _efContext.SaveChanges();

                userRolesFound.Add(userRoles[i].UserRoleId, false);
            }

            UserRole expected;
            var actual = _userService.GetUserRolesByUserId(user.UserID);
            foreach (var actualObj in actual)
            {
                if (userRolesFound.ContainsKey(actualObj.UserRoleId))
                {
                    expected = userRoles.Where(ur => ur.UserRoleId == actualObj.UserRoleId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    userRolesFound[actualObj.UserRoleId] = true;
                }
            }

            if (userRolesFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "user roles", "GetUserRolesByUserId"));
        }

        [TestMethod]
        public void MergeUserDataTest()
        {
            User userFromDb, newUser;

            #region Case: overwrite
            userFromDb = new User()
            {
                Lastname = "MergeUserDataTest_Lastname_userFromDb",
                Firstname = "MergeUserDataTest_Firstname_userFromDb",
                Address1 = "MergeUserDataTest_Address1_userFromDb",
                Address2 = "MergeUserDataTest_Address2_userFromDb",
                State = "MergeUserDataTest_State_userFromDb",
                ZIPCode = "MergeUserDataTest_ZIPCode_userFromDb",
                City = "MergeUserDataTest_City_userFromDb",
                CellPhone = "MergeUserDataTest_CellPhone_userFromDb",
                LandLine = "MergeUserDataTest_LandLine_userFromDb",
                DriverLicenseNbr = "MergeUserDataTest_DriverLicenseNbr_userFromDb",
                PassportNbr = "MergeUserDataTest_PassportNbr_userFromDb",
                SocialsecurityNbr = "MergeUserDataTest_SocialsecurityNbr_userFromDb",
                DirectDepositInfo = "MergeUserDataTest_DirectDepositInfo_userFromDb",
                AcceptedTCsInitials = "MergeUserDataTest_AcceptedTCsInitials_userFromDb",
                DictionaryCountry = _testCountry,
                DictionaryCulture = _testCulture
            };

            newUser = new User()
            {
                Lastname = "MergeUserDataTest_Lastname_newUser",
                Firstname = "MergeUserDataTest_Firstname_newUser",
                Address1 = "MergeUserDataTest_Address1_newUser",
                Address2 = "MergeUserDataTest_Address2_newUser",
                State = "MergeUserDataTest_State_newUser",
                ZIPCode = "MergeUserDataTest_ZIPCode_newUser",
                City = "MergeUserDataTest_City_newUser",
                CellPhone = "MergeUserDataTest_CellPhone_newUser",
                LandLine = "MergeUserDataTest_LandLine_newUser",
                DriverLicenseNbr = "MergeUserDataTest_DriverLicenseNbr_newUser",
                PassportNbr = "MergeUserDataTest_PassportNbr_newUser",
                SocialsecurityNbr = "MergeUserDataTest_SocialsecurityNbr_newUser",
                DirectDepositInfo = "MergeUserDataTest_DirectDepositInfo_newUser",
                AcceptedTCsInitials = "MergeUserDataTest_AcceptedTCsInitials_newUser",
                DictionaryCulture = _testCulture
            };

            _userService.MergeUserData(userFromDb, newUser, true);

            Assert.AreEqual(newUser.Lastname, userFromDb.Lastname);
            Assert.AreEqual(newUser.Firstname, userFromDb.Firstname);
            Assert.AreEqual(newUser.Address1, userFromDb.Address1);
            Assert.AreEqual(newUser.Address2, userFromDb.Address2);
            Assert.AreEqual(newUser.State, userFromDb.State);
            Assert.AreEqual(newUser.ZIPCode, userFromDb.ZIPCode);
            Assert.AreEqual(newUser.City, userFromDb.City);
            Assert.AreEqual(newUser.CellPhone, userFromDb.CellPhone);
            Assert.AreEqual(newUser.LandLine, userFromDb.LandLine);
            Assert.AreEqual(newUser.DriverLicenseNbr, userFromDb.DriverLicenseNbr);
            Assert.AreEqual(newUser.PassportNbr, userFromDb.PassportNbr);
            Assert.AreEqual(newUser.SocialsecurityNbr, userFromDb.SocialsecurityNbr);
            Assert.AreEqual(newUser.DirectDepositInfo, userFromDb.DirectDepositInfo);
            Assert.AreEqual(newUser.AcceptedTCsInitials, userFromDb.AcceptedTCsInitials);
            #endregion

            #region Case: nulls
            userFromDb = new User()
            {
                Lastname = null,
                Firstname = null,
                Address1 = null,
                Address2 = null,
                State = null,
                ZIPCode = null,
                City = null,
                CellPhone = null,
                LandLine = null,
                DriverLicenseNbr = null,
                PassportNbr = null,
                SocialsecurityNbr = null,
                DirectDepositInfo = null,
                AcceptedTCsInitials = null,
                DictionaryCountry = _testCountry,
                DictionaryCulture = _testCulture
            };

            newUser = new User()
            {
                Lastname = "MergeUserDataTest_Lastname_newUser",
                Firstname = "MergeUserDataTest_Firstname_newUser",
                Address1 = "MergeUserDataTest_Address1_newUser",
                Address2 = "MergeUserDataTest_Address2_newUser",
                State = "MergeUserDataTest_State_newUser",
                ZIPCode = "MergeUserDataTest_ZIPCode_newUser",
                City = "MergeUserDataTest_City_newUser",
                CellPhone = "MergeUserDataTest_CellPhone_newUser",
                LandLine = "MergeUserDataTest_LandLine_newUser",
                DriverLicenseNbr = "MergeUserDataTest_DriverLicenseNbr_newUser",
                PassportNbr = "MergeUserDataTest_PassportNbr_newUser",
                SocialsecurityNbr = "MergeUserDataTest_SocialsecurityNbr_newUser",
                DirectDepositInfo = "MergeUserDataTest_DirectDepositInfo_newUser",
                AcceptedTCsInitials = "MergeUserDataTest_AcceptedTCsInitials_newUser",
                DictionaryCulture = _testCulture
            };

            _userService.MergeUserData(userFromDb, newUser, false);

            Assert.AreEqual(newUser.Lastname, userFromDb.Lastname);
            Assert.AreEqual(newUser.Firstname, userFromDb.Firstname);
            Assert.AreEqual(newUser.Address1, userFromDb.Address1);
            Assert.AreEqual(newUser.Address2, userFromDb.Address2);
            Assert.AreEqual(newUser.State, userFromDb.State);
            Assert.AreEqual(newUser.ZIPCode, userFromDb.ZIPCode);
            Assert.AreEqual(newUser.City, userFromDb.City);
            Assert.AreEqual(newUser.CellPhone, userFromDb.CellPhone);
            Assert.AreEqual(newUser.LandLine, userFromDb.LandLine);
            Assert.AreEqual(newUser.DriverLicenseNbr, userFromDb.DriverLicenseNbr);
            Assert.AreEqual(newUser.PassportNbr, userFromDb.PassportNbr);
            Assert.AreEqual(newUser.SocialsecurityNbr, userFromDb.SocialsecurityNbr);
            Assert.AreEqual(newUser.DirectDepositInfo, userFromDb.DirectDepositInfo);
            Assert.AreEqual(newUser.AcceptedTCsInitials, userFromDb.AcceptedTCsInitials);
            #endregion

            #region Case: empty strings
            userFromDb = new User()
            {
                Lastname = "",
                Firstname = "",
                Address1 = "",
                Address2 = "",
                State = "",
                ZIPCode = "",
                City = "",
                CellPhone = "",
                LandLine = "",
                DriverLicenseNbr = "",
                PassportNbr = "",
                SocialsecurityNbr = "",
                DirectDepositInfo = "",
                AcceptedTCsInitials = null,
                DictionaryCountry = _testCountry,
                DictionaryCulture = _testCulture
            };

            newUser = new User()
            {
                Lastname = "MergeUserDataTest_Lastname_newUser",
                Firstname = "MergeUserDataTest_Firstname_newUser",
                Address1 = "MergeUserDataTest_Address1_newUser",
                Address2 = "MergeUserDataTest_Address2_newUser",
                State = "MergeUserDataTest_State_newUser",
                ZIPCode = "MergeUserDataTest_ZIPCode_newUser",
                City = "MergeUserDataTest_City_newUser",
                CellPhone = "MergeUserDataTest_CellPhone_newUser",
                LandLine = "MergeUserDataTest_LandLine_newUser",
                DriverLicenseNbr = "MergeUserDataTest_DriverLicenseNbr_newUser",
                PassportNbr = "MergeUserDataTest_PassportNbr_newUser",
                SocialsecurityNbr = "MergeUserDataTest_SocialsecurityNbr_newUser",
                DirectDepositInfo = "MergeUserDataTest_DirectDepositInfo_newUser",
                AcceptedTCsInitials = "MergeUserDataTest_AcceptedTCsInitials_newUser",
                DictionaryCulture = _testCulture
            };

            _userService.MergeUserData(userFromDb, newUser, false);

            Assert.AreEqual(newUser.Lastname, userFromDb.Lastname);
            Assert.AreEqual(newUser.Firstname, userFromDb.Firstname);
            Assert.AreEqual(newUser.Address1, userFromDb.Address1);
            Assert.AreEqual(newUser.Address2, userFromDb.Address2);
            Assert.AreEqual(newUser.State, userFromDb.State);
            Assert.AreEqual(newUser.ZIPCode, userFromDb.ZIPCode);
            Assert.AreEqual(newUser.City, userFromDb.City);
            Assert.AreEqual(newUser.CellPhone, userFromDb.CellPhone);
            Assert.AreEqual(newUser.LandLine, userFromDb.LandLine);
            Assert.AreEqual(newUser.DriverLicenseNbr, userFromDb.DriverLicenseNbr);
            Assert.AreEqual(newUser.PassportNbr, userFromDb.PassportNbr);
            Assert.AreEqual(newUser.SocialsecurityNbr, userFromDb.SocialsecurityNbr);
            Assert.AreEqual(newUser.DirectDepositInfo, userFromDb.DirectDepositInfo);
            Assert.AreEqual(newUser.AcceptedTCsInitials, userFromDb.AcceptedTCsInitials);
            #endregion

            #region Case: mixed
            userFromDb = new User()
            {
                Lastname = null,
                Firstname = "MergeUserDataTest_Firstname_userFromDb",
                Address1 = "",
                Address2 = null,
                State = "MergeUserDataTest_State_userFromDb",
                ZIPCode = "MergeUserDataTest_ZIPCode_userFromDb",
                City = "",
                CellPhone = "MergeUserDataTest_CellPhone_userFromDb",
                LandLine = "",
                DriverLicenseNbr = "MergeUserDataTest_DriverLicenseNbr_userFromDb",
                PassportNbr = null,
                SocialsecurityNbr = "MergeUserDataTest_SocialsecurityNbr_userFromDb",
                DirectDepositInfo = null,
                AcceptedTCsInitials = null,
                DictionaryCountry = _testCountry,
                DictionaryCulture = _testCulture
            };

            newUser = new User()
            {
                Lastname = "MergeUserDataTest_Lastname_newUser",
                Firstname = "MergeUserDataTest_Firstname_newUser",
                Address1 = "MergeUserDataTest_Address1_newUser",
                Address2 = "MergeUserDataTest_Address2_newUser",
                State = "MergeUserDataTest_State_newUser",
                ZIPCode = "MergeUserDataTest_ZIPCode_newUser",
                City = "MergeUserDataTest_City_newUser",
                CellPhone = "MergeUserDataTest_CellPhone_newUser",
                LandLine = "MergeUserDataTest_LandLine_newUser",
                DriverLicenseNbr = "MergeUserDataTest_DriverLicenseNbr_newUser",
                PassportNbr = "MergeUserDataTest_PassportNbr_newUser",
                SocialsecurityNbr = "MergeUserDataTest_SocialsecurityNbr_newUser",
                DirectDepositInfo = "MergeUserDataTest_DirectDepositInfo_newUser",
                AcceptedTCsInitials = "MergeUserDataTest_AcceptedTCsInitials_newUser",
                DictionaryCulture = _testCulture
            };

            _userService.MergeUserData(userFromDb, newUser, false);

            Assert.AreEqual(newUser.Lastname, userFromDb.Lastname);
            Assert.AreNotEqual(newUser.Firstname, userFromDb.Firstname);
            Assert.AreEqual(newUser.Address1, userFromDb.Address1);
            Assert.AreEqual(newUser.Address2, userFromDb.Address2);
            Assert.AreNotEqual(newUser.State, userFromDb.State);
            Assert.AreNotEqual(newUser.ZIPCode, userFromDb.ZIPCode);
            Assert.AreEqual(newUser.City, userFromDb.City);
            Assert.AreNotEqual(newUser.CellPhone, userFromDb.CellPhone);
            Assert.AreEqual(newUser.LandLine, userFromDb.LandLine);
            Assert.AreNotEqual(newUser.DriverLicenseNbr, userFromDb.DriverLicenseNbr);
            Assert.AreEqual(newUser.PassportNbr, userFromDb.PassportNbr);
            Assert.AreNotEqual(newUser.SocialsecurityNbr, userFromDb.SocialsecurityNbr);
            Assert.AreEqual(newUser.DirectDepositInfo, userFromDb.DirectDepositInfo);
            Assert.AreEqual(newUser.AcceptedTCsInitials, userFromDb.AcceptedTCsInitials);
            #endregion

            #region Case: no change
            userFromDb = new User()
            {
                Lastname = "MergeUserDataTest_Lastname_userFromDb",
                Firstname = "MergeUserDataTest_Firstname_userFromDb",
                Address1 = "MergeUserDataTest_Address1_userFromDb",
                Address2 = "MergeUserDataTest_Address2_userFromDb",
                State = "MergeUserDataTest_State_userFromDb",
                ZIPCode = "MergeUserDataTest_ZIPCode_userFromDb",
                City = "MergeUserDataTest_City_userFromDb",
                CellPhone = "MergeUserDataTest_CellPhone_userFromDb",
                LandLine = "MergeUserDataTest_LandLine_userFromDb",
                DriverLicenseNbr = "MergeUserDataTest_DriverLicenseNbr_userFromDb",
                PassportNbr = "MergeUserDataTest_PassportNbr_userFromDb",
                SocialsecurityNbr = "MergeUserDataTest_SocialsecurityNbr_userFromDb",
                DirectDepositInfo = "MergeUserDataTest_DirectDepositInfo_userFromDb",
                AcceptedTCsInitials = "MergeUserDataTest_AcceptedTCsInitials_userFromDb",
                DictionaryCountry = _testCountry,
                DictionaryCulture = _testCulture
            };

            newUser = new User()
            {
                Lastname = "MergeUserDataTest_Lastname_newUser",
                Firstname = "MergeUserDataTest_Firstname_newUser",
                Address1 = "MergeUserDataTest_Address1_newUser",
                Address2 = "MergeUserDataTest_Address2_newUser",
                State = "MergeUserDataTest_State_newUser",
                ZIPCode = "MergeUserDataTest_ZIPCode_newUser",
                City = "MergeUserDataTest_City_newUser",
                CellPhone = "MergeUserDataTest_CellPhone_newUser",
                LandLine = "MergeUserDataTest_LandLine_newUser",
                DriverLicenseNbr = "MergeUserDataTest_DriverLicenseNbr_newUser",
                PassportNbr = "MergeUserDataTest_PassportNbr_newUser",
                SocialsecurityNbr = "MergeUserDataTest_SocialsecurityNbr_newUser",
                DirectDepositInfo = "MergeUserDataTest_DirectDepositInfo_newUser",
                AcceptedTCsInitials = "MergeUserDataTest_AcceptedTCsInitials_newUser",
                DictionaryCulture = _testCulture
            };

            _userService.MergeUserData(userFromDb, newUser, false);

            Assert.AreNotEqual(newUser.Lastname, userFromDb.Lastname);
            Assert.AreNotEqual(newUser.Firstname, userFromDb.Firstname);
            Assert.AreNotEqual(newUser.Address1, userFromDb.Address1);
            Assert.AreNotEqual(newUser.Address2, userFromDb.Address2);
            Assert.AreNotEqual(newUser.State, userFromDb.State);
            Assert.AreNotEqual(newUser.ZIPCode, userFromDb.ZIPCode);
            Assert.AreNotEqual(newUser.City, userFromDb.City);
            Assert.AreNotEqual(newUser.CellPhone, userFromDb.CellPhone);
            Assert.AreNotEqual(newUser.LandLine, userFromDb.LandLine);
            Assert.AreNotEqual(newUser.DriverLicenseNbr, userFromDb.DriverLicenseNbr);
            Assert.AreNotEqual(newUser.PassportNbr, userFromDb.PassportNbr);
            Assert.AreNotEqual(newUser.SocialsecurityNbr, userFromDb.SocialsecurityNbr);
            Assert.AreNotEqual(newUser.DirectDepositInfo, userFromDb.DirectDepositInfo);
            Assert.AreNotEqual(newUser.AcceptedTCsInitials, userFromDb.AcceptedTCsInitials);
            #endregion
        }

        [TestMethod]
        public void GetManagerForZipCodeTest()
        {
            //Tested method: public User GetManagerForZipCode(string zipCode)
            UserRole expected = null;
            string zipCode = "00100";

            int zipCodeRangeId = _userService.GetZipCodeRanges().ToList().First().ZipCodeRangeId;

            User user = new User()
            {
                Lastname = "GetUserRolesByUserIdTest_Lastname",
                Firstname = "GetUserRolesByUserIdTest_Firstname",
                email = "GetUserRolesByUserIdTest_email",
                Password = "GetUserRolesByUserIdTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            ;

            user.UserRoles.Add(new UserRole()
            {
                Status = 3,
                Role = _rolesService.GetRoleByRoleLevel(RoleLevel.Manager),
                User = user,
                ActivationToken = null
            });
            _userService.AddUser(user);
            _testUsers.Add(user);
            _userService.AddZipCodeRangeToManager(zipCodeRangeId, user.UserRoles.SingleOrDefault().UserRoleId);

            User user2 = new User()
            {
                Lastname = "GetUserRolesByUserIdTest_Lastname",
                Firstname = "GetUserRolesByUserIdTest_Firstname",
                email = "GetUserRolesByUserIdTest_email",
                Password = "GetUserRolesByUserIdTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };

            user2.UserRoles.Add(new UserRole()
            {
                Status = 3,
                Role = _rolesService.GetRoleByRoleLevel(RoleLevel.Manager),
                User = user2,
                ActivationToken = null
            });
            _userService.AddUser(user2);
            _testUsers.Add(user2);
            _userService.AddZipCodeRangeToManager(zipCodeRangeId, user2.UserRoles.SingleOrDefault().UserRoleId);
            _efContext.SaveChanges();
            expected = _userService.GetManagerRoleForZipCode(zipCode);

            Assert.IsNotNull(expected);
            ExtendedAssert.AreEqual(expected, user.UserRoles.First());
            _userService.DeleteZipCodeManager(user.UserRoles.SingleOrDefault().ManagerZipCodeRanges.SingleOrDefault().ManagerZipCodeRangeId);
            _userService.DeleteZipCodeManager(user2.UserRoles.SingleOrDefault().ManagerZipCodeRanges.SingleOrDefault().ManagerZipCodeRangeId);
        }

        [TestMethod]
        public void GetZipCodesRangesForUserRoleTest()
        {
            //public IEnumerable<ManagerZipCodeRange> GetZipCodesRangesForUserRole(int userRoleId)
            var zipCodeRange = _userService.GetZipCodeRanges().ToList().First();

            #region User 1
            User user = new User()
            {
                Lastname = "GetUserRolesByUserIdTest_Lastname",
                Firstname = "GetUserRolesByUserIdTest_Firstname",
                email = "GetUserRolesByUserIdTest_email",
                Password = "GetUserRolesByUserIdTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            ;

            user.UserRoles.Add(new UserRole()
            {
                Status = 3,
                Role = _rolesService.GetRoleByRoleLevel(RoleLevel.Manager),
                User = user,
                ActivationToken = null
            });
            _userService.AddUser(user);
            _testUsers.Add(user);
            _userService.AddZipCodeRangeToManager(zipCodeRange.ZipCodeRangeId, user.UserRoles.SingleOrDefault().UserRoleId);
            #endregion

            _efContext.SaveChanges();

            var expected = _userService.GetZipCodesRangesForUserRole(user.UserRoles.SingleOrDefault().UserRoleId);
            Assert.IsNotNull(expected);
            Assert.AreEqual(expected.Count(), 1);
            Assert.AreEqual(expected.Single().ZipCodeRange, zipCodeRange);
            _userService.DeleteZipCodeManager(user.UserRoles.SingleOrDefault().ManagerZipCodeRanges.SingleOrDefault().ManagerZipCodeRangeId);
        }

        [TestMethod]
        public void AddZipCodeRangeToManagerTest()
        {
            //public IEnumerable<ManagerZipCodeRange> GetZipCodesRangesForUserRole(int userRoleId)
            var zipCodeRange1 = _userService.GetZipCodeRanges().ToList().First();
            var zipCodeRange2 = _userService.GetZipCodeRanges().ToList().Last();

            #region User 1
            User user = new User()
            {
                Lastname = "GetUserRolesByUserIdTest_Lastname",
                Firstname = "GetUserRolesByUserIdTest_Firstname",
                email = "GetUserRolesByUserIdTest_email",
                Password = "GetUserRolesByUserIdTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            ;

            user.UserRoles.Add(new UserRole()
            {
                Status = 3,
                Role = _rolesService.GetRoleByRoleLevel(RoleLevel.Manager),
                User = user,
                ActivationToken = null
            });
            _userService.AddUser(user);
            _testUsers.Add(user);
            _userService.AddZipCodeRangeToManager(zipCodeRange1.ZipCodeRangeId, user.UserRoles.SingleOrDefault().UserRoleId);
            _userService.AddZipCodeRangeToManager(zipCodeRange2.ZipCodeRangeId, user.UserRoles.SingleOrDefault().UserRoleId);
            #endregion

            _efContext.SaveChanges();

            var expected = _userService.GetZipCodesRangesForUserRole(user.UserRoles.SingleOrDefault().UserRoleId);
            Assert.IsNotNull(expected);
            Assert.AreEqual(expected.Count(), 2);
            Assert.AreEqual(expected.First().ZipCodeRange, zipCodeRange1);
            Assert.AreEqual(expected.Last().ZipCodeRange, zipCodeRange2);
            _userService.DeleteZipCodeManager(user.UserRoles.SingleOrDefault().ManagerZipCodeRanges.First().ManagerZipCodeRangeId);
            _userService.DeleteZipCodeManager(user.UserRoles.SingleOrDefault().ManagerZipCodeRanges.Last().ManagerZipCodeRangeId);
            
        }

        [TestMethod]
        public void DeleteZipCodeManagerTest()
        {
            //public IEnumerable<ManagerZipCodeRange> GetZipCodesRangesForUserRole(int userRoleId)
            var zipCodeRange = _userService.GetZipCodeRanges().ToList().First();

            #region User 1
            User user = new User()
            {
                Lastname = "GetUserRolesByUserIdTest_Lastname",
                Firstname = "GetUserRolesByUserIdTest_Firstname",
                email = "GetUserRolesByUserIdTest_email",
                Password = "GetUserRolesByUserIdTest_Password",
                IsGeneratedPassword = true,
                DictionaryCulture = _testCulture
            };
            ;

            user.UserRoles.Add(new UserRole()
            {
                Status = 3,
                Role = _rolesService.GetRoleByRoleLevel(RoleLevel.Manager),
                User = user,
                ActivationToken = null
            });
            _userService.AddUser(user);
            _testUsers.Add(user);
            _userService.AddZipCodeRangeToManager(zipCodeRange.ZipCodeRangeId, user.UserRoles.SingleOrDefault().UserRoleId);
            #endregion

            _efContext.SaveChanges();
            _userService.DeleteZipCodeManager(user.UserRoles.SingleOrDefault().ManagerZipCodeRanges.SingleOrDefault().ManagerZipCodeRangeId);
            _efContext.SaveChanges();
            var actual = _userService.GetZipCodesRangesForUserRole(user.UserRoles.SingleOrDefault().UserRoleId);
            Assert.IsTrue(actual.Count() == 0);
        }

        [TestMethod]
        public void GetZipCodeRangeByIdTest()
        {
            ZipCodeRange expected = new ZipCodeRange()
            {
                ZipCodeStart = "12345",
                ZipCodeEnd = "54321"
            };

            _userService.AddZipCodeRange(expected);
            _efContext.SaveChanges();
            _testRanges.Add(expected);

            var actual = _userService.GetZipCodeRangeById(expected.ZipCodeRangeId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddZipCodeRangeTest()
        {
            ZipCodeRange expected = new ZipCodeRange()
            {
                ZipCodeStart = "12345",
                ZipCodeEnd = "54321"
            };

            _userService.AddZipCodeRange(expected);
            _efContext.SaveChanges();
            _testRanges.Add(expected);

            var actual = _userService.GetZipCodeRangeById(expected.ZipCodeRangeId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DeleteZipCodeRangeByIdTest()
        {
            #region Delete successfully
            ZipCodeRange deletedRange = new ZipCodeRange()
            {
                ZipCodeStart = "12345",
                ZipCodeEnd = "54321"
            };

            _userService.AddZipCodeRange(deletedRange);
            _efContext.SaveChanges();

            _userService.DeleteZipCodeRangeById(deletedRange.ZipCodeRangeId);

            var dbRange = _userService.GetZipCodeRangeById(deletedRange.ZipCodeRangeId);

            Assert.IsNull(dbRange, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced zip code range deletion            
            ZipCodeRange referencedRange = new ZipCodeRange()
            {
                ZipCodeStart = "12345",
                ZipCodeEnd = "54321"
            };

            _userService.AddZipCodeRange(referencedRange);

            User referencedUser = new User()
            {
                Firstname = "DeleteAmenityTest_Firstname_Referenced",
                Lastname = "DeleteAmenityTest_Lastname_Referenced",
                email = "DeleteAmenityTest_E-mail_Referenced",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _dictionaryCultureService.GetCultures().First()
            };

            _userService.AddUser(referencedUser);

            UserRole referencedRole = _rolesService.AddUserToRole(referencedUser, RoleLevel.Manager);
            referencedRole.Status = (int)UserStatus.Deleted;
            referencedUser.UserRoles.Add(referencedRole);

            _efContext.SaveChanges();

            _userService.AddZipCodeRangeToManager(referencedRange.ZipCodeRangeId, referencedRole.UserRoleId);

            _efContext.SaveChanges();

            var managerRangeId = referencedRole.ManagerZipCodeRanges.First().ManagerZipCodeRangeId;

            bool exceptionCaught = false;
            try
            {
                _userService.DeleteZipCodeRangeById(referencedRange.ZipCodeRangeId);
            }
            catch (Exception)
            {
                exceptionCaught = true;
            }

            _userService.DeleteZipCodeManager(managerRangeId);
            _userService.DeleteUserById(referencedUser.UserID);

            if (!exceptionCaught)
                Assert.Fail(Messages.DeletionShouldNotSucceed);

            #endregion
        }

        [TestMethod]
        public void RemoveGuestPaymentMethodTest()
        {

            GuestPaymentMethod _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUsers.First()
            };
            DataAccessHelper.AddGuestPaymentMethod(_efContext, _testPaymentMethod);
            _testPaymentMethods.Add(_testPaymentMethod);
            _efContext.SaveChanges();

            #region Deletion succesfull
            _userService.RemoveGuestPaymentMethod(_testPaymentMethod.GuestPaymentMethodID, _testPaymentMethod.User.UserID);
            _efContext.SaveChanges();

            int userPaymentmethods = _efContext.GuestPaymentMethods.ToList().Where(s => s.User.UserID == _testUsers.First().UserID).Count();

            Assert.AreEqual(0, userPaymentmethods);
            #endregion

            #region Deletion unsuccessful

            ExtendedAssert.Throws<NullReferenceException>(() =>
                {
                    _userService.RemoveGuestPaymentMethod(_testPaymentMethod.GuestPaymentMethodID, _testPaymentMethod.User.UserID);
                });

            #endregion
        }

        [TestMethod]
        public void GetInactiveGuestPaymentMethodsTest()
        {
            for (int i = 0; i < 3; i++)
            {

                GuestPaymentMethod gpm = new GuestPaymentMethod()
                {
                    CreditCardLast4Digits = "843" + i,
                    CreditCardType = _bookingService.GetCreditCardTypes().First(),
                    ReferenceToken = "a",
                    User = _testUsers.First(),
                };
                DataAccessHelper.AddGuestPaymentMethod(_efContext, gpm);
                _testPaymentMethods.Add(gpm);

                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUsers.First(),
                    ConfirmationID = "abcd_" + i,
                    BookingDate = DateTime.Now,
                    BookingStatus = (int)BookingStatus.CheckedIn,
                    DateArrival = DateTime.Now,
                    DateDeparture = DateTime.Now.AddDays(10 * i),
                    TotalPrice = 2.45M,
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)i },
                    GuestPaymentMethod = gpm,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)i },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + i,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = null,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testUnit.Reservations.Add(temp);

                gpm.Reservations.Add(temp);
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();

            }

            List<GuestPaymentMethod> actual = _userService.GetInactiveGuestPaymentMethods(4).ToList();
            Assert.AreEqual(0, actual.Count);

            actual = _userService.GetInactiveGuestPaymentMethods(12).ToList();
            Assert.AreEqual(0, actual.Count);

            actual = _userService.GetInactiveGuestPaymentMethods(-1).ToList();
            Assert.AreEqual(1, actual.Count);

            actual = _userService.GetInactiveGuestPaymentMethods(-11).ToList();
            Assert.AreEqual(2, actual.Count);

            actual = _userService.GetInactiveGuestPaymentMethods(-21).ToList();
            Assert.AreEqual(3, actual.Count);

            ExtendedAssert.AreEqual(_testPaymentMethods[0], actual[0]);
            ExtendedAssert.AreEqual(_testPaymentMethods[1], actual[1]);
            ExtendedAssert.AreEqual(_testPaymentMethods[2], actual[2]);
        }

        #endregion
    }
}
