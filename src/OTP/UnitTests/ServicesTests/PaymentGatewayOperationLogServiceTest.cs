﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using DataAccess.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class PaymentGatewayOperationLogServiceTest
    {
        #region Fields
        /// <summary>
        /// list of amenity groups used in test methods
        /// </summary>
        private List<PaymentGatewayOperationsLog> _testLogs = new List<PaymentGatewayOperationsLog>();

        /// <summary>
        /// objects used in tests
        /// </summary>
        private Reservation _testReservation;
        private Property _testProperty;
        private Unit _testUnit;
        private UnitType _testUnitType;
        private User _testUser;

        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;
        #endregion

        #region Services
        private IPaymentGatewayOperationLogService _paymentGatewayOperationLogService;
        private IReservationsService _reservationsService;
        private IUserService _usersService;
        private IPropertiesService _propertiesService;
        private IUnitsService _unitsService;
        private IUnitTypesService _unitTypesService;
        private IDictionaryCultureService _dictionaryCultureService;
        private IZohoCrmService _zohoCrmService = null;
        private IMessageService _messageService = null;
        private ICrmOperationLogsService _crmOperationLogsService = null;
        private ISettingsService _settingsService = null;

        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            _paymentGatewayOperationLogService = new PaymentGatewayOperationLogService(mock.Object);

            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            IStateService stateService = new StateService(mock.Object);
            IBlobService blobService = new BlobService(mock.Object);
            IPropertyAddOnsService propertyAddOnsService = new PropertyAddOnsService(mock.Object);
            ITaxesService taxesService = new TaxesService(mock.Object);

            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _messageService = new MessageService(mock.Object, _settingsService);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _propertiesService = new PropertiesServices(configurationService, blobService, _settingsService, mock.Object);
            _unitTypesService = new UnitTypesService(mock.Object);
            _unitsService = new UnitsService(mock.Object, stateService, _unitTypesService);
            _usersService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);

            IEventLogService eventLogSevice = new EventLogService(mock.Object);
            IMessageService messageService = new MessageService(mock.Object, _settingsService);
            IPaymentGatewayService paymentGatewayService = new PaymentGatewayService(mock.Object, eventLogSevice, _settingsService, messageService);
            IAuthorizationService authorizationService = new AuthorizationService(_usersService, mock.Object);
            IBookingService bookingService = new BookingService(mock.Object, _propertiesService, propertyAddOnsService, taxesService, _unitsService, _usersService, stateService, eventLogSevice, _settingsService, messageService, paymentGatewayService, _unitTypesService, authorizationService);
            IDestinationsService _destinationService = new DestinationsService(configurationService, mock.Object);
            IPropertyTypesService _propertyTypeService = new PropertyTypeService(mock.Object);
            IDictionaryCountryService _countryService = new DictionaryCountryService(mock.Object);
            IDocumentService documentService = new PdfDocumentService(mock.Object, _settingsService);
            _reservationsService = new ReservationsService(configurationService, blobService, mock.Object, _propertiesService, bookingService, paymentGatewayService, documentService, messageService, _settingsService, _unitsService);
            #endregion

            #region Setup Objects
            DictionaryCountry _testCountry = _countryService.GetCountries().First();
            Destination _testDestination = _destinationService.GetDestinations().First();
            PropertyType _testPropertyType = _propertyTypeService.GetPropertyTypes().First();
            DataAccess.CreditCardType _testCreditCardType = bookingService.GetCreditCardTypes().First();
            DictionaryCulture _testCulture = _dictionaryCultureService.GetCultures().First();

            _testUser = new User()
            {
                Firstname = "PaymentGatewayOperationLogServiceTest_Firstname",
                Lastname = "PaymentGatewayOperationLogServiceTest_Lastname",
                email = "PaymentGatewayOperationLogServiceTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture
            };
            _usersService.AddUser(_testUser);

            _testProperty = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "PaymentGatewayOperationLogServiceTest_PropertyCode",
                Address1 = "PaymentGatewayOperationLogServiceTest_Address1",
                State = "PaymentGatewayOperationLogServiceTest_State",
                ZIPCode = "PaymentGatewayOperationLogServiceTest_ZipCode",
                City = "PaymentGatewayOperationLogServiceTest_City",
                DictionaryCountry = _testCountry,
                DictionaryCulture = _testCulture,
                PropertyType = _testPropertyType,
                TimeZone = "TZ"
            };
            _testProperty.SetPropertyNameValue("PaymentGatewayOperationLogServiceTest_PropertyName");
            _testProperty.SetShortDescriptionValue("PaymentGatewayOperationLogServiceTest_ShortDescription");
            _testProperty.SetLongDescriptionValue("PaymentGatewayOperationLogServiceTest_LongDescription");
            _propertiesService.AddProperty(_testProperty);

            _testUnitType = new UnitType()
            {
                UnitTypeCode = "PaymentGatewayOperationLogServiceTest_UnitTypeCode",
                Property = _testProperty
            };
            _testUnitType.SetTitleValue("PaymentGatewayOperationLogServiceTest_UnitTypeTitle");
            _testUnitType.SetDescValue("PaymentGatewayOperationLogServiceTest_UnitTypeDesc");
            _unitTypesService.AddUnitType(_testUnitType);

            _testUnit = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                UnitCode = "PaymentGatewayOperationLogServiceTest_UnitCode"
            };
            _testUnit.SetTitleValue("PaymentGatewayOperationLogServiceTest_UnitTypeTitle");
            _testUnit.SetDescValue("PaymentGatewayOperationLogServiceTest_UnitTypeDesc");
            _unitsService.AddUnit(_testUnit);

            _testReservation = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = null,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                LinkAuthorizationToken = "343sdcge54",
                DictionaryCulture = _testCulture
            };

            _efContext.SaveChanges();
            #endregion
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (PaymentGatewayOperationsLog pgol in _testLogs)
            {
                if (_paymentGatewayOperationLogService.GetPaymentGatewayOperationsLogById(pgol.Id) != null)
                    _paymentGatewayOperationLogService.DeletePaymentGatewayOperationsLogById(pgol.Id);
            }
            _testLogs.Clear();

            //_reservationsService.DeleteReservationById(_testReservation.ReservationID);
            DataAccessHelper.RemoveReservation(_efContext, _testReservation);
            _unitsService.DeleteUnit(_testUnit.UnitID);
            _unitTypesService.DeleteUnitType(_testUnitType.UnitTypeID);
            _propertiesService.RemoveProperty(_testProperty.PropertyID);
            _usersService.DeleteUserById(_testUser.UserID);
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void AddOperationLogTest()
        {
            PaymentGatewayOperationsLog expected = new PaymentGatewayOperationsLog()
            {
                OperationType = (int)PaymentGatewayOperationType.AddPaymentMethod,
                RowDate = DateTime.Now,
                Message = "AddOperationLogTest_Message",
                GatewayResponse = "<xml>AddOperationLogTest_GatewayResponse</xml>",
                Reservation = _testReservation
            };
            _testLogs.Add(expected);
            _paymentGatewayOperationLogService.AddOperationLog(expected);
            _efContext.SaveChanges();

            var actual = _paymentGatewayOperationLogService.GetPaymentGatewayOperationsLogById(expected.Id);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetPaymentGatewayOperationsLogByIdTest()
        {
            PaymentGatewayOperationsLog expected = new PaymentGatewayOperationsLog()
            {
                OperationType = (int)PaymentGatewayOperationType.AddPaymentMethod,
                RowDate = DateTime.Now,
                Message = "GetPaymentGatewayOperationsLogByIdTest_Message",
                GatewayResponse = "<xml>GetPaymentGatewayOperationsLogByIdTest_GatewayResponse</xml>",
                Reservation = _testReservation
            };
            _testLogs.Add(expected);
            _paymentGatewayOperationLogService.AddOperationLog(expected);
            _efContext.SaveChanges();

            var actual = _paymentGatewayOperationLogService.GetPaymentGatewayOperationsLogById(expected.Id);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetPaymentGatewayOperationsLogsTest()
        {
            Dictionary<int, bool> newPaymentGatewayOperationsLogsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; i++)
            {
                PaymentGatewayOperationsLog temp = new PaymentGatewayOperationsLog()
                {
                    OperationType = (int)PaymentGatewayOperationType.AddPaymentMethod,
                    RowDate = DateTime.Now,
                    Message = "GetPaymentGatewayOperationsLogByIdTest_Message_" + i,
                    GatewayResponse = "<xml>GetPaymentGatewayOperationsLogByIdTest_GatewayResponse_" + i + "</xml>",
                    Reservation = _testReservation
                };

                _testLogs.Add(temp);
                _paymentGatewayOperationLogService.AddOperationLog(temp);
                _efContext.SaveChanges();

                newPaymentGatewayOperationsLogsFound.Add(_testLogs[i].Id, false);
            }

            PaymentGatewayOperationsLog expected;
            var actual = _paymentGatewayOperationLogService.GetPaymentGatewayOperationsLogs();

            foreach (var actualObj in actual)
            {
                if (newPaymentGatewayOperationsLogsFound.ContainsKey(actualObj.Id))
                {
                    expected = _testLogs.Where(a => a.Id == actualObj.Id).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newPaymentGatewayOperationsLogsFound[actualObj.Id] = true;
                }
            }

            if (newPaymentGatewayOperationsLogsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "payment gateway operation logs", "GetPaymentGatewayOperationsLogs"));
        }

        [TestMethod]
        public void DeletePaymentGatewayOperationsLogByIdTest()
        {
            #region Delete successfully
            PaymentGatewayOperationsLog deleted = new PaymentGatewayOperationsLog()
            {
                OperationType = (int)PaymentGatewayOperationType.AddPaymentMethod,
                RowDate = DateTime.Now,
                Message = "DeletePaymentGatewayOperationsLogByIdTest_Message",
                GatewayResponse = "<xml>DeletePaymentGatewayOperationsLogByIdTest_GatewayResponse</xml>",
                Reservation = _testReservation
            };

            _paymentGatewayOperationLogService.AddOperationLog(deleted);
            _efContext.SaveChanges();

            _paymentGatewayOperationLogService.DeletePaymentGatewayOperationsLogById(deleted.Id);

            var dbLog = _paymentGatewayOperationLogService.GetPaymentGatewayOperationsLogById(deleted.Id);

            Assert.IsNull(dbLog, Messages.ObjectIsNotNull);
            #endregion
        }
        #endregion
    }
}
