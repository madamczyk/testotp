﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using BusinessLogic.Objects.Templates.Document;
using BusinessLogic.Objects.StaticContentRepresentation;
using DataAccess;
using DataAccess.Enums;
using DataAccess.CustomObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Globalization;
using Common.Extensions;
using Common.Helpers;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class ReservationsServiceTest
    {
        #region Fields
        /// <summary>
        /// list of reservations used in test methods
        /// </summary>
        private List<Reservation> _testReservations = new List<Reservation>();

        /// <summary>
        /// objects used in tests
        /// </summary>
        private List<ReservationTransaction> _testTransactions = new List<ReservationTransaction>();
        private Property _testProperty;
        private Unit _testUnit;
        private UnitType _testUnitType;
        private User _testUser;
        private GuestPaymentMethod _testPaymentMethod;
        private DictionaryCulture _testCulture;
        private User _testOwner = null;
        private User _testPropertyManager = null;
        private User _testKeyManager = null;
        private Tax _testTax = null;
        private Destination _testDestination = null;
        private List<Message> _testMessages = new List<Message>();
        private PropertyAssignedManager _testAssignedPropertyManager = null;
        private PropertyAssignedManager _testAssignedKeyManager = null;

        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;
        #endregion

        #region Services
        private IReservationsService _reservationsService;
        private IUserService _usersService;
        private IPropertiesService _propertiesService;
        private IUnitsService _unitsService;
        private IUnitTypesService _unitTypesService;
        private IDictionaryCultureService _dictionaryCultureService;
        private ITaxesService _taxesService;
        private IMessageService _messageService;
        private IDocumentService _documentService;
        private IZohoCrmService _zohoCrmService = null;
        private ICrmOperationLogsService _crmOperationLogsService = null;
        private ISettingsService _settingsService = null;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            var settings = new Mock<IConfigurationService>();
            settings.Setup(f => f.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl)).Returns("blloburl");

            IConfigurationService configurationService = settings.Object;
            _settingsService = new SettingsService(mock.Object);
            IStateService stateService = new StateService(mock.Object);
            IBlobService blobService = new BlobService(mock.Object);
            _propertiesService = new PropertiesServices(configurationService, blobService, _settingsService, mock.Object);
            IPropertyAddOnsService propertyAddOnsService = new PropertyAddOnsService(mock.Object);
            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _messageService = new MessageService(mock.Object, _settingsService);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _taxesService = new TaxesService(mock.Object);
            _unitTypesService = new UnitTypesService(mock.Object);
            _unitsService = new UnitsService(mock.Object, stateService, _unitTypesService);
            _usersService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
            IEventLogService eventLogSevice = new EventLogService(mock.Object);
            IStorageQueueService queueService = new StorageQueueService(configurationService, mock.Object);
            _documentService = new PdfDocumentService(mock.Object, _settingsService);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);
            _messageService = new MessageService(queueService, _documentService, mock.Object, _settingsService);
            IPaymentGatewayService paymentGatewayService = new PaymentGatewayService(mock.Object, eventLogSevice, _settingsService, _messageService);
            IAuthorizationService authorizationService = new AuthorizationService(_usersService, mock.Object);
            IBookingService bookingService = new BookingService(mock.Object, _propertiesService, propertyAddOnsService, _taxesService, _unitsService, _usersService, stateService, eventLogSevice, _settingsService, _messageService, paymentGatewayService, _unitTypesService, authorizationService);
            _reservationsService = new ReservationsService(configurationService, blobService, mock.Object, _propertiesService, bookingService, paymentGatewayService, _documentService, _messageService, _settingsService, _unitsService);
            IDestinationsService _destinationService = new DestinationsService(configurationService, mock.Object);
            IPropertyTypesService _propertyTypeService = new PropertyTypeService(mock.Object);
            IDictionaryCountryService _countryService = new DictionaryCountryService(mock.Object);
            #endregion

            #region Setup Objects
            DictionaryCountry _testCountry = _countryService.GetCountries().First();
            _testDestination = _destinationService.GetDestinations().First();
            PropertyType _testPropertyType = _propertyTypeService.GetPropertyTypes().First();

            _testTax = new Tax()
            {
                Destination = _testDestination,
                Percentage = 10.0m,
                Name_i18n = "TestTaxName_i18",
                NameCurrentLanguage = "en"
            };
            _testTax.SetNameValue("TestTaxName");
            _taxesService.AddTax(_testTax);

            DataAccess.CreditCardType _testCreditCardType = bookingService.GetCreditCardTypes().First();
            _testCulture = _dictionaryCultureService.GetCultures().Where(pr => pr.CultureCode == "en-US").SingleOrDefault();

            _testUser = new User()
            {
                Firstname = "ReservationsServiceTest_Firstname",
                Lastname = "ReservationsServiceTest_Lastname",
                email = "ReservationsServiceTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture,
                AcceptedTCsInitials = "RR"
            };
            _usersService.AddUser(_testUser);

            _testPropertyManager = new User()
            {
                Firstname = "ReservationsServiceTest_Firstname_Manager",
                Lastname = "ReservationsServiceTest_Lastname_Manager",
                email = "ReservationsServiceTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture,
                AcceptedTCsInitials = "RR"
            };
            _usersService.AddUser(_testPropertyManager);

            _testKeyManager = new User()
            {
                Firstname = "ReservationsServiceTest_Firstname_KeyManager",
                Lastname = "ReservationsServiceTest_Lastname_KeyManager",
                email = "ReservationsServiceTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture,
                AcceptedTCsInitials = "RR"
            };
            _usersService.AddUser(_testKeyManager);

            _testProperty = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "ReservationsServiceTest_PropertyCode",
                Address1 = "ReservationsServiceTest_Address1",
                Address2 = "ReservationsServiceTest_Address2",
                State = "ReservationsServiceTest_State",
                ZIPCode = "ReservationsServiceTest_ZipCode",
                City = "ReservationsServiceTest_City",
                DictionaryCountry = _testCountry,                 
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                PropertyLive = true,
                PropertyStaticContents = new List<DataAccess.PropertyStaticContent>(),
                TimeZone = "TZ"
            };
            _testProperty.PropertyStaticContents.Add(new PropertyStaticContent() { ContentCode = 15, ContentValue = "<i18nString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><i18nContent><i18nPair><Code>en</Code><Content>iiii</Content></i18nPair></i18nContent></i18nString>" });
            _testProperty.SetPropertyNameValue("ReservationsServiceTest_PropertyName");
            _testProperty.SetShortDescriptionValue("ReservationsServiceTest_ShortDescription");
            _testProperty.SetLongDescriptionValue("ReservationsServiceTest_LongDescription");
            _propertiesService.AddProperty(_testProperty);

            _testUnitType = new UnitType()
            {
                UnitTypeCode = "ReservationsServiceTest_UnitTypeCode",
                Property = _testProperty
            };
            _testUnitType.SetTitleValue("ReservationsServiceTest_UnitTypeTitle");
            _testUnitType.SetDescValue("ReservationsServiceTest_UnitTypeDesc");
            _unitTypesService.AddUnitType(_testUnitType);

            _testUnit = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                UnitCode = "ReservationsServiceTest_UnitCode"
            };
            _testUnit.SetTitleValue("ReservationsServiceTest_UnitTypeTitle");
            _testUnit.SetDescValue("ReservationsServiceTest_UnitTypeDesc");
            _unitsService.AddUnit(_testUnit);

            _testPaymentMethod = new GuestPaymentMethod()
            {
                User = _testUser,
                CreditCardType = _testCreditCardType,
                CreditCardLast4Digits = "1234",
                ReferenceToken = "aavsvsdfg"
            };
            _efContext.GuestPaymentMethods.Add(_testPaymentMethod);

            _testOwner = new User()
            {
                Firstname = "ReservationsServiceTest_Owner",
                Lastname = "ReservationsServiceTest_Lastname",
                email = "ReservationsServiceTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture,
                Properties = new List<Property>()
            };

            _testOwner.Properties.Add(_testProperty);
            _usersService.AddUser(_testOwner);

            _testAssignedPropertyManager = new PropertyAssignedManager()
            {
                Property = _testProperty,
                User = _testPropertyManager,
                Role = _efContext.Roles.Where(r => r.RoleLevel.Equals((int)RoleLevel.PropertyManager)).FirstOrDefault()
            };
            _testProperty.PropertyAssignedManagers.Add(_testAssignedPropertyManager);
            _efContext.PropertyAssignedManagers.Add(_testAssignedPropertyManager);

            _testAssignedKeyManager = new PropertyAssignedManager()
            {
                Property = _testProperty,
                User = _testKeyManager,
                Role = _efContext.Roles.Where(r => r.RoleLevel.Equals((int)RoleLevel.KeyManager)).FirstOrDefault()
            };
            _testProperty.PropertyAssignedManagers.Add(_testAssignedKeyManager);
            _efContext.PropertyAssignedManagers.Add(_testAssignedKeyManager);

            _efContext.SaveChanges();
            #endregion
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            if (_testMessages != null)
            {
                foreach (Message m in _testMessages)
                {
                    //if (_messageService.GetMessageById(m.MessageId) != null) ;
                    DataAccessHelper.RemoveMessage(_efContext, m);
                }
            }

            if (_taxesService.GetTaxById(_testTax.TaxId) != null)
                _taxesService.DeleteTax(_testTax.TaxId);

            foreach (Reservation r in _testReservations)
            {
                if (_reservationsService.GetReservationById(r.ReservationID) != null)
                    //_reservationsService.DeleteReservationById(r.ReservationID);
                    DataAccessHelper.RemoveReservation(_efContext, r);
            }
            _testReservations.Clear();

            _usersService.RemoveGuestPaymentMethod(_testPaymentMethod.GuestPaymentMethodID, _testUser.UserID);
            _unitsService.DeleteUnit(_testUnit.UnitID);
            _unitTypesService.DeleteUnitType(_testUnitType.UnitTypeID);
            _propertiesService.RemoveProperty(_testProperty.PropertyID);
            _usersService.DeleteUserById(_testUser.UserID);
            _usersService.DeleteUserById(_testOwner.UserID);
            _usersService.DeleteUserById(_testPropertyManager.UserID);
            _usersService.DeleteUserById(_testKeyManager.UserID);

            _efContext.SaveChanges();
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void AddReservationTest()
        {
            Reservation expected = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(expected);
            _reservationsService.AddReservation(expected);
            _efContext.SaveChanges();

            var actual = _reservationsService.GetReservationById(expected.ReservationID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetReservationsTest()
        {
            Dictionary<int, bool> newReservationsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; i++)
            {
                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + i,
                    BookingDate = DateTime.Now,
                    BookingStatus = (int)BookingStatus.CheckedIn,
                    DateArrival = DateTime.Now,
                    DateDeparture = DateTime.Now,
                    TotalPrice = 2.45M,                     
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)i },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)i },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + i,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = null,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();

                newReservationsFound.Add(_testReservations[i].ReservationID, false);
            }

            Reservation expected;
            var actual = _reservationsService.GetReservations();

            foreach (var actualObj in actual)
            {
                if (newReservationsFound.ContainsKey(actualObj.ReservationID))
                {
                    expected = _testReservations.Where(a => a.ReservationID == actualObj.ReservationID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newReservationsFound[actualObj.ReservationID] = true;
                }
            }

            if (newReservationsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "reservations", "GetReservations"));
        }

        [TestMethod]
        public void DeleteReservationByIdTest()
        {
            #region Delete successfully
            Reservation deleted = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture
            };
            _reservationsService.AddReservation(deleted);
            _efContext.SaveChanges();

            // _reservationsService.DeleteReservationById(deleted.ReservationID);
            DataAccessHelper.RemoveReservation(_efContext, deleted);

            var dbReservation = _reservationsService.GetReservationById(deleted.ReservationID);

            Assert.IsNull(dbReservation, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced user deletion
            #endregion
        }

        [TestMethod]
        public void DeleteTentativeReservationTest()
        {
            #region Delete successfully
            Reservation deleted = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.TentativeBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture
            };
            _reservationsService.AddReservation(deleted);
            _efContext.SaveChanges();

            _reservationsService.DeleteTentativeReservation(deleted.ReservationID);

            var dbReservation = _reservationsService.GetReservationById(deleted.ReservationID);

            Assert.IsNull(dbReservation, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced user deletion
            #endregion
        }

        [TestMethod]
        public void GetReservationsByUserIdTest()
        {
            #region No trips for user

            List<ReservationForUser> test = _reservationsService.GetReservationsByUserId(_testUser.UserID).ToList();
            Assert.AreEqual(0, test.Count);

            #endregion

            #region User's trips exists
            Dictionary<int, bool> newReservationsFound = new Dictionary<int, bool>();

            for (int j = 0; j < 5; j++)
            {
                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + j,
                    BookingDate = DateTime.Now,
                    BookingStatus = (int)BookingStatus.CheckedIn,
                    DateArrival = DateTime.Now,
                    DateDeparture = DateTime.Now,
                    TotalPrice = 2.45M,                     
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)j },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)j },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + j,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = null,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();

                newReservationsFound.Add(_testReservations[j].ReservationID, false);
            }

            ReservationForUser expected;
            var actual = _reservationsService.GetReservationsByUserId(_testUser.UserID);
            int i = 0;
            foreach (var actualObj in actual)
            {
                if (newReservationsFound.ContainsKey(actualObj.ReservationID))
                {
                    expected = new ReservationForUser()
                    {
                        BookingDate = _testReservations[i].BookingDate,
                        BookingStatus = _testReservations[i].BookingStatus,
                        ConfirmationID = _testReservations[i].ConfirmationID,
                        DateArrival = _testReservations[i].DateArrival,
                        DateDeparture = _testReservations[i].DateDeparture,
                        PropertyID = _testReservations[i].Property.PropertyID,
                        ReservationID = _testReservations[i].ReservationID,
                        Thumbnails = new List<string>(),
                        TotalPrice = _testReservations[i].TotalPrice,
                        TripBalance = _testReservations[i].TripBalance,
                        Property = _testProperty
                    };
                    i++;
                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newReservationsFound[actualObj.ReservationID] = true;
                }
            }

            if (newReservationsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "reservations", "GetReservationsByUserId"));
            #endregion
        }

        [TestMethod]
        public void GetCurrentReservationsByUserIdTest()
        {
            #region No trips for user

            List<ReservationForUser> test = _reservationsService.GetCurrentReservationsByUserId(_testUser.UserID).ToList();
            Assert.AreEqual(0, test.Count);

            #endregion

            #region User's trips exists
            Dictionary<int, bool> newReservationsFound = new Dictionary<int, bool>();

            for (int j = 0; j < 5; j++)
            {
                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + j,
                    BookingDate = DateTime.Now,
                    BookingStatus = (j % 2 == 0) ? (int)BookingStatus.CheckedIn : (int)BookingStatus.CheckedOut,
                    DateArrival = DateTime.Now,
                    DateDeparture = DateTime.Now,
                    TotalPrice = 2.45M,
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)j },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)j },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + j,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = null,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();

                if (j % 2 == 0) newReservationsFound.Add(_testReservations[j].ReservationID, false);
            }

            ReservationForUser expected;
            var actual = _reservationsService.GetCurrentReservationsByUserId(_testUser.UserID);
            int k = 0;
            int i = 0;
            foreach (var actualObj in actual)
            {
                if (newReservationsFound.ContainsKey(actualObj.ReservationID))
                {
                    i = 2 * k;
                    expected = new ReservationForUser()
                    {
                        BookingDate = _testReservations[i].BookingDate,
                        BookingStatus = _testReservations[i].BookingStatus,
                        ConfirmationID = _testReservations[i].ConfirmationID,
                        DateArrival = _testReservations[i].DateArrival,
                        DateDeparture = _testReservations[i].DateDeparture,
                        PropertyID = _testReservations[i].Property.PropertyID,
                        ReservationID = _testReservations[i].ReservationID,
                        Thumbnails = new List<string>(),
                        TotalPrice = _testReservations[i].TotalPrice,
                        TripBalance = _testReservations[i].TripBalance,
                        Property = _testProperty
                    };
                    k++;
                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newReservationsFound[actualObj.ReservationID] = true;
                }
            }

            if (newReservationsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "reservations", "GetCurrentReservationsByUserId"));
            #endregion
        }

        [TestMethod]
        public void GetReservationsForHomeOwnerTest()
        {
            #region Reservations for homeowner doesn't exist.
            _testProperty.User = _testOwner;
            _efContext.SaveChanges();

            var actual = _reservationsService.GetReservationsForProperty(_testProperty.PropertyID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(0, actual.Count());

            #endregion

            #region Reservations for homeowner exists
            Dictionary<string, bool> newReservationsFound = new Dictionary<string, bool>();

            for (int j = 0; j < 5; j++)
            {
                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + j,
                    BookingDate = DateTime.Now,
                    BookingStatus = (int)BookingStatus.CheckedIn,
                    DateArrival = DateTime.Now,
                    DateDeparture = DateTime.Now,
                    TotalPrice = 2.45M,                     
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)j },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)j },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + j,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = null,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();

                newReservationsFound.Add(_testReservations[j].ConfirmationID, false);
            }

            ReservationForHomeOwner expected;
            actual = _reservationsService.GetReservationsForProperty(_testProperty.PropertyID);
            int i = 0;
            foreach (var actualObj in actual)
            {
                if (newReservationsFound.ContainsKey(actualObj.ConfirmationId))
                {
                    expected = new ReservationForHomeOwner()
                    {
                        Booked = _testReservations[i].BookingDate,
                        ConfirmationId = _testReservations[i].ConfirmationID,
                        CheckIn = _testReservations[i].DateArrival,
                        CheckOut = _testReservations[i].DateDeparture,
                        GrossDue = _testReservations[i].TotalPrice,
                        CultureCode = _testReservations[i].Property.DictionaryCulture.CultureCode,
                        User = _testReservations[i].User,
                        TransactionFee = _testReservations[i].TransactionFee,
                        OwnerNet = (_testReservations[i].TotalPrice - _testReservations[i].TransactionFee),
                        PropertyId = _testReservations[i].Property.PropertyID,
                        UnitId = _testReservations[i].Unit.UnitID,
                        BookingStatus = _testReservations[i].BookingStatus
                    };
                    i++;
                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newReservationsFound[actualObj.ConfirmationId] = true;
                }
            }

            if (newReservationsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "reservations", "GetReservationsForHomeOwner"));
            #endregion
        }

        [TestMethod]
        public void SendTripDetailEmailsTest()
        {
            Reservation temp = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(temp);
            _reservationsService.AddReservation(temp);
            _efContext.SaveChanges();

            PropertyDetailsInvoice invoiceDetails = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                TotalAccomodation = 700.0m,
                LengthOfStay = 7,
                PricePerNight = 100.0m,
                TaxList = new List<Tax>(),
                InvoiceTotal = 770.0m
            };
            invoiceDetails.TaxList.ToList().Add(_testTax);

            UserAgreement agree = new UserAgreement()
            {
                DocumentType = DocumentType.PDF,
                ConfirmationCode = temp.ConfirmationID,
                CreatedDate = DateTime.Now.ToLocalizedDateString(),
                Amenities = "",
                CheckInDate = temp.DateArrival.ToLocalizedDateString(),
                CheckOutDate = temp.DateDeparture.ToLocalizedDateString(),
                GuestFullName = _testUser.FullName,
                GuestInitials = "RR",
                GuestsNumbers = _testUnit.MaxNumberOfGuests.ToString(),
                OwnerFullName = _testUser.FullName,
                OwnerInitials = "",
                PaymentInfo = "",
                PropertyAddress = string.Format("{0}, {1}, {2} {3}", _testProperty.Address1, _testProperty.Address2, _testProperty.City, _testProperty.State)
            };

            Invoice invoice = new Invoice();

            invoice.DocumentType = DocumentType.PDF;
            CultureInfo propertyCulture = invoiceDetails.Culture;
            propertyCulture.NumberFormat.CurrencyNegativePattern = 1;
            invoice.ConfirmationId = temp.ConfirmationID;
            invoice.GuestAddress1 = temp.User.Address1;
            invoice.GuestAddress2 = string.Format("{0} {1} {2}, {3}", temp.User.Address2, temp.User.City, temp.User.State, temp.User.ZIPCode);
            invoice.GuestFullName = temp.User.FullName;
            invoice.PropertyAddress = string.Format("{0} {1}, {2}", temp.Property.Address1, temp.Property.City, temp.Property.State);
            invoice.PropertyName = temp.Property.PropertyNameCurrentLanguage;
            invoice.Total = invoiceDetails.InvoiceTotal.ToString("c", propertyCulture);
            invoice.Subtotal = (invoiceDetails.SubtotalAccomodationAddons).ToString("c", propertyCulture);
            string salesAndTaxes = string.Empty;
            foreach (var tax in invoiceDetails.TaxList)
            {
                salesAndTaxes += string.Format("<tr><td class='sumAlignLeft'>{0}</td><td class='sumAlignRight'>{1}</td></tr>",
                   tax.NameCurrentLanguage, tax.CalculatedValue.ToString("c", propertyCulture));
            }
            invoice.SalesAndTaxes = salesAndTaxes;
            invoice.CreatedDate = DateTime.Now.ToLocalizedDateString();
            invoice.InvoiceId = temp.ConfirmationID;
            DateTime bookingDate = temp.BookingDate;
            invoice.DuePercent = string.Format("{0} {1}", bookingDate.AddDays(30) < temp.DateArrival ? "50" : "100", Common.Resources.Common.Invoice_Document_PercentDueBy);
            invoice.DueByDate = bookingDate.AddHours(24).ToLocalizedDateString() + " " + new DateTime(bookingDate.Ticks).ToString("hh:mm tt", CultureInfo.InvariantCulture);
            invoice.SecondPartDue = DateTime.Now.AddDays(30) < temp.DateArrival ?
                string.Format("<tr><td class='sumAlignLeft boldRow'>50% BALANCE DUE BY " + temp.DateArrival.AddDays(-30).ToShortDateString() + "</td><td class='sumAlignRight boldRow'>&nbsp;</td></tr>") : "<tr></tr>";
            string sevenDaysDiscount = string.Empty;
            invoice.SevenDayDiscount = sevenDaysDiscount;
            DateTime mostRestrictiveArrivalDate = temp.DateArrival;
            invoice.CancellationPolicy = "";
            invoice.InvoicePositions = string.Format(invoice.PositionsTag, Common.Resources.Common.Section_Invoice_Accomodation, invoiceDetails.PricePerNight.ToString("c", propertyCulture), invoiceDetails.LengthOfStay, invoiceDetails.TotalAccomodation.ToString("c", propertyCulture));

            TripDetails tripDetails = new TripDetails();

            tripDetails.DocumentType = DocumentType.PDF;
            tripDetails.GuestFullName = string.Format("{0}", temp.User.Firstname);
            tripDetails.BookedDate = DateTime.Now.ToString("g");
            tripDetails.CheckIn = temp.DateArrival.ToLongDateString();
            tripDetails.CheckOut = temp.DateDeparture.ToLongDateString();
            tripDetails.CheckInTime = new DateTime(temp.Property.CheckIn.Ticks).ToString("t");
            tripDetails.CheckOutTime = new DateTime(temp.Property.CheckOut.Ticks).ToString("t");
            tripDetails.ConfirmationCode = temp.ConfirmationID;
            tripDetails.DateBeforeArrival = temp.DateArrival.AddDays(-2).ToLocalizedDateString();
            tripDetails.Directions = "";
            tripDetails.GuestFullName = string.Format("{0} {1}", temp.User.Firstname, temp.User.Lastname);
            tripDetails.GuestsNumber = temp.NumberOfGuests.ToString();
            tripDetails.CardinalityPeron = temp.NumberOfGuests > 1 ? Common.Resources.Common.Cardinality_Persons.ToLower() : Common.Resources.Common.Cardinality_Person.ToLower();
            tripDetails.Latitude = temp.Property.Latitude.ToString().Replace(",", ".");
            tripDetails.Longitude = temp.Property.Longitude.ToString().Replace(",", ".");
            tripDetails.MaxNoOfGuests = temp.Unit.MaxNumberOfGuests.ToString();
            tripDetails.NightsNumber = invoiceDetails.LengthOfStay.ToString();
            tripDetails.PropertyAddress = string.Format("{0}, {1}, {2} {3}", temp.Property.Address1, temp.Property.Address2, temp.Property.City, temp.Property.State);
            tripDetails.PropertyCountry = temp.Property.DictionaryCountry.CountryNameCurrentLanguage;
            tripDetails.GuestEmail = temp.User.email;
            tripDetails.PropertyName = temp.Property.PropertyNameCurrentLanguage;
            tripDetails.CancellationPolicy = "";
            var psc = this._propertiesService.GetPropertyStaticContentByType(temp.Property.PropertyID, PropertyStaticContentType.ParkingInformation).SingleOrDefault();
            tripDetails.OnStreetParkingAvailable = psc != null ? psc.ContentValueCurrentLanguage : "";

            _reservationsService.SendTripDetailEmails("emailSubject", temp, invoiceDetails, agree, invoice, tripDetails);

            _testMessages = _messageService.GetMessages().ToList();

            Assert.IsNotNull(_testMessages);
            //
        }

        [TestMethod]
        public void GetReservationTransactionsForHomeOwnerTest()
        {
            #region Transactions for homeowner doesn't exist.
            _testProperty.User = _testOwner;
            _efContext.SaveChanges();

            var actual = _reservationsService.GetReservationTransactionsForHomeOwner(_testOwner.UserID, _testProperty.PropertyID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(0, actual.Count());

            #endregion

            #region Transactions for homeowner exists
            Dictionary<string, bool> newReservationsFound = new Dictionary<string, bool>();

            for (int j = 0; j < 5; j++)
            {

                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + j,
                    BookingDate = DateTime.Now,
                    BookingStatus = (int)BookingStatus.CheckedOut,
                    DateArrival = DateTime.Now,
                    DateDeparture = DateTime.Now,
                    TotalPrice = 2.45M,                     
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)j },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)j },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + j,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = null,
                    ReservationTransactions = new List<ReservationTransaction>(),
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                ReservationTransaction rt = new ReservationTransaction()
                {
                    Amount = 1.0m,
                     
                    TransactionCode = _efContext.TransactionCodes.Where(t => t.TransactionCode1.Value.Equals((int)TransactionCodes.Tax)).FirstOrDefault(),
                    TransactionDate = DateTime.Now,

                };
                rt.SetNameValue("name");
                _testTransactions.Add(rt);
                temp.ReservationTransactions.Add(rt);
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);

                _efContext.SaveChanges();

                newReservationsFound.Add(_testReservations[j].ConfirmationID, false);
            }

            ReservationForHomeOwner expected;
            actual = _reservationsService.GetReservationTransactionsForHomeOwner(_testOwner.UserID, _testProperty.PropertyID);
            int i = 0;
            foreach (var actualObj in actual)
            {
                if (newReservationsFound.ContainsKey(actualObj.ConfirmationId))
                {
                    expected = new ReservationForHomeOwner()
                    {
                        Booked = _testReservations[i].BookingDate,
                        ConfirmationId = _testReservations[i].ConfirmationID,
                        CheckIn = _testReservations[i].DateArrival,
                        CheckOut = _testReservations[i].DateDeparture,
                        GrossDue = _testReservations[i].TotalPrice,
                        CultureCode = _testReservations[i].Property.DictionaryCulture.CultureCode,
                        User = _testReservations[i].User,
                        TransactionFee = _testReservations[i].TransactionFee,
                        OwnerNet = (_testReservations[i].TotalPrice - _testReservations[i].TransactionFee),
                        PropertyId = _testReservations[i].Property.PropertyID,
                        UnitId = _testReservations[i].Unit.UnitID,
                        BookingStatus = _testReservations[i].BookingStatus,
                        TaxValue = _testTransactions[i].Amount
                    };
                    i++;
                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newReservationsFound[actualObj.ConfirmationId] = true;
                }
            }

            if (newReservationsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "reservations", "GetReservationsForHomeOwner"));
            #endregion
        }

        [TestMethod]
        public void GetReservationsForHomeOwnerUnitTest()
        {
            #region Reservations for homeowner's unit doesn't exist.
            _testProperty.User = _testOwner;

            _efContext.SaveChanges();

            var actual = _reservationsService.GetReservationsForUnit(_testUnit.UnitID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(0, actual.Count());

            #endregion

            #region Reservations for homeowner's unit exists
            Dictionary<string, bool> newReservationsFound = new Dictionary<string, bool>();

            int[] x = new int[5] {  (int) BookingStatus.Cancelled,
                                    (int) BookingStatus.TentativeBooking,
                                    (int) BookingStatus.Withdrew,
                                    (int) BookingStatus.CheckedIn,
                                    (int) BookingStatus.CheckedIn };


            for (int j = 0; j < 5; j++)
            {
                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + j,
                    BookingDate = DateTime.Now,
                    BookingStatus = x[j],
                    DateArrival = DateTime.Now,
                    DateDeparture = DateTime.Now,
                    TotalPrice = 2.45M,
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)j },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)j },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + j,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = null,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();
                if (_testReservations[j].BookingStatus == (int)BookingStatus.CheckedIn)
                    newReservationsFound.Add(_testReservations[j].ConfirmationID, false);
            }

            ReservationForHomeOwner expected;
            actual = _reservationsService.GetReservationsForUnit(_testUnit.UnitID);
            int i = 3;
            foreach (var actualObj in actual)
            {
                if (newReservationsFound.ContainsKey(actualObj.ConfirmationId))
                {
                    expected = new ReservationForHomeOwner()
                    {
                        Booked = _testReservations[i].BookingDate,
                        ConfirmationId = _testReservations[i].ConfirmationID,
                        CheckIn = _testReservations[i].DateArrival,
                        CheckOut = _testReservations[i].DateDeparture,
                        GrossDue = _testReservations[i].TotalPrice,
                        CultureCode = _testReservations[i].Property.DictionaryCulture.CultureCode,
                        User = _testReservations[i].User,
                        TransactionFee = _testReservations[i].TransactionFee,
                        OwnerNet = (_testReservations[i].TotalPrice - _testReservations[i].TransactionFee),
                        PropertyId = _testReservations[i].Property.PropertyID,
                        UnitId = _testReservations[i].Unit.UnitID,
                        BookingStatus = _testReservations[i].BookingStatus
                    };
                    i++;
                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newReservationsFound[actualObj.ConfirmationId] = true;
                }
            }

            if (newReservationsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "reservations", "GetReservationsForHomeOwner"));
            #endregion
        }

        [TestMethod]
        public void GetNotFinalizedOrCanceledReservationsTest()
        {
            #region Canceled reservations doesn't exist.

            var actual = _reservationsService.GetNotFinalizedOrCanceledReservations();

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(0, actual.Count());

            #endregion

            #region Some canceled reservations exists
            Dictionary<int, bool> newReservationsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; i++)
            {
                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + i,
                    BookingDate = (i == 2) ? DateTime.Now.AddMinutes(-16) : DateTime.Now,
                    BookingStatus = (i != 4) ? ((i == 2) ? (int)BookingStatus.TentativeBooking : (int)BookingStatus.Cancelled) : (int)BookingStatus.CheckedIn,
                    DateArrival = DateTime.Now,
                    DateDeparture = DateTime.Now,
                    TotalPrice = 2.45M,
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)i },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)i },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + i,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = null,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();

                if (i != 4) newReservationsFound.Add(_testReservations[i].ReservationID, false);
            }

            Reservation expected;
            actual = _reservationsService.GetNotFinalizedOrCanceledReservations();

            foreach (var actualObj in actual)
            {
                if (newReservationsFound.ContainsKey(actualObj.ReservationID))
                {
                    expected = _testReservations.Where(a => a.ReservationID == actualObj.ReservationID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newReservationsFound[actualObj.ReservationID] = true;
                }
            }

            if (newReservationsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "reservations", "GetNotFinalizedOrCanceledReservations"));
            #endregion
        }

        [TestMethod]
        public void GetBookedReservationsAfter24HoursTest()
        {
            #region Booked reservations doesn't exist.

            var actual = _reservationsService.GetBookedReservationsAfter24Hours();

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(0, actual.Count());

            #endregion

            #region Some booked reservations exists
            Dictionary<int, bool> newReservationsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; i++)
            {
                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + i,
                    BookingDate = DateTime.Now.AddHours(-(24 + i)),
                    BookingStatus = (int)BookingStatus.ReceivedBooking,
                    DateArrival = DateTime.Now,
                    DateDeparture = DateTime.Now,
                    TotalPrice = 2.45M,                     
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)i },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)i },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + i,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = null,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();

                newReservationsFound.Add(_testReservations[i].ReservationID, false);
            }

            Reservation expected;
            actual = _reservationsService.GetBookedReservationsAfter24Hours();

            foreach (var actualObj in actual)
            {
                if (newReservationsFound.ContainsKey(actualObj.ReservationID))
                {
                    expected = _testReservations.Where(a => a.ReservationID == actualObj.ReservationID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newReservationsFound[actualObj.ReservationID] = true;
                }
            }

            if (newReservationsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "reservations", "GetBookedReservationsAfter24Hours"));
            #endregion
        }

        [TestMethod]
        public void GetUpcoming30DaysUpfrontReservationsTest()
        {
            #region Upcoming reservations doesn't exist.

            var actual = _reservationsService.GetUpcomingXDaysUpfrontReservations();

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(0, actual.Count());

            #endregion

            #region Some upcoming reservations exists
            Dictionary<int, bool> newReservationsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; i++)
            {
                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + i,
                    BookingDate = DateTime.Now.AddDays(-35),
                    BookingStatus = (int)BookingStatus.FinalizedBooking,
                    DateArrival = DateTime.Now.AddDays(i * 5),
                    DateDeparture = DateTime.Now.AddDays(i * 6),
                    TotalPrice = 2.45M,                     
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)i },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)i },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + i,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = null,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();

                newReservationsFound.Add(_testReservations[i].ReservationID, false);
            }

            Reservation expected;
            actual = _reservationsService.GetUpcomingXDaysUpfrontReservations();

            foreach (var actualObj in actual)
            {
                if (newReservationsFound.ContainsKey(actualObj.ReservationID))
                {
                    expected = _testReservations.Where(a => a.ReservationID == actualObj.ReservationID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newReservationsFound[actualObj.ReservationID] = true;
                }
            }

            if (newReservationsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "reservations", "GetUpcoming30DaysUpfrontReservations"));
            #endregion
        }

        [TestMethod]
        public void FindMostRestrictiveArrivalDateForComparisionTest()
        {
            int lastId = 0;
            for (int i = 0; i < 5; i++)
            {
                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + i,
                    BookingDate = DateTime.Now.AddDays(-10),
                    BookingStatus = (int)BookingStatus.FinalizedBooking,
                    DateArrival = (i % 2 == 0) ? DateTime.Now.AddDays(i) : DateTime.Now.AddDays(-i),
                    DateDeparture = DateTime.Now.AddDays((i + 1) * 6),
                    TotalPrice = 2.45M,                     
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)i },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)i },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + i,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = (i == 0) ? (int?)null : lastId,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();
                lastId = temp.ReservationID;
            }

            //TODO: add more cases and check correctness

            DateTime newArrival = DateTime.Now.AddDays(10);

            string expected = DateTime.Now.AddDays(0).ToShortDateString();
            string actual = _reservationsService.FindMostRestrictiveArrivalDateForComparision(_testReservations[0], newArrival).ToShortDateString();
            Assert.AreEqual(expected, actual);

            expected = DateTime.Now.AddDays(-1).ToShortDateString();
            actual = _reservationsService.FindMostRestrictiveArrivalDateForComparision(_testReservations[1], newArrival).ToShortDateString();
            Assert.AreEqual(expected, actual);

            expected = DateTime.Now.AddDays(-1).ToShortDateString();
            actual = _reservationsService.FindMostRestrictiveArrivalDateForComparision(_testReservations[2], newArrival).ToShortDateString();
            Assert.AreEqual(expected, actual);

            expected = DateTime.Now.AddDays(-3).ToShortDateString();
            actual = _reservationsService.FindMostRestrictiveArrivalDateForComparision(_testReservations[3], newArrival).ToShortDateString();
            Assert.AreEqual(expected, actual);

            expected = DateTime.Now.AddDays(-3).ToShortDateString();
            actual = _reservationsService.FindMostRestrictiveArrivalDateForComparision(_testReservations[4], newArrival).ToShortDateString();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void FindOriginalReservationTest()
        {
            int lastId = 0;
            for (int i = 0; i < 5; i++)
            {
                Reservation temp = new Reservation()
                {
                    Property = _testProperty,
                    Unit = _testUnit,
                    UnitType = _testUnitType,
                    User = _testUser,
                    ConfirmationID = "abcd_" + i,
                    BookingDate = DateTime.Now.AddDays(-10),
                    BookingStatus = (int)BookingStatus.FinalizedBooking,
                    DateArrival = (i % 2 == 0) ? DateTime.Now.AddDays(i) : DateTime.Now.AddDays(-i),
                    DateDeparture = DateTime.Now.AddDays((i + 1) * 6),
                    TotalPrice = 2.45M,                     
                    TripBalance = 123.4M,
                    Agreement = new byte[] { 234, 54, 23, 54, 2, (byte)i },
                    GuestPaymentMethod = _testPaymentMethod,
                    NumberOfGuests = 1,
                    Invoice = new byte[] { 12, 3, 5, 23, 54, (byte)i },
                    TransactionFee = 23.67M,
                    TransactionToken = "24r343rt_" + i,
                    UpfrontPaymentProcessed = null,
                    ParentReservationId = (i == 0) ? (int?)null : lastId,
                    LinkAuthorizationToken = "435df",
                    DictionaryCulture = _testCulture
                };
                _testReservations.Add(temp);
                _reservationsService.AddReservation(temp);
                _efContext.SaveChanges();
                lastId = temp.ReservationID;
            }

            #region No parent reservation

            Reservation expected = _testReservations[0];

            Reservation actual = _reservationsService.FindOriginalReservation(_testReservations[0]);

            ExtendedAssert.AreEqual(expected, actual);

            #endregion

            #region Parent reservations exists

            for (int j = 0; j < 4; j++)
            {
                actual = _reservationsService.FindOriginalReservation(_testReservations[j]);

                ExtendedAssert.AreEqual(expected, actual);
            }

            #endregion

        }

        [TestMethod]
        public void GenerateAgreementDocumentTest()
        {
            Reservation temp = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(temp);
            _reservationsService.AddReservation(temp);
            _efContext.SaveChanges();

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                TotalAccomodation = 700.0m,
                LengthOfStay = 7,
                PricePerNight = 100.0m,
                TaxList = new List<Tax>(),
                InvoiceTotal = 770.0m
            };
            invoice.TaxList.ToList().Add(_testTax);

            UserAgreement expected = new UserAgreement()
            {
                DocumentType = DocumentType.PDF,
                ConfirmationCode = temp.ConfirmationID,
                CreatedDate = DateTime.Now.ToLocalizedDateString(),
                Amenities = "",
                CheckInDate = temp.DateArrival.ToLocalizedDateString(),
                CheckOutDate = temp.DateDeparture.ToLocalizedDateString(),
                GuestFullName = _testUser.FullName,
                GuestInitials = "RR",
                GuestsNumbers = _testUnit.MaxNumberOfGuests.ToString(),
                OwnerFullName = _testUser.FullName,
                OwnerInitials = "",
                PaymentInfo = "",
                PropertyAddress = string.Format("{0}, {1}, {2} {3}", _testProperty.Address1, _testProperty.Address2, _testProperty.City, _testProperty.State),
                TermsOfServiceHref = string.Format("http://{0}{1}", this._settingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ExtranetURL), "/Home/TermsOfService")
            };

            UserAgreement actual = _reservationsService.GenerateAgreementDocument(temp, invoice);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GenerateInvoiceDocumentTest()
        {
            Reservation temp = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(temp);
            _reservationsService.AddReservation(temp);
            _efContext.SaveChanges();

            PropertyDetailsInvoice invoiceDetails = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                TotalAccomodation = 700.0m,
                LengthOfStay = 7,
                PricePerNight = 100.0m,
                TaxList = new List<Tax>(),
                InvoiceTotal = 770.0m
            };
            invoiceDetails.TaxList.ToList().Add(_testTax);

            Invoice expected = new Invoice();

            #region Expected invoice generation
            expected.DocumentType = DocumentType.PDF;
            CultureInfo propertyCulture = invoiceDetails.Culture;

            expected.ConfirmationId = temp.ConfirmationID;
            expected.GuestAddress1 = temp.User.Address1;
            expected.GuestAddress2 = string.Format("{0} {1} {2}, {3}", temp.User.Address2, temp.User.City, temp.User.State, temp.User.ZIPCode);
            expected.GuestFullName = temp.User.FullName;
            expected.PropertyAddress = string.Format("{0} {1}, {2}", temp.Property.Address1, temp.Property.City, temp.Property.State);
            expected.PropertyName = temp.Property.PropertyNameCurrentLanguage;
            expected.Total = invoiceDetails.InvoiceTotal.ToString("c", propertyCulture);
            expected.Subtotal = (invoiceDetails.SubtotalAccomodationAddons).ToString("c", propertyCulture);
            string salesAndTaxes = string.Empty;
            foreach (var tax in invoiceDetails.TaxList)
            {
                salesAndTaxes += string.Format("<tr><td class='sumAlignLeft'>{0}</td><td class='sumAlignRight'>{1}</td></tr>",
                   tax.NameCurrentLanguage, tax.CalculatedValue.ToString("c", propertyCulture));
            }
            expected.SalesAndTaxes = salesAndTaxes;
            expected.CreatedDate = DateTime.Now.ToLocalizedDateString();
            expected.InvoiceId = temp.ConfirmationID;
            DateTime bookingDate = temp.BookingDate;
            expected.DuePercent = string.Format("{0} {1}", bookingDate.AddDays(30) < temp.DateArrival ? "50" : "100", Common.Resources.Common.Invoice_Document_PercentDueBy);
            expected.DueByDate = bookingDate.AddHours(24).ToLocalizedDateString() + " " + new DateTime(bookingDate.Ticks).ToString("hh:mm tt", CultureInfo.InvariantCulture);
            expected.SecondPartDue = DateTime.Now.AddDays(30) < temp.DateArrival ?
                string.Format("<tr><td class='sumAlignLeft boldRow'>50% BALANCE DUE BY " + temp.DateArrival.AddDays(-30).ToShortDateString() + "</td><td class='sumAlignRight boldRow'>&nbsp;</td></tr>") : "<tr></tr>";
            string sevenDaysDiscount = string.Empty;
            expected.SevenDayDiscount = sevenDaysDiscount;
            DateTime mostRestrictiveArrivalDate = temp.DateArrival;
            expected.CancellationPolicy = "";
            expected.InvoicePositions = string.Format(expected.PositionsTag, Common.Resources.Common.Section_Invoice_Accomodation, invoiceDetails.PricePerNight.ToString("c", propertyCulture), invoiceDetails.LengthOfStay, invoiceDetails.TotalAccomodation.ToString("c", propertyCulture));
            #endregion

            Invoice actual = _reservationsService.GenerateInvoiceDocument(temp, invoiceDetails);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GenerateTripDetailsDocumentTest()
        {
            CultureInfo cInfoUser = new CultureInfo(CultureCode.pl_PL);
            Reservation temp = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture

            };
            _testReservations.Add(temp);
            _reservationsService.AddReservation(temp);
            _efContext.SaveChanges();

            PropertyDetailsInvoice invoiceDetails = new PropertyDetailsInvoice()
            {
                Culture = new CultureInfo("en-US"),
                TotalAccomodation = 700.0m,
                LengthOfStay = 7,
                PricePerNight = 100.0m,
                TaxList = new List<Tax>(),
                InvoiceTotal = 770.0m
            };
            invoiceDetails.TaxList.ToList().Add(_testTax);

            TripDetails expected = new TripDetails();

            #region Generate expected TripDetails
            expected.DocumentType = DocumentType.PDF;
            expected.GuestFullName = string.Format("{0}", temp.User.Firstname);
            expected.BookedDate = DateTime.Now.ToString("g");
            expected.CheckIn = temp.DateArrival.ToLongDateString();
            expected.CheckOut = temp.DateDeparture.ToLongDateString();
            expected.CheckInTime = new DateTime(temp.Property.CheckIn.Ticks).ToString("t");
            expected.CheckOutTime = new DateTime(temp.Property.CheckOut.Ticks).ToString("t");
            expected.ConfirmationCode = temp.ConfirmationID;
            expected.DateBeforeArrival = temp.DateArrival.AddDays(-2).ToLocalizedDateString();
            expected.Directions = "";
            expected.GuestFullName = string.Format("{0} {1}", temp.User.Firstname, temp.User.Lastname);
            expected.GuestsNumber = temp.NumberOfGuests.ToString();
            expected.CardinalityPeron = temp.NumberOfGuests > 1 ? Common.Resources.Common.Cardinality_Persons.ToLower() : Common.Resources.Common.Cardinality_Person.ToLower();
            expected.Latitude = temp.Property.Latitude.ToString().Replace(",", ".");
            expected.Longitude = temp.Property.Longitude.ToString().Replace(",", ".");
            expected.MaxNoOfGuests = temp.Unit.MaxNumberOfGuests.ToString();
            expected.NightsNumber = invoiceDetails.LengthOfStay.ToString();
            expected.PropertyAddress = string.Format("{0}, {1}, {2} {3}", temp.Property.Address1, temp.Property.Address2, temp.Property.City, temp.Property.State);
            expected.PropertyCountry = temp.Property.DictionaryCountry.CountryNameCurrentLanguage;
            expected.GuestEmail = temp.User.email;
            expected.PropertyName = temp.Property.PropertyNameCurrentLanguage;
            DateTime mostRestrictiveArrivalDate = temp.DateArrival;
            expected.CancellationPolicy = "";
            var psc = this._propertiesService.GetPropertyStaticContentByType(temp.Property.PropertyID, PropertyStaticContentType.ParkingInformation).SingleOrDefault();
            expected.OnStreetParkingAvailable = psc != null ? psc.ContentValueCurrentLanguage : "";
            //var directions = this._propertiesService.GetPropertyDirectionsToAirport(temp.Property.PropertyID);
            //foreach (DirectionToAirport direction in directions)
            //{
            //    direction.SetName(LanguageCode.en, "AiroprtName");
            //    expected.Directions += string.Format("<a href='{0}'>{1}</a><br />", direction.DirectionLink, direction.Name.ToString());
            //}
            #endregion

            TripDetails actual = _reservationsService.GenerateTripDetailsDocument(temp, invoiceDetails);

            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CancellationPolicyTest()
        {
            List<string> cancellationTexts = new List<string>();
            CultureInfo cInfoUser = new CultureInfo(CultureCode.en_US.ToString());

            #region Test 1

            DateTime bookingDate = DateTime.Now.AddHours(-20);
            DateTime start = DateTime.Now.AddDays(35);
            DateTime h24AfterBooking = bookingDate.AddHours(24);
            DateTime d30BeforeArrival = start.AddDays(-30);

            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyHoursHalfRefund, d30BeforeArrival.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(Common.Resources.CancellationPolicy.CancellationPolicyTryingToResell);

            string actual = _reservationsService.CancellationPolicy(start, bookingDate, cInfoUser);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(cancellationTexts[0], actual);

            cancellationTexts.Clear();

            #endregion

            #region Test 2

            bookingDate = DateTime.Now.AddHours(-25);
            start = DateTime.Now.AddDays(35);
            h24AfterBooking = bookingDate.AddHours(24);
            d30BeforeArrival = start.AddDays(-30);

            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyHoursHalfRefund, d30BeforeArrival.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(Common.Resources.CancellationPolicy.CancellationPolicyTryingToResell);

            actual = _reservationsService.CancellationPolicy(start, bookingDate, cInfoUser);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(cancellationTexts[1], actual);

            cancellationTexts.Clear();

            #endregion

            #region Test 3

            bookingDate = DateTime.Now.AddHours(-20);
            start = DateTime.Now.AddDays(20);
            h24AfterBooking = bookingDate.AddHours(24);
            d30BeforeArrival = start.AddDays(-30);

            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyHoursHalfRefund, d30BeforeArrival.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(Common.Resources.CancellationPolicy.CancellationPolicyTryingToResell);

            actual = _reservationsService.CancellationPolicy(start, bookingDate, cInfoUser);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(cancellationTexts[2], actual);

            cancellationTexts.Clear();

            #endregion

            #region Test 4

            bookingDate = DateTime.Now.AddHours(-25);
            start = DateTime.Now.AddDays(20);
            h24AfterBooking = bookingDate.AddHours(24);
            d30BeforeArrival = start.AddDays(-30);

            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyHoursHalfRefund, d30BeforeArrival.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(Common.Resources.CancellationPolicy.CancellationPolicyTryingToResell);

            actual = _reservationsService.CancellationPolicy(start, bookingDate, cInfoUser);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(cancellationTexts[3], actual);

            cancellationTexts.Clear();

            #endregion

            #region Test 5

            bookingDate = DateTime.Now.AddHours(-30);
            start = DateTime.Now.AddDays(10);
            h24AfterBooking = bookingDate.AddHours(24);
            d30BeforeArrival = start.AddDays(-30);

            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyHoursHalfRefund, d30BeforeArrival.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(String.Format(Common.Resources.CancellationPolicy.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser)));
            cancellationTexts.Add(Common.Resources.CancellationPolicy.CancellationPolicyTryingToResell);

            actual = _reservationsService.CancellationPolicy(start, bookingDate, cInfoUser);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(cancellationTexts[3], actual);

            cancellationTexts.Clear();

            #endregion
        }

        [TestMethod]
        public void SetReservationStatusTest()
        {
            Reservation temp = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.CheckedOut,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = true,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture
            };

            _testReservations.Add(temp);
            _reservationsService.AddReservation(temp);
            _efContext.SaveChanges();

            var reservation = _reservationsService.GetReservationById(temp.ReservationID);
            _reservationsService.SetReservationStatus(BookingStatus.Completed, reservation.ReservationID);
            _efContext.SaveChanges();

            reservation = _reservationsService.GetReservationById(reservation.ReservationID);
            Assert.AreEqual(reservation.BookingStatus, (int)BookingStatus.Completed);
        }

        [TestMethod]
        public void GetRervationsByBookingStatusTest()
        {
            Reservation temp = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.CheckedOut,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = true,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture
            };

            _testReservations.Add(temp);
            _reservationsService.AddReservation(temp);

            Reservation temp1 = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_b",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.DatesChanged,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_b",
                UpfrontPaymentProcessed = true,
                ParentReservationId = null,
                LinkAuthorizationToken = "2454353535dt23",
                DictionaryCulture = _testCulture
            };

            _testReservations.Add(temp1);
            _reservationsService.AddReservation(temp1);
            _efContext.SaveChanges();

            var reservations = _reservationsService.GetRervationsByBookingStatus(BookingStatus.CheckedOut);
            ExtendedAssert.AreEqual(temp, reservations.First());
        }

        #endregion

        [TestCategory("Key Management Form")]
        [TestMethod]
        public void GetKeyManagementReservationsByHomeownerTest()
        {
            // Assign
            Reservation temp = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = true,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture

            };

            _testReservations.Add(temp);
            _reservationsService.AddReservation(temp);

            Reservation temp2 = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.ReceivedBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = true,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture

            };
            _testReservations.Add(temp2);
            _reservationsService.AddReservation(temp2);

            _efContext.SaveChanges();

            // Act
            var keyManagementListData = _reservationsService.GetKeyManagementReservationsByHomeowner(this._testOwner.UserID);

            // Assert
            int expectedPropertyId = _testProperty.PropertyID;
            int expectedNumberOfReservations = 1;
            Assert.AreEqual(expectedNumberOfReservations, keyManagementListData.Count());
            Assert.AreEqual(expectedPropertyId, keyManagementListData[0].Property.PropertyID);
        }

        [TestCategory("Key Management Form")]
        [TestMethod]
        public void GetKeyManagementReservationsByPropertyManagerTest()
        {
            // Assign
            Reservation temp = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = true,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture

            };

            _testReservations.Add(temp);
            _reservationsService.AddReservation(temp);

            Reservation temp2 = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.ReceivedBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = true,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture

            };
            _testReservations.Add(temp2);
            _reservationsService.AddReservation(temp2);

            _efContext.SaveChanges();

            // Act
            var keyManagementOwnerListData = _reservationsService.GetKeyManagementReservationsByManagerRole(this._testOwner.UserID, RoleLevel.PropertyManager);
            var keyManagementPropertyManagerListData = _reservationsService.GetKeyManagementReservationsByManagerRole(this._testPropertyManager.UserID, RoleLevel.PropertyManager);
            var keyManagementKeyManagerListData = _reservationsService.GetKeyManagementReservationsByManagerRole(this._testPropertyManager.UserID, RoleLevel.KeyManager);

            // Assert
            int expectedPropertyId = _testProperty.PropertyID;
            int expectedNumberOfReservations = 1;
            Assert.AreEqual(expectedNumberOfReservations, keyManagementPropertyManagerListData.Count());
            Assert.AreEqual(expectedPropertyId, keyManagementPropertyManagerListData[0].Property.PropertyID);
            Assert.AreEqual(0, keyManagementOwnerListData.Count());
            Assert.AreEqual(0, keyManagementKeyManagerListData.Count());
        }

        [TestCategory("Key Management Form")]
        [TestMethod]
        public void GetKeyManagementReservationsByKeyManagerTest()
        {
            // Assign
            Reservation temp = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = true,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture

            };

            _testReservations.Add(temp);
            _reservationsService.AddReservation(temp);

            Reservation temp2 = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_a",
                BookingDate = DateTime.Now.AddDays(-10),
                BookingStatus = (int)BookingStatus.ReceivedBooking,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                 
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 1 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_a",
                UpfrontPaymentProcessed = true,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture

            };
            _testReservations.Add(temp2);
            _reservationsService.AddReservation(temp2);


            _efContext.SaveChanges();

            // Act
            var keyManagementOwnerListData = _reservationsService.GetKeyManagementReservationsByManagerRole(this._testOwner.UserID, RoleLevel.PropertyManager);
            var keyManagementPropertyManagerListData = _reservationsService.GetKeyManagementReservationsByManagerRole(this._testKeyManager.UserID, RoleLevel.PropertyManager);
            var keyManagementKeyManagerListData = _reservationsService.GetKeyManagementReservationsByManagerRole(this._testKeyManager.UserID, RoleLevel.KeyManager);

            // Assert
            int expectedPropertyId = _testProperty.PropertyID;
            int expectedNumberOfReservations = 1;
            Assert.AreEqual(expectedNumberOfReservations, keyManagementKeyManagerListData.Count());
            Assert.AreEqual(expectedPropertyId, keyManagementKeyManagerListData[0].Property.PropertyID);
            Assert.AreEqual(0, keyManagementOwnerListData.Count());
            Assert.AreEqual(0, keyManagementPropertyManagerListData.Count());
        }
    }
}