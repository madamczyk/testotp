﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccess;
using System.Collections.Generic;
using BusinessLogic.ServiceContracts;
using System.Configuration;
using Moq;
using BusinessLogic.Services;
using DataAccess.Enums;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class TagGroupsServiceTest
    {
        #region Fields
        /// <summary>
        /// list of amenity groups used in test methods
        /// </summary>
        private List<TagGroup> _testTagGroups = new List<TagGroup>();
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;
        #endregion

        #region Services
        private ITagGroupsService _TagGroupsService;
        private ITagsService _TagService;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);

            _TagGroupsService = new TagGroupsService(mock.Object);
            _TagService = new TagsService(mock.Object);
            #endregion
        }

        /// <summary>
        /// Cleanup the data before each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (TagGroup ag in _testTagGroups)
            {
                if (_TagGroupsService.GetTagGroupById(ag.TagGroupId) != null)
                {
                    List<int> tagsID = new List<int>();
                    foreach(Tag tag in ag.Tags)
                    {
                        tagsID.Add(tag.TagID);
                    }
                    foreach (int id in tagsID)
                    {
                        _TagService.DeleteTag(id);
                    }
                    _TagGroupsService.DeleteTagGroup(ag.TagGroupId);
                    tagsID.Clear();
                }
            }
            _testTagGroups.Clear();
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void GetTagGroupByIdTest()
        {
            TagGroup expected = new TagGroup();
            expected.SetNameValue("GetTagGroupByIdTest_Name");

            _TagGroupsService.AddTagGroup(expected);
            _efContext.SaveChanges();
            _testTagGroups.Add(expected);

            TagGroup actual = _TagGroupsService.GetTagGroupById(expected.TagGroupId);

            Assert.IsNotNull(actual);

            Assert.AreEqual(expected.TagGroupId, actual.TagGroupId);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Name_i18n, actual.Name_i18n);
            Assert.AreEqual(expected.NameCurrentLanguage, actual.NameCurrentLanguage);

            Assert.AreEqual(expected.Tags, actual.Tags);
        }

        [TestMethod]
        public void AddTagGroupTest()
        {
            TagGroup expected = new TagGroup();
            Tag newTag = new Tag();
            newTag.SetNameValue("TagGroupTest_Name");
            expected.Tags.Add(newTag);
            expected.SetNameValue("AddTagGroupTest_Name");

            _TagGroupsService.AddTagGroup(expected);
            _efContext.SaveChanges();
            _testTagGroups.Add(expected);

            TagGroup actual = _TagGroupsService.GetTagGroupById(expected.TagGroupId);

            Assert.IsNotNull(actual);

            Assert.AreEqual(expected.TagGroupId, actual.TagGroupId);
            Assert.AreEqual(expected.Tags, actual.Tags);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Name_i18n, actual.Name_i18n);
            Assert.AreEqual(expected.NameCurrentLanguage, actual.NameCurrentLanguage);

        }

        [TestMethod]
        public void AddTagGroupRequiredFieldsTest()
        {
            #region Test field AmenityIcon
            // AmenityIcon is of type int and cannot be nullified
            #endregion

            #region Test field Name_i18n
            TagGroup TagGroup2 = new TagGroup();

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _TagGroupsService.AddTagGroup(TagGroup2);
                _efContext.SaveChanges();
            }, "Name_i18n");
            #endregion
        }

        [TestMethod]
        public void GetTagGroupsTest()
        {
            TagGroup[] newTagGroups = new TagGroup[5];
            Dictionary<int, bool> newTagGroupsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newTagGroups[i] = new TagGroup();
                newTagGroups[i].SetNameValue("GetTagGroupsTest_Name_" + i);

                _TagGroupsService.AddTagGroup(newTagGroups[i]);
                _efContext.SaveChanges();
                _testTagGroups.Add(newTagGroups[i]);

                newTagGroupsFound.Add(newTagGroups[i].TagGroupId, false);
            }

            var TagGroups = _TagGroupsService.GetTagGroups();
            foreach (TagGroup TagGroup in TagGroups)
            {
                if (newTagGroupsFound.ContainsKey(TagGroup.TagGroupId))
                {
                    TagGroup temp = _TagGroupsService.GetTagGroupById(TagGroup.TagGroupId);

                    Assert.IsNotNull(temp);
                    Assert.AreEqual(TagGroup.TagGroupId, temp.TagGroupId);
                    Assert.AreEqual(TagGroup.Name, temp.Name);
                    Assert.AreEqual(TagGroup.Name_i18n, temp.Name_i18n);
                    Assert.AreEqual(TagGroup.NameCurrentLanguage, temp.NameCurrentLanguage);
                    Assert.AreEqual(TagGroup.Tags, temp.Tags);

                    newTagGroupsFound[TagGroup.TagGroupId] = true;
                }
            }

            //if (newTagGroupsFound.Any(nag => nag.Value == false))
            //    Assert.Fail(String.Format(Messages.NotPresentInCollection, "amenity groups", "GetTagGroups"));
        }

        [TestMethod]
        public void DeleteTagGroupTest()
        {
            #region Delete successfully
            TagGroup TagGroup = new TagGroup();
            TagGroup.SetNameValue("DeleteTagGroupTest_Name_Successfully");
            
            _TagGroupsService.AddTagGroup(TagGroup);
            _efContext.SaveChanges();
            _TagGroupsService.DeleteTagGroup(TagGroup.TagGroupId);

            TagGroup dbTagGroup = _TagGroupsService.GetTagGroupById(TagGroup.TagGroupId);

            Assert.IsNull(dbTagGroup, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced amenity group deletion
            TagGroup referencedTagGroup = new TagGroup();
            referencedTagGroup.SetNameValue("DeleteTagGroupTest_Name_Referenced");

            _TagGroupsService.AddTagGroup(referencedTagGroup);
            _efContext.SaveChanges();

            Tag referencedTag = new Tag()
            {
                TagGroup = referencedTagGroup
            };

            referencedTag.SetNameValue("DeleteTagGroupTest_Title_Referenced");


            _TagService.AddTag(referencedTag);
            _efContext.SaveChanges();

            bool exceptionCaught = false;
            try
            {
                _TagGroupsService.DeleteTagGroup(referencedTagGroup.TagGroupId);
            }
            catch (Exception)
            {
                exceptionCaught = true;
            }

            _TagService.DeleteTag(referencedTag.TagID);

            if (!exceptionCaught)
                Assert.Fail(Messages.DeletionShouldNotSucceed);
            #endregion
        }

        [TestMethod]
        public void ChangeTagGroupPositionTest()
        {
            #region Add tags
            TagGroup[] newTagGroups = new TagGroup[5];

            for (int i = 0; i < 5; ++i)
            {
                newTagGroups[i] = new TagGroup()
                {
                    Position = _TagGroupsService.GetPositionForNewTagGroup()
                };
                newTagGroups[i].SetNameValue("ChangeTagGroupPosition_Name_" + i);

                _TagGroupsService.AddTagGroup(newTagGroups[i]);
                _efContext.SaveChanges();
                _testTagGroups.Add(newTagGroups[i]);
            }

            int lastpos = 0;
            IEnumerable<TagGroup> tgList = _TagGroupsService.GetTagGroups();
            foreach (TagGroup tg in tgList)
            {
                if (tg.Position > lastpos) lastpos = tg.Position;
            }
            #endregion

            #region Test1

            int expectedPosition = lastpos;
            
            _TagGroupsService.ChangeTagGroupPosition(newTagGroups[0].TagGroupId, 5);

            int actualPosition = newTagGroups[0].Position;

            Assert.AreEqual(expectedPosition, actualPosition);

            #endregion

            #region Test2

            expectedPosition = lastpos - 4;

            _TagGroupsService.ChangeTagGroupPosition(newTagGroups[1].TagGroupId, -1);

            actualPosition = newTagGroups[1].Position;

            Assert.AreEqual(expectedPosition, actualPosition);

            #endregion

            #region Test3

            expectedPosition = lastpos;

            _TagGroupsService.ChangeTagGroupPosition(newTagGroups[2].TagGroupId, 10);

            actualPosition = newTagGroups[2].Position;

            Assert.AreEqual(expectedPosition, actualPosition);

            #endregion

            #region Test4

            expectedPosition = newTagGroups[3].Position;

            _TagGroupsService.ChangeTagGroupPosition(newTagGroups[3].TagGroupId, 0);

            actualPosition = newTagGroups[3].Position;

            Assert.AreEqual(expectedPosition, actualPosition);

            #endregion
        }

        [TestMethod]
        public void GetPositionForNewTagGroupTest()
        {
            int lastpos = 0;
            IEnumerable<TagGroup> tgList = _TagGroupsService.GetTagGroups();
            foreach (TagGroup tg in tgList)
            {
                if (tg.Position > lastpos) lastpos = tg.Position;
            }
            int actual = _TagGroupsService.GetPositionForNewTagGroup();
            Assert.AreEqual(lastpos+1, actual);
        }
        #endregion
    }
}
