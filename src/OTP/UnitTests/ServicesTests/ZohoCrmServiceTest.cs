﻿using BusinessLogic.Objects.ZohoCrm;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace UnitTests.ServicesTests
{
    // This test can only be ran manually
    //[TestClass]
    public class ZohoCrmServiceTest
    {
        #region Fields

        private OTPEntities _context;
        private DictionaryCountry _testCountry;
        private DictionaryCulture _testCulture;

        #endregion

        #region Services

        private IZohoCrmService _zohoCrmService;
        private ICrmOperationLogsService _crmOperationLogsService;
        private ISettingsService _settingsSerivce;
        private IUserService _userService;
        private IMessageService _messageService;
        private IDictionaryCountryService _countryService;
        private IDictionaryCultureService _dictionaryCultureService;

        #endregion

        // This test can only be ran manually
        //[TestInitialize]
        public void PrepareData()
        {
            #region Setup Services

            _context = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_context);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            ISettingsService settingsService = new SettingsService(mock.Object);

            _settingsSerivce = new SettingsService(_context);
            _crmOperationLogsService = new CrmOperationLogsService(_context);
            _zohoCrmService = new ZohoCrmService(_context, _settingsSerivce, _crmOperationLogsService);
            _messageService = new MessageService(_context);
            _userService = new UsersService(_context, _zohoCrmService, _messageService, _settingsSerivce);
            _countryService = new DictionaryCountryService(mock.Object);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);

            #endregion

            _testCountry = _countryService.GetCountries().First();
            _testCulture = _dictionaryCultureService.GetCultures().First();
        }

        // This test can only be ran manually
        //[TestCleanup]
        public void Cleanup()
        {
        }

        // This test can only be ran manually
        //[TestMethod]
        public void AddLeadTest()
        {
            User user = new User()
            {
                Lastname = "AddLeadTest_Lastname",
                Firstname = "AddLeadTest_Firstname",
                Address1 = "AddLeadTest_Address1",
                Address2 = "AddLeadTest_Address2",
                State = "AddLeadTest_State",
                ZIPCode = "AddLeadTest_ZIPCode",
                City = "AddLeadTest_City",
                DictionaryCountry = _testCountry,
                CellPhone = "123456789",
                LandLine = "AddLeadTest_LandLine",
                email = "AddLeadTest@AddLeadTest.com",
                DriverLicenseNbr = "AddLeadTest_DriverLicenseNbr",
                PassportNbr = "AddLeadTest_PassportNbr",
                SocialsecurityNbr = "AddLeadTest_SocialsecurityNbr",
                DirectDepositInfo = "AddLeadTest_DirectDepositInfo",
                SecurityQuestion = "AddLeadTest_SecurityQuestion",
                SecurityAnswer = "AddLeadTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "AddLeadTest_AcceptedTCsInitials",
                Password = "AddLeadTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                DictionaryCulture = _testCulture
            };

            _userService.AddUser(user);

            ZohoCrmResponse response = _zohoCrmService.AddZohoCrmLead(user);

            Assert.IsNull(response.Error);

            ZohoCrmFieldLabel field = response.Result.RecordDetail.Fields.SingleOrDefault(f => f.ValAttribute == "Id");
           
            ZohoCrmConversionResponse convertResponse = null;

            if (field != null)
            {
                convertResponse = _zohoCrmService.ConvertZohoCrmLeadToAccount(field.Value, user);
                Assert.IsNotNull(convertResponse, string.Format("Failed on convert lead to account- Lead Id {0}", field.Value));
            }

            if(convertResponse != null)
            {
                var deleteAccunt = _zohoCrmService.DeleteZohoCrmAccountRecord(convertResponse.Account);
                var deleteContant = _zohoCrmService.DeleteZohoCrmContactRecord(convertResponse.Contact);
                var deleteLead = _zohoCrmService.DeleteZohoCrmLeadRecord(field.Value);
            }

            _userService.DeleteUserById(user.UserID);

        }
    }
}
