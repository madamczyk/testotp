﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using DataAccess.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class ScheduledTasksServiceTest
    {
        #region Fields
        /// <summary>
        /// list of amenity groups used in test methods
        /// </summary>
        private List<ScheduledTask> _testTasks = new List<ScheduledTask>();

        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;
        #endregion

        #region Services
        private IScheduledTasksService _scheduledTasksService;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            _scheduledTasksService = new ScheduledTasksService(mock.Object);
            #endregion
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (ScheduledTask st in _testTasks)
            {
                if (_scheduledTasksService.GetScheduledTaskById(st.ScheduledTaskId) != null)
                    _scheduledTasksService.DeleteScheduledTask(st.ScheduledTaskId);
            }
            _testTasks.Clear();
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void AddScheduledTaskTest()
        {
            ScheduledTask expected = new ScheduledTask()
            {
                ScheduledTaskType = (int)ScheduledTaskType.Email,
                ScheduleType = (int)ScheduleType.Once,
                ScheduleIntervalType = (int) ScheduleIntervalType.Days,
                TimeInterval = 5,
                ScheduleTimestamp = DateTime.Now,
                LastExecutionTime = DateTime.Now,
                NextPlannedExecutionTime = DateTime.Now.AddDays(10),
                TaskCompleted = true
            };
            _testTasks.Add(expected);
            _scheduledTasksService.AddScheduledTask(expected);
            _efContext.SaveChanges();

            var actual = _scheduledTasksService.GetScheduledTaskById(expected.ScheduledTaskId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetScheduledTaskByIdTest()
        {
            ScheduledTask expected = new ScheduledTask()
            {
                ScheduledTaskType = (int)ScheduledTaskType.Email,
                ScheduleType = (int)ScheduleType.Once,
                ScheduleIntervalType = (int)ScheduleIntervalType.Days,
                TimeInterval = 5,
                ScheduleTimestamp = DateTime.Now,
                LastExecutionTime = DateTime.Now,
                NextPlannedExecutionTime = DateTime.Now.AddDays(10),
                TaskCompleted = true
            };
            _testTasks.Add(expected);
            _scheduledTasksService.AddScheduledTask(expected);
            _efContext.SaveChanges();
            
            var actual = _scheduledTasksService.GetScheduledTaskById(expected.ScheduledTaskId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetScheduledTasksTest()
        {
            Dictionary<int, bool> newTasksFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; i++)
            {
                ScheduledTask temp = new ScheduledTask()
                {
                    ScheduledTaskType = (int)ScheduledTaskType.Email,
                    ScheduleType = (int)ScheduleType.Once,
                    ScheduleIntervalType = (int)ScheduleIntervalType.Days,
                    TimeInterval = 5 + i,
                    ScheduleTimestamp = DateTime.Now,
                    LastExecutionTime = DateTime.Now,
                    NextPlannedExecutionTime = DateTime.Now.AddDays(10),
                    TaskCompleted = true
                };
                _testTasks.Add(temp);
                _scheduledTasksService.AddScheduledTask(temp);
                _efContext.SaveChanges();

                newTasksFound.Add(_testTasks[i].ScheduledTaskId, false);
            }

            ScheduledTask expected;
            var actual = _scheduledTasksService.GetScheduledTasks();

            foreach (var actualObj in actual)
            {
                if (newTasksFound.ContainsKey(actualObj.ScheduledTaskId))
                {
                    expected = _testTasks.Where(a => a.ScheduledTaskId == actualObj.ScheduledTaskId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newTasksFound[actualObj.ScheduledTaskId] = true;
                }
            }

            if (newTasksFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "scheduled tasks", "GetScheduledTasks"));
        }

        #endregion
    }
}
