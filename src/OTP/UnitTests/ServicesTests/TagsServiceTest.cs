﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class TagsServiceTest
    {
        #region Fields
        /// <summary>
        /// tag group for test methods
        /// </summary>
        private TagGroup _testTagsGroup = null;
        /// <summary>
        /// property 
        /// </summary>
        private Property _testProperty = null;
        /// <summary>
        /// list of tags used in test methods
        /// </summary>
        private List<Tag> _testTags = new List<Tag>();
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext = null;
        #endregion

        #region Services
        private ITagsService _tagsService = null;

        private ITagGroupsService _tagsGroupService = null;

        private IPropertiesService _propertiesService = null;

        private IDestinationsService _destinationService = null;
        
        private IPropertyTypesService _propTypeService = null;

        private IDictionaryCountryService _dictCountryService = null;

        private IUserService _userService = null;

        private IBlobService _blobService = null;

        private IDictionaryCultureService _dictionaryCultureService = null;

        private IZohoCrmService _zohoCrmService = null;

        private IMessageService _messageService = null;

        private ICrmOperationLogsService _crmOperationLogsService = null;

        private ISettingsService _settingsService = null;

        #endregion

        #region Prepare/Cleanup Data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _messageService = new MessageService(mock.Object, _settingsService);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _blobService = new BlobService(mock.Object);
            _tagsService = new TagsService(mock.Object);
            _propertiesService = new PropertiesServices(configurationService, _blobService, _settingsService, mock.Object);
            _tagsGroupService = new TagGroupsService(mock.Object);
            _destinationService = new DestinationsService(configurationService, mock.Object);
            _propTypeService = new PropertyTypeService(mock.Object);
            _dictCountryService = new DictionaryCountryService(mock.Object);
            _userService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);
            #endregion

            _testTagsGroup = new TagGroup();
            _testTagsGroup.Position = 1;
            _testTagsGroup.SetNameValue("TestGroup", DataAccess.Enums.CultureCode.de_DE);
            _tagsGroupService.AddTagGroup(_testTagsGroup);
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            if (_testProperty != null)
            {
                if (_propertiesService.GetPropertyById(_testProperty.PropertyID) != null)
                {
                    int uid = _testProperty.User.UserID;
                    _propertiesService.RemoveProperty(_testProperty.PropertyID);
                    _userService.DeleteUserById(uid);
                }
            }
            foreach (Tag t in _testTags)
            {
                if (_tagsService.GetTagById(t.TagID) != null)
                    _tagsService.DeleteTag(t.TagID);
            }
            _testTags.Clear();

            if (_tagsGroupService.GetTagGroupById(_testTagsGroup.TagGroupId) != null)
                _tagsGroupService.DeleteTagGroup(_testTagsGroup.TagGroupId);
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void AddTagTest()
        {
            Tag expected = new Tag()
            {
                TagGroup = _testTagsGroup,
                
            };
            expected.SetNameValue("TagName");

            _testTags.Add(expected);
            _tagsService.AddTag(expected);
            _efContext.SaveChanges();

            Tag actual = _tagsService.GetTagById(expected.TagID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddTagRequiredFieldsTest()
        {
            #region Test field TagGroup
            Tag tag1 = new Tag()
            {
                TagGroup = null,  
                NameCurrentLanguage = "en",
                
            };
            tag1.SetNameValue("TagName");
            
            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _tagsService.AddTag(tag1);
                _efContext.SaveChanges();
                _testTags.Add(tag1);
            }, "FK_Tags_TagGroup");
            #endregion

            #region Test field NameCurrentLanguage
            Tag tag3 = new Tag()
            {
                TagGroup = null,
                NameCurrentLanguage = null,

            };
            tag3.SetNameValue("TagName");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _tagsService.AddTag(tag3);
                _efContext.SaveChanges();
                _testTags.Add(tag3);
            }, "FK_Tags_TagGroup");
            #endregion

            #region Test field Name
            Tag tag4 = new Tag()
            {
                TagGroup = null,
                NameCurrentLanguage = "en",
            };
            tag4.SetNameValue(null);

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _tagsService.AddTag(tag3);
                _efContext.SaveChanges();
                _testTags.Add(tag3);
            }, "FK_Tags_TagGroup");
            #endregion
        }

        [TestMethod]
        public void DeleteTagTest()
        {
            #region Delete successfully
            Tag tag = new Tag()
            {
                TagGroup = _testTagsGroup,
            };
            tag.SetNameValue("DeletedTagName");

            _tagsService.AddTag(tag);
            _efContext.SaveChanges();
            // delete previously added tag
            _tagsService.DeleteTag(tag.TagID);
            // it should return null(?) if there is no tag in db with given id
            Tag tg = _tagsService.GetTagById(tag.TagID);

            Assert.IsNull(tg, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced tag deletion
            DictionaryCountry referencedCountry = _dictCountryService.GetCountries().First();
            Destination referencedDestination = _destinationService.GetDestinations().First();
            PropertyType referencedPropertyType = _propTypeService.GetPropertyTypes().First();

            User referencedUser = new User()
            {
                Firstname = "FirstName",
                Lastname = "LastName",
                email = "email",
                Password = "Password",
                IsGeneratedPassword = false,
                DictionaryCulture = _dictionaryCultureService.GetCultures().First()
            };

            Tag propTag = new Tag()
            {
                TagGroup = _testTagsGroup,
            };
            propTag.SetNameValue("TagName");

            Property prop = new Property()
            {
                Destination = referencedDestination,
                PropertyTags = new List<PropertyTag>(),
                DictionaryCountry = referencedCountry,
                PropertyType = referencedPropertyType,
                User = referencedUser,
                Address1 = "address",
                City = "city",
                ZIPCode = "zcode",
                PropertyCode = "pcode",
                State = " state",
                DictionaryCulture = _dictionaryCultureService.GetCultures().First(),
                TimeZone = "TZ"
            };

            PropertyTag pTag = new PropertyTag()
            {
                Tag = propTag,
                Property = prop,
            };

            prop.PropertyTags.Add(pTag);
            
            prop.SetLongDescriptionValue("TestLongDescription", DataAccess.Enums.CultureCode.de_DE);
            prop.SetShortDescriptionValue("TestShortDEscription");
            prop.SetPropertyNameValue("PropertyName");
            
            _propertiesService.AddProperty(prop);        
            _testProperty = prop;
            _efContext.SaveChanges();

            ExtendedAssert.Throws<Exception>(() =>
            {
                _tagsService.DeleteTag(propTag.TagID);
            });
            #endregion
        }

        [TestMethod]
        public void GetTagsTest()
        {
            Dictionary<int, bool> newTagsFound = new Dictionary<int, bool>();

            for (int i  = 0; i < 5; i++)
            {
                Tag temp = new Tag()
                {
                    TagGroup = _testTagsGroup,
                };
                _testTags.Add(temp);
                temp.SetNameValue("DeletedTagName");
                _tagsService.AddTag(temp);
                _efContext.SaveChanges();
                
                newTagsFound.Add(_testTags[i].TagID, false);
            }

            Tag expected;
            var actual = _tagsService.GetTags();

            foreach (var actualObj in actual)
            {
                if (newTagsFound.ContainsKey(actualObj.TagID))
                {
                    expected = _testTags.Where(a => a.TagID == actualObj.TagID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newTagsFound[actualObj.TagID] = true;
                }
            }

            if (newTagsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "tags", "GetTags"));
        }
        #endregion
    }
}
