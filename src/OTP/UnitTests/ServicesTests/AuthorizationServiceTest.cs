﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class AuthorizationServiceTest
    {
        #region Fields
        /// <summary>
        /// test objects
        /// </summary>
        private User _testUser = null;
        private DictionaryCulture _testCulture;
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext = null;
        #endregion

        #region Services

        private IZohoCrmService _zohoCrmService = null;
        private IMessageService _messageService = null;
        private ICrmOperationLogsService _crmOperationLogsService = null;
        private ISettingsService _settingsService = null;
        private IAuthorizationService _authorizationService = null;
        private IUserService _userService = null;
        private IDictionaryCultureService _dictionaryCultureService = null;
        #endregion

        #region Prepare/Cleanup Data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _messageService = new MessageService(mock.Object, _settingsService);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _userService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
            _authorizationService = new AuthorizationService(_userService, mock.Object);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);

            #endregion

            _testCulture = _dictionaryCultureService.GetCultures().First();

            _testUser = new User()
            {
                Firstname = "AuthorizationServiceTest_Firstname",
                Lastname = "AuthorizationServiceTest_Lastname",
                email = "AuthorizationServiceTest_E-mail",
                Password = Common.MD5Helper.Encode("p@ssw0rd"),
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(_testUser);
            _efContext.SaveChanges();

        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            _userService.DeleteUserById(_testUser.UserID);
            _efContext.SaveChanges();
        }
        #endregion

        #region Test Methods
        [TestMethod]
        public void ValidateUserLoginTest()
        {

        #region Login successfull
            bool test = _authorizationService.ValidateUserLogin(_testUser.email, "p@ssw0rd");
            Assert.IsTrue(test);
        #endregion

        #region Login unsuccessfull
            test = _authorizationService.ValidateUserLogin(_testUser.email, "password");
            Assert.IsFalse(test);
        #endregion

        }

        [TestMethod]
        public void GeneratePasswordTest()
        {
            string result = _authorizationService.GeneratePassword();

            Assert.AreEqual(8, result.Length);
        }
        #endregion
    }
}
