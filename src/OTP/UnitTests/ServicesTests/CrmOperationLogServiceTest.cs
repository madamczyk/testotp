﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class CrmOperationLogServiceTest
    {
        #region Fields

        private OTPEntities _efContext;
        private DictionaryCountry _testCountry;
        private DictionaryCulture _testCulture;
        private List<CrmOperationLog> _logs;
        private List<User> _users;

        #endregion

        #region Services

        private IZohoCrmService _zohoCrmService;
        private ICrmOperationLogsService _crmOperationLogsService;
        private ISettingsService _settingsSerivce;
        private IUserService _userService;
        private IMessageService _messageService;
        private IDictionaryCountryService _countryService;
        private IDictionaryCultureService _dictionaryCultureService;

        #endregion

        #region Prepare Data

        [TestInitialize()]
        public void PrepareData()
        {
            #region Setup Services

            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();

            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            _settingsSerivce = new SettingsService(_efContext);
            _crmOperationLogsService = new CrmOperationLogsService(_efContext);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsSerivce, _crmOperationLogsService);
            _messageService = new MessageService(_efContext);
            _userService = new UsersService(_efContext, _zohoCrmService, _messageService, _settingsSerivce);
            _countryService = new DictionaryCountryService(mock.Object);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);

            #endregion

            _logs = new List<CrmOperationLog>();
            _users = new List<User>();
            _testCountry = _countryService.GetCountries().First();
            _testCulture = _dictionaryCultureService.GetCultures().First();
        }

        #endregion

        #region Test Cleanup

        [TestCleanup()]
        public void Cleanup()
        {
            foreach (CrmOperationLog log in _logs)
            {
                _crmOperationLogsService.DeleteCrmOperationsLogById(log.Id);
            }

            _logs.Clear();

            foreach (var user in _users)
            {
                _userService.DeleteUserById(user.UserID);
            }
        }

        #endregion

        #region Test Methods

        [TestMethod()]
        public void AddCrmOperationLogTest()
        {
            User user = new User()
            {
                Lastname = "AddCrmOperationLogTest_Lastname",
                Firstname = "AddCrmOperationLogTest_Firstname",
                Address1 = "AddCrmOperationLogTest_Address1",
                Address2 = "AddCrmOperationLogTest_Address2",
                State = "AddCrmOperationLogTest_State",
                ZIPCode = "AddCrmOperationLogTest_ZIPCode",
                City = "AddCrmOperationLogTest_City",
                DictionaryCountry = _testCountry,
                CellPhone = "123456789",
                LandLine = "AddCrmOperationLogTest_LandLine",
                email = "AddCrmOperationLogTest@AddCrmOperationLogTest.com",
                DriverLicenseNbr = "AddCrmOperationLogTest_DriverLicenseNbr",
                PassportNbr = "AddCrmOperationLogTest_PassportNbr",
                SocialsecurityNbr = "AddCrmOperationLogTest_SocialsecurityNbr",
                DirectDepositInfo = "AddCrmOperationLogTest_DirectDepositInfo",
                SecurityQuestion = "AddCrmOperationLogTest_SecurityQuestion",
                SecurityAnswer = "AddCrmOperationLogTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "AddCrmOperationLogTest_AcceptedTCsInitials",
                Password = "AddCrmOperationLogTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                DictionaryCulture = _testCulture
            };

            _userService.AddUser(user);
            _users.Add(user);

            CrmOperationLog log = new CrmOperationLog
            {
                CrmRequest = "<xml>AddCrmOperationLogTest_CrmRequest</xml>",
                CrmResponse = "<xml>AddCrmOperationLogTest_CrmResponse</xml>",
                Message = "AddCrmOperationLogTest_Message",
                OperationType = 1,
                User = user,
                RowDate = DateTime.Now
            };

            _crmOperationLogsService.AddOperationLog(log);
            _efContext.SaveChanges();
            _logs.Add(log);

            var testLog = _crmOperationLogsService.GetCrmOperationLogsById(log.Id);

            Assert.IsNotNull(testLog);
            ExtendedAssert.AreEqual(log, testLog);
        }

        [TestMethod()]
        public void GetCrmOperationLogByIdTest()
        {
            User user = new User()
            {
                Lastname = "GetCrmOperationLogByIdTest_Lastname",
                Firstname = "GetCrmOperationLogByIdTest_Firstname",
                Address1 = "GetCrmOperationLogByIdTest_Address1",
                Address2 = "GetCrmOperationLogByIdTest_Address2",
                State = "GetCrmOperationLogByIdTest_State",
                ZIPCode = "GetCrmOperationLogByIdTest_ZIPCode",
                City = "GetCrmOperationLogByIdTest_City",
                DictionaryCountry = _testCountry,
                CellPhone = "123456789",
                LandLine = "GetCrmOperationLogByIdTest_LandLine",
                email = "GetCrmOperationLogByIdTest@GetCrmOperationLogByIdTest.com",
                DriverLicenseNbr = "GetCrmOperationLogByIdTest_DriverLicenseNbr",
                PassportNbr = "GetCrmOperationLogByIdTest_PassportNbr",
                SocialsecurityNbr = "GetCrmOperationLogByIdTest_SocialsecurityNbr",
                DirectDepositInfo = "GetCrmOperationLogByIdTest_DirectDepositInfo",
                SecurityQuestion = "GetCrmOperationLogByIdTest_SecurityQuestion",
                SecurityAnswer = "GetCrmOperationLogByIdTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "GetCrmOperationLogByIdTest_AcceptedTCsInitials",
                Password = "GetCrmOperationLogByIdTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                DictionaryCulture = _testCulture
            };

            _userService.AddUser(user);
            _users.Add(user);

            CrmOperationLog log = new CrmOperationLog
            {
                CrmRequest = "<xml>AddCrmOperationLogTest_CrmRequest</xml>",
                CrmResponse = "<xml>AddCrmOperationLogTest_CrmResponse</xml>",
                Message = "AddCrmOperationLogTest_Message",
                User = user,
                OperationType = 1,
                RowDate = DateTime.Now
            };

            _crmOperationLogsService.AddOperationLog(log);
            _efContext.SaveChanges();
            _logs.Add(log);

            var actual = _crmOperationLogsService.GetCrmOperationLogsById(log.Id);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(log, actual);
        }

        [TestMethod()]
        public void GetCrmOperationLogsTest()
        {
            User user = new User()
            {
                Lastname = "GetCrmOperationLogTest_Lastname",
                Firstname = "GetCrmOperationLogTest_Firstname",
                Address1 = "GetCrmOperationLogTest_Address1",
                Address2 = "GetCrmOperationLogTest_Address2",
                State = "GetCrmOperationLogTest_State",
                ZIPCode = "GetCrmOperationLogTest_ZIPCode",
                City = "GetCrmOperationLogTest_City",
                DictionaryCountry = _testCountry,
                CellPhone = "123456789",
                LandLine = "GetCrmOperationLogTest_LandLine",
                email = "GetCrmOperationLogTest@GetCrmOperationLogTest.com",
                DriverLicenseNbr = "GetCrmOperationLogTest_DriverLicenseNbr",
                PassportNbr = "GetCrmOperationLogTest_PassportNbr",
                SocialsecurityNbr = "GetCrmOperationLogTest_SocialsecurityNbr",
                DirectDepositInfo = "GetCrmOperationLogTest_DirectDepositInfo",
                SecurityQuestion = "GetCrmOperationLogTest_SecurityQuestion",
                SecurityAnswer = "GetCrmOperationLogTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "GetCrmOperationLogTest_AcceptedTCsInitials",
                Password = "GetCrmOperationLogTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                DictionaryCulture = _testCulture
            };

            _userService.AddUser(user);
            _users.Add(user);

            Dictionary<int, bool> newCrmOperationsLogsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; i++)
            {
                CrmOperationLog log = new CrmOperationLog
                {
                    CrmRequest = "<xml>GetCrmOperationLogsTest_CrmRequest" + i.ToString() + "</xml>",
                    CrmResponse = "<xml>GetCrmOperationLogsTest_CrmResponse" + i.ToString() + "</xml>",
                    Message = "GetCrmOperationLogsTest_Message" + i.ToString(),
                    OperationType = 1,
                    User = user,
                    RowDate = DateTime.Now
                };

                _crmOperationLogsService.AddOperationLog(log);
                _efContext.SaveChanges();
                newCrmOperationsLogsFound.Add(log.Id, false);
                _logs.Add(log);
            }

            var dbLogs = _crmOperationLogsService.GetCrmOperationsLog();

            foreach (var dbLog in dbLogs)
            {
                if (newCrmOperationsLogsFound.ContainsKey(dbLog.Id))
                {
                    var expected = _logs.Where(l => l.Id == dbLog.Id).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, dbLog);

                    newCrmOperationsLogsFound[dbLog.Id] = true;
                }
            }

            if (newCrmOperationsLogsFound.Any(na => na.Value == false))
            {
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "crm operation logs", "GetCrmOperationLogsTest"));
            }
        }

        [TestMethod]
        public void DeleteCrmOperationsLogByIdTest()
        {
            User user = new User()
            {
                Lastname = "DeleteCrmOperationLogTest_Lastname",
                Firstname = "DeleteCrmOperationLogTest_Firstname",
                Address1 = "DeleteCrmOperationLogTest_Address1",
                Address2 = "DeleteCrmOperationLogTest_Address2",
                State = "DeleteCrmOperationLogTest_State",
                ZIPCode = "DeleteCrmOperationLogTest_ZIPCode",
                City = "DeleteCrmOperationLogTest_City",
                DictionaryCountry = _testCountry,
                CellPhone = "123456789",
                LandLine = "DeleteCrmOperationLogTest_LandLine",
                email = "DeleteCrmOperationLogTest@DeleteCrmOperationLogTest.com",
                DriverLicenseNbr = "DeleteCrmOperationLogTest_DriverLicenseNbr",
                PassportNbr = "DeleteCrmOperationLogTest_PassportNbr",
                SocialsecurityNbr = "DeleteCrmOperationLogTest_SocialsecurityNbr",
                DirectDepositInfo = "DeleteCrmOperationLogTest_DirectDepositInfo",
                SecurityQuestion = "DeleteCrmOperationLogTest_SecurityQuestion",
                SecurityAnswer = "DeleteCrmOperationLogTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "DeleteCrmOperationLogTest_AcceptedTCsInitials",
                Password = "DeleteCrmOperationLogTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                DictionaryCulture = _testCulture
            };

            _userService.AddUser(user);
            _users.Add(user);

            #region Delete successfully
            CrmOperationLog log = new CrmOperationLog
            {
                CrmRequest = "<xml>DeleteCrmOperationLogTest_CrmRequest</xml>",
                CrmResponse = "<xml>DeleteCrmOperationLogTest_CrmResponse</xml>",
                Message = "DeleteCrmOperationLogTest_Message",
                User = user,
                OperationType = 1,
                RowDate = DateTime.Now
            };

            _crmOperationLogsService.AddOperationLog(log);
            _efContext.SaveChanges();

            _crmOperationLogsService.DeleteCrmOperationsLogById(log.Id);

            var dbLog = _crmOperationLogsService.GetCrmOperationLogsById(log.Id);

            Assert.IsNull(dbLog, Messages.ObjectIsNotNull);

            #endregion
        }

        #endregion

    }
}
