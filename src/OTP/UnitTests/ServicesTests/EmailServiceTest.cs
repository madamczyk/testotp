﻿using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using DataAccess.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class EmailServiceTest
    {
        #region Fields
        /// <summary>
        /// recipients list of test emails
        /// </summary>
        private List<string> _recipients = new List<string>();
        /// <summary>
        /// cc list of test emails
        /// </summary>
        private List<string> _CCList = new List<string>();
        /// <summary>
        /// reply to list of st emails
        /// </summary>
        private List<string> _replyToList = new List<string>();
        /// <summary>
        /// recipients list in one string
        /// </summary>
        private StringBuilder _recipientsStr = new StringBuilder();
        /// <summary>
        /// cc list in one string
        /// </summary>
        private StringBuilder _CCListStr = new StringBuilder();
        /// <summary>
        /// reply to list in one string
        /// </summary>
        private StringBuilder _replyToListStr = new StringBuilder();

        private Message _testMsg = null;
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;
        #endregion

        #region Services
        private Mock<IStorageQueueService> _storageQueueService = null;
        private IMessageService _messageService = null;
        private IDocumentService _docsService = null;
        private ISettingsService _settingsService = null;
        private IDictionaryCultureService _dictCulture = null;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            _settingsService = new SettingsService(mock.Object);

            _storageQueueService = new Mock<IStorageQueueService>();
            _messageService = new MessageService(mock.Object,_settingsService);
            _docsService = new PdfDocumentService(mock.Object, _settingsService);
            _dictCulture = new DictionaryCultureService(mock.Object);
            #endregion

            #region Setup data
            _recipients = new List<string>();
            _recipients.Add("test1@test.com");
            _recipients.Add("test2@test.com");
            _recipients.Add("test3@test.com");

            _CCList = new List<string>();
            _CCList.Add("test4@test.com");
            _CCList.Add("test5@test.com");
            _CCList.Add("test6@test.com");

            _replyToList = new List<string>();
            _replyToList.Add("test7@test.com");
            _replyToList.Add("test8@test.com");
            _replyToList.Add("test9@test.com");

            _recipientsStr = new StringBuilder();
            _CCListStr = new StringBuilder();
            _replyToListStr = new StringBuilder();

            foreach (string addr in _recipients)
            {
                _recipientsStr.Append(addr);
                _recipientsStr.Append(",");
            }
            _recipientsStr.Remove(_recipientsStr.Length - 1, 1);

            foreach (string addr in _CCList)
            {
                _CCListStr.Append(addr);
                _CCListStr.Append(",");
            }
            _CCListStr.Remove(_CCListStr.Length - 1, 1);

            foreach (string addr in _replyToList)
            {
                _replyToListStr.Append(addr);
                _replyToListStr.Append(",");
            }
            _replyToListStr.Remove(_replyToListStr.Length - 1, 1);
            #endregion
        }

        /// <summary>
        /// Cleanup the data before each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            DataAccessHelper.RemoveMessage(_efContext, _testMsg);
        }
        #endregion

        #region Private methods
        // copied from EmailService
        private string CompleteTemplate(string template, IDictionary<string, string> values)
        {
            const string EmailParameterTagStart = "{{";
            const string EmailParameterTagEnd = "}}";

            foreach (KeyValuePair<string, string> kvp in values)
                template = Regex.Replace(template, EmailParameterTagStart +
                                        @"\s*" +
                                        kvp.Key +
                                        @"\s*" +
                                        EmailParameterTagEnd,
                                        kvp.Value);
            return template;
        }

        #endregion

        #region Test methods
        [TestMethod]
        public void SendOwnerActivationEmailTest()
        {
            StringBuilder msgBody = new StringBuilder();
           
            OwnerActivationEmail testEmail = new OwnerActivationEmail(_recipients)
            {
                ActivationLinkTitle = "Activation Link",
                ActivationLinkUrl = "www.activationlinks.com",
                CcAddresses = _CCList,
                RecipientAddresses = _recipients,
                ReplyToAddresses = _replyToList,
                UserFirstName = "UserName",
                UserLogin = "UserLogin",
                UserPassword = "P@55vv0rd",   
            };

            Template template = _messageService.GetTemplateByTemplateType(TemplateType.OwnerActivationEmail, CultureCode.en_US);

            msgBody.Append(template.Content);
            string body = CompleteTemplate( msgBody.ToString(), testEmail.Parameters);
            string subject = CompleteTemplate(template.Title, testEmail.Parameters);

            _messageService.AddEmail(testEmail, CultureCode.en_US);

            _testMsg = _messageService.GetMessagesToSend().ElementAt(_messageService.GetMessagesToSend().Count() - 1);

            Assert.IsNotNull(_testMsg, Messages.ObjectIsNull);
            Assert.AreEqual(_CCListStr.ToString(), _testMsg.CC);
            Assert.AreEqual(_recipientsStr.ToString(), _testMsg.Recipients);
            Assert.AreEqual(_replyToListStr.ToString(), _testMsg.ReplyTo);
            Assert.AreEqual((int) MessageStatus.New, _testMsg.Status);
            Assert.AreEqual(0, _testMsg.SendAttempts);
            Assert.AreEqual(subject, _testMsg.Subject);
            Assert.AreEqual(body, _testMsg.Body);
        }

        [TestMethod]
        public void SendOwnerJoiningEmailTest()
        {
            StringBuilder msgBody = new StringBuilder();
            string date = DateTime.Today.ToShortDateString();

            OwnerJoiningEmail testEmail = new OwnerJoiningEmail(_recipients)
            {
                CcAddresses = _CCList,
                RecipientAddresses = _recipients,
                ReplyToAddresses = _replyToList,
                UserFirstName = "UserName",
                AccountManagerName = "AccManager",
                PhoneNumber = "+1234567890",
                PreferredTimeFrom = date,
                PreferredTimeTo = date,
                PreferredDates = date + "|" + date,
            };

            Template template = _messageService.GetTemplateByTemplateType(TemplateType.OwnerJoinLetter, CultureCode.en_US);

            msgBody.Append(template.Content);
            string body = CompleteTemplate(msgBody.ToString(), testEmail.Parameters);
            string subject = CompleteTemplate(template.Title, testEmail.Parameters);

            _messageService.AddEmail(testEmail, CultureCode.en_US);

            _testMsg = _messageService.GetMessagesToSend().ElementAt(_messageService.GetMessagesToSend().Count() - 1);

            Assert.IsNotNull(_testMsg, Messages.ObjectIsNull);
            Assert.AreEqual(_CCListStr.ToString(), _testMsg.CC);
            Assert.AreEqual(_recipientsStr.ToString(), _testMsg.Recipients);
            Assert.AreEqual(_replyToListStr.ToString(), _testMsg.ReplyTo);
            Assert.AreEqual((int)MessageStatus.New, _testMsg.Status);
            Assert.AreEqual(0, _testMsg.SendAttempts);
            Assert.AreEqual(subject, _testMsg.Subject);
            Assert.AreEqual(body, _testMsg.Body);
        }

        [TestMethod]
        public void SendPasswordResetEmailTest()
        {
            StringBuilder msgBody = new StringBuilder();

            PasswordResetEmail testEmail = new PasswordResetEmail(_recipients)
            {
                CcAddresses = _CCList,
                RecipientAddresses = _recipients,
                ReplyToAddresses = _replyToList,
                UserFirstName = "UserName",
                TempPassword = "tempp@ss",
            };

            Template template = _messageService.GetTemplateByTemplateType(TemplateType.PasswordResetEmail, CultureCode.en_US);

            msgBody.Append(template.Content);
            string body = CompleteTemplate(msgBody.ToString(), testEmail.Parameters);
            string subject = CompleteTemplate(template.Title, testEmail.Parameters);

            _messageService.AddEmail(testEmail, CultureCode.en_US);

            _testMsg = _messageService.GetMessagesToSend().ElementAt(_messageService.GetMessagesToSend().Count() - 1);

            Assert.IsNotNull(_testMsg, Messages.ObjectIsNull);
            Assert.AreEqual(_CCListStr.ToString(), _testMsg.CC);
            Assert.AreEqual(_recipientsStr.ToString(), _testMsg.Recipients);
            Assert.AreEqual(_replyToListStr.ToString(), _testMsg.ReplyTo);
            Assert.AreEqual((int)MessageStatus.New, _testMsg.Status);
            Assert.AreEqual(0, _testMsg.SendAttempts);
            Assert.AreEqual(subject, _testMsg.Subject);
            Assert.AreEqual(body, _testMsg.Body);
        }

        [TestMethod]
        public void SendPasswordResetBookingEmailTest()
        {
            StringBuilder msgBody = new StringBuilder();

            PasswordResetBookingEmail testEmail = new PasswordResetBookingEmail(_recipients)
            {
                CcAddresses = _CCList,
                RecipientAddresses = _recipients,
                ReplyToAddresses = _replyToList,
                UserFirstName = "UserName",
                TempPassword = "tempp@ss",
                LinkUrl = "linkURL"
            };

            Template template = _messageService.GetTemplateByTemplateType(TemplateType.PasswordResetBookingEmail, CultureCode.en_US);

            msgBody.Append(template.Content);
            string body = CompleteTemplate(msgBody.ToString(), testEmail.Parameters);
            string subject = CompleteTemplate(template.Title, testEmail.Parameters);

            _messageService.AddEmail(testEmail, CultureCode.en_US);

            _testMsg = _messageService.GetMessagesToSend().ElementAt(_messageService.GetMessagesToSend().Count() - 1);

            Assert.IsNotNull(_testMsg, Messages.ObjectIsNull);
            Assert.AreEqual(_CCListStr.ToString(), _testMsg.CC);
            Assert.AreEqual(_recipientsStr.ToString(), _testMsg.Recipients);
            Assert.AreEqual(_replyToListStr.ToString(), _testMsg.ReplyTo);
            Assert.AreEqual((int)MessageStatus.New, _testMsg.Status);
            Assert.AreEqual(0, _testMsg.SendAttempts);
            Assert.AreEqual(subject, _testMsg.Subject);
            Assert.AreEqual(body, _testMsg.Body);
        }

        [TestMethod]
        public void SendUserActivationEmailTest()
        {
            StringBuilder msgBody = new StringBuilder();

            UserActivationEmail testEmail = new UserActivationEmail(_recipients)
            {
                CcAddresses = _CCList,
                RecipientAddresses = _recipients,
                ReplyToAddresses = _replyToList,
                UserFirstName = "UserName",
                ActivationLinkTitle = "Activation Link",
                ActivationLinkUrl = "www.activationlinks.com",
            };

            Template template = _messageService.GetTemplateByTemplateType(TemplateType.UserSignupConfirmation, CultureCode.en_US);

            msgBody.Append(template.Content);
            string body = CompleteTemplate(msgBody.ToString(), testEmail.Parameters);
            string subject = CompleteTemplate(template.Title, testEmail.Parameters);

            _messageService.AddEmail(testEmail, CultureCode.en_US);

            _testMsg = _messageService.GetMessagesToSend().ElementAt(_messageService.GetMessagesToSend().Count() - 1);

            Assert.IsNotNull(_testMsg, Messages.ObjectIsNull);
            Assert.AreEqual(_CCListStr.ToString(), _testMsg.CC);
            Assert.AreEqual(_recipientsStr.ToString(), _testMsg.Recipients);
            Assert.AreEqual(_replyToListStr.ToString(), _testMsg.ReplyTo);
            Assert.AreEqual((int)MessageStatus.New, _testMsg.Status);
            Assert.AreEqual(0, _testMsg.SendAttempts);
            Assert.AreEqual(subject, _testMsg.Subject);
            Assert.AreEqual(body, _testMsg.Body);
        }

        [TestMethod]
        public void SendEVSServiceErrorEmailTest()
        {
            StringBuilder msgBody = new StringBuilder();

            EVSServiceErrorEmail testEmail = new EVSServiceErrorEmail(_recipients)
            {
                CcAddresses = _CCList,
                RecipientAddresses = _recipients,
                ReplyToAddresses = _replyToList,
                UserId = "12312",
                UserName = "UserName",
                Message = "MEssage", 
            };

            Template template = _messageService.GetTemplateByTemplateType(TemplateType.EVSServiceError, CultureCode.en_US);

            msgBody.Append(template.Content);
            string body = CompleteTemplate(msgBody.ToString(), testEmail.Parameters);
            string subject = CompleteTemplate(template.Title, testEmail.Parameters);

            _messageService.AddEmail(testEmail, CultureCode.en_US);

            _testMsg = _messageService.GetMessagesToSend().ElementAt(_messageService.GetMessagesToSend().Count() - 1);

            Assert.IsNotNull(_testMsg, Messages.ObjectIsNull);
            Assert.AreEqual(_CCListStr.ToString(), _testMsg.CC);
            Assert.AreEqual(_recipientsStr.ToString(), _testMsg.Recipients);
            Assert.AreEqual(_replyToListStr.ToString(), _testMsg.ReplyTo);
            Assert.AreEqual((int)MessageStatus.New, _testMsg.Status);
            Assert.AreEqual(0, _testMsg.SendAttempts);
            Assert.AreEqual(subject, _testMsg.Subject);
            Assert.AreEqual(body, _testMsg.Body);
        }

        [TestMethod]
        public void SendEVSVerificationFaildEmailTest()
        {
            StringBuilder msgBody = new StringBuilder();

            EVSVerificationFaildEmail testEmail = new EVSVerificationFaildEmail(_recipients)
            {
                CcAddresses = _CCList,
                RecipientAddresses = _recipients,
                ReplyToAddresses = _replyToList,
                UserId = "12312",
                UserName = "UserName",
            };

            Template template = _messageService.GetTemplateByTemplateType(TemplateType.EVSVerificationFailed, CultureCode.en_US);

            msgBody.Append(template.Content);
            string body = CompleteTemplate(msgBody.ToString(), testEmail.Parameters);
            string subject = CompleteTemplate(template.Title, testEmail.Parameters);

            _messageService.AddEmail(testEmail, CultureCode.en_US);

            _testMsg = _messageService.GetMessagesToSend().ElementAt(_messageService.GetMessagesToSend().Count() - 1);

            Assert.IsNotNull(_testMsg, Messages.ObjectIsNull);
            Assert.AreEqual(_CCListStr.ToString(), _testMsg.CC);
            Assert.AreEqual(_recipientsStr.ToString(), _testMsg.Recipients);
            Assert.AreEqual(_replyToListStr.ToString(), _testMsg.ReplyTo);
            Assert.AreEqual((int)MessageStatus.New, _testMsg.Status);
            Assert.AreEqual(0, _testMsg.SendAttempts);
            Assert.AreEqual(subject, _testMsg.Subject);
            Assert.AreEqual(body, _testMsg.Body);
        }

        [TestMethod]
        public void SendReservationConfirmationForOwnerTest()
        {
            StringBuilder msgBody = new StringBuilder();

            ReservationConfirmationForOwner testEmail = new ReservationConfirmationForOwner(_recipients)
            {
                CcAddresses = _CCList,
                RecipientAddresses = _recipients,
                ReplyToAddresses = _replyToList,
                GuestFullName = "GueastFullName",
                OwnerFirstName = "OwnerFirstName"
            };

            Template template = _messageService.GetTemplateByTemplateType(TemplateType.ReservationConfirmationForOwner, CultureCode.en_US);

            msgBody.Append(template.Content);
            string body = CompleteTemplate(msgBody.ToString(), testEmail.Parameters);
            string subject = CompleteTemplate(template.Title, testEmail.Parameters);

            _messageService.AddEmail(testEmail, CultureCode.en_US);

            _testMsg = _messageService.GetMessagesToSend().ElementAt(_messageService.GetMessagesToSend().Count() - 1);

            Assert.IsNotNull(_testMsg, Messages.ObjectIsNull);
            Assert.AreEqual(_CCListStr.ToString(), _testMsg.CC);
            Assert.AreEqual(_recipientsStr.ToString(), _testMsg.Recipients);
            Assert.AreEqual(_replyToListStr.ToString(), _testMsg.ReplyTo);
            Assert.AreEqual((int)MessageStatus.New, _testMsg.Status);
            Assert.AreEqual(0, _testMsg.SendAttempts);
            Assert.AreEqual(subject, _testMsg.Subject);
            Assert.AreEqual(body, _testMsg.Body);
        }

        [TestMethod]
        public void SendReservationConfirmationForGuestTest()
        {
            StringBuilder msgBody = new StringBuilder();

            ReservationConfirmationForGuest testEmail = new ReservationConfirmationForGuest(_recipients)
            {
                CcAddresses = _CCList,
                RecipientAddresses = _recipients,
                ReplyToAddresses = _replyToList,
                GuestFirstName = "GuestFirstName"
            };

            Template template = _messageService.GetTemplateByTemplateType(TemplateType.ReservationConfirmationForGuest, CultureCode.en_US);

            msgBody.Append(template.Content);
            string body = CompleteTemplate(msgBody.ToString(), testEmail.Parameters);
            string subject = CompleteTemplate(template.Title, testEmail.Parameters);

            _messageService.AddEmail(testEmail, CultureCode.en_US);

            _testMsg = _messageService.GetMessagesToSend().ElementAt(_messageService.GetMessagesToSend().Count() - 1);

            Assert.IsNotNull(_testMsg, Messages.ObjectIsNull);
            Assert.AreEqual(_CCListStr.ToString(), _testMsg.CC);
            Assert.AreEqual(_recipientsStr.ToString(), _testMsg.Recipients);
            Assert.AreEqual(_replyToListStr.ToString(), _testMsg.ReplyTo);
            Assert.AreEqual((int)MessageStatus.New, _testMsg.Status);
            Assert.AreEqual(0, _testMsg.SendAttempts);
            Assert.AreEqual(subject, _testMsg.Subject);
            Assert.AreEqual(body, _testMsg.Body);
        }

        [TestMethod]
        public void SendPaymentAgeVerificationEmailTest()
        {
            StringBuilder msgBody = new StringBuilder();

            PaymentAgeVerificationEmail testEmail = new PaymentAgeVerificationEmail(_recipients)
            {
                CcAddresses = _CCList,
                RecipientAddresses = _recipients,
                ReplyToAddresses = _replyToList,
                BirthDate = "21/12/2012",
                UserName = "UserName"
            };

            Template template = _messageService.GetTemplateByTemplateType(TemplateType.AgeVerificationFailed, CultureCode.en_US);

            msgBody.Append(template.Content);
            string body = CompleteTemplate(msgBody.ToString(), testEmail.Parameters);
            string subject = CompleteTemplate(template.Title, testEmail.Parameters);

            _messageService.AddEmail(testEmail, CultureCode.en_US);

            _testMsg = _messageService.GetMessagesToSend().ElementAt(_messageService.GetMessagesToSend().Count() - 1);

            Assert.IsNotNull(_testMsg, Messages.ObjectIsNull);
            Assert.AreEqual(_CCListStr.ToString(), _testMsg.CC);
            Assert.AreEqual(_recipientsStr.ToString(), _testMsg.Recipients);
            Assert.AreEqual(_replyToListStr.ToString(), _testMsg.ReplyTo);
            Assert.AreEqual((int)MessageStatus.New, _testMsg.Status);
            Assert.AreEqual(0, _testMsg.SendAttempts);
            Assert.AreEqual(subject, _testMsg.Subject);
            Assert.AreEqual(body, _testMsg.Body);
        }

        [TestMethod]
        public void GetTemplateByTemplateTypeTest()
        {
            Template testTemplateEN = _efContext.Templates.Where(f => f.Type == (int)TemplateType.OwnerActivationEmail && f.DictionaryCulture.CultureCode.Equals("en-US")).First();

            Template testTemplateDE = new Template()
            {
                Content = "DE",
                Title = "Das Titel",
                DictionaryCulture = _dictCulture.GetCultureByCode("de-DE"),
                Type = (int)TemplateType.OwnerActivationEmail
            };

            _efContext.Templates.Add(testTemplateDE);
            _efContext.SaveChanges();

            Template returnedEN = _messageService.GetTemplateByTemplateType(TemplateType.OwnerActivationEmail, CultureCode.en_US);
            Template returnedDE = _messageService.GetTemplateByTemplateType(TemplateType.OwnerActivationEmail, CultureCode.de_DE);

            Assert.IsNotNull(returnedEN, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedDE, Messages.ObjectIsNull);

            ExtendedAssert.AreEqual(testTemplateEN, returnedEN);
            ExtendedAssert.AreEqual(testTemplateDE, returnedDE);

            _efContext.Templates.Remove(testTemplateDE);
            _efContext.SaveChanges();
        }
        #endregion
    }
}
