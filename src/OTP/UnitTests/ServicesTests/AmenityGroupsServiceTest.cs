﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using DataAccess.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class AmenityGroupsServiceTest
    {
        #region Fields
        /// <summary>
        /// list of amenity groups used in test methods
        /// </summary>
        private List<AmenityGroup> _testAmenityGroups = new List<AmenityGroup>();
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;
        #endregion

        #region Services
        private IAmenityGroupsService _amenityGroupsService;
        private IAmenityService _amenitiesService;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);

            _amenityGroupsService = new AmenityGroupsService(mock.Object);
            _amenitiesService = new AmenityService(mock.Object);
            #endregion
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (AmenityGroup ag in _testAmenityGroups)
            {
                if (_amenityGroupsService.GetAmenityGroupById(ag.AmenityGroupId) != null)
                    _amenityGroupsService.DeleteAmenityGroup(ag.AmenityGroupId);
            }
            _testAmenityGroups.Clear();
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void GetAmenityGroupByIdTest()
        {
            AmenityGroup expected = new AmenityGroup()
            {
                AmenityIcon = 123
            };
            expected.SetNameValue("GetAmenityGroupByIdTest_Name");

            _amenityGroupsService.AddAmenityGroup(expected);
            _efContext.SaveChanges();
            _testAmenityGroups.Add(expected);

            var actual = _amenityGroupsService.GetAmenityGroupById(expected.AmenityGroupId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddAmenityGroupTest()
        {
            AmenityGroup expected = new AmenityGroup()
            {
                AmenityIcon = 12
            };
            expected.SetNameValue("AddAmenityGroupTest_Name");

            _amenityGroupsService.AddAmenityGroup(expected);
            _efContext.SaveChanges();
            _testAmenityGroups.Add(expected);

            var actual = _amenityGroupsService.GetAmenityGroupById(expected.AmenityGroupId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddAmenityGroupRequiredFieldsTest()
        {
            AmenityGroup amenityGroup;

            #region Test field AmenityIcon
            // AmenityIcon is of type int and cannot be nullified
            #endregion

            #region Test field Name_i18n
            amenityGroup = new AmenityGroup()
            {
                AmenityIcon = 123
            };

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _amenityGroupsService.AddAmenityGroup(amenityGroup);
                _efContext.SaveChanges();
            }, "Name_i18n");
            #endregion
        }

        [TestMethod]
        public void GetAmenityGroupsTest()
        {
            AmenityGroup[] newAmenityGroups = new AmenityGroup[5];
            Dictionary<int, bool> newAmenityGroupsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newAmenityGroups[i] = new AmenityGroup()
                {
                    AmenityIcon = 123
                };
                newAmenityGroups[i].SetNameValue("GetAmenityGroupsTest_Name_" + i);

                _amenityGroupsService.AddAmenityGroup(newAmenityGroups[i]);
                _efContext.SaveChanges();
                _testAmenityGroups.Add(newAmenityGroups[i]);

                newAmenityGroupsFound.Add(newAmenityGroups[i].AmenityGroupId, false);
            }

            AmenityGroup expected;
            var actual = _amenityGroupsService.GetAmenityGroups();
            foreach (var actualObj in actual)
            {
                if (newAmenityGroupsFound.ContainsKey(actualObj.AmenityGroupId))
                {
                    expected = _amenityGroupsService.GetAmenityGroupById(actualObj.AmenityGroupId);

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newAmenityGroupsFound[actualObj.AmenityGroupId] = true;
                }
            }

            if (newAmenityGroupsFound.Any(nag => nag.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "amenity groups", "GetAmenityGroups"));
        }

        [TestMethod]
        public void DeleteAmenityGroupTest()
        {
            #region Delete successfully
            AmenityGroup deleted = new AmenityGroup()
            {
                AmenityIcon = 123
            };
            deleted.SetNameValue("DeleteAmenityGroupTest_Name_Successfully");

            _amenityGroupsService.AddAmenityGroup(deleted);
            _efContext.SaveChanges();

            _amenityGroupsService.DeleteAmenityGroup(deleted.AmenityGroupId);

            var dbAmenityGroup = _amenityGroupsService.GetAmenityGroupById(deleted.AmenityGroupId);

            Assert.IsNull(dbAmenityGroup, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced amenity group deletion
            AmenityGroup referencedAmenityGroup = new AmenityGroup()
            {
                AmenityIcon = 123
            };
            referencedAmenityGroup.SetNameValue("DeleteAmenityGroupTest_Name_Referenced");

            _amenityGroupsService.AddAmenityGroup(referencedAmenityGroup);

            Amenity referencedAmenity = new Amenity()
            {
                AmenityGroup = referencedAmenityGroup,
                AmenityCode = "DeleteAmenityGroupTest_Code_Referenced",
            };
            referencedAmenity.SetTitleValue("DeleteAmenityGroupTest_Title_Referenced");

            _amenitiesService.AddAmenity(referencedAmenity);

            _efContext.SaveChanges();

            bool exceptionCaught = false;
            try
            {
                _amenityGroupsService.DeleteAmenityGroup(referencedAmenityGroup.AmenityGroupId);
            }
            catch (Exception)
            {
                exceptionCaught = true;
            }

            _amenitiesService.DeleteAmenity(referencedAmenity.AmenityID);

            if (!exceptionCaught)
                Assert.Fail(Messages.DeletionShouldNotSucceed);
            #endregion
        }

        [TestMethod]
        public void ChangeAmenityGroupPositionTest()
        {
            AmenityGroup[] newAmenityGroups = new AmenityGroup[5];

            for (int i = 0; i < 5; ++i)
            {
                newAmenityGroups[i] = new AmenityGroup()
                {
                    AmenityIcon = 123
                };
                newAmenityGroups[i].SetNameValue("GetAmenityGroupsTest_Name_" + i);

                _amenityGroupsService.AddAmenityGroup(newAmenityGroups[i]);
                _efContext.SaveChanges();
                _testAmenityGroups.Add(newAmenityGroups[i]);
            }

            int lastpos = 0;
            IEnumerable<AmenityGroup> agList = _amenityGroupsService.GetAmenityGroups();
            lastpos = agList.Max(f => f.Position);

            #region Test1

            int expectedPosition = lastpos;

            _amenityGroupsService.ChangeAmenityGroupPosition(newAmenityGroups[0].AmenityGroupId, 5);

            int actualPosition = newAmenityGroups[0].Position;

            Assert.AreEqual(expectedPosition, actualPosition);

            #endregion

            #region Test2

            expectedPosition = lastpos - 4;

            _amenityGroupsService.ChangeAmenityGroupPosition(newAmenityGroups[1].AmenityGroupId, -1);

            actualPosition = newAmenityGroups[1].Position;

            Assert.AreEqual(expectedPosition, actualPosition);

            #endregion

            #region Test3

            expectedPosition = lastpos;

            _amenityGroupsService.ChangeAmenityGroupPosition(newAmenityGroups[2].AmenityGroupId, 10);

            actualPosition = newAmenityGroups[2].Position;

            Assert.AreEqual(expectedPosition, actualPosition);

            #endregion

            #region Test4

            expectedPosition = newAmenityGroups[3].Position;

            _amenityGroupsService.ChangeAmenityGroupPosition(newAmenityGroups[3].AmenityGroupId, 0);

            actualPosition = newAmenityGroups[3].Position;

            Assert.AreEqual(expectedPosition, actualPosition);

            #endregion
        }

        #endregion
    }
}
