﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using BusinessLogic.PaymentGateway;
using DataAccess;
using DataAccess.Enums;
using DataAccess.CustomObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Common.Exceptions;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class PaymentGatewayServiceTest
    {
        #region Tokens

        private const string PAYMENT_TOKEN = "BWrtwqQban0DFhlUnINsIFBGMdP";

        private const string INVALID_CARD_TOKEN = "7hlw8UB2ZigT4VNCFG0Uy2ZwaRf";

        #endregion

        #region Fields
        /// <summary>
        /// test objects
        /// </summary>
        private List<Reservation> _testReservations = new List<Reservation>();
        private Reservation _testReservation = null;
        private Property _testProperty = null;
        private Unit _testUnit = null;
        private UnitType _testUnitType = null;
        private UnitRate _testUnitRate = null;
        private Tax _testTax = null;
        private Destination _testDestination = null;
        private DictionaryCountry _testCountry = null;
        private DictionaryCulture _testCulture = null;
        private User _testUser = null;
        private GuestPaymentMethod _testPaymentMethod = null;

        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext = null;
        #endregion

        #region Services

        private IEventLogService _logService = null;
        private IMessageService _messageService = null;
        private IPaymentGatewayService _paymentService = null;
        private IReservationsService _reservationsService = null;
        private IUserService _usersService = null;
        private IPropertiesService _propertiesService = null;
        private IUnitsService _unitsService = null;
        private IUnitTypesService _unitTypesService = null;
        private IDictionaryCultureService _dictionaryCultureService = null;
        private IDestinationsService _destinationService = null;
        private IDictionaryCountryService _dictCountryService = null;
        private ITaxesService _taxesService = null;
        private IBookingService _bookingService = null;

        private IZohoCrmService _zohoCrmService = null;
        private ICrmOperationLogsService _crmOperationLogsService = null;
        private ISettingsService _settingsService = null;

        #endregion

        #region Prepare/Cleanup Data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            IStateService stateService = new StateService(mock.Object);
            IBlobService blobService = new BlobService(mock.Object);
            IPropertyAddOnsService propertyAddOnsService = new PropertyAddOnsService(mock.Object);
            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _messageService = new MessageService(mock.Object, _settingsService);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _propertiesService = new PropertiesServices(configurationService, blobService, _settingsService, mock.Object);
            _taxesService = new TaxesService(mock.Object);
            _unitTypesService = new UnitTypesService(mock.Object);
            _unitsService = new UnitsService(mock.Object, stateService, _unitTypesService);
            _usersService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
            _logService = new EventLogService(mock.Object);
            _paymentService = new PaymentGatewayService(mock.Object, _logService, _settingsService, _messageService);
            IAuthorizationService authorizationService = new AuthorizationService(_usersService, mock.Object);
            IBookingService bookingService = new BookingService(mock.Object, _propertiesService, propertyAddOnsService, _taxesService, _unitsService, _usersService, stateService, _logService, _settingsService, _messageService, _paymentService, _unitTypesService, authorizationService);
            IDocumentService documentService = new PdfDocumentService(mock.Object, _settingsService);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);

            _reservationsService = new ReservationsService(configurationService, blobService, mock.Object, _propertiesService, bookingService, _paymentService, documentService, _messageService, _settingsService, _unitsService);
            _destinationService = new DestinationsService(configurationService, mock.Object);
            IPropertyTypesService _propertyTypeService = new PropertyTypeService(mock.Object);
            _dictCountryService = new DictionaryCountryService(mock.Object);
           
            _bookingService = new BookingService(mock.Object, _propertiesService, propertyAddOnsService, _taxesService,
                                    _unitsService, _usersService, stateService, _logService, _settingsService,
                                    _messageService, _paymentService, _unitTypesService, authorizationService);
            
            #endregion

            #region Setup data

            _testCountry = _dictCountryService.GetCountries().First();
            _testCulture = _dictionaryCultureService.GetCultures().First();

            _testDestination = new Destination();
            _testDestination.SetDestinationNameValue("TestDestName");
            _testDestination.SetDestinationDescriptionValue("DestDescrition");
            _destinationService.AddDestination(_testDestination);

            _testTax = new Tax()
            {
                Percentage = 20.0m,
            };
            _testTax.SetNameValue("TestTaxName");
            _taxesService.AddTax(_testTax);

            _testDestination.Taxes.Add(_testTax);

            _testUser = new User()
            {
                Lastname = "BookingServiceTest_Lastname",
                Firstname = "BookingServiceTest_Firstname",
                Address1 = "BookingServiceTest_Address1",
                Address2 = "BookingServiceTest_Address2",
                State = "BookingServiceTest_State",
                ZIPCode = "BookingServiceTest_ZIPCode",
                City = "BookingServiceTest_City",
                DictionaryCountry = _testCountry,
                CellPhone = "BookingServiceTest_CellPhone",
                LandLine = "BookingServiceTest_LandLine",
                email = "BookingServiceTest_email",
                DriverLicenseNbr = "BookingServiceTest_DriverLicenseNbr",
                PassportNbr = "BookingServiceTest_PassportNbr",
                SocialsecurityNbr = "BookingServiceTest_SocialsecurityNbr",
                DirectDepositInfo = "BookingServiceTest_DirectDepositInfo",
                SecurityQuestion = "BookingServiceTest_SecurityQuestion",
                SecurityAnswer = "BookingServiceTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "BookingServiceTest_AcceptedTCsInitials",
                Password = "BookingServiceTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = _testCulture
            };
            _usersService.AddUser(_testUser);

            PropertyType propertyType = new PropertyType();
            propertyType.SetNameValue("BookingServiceTest_PropertyTypeName");
            propertyType.PersistI18nValues();
                
            _testProperty = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "BookingServiceTest_PropertyCode",
                Address1 = "BookingServiceTest_Address1",
                Address2 = "BookingServiceTest_Address2",
                State = "BookingServiceTest_State",
                ZIPCode = "BookingServiceTest_ZipCode",
                City = "BookingServiceTest_City",
                DictionaryCountry = _testCountry,                 
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern"
            };
            _testProperty.SetPropertyNameValue("BookingServiceTest_PropertyName");
            _testProperty.SetShortDescriptionValue("BookingServiceTest_ShortDescription");
            _testProperty.SetLongDescriptionValue("BookingServiceTest_LongDescription");
            _testProperty.PersistI18nValues();
            _propertiesService.AddProperty(_testProperty);

            _testUnitType  = new UnitType()
            {
                Property = _testProperty,
                UnitTypeCode = "BookingServiceTest_UnitTypeCode"
                
            };
            _testUnitType.SetTitleValue("BookingServiceTest_UnitTypeTitle");
            _testUnitType.SetDescValue("BookingServiceTest_UnitTypeDesc");
            _testUnitType.PersistI18nValues();
            _unitTypesService.AddUnitType(_testUnitType);

            _testUnitRate = new UnitRate()
            {
                DailyRate = 500.0m,
                DateFrom = new DateTime(2012, 12, 21),
                DateUntil = new DateTime(2013, 1, 11),
            };
            _unitsService.AddUnitRate(_testUnitRate);

            _testUnit = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                UnitRates = new List<UnitRate>(),
                UnitCode = "BookingServiceTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            _testUnit.UnitRates.Add(_testUnitRate);
            _testUnit.SetTitleValue("BookingServiceTest_Title");
            _testUnit.SetDescValue("BookingServiceTest_Desc");
            _testUnit.PersistI18nValues();

            _unitsService.AddUnit(_testUnit);

            _efContext.SaveChanges();

            #endregion
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            int uid = _testProperty.User.UserID;
            if (_taxesService.GetTaxById(_testTax.TaxId) != null)
                _taxesService.DeleteTax(_testTax.TaxId);

            foreach (Reservation r in _testReservations)
                if (_reservationsService.GetReservationById(r.ReservationID) != null)
                    //_reservationsService.DeleteReservationById(r.ReservationID);
                    DataAccessHelper.RemoveReservation(_efContext, r);
            _testReservations.Clear();

            if (_unitsService.GetUnitById(_testUnit.UnitID) != null)
                _unitsService.DeleteUnit(_testUnit.UnitID);

            if (_unitTypesService.GetUnitTypeById(_testUnitType.UnitTypeID) != null)
                _unitTypesService.DeleteUnitType(_testUnitType.UnitTypeID);

            if (_propertiesService.GetPropertyById(_testProperty.PropertyID) != null)
                _propertiesService.RemoveProperty(_testProperty.PropertyID);

            if (_destinationService.GetDestinationById(_testDestination.DestinationID) != null)
                _destinationService.DeleteDestination(_testDestination.DestinationID);
            _usersService.DeleteUserById(uid);
    
        }
        #endregion

        #region Test Methods

        [TestMethod]
        public void GetPaymentAdditionalDataTest()
        {
            #region Valid credit card

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "0005",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int) BookingStatus.CheckedIn,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = PAYMENT_TOKEN,
                TripBalance = 1.0m,
                PaymentReceived = 2.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture
                
            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            PaymentMethodResponse expected = new PaymentMethodResponse()
            {
                CardType = "american_express",
                CreditCardNumber = "XXXX-XXXX-XXXX-0005",
                Errors = new string[0],
                Last4Digits = "0005",
                Token = PAYMENT_TOKEN,
                AdditionalData = new AdditionalData()
                {
                    Address = "268 Rolland Drive, Unit 12",
                    City = "Arlington",
                    CountryId = "us", 
                    CreditHolderId = "3",
                    FirstName = "GUEST",
                    Initials = "GL",
                    LastName = "LGBS",
                    State = "Virginia",
                    ZipCode = "76001"
                }
            };

            PaymentMethodResponse actual = _paymentService.GetPaymentAdditionalData(PAYMENT_TOKEN, _testReservation.ReservationID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            ExtendedAssert.AreEqual(expected, actual);

            #endregion

            #region Invalid credit card

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = "aaa",
                TripBalance = 1.0m,
                PaymentReceived = 2.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture
            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            expected = new PaymentMethodResponse()
            {
                CardType = "american_express",
                CreditCardNumber = "XXXX-XXXX-XXXX-8431",
                Errors = new string[0],
                Last4Digits = "8431",
                Token = INVALID_CARD_TOKEN,
                AdditionalData = new AdditionalData()
                {
                    Address = "268 Rolland Drive, Unit 12",
                    City = "Arlington",
                    CountryId = "us",
                    CreditHolderId = "3",
                    FirstName = "GUEST",
                    Initials = "GL",
                    LastName = "LGBS",
                    State = "Virginia",
                    ZipCode = "76001"
                }
            };

            actual = _paymentService.GetPaymentAdditionalData(INVALID_CARD_TOKEN, _testReservation.ReservationID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            ExtendedAssert.AreEqual(expected, actual);

            #endregion

            #region Invalid token

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),                 
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = "aaa",
                TripBalance = 1.0m,
                PaymentReceived = 2.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            ExtendedAssert.Throws<PaymentGatewayException>(() =>
                {
                    actual = _paymentService.GetPaymentAdditionalData("invalid_token", _testReservation.ReservationID);
                });

            #endregion
        }

        [TestMethod]
        public void AuthorizePaymentTest()
        {
            #region Valid credit card

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "0005",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),
                 
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = PAYMENT_TOKEN,
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            TransactionRequest request = new TransactionRequest()
            {
                Amount = "1",
                CurrencyCode="USD",
                PaymentToken = PAYMENT_TOKEN
            };

            TransactionResponse expected = new TransactionResponse()
            {
                Amount = "1",
                CurrencyCode = "USD",
                Message = new BusinessLogic.Objects.PaymentGateway.Message()
                {
                    MessageKey = "messages.transaction_succeeded",
                    Text = "Succeeded!"
                },
                Succeded = true,
                OnTest = "true",
                State = "succeeded",
                TransactionType = "Authorization",
                Response = new TransactionResponseElement()
                {
                    ErrorCode = "",
                    Message = "Successful authorize",
                    Success = true,
                    PaymentMethod = null
                }

            };

            TransactionResponse actual = _paymentService.AuthorizePayment(request, _testReservation.ReservationID);

            ExtendedAssert.AreEqual(expected, actual);

            #endregion

            #region Invalid credit card

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),
                 
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = INVALID_CARD_TOKEN,
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            request = new TransactionRequest()
            {
                Amount = "1",
                CurrencyCode = "USD",
                PaymentToken = INVALID_CARD_TOKEN
            };

            expected = new TransactionResponse()
            {
                Amount = "1",
                CurrencyCode = "USD",
                Message = new BusinessLogic.Objects.PaymentGateway.Message()
                {
                    MessageKey = null,
                    Text = "Unable to process the authorize transaction."
                },
                Succeded = false,
                OnTest = "true",
                State = "gateway_processing_failed",
                TransactionType = "Authorization",
                Response = new TransactionResponseElement()
                {
                    ErrorCode = "",
                    Message = "Unable to process the authorize transaction.",
                    Success = false,
                    PaymentMethod = null
                }

            };

            actual = _paymentService.AuthorizePayment(request, _testReservation.ReservationID);

            ExtendedAssert.AreEqual(expected, actual);

            #endregion

            #region Invalid token

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),                 
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = "invalid_token",
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            request = new TransactionRequest()
            {
                Amount = "1",
                CurrencyCode = "USD",
                PaymentToken = "invalid_token"
            };

            ExtendedAssert.Throws<PaymentGatewayException>(() =>
                {
                    actual = _paymentService.AuthorizePayment(request, _testReservation.ReservationID);
                });

            #endregion
        }

        [TestMethod]
        public void PurchaseTest()
        {
            #region Valid credit card

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "0005",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = PAYMENT_TOKEN,
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            TransactionRequest request = new TransactionRequest()
            {
                Amount = "1",
                CurrencyCode = "USD",
                PaymentToken = PAYMENT_TOKEN
            };

            TransactionResponse expected = new TransactionResponse()
            {
                Amount = "1",
                CurrencyCode = "USD",
                Message = new BusinessLogic.Objects.PaymentGateway.Message()
                {
                    MessageKey = "messages.transaction_succeeded",
                    Text = "Succeeded!"
                },
                Succeded = true,
                OnTest = "true",
                State = "succeeded",
                TransactionType = "Purchase",
                Response = new TransactionResponseElement()
                {
                    ErrorCode = "",
                    Message = "Successful purchase",
                    Success = true,
                    PaymentMethod = null
                }

            };

            TransactionResponse actual = _paymentService.Purchase(request, _testReservation.ReservationID);

            ExtendedAssert.AreEqual(expected, actual);

            #endregion

            #region Invalid credit card

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = INVALID_CARD_TOKEN,
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            request = new TransactionRequest()
            {
                Amount = "1",
                CurrencyCode = "USD",
                PaymentToken = INVALID_CARD_TOKEN
            };

            expected = new TransactionResponse()
            {
                Amount = "1",
                CurrencyCode = "USD",
                Message = new BusinessLogic.Objects.PaymentGateway.Message()
                {
                    MessageKey = null,
                    Text = "Unable to process the purchase transaction."
                },
                Succeded = false,
                OnTest = "true",
                State = "gateway_processing_failed",
                TransactionType = "Purchase",
                Response = new TransactionResponseElement()
                {
                    ErrorCode = "",
                    Message = "Unable to process the purchase transaction.",
                    Success = false,
                    PaymentMethod = null
                }

            };

            actual = _paymentService.Purchase(request, _testReservation.ReservationID);

            ExtendedAssert.AreEqual(expected, actual);

            #endregion

            #region Invalid token

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = "invalid_token",
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            request = new TransactionRequest()
            {
                Amount = "1",
                CurrencyCode = "USD",
                PaymentToken = "invalid_token"
            };

            ExtendedAssert.Throws<PaymentGatewayException>(() =>
            {
                actual = _paymentService.Purchase(request, _testReservation.ReservationID);
            });

            #endregion
        }

        [TestMethod]
        public void CapturePaymentTest()
        {
            #region Valid credit card

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "0005",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = PAYMENT_TOKEN,
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            TransactionRequest request = new TransactionRequest()
            {
                Amount = "1",
                CurrencyCode = "USD",
                PaymentToken = PAYMENT_TOKEN
            };

            TransactionResponse expected = new TransactionResponse()
            {
                Amount = "1",
                CurrencyCode = "USD",
                Message = new BusinessLogic.Objects.PaymentGateway.Message()
                {
                    MessageKey = "messages.transaction_succeeded",
                    Text = "Succeeded!"
                },
                Succeded = true,
                OnTest = "true",
                State = "succeeded",
                TransactionType = "Capture",
                Response = new TransactionResponseElement()
                {
                    ErrorCode = "",
                    Message = "Successful capture",
                    Success = true,
                    PaymentMethod = null
                }
            };

            TransactionResponse actual = _paymentService.AuthorizePayment(request, _testReservation.ReservationID);

            actual = _paymentService.CapturePayment(actual.Token, _testReservation.ReservationID);

            ExtendedAssert.AreEqual(expected, actual);

            #endregion

            #region Invalid credit card

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),                 
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = INVALID_CARD_TOKEN,
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            request = new TransactionRequest()
            {
                Amount = "1",
                CurrencyCode = "USD",
                PaymentToken = INVALID_CARD_TOKEN
            };

            actual = _paymentService.AuthorizePayment(request, _testReservation.ReservationID);

            ExtendedAssert.Throws<PaymentGatewayException>(() =>
                {
                    actual = _paymentService.CapturePayment(actual.Token, _testReservation.ReservationID);
                });

            #endregion

            #region Invalid token

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),                 
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = "invalid_token",
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            ExtendedAssert.Throws<PaymentGatewayException>(() =>
            {
                actual = _paymentService.CapturePayment("invalid_token", _testReservation.ReservationID);
            });

            #endregion
        }

        [TestMethod]
        public void CancelPaymentTest()
        {
            #region Valid credit card

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "0005",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),                 
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = PAYMENT_TOKEN,
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture
            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            TransactionRequest request = new TransactionRequest()
            {
                Amount = "1",
                CurrencyCode = "USD",
                PaymentToken = PAYMENT_TOKEN
            };

            TransactionResponse expected = new TransactionResponse()
            {
                Amount = null,
                CurrencyCode = null,
                Message = new BusinessLogic.Objects.PaymentGateway.Message()
                {
                    MessageKey = "messages.transaction_succeeded",
                    Text = "Succeeded!"
                },
                Succeded = true,
                OnTest = "true",
                State = "succeeded",
                TransactionType = "Void",
                Response = new TransactionResponseElement()
                {
                    ErrorCode = "",
                    Message = "Successful void",
                    Success = true,
                    PaymentMethod = null
                }
            };

            TransactionResponse actual = _paymentService.AuthorizePayment(request, _testReservation.ReservationID);

            actual = _paymentService.CancelPayment(actual.Token, _testReservation.ReservationID);

            ExtendedAssert.AreEqual(expected, actual);

            #endregion

            #region Invalid credit card

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),                 
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = INVALID_CARD_TOKEN,
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            request = new TransactionRequest()
            {
                Amount = "1",
                CurrencyCode = "USD",
                PaymentToken = INVALID_CARD_TOKEN
            };

            actual = _paymentService.AuthorizePayment(request, _testReservation.ReservationID);

            ExtendedAssert.Throws<PaymentGatewayException>(() =>
            {
                actual = _paymentService.CancelPayment(actual.Token, _testReservation.ReservationID);
            });

            #endregion

            #region Invalid token

            _testPaymentMethod = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = "8431",
                CreditCardType = _bookingService.GetCreditCardTypes().First(),
                ReferenceToken = "a",
                User = _testUser
            };

            _testReservation = new Reservation()
            {
                Agreement = new byte[] { 1, 2, 3, 4, 5 },
                BillingAddress = "PaymentGatewayServiceTest_BillingAddress",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(1),
                DateDeparture = DateTime.Now.AddDays(8),                 
                GuestPaymentMethod = _testPaymentMethod,
                Invoice = new byte[] { 1, 2, 3, 4, 5 },
                NumberOfGuests = 1,
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                TotalPrice = 1.0m,
                TransactionToken = "invalid_token",
                TripBalance = 1.0m,
                PaymentReceived = 1.0m,
                LinkAuthorizationToken = Guid.NewGuid().ToString(),
                DictionaryCulture = _testCulture

            };
            _testProperty.Reservations.Add(_testReservation);
            _testUnit.Reservations.Add(_testReservation);
            _testUnitType.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges(); _testReservations.Add(_testReservation);

            ExtendedAssert.Throws<PaymentGatewayException>(() =>
            {
                actual = _paymentService.CancelPayment("invalid_token", _testReservation.ReservationID);
            });

            #endregion
        }

        #endregion
    }
}
