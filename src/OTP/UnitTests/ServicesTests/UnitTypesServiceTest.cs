﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using DataAccess.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class UnitTypesServiceTest
    {
        #region Fields
        /// <summary>
        /// objects used in tests
        /// </summary>
        private Property _testProperty = null;
        private User _testUser = null;
        private DictionaryCulture _testCulture = null;
        private List<UnitType> _testUnitTypes = new List<UnitType>();
        private List<UnitType7dayDiscounts> _testDiscounts = new List<UnitType7dayDiscounts>();
        private List<UnitTypeMLO> _testMlos = new List<UnitTypeMLO>();
        private List<Unit> _testUnits = new List<Unit>();
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;
        #endregion

        #region Services
        private IUnitTypesService _unitTypesService = null;

        private IPropertiesService _propertiesService = null;

        private IDestinationsService _destinationService = null;
        
        private IPropertyTypesService _propTypeService = null;

        private IDictionaryCountryService _dictCountryService = null;

        private IUserService _userService = null;

        private IBlobService _blobService = null;

        private IUnitsService _unitService = null;

        private IStateService _stateService = null;

        private IDictionaryCultureService _dictionaryCultureService = null;

        private IZohoCrmService _zohoCrmService = null;

        private IMessageService _messageService = null;

        private ICrmOperationLogsService _crmOperationLogsService = null;

        private ISettingsService _settingsService = null;

        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);

            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _messageService = new MessageService(mock.Object, _settingsService);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _unitTypesService = new UnitTypesService(mock.Object);
            _blobService = new BlobService(mock.Object);
            _propertiesService = new PropertiesServices(configurationService, _blobService, _settingsService, mock.Object);
            _destinationService = new DestinationsService(configurationService, mock.Object);
            _propTypeService = new PropertyTypeService(mock.Object);
            _dictCountryService = new DictionaryCountryService(mock.Object);
            _userService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
            _stateService = new StateService(mock.Object);
            _unitService = new UnitsService(mock.Object, _stateService, _unitTypesService);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);

            #endregion

            #region Setup Property
            DictionaryCountry _testCountry = _dictCountryService.GetCountries().First();
            Destination _testDestination = _destinationService.GetDestinations().First();
            PropertyType _testPropertyType = _propTypeService.GetPropertyTypes().First();
            _testCulture = _dictionaryCultureService.GetCultures().First();

            _testUser = new User()
            {
                Firstname = "PropertiesServiceTest_Firstname",
                Lastname = "PropertiesServiceTest_Lastname",
                email = "PropertiesServiceTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(_testUser);
            _efContext.SaveChanges();

            _testProperty = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "PropertiesServiceTest_PropertyCode",
                Address1 = "PropertiesServiceTest_Address1",
                State = "PropertiesServiceTest_State",
                ZIPCode = "PropertiesServiceTest_ZipCode",
                City = "PropertiesServiceTest_City",
                DictionaryCountry = _testCountry,
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "TimeZone"
            };
            _testProperty.SetPropertyNameValue("PropertiesServiceTest_PropertyName");
            _testProperty.SetShortDescriptionValue("PropertiesServiceTest_ShortDescription");
            _testProperty.SetLongDescriptionValue("PropertiesServiceTest_LongDescription");

            _propertiesService.AddProperty(_testProperty);
            _efContext.SaveChanges();
            #endregion
        }

        /// <summary>
        /// Cleanup the data before each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (UnitType7dayDiscounts d in _testDiscounts)
            {
                if (_unitTypesService.GetUnitType7DaysDiscount(d.UnitType7dayID) != null)
                    _unitTypesService.DeleteUnitType7DaysDiscount(d.UnitType7dayID);
            }
            _testDiscounts.Clear();

            foreach (Unit u in _testUnits)
            {
                if (_unitService.GetUnitById(u.UnitID) != null)
                    _unitService.DeleteUnit(u.UnitID);
            }
            _testUnits.Clear();

            foreach (UnitType ut in _testUnitTypes)
            {
                if (_unitTypesService.GetUnitTypeById(ut.UnitTypeID) != null)
                    _unitTypesService.DeleteUnitType(ut.UnitTypeID);
            }
            _testUnitTypes.Clear();

            if (_testProperty != null)
            {
                if (_propertiesService.GetPropertyById(_testProperty.PropertyID) != null)
                {
                    int uid = _testProperty.User.UserID;
                    _propertiesService.RemoveProperty(_testProperty.PropertyID);
                    _userService.DeleteUserById(uid);
                }
            }
        }
        #endregion

        #region Test methods

        #region UnitType
        [TestMethod]
        public void GetUnitTypesByPropertyIdTest()
        {
            UnitType[] unitTypes = new UnitType[5];
            for (int i  = 0; i < 5; i++)
            {
                unitTypes[i] = new UnitType()
                {
                    UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode" + i.ToString(),
                    Property = _testProperty
                };
                unitTypes[i].SetTitleValue("UnitTypesServiceTest_UnitTypeTitle" + i.ToString());
                unitTypes[i].SetDescValue("UnitTypesServiceTest_UnitTypeDesc" + i.ToString());

                _testUnitTypes.Add(unitTypes[i]);
                _unitTypesService.AddUnitType(unitTypes[i]);
            }

            Unit[] units = new Unit[5];
            for (int i = 0; i < 5; i++)
            {
                units[i] = new Unit()
                {
                    Property = _testProperty,
                    UnitType = unitTypes[i],
                    UnitCode = "UnitTypesServiceTest_UnitCode" + i.ToString()
                };
                units[i].SetTitleValue("UnitTypesServiceTest_UnitTitle" + i.ToString(), CultureCode.en_US);
                units[i].SetDescValue("UnitTypesServiceTest_UnitDesc" + i.ToString(), CultureCode.en_US); 

                _testUnits.Add(units[i]);
                _unitService.AddUnit(units[i]);
            }

             _efContext.SaveChanges();

            List<UnitType> returned = _unitTypesService.GetUnitTypesByPropertyId(_testProperty.PropertyID).ToList();

            for (int i = 0; i < 5; i++)
            {
                ExtendedAssert.AreEqual(unitTypes[i], returned[i]);
            }
        }

        [TestMethod]
        public void AddUnitTypeTest()
        {
            UnitType expected = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode1",
                Property = _testProperty
            };
            expected.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            expected.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            _testUnitTypes.Add(expected);
            _unitTypesService.AddUnitType(expected);
            _efContext.SaveChanges();

            UnitType actual = _unitTypesService.GetUnitTypeById(expected.UnitTypeID);

            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddUnitTypeRequiredFieldsTest()
        {
            #region Property
            UnitType unitType1 = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode1",
                Property = null
            };
            unitType1.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            unitType1.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            ExtendedAssert.ThrowsdbUpdateException(() =>
                {
                    _unitTypesService.AddUnitType(unitType1);
                    _efContext.SaveChanges();
                }, "FK_UnitTypes_Properties");
            _efContext.UnitTypes.Remove(unitType1);
            _efContext.SaveChanges();
            #endregion

            #region Unit type code
            UnitType unitType2 = new UnitType()
            {
                UnitTypeCode = null,
                Property = _testProperty
            };
            unitType2.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            unitType2.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _unitTypesService.AddUnitType(unitType2);
                _efContext.SaveChanges();
            }, "UnitTypeCode");
            _efContext.UnitTypes.Remove(unitType2);
            _efContext.SaveChanges();
            #endregion
        }

        [TestMethod]
        public void DeleteUnitTypeTest()
        {
            #region Deletion successfull
            UnitType expected = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode1",
                Property = _testProperty
            };
            expected.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            expected.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            _testUnitTypes.Add(expected);
            _unitTypesService.AddUnitType(expected);
            _efContext.SaveChanges();

            int utid = expected.UnitTypeID;
            _unitTypesService.DeleteUnitType(expected.UnitTypeID);

            UnitType test = _unitTypesService.GetUnitTypeById(utid);
            Assert.IsNull(test, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced unit type deletion

            UnitType unitType = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode1",
                Property = _testProperty
            };
            unitType.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            unitType.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            _testUnitTypes.Add(unitType);
            _unitTypesService.AddUnitType(unitType);

            Unit unit = new Unit()
            {
                Property = _testProperty,
                UnitType = unitType,
                UnitCode = "UnitTypesServiceTest_UnitCode1"
            };
            unit.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            unit.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            _testUnits.Add(unit);
            _unitService.AddUnit(unit);

            _efContext.SaveChanges();

            ExtendedAssert.Throws<Exception>(() =>
            {
                _unitTypesService.DeleteUnitType(unitType.UnitTypeID);
            });
            #endregion
        }
        #endregion

        #region UnitType7DayDiscount
        [TestMethod]
        public void Get7DayUnitTypeDiscountsTest()
        {
            UnitType unitType = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode1",
                Property = _testProperty
            };
            unitType.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            unitType.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            _testUnitTypes.Add(unitType);
            _unitTypesService.AddUnitType(unitType);

            UnitType7dayDiscounts discount1 = new UnitType7dayDiscounts()
            {
                UnitType = unitType,
                Discount = 20.0m,
                DateFrom = new DateTime(2012, 12, 21),
                DateUntil = new DateTime(2014, 12, 28)
            };
            _testDiscounts.Add(discount1);
            _unitTypesService.AddUnitType7DayDiscount(discount1);

            UnitType7dayDiscounts discount2 = new UnitType7dayDiscounts()
            {
                UnitType = unitType,
                Discount = 20.0m,
                DateFrom = new DateTime(2040, 12, 21),
                DateUntil = new DateTime(2041, 12, 28)
            };
            _testDiscounts.Add(discount2);
            _unitTypesService.AddUnitType7DayDiscount(discount2);

            _efContext.SaveChanges();

            List<UnitType7dayDiscounts> returned = _unitTypesService.Get7DayUnitTypeDiscounts(unitType.UnitTypeID).ToList();

            ExtendedAssert.AreEqual(discount1, returned[0]);
            ExtendedAssert.AreEqual(discount2, returned[1]);
        }

        [TestMethod]
        public void AddUnitType7DayDiscountTest()
        {
            UnitType unitType = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode1",
                Property = _testProperty
            };
            unitType.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            unitType.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            _testUnitTypes.Add(unitType);
            _unitTypesService.AddUnitType(unitType);

            UnitType7dayDiscounts expected = new UnitType7dayDiscounts()
            {
                UnitType = unitType,
                Discount = 20.0m,
                DateFrom = new DateTime(2012, 12, 21),
                DateUntil = new DateTime(2014, 12, 28)
            };
            _testDiscounts.Add(expected);
            _unitTypesService.AddUnitType7DayDiscount(expected);

            _efContext.SaveChanges();

            UnitType7dayDiscounts actual = _unitTypesService.GetUnitType7DaysDiscount(expected.UnitType7dayID);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddUnitType7DayDiscountRequiredFieldsTest()
        {
            UnitType unitType = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode",
                Property = _testProperty
            };
            unitType.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle");
            unitType.SetDescValue("UnitTypesServiceTest_UnitTypeDesc");

            _testUnitTypes.Add(unitType);
            _unitTypesService.AddUnitType(unitType);
            _efContext.SaveChanges();

            #region Unit type
            UnitType7dayDiscounts discount1 = new UnitType7dayDiscounts()
            {
                UnitType = null,
                Discount = 20.0m,
                DateFrom = new DateTime(2012, 12, 21),
                DateUntil = new DateTime(2014, 12, 28)
            };
            _testDiscounts.Add(discount1);

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _unitTypesService.AddUnitType7DayDiscount(discount1);
                _efContext.SaveChanges();
            }, "FK_UnitType7dayDiscounts_UnitTypes");
            _efContext.UnitType7dayDiscounts.Remove(discount1);
            _efContext.SaveChanges();
            #endregion
        }

        [TestMethod]
        public void DeleteUnitType7DaysDiscountTest()
        {
            #region Deletion successfull
            UnitType unitType1 = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode1",
                Property = _testProperty
            };
            unitType1.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            unitType1.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            _testUnitTypes.Add(unitType1);
            _unitTypesService.AddUnitType(unitType1);

            UnitType7dayDiscounts expected = new UnitType7dayDiscounts()
            {
                UnitType = unitType1,
                Discount = 20.0m,
                DateFrom = new DateTime(2012, 12, 21),
                DateUntil = new DateTime(2014, 12, 28)
            };
            _testDiscounts.Add(expected);
            _unitTypesService.AddUnitType7DayDiscount(expected);

            _efContext.SaveChanges();

            int did = expected.UnitType7dayID;
            _unitTypesService.DeleteUnitType7DaysDiscount(expected.UnitType7dayID);

            UnitType7dayDiscounts test = _unitTypesService.GetUnitType7DaysDiscount(did);
            Assert.IsNull(test, Messages.ObjectIsNotNull);
            #endregion
        }

        [TestMethod]
        public void GetUnitType7DaysDiscountTest()
        {
            UnitType unitType = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode1",
                Property = _testProperty
            };
            unitType.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            unitType.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            _testUnitTypes.Add(unitType);
            _unitTypesService.AddUnitType(unitType);

            UnitType7dayDiscounts expected = new UnitType7dayDiscounts()
            {
                UnitType = unitType,
                Discount = 20.0m,
                DateFrom = new DateTime(2012, 12, 21),
                DateUntil = new DateTime(2044, 12, 28)
            };
            _testDiscounts.Add(expected);
            _unitTypesService.AddUnitType7DayDiscount(expected);

            _efContext.SaveChanges();

            UnitType7dayDiscounts actual = _unitTypesService.GetUnitType7DaysDiscount(expected.UnitType7dayID);
            ExtendedAssert.AreEqual(expected, actual);     
        }
        #endregion

        #region UnitTypeMlos
        [TestMethod]
        public void DeleteUnitTypeMlosTest()
        {
            #region Deletion successfull
            UnitType unitType1 = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode",
                Property = _testProperty
            };
            unitType1.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle");
            unitType1.SetDescValue("UnitTypesServiceTest_UnitTypeDesc");

            _testUnitTypes.Add(unitType1);
            _unitTypesService.AddUnitType(unitType1);

            UnitTypeMLO expected = new UnitTypeMLO()
            {
                UnitType = unitType1,
                MLOS = 20,
                DateFrom = new DateTime(2012, 12, 21),
                DateUntil = new DateTime(2014, 12, 28)
            };
            _testMlos.Add(expected);
            _unitTypesService.AddUnitTypeMlos(expected);

            _efContext.SaveChanges();

            int did = expected.UnitTypeMLOSID;
            _unitTypesService.DeleteUnitTypeMlos(expected.UnitTypeMLOSID);

            UnitTypeMLO test = _unitTypesService.GetUnitTypeMlosById(did);
            Assert.IsNull(test, Messages.ObjectIsNotNull);
            #endregion
        }

        [TestMethod]
        public void AddUnitTypeMlosTest()
        {
            UnitType unitType = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode",
                Property = _testProperty
            };
            unitType.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle");
            unitType.SetDescValue("UnitTypesServiceTest_UnitTypeDesc");

            _testUnitTypes.Add(unitType);
            _unitTypesService.AddUnitType(unitType);

            UnitTypeMLO expected = new UnitTypeMLO()
            {
                UnitType = unitType,
                MLOS = 14,
                DateFrom = new DateTime(2012, 12, 21),
                DateUntil = new DateTime(2014, 12, 28)
            };
            _testMlos.Add(expected);
            _unitTypesService.AddUnitTypeMlos(expected);

            _efContext.SaveChanges();

            UnitTypeMLO actual = _unitTypesService.GetUnitTypeMlosById(expected.UnitTypeMLOSID);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddUnitTypeMlosRequiredFieldsTest()
        {
            UnitType unitType = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode",
                Property = _testProperty
            };
            unitType.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle");
            unitType.SetDescValue("UnitTypesServiceTest_UnitTypeDesc");

            _testUnitTypes.Add(unitType);
            _unitTypesService.AddUnitType(unitType);
            _efContext.SaveChanges();

            #region Unit type
            UnitTypeMLO mlos1 = new UnitTypeMLO()
            {
                UnitType = null,
                MLOS = 14,
                DateFrom = new DateTime(2012, 12, 21),
                DateUntil = new DateTime(2014, 12, 28)
            };
            _testMlos.Add(mlos1);

            ExtendedAssert.ThrowsdbUpdateException(() =>
                {
                    _unitTypesService.AddUnitTypeMlos(mlos1);
                    _efContext.SaveChanges();
                }, "FK_UnitTypeMLOS_UnitTypes");
            _efContext.UnitTypeMLOS.Remove(mlos1);
            _efContext.SaveChanges();
            #endregion
        }

        [TestMethod]
        public void GetUnitTypeMlosTest()
        {
            UnitType unitType = new UnitType()
            {
                UnitTypeCode = "UnitTypesServiceTest_UnitTypeCode1",
                Property = _testProperty
            };
            unitType.SetTitleValue("UnitTypesServiceTest_UnitTypeTitle1");
            unitType.SetDescValue("UnitTypesServiceTest_UnitTypeDesc1");

            _testUnitTypes.Add(unitType);
            _unitTypesService.AddUnitType(unitType);

            UnitTypeMLO[] mlos = new UnitTypeMLO[5];
            for (int i = 0; i < 5; i++)
            {
                mlos[i] = new UnitTypeMLO()
                {
                    UnitType = unitType,
                    MLOS = i * 7,
                    DateFrom = new DateTime(2013, 1, 1).AddMonths(i),
                    DateUntil = new DateTime(2013, 2, 1).AddMonths(i)
                };
                _testMlos.Add(mlos[i]);
                _unitTypesService.AddUnitTypeMlos(mlos[i]);
            }
            _efContext.SaveChanges();

            List<UnitTypeMLO> returned = _unitTypesService.GetUnitTypeMlos(unitType.UnitTypeID).ToList();
            for (int i = 0; i < 5; i++)
            {
                ExtendedAssert.AreEqual(mlos[i], returned[i]); 
            } 
        }
        #endregion

        #endregion
    }
}
