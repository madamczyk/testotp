﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class SalesPersonsTest
    {
        #region Fields

        /// <summary>
        /// List for test sales persons
        /// </summary>
        private readonly List<SalesPerson> _testSalesPersons = new List<SalesPerson>();

        /// <summary>
        /// DataBase context
        /// </summary>
        private OTPEntities _dbContext;

        #endregion

        #region Services

        private ISalesPersonsService _salesPersonsService;

        #endregion

        #region Begin & end test methods

        /// <summary>
        /// Initializes test with neccessary data and bindings
        /// </summary>
        [TestInitialize()]
        public void TestInit()
        {
            CreateDbContext();
        }

        /// <summary>
        /// Cleans up data after test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (SalesPerson salesPerson in _testSalesPersons)
            {
                if (_salesPersonsService.GetSalesPersonById(salesPerson.SalesPersonId) != null)
                {
                    _salesPersonsService.DeleteSalesPerson(salesPerson.SalesPersonId);
                }
            }

            _testSalesPersons.Clear();
        }

        #endregion

        #region TestMethods

        [TestMethod]
        public void GetSalesPersonByIdTest()
        {
            SalesPerson expectedSalesPerson = new SalesPerson()
            {
                FirstName = "GetSalesPersonByIdTest_FirstName",
                LastName = "GetSalesPersonByIdTest_LastName",
                CommisionDate = DateTime.Now,
                Percentage = 3.14M
            };

            _salesPersonsService.AddSalesPerson(expectedSalesPerson);
            _dbContext.SaveChanges();
            _testSalesPersons.Add(expectedSalesPerson);

            var actualSalesPerson = _salesPersonsService.GetSalesPersonById(expectedSalesPerson.SalesPersonId);

            Assert.IsNotNull(actualSalesPerson);
            ExtendedAssert.AreEqual(expectedSalesPerson, actualSalesPerson);
        }

        [TestMethod]
        public void AddSalesPersonsTest()
        {
            SalesPerson expectedSalesPerson = new SalesPerson()
            {
                FirstName = "AddSalesPersonTest_FirstName",
                LastName = "AddSalesPersonTest_LastName",
                CommisionDate = DateTime.Now,
                Percentage = 3.14M
            };

            _salesPersonsService.AddSalesPerson(expectedSalesPerson);
            _dbContext.SaveChanges();
            _testSalesPersons.Add(expectedSalesPerson);

            var actualSalesPerson = _salesPersonsService.GetSalesPersonById(expectedSalesPerson.SalesPersonId);

            Assert.IsNotNull(actualSalesPerson);
            ExtendedAssert.AreEqual(expectedSalesPerson, actualSalesPerson);
        }

        [TestMethod]
        public void GetSalesPersonsTest()
        {
            SalesPerson[] newSalesPerson = new SalesPerson[5];
            Dictionary<int, bool> newSalesPersonsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newSalesPerson[i] = new SalesPerson
                    {
                        FirstName = string.Format("GetSalesPersonsTest_FirstName_{0}", i),
                        LastName = string.Format("GetSalesPersonsTest_LastName_{0}", i),
                        CommisionDate = DateTime.Now.AddDays(i),
                        Percentage = i * 10 + i + 3.54M
                    };

                _salesPersonsService.AddSalesPerson(newSalesPerson[i]);
                _dbContext.SaveChanges();

                _testSalesPersons.Add(newSalesPerson[i]);

                newSalesPersonsFound.Add(newSalesPerson[i].SalesPersonId, false);
            }

            SalesPerson expectedSalesPerson;
            var actualSalesPersons = _salesPersonsService.GetSalesPersons();

            foreach (var actualSalesPerson in actualSalesPersons)
            {
                if (newSalesPersonsFound.ContainsKey(actualSalesPerson.SalesPersonId))
                {
                    expectedSalesPerson = newSalesPerson.Where(a => a.SalesPersonId == actualSalesPerson.SalesPersonId).FirstOrDefault();

                    Assert.IsNotNull(expectedSalesPerson);
                    ExtendedAssert.AreEqual(expectedSalesPerson, actualSalesPerson);

                    newSalesPersonsFound[actualSalesPerson.SalesPersonId] = true;
                }
            }

            if (newSalesPersonsFound.Any(na => na.Value == false))
            {
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "salesPersons", "GetSalesPersons"));
            }
        }

        [TestMethod]
        public void DeleteSalesPersonsTest()
        {
            SalesPerson deleteSalesPerson = new SalesPerson()
            {
                FirstName = "DeleteSalesPersonsTest_FirstName",
                LastName = "DeleteSalesPersonsTest_LastName",
                CommisionDate = DateTime.Now,
                Percentage = 3.54M
            };

            _salesPersonsService.AddSalesPerson(deleteSalesPerson);
            _dbContext.SaveChanges();

            _salesPersonsService.DeleteSalesPerson(deleteSalesPerson.SalesPersonId);

            var dbSalesPerson = _salesPersonsService.GetSalesPersonById(deleteSalesPerson.SalesPersonId);

            Assert.IsNull(dbSalesPerson, Messages.ObjectIsNotNull);
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Creates and assings DB Context and business logic services
        /// </summary>
        private void CreateDbContext()
        {
            _dbContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_dbContext);
            mock.Setup(f => f.Current).Returns(new object());

            _salesPersonsService = new SalesPersonsService(mock.Object);
        }
 
        #endregion
    }
}
