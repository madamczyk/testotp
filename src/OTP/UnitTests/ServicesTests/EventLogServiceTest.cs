﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using DataAccess.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class EventLogServiceTest
    {
        #region Fields
        /// <summary>
        /// list of amenity groups used in test methods
        /// </summary>
        private List<EventLog> _testLogs = new List<EventLog>();

        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;
        #endregion

        #region Services
        private IEventLogService _eventLogService;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());
            
            _eventLogService= new EventLogService(mock.Object);
            #endregion
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (EventLog el in _testLogs)
            {
                if (_eventLogService.GetEventLogById(el.Id) != null)
                    _eventLogService.DeleteEventLogById(el.Id);
            }
            _testLogs.Clear();
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void AddEventLogTest()
        {
            EventLog expected = new EventLog()
            {
                CorrelationId = new Guid(),
                Timestamp = DateTime.Now,
                Category = EventCategory.Error.ToString(),
                Source = EventLogSource.Intranet.ToString(),
                Message = "AddEventLogTest_Message"

            };
            _testLogs.Add(expected);
            _eventLogService.AddEventLog(expected);
            _efContext.SaveChanges();

            var actual = _eventLogService.GetEventLogById(expected.Id);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetEventLogByIdTest()
        {
            EventLog expected = new EventLog()
            {
                CorrelationId = new Guid(),
                Timestamp = DateTime.Now,
                Category = EventCategory.Error.ToString(),
                Source = EventLogSource.Intranet.ToString(),
                Message = "GetEventLogByIdTest_Message"
                
            };
            _testLogs.Add(expected);
            _eventLogService.AddEventLog(expected);
            _efContext.SaveChanges();

            var actual = _eventLogService.GetEventLogById(expected.Id);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetEventLogsTest()
        {
            Dictionary<int, bool> newEventLogsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; i++)
            {
                EventLog temp = new EventLog()
                {
                    CorrelationId = new Guid(),
                    Timestamp = DateTime.Now,
                    Category = EventCategory.Error.ToString(),
                    Source = EventLogSource.Intranet.ToString(),
                    Message = "GetEventLogsTest_Message_" + i

                };
                _testLogs.Add(temp);
                _eventLogService.AddEventLog(temp);
                _efContext.SaveChanges();

                newEventLogsFound.Add(_testLogs[i].Id, false);
            }

            EventLog expected;
            var actual = _eventLogService.GetEventLogs();

            foreach (var actualObj in actual)
            {
                if (newEventLogsFound.ContainsKey(actualObj.Id))
                {
                    expected = _testLogs.Where(a => a.Id == actualObj.Id).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newEventLogsFound[actualObj.Id] = true;
                }
            }

            if (newEventLogsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "event logs", "GetEventLogs"));
        }

        [TestMethod]
        public void GetEventLogsForDayTest()
        {
            Dictionary<int, bool> newEventLogsFound = new Dictionary<int, bool>();
            Random rnd = new Random();
            EventLog temp;
            DateTime Now = DateTime.Now;

            #region Prepare logs
            temp = new EventLog()
            {
                CorrelationId = new Guid(),
                Timestamp = Now.AddDays(-2),
                Category = EventCategory.Error.ToString(),
                Source = EventLogSource.Intranet.ToString(),
                Message = "GetEventLogsForDayTest_Message_1"

            };
            _testLogs.Add(temp);
            _eventLogService.AddEventLog(temp);
            _efContext.SaveChanges();

            temp = new EventLog()
            {
                CorrelationId = new Guid(),
                Timestamp = Now.AddDays(-1),
                Category = EventCategory.Error.ToString(),
                Source = EventLogSource.Intranet.ToString(),
                Message = "GetEventLogsForDayTest_Message_2"

            };
            _testLogs.Add(temp);
            _eventLogService.AddEventLog(temp);
            _efContext.SaveChanges();

            temp = new EventLog()
            {
                CorrelationId = new Guid(),
                Timestamp = Now,
                Category = EventCategory.Error.ToString(),
                Source = EventLogSource.Intranet.ToString(),
                Message = "GetEventLogsForDayTest_Message_3"

            };
            _testLogs.Add(temp);
            _eventLogService.AddEventLog(temp);
            _efContext.SaveChanges();
            newEventLogsFound.Add(temp.Id, false);

            temp = new EventLog()
            {
                CorrelationId = new Guid(),
                Timestamp = Now,
                Category = EventCategory.Error.ToString(),
                Source = EventLogSource.Intranet.ToString(),
                Message = "GetEventLogsForDayTest_Message_4"

            };
            _testLogs.Add(temp);
            _eventLogService.AddEventLog(temp);
            _efContext.SaveChanges();
            newEventLogsFound.Add(temp.Id, false);

            temp = new EventLog()
            {
                CorrelationId = new Guid(),
                Timestamp = Now.AddDays(1),
                Category = EventCategory.Error.ToString(),
                Source = EventLogSource.Intranet.ToString(),
                Message = "GetEventLogsForDayTest_Message_5"

            };
            _testLogs.Add(temp);
            _eventLogService.AddEventLog(temp);
            _efContext.SaveChanges();

            temp = new EventLog()
            {
                CorrelationId = new Guid(),
                Timestamp = Now.AddDays(2),
                Category = EventCategory.Error.ToString(),
                Source = EventLogSource.Intranet.ToString(),
                Message = "GetEventLogsForDayTest_Message_6"

            };
            _testLogs.Add(temp);
            _eventLogService.AddEventLog(temp);
            _efContext.SaveChanges();
            #endregion

            EventLog expected;
            var actual = _eventLogService.GetEventLogsForDay(Now);

            foreach (var actualObj in actual)
            {
                if (newEventLogsFound.ContainsKey(actualObj.Id))
                {
                    expected = _testLogs.Where(a => a.Id == actualObj.Id).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newEventLogsFound[actualObj.Id] = true;
                }
            }

            if (newEventLogsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "event logs", "GetEventLogsForDay"));
        }

        [TestMethod]
        public void DeleteEventLogByIdTest()
        {
            #region Delete successfully
            EventLog deleted = new EventLog()
            {
                CorrelationId = new Guid(),
                Timestamp = DateTime.Now,
                Category = EventCategory.Error.ToString(),
                Source = EventLogSource.Intranet.ToString(),
                Message = "DeleteEventLogByIdTest_Message"

            };

            _eventLogService.AddEventLog(deleted);
            _efContext.SaveChanges();

            _eventLogService.DeleteEventLogById(deleted.Id);

            var dbEventLog = _eventLogService.GetEventLogById(deleted.Id);

            Assert.IsNull(dbEventLog, Messages.ObjectIsNotNull);
            #endregion
        }
        #endregion
    }
}
