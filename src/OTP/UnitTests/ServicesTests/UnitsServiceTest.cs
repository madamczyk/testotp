﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using DataAccess.CustomObjects;
using BusinessLogic.Services;
using Moq;
using System.Configuration;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class UnitsServiceTest
    {
        #region Fields
        /// <summary>
        /// list of units used in test methods
        /// </summary>
        private List<Unit> _testUnits = new List<Unit>();

        private List<UnitType> _testUnitTypes = new List<UnitType>();

        private List<UnitTypeMLO> _testMloses = new List<UnitTypeMLO>();

        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;

        /// <summary>
        /// test objects used in methods
        /// </summary>
        private User _testUser;
        private Destination _testDestination;
        private DictionaryCountry _testCountry;
        private PropertyType _testPropertyType;
        private Property _testProperty;
        private Unit _testUnit;
        private UnitType _testUnitType;
        private DictionaryCulture _testCulture;
        private Reservation _testReservation = null;
        private User _testManager = null;
        #endregion

        #region Services

        private IPropertiesService _propertiesService;
        private IDestinationsService _destinationService;
        private IDictionaryCountryService _countryService;
        private IPropertyTypesService _propertyTypeService;
        private IUnitTypesService _unitTypesService;
        private IUnitsService _unitsService;
        private IUserService _userService;
        private IPropertyAddOnsService _addOnService;
        private IAppliancesService _appliancesService;
        private IBlobService _blobService;
        private IStateService _stateService;
        private IDictionaryCultureService _dictionaryCultureService;
        private IReservationsService _reservationsService = null;
        private IZohoCrmService _zohoCrmService = null;
        private ICrmOperationLogsService _crmOperationLogsService = null;
        private ISettingsService _settingsService = null;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IMessageService messageService = new MessageService(mock.Object, _settingsService);
            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);

            var _configurationService = new Mock<IConfigurationService>();
            _blobService = new BlobService(mock.Object);
            _appliancesService = new AppliancesService(mock.Object);
            _propertiesService = new PropertiesServices(_configurationService.Object, _blobService, _settingsService, mock.Object);
            _destinationService = new DestinationsService(_configurationService.Object, mock.Object);
            _propertiesService = new PropertiesServices(_configurationService.Object, _blobService, _settingsService, mock.Object);
            _destinationService = new DestinationsService(_configurationService.Object, mock.Object);
            _propertyTypeService = new PropertyTypeService(mock.Object);
            _countryService = new DictionaryCountryService(mock.Object);
            _unitTypesService = new UnitTypesService(mock.Object);
            _userService = new UsersService(mock.Object, _zohoCrmService, messageService, _settingsService);
            _addOnService = new PropertyAddOnsService(mock.Object);            
            _stateService = new StateService(mock.Object);
            _unitsService = new UnitsService(mock.Object, _stateService, _unitTypesService);       
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);
            IStateService stateService = new StateService(mock.Object);
            ITaxesService _taxesService = new TaxesService(mock.Object);
            IPropertyAddOnsService propertyAddOnsService = new PropertyAddOnsService(mock.Object);
            IEventLogService eventLogSevice = new EventLogService(mock.Object);
            IStorageQueueService queueService = new StorageQueueService(_configurationService.Object, mock.Object);
            IDocumentService _documentService = new PdfDocumentService(mock.Object, _settingsService);
            IMessageService _messageService = new MessageService(queueService, _documentService, mock.Object, _settingsService);
            IPaymentGatewayService paymentGatewayService = new PaymentGatewayService(mock.Object, eventLogSevice, _settingsService, _messageService);
            IAuthorizationService authorizationService = new AuthorizationService(_userService, mock.Object);
            IBookingService _bookingService = new BookingService(mock.Object, _propertiesService, propertyAddOnsService, _taxesService, _unitsService, _userService, stateService, eventLogSevice, _settingsService, _messageService, paymentGatewayService, _unitTypesService, authorizationService);
            _reservationsService = new ReservationsService(_configurationService.Object, _blobService, mock.Object, _propertiesService, _bookingService, paymentGatewayService, _documentService, _messageService, _settingsService, _unitsService);
            #endregion

            _testCountry = _countryService.GetCountries().First();
            _testDestination = _destinationService.GetDestinations().First();
            _testPropertyType = _propertyTypeService.GetPropertyTypes().First();
            _testCulture = _dictionaryCultureService.GetCultures().First();
            _testUser = new User()
            {
                Firstname = "UnitsServiceTest_Firstname",
                Lastname = "UnitsServiceTest_Lastname",
                email = "UnitsServiceTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(_testUser);

            _testProperty = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "UnitsServiceTest_PropertyCode",
                Address1 = "UnitsServiceTest_Address1",
                State = "UnitsServiceTest_State",
                ZIPCode = "UnitsServiceTest_ZipCode",
                City = "UnitsServiceTest_City",
                DictionaryCountry = _testCountry,
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "TZ",
                PropertyLive = true
            };
         
            _testProperty.SetPropertyNameValue("UnitsServiceTest_PropertyName");
            _testProperty.SetShortDescriptionValue("UnitsServiceTest_ShortDescription");
            _testProperty.SetLongDescriptionValue("UnitsServiceTest_LongDescription");

            _propertiesService.AddProperty(_testProperty);

            _testUnitType = new UnitType()
            {
                UnitTypeCode = "UnitsServiceTest_UnitTypeCode", 
                Property = _testProperty  
            };
            _testUnitType.SetTitleValue("UnitsServiceTest_UnitTypeTitle");
            _testUnitType.SetDescValue("UnitsServiceTest_UnitTypeDesc");

            _unitTypesService.AddUnitType(_testUnitType);

            _testUnit = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                UnitCode = "UnitsServiceTest_UnitCode"
            };
            _testUnit.SetTitleValue("UnitsServiceTest_UnitTypeTitle");
            _testUnit.SetDescValue("UnitsServiceTest_UnitTypeDesc");

            _unitsService.AddUnit(_testUnit);
            _testUnits.Add(_testUnit);

            _efContext.SaveChanges();
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            if (_testReservation != null)
                DataAccessHelper.RemoveReservation(_efContext, _testReservation);

            foreach (UnitTypeMLO ut in _testMloses)
            {
                if (_unitTypesService.GetUnitTypeMlosById(ut.UnitTypeMLOSID) != null)
                    _unitTypesService.DeleteUnitTypeMlos(ut.UnitTypeMLOSID);
            }
            _testMloses.Clear();

            foreach (Unit u in _testUnits)
            {
                if (_unitsService.GetUnitById(u.UnitID) != null)
                    _unitsService.DeleteUnit(u.UnitID);
            }
            _testUnits.Clear();

            foreach (UnitType ut in _testUnitTypes)
            {
                if (_unitTypesService.GetUnitTypeById(ut.UnitTypeID) != null)
                    _unitTypesService.DeleteUnitType(ut.UnitTypeID);
            }
            _testUnitTypes.Clear();

            _unitTypesService.DeleteUnitType(_testUnitType.UnitTypeID);
            _propertiesService.RemoveProperty(_testProperty.PropertyID);
            _userService.DeleteUserById(_testUser.UserID);
            if (_testManager != null) _userService.DeleteUserById(_testManager.UserID);
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void AddUnitAndGetUnitByIdTest()
        {
            Unit expected = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                UnitCode = "AddUnitAndGetUnitByIdTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            expected.SetTitleValue("AddUnitAndGetUnitByIdTest_Title");
            expected.SetDescValue("AddUnitAndGetUnitByIdTest_Desc");

            _unitsService.AddUnit(expected);
            _efContext.SaveChanges();
            _testUnits.Add(expected);

            var actual = _unitsService.GetUnitById(expected.UnitID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddUnitRequiredFieldsTest()
        {
            Unit unit;

            #region Test field PropertyID
            unit = new Unit()
            {
                UnitType = _testUnitType,
                UnitCode = "AddUnitRequiredFieldsTest_Code_PropertyID",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("AddUnitRequiredFieldsTest_Title_PropertyID");
            unit.SetDescValue("AddUnitRequiredFieldsTest_Desc_PropertyID");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _unitsService.AddUnit(unit);
                _efContext.SaveChanges();
            }, "FK_Units_Properties");
            _efContext.Units.Remove(unit);
            _efContext.SaveChanges();
            #endregion

            #region Test field UnitTypeID
            unit = new Unit()
            {
                Property = _testProperty,
                UnitCode = "AddUnitRequiredFieldsTest_Code_UnitTypeID",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("AddUnitRequiredFieldsTest_Title_UnitTypeID");
            unit.SetDescValue("AddUnitRequiredFieldsTest_Desc_UnitTypeID");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _unitsService.AddUnit(unit);
                _efContext.SaveChanges();
            }, "FK_Units_UnitTypes");
            _efContext.Units.Remove(unit);
            _efContext.SaveChanges();
            #endregion

            #region Test field UnitCode
            unit = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("AddUnitRequiredFieldsTest_Title_UnitCode");
            unit.SetDescValue("AddUnitRequiredFieldsTest_Desc_UnitCode");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _unitsService.AddUnit(unit);
                _efContext.SaveChanges();
            }, "UnitCode");
            _efContext.Units.Remove(unit);
            _efContext.SaveChanges();
            #endregion

            #region Test field UnitTitle_i18n
            unit = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                UnitCode = "AddUnitRequiredFieldsTest_Code_UnitTitle_i18n",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetDescValue("AddUnitRequiredFieldsTest_Desc_UnitTitle_i18n");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _unitsService.AddUnit(unit);
                _efContext.SaveChanges();
            }, "UnitTitle_i18n");
            _efContext.Units.Remove(unit);
            _efContext.SaveChanges();
            #endregion

            #region Test field UnitDesc_i18n
            unit = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                UnitCode = "AddUnitRequiredFieldsTest_Code_UnitDesc_i18n",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("AddUnitRequiredFieldsTest_Title_UnitDesc_i18n");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _unitsService.AddUnit(unit);
                _efContext.SaveChanges();
            }, "UnitDesc_i18n");
            _efContext.Units.Remove(unit);
            _efContext.SaveChanges();
            #endregion

            #region Test field CleaningStatus
            // CleaningStatus is of type int and cannot be nullified
            #endregion

            #region Test field MaxNumberOfGuests
            // MaxNumberOfGuests is of type int and cannot be nullified
            #endregion

            #region Test field MaxNumberOfBedRooms
            // MaxNumberOfBedRooms is of type int and cannot be nullified
            #endregion

            #region Test field MaxNumberOfBathrooms
            // MaxNumberOfBathrooms is of type int and cannot be nullified
            #endregion
        }

        [TestMethod]
        public void GetUnitMlosTest()
        {
            UnitTypeMLO unitTypeMlos;
            IEnumerable<UnitTypeMLO> actual;
            Dictionary<int, bool> expected;

            #region Prepare test MLOSes
            unitTypeMlos = new UnitTypeMLO()
            {
                UnitType = _testUnitType,
                MLOS = 3,
                DateFrom = new DateTime(2009, 9, 9),
                DateUntil = new DateTime(2013, 1, 1)
            };
            _unitTypesService.AddUnitTypeMlos(unitTypeMlos);

            unitTypeMlos = new UnitTypeMLO()
            {
                UnitType = _testUnitType,
                MLOS = 5,
                DateFrom = new DateTime(1800, 1, 1),
                DateUntil = new DateTime(5000, 12, 12)
            };
            _unitTypesService.AddUnitTypeMlos(unitTypeMlos);

            unitTypeMlos = new UnitTypeMLO()
            {
                UnitType = _testUnitType,
                MLOS = 7,
                DateFrom = new DateTime(2010, 1, 2),
                DateUntil = new DateTime(2012, 12, 12)
            };
            _unitTypesService.AddUnitTypeMlos(unitTypeMlos);

            unitTypeMlos = new UnitTypeMLO()
            {
                UnitType = _testUnitType,
                MLOS = 8,
                DateFrom = new DateTime(2009, 1, 1),
                DateUntil = new DateTime(2010, 1, 1)
            };
            _unitTypesService.AddUnitTypeMlos(unitTypeMlos);

            _efContext.SaveChanges();
            #endregion

            //#region Check MLOSes between NULL and NULL
            //actual = _unitsService.GetUnitMlos(_testUnit.UnitID, null, null);
            //expected = new Dictionary<int, bool>() { {3, false}, {5, false}, {7, false}, {8, false} };

            //foreach (var obj in actual)
            //    if (expected.ContainsKey(obj.MLOS))
            //        expected[obj.MLOS] = true;

            //if (expected.Any(m => m.Value == false))
            //    Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit MLOSes (null, null)", "GetUnitMlos"));
            //#endregion

            #region Check MLOSes between NULL and NULL
            actual = _unitsService.GetUnitMlos(_testUnit.UnitID, null, null);
            var expectedResult = new List<UnitTypeMLO>();
            CollectionAssert.AreEqual(expectedResult, (List<UnitTypeMLO>) actual); 
            #endregion

            //#region Check MLOSes between NULL and 2009/12/12
            //actual = _unitsService.GetUnitMlos(_testUnit.UnitID, null, new DateTime(2009, 12, 12));
            //expected = new Dictionary<int, bool>() { { 3, false }, { 5, false }, { 8, false } };

            //foreach (var obj in actual)
            //    if (expected.ContainsKey(obj.MLOS))
            //        expected[obj.MLOS] = true;

            //if (expected.Any(m => m.Value == false))
            //    Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit MLOSes (null, 2009/12/12)", "GetUnitMlos"));
            //#endregion

            #region Check MLOSes between NULL and 2009/12/12
            actual = _unitsService.GetUnitMlos(_testUnit.UnitID, null, new DateTime(2009, 12, 12));
            expectedResult = new List<UnitTypeMLO>();
            CollectionAssert.AreEqual(expectedResult, (List<UnitTypeMLO>)actual);
            #endregion

            //#region Check MLOSes between 2011/12/12 and NULL
            //actual = _unitsService.GetUnitMlos(_testUnit.UnitID, new DateTime(2011, 12, 12), null);
            //expected = new Dictionary<int, bool>() { { 3, false }, { 5, false }, { 7, false } };

            //foreach (var obj in actual)
            //    if (expected.ContainsKey(obj.MLOS))
            //        expected[obj.MLOS] = true;

            //if (expected.Any(m => m.Value == false))
            //    Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit MLOSes (2011/12/12, null)", "GetUnitMlos"));
            //#endregion

            #region Check MLOSes between NULL and 2011/12/12
            actual = _unitsService.GetUnitMlos(_testUnit.UnitID, null, new DateTime(2011, 12, 12));
            expectedResult = new List<UnitTypeMLO>();
            CollectionAssert.AreEqual(expectedResult, (List<UnitTypeMLO>)actual); 
            #endregion

            #region Check MLOSes between 2013/1/2 and 2014/12/12
            actual = _unitsService.GetUnitMlos(_testUnit.UnitID, new DateTime(2013, 1, 2), new DateTime(2014, 12, 12));
            expected = new Dictionary<int, bool>() { { 5, false } };

            foreach (var obj in actual)
                if (expected.ContainsKey(obj.MLOS))
                    expected[obj.MLOS] = true;

            if (expected.Any(m => m.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit MLOSes (2013/1/2, 2014/12/12)", "GetUnitMlos"));
            #endregion
        }

        [TestMethod]
        public void GetUnitsByPropertyIdTest()
        {
            Unit[] newUnits = new Unit[5];
            Dictionary<int, bool> newUnitsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newUnits[i] = new Unit()
                {
                    Property = _testProperty,
                    UnitType = _testUnitType,
                    UnitCode = "GetUnitsByPropertyIdTest_Code",
                    CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                    MaxNumberOfBathrooms = 1,
                    MaxNumberOfBedRooms = 2,
                    MaxNumberOfGuests = 3
                };
                newUnits[i].SetTitleValue("GetUnitsByPropertyIdTest_Title");
                newUnits[i].SetDescValue("GetUnitsByPropertyIdTest_Desc");

                _unitsService.AddUnit(newUnits[i]);
                _efContext.SaveChanges();

                newUnitsFound.Add(newUnits[i].UnitID, false);

                _testUnits.Add(newUnits[i]);
            }

            Unit expected;
            var actual = _unitsService.GetUnitsByPropertyId(_testProperty.PropertyID);
            foreach (var actualObj in actual)
            {
                if (newUnitsFound.ContainsKey(actualObj.UnitID))
                {
                    expected = newUnits.Where(u => u.UnitID == actualObj.UnitID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newUnitsFound[actualObj.UnitID] = true;
                }
            }

            if (newUnitsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "units", "GetUnitsByPropertyId"));
        }

        [TestMethod]
        public void GetUnitStaticContentInfoTest()
        {
            Dictionary<UnitStaticContentType, bool> types = new Dictionary<UnitStaticContentType, bool>();
            UnitStaticContent tmpUsc;
            List<UnitStaticContent> tmpList;
            UnitStaticContentResult[] newUscrs = new UnitStaticContentResult[3];
            Dictionary<int, bool> newPscsFound = new Dictionary<int, bool>();

            types.Add(UnitStaticContentType.Bathrooms, false);
            types.Add(UnitStaticContentType.Bedrooms, false);
            types.Add(UnitStaticContentType.LivingAreasAndDining, false);

            for (int k = 0; k < types.Count; ++k)
            {
                newUscrs[k] = new UnitStaticContentResult();

                newUscrs[k].ContentCode = (int)types.ElementAt(k).Key;
                tmpList = new List<UnitStaticContent>();
                for (int i = 0; i < 3; ++i)
                {
                    tmpUsc = new UnitStaticContent()
                    {
                        Unit = _testUnit,
                        ContentCode = (int)types.ElementAt(k).Key,
                    };
                    tmpUsc.SetContentValue("GetUnitStaticContentInfoTest_USC_Value_" + ((i + 1) * (k + 1)));

                    _unitsService.AddUnitStaticContent(tmpUsc);
                    _efContext.SaveChanges();

                    newPscsFound.Add(tmpUsc.UnitStaticContentId, false);
                    tmpList.Add(tmpUsc);
                }

                newUscrs[k].ContentItems = tmpList;
            }

            UnitStaticContent expected;
            var actual = _unitsService.GetUnitStaticContentInfo(_testUnit.UnitID);
            foreach (var obj in actual)
            {
                foreach (var newObj in newUscrs)
                {
                    if (newObj.ContentCode == obj.ContentCode)
                    {
                        foreach (var actualObj in obj.ContentItems)
                        {
                            expected = newObj.ContentItems.Where(u => u.UnitStaticContentId == actualObj.UnitStaticContentId).FirstOrDefault();

                            Assert.IsNotNull(expected);
                            ExtendedAssert.AreEqual(expected, actualObj);

                            newPscsFound[actualObj.UnitStaticContentId] = true;
                        }

                        types[(UnitStaticContentType)obj.ContentCode] = true;
                    }
                }
            }

            if (types.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit static contents (types)", "GetUnitStaticContentInfo"));

            if (newPscsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit static contents", "GetUnitStaticContentInfo"));
        }

        [TestMethod]
        public void GetUnitAppliancesTest()
        {
            Appliance[] appliances = _appliancesService.GetAppliances().Take(5).ToArray();
            UnitAppliancy[] unitAppliances = new UnitAppliancy[5];
            Dictionary<int, bool> unitAppliancesFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                unitAppliances[i] = new UnitAppliancy()
                {
                    Unit = _testUnit,
                    Appliance = appliances[i]  
                };

                _unitsService.AddUnitAppliance(unitAppliances[i]);
                _efContext.SaveChanges();

                unitAppliancesFound.Add(unitAppliances[i].Appliance.ApplianceId, false);
            }

            UnitAppliancy expected;
            var actual = _unitsService.GetUnitAppliances(_testUnit.UnitID);
            foreach (var actualObj in actual)
            {
                if (unitAppliancesFound.ContainsKey(actualObj.ApplianceId))
                {
                    expected = unitAppliances.Where(u => u.Appliance.ApplianceId == actualObj.ApplianceId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected.Appliance, actualObj);

                    unitAppliancesFound[actualObj.ApplianceId] = true;
                }
            }

            if (unitAppliancesFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit appliances", "GetUnitAppliances"));
        }

        [TestMethod]
        public void DeleteUnitTest()
        {
            #region Delete successfully
            Unit deleted = new Unit()
            {
                Property = _testProperty,
                UnitType = _testUnitType,
                UnitCode = "DeleteUnitTest_Code_Successfully"
            };
            deleted.SetTitleValue("DeleteUnitTest_Title_Successfully");
            deleted.SetDescValue("DeleteUnitTest_Desc_Successfully");

            _unitsService.AddUnit(deleted);
            _efContext.SaveChanges();

            _unitsService.DeleteUnit(deleted.UnitID);

            var dbUnit = _unitsService.GetUnitById(deleted.UnitID);

            Assert.IsNull(dbUnit, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced unit deletion
            
            #endregion
        }

        [TestMethod]
        public void GetUnitStaticContentByTypeTest()
        {
            UnitStaticContent[] newUscrs = new UnitStaticContent[5];
            Dictionary<int, bool> newPscsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newUscrs[i] = new UnitStaticContent()
                {
                    Unit = _testUnit,
                    ContentCode = (int)UnitStaticContentType.Bathrooms
                };
                newUscrs[i].SetContentValue("GetUnitStaticContentByTypeTest_Value_" + i);

                _unitsService.AddUnitStaticContent(newUscrs[i]);
                _efContext.SaveChanges();

                newPscsFound.Add(newUscrs[i].UnitStaticContentId, false);
            }

            UnitStaticContent expected;
            var actual = _unitsService.GetUnitStaticContentByType(_testUnit.UnitID, UnitStaticContentType.Bathrooms);
            foreach (var actualObj in actual)
            {
                if (newPscsFound.ContainsKey(actualObj.UnitStaticContentId))
                {
                    expected = newUscrs.Where(u => u.UnitStaticContentId == actualObj.UnitStaticContentId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newPscsFound[actualObj.UnitStaticContentId] = true;
                }
            }

            if (newPscsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit static contents", "GetUnitStaticContentByTypeTest"));
        }

        [TestMethod]
        public void RemoveStaticContentTest()
        {
            #region Delete successfully
            UnitStaticContent deleted = new UnitStaticContent()
            {
                ContentCode = 666,
                Unit = _testUnit
            };

            _unitsService.AddUnitStaticContent(deleted);
            _efContext.SaveChanges();

            _unitsService.RemoveStaticContent(deleted.UnitStaticContentId);

            var dbUnitStaticContent = _unitsService.GetUnitStaticContentById(deleted.UnitStaticContentId);

            Assert.IsNull(dbUnitStaticContent, Messages.ObjectIsNotNull);
            #endregion
        }

        [TestMethod]
        public void AddUnitStaticContentAndGetUnitStaticContentByIdTest()
        {
            UnitStaticContent expected = new UnitStaticContent()
            {
                Unit = _testUnit,
                ContentCode = 666,
                ContentValue = "AddUnitStaticContentAndGetUnitStaticContentByIdTest_Value"
            };
            expected.SetContentValue("AddUnitStaticContentAndGetUnitStaticContentByIdTest_ValueXML");

            _unitsService.AddUnitStaticContent(expected);
            _efContext.SaveChanges();

            var actual = _unitsService.GetUnitStaticContentById(expected.UnitStaticContentId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetUnitRatesTest()
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            UnitRate[] newUrs = new UnitRate[5];
            Dictionary<int, bool> newUrsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newUrs[i] = new UnitRate()
                {
                    Unit = _testUnit,
                    DailyRate = Convert.ToDecimal(((double) rnd.Next(1000) + rnd.NextDouble())),
                    DateFrom = new DateTime(DateTime.Now.Ticks + rnd.Next()),
                    DateUntil = new DateTime(DateTime.Now.Ticks + rnd.Next())
                };

                _unitsService.AddUnitRate(newUrs[i]);
                _efContext.SaveChanges();

                newUrsFound.Add(newUrs[i].UnitRateID, false);
            }

            UnitRate expected;
            var actual = _unitsService.GetUnitRates(_testUnit.UnitID);
            foreach (var actualObj in actual)
            {
                if (newUrsFound.ContainsKey(actualObj.UnitRateID))
                {
                    expected = newUrs.Where(u => u.UnitRateID == actualObj.UnitRateID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newUrsFound[actualObj.UnitRateID] = true;
                }
            }

            if (newUrsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit rates", "GetUnitRates"));
        }

        [TestMethod]
        public void GetUnitPriceTest()
        {
            UnitRate unitRate;
            decimal? expected, actual;

            #region Test case: no period defined, without rates, without addons
            actual = _unitsService.GetUnitPrice(_testUnit.UnitID, null, null);

            Assert.IsFalse(actual.HasValue);
            #endregion

            #region Prepare test unit rates
            unitRate = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 400,
                DateFrom = new DateTime(2013, 1, 3),
                DateUntil = new DateTime(2013, 5, 15)
            };
            _unitsService.AddUnitRate(unitRate);

            unitRate = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 650,
                DateFrom = new DateTime(2013, 5, 16),
                DateUntil = new DateTime(2013, 8, 13)
            };
            _unitsService.AddUnitRate(unitRate);

            unitRate = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 800,
                DateFrom = new DateTime(2013, 9, 1),
                DateUntil = new DateTime(2013, 10, 31)
            };
            _unitsService.AddUnitRate(unitRate);

            unitRate = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 100,
                DateFrom = new DateTime(2010, 1, 1),
                DateUntil = new DateTime(2010, 10, 31)
            };
            _unitsService.AddUnitRate(unitRate);

            _efContext.SaveChanges();
            #endregion

            #region Test case: no period defined
            expected = 400;
            actual = _unitsService.GetUnitPrice(_testUnit.UnitID, null, null);

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion

            #region Test case: dateFrom defined
            expected = 400;
            actual = _unitsService.GetUnitPrice(_testUnit.UnitID, DateTime.Now, null);

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion

            #region Test case: dateUntil defined
            expected = 400;
            actual = _unitsService.GetUnitPrice(_testUnit.UnitID, null, DateTime.Now.AddMonths(2));

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion

            #region Test case: dates defined, test 1, without addons
            expected = 400 * 6 + 650 * 90 + 800 * 9;
            actual = _unitsService.GetUnitPrice(_testUnit.UnitID, new DateTime(2013, 5, 10), new DateTime(2013, 9, 10));

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion

            #region Test case: dates defined, test 2, without addons
            expected = 650 * 13 + 800 * 14;
            actual = _unitsService.GetUnitPrice(_testUnit.UnitID, new DateTime(2013, 8, 1), new DateTime(2013, 9, 15));

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion

            #region Test case: dates defined, test 3, without addons
            expected = 0;
            actual = _unitsService.GetUnitPrice(_testUnit.UnitID, new DateTime(2012, 1, 1), new DateTime(2012, 1, 10));

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion
        }

        [TestMethod]
        public void GetUnitPriceAccomodationTest()
        {
            UnitRate unitRate;
            decimal? expected, actual;

            #region Test case: no period defined, without rates
            actual = _unitsService.GetUnitPriceAccomodation(_testUnit.UnitID, null, null);

            Assert.IsFalse(actual.HasValue);
            #endregion

            #region Prepare test unit rates
            unitRate = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 400,
                DateFrom = new DateTime(2013, 1, 3),
                DateUntil = new DateTime(2013, 5, 15)
            };
            _unitsService.AddUnitRate(unitRate);

            unitRate = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 650,
                DateFrom = new DateTime(2013, 5, 16),
                DateUntil = new DateTime(2013, 8, 13)
            };
            _unitsService.AddUnitRate(unitRate);

            unitRate = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 800,
                DateFrom = new DateTime(2013, 9, 1),
                DateUntil = new DateTime(2013, 10, 31)
            };
            _unitsService.AddUnitRate(unitRate);

            unitRate = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 100,
                DateFrom = new DateTime(2010, 1, 1),
                DateUntil = new DateTime(2010, 10, 31)
            };
            _unitsService.AddUnitRate(unitRate);

            _efContext.SaveChanges();
            #endregion

            #region Test case: no period defined
            expected = 400;
            actual = _unitsService.GetUnitPriceAccomodation(_testUnit.UnitID, null, null);

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion

            #region Test case: dateFrom defined
            expected = 400;
            actual = _unitsService.GetUnitPriceAccomodation(_testUnit.UnitID, DateTime.Now, null);

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion

            #region Test case: dateUntil defined
            expected = 400;
            actual = _unitsService.GetUnitPriceAccomodation(_testUnit.UnitID, null, DateTime.Now.AddMonths(2));

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion

            #region Test case: dates defined, test 1, without addons
            expected = 400 * 6 + 650 * 90 + 800 * 9;
            actual = _unitsService.GetUnitPriceAccomodation(_testUnit.UnitID, new DateTime(2013, 5, 10), new DateTime(2013, 9, 10));

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion

            #region Test case: dates defined, test 2, without addons
            expected = 650 * 13 + 800 * 14;
            actual = _unitsService.GetUnitPriceAccomodation(_testUnit.UnitID, new DateTime(2013, 8, 1), new DateTime(2013, 9, 15));

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion

            #region Test case: dates defined, test 3, without addons
            expected = 0;
            actual = _unitsService.GetUnitPriceAccomodation(_testUnit.UnitID, new DateTime(2012, 1, 1), new DateTime(2012, 1, 10));

            Assert.AreEqual(expected.HasValue, actual.HasValue);
            Assert.AreEqual(expected.Value, actual.Value);
            #endregion
        }

        [TestMethod]
        public void AddUnitRateAndGetUnitRateByIdTest()
        {
            UnitRate expected = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 12345.67M,
                DateFrom = DateTime.Now,
                DateUntil = DateTime.Now.AddMonths(2).AddDays(3)
            };

            _unitsService.AddUnitRate(expected);
            _efContext.SaveChanges();

            var actual = _unitsService.GetUnitRateById(expected.UnitRateID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DeleteUnitRateTest()
        {
            #region Delete successfully
            UnitRate deleted = new UnitRate()
            {
                Unit = _testUnit,
                DateFrom = DateTime.Now,
                DateUntil = DateTime.Now.AddMonths(2).AddDays(3)
            };

            _unitsService.AddUnitRate(deleted);
            _efContext.SaveChanges();

            _unitsService.DeleteUnitRate(deleted.UnitRateID);

            var dbUnitRate = _unitsService.GetUnitRateById(deleted.UnitRateID);

            Assert.IsNull(dbUnitRate, Messages.ObjectIsNotNull);
            #endregion
        }

        [TestMethod]
        public void GetUnitApplianciesByUnitIdTest()
        {
            Appliance[] appliances = _appliancesService.GetAppliances().Take(5).ToArray();
            UnitAppliancy[] unitAppliances = new UnitAppliancy[5];
            Dictionary<int, bool> unitAppliancesFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                unitAppliances[i] = new UnitAppliancy()
                {
                    Unit = _testUnit,
                    Appliance = appliances[i]
                };

                _unitsService.AddUnitAppliance(unitAppliances[i]);
                _efContext.SaveChanges();

                unitAppliancesFound.Add(unitAppliances[i].UnitApplianceId, false);
            }

            UnitAppliancy expected;
            var actual = _unitsService.GetUnitApplianciesByUnitId(_testUnit.UnitID);
            foreach (var actualObj in actual)
            {
                if (unitAppliancesFound.ContainsKey(actualObj.UnitApplianceId))
                {
                    expected = unitAppliances.Where(u => u.UnitApplianceId == actualObj.UnitApplianceId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    unitAppliancesFound[actualObj.UnitApplianceId] = true;
                }
            }

            if (unitAppliancesFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit appliances", "GetUnitApplianciesByUnitId"));
        }

        [TestMethod]
        public void AddUnitApplianceAndGetUnitApplianceByIdTest()
        {
            UnitAppliancy expected = new UnitAppliancy()
            {
                Unit = _testUnit,
                Appliance = _appliancesService.GetAppliances().First()
            };

            _unitsService.AddUnitAppliance(expected);
            _efContext.SaveChanges();

            var actual = _unitsService.GetUnitApplianceById(expected.UnitApplianceId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DeleteUnitApplianceTest()
        {
            #region Delete successfully
            UnitAppliancy deleted = new UnitAppliancy()
            {
                Unit = _testUnit,
                Appliance = _appliancesService.GetAppliances().First()
            };

            _unitsService.AddUnitAppliance(deleted);
            _efContext.SaveChanges();

            _unitsService.DeleteUnitAppliance(deleted.UnitApplianceId);

            var dbUnitAppliancy = _unitsService.GetUnitApplianceById(deleted.UnitApplianceId);

            Assert.IsNull(dbUnitAppliancy, Messages.ObjectIsNotNull);
            #endregion
        }

        [TestMethod]
        public void AddUnitBlockadeAndGetUnitBlockageByIdTest()
        {
            UnitInvBlocking expected = new UnitInvBlocking()
            {
                Unit = _testUnit,
                DateFrom = DateTime.Now,
                DateUntil = DateTime.Now.AddDays(3),                
                Type = 666,
                Reservation = null
            };

            _unitsService.AddUnitBlockade(expected);
            _efContext.SaveChanges();

            var actual = _unitsService.GetUnitBlockageById(expected.UnitInvBlockingID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DeleteUnitBlockageTest()
        {
            #region Delete successfully
            UnitInvBlocking deleted = new UnitInvBlocking()
            {
                Unit = _testUnit,
                DateFrom = DateTime.Now,
                DateUntil = DateTime.Now.AddDays(3),
                Type = 666,
                Reservation = null
            };

            _unitsService.AddUnitBlockade(deleted);
            _efContext.SaveChanges();

            _unitsService.DeleteUnitBlockage(deleted.UnitInvBlockingID);

            var dbUnitInvBlocking = _unitsService.GetUnitBlockageById(deleted.UnitInvBlockingID);

            Assert.IsNull(dbUnitInvBlocking, Messages.ObjectIsNotNull);
            #endregion
        }

        [TestMethod]
        public void GetUnitBlockingsTest()
        {
            UnitInvBlocking[] newUnitBlockings = new UnitInvBlocking[5];
            Dictionary<int, bool> newUnitBlockingsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newUnitBlockings[i] = new UnitInvBlocking()
                {
                    Unit = _testUnit,
                    DateFrom = DateTime.Now,
                    DateUntil = DateTime.Now.AddDays(i+1),                    
                    Type = i * 5,
                    Reservation = null
                };

                _unitsService.AddUnitBlockade(newUnitBlockings[i]);
                _efContext.SaveChanges();

                newUnitBlockingsFound.Add(newUnitBlockings[i].UnitInvBlockingID, false);
            }

            UnitInvBlocking expected;
            var actual = _unitsService.GetUnitBlockings(_testUnit.UnitID);
            foreach (var actualObj in actual)
            {
                if (newUnitBlockingsFound.ContainsKey(actualObj.UnitInvBlockingID))
                {
                    expected = newUnitBlockings.Where(a => a.UnitInvBlockingID == actualObj.UnitInvBlockingID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newUnitBlockingsFound[actualObj.UnitInvBlockingID] = true;
                }
            }

            if (newUnitBlockingsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "unit blockings", "GetUnitBlockings"));
        }

        [TestMethod]
        public void GetUnitAvailabilityTest()
        {
            int i;
            UnitTypeMLO[] unitTypeMlos;
            UnitRate[] unitRates;
            UnitInvBlocking[] unitBlockages;
            IEnumerable<UnitAvailabilityResult> actual;
            List<UnitAvailabilityResult> expected;

            #region Prepare MLOSes
            unitTypeMlos = new UnitTypeMLO[3];

            unitTypeMlos[0] = new UnitTypeMLO()
            {
                UnitType = _testUnitType,
                MLOS = 3,
                DateFrom = new DateTime(2009, 1, 1),
                DateUntil = new DateTime(2010, 1, 1)
            };
            _unitTypesService.AddUnitTypeMlos(unitTypeMlos[0]);

            unitTypeMlos[1] = new UnitTypeMLO()
            {
                UnitType = _testUnitType,
                MLOS = 7,
                DateFrom = new DateTime(2010, 1, 2),
                DateUntil = new DateTime(2012, 11, 28)
            };
            _unitTypesService.AddUnitTypeMlos(unitTypeMlos[1]);

            unitTypeMlos[2] = new UnitTypeMLO()
            {
                UnitType = _testUnitType,
                MLOS = 8,
                DateFrom = new DateTime(2012, 12, 1),
                DateUntil = new DateTime(2012, 12, 31)
            };
            _unitTypesService.AddUnitTypeMlos(unitTypeMlos[2]);

            _efContext.SaveChanges();
            #endregion

            #region Prepare unit rates
            unitRates = new UnitRate[3];

            unitRates[0] = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 650,
                DateFrom = new DateTime(2009, 1, 1),
                DateUntil = new DateTime(2012, 11, 13)
            };
            _unitsService.AddUnitRate(unitRates[0]);

            unitRates[1] = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 800,
                DateFrom = new DateTime(2012, 11, 14),
                DateUntil = new DateTime(2012, 12, 5)
            };
            _unitsService.AddUnitRate(unitRates[1]);

            unitRates[2] = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = 100,
                DateFrom = new DateTime(2012, 12, 6),
                DateUntil = new DateTime(2012, 12, 31)
            };
            _unitsService.AddUnitRate(unitRates[2]);
            #endregion

            #region Prepare blockings
            unitBlockages = new UnitInvBlocking[2];

            unitBlockages[0] = new UnitInvBlocking()
            {
                Unit = _testUnit,
                DateFrom = new DateTime(2009, 7, 1),
                DateUntil = new DateTime(2009, 8, 31),
                Type = 123,
                Reservation = null
            };
            _unitsService.AddUnitBlockade(unitBlockages[0]);

            unitBlockages[1] = new UnitInvBlocking()
            {
                Unit = _testUnit,
                DateFrom = new DateTime(2012, 11, 10),
                DateUntil = new DateTime(2012, 12, 24),
                Type = 123,
                Reservation = null
            };
            _unitsService.AddUnitBlockade(unitBlockages[1]);

            _efContext.SaveChanges();
            #endregion

            #region Test 1
            #region Expected
            expected = new List<UnitAvailabilityResult>() {
                new UnitAvailabilityResult() { Date = "2012-11-01", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-02", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-03", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-04", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-05", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-06", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-07", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-08", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-09", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-10", IsBlocked = true, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-11", IsBlocked = true, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-12", IsBlocked = true, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-13", IsBlocked = true, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-14", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-15", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-16", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-17", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-18", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-19", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-20", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-21", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-22", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-23", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-24", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-25", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-26", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-27", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-28", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-29", IsBlocked = true, Mlos = 0, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-30", IsBlocked = true, Mlos = 0, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-12-01", IsBlocked = true, Mlos = 8, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-12-02", IsBlocked = true, Mlos = 8, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-12-03", IsBlocked = true, Mlos = 8, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-12-04", IsBlocked = true, Mlos = 8, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-12-05", IsBlocked = true, Mlos = 8, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-12-06", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-07", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-08", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-09", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-10", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-11", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-12", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-13", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-14", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-15", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-16", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-17", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-18", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-19", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-20", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-21", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-22", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-23", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-24", IsBlocked = true, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-25", IsBlocked = false, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-26", IsBlocked = false, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-27", IsBlocked = false, Mlos = 8, Price = 100 }, 
                new UnitAvailabilityResult() { Date = "2012-12-28", IsBlocked = false, Mlos = 8, Price = 100 }
            };
            #endregion

            actual = _unitsService.GetUnitAvailability(_testUnit.UnitID, new DateTime(2012, 11, 1), new DateTime(2012, 12, 28));

            Assert.AreEqual(expected.Count(), actual.Count());
            for (i = 0; i < expected.Count(); ++i)
                ExtendedAssert.AreEqual(expected[i], actual.ElementAt(i));
            #endregion

            #region Test 2
            #region Expected
            expected = new List<UnitAvailabilityResult>() {
                new UnitAvailabilityResult() { Date = "2012-10-01", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-02", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-03", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-04", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-05", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-06", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-07", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-08", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-09", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-10", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-11", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-12", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-13", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-14", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-15", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-16", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-17", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-18", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-19", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-20", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-21", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-22", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-23", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-24", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-25", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-26", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-27", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-28", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-29", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-30", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-10-31", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-01", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-02", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-03", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-04", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-05", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-06", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-07", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-08", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-09", IsBlocked = false, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-10", IsBlocked = true, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-11", IsBlocked = true, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-12", IsBlocked = true, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-13", IsBlocked = true, Mlos = 7, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2012-11-14", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-15", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-16", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-17", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-18", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-19", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-20", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-21", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-22", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-23", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-24", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-25", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-26", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-27", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-28", IsBlocked = true, Mlos = 7, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-29", IsBlocked = true, Mlos = 0, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-11-30", IsBlocked = true, Mlos = 0, Price = 800 }, 
                new UnitAvailabilityResult() { Date = "2012-12-01", IsBlocked = true, Mlos = 8, Price = 800 }
            };
            #endregion

            actual = _unitsService.GetUnitAvailability(_testUnit.UnitID, new DateTime(2012, 10, 1), new DateTime(2012, 12, 1));

            Assert.AreEqual(expected.Count(), actual.Count());
            for (i = 0; i < expected.Count(); ++i)
                ExtendedAssert.AreEqual(expected[i], actual.ElementAt(i));
            #endregion

            #region Test 3
            #region Expected
            expected = new List<UnitAvailabilityResult>() {
                new UnitAvailabilityResult() { Date = "2009-06-06", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-07", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-08", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-09", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-10", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-11", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-12", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-13", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-14", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-15", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-16", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-17", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-18", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-19", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-20", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-21", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-22", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-23", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-24", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-25", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-26", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-27", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-28", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-29", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-06-30", IsBlocked = false, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-01", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-02", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-03", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-04", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-05", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-06", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-07", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-08", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-09", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-10", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-11", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-12", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-13", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-14", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-15", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-16", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-17", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-18", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-19", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-20", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-21", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-22", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-23", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-24", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-25", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-26", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-27", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-28", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-29", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-30", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-07-31", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-08-01", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-08-02", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-08-03", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-08-04", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-08-05", IsBlocked = true, Mlos = 3, Price = 650 }, 
                new UnitAvailabilityResult() { Date = "2009-08-06", IsBlocked = true, Mlos = 3, Price = 650 }
            };
            #endregion

            actual = _unitsService.GetUnitAvailability(_testUnit.UnitID, new DateTime(2009, 6, 6), new DateTime(2009, 8, 6));

            Assert.AreEqual(expected.Count(), actual.Count());
            for (i = 0; i < expected.Count(); ++i)
                ExtendedAssert.AreEqual(expected[i], actual.ElementAt(i));
            #endregion
        }

        [TestMethod]
        public void UpdateUnitRatesTest()
        {
            DateTime dateStart = new DateTime(2013, 03, 01);
            DateTime dateEnd = new DateTime(2013, 03, 31);
            decimal dailyRate = 200M;

            #region Add first Unit Rate

            UnitRate unitRate = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = dailyRate,
                DateFrom = dateStart,
                DateUntil = dateEnd,
                Monday = true,
                Tuesday = true,
                Wednesday = true,
                Thursday = true,
                Friday = true,
                Saturday = true,
                Sunday = true
            };

            _unitsService.UpdateUnitRates(_testUnit.UnitID, unitRate, _testUser.UserID);

            UnitRate dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, dateStart, dateEnd).SingleOrDefault();
            Assert.IsNotNull(dbUnitRate);

            #endregion

            #region Update Rates for period with selected days

            decimal testDailyRate = 100M;
            UnitRate testUnitRate = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = testDailyRate,
                DateFrom = dateStart,
                DateUntil = dateEnd,
                Monday = true,
                Tuesday = true,
                Thursday = true,
                Friday = true
            };

            _unitsService.UpdateUnitRates(_testUnit.UnitID, testUnitRate, _testUser.UserID);

            DateTime testDateStart1 = new DateTime(2013, 03, 04);
            DateTime testDateStart2 = new DateTime(2013, 03, 07);
            DateTime testDateStart3 = new DateTime(2013, 03, 11);
            DateTime testDateStart4 = new DateTime(2013, 03, 14);
            DateTime testDateStart5 = new DateTime(2013, 03, 18);
            DateTime testDateStart6 = new DateTime(2013, 03, 21);
            DateTime testDateStart7 = new DateTime(2013, 03, 25);
            DateTime testDateStart8 = new DateTime(2013, 03, 28);
            DateTime testDateStart9 = new DateTime(2013, 03, 01);

            DateTime testDateEnd1 = new DateTime(2013, 03, 05);
            DateTime testDateEnd2 = new DateTime(2013, 03, 08);
            DateTime testDateEnd3 = new DateTime(2013, 03, 12);
            DateTime testDateEnd4 = new DateTime(2013, 03, 15);
            DateTime testDateEnd5 = new DateTime(2013, 03, 19);
            DateTime testDateEnd6 = new DateTime(2013, 03, 22);
            DateTime testDateEnd7 = new DateTime(2013, 03, 26);
            DateTime testDateEnd8 = new DateTime(2013, 03, 29);
            DateTime testDateEnd9 = new DateTime(2013, 03, 01);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart1, testDateEnd1).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate);
            Assert.IsNotNull(dbUnitRate);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart2, testDateEnd2).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate);
            Assert.IsNotNull(dbUnitRate);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart3, testDateEnd3).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate);
            Assert.IsNotNull(dbUnitRate);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart4, testDateEnd4).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate);
            Assert.IsNotNull(dbUnitRate);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart5, testDateEnd5).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate);
            Assert.IsNotNull(dbUnitRate);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart6, testDateEnd6).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate);
            Assert.IsNotNull(dbUnitRate);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart7, testDateEnd7).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate);
            Assert.IsNotNull(dbUnitRate);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart8, testDateEnd8).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate);
            Assert.IsNotNull(dbUnitRate);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart9, testDateEnd9).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate);
            Assert.IsNotNull(dbUnitRate);

            // Check whole periods in DB
            Assert.AreEqual(18, _unitsService.GetUnitRates(_testUnit.UnitID, dateStart, dateEnd).Count());

            #endregion

            #region Update Rates for small period

            decimal testDailyRate2 = 999.99M;
            DateTime testDateStart10 = new DateTime(2013, 03, 10);
            DateTime testDateEnd10 = new DateTime(2013, 03, 20);
            
            UnitRate testUnitRate2 = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = testDailyRate2,
                DateFrom = testDateStart10,
                DateUntil = testDateEnd10,
                Monday = true,
                Tuesday = true,
                Wednesday = true,
                Thursday = true,
                Friday = true,
                Saturday = true,
                Sunday = true
            };

            _unitsService.UpdateUnitRates(_testUnit.UnitID, testUnitRate2, _testUser.UserID);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart10, testDateEnd10).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate2);
            Assert.IsNotNull(dbUnitRate);
            
            // Check whole periods in DB
            Assert.AreEqual(13, _unitsService.GetUnitRates(_testUnit.UnitID, dateStart, dateEnd).Count());
            
            #endregion

            #region Update Rates for all period

            decimal testDailyRate3 = 1.11M;
            DateTime testDateStart11 = new DateTime(2013, 03, 01);
            DateTime testDateEnd11 = new DateTime(2013, 03, 31);

            UnitRate testUnitRate3 = new UnitRate()
            {
                Unit = _testUnit,
                DailyRate = testDailyRate3,
                DateFrom = testDateStart11,
                DateUntil = testDateEnd11,
                Monday = true,
                Tuesday = true,
                Wednesday = true,
                Thursday = true,
                Friday = true,
                Saturday = true,
                Sunday = true
            };

            _unitsService.UpdateUnitRates(_testUnit.UnitID, testUnitRate3, _testUser.UserID);

            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart11, testDateEnd11).SingleOrDefault();
            Assert.AreEqual(dbUnitRate.DailyRate, testDailyRate3);
            Assert.IsNotNull(dbUnitRate);

            // Check whole periods in DB
            Assert.AreEqual(1, _unitsService.GetUnitRates(_testUnit.UnitID, dateStart, dateEnd).Count());

            #endregion

            #region Delete Unit Rates

            _unitsService.DeleteUnitRate(dbUnitRate.UnitRateID);
            dbUnitRate = _unitsService.GetUnitRates(_testUnit.UnitID, testDateStart11, testDateEnd11).SingleOrDefault();
            Assert.IsNull(dbUnitRate);

            #endregion
        }

        [TestMethod]
        public void GetUnitBlockageByReservationIdTest()
        {
            _testReservation = new Reservation()
            {
                Property = _testProperty,
                Unit = _testUnit,
                UnitType = _testUnitType,
                User = _testUser,
                ConfirmationID = "abcd_1",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2, 2 },
                GuestPaymentMethod = null,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54, 1 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt_1",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture,
                UnitInvBlockings = new List<UnitInvBlocking>()
            };
            _testUnit.Reservations.Add(_testReservation);
            _reservationsService.AddReservation(_testReservation);
            _efContext.SaveChanges();

            UnitInvBlocking temp = new UnitInvBlocking()
            {
                Comment = "comment",
                DateFrom = DateTime.Now.AddDays(1),
                DateUntil = DateTime.Now.AddDays(10),
                Reservation = _testReservation,
                Unit = _testUnit

            };
            _testReservation.UnitInvBlockings.Add(temp);

            _efContext.SaveChanges();

            UnitInvBlocking actual = _unitsService.GetUnitBlockageByReservationId(_testReservation.ReservationID);

            ExtendedAssert.AreEqual(temp, actual);


        }

        [TestMethod]
        public void DeleteMlosesTest()
        {
            for (int i = 0; i < 5; i++)
            {
                UnitType unitType = new UnitType()
                {
                    UnitTypeCode = "UnitsServiceTest_UnitTypeCode",
                    Property = _testProperty
                };
                unitType.SetTitleValue("UnitsServiceTest_UnitTypeTitle");
                unitType.SetDescValue("UnitsServiceTest_UnitTypeDesc");

                _testUnitTypes.Add(unitType);
                _unitTypesService.AddUnitType(unitType);

                UnitTypeMLO mlos = new UnitTypeMLO()
                {
                    UnitType = unitType,
                    MLOS = 14,
                    DateFrom = new DateTime(2012, 12, 21),
                    DateUntil = new DateTime(2014, 12, 28)
                };
                _testMloses.Add(mlos);
                _unitTypesService.AddUnitTypeMlos(mlos);
            }
            _efContext.SaveChanges();

            _unitsService.DeleteMloses(_testMloses.Select(p => p.UnitTypeMLOSID));

            foreach (UnitTypeMLO ml in _testMloses) Assert.IsNull(_unitTypesService.GetUnitTypeMlosById(ml.UnitTypeMLOSID), Messages.ObjectIsNotNull);
        }

        [TestMethod]
        public void GetUnitsByManagerIdTest()
        {
            _testManager = new User()
            {
                Firstname = "GetUnitsByManagerIdTest_Firstname",
                Lastname = "GetUnitsByManagerIdTest_Lastname",
                email = "GetUnitsByManagerIdTest_E-mail",
                Password = "GetUnitsByManagerIdTest_Password",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture
            };

            _userService.AddUser(_testManager);

            _propertiesService.AddPropertyManager(_testManager.UserID, _testProperty.PropertyID, RoleLevel.PropertyManager);
            _efContext.SaveChanges();

            for (int i = 0; i < 2; i++)
            {
                UnitType _testUnitType = new UnitType()
                {
                    UnitTypeCode = "UnitsServiceTest_UnitTypeCode",
                    Property = _testProperty
                };
                _testUnitType.SetTitleValue("UnitsServiceTest_UnitTypeTitle");
                _testUnitType.SetDescValue("UnitsServiceTest_UnitTypeDesc");

                _unitTypesService.AddUnitType(_testUnitType);
                _testUnitTypes.Add(_testUnitType);

                Unit testUnit = new Unit()
                {
                    Property = _testProperty,
                    UnitType = _testUnitType,
                    UnitCode = "UnitsServiceTest_UnitCode"
                };
                testUnit.SetTitleValue("UnitsServiceTest_UnitTypeTitle");
                testUnit.SetDescValue("UnitsServiceTest_UnitTypeDesc");

                _unitsService.AddUnit(testUnit);
                _testUnits.Add(testUnit);

            }
            _efContext.SaveChanges();

            List<Unit> actual = _unitsService.GetUnitsByManagerId(_testManager.UserID, RoleLevel.PropertyManager).ToList();

            ExtendedAssert.AreEqual(_testUnit, actual[0]);
            ExtendedAssert.AreEqual(_testUnits[0], actual.Where(f => f.UnitID == _testUnits[0].UnitID).SingleOrDefault());
            ExtendedAssert.AreEqual(_testUnits[1], actual.Where(f => f.UnitID == _testUnits[1].UnitID).SingleOrDefault());
        }

        [TestMethod]
        public void GetUnitForPropertyByCodeTest()
        {
            // add more cases
            Unit actual = _unitsService.GetUnitForPropertyByCode(_testProperty.PropertyID, _testUnit.UnitCode);
            ExtendedAssert.AreEqual(_testUnit, actual);
        }

        [TestMethod]
        public void GetUnitsByOwnerIdTest()
        {
            // add unavaliable units and test it 
            Unit actual = _unitsService.GetUnitsByOwnerId(_testUser.UserID).First();
            ExtendedAssert.AreEqual(_testUnit, actual);
        }
      
        [TestMethod]
        public void AddAndAdjustUnitBlockadeTest()
        {
            #region OwnerStay blockade type

            UnitInvBlocking blockade1 = new UnitInvBlocking()
            {
                Comment = "AddAndAdjustUnitBlockadeTest_Comment",
                DateFrom = DateTime.Now.AddDays(10),
                DateUntil = DateTime.Now.AddDays(40),
                Reservation = null,
                Type = (int) UnitBlockingType.OwnerStay,
                Unit = _testUnit,
            };

            _unitsService.AddAndAdjustUnitBlockade(blockade1);

            ExtendedAssert.AreEqual(_testUnit.UnitInvBlockings.Where(a => a.UnitInvBlockingID == blockade1.UnitInvBlockingID).SingleOrDefault(), blockade1);

            #endregion

            #region Avaliable blockade type

            UnitInvBlocking blockade2 = new UnitInvBlocking()
            {
                Comment = "AddAndAdjustUnitBlockadeTest_Comment",
                DateFrom = DateTime.Now.AddDays(20),
                DateUntil = DateTime.Now.AddDays(30),
                Reservation = null,
                Type = (int)UnitBlockingType.Available,
                Unit = _testUnit,
            };

            _unitsService.AddAndAdjustUnitBlockade(blockade2);
            int tempId = blockade1.UnitInvBlockingID;
            Assert.AreEqual(_testUnit.UnitInvBlockings.Where(a => a.UnitInvBlockingID == (tempId + 1)).SingleOrDefault().DateFrom, blockade2.DateUntil.AddDays(1));
            Assert.AreEqual(_testUnit.UnitInvBlockings.Where(a => a.UnitInvBlockingID == (tempId + 1)).SingleOrDefault().DateUntil.ToShortDateString(), DateTime.Now.AddDays(40).ToShortDateString());
            Assert.AreEqual((int) UnitBlockingType.OwnerStay, (_testUnit.UnitInvBlockings.Where(a => a.UnitInvBlockingID == (tempId + 1)).SingleOrDefault().Type));


            #endregion
        }


        // ?! 
        //[TestMethod]
        public void UpdateMlosesTest()
        {
            for (int i = 0; i < 5; i++)
            {

                UnitTypeMLO mlos = new UnitTypeMLO()
                {
                    UnitType = _testUnitType,
                    MLOS = 14,
                    DateFrom = DateTime.Now.AddDays(i * 2),
                    DateUntil = DateTime.Now.AddDays(i * 2 + 1),
                };
                _testMloses.Add(mlos);
                _unitTypesService.AddUnitTypeMlos(mlos);
            }
            _efContext.SaveChanges();

            List<UnitTypeMLO> newMloses = new List<UnitTypeMLO>();

            for (int i = 0; i < 5; i++)
            {
                newMloses.Add(new UnitTypeMLO()
                {
                    UnitType = _testUnitType,
                    MLOS = 5,
                    DateFrom = DateTime.Now.AddDays((i + 1) * 10),
                    DateUntil = DateTime.Now.AddDays((i + 1) * 11),
                });
            }

            _unitsService.UpdateMloses(_testUnit.UnitID, newMloses);

            for (int i = 0; i < 5; i++)
            {
                ExtendedAssert.AreEqual(newMloses[i], _testMloses[i]);
            }

        }

        //[TestMethod]
        public void DeleteCTAsTest()
        {
        }

        #endregion
    }
}
