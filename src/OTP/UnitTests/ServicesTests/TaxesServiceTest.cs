﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class TaxesServiceTest
    {
        #region Fields
        /// <summary>
        /// list of taxes do delete after each test
        /// </summary>
        private List<Tax> _testTaxes = new List<Tax>();
        /// <summary>
        /// destination needed for adding new taxes
        /// </summary>
        private Destination _testDestination = null;
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext = null;
        #endregion

        #region Services

        private ITaxesService _taxesService = null;
        private IDestinationsService _destServices = null;

        #endregion

        #region Prepare/Cleanup Data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);

            _taxesService = new TaxesService(mock.Object);
            _destServices = new DestinationsService(configurationService, mock.Object);
            #endregion

            _testDestination = new Destination();
            _testDestination.SetDestinationNameValue("TestDestName", DataAccess.Enums.CultureCode.en_US);
            _testDestination.SetDestinationDescriptionValue("DestDescrition", DataAccess.Enums.CultureCode.en_US);
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (Tax t in _testTaxes)
            {
                if (_taxesService.GetTaxById(t.TaxId) != null)
                    _taxesService.DeleteTax(t.TaxId);
            }
            _testTaxes.Clear();

            if (_destServices.GetDestinationById(_testDestination.DestinationID) != null)
                _destServices.DeleteDestination(_testDestination.DestinationID);
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void AddTaxTest()
        {
            Tax expected = new Tax()
            {
                Destination = _testDestination,
                CalculatedValue = 10.0m,
                Percentage = 20.0m,
                Name_i18n = "TestTaxName_i18",
                NameCurrentLanguage = "en"
            };
            expected.SetNameValue("TestTaxName");

            _testTaxes.Add(expected);
            _taxesService.AddTax(expected);

            _efContext.SaveChanges();
            
            Tax actual = _taxesService.GetTaxById(expected.TaxId);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.CalculatedValue, actual.CalculatedValue);
            Assert.AreEqual(expected.Percentage, actual.Percentage);
            Assert.AreEqual(expected.Name_i18n, actual.Name_i18n);
            Assert.AreEqual(expected.NameCurrentLanguage, actual.NameCurrentLanguage);
        }

        [TestMethod]
        public void AddTaxRequiredFieldTest()
        {
            #region Test field Destination
            Tax tax1 = new Tax()
            {
                Destination = null,
                CalculatedValue = 10.0m,
                Percentage = 20.0m,
                NameCurrentLanguage = "en"
            };
            tax1.SetNameValue("TestTaxName");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _taxesService.AddTax(tax1);
                _efContext.SaveChanges();
                _testTaxes.Add(tax1);
            }, "FK_Taxes_OTP_Destinations");
            #endregion

            #region Test field NameCurrentLanguage
            Tax tax2 = new Tax()
            {
                Destination = _testDestination,
                CalculatedValue = 10.0m,
                Percentage = 20.0m,
                NameCurrentLanguage = null
            };
            tax2.SetNameValue("TestTaxName");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _taxesService.AddTax(tax2);
                _efContext.SaveChanges();
                _testTaxes.Add(tax2);
            }, "FK_Taxes_OTP_Destinations");
            #endregion

            #region Test field Name
            Tax tax3 = new Tax()
            {
                Destination = _testDestination,
                CalculatedValue = 10.0m,
                Percentage = 20.0m,
                NameCurrentLanguage = "en"
            };
            tax3.SetNameValue(null);

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _taxesService.AddTax(tax3);
                _efContext.SaveChanges();
                _testTaxes.Add(tax3);
            }, "FK_Taxes_OTP_Destinations");
            #endregion
        }

        [TestMethod]
        public void GetTaxesTest()
        {
            for (int i = 0; i < 5; i++)
            {
                Tax temp = new Tax()
                {
                    Destination = _testDestination,
                    CalculatedValue = 10.0m,
                    Percentage = 20.0m,
                    Name_i18n = "TestTaxName_i18",
                    NameCurrentLanguage = "en"
                };
                temp.SetNameValue("TestTaxName");

                _testTaxes.Add(temp);
                _taxesService.AddTax(temp);
            }
            _efContext.SaveChanges();

            IEnumerable<Tax> t = _taxesService.GetTaxes();

            for (int i = 0; i < 5; i++)
            {
                Assert.AreEqual(_testTaxes[i].Name, t.ElementAt(i).Name);
                Assert.AreEqual(_testTaxes[i].CalculatedValue, t.ElementAt(i).CalculatedValue);
                Assert.AreEqual(_testTaxes[i].Percentage, t.ElementAt(i).Percentage);
                Assert.AreEqual(_testTaxes[i].Name_i18n, t.ElementAt(i).Name_i18n);
                Assert.AreEqual(_testTaxes[i].NameCurrentLanguage, t.ElementAt(i).NameCurrentLanguage);
            }
        }

        [TestMethod]
        public void GetTaxesForDestinationTest()
        {
            for (int i = 0; i < 5; i++)
            {
                Tax temp = new Tax()
                {
                    Destination = _testDestination,
                    CalculatedValue = 10.0m,
                    Percentage = 20.0m,
                    Name_i18n = "TestTaxName_i18",
                    NameCurrentLanguage = "en"
                };
                temp.SetNameValue("TestTaxName");

                _testTaxes.Add(temp);
                _taxesService.AddTax(temp);
            }
            _efContext.SaveChanges();

            IEnumerable<Tax> t = _taxesService.GetTaxesForDestination(_testDestination.DestinationID);

            for (int i = 0; i < 5; i++)
            {
                Assert.AreEqual(_testTaxes[i].Name, t.ElementAt(i).Name);
                Assert.AreEqual(_testTaxes[i].CalculatedValue, t.ElementAt(i).CalculatedValue);
                Assert.AreEqual(_testTaxes[i].Percentage, t.ElementAt(i).Percentage);
                Assert.AreEqual(_testTaxes[i].Name_i18n, t.ElementAt(i).Name_i18n);
                Assert.AreEqual(_testTaxes[i].NameCurrentLanguage, t.ElementAt(i).NameCurrentLanguage);
            }
        }

        [TestMethod]
        public void DeleteTaxTest()
        {
            #region Delete succesfull
            Tax tax = new Tax()
            {
                Destination = _testDestination,
            };
            tax.SetNameValue("DeletedTestTaxName");

            _taxesService.AddTax(tax);
            _efContext.SaveChanges();

            _taxesService.DeleteTax(tax.TaxId);
            Tax t = _taxesService.GetTaxById(tax.TaxId);

            Assert.IsNull(t, Messages.ObjectIsNotNull);
            #endregion
        }
        #endregion
    }
}
