﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using DataAccess.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class TaskSchedulerEventLogServiceTest
    {
        #region Fields
        /// <summary>
        /// list of amenity groups used in test methods
        /// </summary>
        private List<TaskInstance> _testInstances = new List<TaskInstance>();
        private List<TaskInstanceDetail> _testInstanceDetails = new List<TaskInstanceDetail>();

        private ScheduledTask _testTask;
        private TaskInstance _testInstance;

        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;
        #endregion

        #region Services
        private ITaskSchedulerEventLogService _taskSchedulerEventLogService;
        private IScheduledTasksService _scheduledTasksService;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            _taskSchedulerEventLogService = new TaskSchedulerEventLogService(mock.Object);
            _scheduledTasksService = new ScheduledTasksService(mock.Object);
            #endregion

            #region Setup Objects
            _testTask = new ScheduledTask()
            {
                ScheduledTaskType = (int)ScheduledTaskType.Email,
                ScheduleType = (int)ScheduleType.Once,
                ScheduleIntervalType = (int)ScheduleIntervalType.Days,
                TimeInterval = 5,
                ScheduleTimestamp = DateTime.Now,
                LastExecutionTime = DateTime.Now,
                NextPlannedExecutionTime = DateTime.Now.AddDays(10),
                TaskCompleted = true
            };
            _scheduledTasksService.AddScheduledTask(_testTask);

            _testInstance = new TaskInstance()
            {
                ExecutionTimestamp = DateTime.Now,
                ExecutionResult = (int)TaskExecutionResult.Failure,
                ScheduledTask = _testTask
            };
            _taskSchedulerEventLogService.AddTaskInstance(_testInstance);
            _testInstances.Add(_testInstance);

            _efContext.SaveChanges();
            #endregion
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (TaskInstanceDetail tid in _testInstanceDetails)
            {
                if (_taskSchedulerEventLogService.GetTaskInstanceDetailById(tid.Id) != null)
                    _taskSchedulerEventLogService.DeleteTaskInstanceDetail(tid.Id);
            }
            _testInstanceDetails.Clear();

            foreach (TaskInstance ti in _testInstances)
            {
                if (_taskSchedulerEventLogService.GetTaskInstanceById(ti.Id) != null)
                    _taskSchedulerEventLogService.DeleteTaskInstance(ti.Id);
            }
            _testInstances.Clear();

            _scheduledTasksService.DeleteScheduledTask(_testTask.ScheduledTaskId);
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void AddTaskInstanceTest()
        {
            TaskInstance expected = new TaskInstance()
            {
                ExecutionTimestamp = DateTime.Now,
                ExecutionResult = (int)TaskExecutionResult.Failure,
                ScheduledTask = _testTask
            };
            _testInstances.Add(expected);
            _taskSchedulerEventLogService.AddTaskInstance(expected);
            _efContext.SaveChanges();

            var actual = _taskSchedulerEventLogService.GetTaskInstanceById(expected.Id);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddTaskInstanceDetailTest()
        {
            TaskInstanceDetail expected = new TaskInstanceDetail()
            {
                Category = (int)TaskDetailsMessageType.Error,
                Message = "AddTaskInstanceDetailTest_Message",
                TaskInstance = _testInstance
            };
            _testInstanceDetails.Add(expected);
            _taskSchedulerEventLogService.AddTaskInstanceDetail(expected);
            _efContext.SaveChanges();

            var actual = _taskSchedulerEventLogService.GetTaskInstanceDetailById(expected.Id);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetTaskInstanceByIdTest()
        {
            TaskInstance expected = new TaskInstance()
            {
                ExecutionTimestamp = DateTime.Now,
                ExecutionResult = (int)TaskExecutionResult.Failure,
                ScheduledTask = _testTask
            };
            _testInstances.Add(expected);
            _taskSchedulerEventLogService.AddTaskInstance(expected);
            _efContext.SaveChanges();

            var actual = _taskSchedulerEventLogService.GetTaskInstanceById(expected.Id);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetTaskInstanceDetailByIdTest()
        {
            TaskInstanceDetail expected = new TaskInstanceDetail()
            {
                Category = (int)TaskDetailsMessageType.Error,
                Message = "GetTaskInstanceDetailByIdTest_Message",
                TaskInstance = _testInstance
            };
            _testInstanceDetails.Add(expected);
            _taskSchedulerEventLogService.AddTaskInstanceDetail(expected);
            _efContext.SaveChanges();

            var actual = _taskSchedulerEventLogService.GetTaskInstanceDetailById(expected.Id);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetTaskInstanceDetailsTest()
        {
            Dictionary<int, bool> newDetailsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; i++)
            {
                TaskInstanceDetail temp = new TaskInstanceDetail()
                {
                    Category = (int)TaskDetailsMessageType.Error,
                    Message = "GetTaskInstanceDetailsTest_Message_" + i,
                    TaskInstance = _testInstance
                };
                _testInstanceDetails.Add(temp);
                _taskSchedulerEventLogService.AddTaskInstanceDetail(temp);
                _efContext.SaveChanges();

                newDetailsFound.Add(_testInstanceDetails[i].Id, false);
            }

            TaskInstanceDetail expected;
            var actual = _taskSchedulerEventLogService.GetTaskInstanceDetails();

            foreach (var actualObj in actual)
            {
                if (newDetailsFound.ContainsKey(actualObj.Id))
                {
                    expected = _testInstanceDetails.Where(a => a.Id == actualObj.Id).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newDetailsFound[actualObj.Id] = true;
                }
            }

            if (newDetailsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "task instance details", "GetTaskInstanceDetails"));
        }

        [TestMethod]
        public void GetTaskInstanceDetailsByTaskIdTest()
        {
            Dictionary<int, bool> newDetailsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; i++)
            {
                TaskInstanceDetail temp = new TaskInstanceDetail()
                {
                    Category = (int)TaskDetailsMessageType.Error,
                    Message = "GetTaskInstanceDetailsByTaskIdTest_Message_" + i,
                    TaskInstance = _testInstance
                };
                _testInstanceDetails.Add(temp);
                _taskSchedulerEventLogService.AddTaskInstanceDetail(temp);
                _efContext.SaveChanges();

                newDetailsFound.Add(_testInstanceDetails[i].Id, false);
            }

            TaskInstanceDetail expected;
            var actual = _taskSchedulerEventLogService.GetTaskInstanceDetailsByTaskId(_testInstance.ScheduledTask.ScheduledTaskId);

            foreach (var actualObj in actual)
            {
                if (newDetailsFound.ContainsKey(actualObj.Id))
                {
                    expected = _testInstanceDetails.Where(a => a.Id == actualObj.Id).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newDetailsFound[actualObj.Id] = true;
                }
            }

            if (newDetailsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "task instance details", "GetTaskInstanceDetailsByTaskId"));
        }

        [TestMethod]
        public void DeleteTaskInstance()
        {
            #region Delete successfully
            TaskInstance deleted = new TaskInstance()
            {
                ExecutionTimestamp = DateTime.Now,
                ExecutionResult = (int)TaskExecutionResult.Failure,
                ScheduledTask = _testTask
            };

            _taskSchedulerEventLogService.AddTaskInstance(deleted);
            _efContext.SaveChanges();

            _taskSchedulerEventLogService.DeleteTaskInstance(deleted.Id);

            var dbTaskInstance = _taskSchedulerEventLogService.GetTaskInstanceById(deleted.Id);

            Assert.IsNull(dbTaskInstance, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced task instance deletion
            TaskInstance referencedTaskInstance = new TaskInstance()
            {
                ExecutionTimestamp = DateTime.Now,
                ExecutionResult = (int)TaskExecutionResult.Failure,
                ScheduledTask = _testTask
            };
            _taskSchedulerEventLogService.AddTaskInstance(referencedTaskInstance);

            TaskInstanceDetail referncedTaskInstanceDetail = new TaskInstanceDetail()
            {
                Category = (int)TaskDetailsMessageType.Error,
                Message = "DeleteTaskInstance_Referenced_Message",
                TaskInstance = referencedTaskInstance
            };
            _taskSchedulerEventLogService.AddTaskInstanceDetail(referncedTaskInstanceDetail);

            _efContext.SaveChanges();

            bool exceptionCaught = false;
            try
            {
                _taskSchedulerEventLogService.DeleteTaskInstance(referencedTaskInstance.Id);
            }
            catch (Exception)
            {
                exceptionCaught = true;
            }

            _taskSchedulerEventLogService.DeleteTaskInstanceDetail(referncedTaskInstanceDetail.Id);

            if (!exceptionCaught)
                Assert.Fail(Messages.DeletionShouldNotSucceed);
            #endregion
        }

        [TestMethod]
        public void DeleteTaskInstanceDetail()
        {
            #region Delete successfully
            TaskInstanceDetail deleted = new TaskInstanceDetail()
            {
                Category = (int)TaskDetailsMessageType.Error,
                Message = "DeleteTaskInstanceDetail_Message",
                TaskInstance = _testInstance
            };

            _taskSchedulerEventLogService.AddTaskInstanceDetail(deleted);
            _efContext.SaveChanges();

            _taskSchedulerEventLogService.DeleteTaskInstanceDetail(deleted.Id);

            var dbTaskInstanceDetail = _taskSchedulerEventLogService.GetTaskInstanceDetailById(deleted.Id);

            Assert.IsNull(dbTaskInstanceDetail, Messages.ObjectIsNotNull);
            #endregion
        }
        #endregion
    }
}
