﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using BusinessLogic.EVS;
using BusinessLogic.Objects;
using BusinessLogic.PaymentGateway;
using BusinessLogic;
using BusinessLogic.Enums;
using DataAccess;
using DataAccess.Enums;
using DataAccess.CustomObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Common.Exceptions;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class BookingServiceTest
    {
        #region Fields
        /// <summary>
        /// test objects
        /// </summary>
        private List<Property> _testProperties = new List<Property>();

        private List<Unit> _testUnits = new List<Unit>();

        private List<UnitType> _testUnitTypes = new List<UnitType>();

        private List<UnitType7dayDiscounts> _testDiscounts = new List<UnitType7dayDiscounts>();

        private List<PropertyAddOn> _testAddons = new List<PropertyAddOn>();

        private List<UnitRate> _testUnitRates = new List<UnitRate>();

        private List<Reservation> _testReservations = new List<Reservation>();

        private Tax _testTax = null;
        private Destination _testDestination = null;
        private DictionaryCountry _testCountry = null;
        private DictionaryCulture _testCulture = null;
        private User _testUser = null;
        private GuestPaymentMethod _testPaymentMethod = null;
        private UnitType _unitType = null;
        private UnitInvBlocking _testUnitBlock = null;
        private UnitTypeMLO _testMlos = null;
        private SecurityDeposit _testSecurityDeposit = null;

        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext = null;
        #endregion

        #region Services
        private ISettingsService _settingsService = null;
        private IPropertiesService _propertiesService = null;
        private IPropertyAddOnsService _propertyAddOnsService = null;
        private ITaxesService _taxesService = null;
        private IUnitsService _unitService = null; 
        private IUserService _userService = null;
        private IEventLogService _logService = null;
        private IMessageService _messageService = null;
        private IPaymentGatewayService _paymentGatewayService = null;
        private IBlobService _blobService = null;
        private IDictionaryCountryService _dictCountryService = null;
        private IBookingService _bookingService = null;
        private IUnitTypesService _unitTypesService = null;
        private IDestinationsService _destinationService = null;
        private IDictionaryCultureService _dictionaryCultureService = null;
        private IReservationsService _reservationsService = null;
        private IZohoCrmService _zohoCrmService = null;
        #endregion

        #region Mocks
        Mock<IAuthorizationService> _authenticationService = null;
        Mock<IStateService> _stateService = null;
        Mock<IContextService> _contextService = null;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            _contextService = new Mock<IContextService>();
            _contextService.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            _contextService.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(_contextService.Object);

            _authenticationService = new Mock<IAuthorizationService>();

            _authenticationService.Setup(f => f.SingOutUser()).Callback(() => { });

            _destinationService = new DestinationsService(configurationService, _contextService.Object);
            _settingsService = new SettingsService(_contextService.Object);
            _blobService = new BlobService(_contextService.Object);
            _propertiesService = new PropertiesServices(configurationService, _blobService, _settingsService, _contextService.Object);
            _propertyAddOnsService = new PropertyAddOnsService(_contextService.Object);
            _taxesService = new TaxesService(_contextService.Object);
            _logService = new EventLogService(_contextService.Object);
            _messageService = new MessageService(_contextService.Object, _settingsService);
            _dictCountryService = new DictionaryCountryService(_contextService.Object);
            _userService = new UsersService(_contextService.Object, _zohoCrmService, _messageService, _settingsService);
            _stateService = new Mock<IStateService>();
            
            _unitTypesService = new UnitTypesService(_contextService.Object);
            _paymentGatewayService = new PaymentGatewayService(_contextService.Object, _logService, _settingsService, _messageService);
            _unitService = new UnitsService(_contextService.Object, _stateService.Object, _unitTypesService);
            _bookingService = new BookingService(_contextService.Object, _propertiesService, _propertyAddOnsService, _taxesService,
                                                _unitService, _userService, _stateService.Object, _logService, _settingsService,
                                                _messageService, _paymentGatewayService, _unitTypesService, _authenticationService.Object);
            _dictionaryCultureService = new DictionaryCultureService(_contextService.Object);
            IDocumentService _documentService = new PdfDocumentService(_contextService.Object, _settingsService);
            _reservationsService = new ReservationsService(configurationService, _blobService, _contextService.Object, _propertiesService, _bookingService, _paymentGatewayService, _documentService, _messageService, _settingsService, _unitService);
            #endregion

            #region Setup Data

            _testCountry = _dictCountryService.GetCountries().First();
            _testCulture = _dictionaryCultureService.GetCultures().Where(r => r.CultureCode == "en-US").SingleOrDefault();

            _testDestination = new Destination();
            _testDestination.SetDestinationNameValue("TestDestName");
            _testDestination.SetDestinationDescriptionValue("DestDescrition");
            _destinationService.AddDestination(_testDestination);

            _testTax = new Tax()
            {
                Percentage = 20.0m,
            };
            _testTax.SetNameValue("TestTaxName");
            _taxesService.AddTax(_testTax);

            _testDestination.Taxes.Add(_testTax);

            _testUser = new User()
            {
                Lastname = "BookingServiceTest_Lastname",
                Firstname = "BookingServiceTest_Firstname",
                FullName = "BookingServiceTest_Fullname",
                Address1 = "BookingServiceTest_Address1",
                Address2 = "BookingServiceTest_Address2",
                State = "BookingServiceTest_State",
                ZIPCode = "BookingServiceTest_ZIPCode",
                City = "BookingServiceTest_City",
                DictionaryCountry = _testCountry,
                CellPhone = "BookingServiceTest_CellPhone",
                LandLine = "BookingServiceTest_LandLine",
                email = "BookingServiceTest_email",
                DriverLicenseNbr = "BookingServiceTest_DriverLicenseNbr",
                PassportNbr = "BookingServiceTest_PassportNbr",
                SocialsecurityNbr = "BookingServiceTest_SocialsecurityNbr",
                DirectDepositInfo = "BookingServiceTest_DirectDepositInfo",
                SecurityQuestion = "BookingServiceTest_SecurityQuestion",
                SecurityAnswer = "BookingServiceTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "BookingServiceTest_AcceptedTCsInitials",
                Password = "BookingServiceTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = _testCulture,
                UserRoles = new List<UserRole>()
            };

            _testUser.UserRoles.Add(new UserRole()
            {
                Role = DataAccessHelper.GetRoleById(_efContext, 2), // owner
                Status = (int)UserStatus.Active,
            });

            _userService.AddUser(_testUser);

            DataAccess.CreditCardType _testCreditCardType = _bookingService.GetCreditCardTypes().First();

            _testPaymentMethod = new GuestPaymentMethod()
            {
                User = _testUser,
                CreditCardType = _testCreditCardType,
                CreditCardLast4Digits = "0005",
                ReferenceToken = "BWrtwqQban0DFhlUnINsIFBGMdP"
            };
            _efContext.GuestPaymentMethods.Add(_testPaymentMethod);

            PropertyType propertyType = new PropertyType();
            propertyType.SetNameValue("BookingServiceTest_PropertyTypeName");
            propertyType.PersistI18nValues();
                
            Property property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "BookingServiceTest_PropertyCode",
                Address1 = "BookingServiceTest_Address1",
                Address2 = "BookingServiceTest_Address2",
                State = "BookingServiceTest_State",
                ZIPCode = "BookingServiceTest_ZipCode",
                City = "BookingServiceTest_City",
                DictionaryCountry = _testCountry,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                PropertyAddOns = _testAddons,
                TimeZone = "TZ"
            };
            property.SetPropertyNameValue("BookingServiceTest_PropertyName");
            property.SetShortDescriptionValue("BookingServiceTest_ShortDescription");
            property.SetLongDescriptionValue("BookingServiceTest_LongDescription");
            property.PersistI18nValues();

            _testProperties.Add(property);
            _propertiesService.AddProperty(property);

            _testSecurityDeposit = new SecurityDeposit()
            {
                Amount = 1000,
                DictionaryCulture = property.DictionaryCulture
            };
            _efContext.SecurityDeposits.Add(_testSecurityDeposit);

           _unitType = new UnitType()
            {
                Property = property,
                UnitTypeCode = "BookingServiceTest_UnitTypeCode"
                
            };
            _unitType.SetTitleValue("BookingServiceTest_UnitTypeTitle");
            _unitType.SetDescValue("BookingServiceTest_UnitTypeDesc");
            _unitType.PersistI18nValues();
            _testUnitTypes.Add(_unitType);
            _unitTypesService.AddUnitType(_unitType);

            UnitRate rate1 = new UnitRate()
            {
                DailyRate = 500.0m,
                DateFrom = new DateTime(2012, 12, 21),
                DateUntil = new DateTime(2013, 1, 11),
            };
            _testUnitRates.Add(rate1);
            _unitService.AddUnitRate(rate1);

            UnitRate rate2 = new UnitRate()
            {
                DailyRate = 700.0m,
                DateFrom = new DateTime(2013, 1, 12),
                DateUntil = new DateTime(2013, 4, 12)
            };
            _testUnitRates.Add(rate2);
            _unitService.AddUnitRate(rate2);

            UnitType7dayDiscounts discount1 = new UnitType7dayDiscounts()
            {
                UnitType = _unitType,
                Discount = 10.0m,
                DateFrom = new DateTime(2013, 1, 1),
                DateUntil = new DateTime(2013, 1, 9),
            };
            _testDiscounts.Add(discount1);
            _unitTypesService.AddUnitType7DayDiscount(discount1);

            UnitType7dayDiscounts discount2 = new UnitType7dayDiscounts()
            {
                UnitType = _unitType,
                Discount = 20.0m,
                DateFrom = new DateTime(2013, 2, 13),
                DateUntil = new DateTime(2013, 2, 20)
            };
            _testDiscounts.Add(discount2);
            _unitTypesService.AddUnitType7DayDiscount(discount2);

            UnitTypeMLO mlos1 = new UnitTypeMLO()
            {
                UnitType = _unitType,
                MLOS = 4,
                DateFrom = new DateTime(2013, 2, 15),
                DateUntil = new DateTime(2013, 2, 20)
            };
            _testMlos = mlos1;
            _unitTypesService.AddUnitTypeMlos(mlos1);

            Unit unit = new Unit()
            {
                Property = property,
                UnitType = _unitType,
                UnitRates = _testUnitRates,
                UnitCode = "BookingServiceTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("BookingServiceTest_Title");
            unit.SetDescValue("BookingServiceTest_Desc");
            unit.PersistI18nValues();

            _unitService.AddUnit(unit);
            _testUnits.Add(unit);

            PropertyAddOn addon1 = new PropertyAddOn()
            {
                Price = 100.0m,
                Tax = _testTax,
                Unit = (int) PropertyAddOnUnit.PerDay,
                Property = _testProperties[0],
                IsMandatory = true,

            };

            addon1.SetDescValue("BookingServiceTest_AddonDescription");
            addon1.SetTitleValue("BookingServiceTest_AddonTitle");

            _propertyAddOnsService.Add(addon1);
            _testAddons.Add(addon1);

            _efContext.SaveChanges();

            #endregion
        }

        /// <summary>
        /// Cleanup the data before each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (Reservation r in _testReservations)
            {
                if (_reservationsService.GetReservationById(r.ReservationID) != null)
                {
                    if (r.UnitInvBlockings.Count > 0) _unitService.DeleteUnitBlockageByReservationId(r.ReservationID);
                    DataAccessHelper.RemoveReservation(_efContext, r);
                }
            }
            _testReservations.Clear();

            if (_testPaymentMethod != null)
                _userService.RemoveGuestPaymentMethod(_testPaymentMethod.GuestPaymentMethodID, _testUser.UserID);

            int uid = _testProperties[0].User.UserID;
            if (_propertyAddOnsService.GetById(_testAddons[0].PropertyAddOnID) != null)
                _propertyAddOnsService.DeleteAddOn(_testAddons[0].PropertyAddOnID);
            _testAddons.Clear();

            if (_taxesService.GetTaxById(_testTax.TaxId) != null)
                _taxesService.DeleteTax(_testTax.TaxId);

            foreach (UnitType7dayDiscounts d in _testDiscounts)
            {
                if (_unitTypesService.GetUnitType7DaysDiscount(d.UnitType7dayID) != null)
                    _unitTypesService.DeleteUnitType7DaysDiscount(d.UnitType7dayID);
            }
            _testDiscounts.Clear();

            if (_unitTypesService.GetUnitTypeMlosById(_testMlos.UnitTypeMLOSID) != null)
                _unitTypesService.DeleteUnitTypeMlos(_testMlos.UnitTypeMLOSID); 

            foreach (Unit u in _testUnits)
            {
                if (_unitService.GetUnitById(u.UnitID) != null)
                    _unitService.DeleteUnit(u.UnitID);
            }
            _testUnits.Clear();

            foreach (UnitType ut in _testUnitTypes)
            {
                if (_unitTypesService.GetUnitTypeById(ut.UnitTypeID) != null)
                    _unitTypesService.DeleteUnitType(ut.UnitTypeID);
            }
            _testUnitTypes.Clear();

            foreach (Property p in _testProperties)
            {
                if (_propertiesService.GetPropertyById(p.PropertyID) != null)
                    _propertiesService.RemoveProperty(p.PropertyID);
            }
            _testProperties.Clear();

            if (_destinationService.GetDestinationById(_testDestination.DestinationID) != null)
                _destinationService.DeleteDestination(_testDestination.DestinationID);

            _efContext.UserRoles.Remove(_testUser.UserRoles.First());

            foreach (Message message in _efContext.Messages)
            {
                _efContext.Messages.Remove(message);
            }

            foreach (ReservationBillingAddress billingAddress in _efContext.ReservationBillingAddresses)
            {
                _efContext.ReservationBillingAddresses.Remove(billingAddress);
            }

            _efContext.SecurityDeposits.Remove(_testSecurityDeposit);

            _userService.DeleteUserById(uid);            

        }
        #endregion

        #region Test Methods

        [TestMethod]
        public void CalculateInvoiceTest()
        {
            #region Test 1 (8 days, week of 7 day disc.)

            DateTime from = new DateTime(2013, 1, 1);
            DateTime until = new DateTime(2013, 1, 9);
            TimeSpan diff = until - from;

            PropertyDetailsInvoice expected = new PropertyDetailsInvoice()
            {
                LengthOfStay = diff.Days,
                TotalAccomodation = (decimal)(diff.Days * 500),
                Culture = _testProperties[0].DictionaryCulture.CultureInfo,
                PricePerNight = _testUnitRates[0].DailyRate,
                TaxList = new List<Tax>(),
            };
            expected.TotalSevenDaysDiscount = -(expected.TotalAccomodation * _testDiscounts[0].Discount / 100);
            expected.TotalTax = (expected.TotalAccomodation + expected.TotalSevenDaysDiscount) * _testTax.Percentage / 100;
            expected.TotalAddons = (_testAddons[0].Price * expected.LengthOfStay * _testTax.Percentage / 100);
            expected.SubtotalAccomodationAddons = expected.TotalAccomodation + expected.TotalAddons;
            expected.InvoiceTotal = expected.TotalAccomodation + expected.TotalSevenDaysDiscount + expected.TotalTax + expected.TotalAddons;


            PropertyDetailsInvoice actual = _bookingService.CalculateInvoice(_testProperties.First().PropertyID,
                                  _testUnits.First().UnitID, from, until);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(expected.Culture, actual.Culture);
            Assert.AreEqual(expected.InvoiceTotal, actual.InvoiceTotal);
            Assert.AreEqual(expected.LengthOfStay, actual.LengthOfStay);
            Assert.AreEqual(expected.PricePerNight, actual.PricePerNight);
            Assert.AreEqual(expected.SubtotalAccomodationAddons, actual.SubtotalAccomodationAddons);
            Assert.AreEqual(expected.TotalAccomodation, actual.TotalAccomodation);
            Assert.AreEqual(expected.TotalAddons, actual.TotalAddons);
            Assert.AreEqual(expected.TotalSevenDaysDiscount, actual.TotalSevenDaysDiscount);
            Assert.AreEqual(expected.TotalTax, actual.TotalTax);
            ExtendedAssert.AreEqual(_testTax, actual.TaxList.ElementAt(0));
            ExtendedAssert.AreEqual(_testAddons[0], actual.AddonsList.ElementAt(0));

            #endregion

            #region Test 2 (1 month)

            from = new DateTime(2013, 2, 12);
            until = new DateTime(2013, 3, 12);
            diff = until - from;

            expected = new PropertyDetailsInvoice()
            {
                LengthOfStay = diff.Days,
                TotalAccomodation = (decimal)(diff.Days * _testUnitRates[1].DailyRate),
                Culture = _testProperties[0].DictionaryCulture.CultureInfo,
                PricePerNight = _testUnitRates[1].DailyRate,
                TaxList = new List<Tax>(),
            };
            expected.TotalSevenDaysDiscount = -(expected.PricePerNight * ((_testDiscounts[1].DateUntil - _testDiscounts[1].DateFrom).Days + 1) * _testDiscounts[1].Discount / 100);
            expected.TotalTax = (expected.TotalAccomodation + expected.TotalSevenDaysDiscount) * _testTax.Percentage / 100;
            expected.TotalAddons = (_testAddons[0].Price * expected.LengthOfStay * _testTax.Percentage / 100);
            expected.SubtotalAccomodationAddons = expected.TotalAccomodation + expected.TotalAddons;
            expected.InvoiceTotal = expected.TotalAccomodation + expected.TotalSevenDaysDiscount + expected.TotalTax + expected.TotalAddons;


            actual = _bookingService.CalculateInvoice(_testProperties.First().PropertyID,
                                  _testUnits.First().UnitID, from, until);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(expected.Culture, actual.Culture);
            Assert.AreEqual(expected.InvoiceTotal, actual.InvoiceTotal);
            Assert.AreEqual(expected.LengthOfStay, actual.LengthOfStay);
            Assert.AreEqual(expected.PricePerNight, actual.PricePerNight);
            Assert.AreEqual(expected.SubtotalAccomodationAddons, actual.SubtotalAccomodationAddons);
            Assert.AreEqual(expected.TotalAccomodation, actual.TotalAccomodation);
            Assert.AreEqual(expected.TotalAddons, actual.TotalAddons);
            Assert.AreEqual(expected.TotalSevenDaysDiscount, actual.TotalSevenDaysDiscount);
            Assert.AreEqual(expected.TotalTax, actual.TotalTax);
            ExtendedAssert.AreEqual(_testTax, actual.TaxList.ElementAt(0));
            ExtendedAssert.AreEqual(_testAddons[0], actual.AddonsList.ElementAt(0));

            #endregion

            #region Test 3 (1 month, 2 discounts)

            UnitType7dayDiscounts discount3 = new UnitType7dayDiscounts()
            {
                UnitType = _unitType,
                Discount = 15.0m,
                DateFrom = new DateTime(2013, 2, 21),
                DateUntil = new DateTime(2013, 2, 28)
            };
            _testDiscounts.Add(discount3);
            _unitTypesService.AddUnitType7DayDiscount(discount3);
            _efContext.SaveChanges();

            from = new DateTime(2013, 2, 1);
            until = new DateTime(2013, 3, 12);
            diff = until - from;

            expected = new PropertyDetailsInvoice()
            {
                LengthOfStay = diff.Days,
                TotalAccomodation = (decimal)(diff.Days * _testUnitRates[1].DailyRate),
                Culture = _testProperties[0].DictionaryCulture.CultureInfo,
                PricePerNight = _testUnitRates[1].DailyRate,
                TaxList = new List<Tax>(),
            };
            expected.TotalSevenDaysDiscount = -((expected.PricePerNight * ((_testDiscounts[1].DateUntil - _testDiscounts[1].DateFrom).Days + 1) * _testDiscounts[1].Discount / 100) +
                (expected.PricePerNight * ((_testDiscounts[2].DateUntil - _testDiscounts[2].DateFrom).Days + 1) * _testDiscounts[2].Discount / 100));
            expected.TotalTax = (expected.TotalAccomodation + expected.TotalSevenDaysDiscount) * _testTax.Percentage / 100;
            expected.TotalAddons = (_testAddons[0].Price * expected.LengthOfStay * _testTax.Percentage / 100);
            expected.SubtotalAccomodationAddons = expected.TotalAccomodation + expected.TotalAddons;
            expected.InvoiceTotal = expected.TotalAccomodation + expected.TotalSevenDaysDiscount + expected.TotalTax + expected.TotalAddons;


            actual = _bookingService.CalculateInvoice(_testProperties.First().PropertyID,
                                  _testUnits.First().UnitID, from, until);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(expected.Culture, actual.Culture);
            Assert.AreEqual(expected.InvoiceTotal, actual.InvoiceTotal);
            Assert.AreEqual(expected.LengthOfStay, actual.LengthOfStay);
            Assert.AreEqual(expected.PricePerNight, actual.PricePerNight);
            Assert.AreEqual(expected.SubtotalAccomodationAddons, actual.SubtotalAccomodationAddons);
            Assert.AreEqual(expected.TotalAccomodation, actual.TotalAccomodation);
            Assert.AreEqual(expected.TotalAddons, actual.TotalAddons);
            Assert.AreEqual(expected.TotalSevenDaysDiscount, actual.TotalSevenDaysDiscount);
            Assert.AreEqual(expected.TotalTax, actual.TotalTax);
            ExtendedAssert.AreEqual(_testTax, actual.TaxList.ElementAt(0));
            ExtendedAssert.AreEqual(_testAddons[0], actual.AddonsList.ElementAt(0));

            #endregion
        }

        [TestMethod]
        public void PerformPaymentTest()
        {
            Reservation reservation = new Reservation()
            {
                Property = _testProperties.First(),
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = new DateTime(2013, 2, 13),
                DateDeparture = new DateTime(2013, 2, 20),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                TransactionToken = "BWrtwqQban0DFhlUnINsIFBGMdP",
                LinkAuthorizationToken = "2345",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(reservation);
            _reservationsService.AddReservation(reservation);
            _efContext.SaveChanges();

            ReservationBillingAddress rba = new ReservationBillingAddress()
            {
                City = _testUser.City,
                DictionaryCountry = _testCountry,
                Firstname = _testUser.Firstname,
                Lastname = _testUser.Lastname,
                Reservation = reservation,
                State = _testUser.State,
                Street = _testUser.Address1,
                User = _testUser,
                Zip = "12345"
            };

            PaymentResult actual = _bookingService.PerformPayment(reservation.ReservationID, _testUser.UserID, rba, _testPaymentMethod.GuestPaymentMethodID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(true, actual.PaymentGatewayResult);
        }

        [TestMethod]
        public void AddReservationAgreementTest()
        {
            Reservation reservation = new Reservation()
            {
                Property = _testProperties.First(),
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = new DateTime(2013, 2, 13),
                DateDeparture = new DateTime(2013, 2, 20),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                LinkAuthorizationToken = "",
                DictionaryCulture = _testCulture

            };
            _testReservations.Add(reservation);
            _reservationsService.AddReservation(reservation);
            _efContext.SaveChanges();

            byte[] agreement = new byte[] { 12, 3, 5, 23, 54 };

            _bookingService.AddReservationAgreement(reservation.ReservationID, agreement);

            byte[] actual = reservation.Agreement;

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(agreement, actual);
        }

        [TestMethod]
        public void AddReservationInvoiceTest()
        {
            Reservation reservation = new Reservation()
            {
                Property = _testProperties.First(),
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = new DateTime(2013, 2, 13),
                DateDeparture = new DateTime(2013, 2, 20),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                LinkAuthorizationToken = "",
                DictionaryCulture = _testCulture

            };
            _testReservations.Add(reservation);
            _reservationsService.AddReservation(reservation);
            _efContext.SaveChanges();

            byte[] invoice = new byte[] { 12, 3, 5, 23, 54 };

            _bookingService.AddReservationInvoice(reservation.ReservationID, invoice);

            byte[] actual = reservation.Invoice;

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(invoice, actual);
        }

        [TestMethod]
        public void AddPaymentTransactionTest()
        {
            Reservation reservation = new Reservation()
            {
                Property = _testProperties.First(),
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = new DateTime(2013, 2, 13),
                DateDeparture = new DateTime(2013, 2, 20),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                LinkAuthorizationToken = "2345",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(reservation);
            _reservationsService.AddReservation(reservation);
            _efContext.SaveChanges();


            _bookingService.AddPaymentTransaction(reservation, 1020.0m);

            Reservation test = _reservationsService.GetReservationById(reservation.ReservationID);

            decimal actual = test.ReservationTransactions.First().Amount;

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(1020.0m, actual);

        }

        [TestMethod]
        public void UpdateReservationGuestNumberTest()
        {
            Reservation reservation = new Reservation()
            {
                Property = _testProperties.First(),
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = new DateTime(2013, 2, 13),
                DateDeparture = new DateTime(2013, 2, 20),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                LinkAuthorizationToken = "",
                DictionaryCulture = _testCulture

            };
            _testReservations.Add(reservation);
            _reservationsService.AddReservation(reservation);
            _efContext.SaveChanges();


            _bookingService.UpdateReservationGuestNumber(10, reservation.ReservationID);

            Reservation test = _reservationsService.GetReservationById(reservation.ReservationID);

            int? actual = test.NumberOfGuests;

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(10, actual);

        }

        [TestMethod]
        public void DoTemporaryReservartionTest()
        {
            BookingInfoModel bookInfoModel = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 3, 3),
                dateTripUntil = new DateTime(2013, 3, 17),
                PropertyId = _testProperties.First().PropertyID,
                UnitId = _testUnits.First().UnitID
            };

            int tentativeReservationId = _bookingService.DoTemporaryReservartion(bookInfoModel, _testUser);

            Reservation resv = _reservationsService.GetReservationById(tentativeReservationId);
            _testReservations.Add(resv);
            _testUnitBlock = resv.UnitInvBlockings.First();

            Assert.IsNotNull(resv, Messages.ObjectIsNull);
            Assert.IsNotNull(_testUnitBlock, Messages.ObjectIsNull);
            Assert.AreEqual((int)BookingStatus.TentativeBooking, resv.BookingStatus);
            Assert.AreEqual(bookInfoModel.dateTripFrom, _testUnitBlock.DateFrom);
            Assert.AreEqual(bookInfoModel.dateTripUntil.AddDays(-1), _testUnitBlock.DateUntil);
            Assert.AreEqual((int)UnitBlockingType.Reservation, _testUnitBlock.Type);
        }

        [TestMethod]
        public void SetVerificationInfoTest()
        {
            EVSResponse response = new EVSResponse()
            {
                TransactionDetails = null,
                UserIdentityData = null,
            };

            #region Verification negative
            _bookingService.SetVerificationInfo(_testUser, false, response);

            Assert.AreEqual(false, _testUser.IdentityVerificationDetails.First().VerificationPositive);
            ExtendedAssert.AreEqual(_testUser, _testUser.IdentityVerificationDetails.First().User);
            #endregion


            #region Verification positive
            _bookingService.SetVerificationInfo(_testUser, true, response);

            Assert.AreEqual(true, _testUser.IdentityVerificationDetails.Last().VerificationPositive);
            ExtendedAssert.AreEqual(_testUser, _testUser.IdentityVerificationDetails.Last().User);
            #endregion
        }

        [TestMethod]
        public void GetUserCreditCardTypesTest()
        {
            List<GuestPaymentMethod> actual = _bookingService.GetUserCreditCardTypes(_testUser.UserID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            ExtendedAssert.AreEqual(_testPaymentMethod, actual.SingleOrDefault());
        }

        [TestMethod]
        public void GetCreditCardTypesTest()
        {
            DataAccess.CreditCardType t1 = new DataAccess.CreditCardType()
            {
                Name = "VISA",
                CreditCardTypeId = 1,
                CreditCardIcon = 1,
            };
            DataAccess.CreditCardType t2 = new DataAccess.CreditCardType()
            {
                Name = "MASTERCARD",
                CreditCardTypeId = 2,
                CreditCardIcon = 2,
            };
            DataAccess.CreditCardType t3 = new DataAccess.CreditCardType()
            {
                Name = "AMEX",
                CreditCardTypeId = 3,
                CreditCardIcon = 3,
            };

            List<DataAccess.CreditCardType> actual = _bookingService.GetCreditCardTypes();

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            ExtendedAssert.AreEqual(t1, actual[0]);
            ExtendedAssert.AreEqual(t2, actual[1]);
            ExtendedAssert.AreEqual(t3, actual[2]);
        }

        [TestMethod]
        public void RemoveGuestPaymentMethodTest()
        {
            int temp = _testPaymentMethod.GuestPaymentMethodID;
            _bookingService.RemoveGuestPaymentMethod(_testPaymentMethod.GuestPaymentMethodID);
            _efContext.SaveChanges();
            Assert.AreEqual(null, _efContext.GuestPaymentMethods.Where(g => g.GuestPaymentMethodID == temp).SingleOrDefault());
            _testPaymentMethod = null;
        }

        [TestMethod]
        public void IsUnitAvailableTest()
        {
            Reservation reservation1 = new Reservation()
            {
                Property = _testProperties.First(),
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = new DateTime(2013, 2, 13),
                DateDeparture = new DateTime(2013, 2, 20),
                TotalPrice = 2.45M,
                DictionaryCulture = _testCulture,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                UnitInvBlockings = new List<UnitInvBlocking>(),
                LinkAuthorizationToken = "d"

            };
            _testReservations.Add(reservation1);
            _reservationsService.AddReservation(reservation1);

            Reservation reservation2 = new Reservation()
            {
                Property = _testProperties.First(),
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = new DateTime(2013, 2, 13),
                DateDeparture = new DateTime(2013, 2, 20),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                UnitInvBlockings = new List<UnitInvBlocking>(),
                LinkAuthorizationToken = "d",
                DictionaryCulture = _testCulture

            };
            _testReservations.Add(reservation2);
            _reservationsService.AddReservation(reservation2);

            _efContext.SaveChanges();

            UnitInvBlocking unitBlocking = new UnitInvBlocking()
            {
                DateFrom = new DateTime(2013, 2, 13),
                DateUntil = new DateTime(2013, 2, 20),
                Type = (int)UnitBlockingType.Reservation,
                Unit = _testUnits.First(),
                Reservation = reservation2
            };

            reservation2.UnitInvBlockings.Add(unitBlocking);
            _unitService.AddUnitBlockade(unitBlocking);

            _efContext.SaveChanges();

            #region Unit unavaliable (already booked)
            BookingInfoModel bookInfoModel = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 2, 13),
                dateTripUntil = new DateTime(2013, 2, 20),
                PropertyId = _testProperties.First().PropertyID,
                UnitId = _testUnits.First().UnitID
            };

            bool actual = _bookingService.IsUnitAvailable(bookInfoModel, reservation1.ReservationID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(false, actual);
            #endregion

            #region Unit avaliable (with reservation)
            bookInfoModel = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 3, 4),
                dateTripUntil = new DateTime(2013, 3, 14),
                PropertyId = _testProperties.First().PropertyID,
                UnitId = _testUnits.First().UnitID
            };

            actual = _bookingService.IsUnitAvailable(bookInfoModel, reservation1.ReservationID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(true, actual);
            #endregion

            #region Unit available (no blocking on unit)
            bookInfoModel = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 2, 13),
                dateTripUntil = new DateTime(2013, 2, 20),
                PropertyId = _testProperties.First().PropertyID,
                UnitId = _testUnits.First().UnitID
            };

            actual = _bookingService.IsUnitAvailable(bookInfoModel, reservation2.ReservationID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(true, actual);
            #endregion

            #region Unit unavaliable (to short reservation period < MLOS)
            bookInfoModel = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 2, 15),
                dateTripUntil = new DateTime(2013, 2, 17),
                PropertyId = _testProperties.First().PropertyID,
                UnitId = _testUnits.First().UnitID
            };

            actual = _bookingService.IsUnitAvailable(bookInfoModel, reservation2.ReservationID);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(false, actual);
            #endregion

            #region Unit avaliable (without reservation)
            bookInfoModel = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 2, 4),
                dateTripUntil = new DateTime(2013, 2, 10),
                PropertyId = _testProperties.First().PropertyID,
                UnitId = _testUnits.First().UnitID
            };

            actual = _bookingService.IsUnitAvailable(bookInfoModel);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(true, actual);
            #endregion

            #region Unit unavaliable (without reservation)
            bookInfoModel = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 2, 14),
                dateTripUntil = new DateTime(2013, 2, 19),
                PropertyId = _testProperties.First().PropertyID,
                UnitId = _testUnits.First().UnitID
            };

            actual = _bookingService.IsUnitAvailable(bookInfoModel);

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(false, actual);
            #endregion
        }

        [TestMethod]
        public void BlockUserAfterWrongIDVerificationTest()
        {
            _bookingService.BlockUserAfterWrongIDVerification(_testUser);

            Assert.AreEqual((int)UserStatus.Blocked, _testUser.UserRoles.First().Status);
        }


        [TestMethod]
        public void CheckRatesForPeriodTest()
        {
            bool result = true;

            #region Test1
            //Arrange
            

            // Act
            result = _bookingService.CheckUnitRatesForPeriod(_testUnits.ElementAt(0).UnitID, DateTime.Parse("2013-12-12"), DateTime.Parse("2013-12-30"));

            // Assert
            Assert.AreEqual(false, result);

            #endregion Test1

            #region Test2
            //Arrange
            UnitRate rate = _testUnitRates.ElementAt(0);

            // Act            
            result = _bookingService.CheckUnitRatesForPeriod(_testUnits.ElementAt(0).UnitID, rate.DateFrom, rate.DateUntil);

            // Assert
            Assert.AreEqual(true, result);

            #endregion Test2
        }

        [TestCategory("SecurityDeposit")]
        [TestMethod]
        public void CheckAquireSecurityDepositForReservation()
        {
            // Arange
            Reservation reservation1 = new Reservation()
            {
                Property = _testProperties.First(),
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = new DateTime(2013, 2, 13),
                DateDeparture = new DateTime(2013, 2, 20),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                UnitInvBlockings = new List<UnitInvBlocking>(),
                LinkAuthorizationToken = "d",
                DictionaryCulture = _testCulture

            };
            _testReservations.Add(reservation1);
            _reservationsService.AddReservation(reservation1);
            _efContext.SaveChanges();

            // Act
            _bookingService.CalculateSecurityDeposit(reservation1);
            TransactionResponse response = _bookingService.AquireSecurityDeposit(reservation1);

            // Assert
            decimal expectedSecurityDepositValue = _efContext.SecurityDeposits.Where(d => d.DictionaryCulture.CultureId.Equals(reservation1.Property.DictionaryCulture.CultureId)).SingleOrDefault().Amount;
            decimal expectedSecurityDepositValueCents = expectedSecurityDepositValue * 100;
            Assert.AreEqual(expectedSecurityDepositValueCents.ToString(), response.Amount);
            Assert.AreEqual(expectedSecurityDepositValue, reservation1.SecurityDeposit.Value);
            Assert.AreEqual(response.Token, reservation1.SecurityDepositToken);

        }

        [TestCategory("SecurityDeposit")]
        [TestMethod]
        public void CheckAquireSecurityDepositForReservationPropertyOverride()
        {
            // Arange
            Property property = _testProperties.First();
            property.SecurityDeposit = 250;

            Reservation reservation1 = new Reservation()
            {
                Property = property,
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = new DateTime(2013, 2, 13),
                DateDeparture = new DateTime(2013, 2, 20),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                UnitInvBlockings = new List<UnitInvBlocking>(),
                LinkAuthorizationToken = "d",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(reservation1);
            _reservationsService.AddReservation(reservation1);
            _efContext.SaveChanges();

            // Act
            _bookingService.CalculateSecurityDeposit(reservation1);
            TransactionResponse response = _bookingService.AquireSecurityDeposit(reservation1);

            // Assert
            decimal expectedSecurityDepositValue = property.SecurityDeposit.Value;
            decimal expectedSecurityDepositValueCents = expectedSecurityDepositValue * 100;            
            Assert.AreEqual(expectedSecurityDepositValueCents.ToString(), response.Amount);
            Assert.AreEqual(expectedSecurityDepositValue, reservation1.SecurityDeposit.Value);
            Assert.AreEqual(response.Token, reservation1.SecurityDepositToken);

        }

        [TestCategory("SecurityDeposit")]
        [TestMethod]
        public void CheckIfSecurityDepositIsTakenOnReservation()
        {
            // Arange
            Property property = _testProperties.First();
            property.SecurityDeposit = 250;

            Mock<IPaymentGatewayService> _paymentGatewayMock = new Mock<IPaymentGatewayService>();
            _paymentGatewayMock.Setup(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = true,
                Amount = (property.SecurityDeposit.Value * 100).ToString(),
                Token = "TOKEN"
            });
            _paymentGatewayMock.Setup(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => !t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = true
            });            

            _bookingService = new BookingService(_contextService.Object, _propertiesService, _propertyAddOnsService, _taxesService,
                                    _unitService, _userService, _stateService.Object, _logService, _settingsService,
                                    _messageService, _paymentGatewayMock.Object, _unitTypesService, _authenticationService.Object);


            

            Reservation reservation1 = new Reservation()
            {
                Property = property,
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now.AddDays(20),
                DateDeparture = DateTime.Now.AddDays(30),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                UnitInvBlockings = new List<UnitInvBlocking>(),
                LinkAuthorizationToken = "d",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(reservation1);
            _reservationsService.AddReservation(reservation1);
            _efContext.SaveChanges();

            // Act
            PaymentResult paymentResult = _bookingService.PerformPayment(reservation1.ReservationID, _testUser.UserID, new ReservationBillingAddress(), _testPaymentMethod.GuestPaymentMethodID);

            // Assert
            Assert.AreEqual((int)BookingStatus.ReceivedBooking, reservation1.BookingStatus);
            Assert.AreEqual(true, reservation1.TakeSecurityDeposit.Value);
            Assert.AreEqual(property.SecurityDeposit.Value, reservation1.SecurityDeposit.Value);
            Assert.IsNotNull(reservation1.SecurityDepositToken);

            _paymentGatewayMock.Verify(f => f.AuthorizePayment(It.IsAny<TransactionRequest>(), It.IsAny<int>()), Times.Exactly(2)); // security deposit and invoice            
            _paymentGatewayMock.Verify(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>()), Times.Once());
        }

        /// <summary>
        /// In this scenario security deposit shouldn't be taken ( it is 30 days to travel so security deposit should be taken tommorow )
        /// </summary>
        [TestCategory("SecurityDeposit")]
        [TestMethod]
        public void CheckIfSecurityDepositIsTakenOnReservation30Days()
        {
            // Arange
            Property property = _testProperties.First();
            property.SecurityDeposit = 250;

            Mock<IPaymentGatewayService> _paymentGatewayMock = new Mock<IPaymentGatewayService>();
            _paymentGatewayMock.Setup(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = true,
                Amount = (property.SecurityDeposit.Value * 100).ToString()
            });
            _paymentGatewayMock.Setup(f => f.AuthorizePayment(It.IsAny<TransactionRequest>(), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = true
            });

            _bookingService = new BookingService(_contextService.Object, _propertiesService, _propertyAddOnsService, _taxesService,
                                    _unitService, _userService, _stateService.Object, _logService, _settingsService,
                                    _messageService, _paymentGatewayMock.Object, _unitTypesService, _authenticationService.Object);




            Reservation reservation1 = new Reservation()
            {
                Property = property,
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now.AddDays(30),
                DateDeparture = DateTime.Now.AddDays(40),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                UnitInvBlockings = new List<UnitInvBlocking>(),
                LinkAuthorizationToken = "d",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(reservation1);
            _reservationsService.AddReservation(reservation1);
            _efContext.SaveChanges();

            // Act
            PaymentResult paymentResult = _bookingService.PerformPayment(reservation1.ReservationID, _testUser.UserID, new ReservationBillingAddress(), _testPaymentMethod.GuestPaymentMethodID);

            // Assert
            Assert.AreEqual((int)BookingStatus.ReceivedBooking, reservation1.BookingStatus);
            Assert.AreEqual(true, reservation1.TakeSecurityDeposit.Value);
            Assert.AreEqual(property.SecurityDeposit.Value, reservation1.SecurityDeposit.Value);
            Assert.IsNull(reservation1.SecurityDepositToken);

            _paymentGatewayMock.Verify(f => f.AuthorizePayment(It.IsAny<TransactionRequest>(), It.IsAny<int>()), Times.Exactly(2)); // invoice and security deposit            
            _paymentGatewayMock.Verify(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>()), Times.Once());
        }

        /// <summary>
        /// Validates if correct status of the reservation is set on exception when security deposit was taken and invoice amount cannot be authorized
        /// </summary>
        [ExpectedException(typeof(PaymentGatewayException))]
        [TestCategory("SecurityDeposit")]
        [TestMethod]
        public void CheckIfSecurityDepositIsTakenOnPaymentWhenInvoiceAmountNoFundOnException()
        {
            // Arange
            Property property = _testProperties.First();
            property.SecurityDeposit = 250;

            Mock<IPaymentGatewayService> _paymentGatewayMock = new Mock<IPaymentGatewayService>();
            
            _paymentGatewayMock.Setup(f => f.AuthorizePayment(It.Is<TransactionRequest>(t=>!t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = false
            });  

            _paymentGatewayMock.Setup(f => f.AuthorizePayment(It.Is<TransactionRequest>(t=>t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = true,
                Amount = (property.SecurityDeposit.Value * 100).ToString(),
                Token = "TOKEN"
            });          

            _bookingService = new BookingService(_contextService.Object, _propertiesService, _propertyAddOnsService, _taxesService,
                                    _unitService, _userService, _stateService.Object, _logService, _settingsService,
                                    _messageService, _paymentGatewayMock.Object, _unitTypesService, _authenticationService.Object);


            

            Reservation reservation1 = new Reservation()
            {
                Property = property,
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now.AddDays(20),
                DateDeparture = DateTime.Now.AddDays(30),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                UnitInvBlockings = new List<UnitInvBlocking>(),
                LinkAuthorizationToken = "d",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(reservation1);
            _reservationsService.AddReservation(reservation1);
            _efContext.SaveChanges();

            // Act
            PaymentResult paymentResult = _bookingService.PerformPayment(reservation1.ReservationID, _testUser.UserID, new ReservationBillingAddress(), _testPaymentMethod.GuestPaymentMethodID);            
            
            // Assert
            _paymentGatewayMock.Verify(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>()), Times.Once());
            Assert.AreEqual((int)BookingStatus.ReceivedBooking, reservation1.BookingStatus);
            Assert.AreEqual(true, reservation1.TakeSecurityDeposit.Value);
            Assert.AreEqual(property.SecurityDeposit.Value, reservation1.SecurityDeposit.Value);

        }

        /// <summary>
        /// Validates if correct status of the reservation is set when amount for security deposit cannot be aquired
        /// </summary>
        [TestCategory("SecurityDeposit")]
        [TestMethod]
        public void CheckIfBookingIsCompletedOnSecurityDepositAquireFail()
        {
            // Arange
            Property property = _testProperties.First();
            property.SecurityDeposit = 250;

            Mock<IPaymentGatewayService> _paymentGatewayMock = new Mock<IPaymentGatewayService>();

            _paymentGatewayMock.Setup(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => !t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = true
            });

            _paymentGatewayMock.Setup(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = false,
                Amount = (property.SecurityDeposit.Value * 100).ToString(),
                Token = "TOKEN"
            });

            _bookingService = new BookingService(_contextService.Object, _propertiesService, _propertyAddOnsService, _taxesService,
                                    _unitService, _userService, _stateService.Object, _logService, _settingsService,
                                    _messageService, _paymentGatewayMock.Object, _unitTypesService, _authenticationService.Object);




            Reservation reservation1 = new Reservation()
            {
                Property = property,
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now.AddDays(20),
                DateDeparture = DateTime.Now.AddDays(30),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                UnitInvBlockings = new List<UnitInvBlocking>(),
                LinkAuthorizationToken = "d",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(reservation1);
            _reservationsService.AddReservation(reservation1);
            _efContext.SaveChanges();

            // Act
            PaymentResult paymentResult = _bookingService.PerformPayment(reservation1.ReservationID, _testUser.UserID, new ReservationBillingAddress(), _testPaymentMethod.GuestPaymentMethodID);

            // Assert
            _paymentGatewayMock.Verify(f => f.AuthorizePayment(It.IsAny<TransactionRequest>(), It.IsAny<int>()), Times.Exactly(2)); // invoice and security deposit            
            _paymentGatewayMock.Verify(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>()), Times.Once());
            Assert.AreEqual((int)BookingStatus.ReceivedBooking, reservation1.BookingStatus);
            Assert.AreEqual(true, reservation1.TakeSecurityDeposit.Value);
            Assert.AreEqual(property.SecurityDeposit.Value, reservation1.SecurityDeposit.Value);
            Assert.IsNull(reservation1.SecurityDepositToken);
            Assert.AreEqual(0, _efContext.Messages.Where(m => m.Type.Equals((int)TemplateType.ReservationNoFunds)).Count());            

        }

        /// <summary>
        /// Validates if correct status of the reservation is set when amount for security deposit cannot be aquired
        /// </summary>
        [TestCategory("SecurityDeposit")]
        [TestMethod]
        public void CheckIfEmailIsSendToSupportOnSecurityDepositAquireFail()
        {
            // Arange
            Property property = _testProperties.First();
            property.SecurityDeposit = 250;

            Mock<IPaymentGatewayService> _paymentGatewayMock = new Mock<IPaymentGatewayService>();

            _paymentGatewayMock.Setup(f => f.Purchase(It.Is<TransactionRequest>(t => !t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = true,
                Token = "TOKEN_PAYMENT",
            });

            _paymentGatewayMock.Setup(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>())).Returns(new TransactionResponse()
            {
                Succeded = false,
                Amount = (property.SecurityDeposit.Value * 100).ToString(),
                Token = "TOKEN"
            });

            _bookingService = new BookingService(_contextService.Object, _propertiesService, _propertyAddOnsService, _taxesService,
                                    _unitService, _userService, _stateService.Object, _logService, _settingsService,
                                    _messageService, _paymentGatewayMock.Object, _unitTypesService, _authenticationService.Object);




            Reservation reservation1 = new Reservation()
            {
                Property = property,
                Unit = _testUnits.First(),
                UnitType = _testUnitTypes.First(),
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now.AddDays(10),
                TotalPrice = 2.45M,
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                ReservationTransactions = new List<ReservationTransaction>(),
                UnitInvBlockings = new List<UnitInvBlocking>(),
                LinkAuthorizationToken = "d",
                DictionaryCulture = _testCulture
            };
            _testReservations.Add(reservation1);
            _reservationsService.AddReservation(reservation1);
            _efContext.SaveChanges();

            // Act
            PaymentResult paymentResult = _bookingService.PerformPayment(reservation1.ReservationID, _testUser.UserID, new ReservationBillingAddress()
            {
                City = "test",
                Firstname = "test",
                Lastname = "test",
                State = "test",
                Street = "test",
                User = _testUser,
                Zip = "test",
                DictionaryCountry = property.DictionaryCountry
                
            }, _testPaymentMethod.GuestPaymentMethodID);

            // Assert
            _paymentGatewayMock.Verify(f => f.Purchase(It.IsAny<TransactionRequest>(), It.IsAny<int>()), Times.Exactly(1)); // invoice      
            _paymentGatewayMock.Verify(f => f.AuthorizePayment(It.Is<TransactionRequest>(t => t.Amount.Equals((property.SecurityDeposit.Value * 100).ToString())), It.IsAny<int>()), Times.Once());
            Assert.AreEqual((int)BookingStatus.FinalizedBooking, reservation1.BookingStatus);
            Assert.AreEqual(true, reservation1.TakeSecurityDeposit.Value);
            Assert.AreEqual(property.SecurityDeposit.Value, reservation1.SecurityDeposit.Value);
            Assert.IsNull(reservation1.SecurityDepositToken);
            Assert.AreEqual(1, _efContext.Messages.Where(m=>m.Type.Equals((int)TemplateType.ReservationNoFunds)).Count());            
        }

        #endregion
    }

}
