﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using BusinessLogic.ServiceContracts;
using BusinessLogic.Objects.StaticContentRepresentation;
using BusinessLogic.Objects;
using DataAccess;
using DataAccess.Enums;
using DataAccess.CustomObjects;
using BusinessLogic.Services;
using Moq;
using System.Configuration;
using Common.Serialization;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class PropertiesServiceTest
    {
        #region Fields
        /// <summary>
        /// list of properties used in test methods
        /// </summary>
        private List<Property> _testProperties = new List<Property>();
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;

        private string blobContainter;

        /// <summary>
        /// test objects used in methods
        /// </summary>
        private User _testUser;
        private Destination _testDestination;
        private DictionaryCountry _testCountry;
        private PropertyType _testPropertyType;
        private Property _testProperty, _testProperty2;
        private DictionaryCulture _testCulture;
        private ContentUpdateRequest _testRequest, _testRequest2;
        private List<User> _testManagers = new List<User>();
        #endregion

        #region Services
        private IAmenityGroupsService _amenityGroupsService;
        private IAmenityService _amenitiesService;
        private IPropertiesService _propertiesService;
        private IDestinationsService _destinationService;
        private IDictionaryCountryService _countryService;
        private IDocumentService _documentService;
        private IPropertyTypesService _propertyTypeService;
        private ITagsService _tagsService;
        private ITagGroupsService _tagGroupsService;
        private IUnitTypesService _unitTypesService;
        private IUnitsService _unitsService;
        private IUserService _userService;
        private Mock<IConfigurationService> _configurationService;
        private IBlobService _blobService;
        private IAuthorizationService _authorizationService;
        private IBookingService _bookingService;
        private IEventLogService _eventLogService;
        private IMessageService _messageService;
        private IPaymentGatewayService _paymentGatewayService;
        private IPropertyAddOnsService _propertyAddOnsService;
        private IReservationsService _reservationsService;
        private ISettingsService _settingsService;
        private IStateService _stateService;
        private ITaxesService _taxesService;
        private IUserService _usersService;
        private IDictionaryCultureService _dictionaryCultureService;
        private IZohoCrmService _zohoCrmService = null;
        private ICrmOperationLogsService _crmOperationLogsService = null;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            blobContainter = "StorageBlobUrlTest/";

            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            _configurationService = new Mock<IConfigurationService>();
            _configurationService.Setup(f => f.IsCloudEnvironmentAvailable()).Returns(false);
            _configurationService.Setup(f => f.GetKeyValue(BusinessLogic.Enums.CloudConfigurationKeys.StorageBlobUrl)).Returns(blobContainter);

            _settingsService = new SettingsService(mock.Object);
            _blobService = new BlobService(mock.Object);
            _amenitiesService = new AmenityService(mock.Object);
            _amenityGroupsService = new AmenityGroupsService(mock.Object);
            _propertiesService = new PropertiesServices(_configurationService.Object, _blobService, _settingsService, mock.Object);
            _destinationService = new DestinationsService(_configurationService.Object, mock.Object);
            _propertyTypeService = new PropertyTypeService(mock.Object);
            _countryService = new DictionaryCountryService(mock.Object);
            _unitTypesService = new UnitTypesService(mock.Object);
            _tagsService = new TagsService(mock.Object);
            _tagGroupsService = new TagGroupsService(mock.Object);
            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _userService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
            _eventLogService = new EventLogService(mock.Object);
            _messageService = new MessageService(mock.Object, _settingsService);
            _propertyAddOnsService = new PropertyAddOnsService(mock.Object);
            _settingsService = new SettingsService(mock.Object);
            _stateService = new StateService(mock.Object);
            _taxesService = new TaxesService(mock.Object);
            _unitsService = new UnitsService(mock.Object, _stateService, _unitTypesService);
            _configurationService = new Mock<IConfigurationService>();
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);
            _documentService = new PdfDocumentService(mock.Object, _settingsService);

            _paymentGatewayService = new PaymentGatewayService(mock.Object, _eventLogService, _settingsService, _messageService);
            _authorizationService = new AuthorizationService(_usersService, mock.Object);
            _bookingService = new BookingService(mock.Object, _propertiesService, _propertyAddOnsService, _taxesService, _unitsService, _usersService, _stateService, _eventLogService, _settingsService, _messageService, _paymentGatewayService, _unitTypesService, _authorizationService);

            IConfigurationService configurationService = new ConfigurationService(mock.Object);
            _reservationsService = new ReservationsService(configurationService, _blobService, mock.Object, _propertiesService, _bookingService, _paymentGatewayService, _documentService, _messageService, _settingsService, _unitsService);

            #endregion

            _testCountry = _countryService.GetCountries().First();
            _testDestination = _destinationService.GetDestinations().First();
            _testPropertyType = _propertyTypeService.GetPropertyTypes().First();
            _testCulture = _dictionaryCultureService.GetCultures().First();

            _testUser = new User()
            {
                Firstname = "PropertiesServiceTest_Firstname",
                Lastname = "PropertiesServiceTest_Lastname",
                email = "PropertiesServiceTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture
            };
            _userService.AddUser(_testUser);
            _efContext.SaveChanges();

            _testProperty = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "PropertiesServiceTest_PropertyCode",
                Address1 = "PropertiesServiceTest_Address1",
                State = "PropertiesServiceTest_State",
                ZIPCode = "PropertiesServiceTest_ZipCode",
                City = "PropertiesServiceTest_City",
                DictionaryCountry = _testCountry,
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            _testProperty.SetPropertyNameValue("PropertiesServiceTest_PropertyName");
            _testProperty.SetShortDescriptionValue("PropertiesServiceTest_ShortDescription");
            _testProperty.SetLongDescriptionValue("PropertiesServiceTest_LongDescription");

            _propertiesService.AddProperty(_testProperty);


            _testProperty2 = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "PropertiesServiceTest_PropertyCode_2",
                Address1 = "PropertiesServiceTest_Address1_2",
                State = "PropertiesServiceTest_State_2",
                ZIPCode = "PropertiesServiceTest_ZipCode_2",
                City = "PropertiesServiceTest_City_2",
                DictionaryCountry = _testCountry,
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            _testProperty2.SetPropertyNameValue("PropertiesServiceTest_PropertyName_2");
            _testProperty2.SetShortDescriptionValue("PropertiesServiceTest_ShortDescription_2");
            _testProperty2.SetLongDescriptionValue("PropertiesServiceTest_LongDescription_2");

            _propertiesService.AddProperty(_testProperty2);


            _testRequest = new ContentUpdateRequest()
            {
                Property = _testProperty,
                Description = "PropertiesServiceTest_Description",
                RequestDateTime = DateTime.Now
            };
            _propertiesService.AddContentUpdateRequest(_testRequest);
            _testRequest2 = new ContentUpdateRequest()
            {
                Property = _testProperty2,
                Description = "PropertiesServiceTest_Description_2",
                RequestDateTime = DateTime.Now
            };
            _propertiesService.AddContentUpdateRequest(_testRequest2);


            _efContext.SaveChanges();
            _testProperties.Add(_testProperty2);
            _testProperties.Add(_testProperty);
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (Property p in _testProperties)
            {
                if (_propertiesService.GetPropertyById(p.PropertyID) != null)
                    _propertiesService.RemoveProperty(p.PropertyID);
            }

            _testProperties.Clear();

            foreach (User u in _testManagers)
            {
                if (_userService.GetUserByUserId(u.UserID) != null)
                    _userService.DeleteUserById(u.UserID);
            }
            _testManagers.Clear();

            _userService.DeleteUserById(_testUser.UserID);
        }
        #endregion

        #region Test methods

        #region Property CRUD

        [TestMethod]
        public void GetPropertyByIdTest()
        {
            Property expected = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "GetPropertyByIdTest_PropertyCode",
                Address1 = "GetPropertyByIdTest_Address1",
                Address2 = "GetPropertyByIdTest_Address2",
                State = "GetPropertyByIdTest_State",
                ZIPCode = "GetPropertyByIdTest_ZipCode",
                City = "GetPropertyByIdTest_City",
                DictionaryCountry = _testCountry,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            expected.SetPropertyNameValue("GetPropertyByIdTest_PropertyName");
            expected.SetShortDescriptionValue("GetPropertyByIdTest_ShortDescription");
            expected.SetLongDescriptionValue("GetPropertyByIdTest_LongDescription");

            _propertiesService.AddProperty(expected);
            _efContext.SaveChanges();
            _testProperties.Add(expected);

            var actual = _propertiesService.GetPropertyById(expected.PropertyID);

            Assert.IsNotNull(actual);
        }

        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void GetPropertyByIdOnlyActiveTest()
        {
            Property expected = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "GetPropertyByIdTest_PropertyCode",
                Address1 = "GetPropertyByIdTest_Address1",
                Address2 = "GetPropertyByIdTest_Address2",
                State = "GetPropertyByIdTest_State",
                ZIPCode = "GetPropertyByIdTest_ZipCode",
                City = "GetPropertyByIdTest_City",
                DictionaryCountry = _testCountry,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = false,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            expected.SetPropertyNameValue("GetPropertyByIdTest_PropertyName");
            expected.SetShortDescriptionValue("GetPropertyByIdTest_ShortDescription");
            expected.SetLongDescriptionValue("GetPropertyByIdTest_LongDescription");

            _propertiesService.AddProperty(expected);
            _efContext.SaveChanges();
            _testProperties.Add(expected);

            var actual = _propertiesService.GetPropertyById(expected.PropertyID, true);
        }

        [TestMethod]
        public void GetPropertyBySignOffCode()
        {
            // Arrange
            var signOffToken = "TEST";

            Property expected = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "GetPropertyWithNumberOfUnitsByIdWithTest_PropertyCode",
                Address1 = "GetPropertyWithNumberOfUnitsByIdWithTest_Address1",
                Address2 = "GetPropertyWithNumberOfUnitsByIdWithTest_Address2",
                State = "GetPropertyWithNumberOfUnitsByIdWithTest_State",
                ZIPCode = "GetPropertyWithNumberOfUnitsByIdWithTest_ZipCode",
                City = "GetPropertyWithNumberOfUnitsByIdWithTest_City",
                DictionaryCountry = _testCountry,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time",
                SignOffCode = signOffToken
            };
            expected.SetPropertyNameValue("GetPropertyWithNumberOfUnitsByIdWithTest_PropertyName");
            expected.SetShortDescriptionValue("GetPropertyWithNumberOfUnitsByIdWithTest_ShortDescription");
            expected.SetLongDescriptionValue("GetPropertyWithNumberOfUnitsByIdWithTest_LongDescription");

            _propertiesService.AddProperty(expected);
            _efContext.SaveChanges();
            _testProperties.Add(expected);


            // Act
            var actual = _propertiesService.GetPropertyBySingOffToken(signOffToken);

            // Assert
            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetPropertyWithNumberOfUnitsByIdWithTest()
        {
            Property expected = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "GetPropertyWithNumberOfUnitsByIdWithTest_PropertyCode",
                Address1 = "GetPropertyWithNumberOfUnitsByIdWithTest_Address1",
                Address2 = "GetPropertyWithNumberOfUnitsByIdWithTest_Address2",
                State = "GetPropertyWithNumberOfUnitsByIdWithTest_State",
                ZIPCode = "GetPropertyWithNumberOfUnitsByIdWithTest_ZipCode",
                City = "GetPropertyWithNumberOfUnitsByIdWithTest_City",
                DictionaryCountry = _testCountry,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            expected.SetPropertyNameValue("GetPropertyWithNumberOfUnitsByIdWithTest_PropertyName");
            expected.SetShortDescriptionValue("GetPropertyWithNumberOfUnitsByIdWithTest_ShortDescription");
            expected.SetLongDescriptionValue("GetPropertyWithNumberOfUnitsByIdWithTest_LongDescription");

            _propertiesService.AddProperty(expected);
            _efContext.SaveChanges();
            _testProperties.Add(expected);

            UnitType unitType = new UnitType()
            {
                Property = expected,
                UnitTypeCode = "GetPropertyWithNumberOfUnitsByIdWithTest_UnitTypeCode"
            };
            unitType.SetTitleValue("GetPropertyWithNumberOfUnitsByIdWithTest_UnitTypeTitle");
            unitType.SetDescValue("GetPropertyWithNumberOfUnitsByIdWithTest_UnitTypeDesc");

            _unitTypesService.AddUnitType(unitType);
            _efContext.SaveChanges();

            Unit[] units = new Unit[5];

            for (int i = 0; i < 5; ++i)
            {
                units[i] = new Unit()
                {
                    Property = expected,
                    UnitType = unitType,
                    UnitCode = "GetPropertyWithNumberOfUnitsByIdWithTest_UnitCode_" + i,
                };
                units[i].SetTitleValue("GetPropertyWithNumberOfUnitsByIdWithTest_UnitTitle_" + i);
                units[i].SetDescValue("GetPropertyWithNumberOfUnitsByIdWithTest_UnitDesc_" + i);

                _unitsService.AddUnit(units[i]);
                _efContext.SaveChanges();
            }

            int numberOfUnits;
            var actual = _propertiesService.GetPropertyById(expected.PropertyID, out numberOfUnits);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);

            foreach (Unit u in units)
                _unitsService.DeleteUnit(u.UnitID);

            _unitTypesService.DeleteUnitType(unitType.UnitTypeID);
        }

        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void GetPropertyWithNumberOfUnitsByIdOnlyActiveTest()
        {
            Property expected = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "GetPropertyWithNumberOfUnitsByIdWithTest_PropertyCode",
                Address1 = "GetPropertyWithNumberOfUnitsByIdWithTest_Address1",
                Address2 = "GetPropertyWithNumberOfUnitsByIdWithTest_Address2",
                State = "GetPropertyWithNumberOfUnitsByIdWithTest_State",
                ZIPCode = "GetPropertyWithNumberOfUnitsByIdWithTest_ZipCode",
                City = "GetPropertyWithNumberOfUnitsByIdWithTest_City",
                DictionaryCountry = _testCountry,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = false,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            expected.SetPropertyNameValue("GetPropertyWithNumberOfUnitsByIdWithTest_PropertyName");
            expected.SetShortDescriptionValue("GetPropertyWithNumberOfUnitsByIdWithTest_ShortDescription");
            expected.SetLongDescriptionValue("GetPropertyWithNumberOfUnitsByIdWithTest_LongDescription");

            _propertiesService.AddProperty(expected);
            _efContext.SaveChanges();
            _testProperties.Add(expected);

            int numberOfUnits;
            var actual = _propertiesService.GetPropertyById(expected.PropertyID, out numberOfUnits, true);           
        }

        [TestMethod]
        public void GetPropertiesByOwnerIdTest()
        {
            Dictionary<int, bool> propertiesFound = new Dictionary<int, bool>();
            Property[] properties = new Property[5];

            for (int i = 0; i < 5; ++i)
            {
                properties[i] = new Property()
                {
                    User = _testUser,
                    Destination = _testDestination,
                    PropertyCode = "GetPropertiesByOwnerIdTest_PropertyCode_" + i,
                    Address1 = "GetPropertiesByOwnerIdTest_Address1_" + i,
                    Address2 = "GetPropertiesByOwnerIdTest_Address2_" + i,
                    State = "GetPropertiesByOwnerIdTest_State_" + i,
                    ZIPCode = "GetPropertiesByOwnerIdTest_ZipCode_" + i,
                    City = "GetPropertyWithNumberOfUnitsByIdWithTest_City_" + i,
                    DictionaryCountry = _testCountry,
                    Latitude = 100.12M,
                    Longitude = 120.21M,
                    Altitude = 210.17M,
                    PropertyLive = true,
                    StandardCommission = 12.0M,
                    PropertyType = _testPropertyType,
                    CheckIn = new TimeSpan(21, 12, 11),
                    CheckOut = new TimeSpan(12, 21, 22),
                    DictionaryCulture = _testCulture,
                    TimeZone = "Eastern Standard Time"
                };
                properties[i].SetPropertyNameValue("GetPropertiesByOwnerIdTest_PropertyName_" + i);
                properties[i].SetShortDescriptionValue("GetPropertiesByOwnerIdTest_ShortDescription_" + i);
                properties[i].SetLongDescriptionValue("GetPropertiesByOwnerIdTest_LongDescription_" + i);

                _propertiesService.AddProperty(properties[i]);
                _efContext.SaveChanges();
                _testProperties.Add(properties[i]);

                propertiesFound.Add(properties[i].PropertyID, false);
            }


            Property expected;
            var actual = _propertiesService.GetPropertiesByOwnerId(_testUser.UserID);


            foreach (var actualObj in actual)
            {
                if (propertiesFound.ContainsKey(actualObj.PropertyID))
                {
                    expected = properties.Where(p => p.PropertyID == actualObj.PropertyID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    propertiesFound[actualObj.PropertyID] = true;
                }
            }

            if (propertiesFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "properties", "GetPropertiesByOwnerId"));
        }

        [TestMethod]
        public void GetPropertiesByManagerTest()
        {
            User user = new User()
            {
                Firstname = "GetPropertiesByManagerTest_Firstname",
                Lastname = "GetPropertiesByManagerTest_Lastname",
                email = "GetPropertiesByManagerTest_E-mail",                
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture

            };
            _userService.AddUser(user);
            _efContext.SaveChanges();
            _testManagers.Add(user);

            Dictionary<int, bool> propertiesFound = new Dictionary<int, bool>();
            Property[] properties = new Property[5];

            for (int i = 0; i < 5; ++i)
            {
                properties[i] = new Property()
                {
                    User = _testUser,
                    Destination = _testDestination,
                    PropertyCode = "GetPropertiesByManagerTest_PropertyCode_" + i,
                    Address1 = "GetPropertiesByManagerTest_Address1_" + i,
                    Address2 = "GetPropertiesByManagerTest_Address2_" + i,
                    State = "GetPropertiesByManagerTest_State_" + i,
                    ZIPCode = "GetPropertiesByManagerTest_ZipCode_" + i,
                    City = "GetPropertiesByManagerTest_City_" + i,
                    DictionaryCountry = _testCountry,
                    
                    
                    Latitude = 100.12M,
                    Longitude = 120.21M,
                    Altitude = 210.17M,
                    PropertyLive = true,
                    StandardCommission = 12.0M,
                    PropertyType = _testPropertyType,
                    CheckIn = new TimeSpan(21, 12, 11),
                    CheckOut = new TimeSpan(12, 21, 22),
                    DictionaryCulture = _testCulture,
                    TimeZone = "Eastern Standard Time",
                    PropertyAssignedManagers = new List<PropertyAssignedManager>()
                };
                properties[i].SetPropertyNameValue("GetPropertiesByManagerTest_PropertyName_" + i);
                properties[i].SetShortDescriptionValue("GetPropertiesByManagerTest_ShortDescription_" + i);
                properties[i].SetLongDescriptionValue("GetPropertiesByManagerTest_LongDescription_" + i);

                //properties[i].PropertyAssignedManagers.Add(new PropertyAssignedManager()
                //{
                //    Property = properties[i],
                //    User = _testManager,
                //    Role = _efContext.Roles.Where(f => f.RoleLevel.Equals(5)).SingleOrDefault()
                //});

                _propertiesService.AddProperty(properties[i]);
                _efContext.SaveChanges();
                _propertiesService.AddPropertyManager(user.UserID, properties[i].PropertyID, RoleLevel.PropertyManager);
                _efContext.SaveChanges();

                _testProperties.Add(properties[i]);

                propertiesFound.Add(properties[i].PropertyID, false);
            }

            Property expected;
            var actual = _propertiesService.GetPropertiesByManager(user.UserID, RoleLevel.PropertyManager);

            foreach (var actualObj in actual)
            {
                if (propertiesFound.ContainsKey(actualObj.PropertyID))
                {
                    expected = properties.Where(p => p.PropertyID == actualObj.PropertyID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    propertiesFound[actualObj.PropertyID] = true;
                }
            }

            if (propertiesFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "properties", "GetPropertiesByManagerTest"));
        }

        [TestMethod]
        public void AddPropertyManagerTest()
        {
            User user = new User()
            {
                Firstname = "AddPropertyManagerTest_Firstname",
                Lastname = "AddPropertyManagerTest_Lastname",
                email = "AddPropertyManagerTest_E-mail",
                
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture

            };
            _userService.AddUser(user);
            _efContext.SaveChanges();
            _testManagers.Add(user);
            _propertiesService.AddPropertyManager(user.UserID, _testProperty.PropertyID, RoleLevel.PropertyManager);
            _efContext.SaveChanges();

            Assert.AreEqual(1, _testProperty.PropertyAssignedManagers.Count);
            ExtendedAssert.AreEqual(user, _testProperty.PropertyAssignedManagers.First().User);

        }

        [TestMethod]
        public void GetPropertyManagerByTypeTest()
        {
            #region Setup managers

            User user = new User()
            {
                Firstname = "GetPropertyManagerByTypeTest_Firstname",
                Lastname = "GetPropertyManagerByTypeTest_Lastname",
                email = "GetPropertyManagerByTypeTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture,
                UserRoles = new List<UserRole>()
            };
            _userService.AddUser(user);
            _efContext.SaveChanges();
            _testManagers.Add(user);

            _propertiesService.AddPropertyManager(user.UserID, _testProperty.PropertyID, RoleLevel.PropertyManager);
            _efContext.SaveChanges();
            
            user = new User()
            {
                Firstname = "GetPropertyManagerByTypeTest_Firstname",
                Lastname = "GetPropertyManagerByTypeTest_Lastname",
                email = "GetPropertyManagerByTypeTest_E-mail",                
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture,
                UserRoles = new List<UserRole>()
            };
            _userService.AddUser(user);
            _efContext.SaveChanges();
            _testManagers.Add(user);

            _propertiesService.AddPropertyManager(user.UserID, _testProperty.PropertyID, RoleLevel.KeyManager);
            _efContext.SaveChanges();

            user = new User()
            {
                Firstname = "GetPropertyManagerByTypeTest_Firstname",
                Lastname = "GetPropertyManagerByTypeTest_Lastname",
                email = "GetPropertyManagerByTypeTest_E-mail",                
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture,
                UserRoles = new List<UserRole>()
            };
            _userService.AddUser(user);
            _efContext.SaveChanges();
            _testManagers.Add(user);

            _propertiesService.AddPropertyManager(user.UserID, _testProperty.PropertyID, RoleLevel.Manager);
            _efContext.SaveChanges();

            #endregion

            #region Property manager

            PropertyAssignedManager actual = _propertiesService.GetPropertyManagerByType(_testProperty.PropertyID, RoleLevel.PropertyManager);
            ExtendedAssert.AreEqual(_testProperty.PropertyAssignedManagers.ElementAt(0), actual);

            #endregion

            #region Key manager

            actual = _propertiesService.GetPropertyManagerByType(_testProperty.PropertyID, RoleLevel.KeyManager);
            ExtendedAssert.AreEqual(_testProperty.PropertyAssignedManagers.ElementAt(1), actual);

            #endregion

            #region Upload manager

            actual = _propertiesService.GetPropertyManagerByType(_testProperty.PropertyID, RoleLevel.Manager);
            ExtendedAssert.AreEqual(_testProperty.PropertyAssignedManagers.ElementAt(2), actual);

            #endregion
        }

        [TestMethod]
        public void DeleteManagerFromPropertyTest()
        {
            #region One manager
            User user = new User()
            {
                Firstname = "GetPropertyManagerByTypeTest_Firstname",
                Lastname = "GetPropertyManagerByTypeTest_Lastname",
                email = "GetPropertyManagerByTypeTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture,
                UserRoles = new List<UserRole>()
            };
            _userService.AddUser(user);
            _efContext.SaveChanges();
            _testManagers.Add(user);

            _propertiesService.AddPropertyManager(user.UserID, _testProperty.PropertyID, RoleLevel.PropertyManager);
            _efContext.SaveChanges();

            _propertiesService.DeleteManagerFromProperty(_testProperty.PropertyAssignedManagers.ElementAt(0).PropertyAssignedManagerId);
            _efContext.SaveChanges();

            Assert.AreEqual(0, _testProperty.PropertyAssignedManagers.Count);
            #endregion

            #region More managers
            user = new User()
            {
                Firstname = "GetPropertyManagerByTypeTest_Firstname",
                Lastname = "GetPropertyManagerByTypeTest_Lastname",
                email = "GetPropertyManagerByTypeTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture,
                UserRoles = new List<UserRole>()
            };
            _userService.AddUser(user);
            _efContext.SaveChanges();
            _testManagers.Add(user);

            _propertiesService.AddPropertyManager(user.UserID, _testProperty.PropertyID, RoleLevel.PropertyManager);
            _efContext.SaveChanges();

            user = new User()
            {
                Firstname = "GetPropertyManagerByTypeTest_Firstname",
                Lastname = "GetPropertyManagerByTypeTest_Lastname",
                email = "GetPropertyManagerByTypeTest_E-mail",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _testCulture,
                UserRoles = new List<UserRole>()
            };
            _userService.AddUser(user);
            _efContext.SaveChanges();
            _testManagers.Add(user);

            _propertiesService.AddPropertyManager(user.UserID, _testProperty.PropertyID, RoleLevel.KeyManager);
            _efContext.SaveChanges();

            _propertiesService.DeleteManagerFromProperty(_testProperty.PropertyAssignedManagers.ElementAt(1).PropertyAssignedManagerId);
            _efContext.SaveChanges();

            Assert.AreEqual(1, _testProperty.PropertyAssignedManagers.Count);

            Assert.IsNotNull(_testProperty.PropertyAssignedManagers.ElementAt(0), Messages.ObjectIsNull);

            #endregion
        }

        [TestMethod]
        public void GetPropertyByCodeTest()
        {
            #region Property exists
            Property actual = _propertiesService.GetPropertyByCode(_testProperty.PropertyCode);
            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            ExtendedAssert.AreEqual(_testProperty, actual);
            #endregion

            #region Property does not exist
            actual = _propertiesService.GetPropertyByCode("invalid_code");
            Assert.IsNull(actual, Messages.ObjectIsNotNull);
            #endregion
        }

        [TestMethod]
        public void GetPropertiesTest()
        {
            Dictionary<int, bool> newPropertiesFound = new Dictionary<int, bool>();
            Property[] newProperties = new Property[5];

            for (int i = 0; i < 5; ++i)
            {
                newProperties[i] = new Property()
                {
                    User = _testUser,
                    Destination = _testDestination,
                    PropertyCode = "GetPropertiesTest_PropertyCode_" + i,
                    Address1 = "GetPropertiesTest_Address1_" + i,
                    Address2 = "GetPropertiesTest_Address2_" + i,
                    State = "GetPropertiesTest_State_" + i,
                    ZIPCode = "GetPropertiesTest_ZipCode_" + i,
                    City = "GetPropertyWithNumberOfUnitsByIdWithTest_City_" + i,
                    DictionaryCountry = _testCountry,
                    
                    
                    Latitude = 100.12M,
                    Longitude = 120.21M,
                    Altitude = 210.17M,
                    PropertyLive = true,
                    StandardCommission = 12.0M,
                    PropertyType = _testPropertyType,
                    CheckIn = new TimeSpan(21, 12, 11),
                    CheckOut = new TimeSpan(12, 21, 22),
                    DictionaryCulture = _testCulture,
                    TimeZone = "Eastern Standard Time"
                };
                newProperties[i].SetPropertyNameValue("GetPropertiesTest_PropertyName_" + i);
                newProperties[i].SetShortDescriptionValue("GetPropertiesTest_ShortDescription_" + i);
                newProperties[i].SetLongDescriptionValue("GetPropertiesTest_LongDescription_" + i);

                _propertiesService.AddProperty(newProperties[i]);
                _efContext.SaveChanges();
                _testProperties.Add(newProperties[i]);

                newPropertiesFound.Add(newProperties[i].PropertyID, false);
            }

            Property expected;
            var actual = _propertiesService.GetProperties();
            foreach (var actualObj in actual)
            {
                if (newPropertiesFound.ContainsKey(actualObj.PropertyID))
                {
                    expected = newProperties.Where(p => p.PropertyID == actualObj.PropertyID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newPropertiesFound[actualObj.PropertyID] = true;
                }
            }

            if (newPropertiesFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "properties", "GetProperties"));
        }

        [TestMethod]
        public void GetPropertyIdByUnitId()
        {
            UnitType unitType = new UnitType()
            {
                Property = _testProperty,
                UnitTypeCode = "GetPropertyIdByUnitId_UnitTypeCode"
            };
            unitType.SetTitleValue("GetPropertyIdByUnitId_UnitTypeTitle");
            unitType.SetDescValue("GetPropertyIdByUnitId_UnitTypeDesc");

            _unitTypesService.AddUnitType(unitType);
            _efContext.SaveChanges();

            Unit unit = new Unit()
            {
                Property = _testProperty,
                UnitType = unitType,
                UnitCode = "GetPropertyIdByUnitId_UnitCode",
            };
            unit.SetTitleValue("GetPropertyIdByUnitId_UnitTitle");
            unit.SetDescValue("GetPropertyIdByUnitId_UnitDesc");

            _unitsService.AddUnit(unit);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyIdByUnitId(unit.UnitID);

            Assert.AreEqual(_testProperty.PropertyID, actual);

            _unitsService.DeleteUnit(unit.UnitID);
            _unitTypesService.DeleteUnitType(unitType.UnitTypeID);
        }

        [TestMethod]
        public void AddPropertyTest()
        {
            Property expected = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            expected.SetPropertyNameValue("AddPropertyTest_PropertyName");
            expected.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            expected.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            _propertiesService.AddProperty(expected);
            _efContext.SaveChanges();
            _testProperties.Add(expected);

            var actual = _propertiesService.GetPropertyById(expected.PropertyID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddPropertyRequiredFieldsTest()
        {
            Property property;

            #region Test field OwnerId
            property = new Property()
            {
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time",
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "FK_Properties_Users");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field OTP_DestinationID
            property = new Property()
            {
                User = _testUser,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "FK_Properties_Destinations");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field CountryId
            property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "FK_Properties_DictionaryCountries");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field NativeLanguage
            property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "FK_Properties_DictionaryLanguages");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field PropertyCurrency
            property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "FK_Properties_DictionaryCurrencies");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion           

            #region Test field PropertyName_i18n
            property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "PropertyName_i18n");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field Short_Description_i18n
            property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "Short_Description_i18n");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field Long_Description_i18n
            property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");


            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "Long_Description_i18n");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field Address1
            property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "Address1");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field Address2
            // Address2 is nullable
            #endregion

            #region Test field State
            property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                ZIPCode = "AddPropertyTest_ZipCode",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "State");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field ZIPCode
            property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                City = "AddPropertyTest_City",
                DictionaryCountry = _testCountry,
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "ZIPCode");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field City
            property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "AddPropertyTest_PropertyCode",
                Address1 = "AddPropertyTest_Address1",
                Address2 = "AddPropertyTest_Address2",
                State = "AddPropertyTest_State",
                ZIPCode = "AddPropertyTest_ZipCode",
                DictionaryCountry = _testCountry,
                
                
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = _testPropertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("AddPropertyTest_PropertyName");
            property.SetShortDescriptionValue("AddPropertyTest_ShortDescription");
            property.SetLongDescriptionValue("AddPropertyTest_LongDescription");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _propertiesService.AddProperty(property);
                _efContext.SaveChanges();
            }, "City");
            _efContext.Properties.Remove(property);
            _efContext.SaveChanges();
            #endregion

            #region Test field Latitude
            // Latitude is of type decimal and cannot be nullified
            #endregion

            #region Test field Longitude
            // Longitude is of type decimal and cannot be nullified
            #endregion

            #region Test field Altitude
            // Altitude is of type decimal and cannot be nullified
            #endregion

            #region Test field PropertyLive
            // PropertyLive is of type boolean and cannot be nullified
            #endregion

            #region Test field StandardCommission
            // StandardCommission is of type decimal and cannot be nullified
            #endregion

            #region Test field CheckIn
            // CheckIn is of type Timespan and cannot be nullified
            #endregion

            #region Test field CheckOut
            // CheckOut is of type Timespan and cannot be nullified
            #endregion
        }

        #endregion

        #region Check dependencies

        [TestMethod]
        public void HasAnyUseTagTest()
        {
            bool isUsed;

            Tag tag = new Tag()
            {
                TagGroup = _tagGroupsService.GetTagGroups().First()
            };
            tag.SetNameValue("HasAnyUseTagTest_Name");

            _tagsService.AddTag(tag);
            _efContext.SaveChanges();

            isUsed = _propertiesService.HasAnyUseTag(tag.TagID);

            Assert.IsFalse(isUsed);

            PropertyTag propertyTag = new PropertyTag()
            {
                Tag = tag,
                Property = _testProperty
            };

            _propertiesService.AddPropertyTag(propertyTag);
            _efContext.SaveChanges();

            isUsed = _propertiesService.HasAnyUseTag(tag.TagID);

            Assert.IsTrue(isUsed);

            _propertiesService.RemovePropertyTag(propertyTag.PropertyTagID);
            _tagsService.DeleteTag(tag.TagID);
        }

        [TestMethod]
        public void HasAnyUsePropertyTypeTest()
        {
            bool isUsed;

            PropertyType propertyType = new PropertyType();
            propertyType.SetNameValue("HasAnyUsePropertyTypeTest_Name");

            _propertyTypeService.AddPropertyType(propertyType);
            _efContext.SaveChanges();

            isUsed = _propertiesService.HasAnyUsePropertyType(propertyType.PropertyTypeId);

            Assert.IsFalse(isUsed);

            Property property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "HasAnyUsePropertyTypeTest_PropertyCode",
                Address1 = "HasAnyUsePropertyTypeTest_Address1",
                State = "HasAnyUsePropertyTypeTest_State",
                ZIPCode = "HasAnyUsePropertyTypeTest_ZipCode",
                City = "HasAnyUsePropertyTypeTest_City",
                DictionaryCountry = _testCountry,
                
                
                PropertyType = propertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            property.SetPropertyNameValue("HasAnyUsePropertyTypeTest_PropertyName");
            property.SetShortDescriptionValue("HasAnyUsePropertyTypeTest_ShortDescription");
            property.SetLongDescriptionValue("HasAnyUsePropertyTypeTest_LongDescription");

            _propertiesService.AddProperty(property);
            _efContext.SaveChanges();

            isUsed = _propertiesService.HasAnyUsePropertyType(propertyType.PropertyTypeId);

            Assert.IsTrue(isUsed);

            _propertiesService.RemoveProperty(property.PropertyID);
            _propertyTypeService.DeletePropertyType(propertyType.PropertyTypeId);
        }

        [TestMethod]
        public void HasAnyUseAmenityTest()
        {
            bool isUsed;

            Amenity amenity = new Amenity()
            {
                AmenityGroup = _amenityGroupsService.GetAmenityGroups().First(),
                AmenityCode = "HasAnyUseAmenityTest_Code",
                Countable = true
            };
            amenity.SetTitleValue("HasAnyUseAmenityTest_Title");
            amenity.SetDescriptionValue("HasAnyUseAmenityTest_Description");

            _amenitiesService.AddAmenity(amenity);
            _efContext.SaveChanges();

            isUsed = _propertiesService.HasAnyUseAmenity(amenity.AmenityID);

            Assert.IsFalse(isUsed);

            PropertyAmenity propertyAmenity = new PropertyAmenity()
            {
                Amenity = amenity,
                Property = _testProperty
            };

            _propertiesService.AddPropertyAmenity(propertyAmenity);
            _efContext.SaveChanges();

            isUsed = _propertiesService.HasAnyUseAmenity(amenity.AmenityID);

            Assert.IsTrue(isUsed);

            _propertiesService.RemovePropertyAmenity(propertyAmenity.PropertyAmenityID);
            _amenitiesService.DeleteAmenity(amenity.AmenityID);
        }

        [TestMethod]
        public void HasAnyReservationTest()
        {
            bool hasAnyReservation;

            Property property = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "HasAnyReservationTest_PropertyCode",
                Address1 = "HasAnyReservationTest_Address1",
                State = "HasAnyReservationTest_State",
                ZIPCode = "HasAnyReservationTest_ZipCode",
                City = "HasAnyReservationTest_City",
                DictionaryCountry = _testCountry,
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };

            property.SetPropertyNameValue("HasAnyReservationTest_PropertyName");
            property.SetShortDescriptionValue("HasAnyReservationTest_ShortDescription");
            property.SetLongDescriptionValue("HasAnyReservationTest_LongDescription");

            _propertiesService.AddProperty(property);
            _efContext.SaveChanges();

            #region No dependencies

            hasAnyReservation = _propertiesService.HasAnyReservation(property.PropertyID);

            Assert.IsFalse(hasAnyReservation);

            #endregion

            // TODO
            // Change dependencies to Reservations

            #region Reservation dependency

            UnitType unitType = new UnitType
            {
                Property = property,
                UnitTypeCode = "HasAnyReservationTest_UnitTypeCode"
            };

            unitType.SetTitleValue("HasAnyReservationTest_UnitTypeTitle");
            unitType.SetDescValue("HasAnyReservationTest_UnitTypeDesc");

            _unitTypesService.AddUnitType(unitType);

            Unit unit = new Unit
            {
                Property = property,
                UnitType = unitType,
                UnitCode = "HasAnyReservationTest_UnitCode",
            };

            unit.SetTitleValue("HasAnyReservationTest_UnitTitle");
            unit.SetDescValue("HasAnyReservationTest_UnitDesc");

            _unitsService.AddUnit(unit);

            DataAccess.CreditCardType _testCreditCardType = _bookingService.GetCreditCardTypes().First();

            GuestPaymentMethod _testPaymentMethod = new GuestPaymentMethod()
            {
                User = _testUser,
                CreditCardType = _testCreditCardType,
                CreditCardLast4Digits = "1234",
                ReferenceToken = "aavsvsdfg"
            };

            _efContext.GuestPaymentMethods.Add(_testPaymentMethod);
            _efContext.SaveChanges();

            Reservation reseravation = new Reservation
            {
                Property = property,
                Unit = unit,
                UnitType = unitType,
                User = _testUser,
                ConfirmationID = "abcdef",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.CheckedIn,
                DateArrival = DateTime.Now,
                DateDeparture = DateTime.Now,
                TotalPrice = 2.45M,                
                TripBalance = 123.4M,
                Agreement = new byte[] { 234, 54, 23, 54, 2 },
                GuestPaymentMethod = _testPaymentMethod,
                NumberOfGuests = 1,
                Invoice = new byte[] { 12, 3, 5, 23, 54 },
                TransactionFee = 23.67M,
                TransactionToken = "24r343rt",
                UpfrontPaymentProcessed = null,
                ParentReservationId = null,
                LinkAuthorizationToken = "435df",
                DictionaryCulture = _testCulture
            };

            _reservationsService.AddReservation(reseravation);
            _efContext.SaveChanges();

            hasAnyReservation = _propertiesService.HasAnyReservation(property.PropertyID);

            Assert.IsTrue(hasAnyReservation);

            #endregion

            DataAccessHelper.RemoveReservation(_efContext, reseravation);
            _efContext.GuestPaymentMethods.Remove(_testPaymentMethod);
            _efContext.SaveChanges();
            _unitsService.DeleteUnit(unit.UnitID);
            _unitTypesService.DeleteUnitType(unitType.UnitTypeID);
            _propertiesService.RemoveProperty(property.PropertyID);
        }

        #endregion

        #region Guest Reviews

        // TODO GetPropertyReviewRateTest : finish, when service updated
        //[TestMethod]
        //public void GetPropertyReviewRateTest()
        //{
        //    UnitType unitType = new UnitType()
        //    {
        //        Property = _testProperty,
        //        UnitTypeCode = "GetPropertyIdByUnitId_UnitTypeCode"
        //    };
        //    unitType.SetTitleValue(LanguageCode.en, "GetPropertyIdByUnitId_UnitTypeTitle");
        //    unitType.SetDescValue(LanguageCode.en, "GetPropertyIdByUnitId_UnitTypeDesc");

        //    _unitTypesService.AddUnitType(unitType);
        //    _efContext.SaveChanges();

        //    Unit unit = new Unit()
        //    {
        //        Property = _testProperty,
        //        UnitType = unitType,
        //        UnitCode = "GetPropertyIdByUnitId_UnitCode",
        //    };
        //    unit.SetTitleValue(LanguageCode.en, "GetPropertyIdByUnitId_UnitTitle");
        //    unit.SetDescValue(LanguageCode.en, "GetPropertyIdByUnitId_UnitDesc");

        //    _unitsService.AddUnit(unit);
        //    _efContext.SaveChanges();

        //    Reservation reservation = new Reservation()
        //    {
        //        Property = property,
        //        Unit = unit,
        //        UnitType = unitType,
        //        User = _testUser,
        //        BookingDate = DateTime.Now,
        //        DateArrival = DateTime.Now,
        //        DateDeparture = DateTime.Now,
        //        TotalPrice = 0.11M,
        //        TripBalance = 21.10M,
        //        DictionaryCurrency = _currencyService.GetCurrencies().First()
        //    };

        //    Random rnd = new Random((int)DateTime.Now.Ticks);
        //    GuestReview[] guestReviews = new GuestReview[5];
        //    float sumSatisfactionCleanliness = 0,
        //        sumSatisfactionExperience = 0,
        //        sumSatisfactionLocation = 0;

        //    for (int i = 0; i < 5; ++i)
        //    {
        //        guestReviews[i] = new GuestReview()
        //        {
        //            User = _testUser,
        //            Property = property,
        //            Reservation = reservation,
        //            ReviewTitle = "GetPropertyIdByUnitId_ReviewTitle_" + i,
        //            ReviewDate = DateTime.Now,
        //            Satisfaction_Cleanliness = rnd.Next(5),
        //            Satisfaction_Experience = rnd.Next(5),
        //            Satisfaction_Location = rnd.Next(5)
        //        };

        //        sumSatisfactionCleanliness += guestReviews[i].Satisfaction_Cleanliness;
        //        sumSatisfactionExperience += guestReviews[i].Satisfaction_Experience;
        //        sumSatisfactionLocation += guestReviews[i].Satisfaction_Location;
        //    }


        //    _unitsService.DeleteUnit(unit.UnitID);
        //    _unitTypesService.DeleteUnitType(unitType.UnitTypeID);
        //}


        // TODO GetPropertyGuestReviewsTest : finish
        //[TestMethod]
        //public void GetPropertyGuestReviewsTest()
        //{
        //} 

        #endregion

        #region Static Content

        [TestMethod]
        public void GetPropertyStaticContentByIdTest()
        {
            PropertyStaticContent expected = new PropertyStaticContent()
            {
                Property = _testProperty,
                ContentCode = 666,
                ContentValue = "GetPropertyStaticContentByIdTest_Value"
            };
            expected.SetContentValue("GetPropertyStaticContentByIdTest_ValueXML");

            _propertiesService.AddPropertyStaticContent(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyStaticContentById(expected.PropertyStaticContentId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetPropertyStaticContentByTypeTest()
        {
            PropertyStaticContent[] newPscs = new PropertyStaticContent[5];
            Dictionary<int, bool> newPscsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = _testProperty,
                    ContentCode = (int)PropertyStaticContentType.Feature,
                };
                newPscs[i].SetContentValue("GetPropertyStaticContentByTypeTest_Feature_Value_" + i);

                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();

                newPscsFound.Add(newPscs[i].PropertyStaticContentId, false);
            }

            PropertyStaticContent expected;
            var actual = _propertiesService.GetPropertyStaticContentByType(_testProperty.PropertyID, PropertyStaticContentType.Feature);
            foreach (var actualObj in actual)
            {
                if (newPscsFound.ContainsKey(actualObj.PropertyStaticContentId))
                {
                    expected = newPscs.Where(psc => psc.PropertyStaticContentId == actualObj.PropertyStaticContentId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newPscsFound[actualObj.PropertyStaticContentId] = true;
                }
            }

            if (newPscsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "property static contents", "GetPropertyStaticContentByType"));
        }

        [TestMethod]
        public void GetPropertyStaticContentByTypeListTest()
        {
            int i;
            PropertyStaticContent[] newPscs = new PropertyStaticContent[6];
            Dictionary<int, bool> newPscsFound = new Dictionary<int, bool>();

            for (i = 0; i < 2; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = _testProperty,
                    ContentCode = (int)PropertyStaticContentType.Feature,
                };
                newPscs[i].SetContentValue("GetPropertyStaticContentByTypeListTest_Feature_Value_" + i);

                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();

                newPscsFound.Add(newPscs[i].PropertyStaticContentId, false);
            }

            for (i = 2; i < 4; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = _testProperty,
                    ContentCode = (int)PropertyStaticContentType.LocalAreaDescription,
                };
                newPscs[i].SetContentValue("GetPropertyStaticContentByTypeListTest_LocalAreaDescription_Value_" + i);

                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();

                newPscsFound.Add(newPscs[i].PropertyStaticContentId, false);
            }

            for (i = 4; i < 6; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = _testProperty,
                    ContentCode = (int)PropertyStaticContentType.DistanceToMainAttraction,
                };
                newPscs[i].SetContentValue("GetPropertyStaticContentByTypeListTest_DistanceToMainAttraction_Value_" + i);

                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();

                newPscsFound.Add(newPscs[i].PropertyStaticContentId, false);
            }

            PropertyStaticContent expected;
            var actual = _propertiesService.GetPropertyStaticContentByType(_testProperty.PropertyID, new List<PropertyStaticContentType>() { PropertyStaticContentType.Feature, PropertyStaticContentType.LocalAreaDescription, PropertyStaticContentType.DistanceToMainAttraction });
            foreach (var actualObj in actual)
            {
                if (newPscsFound.ContainsKey(actualObj.PropertyStaticContentId))
                {
                    expected = newPscs.Where(psc => psc.PropertyStaticContentId == actualObj.PropertyStaticContentId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newPscsFound[actualObj.PropertyStaticContentId] = true;
                }
            }

            if (newPscsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "property static contents", "GetPropertyStaticContentByType"));
        }

        [TestMethod]
        public void GetPropertyImagesByPropertyIdTest()
        {
            PropertyStaticContent[] newImages = new PropertyStaticContent[5];
            Dictionary<string, bool> newImagesFound = new Dictionary<string, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newImages[i] = new PropertyStaticContent()
                {
                    Property = _testProperty,
                    ContentCode = (int)PropertyStaticContentType.FullScreen,
                    ContentValue = "GetPropertyImagesByPropertyIdTest_FullImage_Value_" + i
                };
                _propertiesService.AddPropertyStaticContent(newImages[i]);
                _efContext.SaveChanges();

                newImagesFound.Add(blobContainter + newImages[i].ContentValue, false);
            }

            var actual = _propertiesService.GetPropertyImagesByPropertyId(_testProperty.PropertyID, PropertyStaticContentType.FullScreen);
            foreach (var obj in actual)
            {
                if (newImagesFound.ContainsKey(obj))
                    newImagesFound[obj] = true;
            }

            if (newImagesFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "full images", "GetPropertyImagesByPropertyId"));
        }

        [TestMethod]
        public void GetPropertyStaticContentInfoTest()
        {
            Dictionary<PropertyStaticContentType, bool> types = new Dictionary<PropertyStaticContentType, bool>();
            PropertyStaticContent tmpPsc;
            List<PropertyStaticContent> tmpList;
            PropertyStaticContentResult[] newPscrs = new PropertyStaticContentResult[3];
            Dictionary<int, bool> newPscsFound = new Dictionary<int, bool>();

            types.Add(PropertyStaticContentType.Feature, false);
            types.Add(PropertyStaticContentType.DistanceToMainAttraction, false);
            types.Add(PropertyStaticContentType.LocalAreaDescription, false);

            for (int k = 0; k < types.Count; ++k)
            {
                newPscrs[k] = new PropertyStaticContentResult();

                newPscrs[k].ContentCode = (int)types.ElementAt(k).Key;
                tmpList = new List<PropertyStaticContent>();
                for (int i = 0; i < 3; ++i)
                {
                    tmpPsc = new PropertyStaticContent()
                    {
                        Property = _testProperty,
                        ContentCode = (int)types.ElementAt(k).Key,
                    };
                    tmpPsc.SetContentValue("GetPropertyStaticContentInfoTest_PSC_Value_" + ((i + 1) * (k + 1)));

                    _propertiesService.AddPropertyStaticContent(tmpPsc);
                    _efContext.SaveChanges();

                    newPscsFound.Add(tmpPsc.PropertyStaticContentId, false);
                    tmpList.Add(tmpPsc);
                }

                newPscrs[k].ContentItems = tmpList;
            }

            PropertyStaticContent expected;
            var actual = _propertiesService.GetPropertyStaticContentInfo(_testProperty.PropertyID);
            foreach (var obj in actual)
            {
                foreach (var newObj in newPscrs)
                {
                    if (newObj.ContentCode == obj.ContentCode)
                    {
                        foreach (var actualObj in obj.ContentItems)
                        {
                            expected = newObj.ContentItems.Where(p => p.PropertyStaticContentId == actualObj.PropertyStaticContentId).FirstOrDefault();

                            Assert.IsNotNull(expected);
                            ExtendedAssert.AreEqual(expected, actualObj);

                            newPscsFound[actualObj.PropertyStaticContentId] = true;
                        }

                        types[(PropertyStaticContentType)obj.ContentCode] = true;
                    }
                }
            }

            if (types.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "property static contents (types)", "GetPropertyStaticContentInfo"));

            if (newPscsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "property static contents", "GetPropertyStaticContentInfo"));
        }

        [TestMethod]
        public void GetPropertyStaticContentsTest()
        {
            int i;
            PropertyStaticContent[] newPscs = new PropertyStaticContent[6];
            Dictionary<int, bool> newPscsFound = new Dictionary<int, bool>();
            PropertyStaticContent expected;
            IEnumerable<PropertyStaticContent> actual;

            #region Without types
            for (i = 0; i < 2; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = (i % 2 > 0) ? _testProperty : _testProperty2,
                    ContentCode = (int)PropertyStaticContentType.Feature,
                };
                newPscs[i].SetContentValue("GetPropertyStaticContentsTest_Feature_Value_" + i);

                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();

                newPscsFound.Add(newPscs[i].PropertyStaticContentId, false);
            }

            for (i = 2; i < 4; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = (i % 2 > 0) ? _testProperty : _testProperty2,
                    ContentCode = (int)PropertyStaticContentType.LocalAreaDescription,
                };
                newPscs[i].SetContentValue("GetPropertyStaticContentsTest_LocalAreaDescription_Value_" + i);

                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();

                newPscsFound.Add(newPscs[i].PropertyStaticContentId, false);
            }

            for (i = 4; i < 6; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = (i % 2 > 0) ? _testProperty : _testProperty2,
                    ContentCode = (int)PropertyStaticContentType.DistanceToMainAttraction,
                };
                newPscs[i].SetContentValue("GetPropertyStaticContentsTest_DistanceToMainAttraction_Value_" + i);

                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();

                newPscsFound.Add(newPscs[i].PropertyStaticContentId, false);
            }

            actual = _propertiesService.GetPropertyStaticContents();
            foreach (var actualObj in actual)
            {
                if (newPscsFound.ContainsKey(actualObj.PropertyStaticContentId))
                {
                    expected = newPscs.Where(psc => psc.PropertyStaticContentId == actualObj.PropertyStaticContentId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newPscsFound[actualObj.PropertyStaticContentId] = true;
                }
            }

            if (newPscsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "property static contents", "GetPropertyStaticContentsTest"));
            #endregion

            newPscsFound.Clear();

            #region With types
            for (i = 0; i < 2; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = (i % 2 > 0) ? _testProperty : _testProperty2,
                    ContentCode = (int)PropertyStaticContentType.Feature,
                };
                newPscs[i].SetContentValue("GetPropertyStaticContentsTest_Feature_Value_" + i);

                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();

                newPscsFound.Add(newPscs[i].PropertyStaticContentId, false);
            }

            for (i = 2; i < 4; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = (i % 2 > 0) ? _testProperty : _testProperty2,
                    ContentCode = (int)PropertyStaticContentType.LocalAreaDescription,
                };
                newPscs[i].SetContentValue("GetPropertyStaticContentsTest_LocalAreaDescription_Value_" + i);

                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();

                newPscsFound.Add(newPscs[i].PropertyStaticContentId, false);
            }

            for (i = 4; i < 6; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = (i % 2 > 0) ? _testProperty : _testProperty2,
                    ContentCode = (int)PropertyStaticContentType.DistanceToMainAttraction,
                };
                newPscs[i].SetContentValue("GetPropertyStaticContentsTest_DistanceToMainAttraction_Value_" + i);

                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();
            }

            actual = _propertiesService.GetPropertyStaticContents(new List<PropertyStaticContentType>() { PropertyStaticContentType.Feature, PropertyStaticContentType.LocalAreaDescription });
            foreach (var actualObj in actual)
            {
                if (newPscsFound.ContainsKey(actualObj.PropertyStaticContentId))
                {
                    expected = newPscs.Where(psc => psc.PropertyStaticContentId == actualObj.PropertyStaticContentId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newPscsFound[actualObj.PropertyStaticContentId] = true;
                }
            }

            if (newPscsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "property static contents", "GetPropertyStaticContentsTest"));
            #endregion
        }

        [TestMethod]
        public void AddPropertyStaticContentTest()
        {
            PropertyStaticContent expected = new PropertyStaticContent()
            {
                Property = _testProperty,
                ContentCode = 666,
                ContentValue = "AddPropertyStaticContentTest_Value"
            };
            expected.SetContentValue("AddPropertyStaticContentTest_ValueXML");

            _propertiesService.AddPropertyStaticContent(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyStaticContentById(expected.PropertyStaticContentId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetPropertySquareFootageTest()
        {
            string PSCValue = "12345.67";

            PropertyStaticContent propertyStaticContent = new PropertyStaticContent()
            {
                Property = _testProperty,
                ContentCode = (int)PropertyStaticContentType.SquareFootage
            };
            propertyStaticContent.ContentValue = PSCValue;

            _propertiesService.AddPropertyStaticContent(propertyStaticContent);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertySquareFootage(_testProperty.PropertyID);

            Assert.IsNotNull(actual);
            Assert.AreEqual(actual, PSCValue);
        }

        [TestMethod]
        public void RemoveStaticContentTest()
        {
            #region Delete successfully
            PropertyStaticContent deleted = new PropertyStaticContent()
            {
                ContentCode = 666,
                Property = _testProperty
            };

            _propertiesService.AddPropertyStaticContent(deleted);
            _efContext.SaveChanges();

            _propertiesService.RemoveStaticContent(deleted.PropertyStaticContentId);

            var dbPropertyStaticContent = _propertiesService.GetPropertyStaticContentById(deleted.PropertyStaticContentId);

            Assert.IsNull(dbPropertyStaticContent, Messages.ObjectIsNotNull);
            #endregion
        }

        #endregion

        #region Property Associations CRUD

        [TestMethod]
        public void AddPropertyAmenityTest()
        {
            Amenity amenity = _amenitiesService.GetAmenities().First();
            PropertyAmenity expected = new PropertyAmenity()
            {
                Amenity = amenity,
                Property = _testProperty,
                Quantity = amenity.Countable ? (int?)1 : null
            };
            expected.SetDetailsValue("AddPropertyAmenityTest_Details");

            _propertiesService.AddPropertyAmenity(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyAmenityById(expected.PropertyAmenityID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddPropertyTagTest()
        {
            PropertyTag expected = new PropertyTag()
            {
                Tag = _tagsService.GetTags().First(),
                Property = _testProperty
            };

            _propertiesService.AddPropertyTag(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyTagById(expected.PropertyTagID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetPropertyAmenitiesByPropertyIdTest()
        {
            Amenity[] amenities = _amenitiesService.GetAmenities().Take(5).ToArray();
            PropertyAmenity[] newPas = new PropertyAmenity[5];
            Dictionary<int, bool> newPasFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newPas[i] = new PropertyAmenity();

                newPas[i].Amenity = amenities[i];
                newPas[i].Property = _testProperty;
                newPas[i].Quantity = amenities[i].Countable ? (int?)1 : null;
                newPas[i].SetDetailsValue("GetPropertyAmenitiesByPropertyIdTest_Details_" + i);

                _propertiesService.AddPropertyAmenity(newPas[i]);
                _efContext.SaveChanges();

                newPasFound.Add(newPas[i].PropertyAmenityID, false);
            }

            PropertyAmenity expected;
            var actual = _propertiesService.GetPropertyAmenitiesByPropertyId(_testProperty.PropertyID);
            foreach (var actualObj in actual)
            {
                if (newPasFound.ContainsKey(actualObj.PropertyAmenityID))
                {
                    expected = newPas.Where(pa => pa.PropertyAmenityID == actualObj.PropertyAmenityID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newPasFound[actualObj.PropertyAmenityID] = true;
                }
            }

            if (newPasFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "property amenities", "GetPropertyAmenitiesByPropertyId"));
        }

        [TestMethod]
        public void GetPropertyAmenityByIdTest()
        {
            Amenity amenity = _amenitiesService.GetAmenities().First();
            PropertyAmenity expected = new PropertyAmenity()
            {
                Amenity = amenity,
                Property = _testProperty,
                Quantity = amenity.Countable ? (int?)1 : null
            };
            expected.SetDetailsValue("GetPropertyAmenityByIdTest_Details");

            _propertiesService.AddPropertyAmenity(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyAmenityById(expected.PropertyAmenityID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetPropertyAmenityTest()
        {
            Amenity amenity = _amenitiesService.GetAmenities().First();
            PropertyAmenity expected = new PropertyAmenity()
            {
                Amenity = amenity,
                Property = _testProperty,
                Quantity = amenity.Countable ? (int?)1 : null
            };
            expected.SetDetailsValue("GetPropertyAmenityTest_Details");

            _propertiesService.AddPropertyAmenity(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyAmenity(_testProperty.PropertyID, amenity.AmenityID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetPropertyTagByIdTest()
        {
            PropertyTag expected = new PropertyTag()
            {
                Tag = _tagsService.GetTags().First(),
                Property = _testProperty
            };

            _propertiesService.AddPropertyTag(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyTagById(expected.PropertyTagID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetPropertyTagTest()
        {
            Tag tag = _tagsService.GetTags().First();
            PropertyTag expected = new PropertyTag()
            {
                Tag = tag,
                Property = _testProperty
            };

            _propertiesService.AddPropertyTag(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyTag(_testProperty.PropertyID, tag.TagID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetAmenityGroupsByPropertyIdTest()
        {
            AmenityGroup[] amenityGroups = _amenityGroupsService.GetAmenityGroups().Take(3).ToArray();
            Dictionary<int, bool> amenityGroupsFound = new Dictionary<int, bool>();
            PropertyAmenity tmpPa;

            foreach (AmenityGroup ag in amenityGroups)
            {
                tmpPa = new PropertyAmenity()
                {
                    Amenity = ag.Amenities.First(),
                    Property = _testProperty,
                };

                _propertiesService.AddPropertyAmenity(tmpPa);
                _efContext.SaveChanges();

                amenityGroupsFound.Add(ag.AmenityGroupId, false);
            }

            var actual = _propertiesService.GetAmenityGroupsByPropertyId(_testProperty.PropertyID);

            foreach (var obj in actual)
            {
                if (amenityGroupsFound.ContainsKey(obj))
                    amenityGroupsFound[obj] = true;
            }

            if (amenityGroupsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "amenity groups", "GetAmenityGroupsByPropertyId"));
        }

        [TestMethod]
        public void CalculatePropertyCommissionTest()
        {
            DateTime from = new DateTime(2013, 3, 1);
            DateTime until = new DateTime(2013, 3, 15);
            decimal total = 1111.0m;
            int lengthOfStay = (from - until).Days;

            decimal actual = _propertiesService.CalculatePropertyCommission(from, until, total, _testProperty.PropertyID, lengthOfStay);
        }

        [TestMethod]
        public void RemovePropertyAmenityTest()
        {
            #region Delete successfully
            PropertyAmenity deleted = new PropertyAmenity()
            {
                Amenity = _amenitiesService.GetAmenities().First(),
                Property = _testProperty,
            };

            _propertiesService.AddPropertyAmenity(deleted);
            _efContext.SaveChanges();

            _propertiesService.RemovePropertyAmenity(deleted.PropertyAmenityID);

            var dbPropertyAmenity = _propertiesService.GetPropertyAmenityById(deleted.PropertyAmenityID);

            Assert.IsNull(dbPropertyAmenity, Messages.ObjectIsNotNull);
            #endregion
        }

        [TestMethod]
        public void RemovePropertyTagTest()
        {
            #region Delete successfully
            PropertyTag deleted = new PropertyTag()
            {
                Tag = _tagsService.GetTags().First(),
                Property = _testProperty,
            };

            _propertiesService.AddPropertyTag(deleted);
            _efContext.SaveChanges();

            _propertiesService.RemovePropertyTag(deleted.PropertyTagID);

            var dbPropertyTag = _propertiesService.GetPropertyTagById(deleted.PropertyTagID);

            Assert.IsNull(dbPropertyTag, Messages.ObjectIsNotNull);
            #endregion
        }

        [TestMethod]
        public void RemovePropertyTest()
        {
            #region Delete successfully
            Property deleted = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "RemovePropertyTest_PropertyCode_Successfully",
                Address1 = "RemovePropertyTest_Address1_Successfully",
                State = "RemovePropertyTest_State_Successfully",
                ZIPCode = "RemovePropertyTest_ZipCode_Successfully",
                City = "RemovePropertyTest_City_Successfully",
                DictionaryCountry = _testCountry,
                
                
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };
            deleted.SetPropertyNameValue("RemovePropertyTest_PropertyName_Successfully");
            deleted.SetShortDescriptionValue("RemovePropertyTest_ShortDescription_Successfully");
            deleted.SetLongDescriptionValue("RemovePropertyTest_LongDescription_Successfully");

            _propertiesService.AddProperty(deleted);
            _efContext.SaveChanges();

            _propertiesService.RemoveProperty(deleted.PropertyID);

            var dbProperty = _propertiesService.GetPropertyById(deleted.PropertyID);
            Assert.IsNull(dbProperty, Messages.ObjectIsNotNull);
            #endregion

            #region Check referenced property deletion

            Property referencedProperty = new Property()
            {
                User = _testUser,
                Destination = _testDestination,
                PropertyCode = "RemovePropertyTest_PropertyCode_Referenced",
                Address1 = "RemovePropertyTest_Address1_Referenced",
                State = "RemovePropertyTest_State_Referenced",
                ZIPCode = "RemovePropertyTest_ZipCode_Referenced",
                City = "RemovePropertyTest_City_Referenced",
                DictionaryCountry = _testCountry,
                
                
                PropertyType = _testPropertyType,
                DictionaryCulture = _testCulture,
                TimeZone = "Eastern Standard Time"
            };

            referencedProperty.SetPropertyNameValue("RemovePropertyTest_PropertyName_Referenced");
            referencedProperty.SetShortDescriptionValue("RemovePropertyTest_ShortDescription_Referenced");
            referencedProperty.SetLongDescriptionValue("RemovePropertyTest_LongDescription_Referenced");

            _propertiesService.AddProperty(referencedProperty);
            _efContext.SaveChanges();

            UnitType referencedUnitType = new UnitType()
            {
                Property = referencedProperty,
                UnitTypeCode = "RemovePropertyTest_UnitTypeCode_Referenced"
            };
            referencedUnitType.SetTitleValue("RemovePropertyTest_UnitTypeTitle_Referenced");
            referencedUnitType.SetDescValue("RemovePropertyTest_UnitTypeDesc_Referenced");

            _unitTypesService.AddUnitType(referencedUnitType);
            _efContext.SaveChanges();

            bool exceptionCaught = false;
            try
            {
                _propertiesService.RemoveProperty(referencedProperty.PropertyID);
            }
            catch (Exception)
            {
                exceptionCaught = true;
            }

            if (exceptionCaught)
            {
                Assert.Fail(Messages.DeletionShouldNotSucceed);
            }

            #endregion
        }

        [TestMethod]
        public void RemovePropertyAmenitiesByPropertyIdTest()
        {
            Amenity[] amenities = _amenitiesService.GetAmenities().Take(5).ToArray();
            PropertyAmenity[] newPas = new PropertyAmenity[5];

            for (int i = 0; i < 5; ++i)
            {
                newPas[i] = new PropertyAmenity();

                newPas[i].Amenity = amenities[i];
                newPas[i].Property = _testProperty;
                newPas[i].Quantity = amenities[i].Countable ? (int?)1 : null;
                newPas[i].SetDetailsValue("RemovePropertyAmenitiesByPropertyIdTest_Details_" + i);

                _propertiesService.AddPropertyAmenity(newPas[i]);
                _efContext.SaveChanges();
            }

            _propertiesService.RemovePropertyAmenitiesByPropertyId(_testProperty.PropertyID);

            Assert.AreEqual(0, _testProperty.PropertyAmenities.Count);
        }

        [TestMethod]
        public void GetPropertyTagsByPropertyIdTest()
        {
            Tag[] tags = _tagsService.GetTags().Take(5).ToArray();
            PropertyTag[] newPas = new PropertyTag[5];
            Dictionary<int, bool> newPasFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newPas[i] = new PropertyTag();

                newPas[i].Tag = tags[i];
                newPas[i].Property = _testProperty;

                _propertiesService.AddPropertyTag(newPas[i]);
                _efContext.SaveChanges();

                newPasFound.Add(newPas[i].PropertyTagID, false);
            }

            PropertyTag expected;
            var actual = _propertiesService.GetPropertyTagsByPropertyId(_testProperty.PropertyID);
            foreach (var actualObj in actual)
            {
                if (newPasFound.ContainsKey(actualObj.PropertyTagID))
                {
                    expected = newPas.Where(pa => pa.PropertyTagID == actualObj.PropertyTagID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newPasFound[actualObj.PropertyTagID] = true;
                }
            }

            if (newPasFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "property tags", "GetPropertyTagsByPropertyId"));
        }

        #endregion

        #region Floor Plans

        [TestMethod]
        public void GetPropertyFloorPlansTest()
        {
            PropertyStaticContent[] newPscs = new PropertyStaticContent[5];

            FloorPlan testFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "DisplayImagePath",

                FloorPlanNameCurrentLanguage = "FloorPlanNameCurrentLanguage",
                OriginalImagePath = "OriginalImagePath",
            };

            testFloorPlan.SetFloorPlanNameValue("FloorPlanName");

            for (int i = 0; i < 5; ++i)
            {
                newPscs[i] = new PropertyStaticContent()
                {
                    Property = _testProperty,
                    ContentCode = (int)PropertyStaticContentType.FloorPlan,
                    ContentValue = SerializationHelper.SerializeObject(testFloorPlan)
                };
                newPscs[i].SetContentValue(SerializationHelper.SerializeObject(testFloorPlan));
                _propertiesService.AddPropertyStaticContent(newPscs[i]);
                _efContext.SaveChanges();
            }

            PropertyStaticContent expected;
            List<FloorPlan> actual = _propertiesService.GetPropertyFloorPlans(_testProperty.PropertyID).ToList();

            foreach (var actualObj in actual)
            {
                expected = newPscs.Where(psc => psc.ContentValue.Equals(SerializationHelper.SerializeObject(actualObj))).FirstOrDefault();
                Assert.IsNotNull(expected);
            }
        }

        [TestMethod]
        public void AddPropertyFloorPlanTest()
        {
            FloorPlan testFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "DisplayImagePath",

                FloorPlanNameCurrentLanguage = "FloorPlanNameCurrentLanguage",
                OriginalImagePath = "OriginalImagePath",
            };
            testFloorPlan.SetFloorPlanNameValue("FloorPlanName");
            _propertiesService.AddPropertyFloorPlan(_testProperty.PropertyID, testFloorPlan);


            PropertyStaticContent actual = _testProperty.PropertyStaticContents.SingleOrDefault();


            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(SerializationHelper.SerializeObject(testFloorPlan), actual.ContentValue);
            Assert.AreEqual((int)PropertyStaticContentType.FloorPlan, actual.ContentCode);
        }

        [TestMethod]
        public void EditPropertyFloorPlanTest()
        {
            FloorPlan testFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "DisplayImagePath",
                FloorPlanNameCurrentLanguage = "FloorPlanNameCurrentLanguage",
                OriginalImagePath = "OriginalImagePath",
            };
            testFloorPlan.SetFloorPlanNameValue("FloorPlanName");
            _propertiesService.AddPropertyFloorPlan(_testProperty.PropertyID, testFloorPlan);

            FloorPlan newFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "ChangedDisplayImagePath",
                FloorPlanNameCurrentLanguage = "ChangedFloorPlanNameCurrentLanguage",
                OriginalImagePath = "ChangedOriginalImagePath",
            };


            _propertiesService.EditPropertyFloorPlan(_testProperty.PropertyStaticContents.SingleOrDefault().PropertyStaticContentId, newFloorPlan);


            PropertyStaticContent actual = _testProperty.PropertyStaticContents.SingleOrDefault();

            Assert.IsNotNull(actual, Messages.ObjectIsNull);
            Assert.AreEqual(SerializationHelper.SerializeObject(newFloorPlan), actual.ContentValue);
        }

        [TestMethod]
        public void AddPanoramaToPropertyFloorPlanTest()
        {
            Panorama testPanorama = new Panorama()
            {
                OffsetX = 123,
                OffsetY = -123,
                // (?)
                PanoramaNameCurrentLanguage = null,
                PanoramaPath = "PanoramaPath",

            };

            testPanorama.SetPanoramaNameValue("PanoramaName");

            FloorPlan testFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "DisplayImagePath",
                FloorPlanNameCurrentLanguage = "FloorPlanNameCurrentLanguage",
                OriginalImagePath = "OriginalImagePath",
            };
            testFloorPlan.SetFloorPlanNameValue("FloorPlanName");
            _propertiesService.AddPropertyFloorPlan(_testProperty.PropertyID, testFloorPlan);

            _propertiesService.AddPanoramaToPropertyFloorPlan(_testProperty.PropertyStaticContents.SingleOrDefault().PropertyStaticContentId, testPanorama);

            Panorama actual = (SerializationHelper.DeserializeObject<FloorPlan>(_testProperty.PropertyStaticContents.SingleOrDefault().ContentValue)).Panoramas[0];

            ExtendedAssert.AreEqual(testPanorama, actual);
        }

        [TestMethod]
        public void EditPropertyFloorPlansPanoramaTest()
        {
            Panorama testPanorama = new Panorama()
            {
                OffsetX = 123,
                OffsetY = -123,
                // (?)
                PanoramaNameCurrentLanguage = null,
                PanoramaPath = "PanoramaPath",
            };
            testPanorama.SetPanoramaNameValue("PanoramaName");

            Panorama modPanorama = new Panorama()
            {
                OffsetX = 1,
                OffsetY = -6,
                // (?)
                Id = 1,
                PanoramaNameCurrentLanguage = null,
                PanoramaPath = "PanoramaPathModified",
            };
            modPanorama.SetPanoramaNameValue("PanoramaNameModified");

            FloorPlan testFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "DisplayImagePath",
                FloorPlanNameCurrentLanguage = "FloorPlanNameCurrentLanguage",
                OriginalImagePath = "OriginalImagePath",
            };
            testFloorPlan.SetFloorPlanNameValue("FloorPlanName");
            _propertiesService.AddPropertyFloorPlan(_testProperty.PropertyID, testFloorPlan);
            _propertiesService.AddPanoramaToPropertyFloorPlan(_testProperty.PropertyStaticContents.SingleOrDefault().PropertyStaticContentId, testPanorama);

            _propertiesService.EditPropertyFloorPlansPanorama(_testProperty.PropertyStaticContents.SingleOrDefault().PropertyStaticContentId, modPanorama);

            Panorama actual = (SerializationHelper.DeserializeObject<FloorPlan>(_testProperty.PropertyStaticContents.SingleOrDefault().ContentValue)).Panoramas[0];

            ExtendedAssert.AreEqual(modPanorama, actual);
        }

        [TestMethod]
        public void DeletePropertyFloorPlansPanoramaTest()
        {
            Panorama testPanorama = new Panorama()
            {
                OffsetX = 123,
                OffsetY = -123,
                PanoramaPath = "PanoramaPath",
            };

            testPanorama.SetPanoramaNameValue("PanoramaName");

            FloorPlan testFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "DisplayImagePath",
                FloorPlanNameCurrentLanguage = "FloorPlanNameCurrentLanguage",
                OriginalImagePath = "OriginalImagePath",
            };
            testFloorPlan.SetFloorPlanNameValue("FloorPlanName");
            _propertiesService.AddPropertyFloorPlan(_testProperty.PropertyID, testFloorPlan);
            _propertiesService.AddPanoramaToPropertyFloorPlan(_testProperty.PropertyStaticContents.SingleOrDefault().PropertyStaticContentId, testPanorama);

            _propertiesService.DeletePropertyFloorPlansPanorama(_testProperty.PropertyStaticContents.SingleOrDefault().PropertyStaticContentId, 1);

            Assert.AreEqual(0, (SerializationHelper.DeserializeObject<FloorPlan>(_testProperty.PropertyStaticContents.SingleOrDefault().ContentValue)).Panoramas.Count());
        }

        //BlobService is used
        //[TestMethod]
        //public void DeletePropertyFloorPlanTest()
        //{
        //    FloorPlan testFloorPlan = new FloorPlan()
        //    {
        //        DisplayImagePath = "DisplayImagePath",
        //        FloorPlanNameCurrentLanguage = "FloorPlanNameCurrentLanguage",
        //        OriginalImagePath = "OriginalImagePath",
        //    };
        //    testFloorPlan.SetFloorPlanNameValue(LanguageCode.en, "FloorPlanName");
        //    _propertiesService.AddPropertyFloorPlan(_testProperty.PropertyID, testFloorPlan);

        //    _propertiesService.DeletePropertyFloorPlan(_testProperty.PropertyStaticContents.ElementAt(0).PropertyStaticContentId);

        //    Assert.AreEqual(0, _testProperty.PropertyStaticContents.Count());
        //}

        #endregion

        #region Directions To Airoprt

        [TestMethod]
        public void GetPropertyDirectionsToAirportTest()
        {
            DirectionToAirport[] directions = new DirectionToAirport[5];

            for (int i = 0; i < 5; i++)
            {
                directions[i] = new DirectionToAirport()
                {
                    DirectionLink = "DirectionLink" + i,
                };
                directions[i].SetName("Name" + i);

                var psc = new PropertyStaticContent()
                {
                    ContentCode = (int)PropertyStaticContentType.DirectionToAirport,
                    ContentValue = SerializationHelper.SerializeObject(directions[i]),
                    Property = _testProperty,
                };
                psc.SetContentValue(SerializationHelper.SerializeObject(directions[i]));
                psc.PersistI18nValues();

                _testProperty.PropertyStaticContents.Add(psc);
            }
            _efContext.SaveChanges();

            List<DirectionToAirport> actual = _propertiesService.GetPropertyDirectionsToAirport(_testProperty.PropertyID).ToList();

            for (int i = 0; i < 5; i++)
            {
                Assert.AreEqual(directions[i].DirectionLink, actual[i].DirectionLink);
                Assert.AreEqual(directions[i].Name.i18nContent.First().Code, actual[i].Name.i18nContent.First().Code);
                Assert.AreEqual(directions[i].Name.i18nContent.First().Content, actual[i].Name.i18nContent.First().Content);
            }

        }

        #endregion

        #region Raw Data

        [TestMethod]
        public void GetPropertyRawDatasTest()
        {
            PropertyRawData[] newRawDatas = new PropertyRawData[5];
            Dictionary<int, bool> newRawDatasFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newRawDatas[i] = new PropertyRawData()
                {
                    Property = _testProperty,
                    RawRecordType = (int)PropertyRawRecordType.Image_ListView,
                    RecordName = "GetPropertyRawDatasTest_RecordName_" + i,
                    Description = "GetPropertyRawDatasTest_Description_" + i,
                    RawRecordContent = "GetPropertyRawDatasTest_Content_" + i
                };
                _propertiesService.AddPropertyRawData(newRawDatas[i]);
                _efContext.SaveChanges();

                newRawDatasFound.Add(newRawDatas[i].PropertyRawDataRecordID, false);
            }

            PropertyRawData expected;
            var actual = _propertiesService.GetPropertyRawDatas(_testProperty.PropertyID);
            foreach (var actualObj in actual)
            {
                if (newRawDatasFound.ContainsKey(actualObj.PropertyRawDataRecordID))
                {
                    expected = newRawDatas.Where(p => p.PropertyRawDataRecordID == actualObj.PropertyRawDataRecordID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newRawDatasFound[actualObj.PropertyRawDataRecordID] = true;
                }
            }

            if (newRawDatasFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "property raw datas", "GetPropertyRawDatas"));
        }

        [TestMethod]
        public void GetPropertyRawDataByIdTest()
        {
            PropertyRawData expected = new PropertyRawData()
            {
                Property = _testProperty,
                RawRecordType = (int)PropertyRawRecordType.Image_ListView,
                Description = "GetPropertyRawDataByIdTest_Description",
                RecordName = "GetPropertyRawDataByIdTest_RecordName",
                RawRecordContent = "GetPropertyRawDataByIdTest_Content"
            };

            _propertiesService.AddPropertyRawData(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyRawDataById(expected.PropertyRawDataRecordID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetPropertyRawDatasTestWithoutId()
        {
            PropertyRawData[] newRawDatas = new PropertyRawData[5];
            Dictionary<int, bool> newRawDatasFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newRawDatas[i] = new PropertyRawData()
                {
                    Property = (i % 2 > 0) ? _testProperty : _testProperty2,
                    RawRecordType = (int)PropertyRawRecordType.Image_ListView,
                    RecordName = "GetPropertyRawDatasTestWithoutId_RecordName_" + i,
                    Description = "GetPropertyRawDatasTestWithoutId_Description_" + i,
                    RawRecordContent = "GetPropertyRawDatasTestWithoutId_Content_" + i
                };
                _propertiesService.AddPropertyRawData(newRawDatas[i]);
                _efContext.SaveChanges();

                newRawDatasFound.Add(newRawDatas[i].PropertyRawDataRecordID, false);
            }

            PropertyRawData expected;
            var actual = _propertiesService.GetPropertyRawDatas();
            foreach (var actualObj in actual)
            {
                if (newRawDatasFound.ContainsKey(actualObj.PropertyRawDataRecordID))
                {
                    expected = newRawDatas.Where(p => p.PropertyRawDataRecordID == actualObj.PropertyRawDataRecordID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newRawDatasFound[actualObj.PropertyRawDataRecordID] = true;
                }
            }

            if (newRawDatasFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "property raw datas", "GetPropertyRawDatas"));
        }

        [TestMethod]
        public void AddPropertyRawDataTest()
        {
            PropertyRawData expected = new PropertyRawData()
            {
                Property = _testProperty,
                RawRecordType = (int)PropertyRawRecordType.Image_ListView,
                Description = "AddPropertyRawDataTest_Description",
                RecordName = "AddPropertyRawDataTest_RecordName",
                RawRecordContent = "AddPropertyRawDataTest_Content"
            };

            _propertiesService.AddPropertyRawData(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetPropertyRawDataById(expected.PropertyRawDataRecordID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        //BlobService is used
        //[TestMethod]
        //public void DeletePropertyRawDataTest()
        //{
        //    #region Delete successfully
        //    PropertyRawData deleted = new PropertyRawData()
        //    {
        //        Property = _testProperty,
        //        RawRecordType = (int)PropertyRawRecordType.Image_ListView,
        //        Description = "DeletePropertyRawDataTest_Description_Successfully",
        //        RecordName = "DeletePropertyRawDataTest_RecordName_Successfully",
        //        RawRecordContent = "DeletePropertyRawDataTest_Content_Successfully"
        //    };

        //    _propertiesService.AddPropertyRawData(deleted);
        //    _efContext.SaveChanges();

        //    _propertiesService.DeletePropertyRawData(deleted.PropertyRawDataRecordID);

        //    var dbRawData = _propertiesService.GetPropertyRawDataById(deleted.PropertyRawDataRecordID);

        //    Assert.IsNull(dbRawData, Messages.ObjectIsNotNull);
        //    #endregion
        //}

        #endregion

        #region Content Update Requests & Details

        [TestMethod]
        public void GetContentUpdateRequestByIdTest()
        {
            ContentUpdateRequest expected = new ContentUpdateRequest()
            {
                Property = _testProperty,
                RequestDateTime = DateTime.Now,
                Description = "GetPropertyRawDataByIdTest_Description"
            };

            _propertiesService.AddContentUpdateRequest(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetContentUpdateRequestById(expected.ContentUpdateRequestId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetContentUpdateRequestDetailById()
        {
            ContentUpdateRequestDetail expected = new ContentUpdateRequestDetail()
            {
                ContentUpdateRequest = _testRequest,
                ContentCode = (int)ContentUpdateRequestDetailContentType.Image,
                ContentValue = "GetContentUpdateRequestDetailById_Value",
                ContentValueXml = "<xml>GetContentUpdateRequestDetailById_XML</xml>"
            };

            _propertiesService.AddContentUpdateRequestDetail(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetContentUpdateRequestDetailById(expected.ContentUpdateRequestDetailId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetContentUpdateRequestsTest()
        {
            ContentUpdateRequest[] newRequests = new ContentUpdateRequest[5];
            Dictionary<int, bool> newRequestsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newRequests[i] = new ContentUpdateRequest()
                {
                    Property = _testProperty,
                    RequestDateTime = DateTime.Now,
                    Description = "GetContentUpdateRequestsTest_Description_" + i
                };
                _propertiesService.AddContentUpdateRequest(newRequests[i]);
                _efContext.SaveChanges();

                newRequestsFound.Add(newRequests[i].ContentUpdateRequestId, false);
            }

            ContentUpdateRequest expected;
            var actual = _propertiesService.GetContentUpdateRequests();
            foreach (var actualObj in actual)
            {
                if (newRequestsFound.ContainsKey(actualObj.ContentUpdateRequestId))
                {
                    expected = newRequests.Where(p => p.ContentUpdateRequestId == actualObj.ContentUpdateRequestId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newRequestsFound[actualObj.ContentUpdateRequestId] = true;
                }
            }

            if (newRequestsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "content update requests", "GetContentUpdateRequests"));
        }

        [TestMethod]
        public void GetContentUpdateRequestDetailsByRequestIdTest()
        {
            ContentUpdateRequestDetail[] newDetails = new ContentUpdateRequestDetail[5];
            Dictionary<int, bool> newDetailsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newDetails[i] = new ContentUpdateRequestDetail()
                {
                    ContentUpdateRequest = _testRequest,
                    ContentCode = (int)ContentUpdateRequestDetailContentType.Image,
                    ContentValue = "GetContentUpdateRequestDetailsByRequestIdTest_Value_" + i,
                    ContentValueXml = "<xml>GetContentUpdateRequestDetailsByRequestIdTest_XML_" + i + "</xml>"
                };
                _propertiesService.AddContentUpdateRequestDetail(newDetails[i]);
                _efContext.SaveChanges();

                newDetailsFound.Add(newDetails[i].ContentUpdateRequestDetailId, false);
            }

            ContentUpdateRequestDetail expected;
            var actual = _propertiesService.GetContentUpdateRequestDetailsByRequestId(_testRequest.ContentUpdateRequestId);
            foreach (var actualObj in actual)
            {
                if (newDetailsFound.ContainsKey(actualObj.ContentUpdateRequestDetailId))
                {
                    expected = newDetails.Where(p => p.ContentUpdateRequestDetailId == actualObj.ContentUpdateRequestDetailId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newDetailsFound[actualObj.ContentUpdateRequestDetailId] = true;
                }
            }

            if (newDetailsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "content update request details", "GetContentUpdateRequestDetailsByRequestId"));
        }

        [TestMethod]
        public void GetContentUpdateRequestDetailsTest()
        {
            ContentUpdateRequestDetail[] newDetails = new ContentUpdateRequestDetail[5];
            Dictionary<int, bool> newDetailsFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newDetails[i] = new ContentUpdateRequestDetail()
                {
                    ContentUpdateRequest = (i % 2 > 0) ? _testRequest : _testRequest2,
                    ContentCode = (int)ContentUpdateRequestDetailContentType.Image,
                    ContentValue = "GetContentUpdateRequestDetailsTest_Value_" + i,
                    ContentValueXml = "<xml>GetContentUpdateRequestDetailsTest_XML_" + i + "</xml>"
                };
                _propertiesService.AddContentUpdateRequestDetail(newDetails[i]);
                _efContext.SaveChanges();

                newDetailsFound.Add(newDetails[i].ContentUpdateRequestDetailId, false);
            }

            ContentUpdateRequestDetail expected;
            var actual = _propertiesService.GetContentUpdateRequestDetails();
            foreach (var actualObj in actual)
            {
                if (newDetailsFound.ContainsKey(actualObj.ContentUpdateRequestDetailId))
                {
                    expected = newDetails.Where(p => p.ContentUpdateRequestDetailId == actualObj.ContentUpdateRequestDetailId).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newDetailsFound[actualObj.ContentUpdateRequestDetailId] = true;
                }
            }

            if (newDetailsFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "content update request details", "GetContentUpdateRequestDetails"));
        }

        [TestMethod]
        public void AddContentUpdateRequestTest()
        {
            ContentUpdateRequest expected = new ContentUpdateRequest()
            {
                Property = _testProperty,
                RequestDateTime = DateTime.Now,
                Description = "AddContentUpdateRequestTest_Description"
            };

            _propertiesService.AddContentUpdateRequest(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetContentUpdateRequestById(expected.ContentUpdateRequestId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddContentUpdateRequestDetailTest()
        {
            ContentUpdateRequestDetail expected = new ContentUpdateRequestDetail()
            {
                ContentUpdateRequest = _testRequest,
                ContentCode = (int)ContentUpdateRequestDetailContentType.Image,
                ContentValue = "AddContentUpdateRequestDetailTest_Value",
                ContentValueXml = "<xml>AddContentUpdateRequestDetailTest_XML</xml>"
            };

            _propertiesService.AddContentUpdateRequestDetail(expected);
            _efContext.SaveChanges();

            var actual = _propertiesService.GetContentUpdateRequestDetailById(expected.ContentUpdateRequestDetailId);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddContentUpdateRequestDetailWithParamsTest()
        {
            ContentUpdateRequestDetail expected = new ContentUpdateRequestDetail()
            {
                ContentUpdateRequest = _testRequest,
                ContentCode = (int)ContentUpdateRequestDetailContentType.Image,
                ContentValue = String.Format("{0}/id{1}/{2}", _settingsService.GetSettingValue(BusinessLogic.Enums.SettingKeyName.ContainersContentUpdateRequests), _testRequest.Property.PropertyID, "house2")
            };

            int addedId = _propertiesService.AddContentUpdateRequestDetail(_testRequest, ContentUpdateRequestDetailContentType.Image, "house2");
            _efContext.SaveChanges();

            var actual = _propertiesService.GetContentUpdateRequestDetailById(addedId);

            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ContentCode, actual.ContentCode);
            Assert.AreEqual(expected.ContentUpdateRequest, actual.ContentUpdateRequest);
            Assert.AreEqual(expected.ContentValue, actual.ContentValue);
            Assert.AreEqual(expected.ContentValueXml, actual.ContentValueXml);
        }

        [TestMethod]
        public void DeleteContentUpdateRequestTest()
        {
            #region Delete successfully
            ContentUpdateRequest deleted = new ContentUpdateRequest()
            {
                Property = _testProperty,
                RequestDateTime = DateTime.Now,
                Description = "DeleteContentUpdateRequestTest_Description_Successfully"
            };

            _propertiesService.AddContentUpdateRequest(deleted);
            _efContext.SaveChanges();

            _propertiesService.DeleteContentUpdateRequest(deleted.ContentUpdateRequestId);

            var dbRequest = _propertiesService.GetContentUpdateRequestById(deleted.ContentUpdateRequestId);

            Assert.IsNull(dbRequest, Messages.ObjectIsNotNull);
            #endregion
        }

        [TestMethod]
        public void DeleteContentUpdateRequestDetailTest()
        {
            #region Delete successfully
            ContentUpdateRequestDetail deleted = new ContentUpdateRequestDetail()
            {
                ContentUpdateRequest = _testRequest,
                ContentCode = (int)ContentUpdateRequestDetailContentType.Image,
                ContentValue = "DeleteContentUpdateRequestDetailTest_Value_Successfully",
                ContentValueXml = "<xml>DeleteContentUpdateRequestDetailTest_XML_Successfully</xml>"
            };

            _propertiesService.AddContentUpdateRequestDetail(deleted);
            _efContext.SaveChanges();

            _propertiesService.DeleteContentUpdateRequestDetail(deleted.ContentUpdateRequestDetailId);

            var dbDetail = _propertiesService.GetContentUpdateRequestDetailById(deleted.ContentUpdateRequestDetailId);

            Assert.IsNull(dbDetail, Messages.ObjectIsNotNull);
            #endregion
        }

        #endregion

        #region Property Time Zone

        [TestMethod]
        public void GetPropertyLocalTimeTest()
        {
            List<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones().ToList();
            DateTime currentTime = DateTime.UtcNow;

            foreach (TimeZoneInfo tz in timeZones)
            {
                _testProperty.TimeZone = tz.Id;
                _efContext.SaveChanges();

                DateTime actual = _propertiesService.GetPropertyLocalTime(currentTime, _testProperty.PropertyID);

                Assert.AreEqual(TimeZoneInfo.ConvertTimeFromUtc(currentTime, tz), actual);
            }
        }

        #endregion

        #endregion
    }
}
