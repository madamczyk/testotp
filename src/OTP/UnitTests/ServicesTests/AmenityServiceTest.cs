﻿using BusinessLogic.ServiceContracts;
using BusinessLogic.Services;
using DataAccess;
using DataAccess.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UnitTests.ServicesTests
{
    [TestClass]
    public class AmenityServiceTest
    {
        #region Fields
        /// <summary>
        /// amenity group for test methods
        /// </summary>
        private AmenityGroup _testAmenityGroup;
        /// <summary>
        /// list of amenities used in test methods
        /// </summary>
        private List<Amenity> _testAmenitites = new List<Amenity>();
        /// <summary>
        /// db context
        /// </summary>
        private OTPEntities _efContext;
        #endregion

        #region Services
        private IAmenityGroupsService _amenityGroupsService;
        private IAmenityService _amenitiesService;
        private IPropertiesService _propertiesService;
        private IDestinationsService _destinationService;
        private IDictionaryCountryService _countryService;
        private IPropertyTypesService _propertyTypeService;
        private IUserService _userService;
        private IBlobService _blobService;
        private IDictionaryCultureService _dictionaryCultureService;
        private IZohoCrmService _zohoCrmService = null;
        private IMessageService _messageService = null;
        private ICrmOperationLogsService _crmOperationLogsService = null;
        private ISettingsService _settingsService = null;
        #endregion

        #region Prepare / Cleanup data
        /// <summary>
        /// Prepare the data before each test
        /// </summary>
        [TestInitialize]
        public void PrepareData()
        {
            #region Setup Services
            _efContext = new OTPEntities(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);

            var mock = new Mock<IContextService>();
            mock.Setup(f => f.GetFromContext("EFContext")).Returns(_efContext);
            mock.Setup(f => f.Current).Returns(new object());

            IConfigurationService configurationService = new ConfigurationService(mock.Object);

            _crmOperationLogsService = new CrmOperationLogsService(mock.Object);
            _settingsService = new SettingsService(_efContext);
            _messageService = new MessageService(mock.Object, _settingsService);
            _zohoCrmService = new ZohoCrmService(_efContext, _settingsService, _crmOperationLogsService);
            _blobService = new BlobService(mock.Object);
            _amenitiesService = new AmenityService(mock.Object);
            _amenityGroupsService = new AmenityGroupsService(mock.Object);
            _propertiesService = new PropertiesServices(configurationService, _blobService, _settingsService, mock.Object);
            _destinationService = new DestinationsService(configurationService, mock.Object);
            _propertyTypeService = new PropertyTypeService(mock.Object);
            _countryService = new DictionaryCountryService(mock.Object);
            _userService = new UsersService(mock.Object, _zohoCrmService, _messageService, _settingsService);
            _dictionaryCultureService = new DictionaryCultureService(mock.Object);
            #endregion

            _testAmenityGroup = _amenityGroupsService.GetAmenityGroups().First();
        }

        /// <summary>
        /// Cleanup the data after each test
        /// </summary>
        [TestCleanup]
        public void CleanupData()
        {
            foreach (Amenity a in _testAmenitites)
            {
                if (_amenitiesService.GetAmenityById(a.AmenityID) != null)
                    _amenitiesService.DeleteAmenity(a.AmenityID);
            }
            _testAmenitites.Clear();
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void GetAmenityByIdTest()
        {
            Amenity expected = new Amenity()
            {
                AmenityGroup = _testAmenityGroup,
                AmenityCode = "GetAmenityByIdTest_Code",
                Countable = true
            };
            expected.SetTitleValue("GetAmenityByIdTest_Title");
            expected.SetDescriptionValue("GetAmenityByIdTest_Description");

            _amenitiesService.AddAmenity(expected);
            _efContext.SaveChanges();
            _testAmenitites.Add(expected);

            var actual = _amenitiesService.GetAmenityById(expected.AmenityID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddAmenityTest()
        {
            Amenity expected = new Amenity()
            {
                AmenityGroup = _testAmenityGroup,
                AmenityCode = "AddAmenityTest_Code",
                Countable = true
            };
            expected.SetTitleValue("AddAmenityTest_Title");
            expected.SetDescriptionValue("AddAmenityTest_Description");
       
            _amenitiesService.AddAmenity(expected);
            _efContext.SaveChanges();
            _testAmenitites.Add(expected);

            var actual = _amenitiesService.GetAmenityById(expected.AmenityID);

            Assert.IsNotNull(actual);
            ExtendedAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddAmenityRequiredFieldsTest()
        {
            Amenity amenity;

            #region Test field AmenityCode
            amenity = new Amenity()
            {

                AmenityGroup = _testAmenityGroup,
                Countable = true
            };
            amenity.SetTitleValue("AddAmenityRequiredFieldsTest_AmenityCode_Title");
            amenity.SetDescriptionValue("AddAmenityRequiredFieldsTest_AmenityCode_Description");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _amenitiesService.AddAmenity(amenity);
                _efContext.SaveChanges();
            }, "AmenityCode");
            _efContext.Amenities.Remove(amenity);
            _efContext.SaveChanges();
            #endregion

            #region Test field AmenityTitle_i18n
            amenity = new Amenity()
            {
                AmenityCode = "AddAmenityRequiredFieldsTest_Title_Code",
                AmenityGroup = _testAmenityGroup,
                Countable = true
            };
            amenity.SetDescriptionValue("AddAmenityRequiredFieldsTest_AmenityCode_Description");

            ExtendedAssert.ThrowsdbEntityValidationExcepion(() =>
            {
                _amenitiesService.AddAmenity(amenity);
                _efContext.SaveChanges();
            }, "AmenityTitle_i18n");
            _efContext.Amenities.Remove(amenity);
            _efContext.SaveChanges();
            #endregion

            #region Test field Description_i18n
            // Description_i18n is nullable
            #endregion

            #region Test field AmenityGroup
            amenity = new Amenity()
            {
                AmenityCode = "AddAmenityRequiredFieldsTest_AmenityGroup_Code",
                Countable = true
            };
            amenity.SetTitleValue("AddAmenityRequiredFieldsTest_AmenityGroup_Title");
            amenity.SetDescriptionValue("AddAmenityRequiredFieldsTest_AmenityCode_Description");

            ExtendedAssert.ThrowsdbUpdateException(() =>
            {
                _amenitiesService.AddAmenity(amenity);
                _efContext.SaveChanges();
            }, "FK_Amenities_AmenityGroup");
            _efContext.Amenities.Remove(amenity);
            _efContext.SaveChanges();
            #endregion

            #region Test field Countable
            // Countable is of type boolean and cannot be nullified
            #endregion
        }

        [TestMethod]
        public void GetAmenitiesTest()
        {
            Amenity[] newAmenities = new Amenity[5];
            Dictionary<int, bool> newAmenitiesFound = new Dictionary<int, bool>();

            for (int i = 0; i < 5; ++i)
            {
                newAmenities[i] = new Amenity()
                {
                    AmenityGroup = _testAmenityGroup,
                    AmenityCode = "GetAmenitiesTest_Code_" + i,
                    Countable = true
                };
                newAmenities[i].SetTitleValue("GetAmenitiesTest_Title_" + i);
                newAmenities[i].SetDescriptionValue("GetAmenitiesTest_Description_" + i);

                _amenitiesService.AddAmenity(newAmenities[i]);
                _efContext.SaveChanges();
                _testAmenitites.Add(newAmenities[i]);

                newAmenitiesFound.Add(newAmenities[i].AmenityID, false);
            }

            Amenity expected;
            var actual = _amenitiesService.GetAmenities();
            foreach (var actualObj in actual)
            {
                if (newAmenitiesFound.ContainsKey(actualObj.AmenityID))
                {
                    expected = newAmenities.Where(a => a.AmenityID == actualObj.AmenityID).FirstOrDefault();

                    Assert.IsNotNull(expected);
                    ExtendedAssert.AreEqual(expected, actualObj);

                    newAmenitiesFound[actualObj.AmenityID] = true;
                }
            }

            if (newAmenitiesFound.Any(na => na.Value == false))
                Assert.Fail(String.Format(Messages.NotPresentInCollection, "amenities", "GetAmenities"));
        }

        [TestMethod]
        public void DeleteAmenityTest()
        {
            #region Delete successfully
            Amenity deleted = new Amenity()
            {
                AmenityGroup = _testAmenityGroup,
                AmenityCode = "DeleteAmenityTest_Code_Successfully",
            };
            deleted.SetTitleValue("DeleteAmenityTest_Title_Successfully");

            _amenitiesService.AddAmenity(deleted);
            _efContext.SaveChanges();

            _amenitiesService.DeleteAmenity(deleted.AmenityID);

            var dbAmenity = _amenitiesService.GetAmenityById(deleted.AmenityID);

            Assert.IsNull(dbAmenity, Messages.ObjectIsNotNull);
            #endregion
            
            #region Check referenced amenity deletion
            DictionaryCountry referencedCountry = _countryService.GetCountries().First();
            Destination referencedDestination = _destinationService.GetDestinations().First();
            PropertyType referencedPropertyType = _propertyTypeService.GetPropertyTypes().First();

            Amenity referencedAmenity = new Amenity()
            {
                AmenityGroup = _testAmenityGroup,
                AmenityCode = "DeleteAmenityTest_Code_Referenced",
            };
            referencedAmenity.SetTitleValue("DeleteAmenityTest_Title_Referenced");

            _amenitiesService.AddAmenity(referencedAmenity);

            User referencedUser = new User()
            {
                Firstname = "DeleteAmenityTest_Firstname_Referenced",
                Lastname = "DeleteAmenityTest_Lastname_Referenced",
                email = "DeleteAmenityTest_E-mail_Referenced",
                Password = "SomePassword",
                IsGeneratedPassword = false,
                DictionaryCulture = _dictionaryCultureService.GetCultures().First()
            };

            _userService.AddUser(referencedUser);

            Property referencedProperty = new Property()
            {
                User = referencedUser,
                Destination = referencedDestination,
                PropertyCode = "DeleteAmenityTest_PropertyCode_Referenced",
                Address1 = "DeleteAmenityTest_Address1_Referenced",
                State = "DeleteAmenityTest_State_Referenced",
                ZIPCode = "DeleteAmenityTest_ZipCode_Referenced",
                City = "DeleteAmenityTest_City_Referenced",
                DictionaryCountry = referencedCountry,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = referencedPropertyType,
                DictionaryCulture = _dictionaryCultureService.GetCultures().First(),
                TimeZone = "TZ"
            };
            referencedProperty.SetPropertyNameValue("DeleteAmenityTest_PropertyName_Referenced");
            referencedProperty.SetShortDescriptionValue("DeleteAmenityTest_ShortDescription_Referenced");
            referencedProperty.SetLongDescriptionValue("DeleteAmenityTest_LongDescription_Referenced");

            _propertiesService.AddProperty(referencedProperty);

            PropertyAmenity propertyAmenity = new PropertyAmenity()
            {
                Amenity = referencedAmenity,
                Property = referencedProperty,
                Quantity = 1,
            };
            propertyAmenity.SetDetailsValue("DeleteAmenityTest_Details_Referenced");

            _propertiesService.AddPropertyAmenity(propertyAmenity);

            _efContext.SaveChanges();

            try
            {
                _amenitiesService.DeleteAmenity(referencedAmenity.AmenityID);
            }
            catch (Exception ex)
            {
                Assert.Fail(String.Format(Messages.DeletionShouldSucceed, ex.GetType()));
            }

            _propertiesService.RemoveProperty(referencedProperty.PropertyID);
            _userService.DeleteUserById(referencedUser.UserID);

            #endregion
        }
        #endregion
    }
}
