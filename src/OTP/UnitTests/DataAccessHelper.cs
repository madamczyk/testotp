﻿using BusinessLogic.Services;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace UnitTests
{
    class DataAccessHelper
    {
        internal static Services GetServices()
        {
            return new Services(ConfigurationManager.ConnectionStrings["OTPTest"].ConnectionString);
        }

        internal static void RemoveMessage(OTPEntities efContext, Message msg)
        {
            if (msg != null)
            {
                List<Attachment> temp = msg.Attachments.ToList();

                foreach (Attachment a in temp)
                {
                    if (a != null) efContext.Attachments.Remove(a);
                }

                msg.Attachments.Clear();
                efContext.Messages.Remove(msg);
                efContext.SaveChanges();
            }
        }

        internal static void RemoveReservation(OTPEntities efContext, Reservation reservation)
        {
            if (reservation != null)
            {
                List<ReservationTransaction> tempRT = reservation.ReservationTransactions.ToList();
                List<ReservationBillingAddress> tempBA = reservation.ReservationBillingAddresses.ToList();

                foreach (ReservationTransaction rt in tempRT)
                {
                    if (rt != null) efContext.ReservationTransactions.Remove(rt);
                }

                reservation.ReservationTransactions.Clear();

                foreach (ReservationBillingAddress ba in tempBA)
                {
                    efContext.ReservationBillingAddresses.Remove(ba);
                }
                reservation.ReservationBillingAddresses.Clear();





                efContext.Reservations.Remove(reservation);
                efContext.SaveChanges();

            }

            //foreach (var logEntry in reservation.PaymentGatewayOperationsLogs)
            //{
            //    logEntry.Reservation = null;
            //}
            //reservation.PaymentGatewayOperationsLogs.Clear();

            ////clearing billing addresses for reservation
            //foreach (var billingAddress in reservation.ReservationBillingAddresses)
            //{
            //    efContext.ReservationBillingAddresses.Remove(billingAddress);
            //}
            //reservation.ReservationBillingAddresses.Clear();

            //clearing reservation transactions for reservation
            //int c = reservation.ReservationTransactions.Count();
            //for (int i = 0; i < c; i++)
            //    efContext.ReservationTransactions.Remove(reservation.ReservationTransactions.ElementAt(i));
            //reservation.ReservationTransactions.Clear();

            ////clearing reservation details for reservation
            //foreach (var reservationDetails in reservation.ReservationDetails)
            //{
            //    efContext.ReservationDetails.Remove(reservationDetails);
            //}
            //reservation.ReservationDetails.Clear();

            ////clearing guest reviews
            //foreach (var guestReview in reservation.GuestReviews)
            //{
            //    efContext.GuestReviews.Remove(guestReview);
            //}
            //reservation.GuestReviews.Clear();
        }

        internal static Role GetRoleById(OTPEntities efContext, int RoleId)
        {
            if (RoleId >= 1 && RoleId <= 7) return efContext.Roles.Where(r => r.RoleId == RoleId).SingleOrDefault();
            else return null;
        }

        internal static void AddGuestPaymentMethod(OTPEntities efContext, GuestPaymentMethod gpm)
        {
            efContext.GuestPaymentMethods.Add(gpm);
        }

        internal static GuestPaymentMethod GetGuestPaymentMethod(OTPEntities efContext, int gpmId)
        {
            try
            {
                return efContext.GuestPaymentMethods.Where(g => g.GuestPaymentMethodID == gpmId).SingleOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
