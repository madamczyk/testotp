﻿using BusinessLogic.Enums;
using BusinessLogic.Objects;
using BusinessLogic.Objects.StaticContentRepresentation;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.ServiceContracts;
using Common;
using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using ExtranetApp.Controllers;
using ExtranetApp.Models;
using ExtranetApp.Resources.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace UnitTests.ExtranetControllersTests
{
    [TestClass]
    public class PropertyDetailControllerTest
    {
        [TestMethod]
        public void PropertyDetailsWithRatingAndOneUnitTest()
        {
            #region Setup services
            var propertiesService = new Mock<IPropertiesService>();
            var unitsService = new Mock<IUnitsService>();
            var amenityGroupsService = new Mock<IAmenityGroupsService>();
            var configurationService = new Mock<IConfigurationService>();
            var propertyAddOnsService = new Mock<IPropertyAddOnsService>();
            var taxesService = new Mock<ITaxesService>();
            var bookingService = new Mock<IBookingService>();
            var appliancesService = new Mock<IAppliancesService>();
            #endregion

            PropertyDetailController propertyDetailController = new PropertyDetailController(propertiesService.Object,
                unitsService.Object,
                amenityGroupsService.Object,
                configurationService.Object,
                propertyAddOnsService.Object,
                taxesService.Object,
                bookingService.Object,
                appliancesService.Object);

            #region Initialize data
            string URL = "http://PropertyDetailsWithRatingAndOneUnitTest.ohtheplaces.com/";
            int outNumberOfUnits = 1;
            int? reviewRate = 3;
            string squareFootage = "1234.45";

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m"
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();           

            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                StarRating = 4,
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            #region Unit static content
            var unitStaticContentResults = new List<UnitStaticContentResult>();
            UnitStaticContentResult tmpUSCR;
            List<UnitStaticContent> tmpUSCList;
            UnitStaticContent tmpUSC;

            tmpUSCR = new UnitStaticContentResult() { ContentCode = (int)UnitStaticContentType.KitchenAndLaundry };
            tmpUSCList = new List<UnitStaticContent>();

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit,
                ContentCode = (int)UnitStaticContentType.KitchenAndLaundry
            };
            tmpUSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_KitchenAndLaundry1");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit,
                ContentCode = (int)UnitStaticContentType.KitchenAndLaundry
            };
            tmpUSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_KitchenAndLaundry2");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSCR.ContentItems = tmpUSCList;
            unitStaticContentResults.Add(tmpUSCR);


            tmpUSCR = new UnitStaticContentResult() { ContentCode = (int)UnitStaticContentType.Bathrooms };
            tmpUSCList = new List<UnitStaticContent>();

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit,
                ContentCode = (int)UnitStaticContentType.Bathrooms
            };
            tmpUSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_Bathrooms1");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit,
                ContentCode = (int)UnitStaticContentType.Bathrooms
            };
            tmpUSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_Bathrooms2");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSCR.ContentItems = tmpUSCList;
            unitStaticContentResults.Add(tmpUSCR);


            tmpUSCR = new UnitStaticContentResult() { ContentCode = (int)UnitStaticContentType.Bedrooms };
            tmpUSCList = new List<UnitStaticContent>();

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit,
                ContentCode = (int)UnitStaticContentType.Bedrooms
            };
            tmpUSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_Bedrooms1");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit,
                ContentCode = (int)UnitStaticContentType.Bedrooms
            };
            tmpUSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_Bedrooms2");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSCR.ContentItems = tmpUSCList;
            unitStaticContentResults.Add(tmpUSCR);


            tmpUSCR = new UnitStaticContentResult() { ContentCode = (int)UnitStaticContentType.LivingAreasAndDining };
            tmpUSCList = new List<UnitStaticContent>();

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit,
                ContentCode = (int)UnitStaticContentType.LivingAreasAndDining
            };
            tmpUSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_LivingAreasAndDining1");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit,
                ContentCode = (int)UnitStaticContentType.LivingAreasAndDining
            };
            tmpUSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_LivingAreasAndDining2");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSCR.ContentItems = tmpUSCList;
            unitStaticContentResults.Add(tmpUSCR);
            #endregion

            #region Unit appliances
            var unitAppliances = new List<Appliance>();
            ApplianceGroup tmpApplianceGroup;
            Appliance tmpAppliance;

            tmpApplianceGroup = new ApplianceGroup()
            {
                ApplianceGroupId = 6574
            };
            tmpApplianceGroup.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_Name");
            tmpApplianceGroup.PersistI18nValues();

            tmpAppliance = new Appliance()
            {
                ApplianceId = 158,
                ApplianceGroup = tmpApplianceGroup
            };
            tmpApplianceGroup.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_Appliance1");
            tmpAppliance.PersistI18nValues();
            unitAppliances.Add(tmpAppliance);

            tmpAppliance = new Appliance()
            {
                ApplianceId = 1345,
                ApplianceGroup = tmpApplianceGroup
            };
            tmpApplianceGroup.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_Appliance2");
            tmpAppliance.PersistI18nValues();
            unitAppliances.Add(tmpAppliance);

            tmpAppliance = new Appliance()
            {
                ApplianceId = 65,
                ApplianceGroup = tmpApplianceGroup
            };
            tmpApplianceGroup.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_Appliance3");
            tmpAppliance.PersistI18nValues();
            unitAppliances.Add(tmpAppliance);
            #endregion

            #region Property static content
            var propertyStaticContentResults = new List<PropertyStaticContentResult>();
            var propertyImages = new List<string>();
            PropertyStaticContentResult tmpPSCR;
            List<PropertyStaticContent> tmpPSCList;
            PropertyStaticContent tmpPSC;

            tmpPSCR = new PropertyStaticContentResult() { ContentCode = (int)PropertyStaticContentType.FullScreen };
            tmpPSCList = new List<PropertyStaticContent>();

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.FullScreen,
                ContentValue = "PropertyDetailsWithRatingAndOneUnitTest_FullImage1"
            };
            tmpPSCList.Add(tmpPSC);
            propertyImages.Add(URL + tmpPSC.ContentValue);

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.FullScreen,
                ContentValue = "PropertyDetailsWithRatingAndOneUnitTest_FullImage2"
            };
            tmpPSCList.Add(tmpPSC);
            propertyImages.Add(URL + tmpPSC.ContentValue);

            tmpPSCR.ContentItems = tmpPSCList;
            propertyStaticContentResults.Add(tmpPSCR);


            tmpPSCR = new PropertyStaticContentResult() { ContentCode = (int)PropertyStaticContentType.Feature };
            tmpPSCList = new List<PropertyStaticContent>();

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.Feature
            };
            tmpPSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_Feature1");
            tmpPSC.PersistI18nValues();
            tmpPSCList.Add(tmpPSC);

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.Feature
            };
            tmpPSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_Feature2");
            tmpPSC.PersistI18nValues();
            tmpPSCList.Add(tmpPSC);

            tmpPSCR.ContentItems = tmpPSCList;
            propertyStaticContentResults.Add(tmpPSCR);


            tmpPSCR = new PropertyStaticContentResult() { ContentCode = (int)PropertyStaticContentType.DistanceToMainAttraction };
            tmpPSCList = new List<PropertyStaticContent>();

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.DistanceToMainAttraction
            };
            tmpPSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_DistanceToMainAttraction1");
            tmpPSC.PersistI18nValues();
            tmpPSCList.Add(tmpPSC);

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.DistanceToMainAttraction
            };
            tmpPSC.SetContentValue("PropertyDetailsWithRatingAndOneUnitTest_DistanceToMainAttraction2");
            tmpPSC.PersistI18nValues();
            tmpPSCList.Add(tmpPSC);

            tmpPSCR.ContentItems = tmpPSCList;
            propertyStaticContentResults.Add(tmpPSCR);
            tmpPSCR = new PropertyStaticContentResult() { ContentCode = (int)PropertyStaticContentType.SquareFootage };
            tmpPSCList = new List<PropertyStaticContent>();

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.SquareFootage,
                ContentValue = "1234.45"
            };
            tmpPSC.SetContentValue("1234.45");
            tmpPSC.PersistI18nValues();
            tmpPSCList.Add(tmpPSC);

            tmpPSCR.ContentItems = tmpPSCList;
            propertyStaticContentResults.Add(tmpPSCR);
            #endregion

            #region Property guest reviews
            var propertyGuestReviews = new List<GuestReview>();
            Reservation tmpReservation;
            GuestReview tmpGuestReview;

            tmpReservation = new Reservation()
            {
                ReservationID = 12673,
                Property = property,
                Unit = unit,
                UnitType = unitType,
                User = user,
                ConfirmationID = "abc123",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(3),
                DateDeparture = DateTime.Now.AddMonths(1),
                TotalPrice = 23232.87M,
                TripBalance = 123.54M,
                Agreement = new byte[] { 23, 54, 76, 87, 25 },
                GuestPaymentMethod = new GuestPaymentMethod()
                {
                    GuestPaymentMethodID = 3453,
                    User = user,
                    ReferenceToken = Guid.NewGuid().ToString(),
                    CreditCardType = new DataAccess.CreditCardType()
                    {
                        CreditCardTypeId = 2342,
                        Name = "PropertyDetailsWithRatingAndOneUnitTest_Name",
                        CreditCardIcon = 3
                    },
                    CreditCardLast4Digits = "4235"
                },
                NumberOfGuests = 4,
                Invoice = new byte[] { 51, 48, 21, 36, 54, 84 },
                LinkAuthorizationToken = Guid.NewGuid().ToString()
            };

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 14,
                Property = property,
                OverallRating = 3,
                Reservation = tmpReservation,
                ReviewTitle = "PropertyDetailsWithRatingAndOneUnitTest_Title1",
                ReviewContent = "PropertyDetailsWithRatingAndOneUnitTest_Content1",
                ReviewDate = DateTime.Now,
                Satisfaction_Experience = 4,
                Satisfaction_Cleanliness = 2,
                Satisfaction_Location = 3
            };
            propertyGuestReviews.Add(tmpGuestReview);

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 42,
                Property = property,
                OverallRating = 3,
                Reservation = tmpReservation,
                ReviewTitle = "PropertyDetailsWithRatingAndOneUnitTest_Title2",
                ReviewContent = "PropertyDetailsWithRatingAndOneUnitTest_Content2",
                ReviewDate = DateTime.Now,
                Satisfaction_Experience = 3,
                Satisfaction_Cleanliness = 1,
                Satisfaction_Location = 5
            };
            propertyGuestReviews.Add(tmpGuestReview);

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 31,
                Property = property,
                OverallRating = 4,
                Reservation = tmpReservation,
                ReviewTitle = "PropertyDetailsWithRatingAndOneUnitTest_Title3",
                ReviewContent = "PropertyDetailsWithRatingAndOneUnitTest_Content3",
                ReviewDate = DateTime.Now,
                Satisfaction_Experience = 3,
                Satisfaction_Cleanliness = 4,
                Satisfaction_Location = 5
            };
            propertyGuestReviews.Add(tmpGuestReview);
            #endregion

            #region Property floor plans
            var propertyFloorPlans = new List<FloorPlan>();
            FloorPlan tmpFloorPlan;

            tmpFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "PropertyDetailsWithRatingAndOneUnitTest_DisplayImagePath1",
                OriginalImagePath = "PropertyDetailsWithRatingAndOneUnitTest_OriginalImagePath1",
                Panoramas = new Panorama[] 
                {
                    new Panorama(){Id = 3, OffsetX  = 5, OffsetY = 6, PanoramaPath = "PropertyDetailsWithRatingAndOneUnitTest_PanoramaPath11"},
                    new Panorama(){Id = 4, OffsetX  = 15, OffsetY = 46, PanoramaPath = "PropertyDetailsWithRatingAndOneUnitTest_PanoramaPath12"},
                    new Panorama(){Id = 5, OffsetX  = 25, OffsetY = 56, PanoramaPath = "PropertyDetailsWithRatingAndOneUnitTest_PanoramaPath13"},
                }
            };
            tmpFloorPlan.SetFloorPlanNameValue("PropertyDetailsWithRatingAndOneUnitTest_Name1");
            propertyFloorPlans.Add(tmpFloorPlan);

            tmpFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "PropertyDetailsWithRatingAndOneUnitTest_DisplayImagePath2",
                OriginalImagePath = "PropertyDetailsWithRatingAndOneUnitTest_OriginalImagePath2",
                Panoramas = new Panorama[] 
                {
                    new Panorama(){Id = 15, OffsetX  = 35, OffsetY = 56, PanoramaPath = "PropertyDetailsWithRatingAndOneUnitTest_PanoramaPath21"},
                    new Panorama(){Id = 34, OffsetX  = 15, OffsetY = 846, PanoramaPath = "PropertyDetailsWithRatingAndOneUnitTest_PanoramaPath22"},
                    new Panorama(){Id = 55, OffsetX  = 25, OffsetY = 56, PanoramaPath = "PropertyDetailsWithRatingAndOneUnitTest_PanoramaPath23"},
                }
            };
            tmpFloorPlan.SetFloorPlanNameValue("PropertyDetailsWithRatingAndOneUnitTest_Name2");
            propertyFloorPlans.Add(tmpFloorPlan);
            #endregion

            #region Property amenities
            var propertyAmenityGroups = new List<int>();
            var propertyAmenities = new List<PropertyAmenity>();
            var propertyAmenitiesGrouped = new List<PropertyAmenities>();
            AmenityGroup tmpAmenityGroup1, tmpAmenityGroup2, tmpOtherAmenityGroup;
            PropertyAmenity tmpPropertyAmenity;
            PropertyAmenities tmpPropertyAmenities;

            tmpAmenityGroup1 = new AmenityGroup()
            {
                AmenityGroupId = 3,
                AmenityIcon = 6,
                Position = 1
            };
            tmpAmenityGroup1.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_Name1");
            tmpAmenityGroup1.PersistI18nValues();
            propertyAmenityGroups.Add(tmpAmenityGroup1.AmenityGroupId);

            tmpPropertyAmenities = new PropertyAmenities()
            {
                AmenityGroupName = tmpAmenityGroup1.NameCurrentLanguage,
                Icon = tmpAmenityGroup1.AmenityIcon,
                Position = tmpAmenityGroup1.Position,
                Amenities = new List<string>()
            };

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 2,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 545,
                    AmenityCode = "PropertyDetailsWithRatingAndOneUnitTest_Code1",
                    AmenityGroup = tmpAmenityGroup1,
                    Countable = true,  
                },
                Quantity = 3
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithRatingAndOneUnitTest_Details1");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title1");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_Description1");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 4,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 556,
                    AmenityCode = "PropertyDetailsWithRatingAndOneUnitTest_Code2",
                    AmenityGroup = tmpAmenityGroup1,
                    Countable = false,
                }
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithRatingAndOneUnitTest_Details2");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title2");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_Description2");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);
            propertyAmenitiesGrouped.Add(tmpPropertyAmenities);

            tmpAmenityGroup2 = new AmenityGroup()
            {
                AmenityGroupId = 5,
                AmenityIcon = 1,
                Position = 2
            };
            tmpAmenityGroup2.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_Name2");
            tmpAmenityGroup2.PersistI18nValues();
            propertyAmenityGroups.Add(tmpAmenityGroup2.AmenityGroupId);
            
            tmpPropertyAmenities = new PropertyAmenities()
            {
                AmenityGroupName = tmpAmenityGroup2.NameCurrentLanguage,
                Icon = tmpAmenityGroup2.AmenityIcon,
                Position = tmpAmenityGroup2.Position,
                Amenities = new List<string>()
            };

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 6,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 242,
                    AmenityCode = "PropertyDetailsWithRatingAndOneUnitTest_Code3",
                    AmenityGroup = tmpAmenityGroup2,
                    Countable = true,
                },
                Quantity = 3
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithRatingAndOneUnitTest_Details3");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title3");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_Description3");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 7,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 2345,
                    AmenityCode = "PropertyDetailsWithRatingAndOneUnitTest_Code4",
                    AmenityGroup = tmpAmenityGroup2,
                    Countable = false,
                }
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithRatingAndOneUnitTest_Details4");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title4");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_Description4");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);
            propertyAmenitiesGrouped.Add(tmpPropertyAmenities);


            tmpOtherAmenityGroup = new AmenityGroup()
            {
                AmenityGroupId = 8,
                AmenityIcon = 0,
                Position = 3
            };
            tmpOtherAmenityGroup.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_Name_Other");
            tmpOtherAmenityGroup.PersistI18nValues();
            propertyAmenityGroups.Add(tmpOtherAmenityGroup.AmenityGroupId);

            tmpPropertyAmenities = new PropertyAmenities()
            {
                AmenityGroupName = tmpOtherAmenityGroup.NameCurrentLanguage,
                Icon = tmpOtherAmenityGroup.AmenityIcon,
                Position = tmpOtherAmenityGroup.Position,
                Amenities = new List<string>()
            };

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 75,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 23423,
                    AmenityCode = "PropertyDetailsWithRatingAndOneUnitTest_Code5",
                    AmenityGroup = tmpOtherAmenityGroup,
                    Countable = true,
                },
                Quantity = 33
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithRatingAndOneUnitTest_Details5");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title5");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_Description5");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 235,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 666252,
                    AmenityCode = "PropertyDetailsWithRatingAndOneUnitTest_Code6",
                    AmenityGroup = tmpOtherAmenityGroup,
                    Countable = false,
                }
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithRatingAndOneUnitTest_Details6");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title6");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_Description6");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);
            propertyAmenitiesGrouped.Add(tmpPropertyAmenities);
            #endregion
            #endregion

            #region Mock services
            configurationService.Setup(f => f.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl)).Returns(URL);
            unitsService.Setup(f => f.GetUnitById(unit.UnitID)).Returns(unit);
            propertiesService.Setup(f => f.GetPropertyIdByUnitId(unit.UnitID)).Returns(property.PropertyID);
            propertiesService.Setup(f => f.GetPropertyById(property.PropertyID, out outNumberOfUnits, false)).Returns(property);
            propertiesService.Setup(f => f.GetPropertyStaticContentInfo(property.PropertyID)).Returns(propertyStaticContentResults);
            unitsService.Setup(f => f.GetUnitStaticContentInfo(unit.UnitID)).Returns(unitStaticContentResults);
            propertiesService.Setup(f => f.GetPropertyReviewRate(property.PropertyID)).Returns(reviewRate);
            propertiesService.Setup(f => f.GetPropertySquareFootage(property.PropertyID)).Returns(squareFootage);
            unitsService.Setup(f => f.GetUnitAppliances(unit.UnitID)).Returns(unitAppliances);
            propertiesService.Setup(f => f.GetPropertyGuestReviews(property.PropertyID)).Returns(propertyGuestReviews);
            propertiesService.Setup(f => f.GetPropertyFloorPlans(property.PropertyID)).Returns(propertyFloorPlans);
            propertiesService.Setup(f => f.GetAmenityGroupsByPropertyId(property.PropertyID)).Returns(propertyAmenityGroups);
            propertiesService.Setup(f => f.GetPropertyAmenitiesByPropertyId(property.PropertyID)).Returns(propertyAmenities);
            amenityGroupsService.Setup(f => f.GetOtherAmenityGroupById()).Returns(tmpOtherAmenityGroup);
            amenityGroupsService.Setup(f => f.GetAmenityGroupById(tmpAmenityGroup1.AmenityGroupId)).Returns(tmpAmenityGroup1);
            amenityGroupsService.Setup(f => f.GetAmenityGroupById(tmpAmenityGroup2.AmenityGroupId)).Returns(tmpAmenityGroup2);
            amenityGroupsService.Setup(f => f.GetAmenityGroupById(tmpOtherAmenityGroup.AmenityGroupId)).Returns(tmpOtherAmenityGroup);
            #endregion

            var result = propertyDetailController.PropertyDetails(unit.UnitID) as ViewResult; 
            Assert.IsNotNull(result, Messages.ObjectIsNull);

            var resultModel = result.Model as ExtranetApp.Models.PropertyDetails.PropertyDetailsModel;
            Assert.IsNotNull(resultModel, Messages.ObjectIsNull);

            Assert.AreEqual(resultModel.StorageUrl, URL.TrimEnd("/".ToCharArray()));
            ExtendedAssert.AreEqual(resultModel.Unit, unit);
            Assert.AreEqual(resultModel.NumberOfPropertyUnits, 1);
            Assert.AreEqual(resultModel.SquareFootage, squareFootage);
            Assert.AreEqual(resultModel.PropertyRating, property.StarRating);
            ExtendedAssert.AreEqual(resultModel.Property, property);
            Assert.IsTrue(resultModel.PropertyImages.SequenceEqual(propertyImages));
            Assert.IsTrue(resultModel.KitchenDesc.SequenceEqual(unitStaticContentResults.Where(uscr => uscr.ContentCode == (int)UnitStaticContentType.KitchenAndLaundry).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.BathroomsDesc.SequenceEqual(unitStaticContentResults.Where(uscr => uscr.ContentCode == (int)UnitStaticContentType.Bathrooms).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.BedroomsDesc.SequenceEqual(unitStaticContentResults.Where(uscr => uscr.ContentCode == (int)UnitStaticContentType.Bedrooms).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.LivingArea.SequenceEqual(unitStaticContentResults.Where(uscr => uscr.ContentCode == (int)UnitStaticContentType.LivingAreasAndDining).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.Features.SequenceEqual(propertyStaticContentResults.Where(pscr => pscr.ContentCode == (int)PropertyStaticContentType.Feature).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.DistanceFormAttractions.SequenceEqual(propertyStaticContentResults.Where(pscr => pscr.ContentCode == (int)PropertyStaticContentType.DistanceToMainAttraction).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.Appliances.SequenceEqual(unitAppliances));
            Assert.IsTrue(resultModel.GuestReviews.SequenceEqual(propertyGuestReviews));
            Assert.IsTrue(resultModel.FloorPlans.SequenceEqual(propertyFloorPlans));
            ExtendedAssert.AreEqual(resultModel.DestinationDetails, destination);
            Assert.AreEqual(resultModel.PropertyUnitName, property.PropertyNameCurrentLanguage);
            //Assert.AreEqual(resultModel.Amenities, propertyAmenitiesGrouped.AsEnumerable()); // TODO: modify
        }

        [TestMethod]
        public void PropertyDetailsWithoutRatingAndMoreUnitsTest()
        {
            #region Setup services
            var propertiesService = new Mock<IPropertiesService>();
            var unitsService = new Mock<IUnitsService>();
            var amenityGroupsService = new Mock<IAmenityGroupsService>();
            var configurationService = new Mock<IConfigurationService>();
            var propertyAddOnsService = new Mock<IPropertyAddOnsService>();
            var taxesService = new Mock<ITaxesService>();
            var bookingService = new Mock<IBookingService>();
            var appliancesService = new Mock<IAppliancesService>();
            #endregion

            PropertyDetailController propertyDetailController = new PropertyDetailController(propertiesService.Object,
                unitsService.Object,
                amenityGroupsService.Object,
                configurationService.Object,
                propertyAddOnsService.Object,
                taxesService.Object,
                bookingService.Object,
                appliancesService.Object);

            #region Initialize data
            string URL = "http://PropertyDetailsWithoutRatingAndMoreUnitsTest.ohtheplaces.com/";
            int outNumberOfUnits = 2;
            string squareFootage = "1234.45";

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Poland");
            country.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Lastname",
                Firstname = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Firstname",
                Address1 = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Address1",
                Address2 = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Address2",
                State = "PropertyDetailsWithoutRatingAndMoreUnitsTest_State",
                ZIPCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_ZIPCode",
                City = "PropertyDetailsWithoutRatingAndMoreUnitsTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithoutRatingAndMoreUnitsTest_CellPhone",
                LandLine = "PropertyDetailsWithoutRatingAndMoreUnitsTest_LandLine",
                email = "PropertyDetailsWithoutRatingAndMoreUnitsTest_email",
                DriverLicenseNbr = "PropertyDetailsWithoutRatingAndMoreUnitsTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithoutRatingAndMoreUnitsTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithoutRatingAndMoreUnitsTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithoutRatingAndMoreUnitsTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithoutRatingAndMoreUnitsTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithoutRatingAndMoreUnitsTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithoutRatingAndMoreUnitsTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m"
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_PropertyCode",
                Address1 = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Address1",
                Address2 = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Address2",
                State = "PropertyDetailsWithoutRatingAndMoreUnitsTest_State",
                ZIPCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_ZipCode",
                City = "PropertyDetailsWithoutRatingAndMoreUnitsTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                StarRating = 2
            };
            property.SetPropertyNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit1 = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Code1",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit1.SetTitleValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Title1");
            unit1.SetDescValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Desc1");
            unit1.PersistI18nValues();
          
            Unit unit2 = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Code2",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit2.SetTitleValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Title2");
            unit2.SetDescValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Desc2");
            unit2.PersistI18nValues();

            #region Unit static content
            var unitStaticContentResults = new List<UnitStaticContentResult>();
            UnitStaticContentResult tmpUSCR;
            List<UnitStaticContent> tmpUSCList;
            UnitStaticContent tmpUSC;

            tmpUSCR = new UnitStaticContentResult() { ContentCode = (int)UnitStaticContentType.KitchenAndLaundry };
            tmpUSCList = new List<UnitStaticContent>();

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit1,
                ContentCode = (int)UnitStaticContentType.KitchenAndLaundry
            };
            tmpUSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_KitchenAndLaundry1");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit1,
                ContentCode = (int)UnitStaticContentType.KitchenAndLaundry
            };
            tmpUSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_KitchenAndLaundry2");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSCR.ContentItems = tmpUSCList;
            unitStaticContentResults.Add(tmpUSCR);


            tmpUSCR = new UnitStaticContentResult() { ContentCode = (int)UnitStaticContentType.Bathrooms };
            tmpUSCList = new List<UnitStaticContent>();

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit1,
                ContentCode = (int)UnitStaticContentType.Bathrooms
            };
            tmpUSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Bathrooms1");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit1,
                ContentCode = (int)UnitStaticContentType.Bathrooms
            };
            tmpUSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Bathrooms2");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSCR.ContentItems = tmpUSCList;
            unitStaticContentResults.Add(tmpUSCR);


            tmpUSCR = new UnitStaticContentResult() { ContentCode = (int)UnitStaticContentType.Bedrooms };
            tmpUSCList = new List<UnitStaticContent>();

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit1,
                ContentCode = (int)UnitStaticContentType.Bedrooms
            };
            tmpUSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Bedrooms1");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit1,
                ContentCode = (int)UnitStaticContentType.Bedrooms
            };
            tmpUSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Bedrooms2");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSCR.ContentItems = tmpUSCList;
            unitStaticContentResults.Add(tmpUSCR);


            tmpUSCR = new UnitStaticContentResult() { ContentCode = (int)UnitStaticContentType.LivingAreasAndDining };
            tmpUSCList = new List<UnitStaticContent>();

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit1,
                ContentCode = (int)UnitStaticContentType.LivingAreasAndDining
            };
            tmpUSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_LivingAreasAndDining1");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSC = new UnitStaticContent()
            {
                Unit = unit1,
                ContentCode = (int)UnitStaticContentType.LivingAreasAndDining
            };
            tmpUSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_LivingAreasAndDining2");
            tmpUSC.PersistI18nValues();
            tmpUSCList.Add(tmpUSC);

            tmpUSCR.ContentItems = tmpUSCList;
            unitStaticContentResults.Add(tmpUSCR);
            #endregion

            #region Unit appliances
            var unitAppliances = new List<Appliance>();
            ApplianceGroup tmpApplianceGroup;
            Appliance tmpAppliance;

            tmpApplianceGroup = new ApplianceGroup()
            {
                ApplianceGroupId = 6574
            };
            tmpApplianceGroup.SetNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Name");
            tmpApplianceGroup.PersistI18nValues();

            tmpAppliance = new Appliance()
            {
                ApplianceId = 158,
                ApplianceGroup = tmpApplianceGroup
            };
            tmpApplianceGroup.SetNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Appliance1");
            tmpAppliance.PersistI18nValues();
            unitAppliances.Add(tmpAppliance);

            tmpAppliance = new Appliance()
            {
                ApplianceId = 1345,
                ApplianceGroup = tmpApplianceGroup
            };
            tmpApplianceGroup.SetNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Appliance2");
            tmpAppliance.PersistI18nValues();
            unitAppliances.Add(tmpAppliance);

            tmpAppliance = new Appliance()
            {
                ApplianceId = 65,
                ApplianceGroup = tmpApplianceGroup
            };
            tmpApplianceGroup.SetNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Appliance3");
            tmpAppliance.PersistI18nValues();
            unitAppliances.Add(tmpAppliance);
            #endregion

            #region Property static content
            var propertyStaticContentResults = new List<PropertyStaticContentResult>();
            var propertyImages = new List<string>();
            PropertyStaticContentResult tmpPSCR;
            List<PropertyStaticContent> tmpPSCList;
            PropertyStaticContent tmpPSC;

            tmpPSCR = new PropertyStaticContentResult() { ContentCode = (int)PropertyStaticContentType.FullScreen };
            tmpPSCList = new List<PropertyStaticContent>();

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.FullScreen,
                ContentValue = "PropertyDetailsWithoutRatingAndMoreUnitsTest_FullImage1"
            };
            tmpPSCList.Add(tmpPSC);
            propertyImages.Add(URL + tmpPSC.ContentValue);

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.FullScreen,
                ContentValue = "PropertyDetailsWithoutRatingAndMoreUnitsTest_FullImage2"
            };
            tmpPSCList.Add(tmpPSC);
            propertyImages.Add(URL + tmpPSC.ContentValue);

            tmpPSCR.ContentItems = tmpPSCList;
            propertyStaticContentResults.Add(tmpPSCR);


            tmpPSCR = new PropertyStaticContentResult() { ContentCode = (int)PropertyStaticContentType.Feature };
            tmpPSCList = new List<PropertyStaticContent>();

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.Feature,
            };
            tmpPSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Feature1");
            tmpPSC.PersistI18nValues();
            tmpPSCList.Add(tmpPSC);

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.Feature
            };
            tmpPSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Feature2");
            tmpPSC.PersistI18nValues();
            tmpPSCList.Add(tmpPSC);

            tmpPSCR.ContentItems = tmpPSCList;
            propertyStaticContentResults.Add(tmpPSCR);


            tmpPSCR = new PropertyStaticContentResult() { ContentCode = (int)PropertyStaticContentType.DistanceToMainAttraction };
            tmpPSCList = new List<PropertyStaticContent>();

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.DistanceToMainAttraction
            };
            tmpPSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_DistanceToMainAttraction1");
            tmpPSC.PersistI18nValues();
            tmpPSCList.Add(tmpPSC);

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.DistanceToMainAttraction
            };
            tmpPSC.SetContentValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_DistanceToMainAttraction2");
            tmpPSC.PersistI18nValues();
            tmpPSCList.Add(tmpPSC);

            tmpPSCR.ContentItems = tmpPSCList;
            propertyStaticContentResults.Add(tmpPSCR);


            tmpPSCR = new PropertyStaticContentResult() { ContentCode = (int)PropertyStaticContentType.SquareFootage };
            tmpPSCList = new List<PropertyStaticContent>();

            tmpPSC = new PropertyStaticContent()
            {
                Property = property,
                ContentCode = (int)PropertyStaticContentType.SquareFootage,
                ContentValue = "1234.45"
            };
            tmpPSC.SetContentValue("1234.45");
            tmpPSC.PersistI18nValues();
            tmpPSCList.Add(tmpPSC);

            tmpPSCR.ContentItems = tmpPSCList;
            propertyStaticContentResults.Add(tmpPSCR);
            #endregion

            #region Property floor plans
            var propertyFloorPlans = new List<FloorPlan>();
            FloorPlan tmpFloorPlan;

            tmpFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "PropertyDetailsWithoutRatingAndMoreUnitsTest_DisplayImagePath1",
                OriginalImagePath = "PropertyDetailsWithoutRatingAndMoreUnitsTest_OriginalImagePath1",
                Panoramas = new Panorama[] 
                {
                    new Panorama(){Id = 3, OffsetX  = 5, OffsetY = 6, PanoramaPath = "PropertyDetailsWithoutRatingAndMoreUnitsTest_PanoramaPath11"},
                    new Panorama(){Id = 4, OffsetX  = 15, OffsetY = 46, PanoramaPath = "PropertyDetailsWithoutRatingAndMoreUnitsTest_PanoramaPath12"},
                    new Panorama(){Id = 5, OffsetX  = 25, OffsetY = 56, PanoramaPath = "PropertyDetailsWithoutRatingAndMoreUnitsTest_PanoramaPath13"},
                }
            };
            tmpFloorPlan.SetFloorPlanNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Name1");
            propertyFloorPlans.Add(tmpFloorPlan);

            tmpFloorPlan = new FloorPlan()
            {
                DisplayImagePath = "PropertyDetailsWithoutRatingAndMoreUnitsTest_DisplayImagePath2",
                OriginalImagePath = "PropertyDetailsWithoutRatingAndMoreUnitsTest_OriginalImagePath2",
                Panoramas = new Panorama[] 
                {
                    new Panorama(){Id = 15, OffsetX  = 35, OffsetY = 56, PanoramaPath = "PropertyDetailsWithoutRatingAndMoreUnitsTest_PanoramaPath21"},
                    new Panorama(){Id = 34, OffsetX  = 15, OffsetY = 846, PanoramaPath = "PropertyDetailsWithoutRatingAndMoreUnitsTest_PanoramaPath22"},
                    new Panorama(){Id = 55, OffsetX  = 25, OffsetY = 56, PanoramaPath = "PropertyDetailsWithoutRatingAndMoreUnitsTest_PanoramaPath23"},
                }
            };
            tmpFloorPlan.SetFloorPlanNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Name2");
            propertyFloorPlans.Add(tmpFloorPlan);
            #endregion

            #region Property amenities
            var propertyAmenityGroups = new List<int>();
            var propertyAmenities = new List<PropertyAmenity>();
            var propertyAmenitiesGrouped = new List<PropertyAmenities>();
            AmenityGroup tmpAmenityGroup1, tmpAmenityGroup2, tmpOtherAmenityGroup;
            PropertyAmenity tmpPropertyAmenity;
            PropertyAmenities tmpPropertyAmenities;
            var propertyTags = new List<PropertyTag>();

            tmpAmenityGroup1 = new AmenityGroup()
            {
                AmenityGroupId = 3,
                AmenityIcon = 6,
                Position = 1
            };
            tmpAmenityGroup1.SetNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Name1");
            tmpAmenityGroup1.PersistI18nValues();
            propertyAmenityGroups.Add(tmpAmenityGroup1.AmenityGroupId);

            tmpPropertyAmenities = new PropertyAmenities()
            {
                AmenityGroupName = tmpAmenityGroup1.NameCurrentLanguage,
                Icon = tmpAmenityGroup1.AmenityIcon,
                Position = tmpAmenityGroup1.Position,
                Amenities = new List<string>()
            };

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 2,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 545,
                    AmenityCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Code1",
                    AmenityGroup = tmpAmenityGroup1,
                    Countable = true,
                },
                Quantity = 3
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Details1");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Title1");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Description1");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 4,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 556,
                    AmenityCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Code2",
                    AmenityGroup = tmpAmenityGroup1,
                    Countable = false,
                }
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Details2");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Title2");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Description2");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);
            propertyAmenitiesGrouped.Add(tmpPropertyAmenities);

            tmpAmenityGroup2 = new AmenityGroup()
            {
                AmenityGroupId = 5,
                AmenityIcon = 1,
                Position = 2
            };
            tmpAmenityGroup2.SetNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Name2");
            tmpAmenityGroup2.PersistI18nValues();
            propertyAmenityGroups.Add(tmpAmenityGroup2.AmenityGroupId);

            tmpPropertyAmenities = new PropertyAmenities()
            {
                AmenityGroupName = tmpAmenityGroup2.NameCurrentLanguage,
                Icon = tmpAmenityGroup2.AmenityIcon,
                Position = tmpAmenityGroup2.Position,
                Amenities = new List<string>()
            };

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 6,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 242,
                    AmenityCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Code3",
                    AmenityGroup = tmpAmenityGroup2,
                    Countable = true,
                },
                Quantity = 3
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Details3");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Title3");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Description3");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 7,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 2345,
                    AmenityCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Code4",
                    AmenityGroup = tmpAmenityGroup2,
                    Countable = false,
                }
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Details4");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Title4");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Description4");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);
            propertyAmenitiesGrouped.Add(tmpPropertyAmenities);


            tmpOtherAmenityGroup = new AmenityGroup()
            {
                AmenityGroupId = 8,
                AmenityIcon = 0,
                Position = 3
            };
            tmpOtherAmenityGroup.SetNameValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Name_Other");
            tmpOtherAmenityGroup.PersistI18nValues();
            propertyAmenityGroups.Add(tmpOtherAmenityGroup.AmenityGroupId);

            tmpPropertyAmenities = new PropertyAmenities()
            {
                AmenityGroupName = tmpOtherAmenityGroup.NameCurrentLanguage,
                Icon = tmpOtherAmenityGroup.AmenityIcon,
                Position = tmpOtherAmenityGroup.Position,
                Amenities = new List<string>()
            };

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 75,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 23423,
                    AmenityCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Code5",
                    AmenityGroup = tmpOtherAmenityGroup,
                    Countable = true,
                },
                Quantity = 33
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Details5");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Title5");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Description5");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);

            tmpPropertyAmenity = new PropertyAmenity()
            {
                PropertyAmenityID = 235,
                Property = property,
                Amenity = new Amenity()
                {
                    AmenityID = 666252,
                    AmenityCode = "PropertyDetailsWithoutRatingAndMoreUnitsTest_Code6",
                    AmenityGroup = tmpOtherAmenityGroup,
                    Countable = false,
                }
            };
            tmpPropertyAmenity.SetDetailsValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Details6");
            tmpPropertyAmenity.Amenity.SetTitleValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Title6");
            tmpPropertyAmenity.Amenity.SetDescriptionValue("PropertyDetailsWithoutRatingAndMoreUnitsTest_Description6");
            tmpPropertyAmenity.PersistI18nValues();
            tmpPropertyAmenity.Amenity.PersistI18nValues();
            tmpPropertyAmenities.Amenities.Add(tmpPropertyAmenity.Amenity.TitleCurrentLanguage + (tmpPropertyAmenity.Quantity.HasValue && tmpPropertyAmenity.Quantity > 1 ? " (" + tmpPropertyAmenity.Quantity + ")" : ""));
            propertyAmenities.Add(tmpPropertyAmenity);
            propertyAmenitiesGrouped.Add(tmpPropertyAmenities);
            #endregion
            #endregion

            #region Mock services
            configurationService.Setup(f => f.GetKeyValue(CloudConfigurationKeys.StorageBlobUrl)).Returns(URL);
            unitsService.Setup(f => f.GetUnitById(unit1.UnitID)).Returns(unit1);
            propertiesService.Setup(f => f.GetPropertyIdByUnitId(unit1.UnitID)).Returns(property.PropertyID);
            propertiesService.Setup(f => f.GetPropertyById(property.PropertyID, out outNumberOfUnits, false)).Returns(property);
            propertiesService.Setup(f => f.GetPropertyStaticContentInfo(property.PropertyID)).Returns(propertyStaticContentResults);
            unitsService.Setup(f => f.GetUnitStaticContentInfo(unit1.UnitID)).Returns(unitStaticContentResults);
            propertiesService.Setup(f => f.GetPropertyReviewRate(property.PropertyID)).Returns((int?)null);
            propertiesService.Setup(f => f.GetPropertySquareFootage(property.PropertyID)).Returns(squareFootage);
            unitsService.Setup(f => f.GetUnitAppliances(unit1.UnitID)).Returns(unitAppliances);
            propertiesService.Setup(f => f.GetPropertyGuestReviews(property.PropertyID)).Returns((IEnumerable<GuestReview>)null);
            propertiesService.Setup(f => f.GetPropertyFloorPlans(property.PropertyID)).Returns(propertyFloorPlans);
            propertiesService.Setup(f => f.GetAmenityGroupsByPropertyId(property.PropertyID)).Returns(propertyAmenityGroups);
            propertiesService.Setup(f => f.GetPropertyAmenitiesByPropertyId(property.PropertyID)).Returns(propertyAmenities);
            amenityGroupsService.Setup(f => f.GetOtherAmenityGroupById()).Returns(tmpOtherAmenityGroup);
            amenityGroupsService.Setup(f => f.GetAmenityGroupById(tmpAmenityGroup1.AmenityGroupId)).Returns(tmpAmenityGroup1);
            amenityGroupsService.Setup(f => f.GetAmenityGroupById(tmpAmenityGroup2.AmenityGroupId)).Returns(tmpAmenityGroup2);
            amenityGroupsService.Setup(f => f.GetAmenityGroupById(tmpOtherAmenityGroup.AmenityGroupId)).Returns(tmpOtherAmenityGroup);
            propertiesService.Setup(f => f.GetPropertyTagsByPropertyId(property.PropertyID)).Returns(propertyTags);

            #endregion

            var result = propertyDetailController.PropertyDetails(unit1.UnitID) as ViewResult;
            Assert.IsNotNull(result, Messages.ObjectIsNull);

            var resultModel = result.Model as ExtranetApp.Models.PropertyDetails.PropertyDetailsModel;
            Assert.IsNotNull(resultModel, Messages.ObjectIsNull);

            Assert.AreEqual(resultModel.StorageUrl, URL.TrimEnd("/".ToCharArray()));
            ExtendedAssert.AreEqual(resultModel.Unit, unit1);
            Assert.AreEqual(resultModel.NumberOfPropertyUnits, 2);
            Assert.AreEqual(resultModel.SquareFootage, squareFootage);
            Assert.AreEqual(resultModel.PropertyRating, 2);
            ExtendedAssert.AreEqual(resultModel.Property, property);
            Assert.IsTrue(resultModel.PropertyImages.SequenceEqual(propertyImages));
            Assert.IsTrue(resultModel.KitchenDesc.SequenceEqual(unitStaticContentResults.Where(uscr => uscr.ContentCode == (int)UnitStaticContentType.KitchenAndLaundry).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.BathroomsDesc.SequenceEqual(unitStaticContentResults.Where(uscr => uscr.ContentCode == (int)UnitStaticContentType.Bathrooms).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.BedroomsDesc.SequenceEqual(unitStaticContentResults.Where(uscr => uscr.ContentCode == (int)UnitStaticContentType.Bedrooms).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.LivingArea.SequenceEqual(unitStaticContentResults.Where(uscr => uscr.ContentCode == (int)UnitStaticContentType.LivingAreasAndDining).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.Features.SequenceEqual(propertyStaticContentResults.Where(pscr => pscr.ContentCode == (int)PropertyStaticContentType.Feature).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.DistanceFormAttractions.SequenceEqual(propertyStaticContentResults.Where(pscr => pscr.ContentCode == (int)PropertyStaticContentType.DistanceToMainAttraction).SingleOrDefault().ContentItems.Select(item => item.ContentValueCurrentLanguage)));
            Assert.IsTrue(resultModel.Appliances.SequenceEqual(unitAppliances));
            Assert.AreEqual(resultModel.GuestReviews, null);
            Assert.IsTrue(resultModel.FloorPlans.SequenceEqual(propertyFloorPlans));
            ExtendedAssert.AreEqual(resultModel.DestinationDetails, destination);
            Assert.AreEqual(resultModel.PropertyUnitName, property.PropertyNameCurrentLanguage + " / " + unit1.UnitTitleCurrentLanguage);
            //Assert.AreEqual(resultModel.Amenities, propertyAmenitiesGrouped.AsEnumerable()); // TODO: modify
        }

        [TestMethod]
        public void GetSortedPropertyReviewsByDefaultTest()
        {
            #region Setup services
            var propertiesService = new Mock<IPropertiesService>();
            var unitsService = new Mock<IUnitsService>();
            var amenityGroupsService = new Mock<IAmenityGroupsService>();
            var configurationService = new Mock<IConfigurationService>();
            var propertyAddOnsService = new Mock<IPropertyAddOnsService>();
            var taxesService = new Mock<ITaxesService>();
            var bookingService = new Mock<IBookingService>();
            var appliancesService = new Mock<IAppliancesService>();
            #endregion

            PropertyDetailController propertyDetailController = new PropertyDetailController(propertiesService.Object,
                unitsService.Object,
                amenityGroupsService.Object,
                configurationService.Object,
                propertyAddOnsService.Object,
                taxesService.Object,
                bookingService.Object,
                appliancesService.Object);

            #region Initialize data
            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("GetSortedPropertyReviewsDefaultSortingTest_Poland");
            country.PersistI18nValues();

            User user = new User()
            {
                Lastname = "GetSortedPropertyReviewsDefaultSortingTest_Lastname",
                Firstname = "GetSortedPropertyReviewsDefaultSortingTest_Firstname",
                Address1 = "GetSortedPropertyReviewsDefaultSortingTest_Address1",
                Address2 = "GetSortedPropertyReviewsDefaultSortingTest_Address2",
                State = "GetSortedPropertyReviewsDefaultSortingTest_State",
                ZIPCode = "GetSortedPropertyReviewsDefaultSortingTest_ZIPCode",
                City = "GetSortedPropertyReviewsDefaultSortingTest_City",
                DictionaryCountry = country,
                CellPhone = "GetSortedPropertyReviewsDefaultSortingTest_CellPhone",
                LandLine = "GetSortedPropertyReviewsDefaultSortingTest_LandLine",
                email = "GetSortedPropertyReviewsDefaultSortingTest_email",
                DriverLicenseNbr = "GetSortedPropertyReviewsDefaultSortingTest_DriverLicenseNbr",
                PassportNbr = "GetSortedPropertyReviewsDefaultSortingTest_PassportNbr",
                SocialsecurityNbr = "GetSortedPropertyReviewsDefaultSortingTest_SocialsecurityNbr",
                DirectDepositInfo = "GetSortedPropertyReviewsDefaultSortingTest_DirectDepositInfo",
                SecurityQuestion = "GetSortedPropertyReviewsDefaultSortingTest_SecurityQuestion",
                SecurityAnswer = "GetSortedPropertyReviewsDefaultSortingTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "GetSortedPropertyReviewsDefaultSortingTest_AcceptedTCsInitials",
                Password = "GetSortedPropertyReviewsDefaultSortingTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m"
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("GetSortedPropertyReviewsDefaultSortingTest_DestinationName");
            destination.SetDestinationDescriptionValue("GetSortedPropertyReviewsDefaultSortingTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("GetSortedPropertyReviewsDefaultSortingTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "GetSortedPropertyReviewsDefaultSortingTest_PropertyCode",
                Address1 = "GetSortedPropertyReviewsDefaultSortingTest_Address1",
                Address2 = "GetSortedPropertyReviewsDefaultSortingTest_Address2",
                State = "GetSortedPropertyReviewsDefaultSortingTest_State",
                ZIPCode = "GetSortedPropertyReviewsDefaultSortingTest_ZipCode",
                City = "GetSortedPropertyReviewsDefaultSortingTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("GetSortedPropertyReviewsDefaultSortingTest_PropertyName");
            property.SetShortDescriptionValue("GetSortedPropertyReviewsDefaultSortingTest_ShortDescription");
            property.SetLongDescriptionValue("GetSortedPropertyReviewsDefaultSortingTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("GetSortedPropertyReviewsDefaultSortingTest_UnitTypeTitle");
            unitType.SetDescValue("GetSortedPropertyReviewsDefaultSortingTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "GetSortedPropertyReviewsDefaultSortingTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("GetSortedPropertyReviewsDefaultSortingTest_Title");
            unit.SetDescValue("GetSortedPropertyReviewsDefaultSortingTest_Desc");
            unit.PersistI18nValues();

            #region Property guest reviews
            var propertyGuestReviews = new List<GuestReview>();
            Reservation tmpReservation;
            GuestReview tmpGuestReview;

            tmpReservation = new Reservation()
            {
                ReservationID = 12673,
                Property = property,
                Unit = unit,
                UnitType = unitType,
                User = user,
                ConfirmationID = "abc123",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(3),
                DateDeparture = DateTime.Now.AddMonths(1),
                TotalPrice = 23232.87M,
                TripBalance = 123.54M,
                Agreement = new byte[] { 23, 54, 76, 87, 25 },
                GuestPaymentMethod = new GuestPaymentMethod()
                {
                    GuestPaymentMethodID = 3453,
                    User = user,
                    ReferenceToken = Guid.NewGuid().ToString(),
                    CreditCardType = new DataAccess.CreditCardType()
                    {
                        CreditCardTypeId = 2342,
                        Name = "GetSortedPropertyReviewsDefaultSortingTest_Name",
                        CreditCardIcon = 3
                    },
                    CreditCardLast4Digits = "4235"
                },
                NumberOfGuests = 4,
                Invoice = new byte[] { 51, 48, 21, 36, 54, 84 }
            };

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 14,
                Property = property,
                OverallRating = 3,
                Reservation = tmpReservation,
                ReviewTitle = "GetSortedPropertyReviewsDefaultSortingTest_Title1",
                ReviewContent = "GetSortedPropertyReviewsDefaultSortingTest_Content1",
                ReviewDate = DateTime.Now.AddDays(10),
                Satisfaction_Experience = 4,
                Satisfaction_Cleanliness = 2,
                Satisfaction_Location = 3
            };
            propertyGuestReviews.Add(tmpGuestReview);

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 42,
                Property = property,
                OverallRating = 3,
                Reservation = tmpReservation,
                ReviewTitle = "GetSortedPropertyReviewsDefaultSortingTest_Title2",
                ReviewContent = "GetSortedPropertyReviewsDefaultSortingTest_Content2",
                ReviewDate = DateTime.Now.AddDays(5),
                Satisfaction_Experience = 3,
                Satisfaction_Cleanliness = 1,
                Satisfaction_Location = 5
            };
            propertyGuestReviews.Add(tmpGuestReview);

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 2342,
                Property = property,
                OverallRating = 2,
                Reservation = tmpReservation,
                ReviewTitle = "GetSortedPropertyReviewsDefaultSortingTest_Title4",
                ReviewContent = "GetSortedPropertyReviewsDefaultSortingTest_Content4",
                ReviewDate = DateTime.Now.AddDays(1),
                Satisfaction_Experience = 2,
                Satisfaction_Cleanliness = 1,
                Satisfaction_Location = 3
            };
            propertyGuestReviews.Add(tmpGuestReview);

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 31,
                Property = property,
                OverallRating = 4,
                Reservation = tmpReservation,
                ReviewTitle = "GetSortedPropertyReviewsDefaultSortingTest_Title3",
                ReviewContent = "GetSortedPropertyReviewsDefaultSortingTest_Content3",
                ReviewDate = DateTime.Now.AddDays(3),
                Satisfaction_Experience = 3,
                Satisfaction_Cleanliness = 4,
                Satisfaction_Location = 5
            };
            propertyGuestReviews.Add(tmpGuestReview);

            var properOrder = new List<int>() { 14, 42, 31, 2342 };
            #endregion
            #endregion

            #region Mock services
            propertiesService.Setup(f => f.GetPropertyGuestReviews(property.PropertyID)).Returns(propertyGuestReviews);
            #endregion

            var result = propertyDetailController.GetSortedPropertyReviews(property.PropertyID) as PartialViewResult;
            Assert.IsNotNull(result, Messages.ObjectIsNull);

            var guestReviews = result.Model as IEnumerable<GuestReview>;
            Assert.IsNotNull(guestReviews, Messages.ObjectIsNull);

            //Assert.AreEqual(guestReviews, propertyGuestReviews); // TODO: assert if all reviews present
            
            for (int i = 0; i <propertyGuestReviews.Count; ++i )
                Assert.AreEqual(properOrder[i], guestReviews.ElementAt(i).GuestReviewID);
        }

        [TestMethod]
        public void GetSortedPropertyReviewsByRatingDescTest()
        {
            #region Setup services
            var propertiesService = new Mock<IPropertiesService>();
            var unitsService = new Mock<IUnitsService>();
            var amenityGroupsService = new Mock<IAmenityGroupsService>();
            var configurationService = new Mock<IConfigurationService>();
            var propertyAddOnsService = new Mock<IPropertyAddOnsService>();
            var taxesService = new Mock<ITaxesService>();
            var bookingService = new Mock<IBookingService>();
            var appliancesService = new Mock<IAppliancesService>();
            #endregion

            PropertyDetailController propertyDetailController = new PropertyDetailController(propertiesService.Object,
                unitsService.Object,
                amenityGroupsService.Object,
                configurationService.Object,
                propertyAddOnsService.Object,
                taxesService.Object,
                bookingService.Object,
                appliancesService.Object);

            #region Initialize data
            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("GetSortedPropertyReviewsRatingDescSortingTest_Poland");
            country.PersistI18nValues();

            User user = new User()
            {
                Lastname = "GetSortedPropertyReviewsRatingDescSortingTest_Lastname",
                Firstname = "GetSortedPropertyReviewsRatingDescSortingTest_Firstname",
                Address1 = "GetSortedPropertyReviewsRatingDescSortingTest_Address1",
                Address2 = "GetSortedPropertyReviewsRatingDescSortingTest_Address2",
                State = "GetSortedPropertyReviewsRatingDescSortingTest_State",
                ZIPCode = "GetSortedPropertyReviewsRatingDescSortingTest_ZIPCode",
                City = "GetSortedPropertyReviewsRatingDescSortingTest_City",
                DictionaryCountry = country,
                CellPhone = "GetSortedPropertyReviewsRatingDescSortingTest_CellPhone",
                LandLine = "GetSortedPropertyReviewsRatingDescSortingTest_LandLine",
                email = "GetSortedPropertyReviewsRatingDescSortingTest_email",
                DriverLicenseNbr = "GetSortedPropertyReviewsRatingDescSortingTest_DriverLicenseNbr",
                PassportNbr = "GetSortedPropertyReviewsRatingDescSortingTest_PassportNbr",
                SocialsecurityNbr = "GetSortedPropertyReviewsRatingDescSortingTest_SocialsecurityNbr",
                DirectDepositInfo = "GetSortedPropertyReviewsRatingDescSortingTest_DirectDepositInfo",
                SecurityQuestion = "GetSortedPropertyReviewsRatingDescSortingTest_SecurityQuestion",
                SecurityAnswer = "GetSortedPropertyReviewsRatingDescSortingTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "GetSortedPropertyReviewsRatingDescSortingTest_AcceptedTCsInitials",
                Password = "GetSortedPropertyReviewsRatingDescSortingTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m"
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("GetSortedPropertyReviewsRatingDescSortingTest_DestinationName");
            destination.SetDestinationDescriptionValue("GetSortedPropertyReviewsRatingDescSortingTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("GetSortedPropertyReviewsRatingDescSortingTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "GetSortedPropertyReviewsRatingDescSortingTest_PropertyCode",
                Address1 = "GetSortedPropertyReviewsRatingDescSortingTest_Address1",
                Address2 = "GetSortedPropertyReviewsRatingDescSortingTest_Address2",
                State = "GetSortedPropertyReviewsRatingDescSortingTest_State",
                ZIPCode = "GetSortedPropertyReviewsRatingDescSortingTest_ZipCode",
                City = "GetSortedPropertyReviewsRatingDescSortingTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("GetSortedPropertyReviewsRatingDescSortingTest_PropertyName");
            property.SetShortDescriptionValue("GetSortedPropertyReviewsRatingDescSortingTest_ShortDescription");
            property.SetLongDescriptionValue("GetSortedPropertyReviewsRatingDescSortingTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("GetSortedPropertyReviewsRatingDescSortingTest_UnitTypeTitle");
            unitType.SetDescValue("GetSortedPropertyReviewsRatingDescSortingTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "GetSortedPropertyReviewsRatingDescSortingTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("GetSortedPropertyReviewsRatingDescSortingTest_Title");
            unit.SetDescValue("GetSortedPropertyReviewsRatingDescSortingTest_Desc");
            unit.PersistI18nValues();

            #region Property guest reviews
            var propertyGuestReviews = new List<GuestReview>();
            Reservation tmpReservation;
            GuestReview tmpGuestReview;

            tmpReservation = new Reservation()
            {
                ReservationID = 12673,
                Property = property,
                Unit = unit,
                UnitType = unitType,
                User = user,
                ConfirmationID = "abc123",
                BookingDate = DateTime.Now,
                BookingStatus = (int)BookingStatus.FinalizedBooking,
                DateArrival = DateTime.Now.AddDays(3),
                DateDeparture = DateTime.Now.AddMonths(1),
                TotalPrice = 23232.87M,
                TripBalance = 123.54M,
                Agreement = new byte[] { 23, 54, 76, 87, 25 },
                GuestPaymentMethod = new GuestPaymentMethod()
                {
                    GuestPaymentMethodID = 3453,
                    User = user,
                    ReferenceToken = Guid.NewGuid().ToString(),
                    CreditCardType = new DataAccess.CreditCardType()
                    {
                        CreditCardTypeId = 2342,
                        Name = "GetSortedPropertyReviewsRatingDescSortingTest_Name",
                        CreditCardIcon = 3
                    },
                    CreditCardLast4Digits = "4235"
                },
                NumberOfGuests = 4,
                Invoice = new byte[] { 51, 48, 21, 36, 54, 84 }
            };

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 14,
                Property = property,
                OverallRating = 1,
                Reservation = tmpReservation,
                ReviewTitle = "GetSortedPropertyReviewsRatingDescSortingTest_Title1",
                ReviewContent = "GetSortedPropertyReviewsRatingDescSortingTest_Content1",
                ReviewDate = DateTime.Now.AddDays(10),
                Satisfaction_Experience = 1,
                Satisfaction_Cleanliness = 1,
                Satisfaction_Location = 1
            };
            propertyGuestReviews.Add(tmpGuestReview);

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 42,
                Property = property,
                OverallRating = 3,
                Reservation = tmpReservation,
                ReviewTitle = "GetSortedPropertyReviewsRatingDescSortingTest_Title2",
                ReviewContent = "GetSortedPropertyReviewsRatingDescSortingTest_Content2",
                ReviewDate = DateTime.Now.AddDays(5),
                Satisfaction_Experience = 3,
                Satisfaction_Cleanliness = 1,
                Satisfaction_Location = 5
            };
            propertyGuestReviews.Add(tmpGuestReview);

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 31,
                Property = property,
                OverallRating = 4,
                Reservation = tmpReservation,
                ReviewTitle = "GetSortedPropertyReviewsRatingDescSortingTest_Title3",
                ReviewContent = "GetSortedPropertyReviewsRatingDescSortingTest_Content3",
                ReviewDate = DateTime.Now.AddDays(3),
                Satisfaction_Experience = 3,
                Satisfaction_Cleanliness = 4,
                Satisfaction_Location = 5
            };
            propertyGuestReviews.Add(tmpGuestReview);

            tmpGuestReview = new GuestReview()
            {
                GuestReviewID = 2342,
                Property = property,
                OverallRating = 2,
                Reservation = tmpReservation,
                ReviewTitle = "GetSortedPropertyReviewsRatingDescSortingTest_Title4",
                ReviewContent = "GetSortedPropertyReviewsRatingDescSortingTest_Content4",
                ReviewDate = DateTime.Now.AddDays(1),
                Satisfaction_Experience = 2,
                Satisfaction_Cleanliness = 1,
                Satisfaction_Location = 3
            };
            propertyGuestReviews.Add(tmpGuestReview);

            var properOrder = new List<int>() { 31, 42, 2342, 14 };
            #endregion
            #endregion

            #region Mock services
            propertiesService.Setup(f => f.GetPropertyGuestReviews(property.PropertyID)).Returns(propertyGuestReviews);
            #endregion

            var result = propertyDetailController.GetSortedPropertyReviews(property.PropertyID, "RatingDesc") as PartialViewResult;
            Assert.IsNotNull(result, Messages.ObjectIsNull);

            var guestReviews = result.Model as IEnumerable<GuestReview>;
            Assert.IsNotNull(guestReviews, Messages.ObjectIsNull);

            //Assert.AreEqual(guestReviews, propertyGuestReviews); // TODO: modify   
         
            for (int i = 0; i < propertyGuestReviews.Count; ++i)
                Assert.AreEqual(properOrder[i], guestReviews.ElementAt(i).GuestReviewID);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void GetSortedPropertyReviewsByUnknownOrderTest()
        {
            #region Setup services
            var propertiesService = new Mock<IPropertiesService>();
            var unitsService = new Mock<IUnitsService>();
            var amenityGroupsService = new Mock<IAmenityGroupsService>();
            var configurationService = new Mock<IConfigurationService>();
            var propertyAddOnsService = new Mock<IPropertyAddOnsService>();
            var taxesService = new Mock<ITaxesService>();
            var bookingService = new Mock<IBookingService>();
            var appliancesService = new Mock<IAppliancesService>();
            #endregion

            PropertyDetailController propertyDetailController = new PropertyDetailController(propertiesService.Object,
                unitsService.Object,
                amenityGroupsService.Object,
                configurationService.Object,
                propertyAddOnsService.Object,
                taxesService.Object,
                bookingService.Object,
                appliancesService.Object);

            var result = propertyDetailController.GetSortedPropertyReviews(1, "RandomName");
        }

        [TestMethod]
        public void GetInvoiceDetailsTest()
        {
            #region Setup services
            var propertiesService = new Mock<IPropertiesService>();
            var unitsService = new Mock<IUnitsService>();
            var amenityGroupsService = new Mock<IAmenityGroupsService>();
            var configurationService = new Mock<IConfigurationService>();
            var propertyAddOnsService = new Mock<IPropertyAddOnsService>();
            var taxesService = new Mock<ITaxesService>();
            var bookingService = new Mock<IBookingService>();
            var appliancesService = new Mock<IAppliancesService>();
            #endregion

            PropertyDetailController propertyDetailController = new PropertyDetailController(propertiesService.Object,
                unitsService.Object,
                amenityGroupsService.Object,
                configurationService.Object,
                propertyAddOnsService.Object,
                taxesService.Object,
                bookingService.Object,
                appliancesService.Object);

            #region Initialize data
            PropertyDetailsInvoice expected = new PropertyDetailsInvoice()
            {
                LengthOfStay = 14,
                PricePerNight = 400,
                TotalAccomodation = 140,
                AddonsList = new List<PropertyAddOn>(),
                TotalAddons = 120.89M,
                SubtotalAccomodationAddons = 4543.56M,
                TaxList = new List<Tax>(),
                TotalTax = 123.43M,
                InvoiceTotal = 1231231.45M,
                Culture = new System.Globalization.CultureInfo("en-US")
            };

            DateTime from = DateTime.Now, until = from.AddDays(14);
            #endregion

            #region Mock services
            bookingService.Setup(f => f.CalculateInvoice(1, 1, from, until)).Returns(expected);
            #endregion

            var result = propertyDetailController.GetInvoiceDetails(1, 1, from, until) as PartialViewResult;
            Assert.IsNotNull(result, Messages.ObjectIsNull);

            var actual = result.Model as PropertyDetailsInvoice;
            Assert.IsNotNull(actual, Messages.ObjectIsNull);

            Assert.AreEqual(expected.LengthOfStay, actual.LengthOfStay);
            Assert.AreEqual(expected.PricePerNight, actual.PricePerNight);
            Assert.AreEqual(expected.TotalAccomodation, actual.TotalAccomodation);
            //Assert.AreEqual(expected.AddonsList, actual.AddonsList); // TODO: modify
            Assert.AreEqual(expected.TotalAddons, actual.TotalAddons);
            Assert.AreEqual(expected.SubtotalAccomodationAddons, actual.SubtotalAccomodationAddons);
            //Assert.AreEqual(expected.TaxList, actual.TaxList); // TODO: modify
            Assert.AreEqual(expected.TotalTax, actual.TotalTax);
            Assert.AreEqual(expected.InvoiceTotal, actual.InvoiceTotal);
            Assert.AreEqual(expected.Culture, actual.Culture);
        }

        [TestMethod]
        public void PropertyDetailsByDateWithDatesPriceMlosTest()
        {
            #region Setup services
            var propertiesService = new Mock<IPropertiesService>();
            var unitsService = new Mock<IUnitsService>();
            var amenityGroupsService = new Mock<IAmenityGroupsService>();
            var configurationService = new Mock<IConfigurationService>();
            var propertyAddOnsService = new Mock<IPropertyAddOnsService>();
            var taxesService = new Mock<ITaxesService>();
            var bookingService = new Mock<IBookingService>();
            var appliancesService = new Mock<IAppliancesService>();
            #endregion

            PropertyDetailController propertyDetailController = new PropertyDetailController(propertiesService.Object,
                unitsService.Object,
                amenityGroupsService.Object,
                configurationService.Object,
                propertyAddOnsService.Object,
                taxesService.Object,
                bookingService.Object,
                appliancesService.Object);

            #region Initialize data
            DateTime from = new DateTime(2012, 11, 12), until = from.AddDays(14);

            var unitTypeMLOS = new List<UnitTypeMLO>();

            unitTypeMLOS.Add(new UnitTypeMLO()
            {
                UnitTypeMLOSID = 1,
                MLOS = 5,
            });

            unitTypeMLOS.Add(new UnitTypeMLO()
            {
                UnitTypeMLOSID = 1,
                MLOS = 7,
            });

            unitTypeMLOS.Add(new UnitTypeMLO()
            {
                UnitTypeMLOSID = 1,
                MLOS = 2,
            });

            int? price = 123;
            #endregion

            #region Mock services
            unitsService.Setup(f => f.GetUnitMlos(1, from, until)).Returns(unitTypeMLOS);
            unitsService.Setup(f => f.GetUnitPrice(1, from, until)).Returns(price);
            #endregion

            var result = propertyDetailController.PropertyDetailsByDate(1, from, until, "en-US");
            Assert.IsNotNull(result, Messages.ObjectIsNull);

            Assert.IsNotNull(result.Data, Messages.ObjectIsNull);
            // how to check anonymous object? we can't, i think
        }

        [TestMethod]
        public void GetPropertyAvailabilityInfoForMonthTest()
        {
            #region Setup services
            var propertiesService = new Mock<IPropertiesService>();
            var unitsService = new Mock<IUnitsService>();
            var amenityGroupsService = new Mock<IAmenityGroupsService>();
            var configurationService = new Mock<IConfigurationService>();
            var propertyAddOnsService = new Mock<IPropertyAddOnsService>();
            var taxesService = new Mock<ITaxesService>();
            var bookingService = new Mock<IBookingService>();
            var appliancesService = new Mock<IAppliancesService>();
            #endregion

            PropertyDetailController propertyDetailController = new PropertyDetailController(propertiesService.Object,
                unitsService.Object,
                amenityGroupsService.Object,
                configurationService.Object,
                propertyAddOnsService.Object,
                taxesService.Object,
                bookingService.Object,
                appliancesService.Object);

            #region Initialize data
            DateTime from = new DateTime(2012, 11, 12), until = from.AddDays(14);

            var list = new List<UnitAvailabilityResult>();

            list.Add(new UnitAvailabilityResult()
            {
                Date = "2012-11-12",
                IsBlocked = false,
                Mlos = 4,
                Price = 400
            });

            list.Add(new UnitAvailabilityResult()
            {
                Date = "2012-11-13",
                IsBlocked = false,
                Mlos = 4,
                Price = 500
            });

            list.Add(new UnitAvailabilityResult()
            {
                Date = "2012-11-14",
                IsBlocked = true,
                Mlos = 3,
                Price = 400
            });
            #endregion

            #region Mock services
            unitsService.Setup(f => f.GetUnitAvailability(1, new DateTime(2012, 10, 1), new DateTime(2012, 11, 30))).Returns(list);
            #endregion

            var result = propertyDetailController.GetPropertyAvailabilityInfoForMonth(1, from, until);
            Assert.IsNotNull(result, Messages.ObjectIsNull);

            var resultList = result.Data as List<UnitAvailabilityResult>;
            Assert.IsNotNull(resultList, Messages.ObjectIsNull);

            //Assert.AreEqual(list, resultList); // TODO: modify
        }
    }
}
