﻿using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using ExtranetApp.Controllers;
using ExtranetApp.Models.Home;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;

namespace UnitTests.ExtranetControllersTests
{
    [TestClass]
    public class HomeControllerTest
    {
        private class Anon
        {
            public int Id;
            public string Title;
            public string Image;
            public List<string> Thumbnails;
            public string Description;
            public bool IsPricePerNight;
            public int Bathrooms;
            public int Bedrooms;
            public int Sleeps;
            public decimal Latitude;
            public decimal Longitude;
            public string PricePerNight;
            public string Price;
            public object Destination;
            public string TitleEncoded;
        }

        [TestMethod]
        public void IndexTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            var result = homeController.Index() as ViewResult;

            Assert.IsNotNull(result, Messages.ObjectIsNotNull);
            Assert.AreEqual(null, result.ViewData["InfoMessage"]);
        }

        [TestMethod]
        public void ListViewTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            var result = homeController.ListView() as ViewResult;

            Assert.IsNotNull(result, Messages.ObjectIsNotNull);
        }

        [TestMethod]
        public void MapViewTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            var result = homeController.MapView() as ViewResult;

            Assert.IsNotNull(result, Messages.ObjectIsNotNull);
        }

        [TestMethod]
        public void AboutTest()
        {            
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            var result = homeController.About() as ViewResult;

            Assert.IsNotNull(result, Messages.ObjectIsNotNull);
        }

        [TestMethod]
        public void FaqTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            List<OTP_Settings> settings = new List<OTP_Settings>();
            settings.Add(new OTP_Settings() { SettingCode = "FAQ.SupportPhone", SettingValue = "FAQ.SupportPhone" });
            settings.Add(new OTP_Settings() { SettingCode = "FAQ.SupportEmail", SettingValue = "FAQ.SupportEmail" });
            settings.Add(new OTP_Settings() { SettingCode = "FAQ.HomeownersEmail", SettingValue = "FAQ.HomeownersEmail" });

            settingsService.Setup(f => f.GetAllSettingsByPrefix(It.IsAny<string>())).Returns(settings);

            var result = homeController.Faq() as ViewResult;

            Assert.IsNotNull(result, Messages.ObjectIsNotNull);
            Assert.AreEqual("FAQ.SupportPhone", result.ViewData["SupportPhone"]);
            Assert.AreEqual("FAQ.SupportEmail", result.ViewData["SupportEmail"]);
            Assert.AreEqual("FAQ.HomeownersEmail", result.ViewData["HomeownersEmail"]);
        }

        [TestMethod]
        public void PoliciesTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            var result = homeController.Policies() as ViewResult;

            Assert.IsNotNull(result, Messages.ObjectIsNotNull);
        }

        [TestMethod]
        public void ContactTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            List<OTP_Settings> settings = new List<OTP_Settings>();
            settings.Add(new OTP_Settings() { SettingCode="ContactUs.GuestServicesEmail", SettingValue="ContactUs.GuestServicesEmail" });
            settings.Add(new OTP_Settings() { SettingCode="ContactUs.GuestServicesPhone", SettingValue="ContactUs.GuestServicesEmail"});
            settings.Add(new OTP_Settings() { SettingCode="ContactUs.HomeownersServicesEmail", SettingValue="ContactUs.GuestServicesEmail"});
            settings.Add(new OTP_Settings() { SettingCode="ContactUs.HomeownersServicesPhone", SettingValue="ContactUs.GuestServicesEmail"});
            settings.Add(new OTP_Settings() { SettingCode="ContactUs.OTPHeadquartersAddressLine1", SettingValue="ContactUs.GuestServicesEmail"});
            settings.Add(new OTP_Settings() { SettingCode="ContactUs.OTPHeadquartersAddressLine2", SettingValue="ContactUs.GuestServicesEmail"});
            settings.Add(new OTP_Settings() { SettingCode="ContactUs.OTPHeadquartersEmail", SettingValue="ContactUs.GuestServicesEmail"});
            settings.Add(new OTP_Settings() { SettingCode="ContactUs.OTPHeadquartersPhone", SettingValue="ContactUs.GuestServicesEmail"});

            settingsService.Setup(f => f.GetAllSettingsByPrefix(It.IsAny<string>())).Returns(settings);

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            var result = homeController.Contact() as ViewResult;

            Assert.IsNotNull(result, Messages.ObjectIsNotNull);
            Assert.AreEqual("ContactUs.GuestServicesEmail", result.ViewData["GuestServicesEmail"]);
            Assert.AreEqual("ContactUs.GuestServicesEmail", result.ViewData["GuestServicesPhone"]);
            Assert.AreEqual("ContactUs.GuestServicesEmail", result.ViewData["HomeownersServicesEmail"]);
            Assert.AreEqual("ContactUs.GuestServicesEmail", result.ViewData["HomeownersServicesPhone"]);
            Assert.AreEqual("ContactUs.GuestServicesEmail", result.ViewData["OTPHeadquartersAddressLine1"]);
            Assert.AreEqual("ContactUs.GuestServicesEmail", result.ViewData["OTPHeadquartersAddressLine2"]);
            Assert.AreEqual("ContactUs.GuestServicesEmail", result.ViewData["OTPHeadquartersEmail"]);
            Assert.AreEqual("ContactUs.GuestServicesEmail", result.ViewData["OTPHeadquartersPhone"]);

        }

        [TestMethod]
        public void GetSampleResultTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            List<Destination> destList = new List<Destination>();
            destList.Add(new Destination()
            {
                DestinationNameCurrentLanguage = "First",
                DestinationDescriptionCurrentLanguage = "First descrition",
                DestinationID = 3,
                
            });

            List<PropertiesSearchResult> search = new List<PropertiesSearchResult>();
            search.Add(new PropertiesSearchResult()
            {
                Id = 100,
                Title = "title",
                
            });

            destinationService.Setup(f => f.GetTopXDestinationsByPropertiesCount(1)).Returns(destList.Take(1));
            
            propertiesService.Setup(f => f.SearchProperties(3,null,null,null,null,null,null,null,null,It.IsAny<string>(),null,null,null,null)).Returns(
                                    (int? destinationId, DateTime? dateFrom, DateTime? dateTo, int? numberOfGuests, int? numberOfBathrooms, 
                                     int? numberOfBedrooms, string propertyTypes, string tags, string amenities, string languageCode, decimal? beginLatitude, 
                                     decimal? endLatitude, decimal? beginLongitude, decimal? endLongitude) => search);

            var result = homeController.GetSampleResult() as PartialViewResult;
            var model = result.Model as List<PropertiesSearchResult>;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(model, Messages.ObjectIsNull);
            Assert.AreEqual("Partial/Slider", result.ViewName);
            Assert.AreEqual(search.Count(), model.Count());
            Assert.AreEqual(search.First().Title, model.First().Title);
            Assert.AreEqual(search.First().Id, model.First().Id);
        }

        [TestMethod]
        public void GetSampleResultListViewTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            List<Destination> destList = new List<Destination>();
            destList.Add(new Destination()
            {
                DestinationNameCurrentLanguage = "First",
                DestinationDescriptionCurrentLanguage = "First descrition",
                DestinationID = 3,

            });

            List<PropertiesSearchResult> search = new List<PropertiesSearchResult>();
            search.Add(new PropertiesSearchResult()
            {
                Id = 100,
                Title = "title",

            });

            destinationService.Setup(f => f.GetTopXDestinationsByPropertiesCount(1)).Returns(destList.Take(1));

            propertiesService.Setup(f => f.SearchProperties(3, null, null, null, null, null, null, null, null, It.IsAny<string>(), null, null, null, null)).Returns(
                                    (int? destinationId, DateTime? dateFrom, DateTime? dateTo, int? numberOfGuests, int? numberOfBathrooms,
                                     int? numberOfBedrooms, string propertyTypes, string tags, string amenities, string languageCode, decimal? beginLatitude,
                                     decimal? endLatitude, decimal? beginLongitude, decimal? endLongitude) => search);

            ListViewModel model = new ListViewModel();
            model.PropertiesResult = search;
            model.CurrentResultCount = search.Count();
            model.TotalResults = search.Count();

            var result = homeController.GetSampleResultListVIew() as PartialViewResult;
            var returnedModel = result.Model as ListViewModel;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual("Partial/ListViewSearchResult", result.ViewName);
            Assert.AreEqual(model.CurrentResultCount, returnedModel.CurrentResultCount);
            Assert.AreEqual(model.TotalResults, returnedModel.TotalResults);
            Assert.AreEqual(model.PropertiesResult.Count(), returnedModel.PropertiesResult.Count());
            Assert.AreEqual(model.PropertiesResult.First().Title, returnedModel.PropertiesResult.First().Title);
            Assert.AreEqual(model.PropertiesResult.First().Id, returnedModel.PropertiesResult.First().Id);
        }

        [TestMethod]
        public void SearchTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            List<PropertiesSearchResult> search = new List<PropertiesSearchResult>();
            search.Add(new PropertiesSearchResult()
            {
                Id = 100,
                Title = "title",
            });

            propertiesService.Setup(f => f.SearchProperties(null, null, null, null, null, null, null, null, null, It.IsAny<string>(), null, null, null, null)).Returns(
                                    (int? destinationId, DateTime? dateFrom, DateTime? dateTo, int? numberOfGuests, int? numberOfBathrooms,
                                     int? numberOfBedrooms, string propertyTypes, string tags, string amenities, string languageCode, decimal? beginLatitude,
                                     decimal? endLatitude, decimal? beginLongitude, decimal? endLongitude) => search);

            var result = homeController.Search() as PartialViewResult;
            var model = result.Model as List<PropertiesSearchResult>;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(model, Messages.ObjectIsNull);
            Assert.AreEqual("Partial/Slider", result.ViewName);
            Assert.AreEqual(search.Count(), model.Count());
            Assert.AreEqual(search.First().Title, model.First().Title);
            Assert.AreEqual(search.First().Id, model.First().Id);
        }

        [TestMethod]
        public void SearchListViewTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();
            var httpRequest = new Mock<HttpRequestBase>();
            var httpResponse = new Mock<HttpResponseBase>();
            var httpContext = new Mock<HttpContextBase>();

            httpContext.Setup(p => p.Request).Returns(httpRequest.Object);
            httpContext.Setup(p => p.Response).Returns(httpResponse.Object);
            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);
            homeController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), homeController);

            List<PropertiesSearchResult> search = new List<PropertiesSearchResult>();
            search.Add(new PropertiesSearchResult()
            {
                Price = 2000.0m,
                Id = 100,
                Title = "title",
            });
            search.Add(new PropertiesSearchResult()
            {
                Price = 1000.0m,
                Id = 101,
                Title = "title2"
            });

            HttpCookieCollection cookies = new HttpCookieCollection();
            cookies.Add(new HttpCookie("list_view_page_no", 1.ToString()));

            httpRequest.Setup(p => p.Cookies).Returns(cookies);

            propertiesService.Setup(f => f.SearchProperties(null, null, null, null, null, null, null, null, null, It.IsAny<string>(), null, null, null, null)).Returns(
                        (int? destinationId, DateTime? dateFrom, DateTime? dateTo, int? numberOfGuests, int? numberOfBathrooms,
                         int? numberOfBedrooms, string propertyTypes, string tags, string amenities, string languageCode, decimal? beginLatitude,
                         decimal? endLatitude, decimal? beginLongitude, decimal? endLongitude) => search);
            stateService.Setup(f => f.GetFromCache<IEnumerable<PropertiesSearchResult>>(It.IsAny<string>())).Returns(null as IEnumerable<PropertiesSearchResult>);

            ListViewModel model = new ListViewModel();
            model.PropertiesResult = search;
            model.CurrentResultCount = search.Count();
            model.TotalResults = search.Count();

            var result = homeController.SearchListView(null, null, null, null, null, null, null, null, null, null, null) as PartialViewResult;
            var returnedModel = result.Model as ListViewModel;

            stateService.Verify(f => f.RemoveFromCache("list_view_result"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.AddToCache("list_view_result", It.IsAny<IEnumerable<PropertiesSearchResult>>(), 600), Times.Once(), Messages.MethodWasNotCalled);
            httpResponse.Verify(f => f.AddHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0"), Times.Once(), Messages.MethodWasNotCalled);
            httpResponse.Verify(f => f.AddHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT"), Times.Once(), Messages.MethodWasNotCalled);

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual("Partial/ListViewSearchResult", result.ViewName);
            Assert.AreEqual(model.CurrentResultCount, returnedModel.CurrentResultCount);
            Assert.AreEqual(model.TotalResults, returnedModel.TotalResults);
            Assert.AreEqual(model.PropertiesResult.Count(), returnedModel.PropertiesResult.Count());

            for (int i = 0; i < model.PropertiesResult.Count(); i++)
            {
                Assert.AreEqual(model.PropertiesResult.ElementAt(i).Price, returnedModel.PropertiesResult.ElementAt(i).Price);
                Assert.AreEqual(model.PropertiesResult.ElementAt(i).Title, returnedModel.PropertiesResult.ElementAt(i).Title);
                Assert.AreEqual(model.PropertiesResult.ElementAt(i).Id, returnedModel.PropertiesResult.ElementAt(i).Id);
            }
        }

        [TestMethod]
        public void SearchListViewWithSortingTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();
            var httpRequest = new Mock<HttpRequestBase>();
            var httpResponse = new Mock<HttpResponseBase>();
            var httpContext = new Mock<HttpContextBase>();

            httpContext.Setup(p => p.Request).Returns(httpRequest.Object);
            httpContext.Setup(p => p.Response).Returns(httpResponse.Object);
            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);
            homeController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), homeController);

            List<PropertiesSearchResult> search = new List<PropertiesSearchResult>();
            search.Add(new PropertiesSearchResult()
            {
                Price = 2000.0m,
                Id = 100,
                Title = "title",
            });
            search.Add(new PropertiesSearchResult()
            {
                Price = 1000.0m,
                Id = 101,
                Title = "title2"
            });

            HttpCookieCollection cookies = new HttpCookieCollection();
            cookies.Add(new HttpCookie("list_view_page_no", 1.ToString()));

            httpRequest.Setup(p => p.Cookies).Returns(cookies);

            propertiesService.Setup(f => f.SearchProperties(null, null, null, null, null, null, null, null, null, It.IsAny<string>(), null, null, null, null)).Returns(
                        (int? destinationId, DateTime? dateFrom, DateTime? dateTo, int? numberOfGuests, int? numberOfBathrooms,
                         int? numberOfBedrooms, string propertyTypes, string tags, string amenities, string languageCode, decimal? beginLatitude,
                         decimal? endLatitude, decimal? beginLongitude, decimal? endLongitude) => search);
            stateService.Setup(f => f.GetFromCache<IEnumerable<PropertiesSearchResult>>(It.IsAny<string>())).Returns(null as IEnumerable<PropertiesSearchResult>);

            ListViewModel model = new ListViewModel();
            model.PropertiesResult = search;
            model.CurrentResultCount = search.Count();
            model.TotalResults = search.Count();

            var result = homeController.SearchListView(null, null, null, null, null, null, null, null, null, "Sort", null) as PartialViewResult;
            var returnedModel = result.Model as ListViewModel;

            stateService.Verify(f => f.AddToCache("list_view_result", It.IsAny<IEnumerable<PropertiesSearchResult>>(), 600), Times.Once(), Messages.MethodWasNotCalled);
            httpResponse.Verify(f => f.AddHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0"), Times.Once(), Messages.MethodWasNotCalled);
            httpResponse.Verify(f => f.AddHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT"), Times.Once(), Messages.MethodWasNotCalled);

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual("Partial/ListViewSearchResult", result.ViewName);
            Assert.AreEqual(model.CurrentResultCount, returnedModel.CurrentResultCount);
            Assert.AreEqual(model.TotalResults, returnedModel.TotalResults);
            Assert.AreEqual(model.PropertiesResult.Count(), returnedModel.PropertiesResult.Count());

            for (int i = 0; i < model.PropertiesResult.Count(); i++)
            {
                Assert.AreEqual(model.PropertiesResult.ElementAt(1-i).Price, returnedModel.PropertiesResult.ElementAt(i).Price);
                Assert.AreEqual(model.PropertiesResult.ElementAt(1-i).Title, returnedModel.PropertiesResult.ElementAt(i).Title);
                Assert.AreEqual(model.PropertiesResult.ElementAt(1-i).Id, returnedModel.PropertiesResult.ElementAt(i).Id);
            }
        }

        [TestMethod]
        public void SearchListViewWithPagingTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();
            var httpRequest = new Mock<HttpRequestBase>();
            var httpResponse = new Mock<HttpResponseBase>();
            var httpContext = new Mock<HttpContextBase>();

            httpContext.Setup(p => p.Request).Returns(httpRequest.Object);
            httpContext.Setup(p => p.Response).Returns(httpResponse.Object);

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);
            homeController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), homeController);

            List<PropertiesSearchResult> search = new List<PropertiesSearchResult>();

            for (int i = 0; i < 13; i++)
            {
                search.Add(new PropertiesSearchResult()
                {
                    Price = 1000.0m * i,
                    Id = 100 + i,
                    Title = "title" + i.ToString(),
                });
            }

            HttpCookieCollection cookies = new HttpCookieCollection();
            cookies.Add(new HttpCookie("list_view_page_no", 3.ToString()));

            httpRequest.Setup(p => p.Cookies).Returns(cookies);

            propertiesService.Setup(f => f.SearchProperties(null, null, null, null, null, null, null, null, null, It.IsAny<string>(), null, null, null, null)).Returns(
                        (int? destinationId, DateTime? dateFrom, DateTime? dateTo, int? numberOfGuests, int? numberOfBathrooms,
                         int? numberOfBedrooms, string propertyTypes, string tags, string amenities, string languageCode, decimal? beginLatitude,
                         decimal? endLatitude, decimal? beginLongitude, decimal? endLongitude) => search);
            stateService.Setup(f => f.GetFromCache<IEnumerable<PropertiesSearchResult>>(It.IsAny<string>())).Returns(null as IEnumerable<PropertiesSearchResult>);

            ListViewModel model = new ListViewModel();
            model.PropertiesResult = search;
            model.CurrentResultCount = search.Count();
            model.TotalResults = search.Count();

            var result = homeController.SearchListView(null, null, null, null, null, null, null, null, null, "Sort", null) as PartialViewResult;
            var returnedModel = result.Model as ListViewModel;

            stateService.Verify(f => f.AddToCache("list_view_result", It.IsAny<IEnumerable<PropertiesSearchResult>>(), 600), Times.Once(), Messages.MethodWasNotCalled);
            httpResponse.Verify(f => f.AddHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0"), Times.Once(), Messages.MethodWasNotCalled);
            httpResponse.Verify(f => f.AddHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT"), Times.Once(), Messages.MethodWasNotCalled);

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual("Partial/ListViewSearchResult", result.ViewName);
            Assert.AreEqual(model.CurrentResultCount, returnedModel.CurrentResultCount);
            Assert.AreEqual(model.TotalResults, returnedModel.TotalResults);
            Assert.AreEqual(1, returnedModel.PropertiesResult.Count()); // 13 - results_per_page * 2

            Assert.AreEqual(model.PropertiesResult.ElementAt(12).Price, returnedModel.PropertiesResult.ElementAt(0).Price);
            Assert.AreEqual(model.PropertiesResult.ElementAt(12).Title, returnedModel.PropertiesResult.ElementAt(0).Title);
            Assert.AreEqual(model.PropertiesResult.ElementAt(12).Id, returnedModel.PropertiesResult.ElementAt(0).Id);
        }

        [TestMethod]
        public void SearchListViewFromCacheTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();
            var httpRequest = new Mock<HttpRequestBase>();
            var httpResponse = new Mock<HttpResponseBase>();
            var httpContext = new Mock<HttpContextBase>();

            httpContext.Setup(p => p.Request).Returns(httpRequest.Object);
            httpContext.Setup(p => p.Response).Returns(httpResponse.Object);

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);
            homeController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), homeController);

            List<PropertiesSearchResult> search = new List<PropertiesSearchResult>();
            search.Add(new PropertiesSearchResult()
            {
                Price = 1000.0m,
                Id = 100,
                Title = "title",
            });
            search.Add(new PropertiesSearchResult()
            {
                Price = 2000.0m,
                Id = 101,
                Title = "title2"
            });

            HttpCookieCollection cookies = new HttpCookieCollection();
            cookies.Add(new HttpCookie("list_view_page_no", 1.ToString()));

            httpRequest.Setup(p => p.Cookies).Returns(cookies);
            stateService.Setup(f => f.GetFromCache<IEnumerable<PropertiesSearchResult>>(It.IsAny<string>())).Returns(search);

            ListViewModel model = new ListViewModel();
            model.PropertiesResult = search;
            model.CurrentResultCount = search.Count();
            model.TotalResults = search.Count();

            var result = homeController.SearchListView(null, null, null, null, null, null, null, null, null, "Sort", null) as PartialViewResult;
            var returnedModel = result.Model as ListViewModel;

            httpResponse.Verify(f => f.AddHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0"), Times.Once(), Messages.MethodWasNotCalled);
            httpResponse.Verify(f => f.AddHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT"), Times.Once(), Messages.MethodWasNotCalled);

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual("Partial/ListViewSearchResult", result.ViewName);
            Assert.AreEqual(model.CurrentResultCount, returnedModel.CurrentResultCount);
            Assert.AreEqual(model.TotalResults, returnedModel.TotalResults);
            Assert.AreEqual(model.PropertiesResult.Count(), returnedModel.PropertiesResult.Count());

            for (int i = 0; i < model.PropertiesResult.Count(); i++)
            {
                Assert.AreEqual(model.PropertiesResult.ElementAt(i).Price, returnedModel.PropertiesResult.ElementAt(i).Price);
                Assert.AreEqual(model.PropertiesResult.ElementAt(i).Title, returnedModel.PropertiesResult.ElementAt(i).Title);
                Assert.AreEqual(model.PropertiesResult.ElementAt(i).Id, returnedModel.PropertiesResult.ElementAt(i).Id);
            }
        }

        [TestMethod]
        public void SearchMapViewTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            List<PropertiesSearchResult> search = new List<PropertiesSearchResult>();
            search.Add(new PropertiesSearchResult()
            {
                Price = 1000.0m,
                PricePerNight = 100.0m,
                IsPricePerNight = true,
                Image = "Image",
                Bathrooms = 1,
                Bedrooms = 2,
                CultureCode = CultureCode.en_US,
                Description = "Description",
                Latitude = 123.0m,
                Longitude = 111.0m,
                Rating = 0,
                Sleeps = 3,
                Thumbnails = new List<string>(),        
                Id = 100,
                Title = "title",
                
            });
            search.Add(new PropertiesSearchResult()
            {
                Price = 2000.0m,
                PricePerNight = 100.0m,
                IsPricePerNight = true,
                Image = "Image",
                Bathrooms = 1,
                Bedrooms = 2,
                CultureCode = CultureCode.en_US,
                Description = "Description",
                Latitude = 123.0m,
                Longitude = 111.0m,
                Rating = 0,
                Sleeps = 3,
                Thumbnails = new List<string>(),
                Id = 101,
                Title = "title2"
            });

            propertiesService.Setup(f => f.SearchProperties(null, null, null, null, null, null, null, null, null, It.IsAny<string>(), null, null, null, null)).Returns(
                                    (int? destinationId, DateTime? dateFrom, DateTime? dateTo, int? numberOfGuests, int? numberOfBathrooms,
                                     int? numberOfBedrooms, string propertyTypes, string tags, string amenities, string languageCode, decimal? beginLatitude,
                                     decimal? endLatitude, decimal? beginLongitude, decimal? endLongitude) => search);

            var target = homeController.SearchMapView() as JsonResult;
            //var result = target.Data as IEnumerable<PropertiesSearchResult>;
            var serializer = new JavaScriptSerializer();
            var output = serializer.Serialize(target.Data);
            var returned = JsonConvert.DeserializeObject<List<Anon>>(output);

            Assert.IsNotNull(target, Messages.ObjectIsNull);
            Assert.IsNotNull(output, Messages.ObjectIsNull);
            Assert.IsNotNull(returned, Messages.ObjectIsNull);

            int l = 0;
            foreach (Anon a in returned)
            {
                PropertiesSearchResult temp = search[l++];
                Assert.AreEqual(temp.Bathrooms, a.Bathrooms);
                Assert.AreEqual(temp.Bedrooms, a.Bedrooms);
                Assert.AreEqual(temp.Description, a.Description);
                Assert.AreEqual(null, a.Destination);
                Assert.AreEqual(temp.Id, a.Id);
                Assert.AreEqual(temp.IsPricePerNight, a.IsPricePerNight);
                Assert.AreEqual(temp.Latitude, a.Latitude);
                Assert.AreEqual(temp.Longitude, a.Longitude);
                Assert.AreEqual(temp.Sleeps, a.Sleeps);
                Assert.AreEqual(temp.Title, a.Title);
            }

        }

        [TestMethod]
        public void TermsOfServiceTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);

            var result = homeController.TermsOfService() as ViewResult;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("TOS", result.ViewData["Mode"]);
            Assert.AreEqual("Regulations", result.ViewName);
        }

        [TestMethod]
        public void PrivacyPolicyTest()
        {
            #region Setup services

            var destinationService = new Mock<IDestinationsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var settingsService = new Mock<ISettingsService>();

            #endregion

            HomeController homeController = new HomeController(destinationService.Object,
                                                               propertiesService.Object,
                                                               stateService.Object,
                                                               settingsService.Object);
            var result = homeController.PrivacyPolicy() as ViewResult;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("PP", result.ViewData["Mode"]);
            Assert.AreEqual("Regulations", result.ViewName);
        }
    }
}
