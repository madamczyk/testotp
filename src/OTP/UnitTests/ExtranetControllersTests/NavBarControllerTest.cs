﻿using BusinessLogic.ServiceContracts;
using DataAccess;
using ExtranetApp.Controllers;
using ExtranetApp.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace UnitTests.ExtranetControllersTests
{
    [TestClass]
    public class NavBarControllerTest
    {
        private class Anon
        {
            public int id;
            public string value;
        }

        /// <summary>
        /// Tests if navbar filters are filled correctly
        /// </summary>
        [TestMethod]
        public void Check_Filter_Initialization()
        {
            // Firstly we are initializing all the service objects that controller will need, they are mocks, these objects
            // will pretend to be our service business layer
            var amenityService = new Mock<IAmenityService>();
            var destinationService = new Mock<IDestinationsService>();
            var propertyTypesService = new Mock<IPropertyTypesService>();
            var tagsService = new Mock<ITagsService>();
            var httpContextService = new Mock<IContextService>();
            var tagGroupService = new Mock<ITagGroupsService>();
            var amenityGroupService = new Mock<IAmenityGroupsService>();

            // We need some stub values for the navbar filter lists (amenities, tags, types and so on) 
            List<Destination> destinationList = new List<Destination>()
            {
                { new Destination() {
                    DestinationNameCurrentLanguage = "First",
                    DestinationDescriptionCurrentLanguage = "First descrition",
                    DestinationID = 3
                }}
                                ,{ new Destination() {
                    DestinationNameCurrentLanguage = "Second",
                    DestinationDescriptionCurrentLanguage = "Second descrition",
                    DestinationID = 2
                }}
                                ,{ new Destination() {
                    DestinationNameCurrentLanguage = "Third",
                    DestinationDescriptionCurrentLanguage = "Third descrition",
                    DestinationID = 1
                }}
            };

            List<AmenityGroup> amenityList = new List<AmenityGroup>()
            {
                
                {new AmenityGroup() { AmenityGroupId = 2, NameCurrentLanguage = "FIRST GROUP", 
                    Amenities = new List<Amenity>() 
                    {
                        new Amenity()
                        {
                            AmenityCode = "A CODE",
                            AmenityID = 24,
                            TitleCurrentLanguage = "AMENITY TITLE"
                        }
                    }
                }}
            };

            List<PropertyType> propertyTypes = new List<PropertyType>()
            {
                
                {new PropertyType() { PropertyTypeId = 2, NameCurrentLanguage = "FIRST" }},
                { new PropertyType() { PropertyTypeId = 15, NameCurrentLanguage = "SECOND" }},
                { new PropertyType() { PropertyTypeId = 15, NameCurrentLanguage = "Third" }} 
            };

            List<TagGroup> tagsList = new List<TagGroup>()
            {
                
                {new TagGroup() { NameCurrentLanguage = "TAG GROUP 1", TagGroupId = 34, 
                    Tags = new List<Tag>()
                    {
                        new Tag()
                        {
                            NameCurrentLanguage = "Tag 1",
                            TagID = 255
                        },
                        new Tag()
                        {
                            NameCurrentLanguage = "Tag 4",
                            TagID = 256
                        }
                    }
            }}};

            // Here we do some setups for the service, we need to tell Mocks what to do when they get a method call
            // We have to say that this method invoked with specyfic parameters will return concrete values
            destinationService.Setup(f => f.GetTopXDestinationsByPropertiesCount(1)).Returns(destinationList.Take(1));
            destinationService.Setup(f => f.GetTopXDestinationsByPropertiesCount(9)).Returns(destinationList);
            amenityGroupService.Setup(f => f.GetAmenityGroups()).Returns(amenityList);
            propertyTypesService.Setup(f => f.GetPropertyTypes()).Returns(propertyTypes);
            tagGroupService.Setup(f => f.GetTagGroups()).Returns(tagsList);
            
            // Here we create our controller class that we will test - we give all services that controller need - here the mocks that we set up earlier
            NavBarController target = new NavBarController(destinationService.Object, amenityService.Object, propertyTypesService.Object, tagsService.Object, tagGroupService.Object, amenityGroupService.Object);

            // Invoke concrete View method
            PartialViewResult result = (PartialViewResult)target.NavBar();
            // Get the ViewResult - and cas it to the specyfic model
            SearchFilterModel model = result.Model as SearchFilterModel;

            // Here we check if everything is ok with our data send to the Razor Engine
            Assert.AreEqual(model.Location.ID, destinationList.First().DestinationID);
            Assert.AreEqual(model.PropertyTypes.Count(), propertyTypes.Count());
            Assert.AreEqual(model.Amenities.Count(), amenityList.Count());
            Assert.AreEqual(model.Experiences.Count(), tagsList.Count());
            Assert.AreEqual(model.Destinations.Count(), destinationList.Count());
        }

        [TestMethod]
        public void Check_Filter_Initialization_Navbar_With_Sort()
        {
            // Firstly we are initializing all the service objects that controller will need, they are mocks, these objects
            // will pretend to be our service business layer
            var amenityService = new Mock<IAmenityService>();
            var destinationService = new Mock<IDestinationsService>();
            var propertyTypesService = new Mock<IPropertyTypesService>();
            var tagsService = new Mock<ITagsService>();
            var httpContextService = new Mock<IContextService>();
            var tagGroupService = new Mock<ITagGroupsService>();
            var amenityGroupService = new Mock<IAmenityGroupsService>();

            // We need some stub values for the navbar filter lists (amenities, tags, types and so on) 
            List<Destination> destinationList = new List<Destination>()
            {
                { new Destination() {
                    DestinationNameCurrentLanguage = "First",
                    DestinationDescriptionCurrentLanguage = "First descrition",
                    DestinationID = 3
                }}
                                ,{ new Destination() {
                    DestinationNameCurrentLanguage = "Second",
                    DestinationDescriptionCurrentLanguage = "Second descrition",
                    DestinationID = 2
                }}
                                ,{ new Destination() {
                    DestinationNameCurrentLanguage = "Third",
                    DestinationDescriptionCurrentLanguage = "Third descrition",
                    DestinationID = 1
                }}
            };

            List<AmenityGroup> amenityList = new List<AmenityGroup>()
            {
                
                {new AmenityGroup() { AmenityGroupId = 2, NameCurrentLanguage = "FIRST GROUP", 
                    Amenities = new List<Amenity>() 
                    {
                        new Amenity()
                        {
                            AmenityCode = "A CODE",
                            AmenityID = 24,
                            TitleCurrentLanguage = "AMENITY TITLE"
                        }
                    }
                }}
            };

            List<PropertyType> propertyTypes = new List<PropertyType>()
            {
                
                {new PropertyType() { PropertyTypeId = 2, NameCurrentLanguage = "FIRST" }},
                { new PropertyType() { PropertyTypeId = 15, NameCurrentLanguage = "SECOND" }},
                { new PropertyType() { PropertyTypeId = 15, NameCurrentLanguage = "Third" }} 
            };

            List<TagGroup> tagsList = new List<TagGroup>()
            {
                
                {new TagGroup() { NameCurrentLanguage = "TAG GROUP 1", TagGroupId = 34, 
                    Tags = new List<Tag>()
                    {
                        new Tag()
                        {
                            NameCurrentLanguage = "Tag 1",
                            TagID = 255
                        },
                        new Tag()
                        {
                            NameCurrentLanguage = "Tag 4",
                            TagID = 256
                        }
                    }
            }}};

            // Here we do some setups for the service, we need to tell Mocks what to do when they get a method call
            // We have to say that this method invoked with specyfic parameters will return concrete values
            destinationService.Setup(f => f.GetTopXDestinationsByPropertiesCount(1)).Returns(destinationList.Take(1));
            destinationService.Setup(f => f.GetTopXDestinationsByPropertiesCount(9)).Returns(destinationList);
            amenityGroupService.Setup(f => f.GetAmenityGroups()).Returns(amenityList);
            propertyTypesService.Setup(f => f.GetPropertyTypes()).Returns(propertyTypes);
            tagGroupService.Setup(f => f.GetTagGroups()).Returns(tagsList);

            // Here we create our controller class that we will test - we give all services that controller need - here the mocks that we set up earlier
            NavBarController target = new NavBarController(destinationService.Object, amenityService.Object, propertyTypesService.Object, tagsService.Object, tagGroupService.Object, amenityGroupService.Object);

            // Invoke concrete View method
            PartialViewResult result = (PartialViewResult)target.NavBarWithSort();
            // Get the ViewResult - and cas it to the specyfic model
            SearchFilterModel model = result.Model as SearchFilterModel;

            // Here we check if everything is ok with our data send to the Razor Engine
            Assert.AreEqual("NavBarWithSort", result.ViewName);
            Assert.AreEqual(model.Location.ID, destinationList.First().DestinationID);
            Assert.AreEqual(model.PropertyTypes.Count(), propertyTypes.Count());
            Assert.AreEqual(model.Amenities.Count(), amenityList.Count());
            Assert.AreEqual(model.Experiences.Count(), tagsList.Count());
            Assert.AreEqual(model.Destinations.Count(), destinationList.Count());
        }

        [TestMethod]
        public void GetDestinationNameByIdTest()
        {
            #region Setup services
            var amenityService = new Mock<IAmenityService>();
            var destinationService = new Mock<IDestinationsService>();
            var propertyTypesService = new Mock<IPropertyTypesService>();
            var tagsService = new Mock<ITagsService>();
            var httpContextService = new Mock<IContextService>();
            var tagGroupService = new Mock<ITagGroupsService>();
            var amenityGroupService = new Mock<IAmenityGroupsService>();
            #endregion

            Destination dest = new Destination()
            {
                DestinationNameCurrentLanguage = "Second",
                DestinationDescriptionCurrentLanguage = "Second descrition",
                DestinationID = 123
            };
            dest.SetDestinationNameValue("testDestinationName");
            destinationService.Setup(f => f.GetDestinationById(dest.DestinationID)).Returns(dest);

            NavBarController target = new NavBarController(destinationService.Object, amenityService.Object, propertyTypesService.Object, tagsService.Object, tagGroupService.Object, amenityGroupService.Object);

            string result1 = target.GetDestinationNameById(dest.DestinationID.ToString());
            Assert.AreEqual(dest.DestinationName.ToString(), result1);

            string result2 = target.GetDestinationNameById("1");
            Assert.AreEqual(String.Empty, result2);
        }

        [TestMethod]
        public void FindDestinationTest()
        {
            #region Setup services
            var amenityService = new Mock<IAmenityService>();
            var destinationService = new Mock<IDestinationsService>();
            var propertyTypesService = new Mock<IPropertyTypesService>();
            var tagsService = new Mock<ITagsService>();
            var httpContextService = new Mock<IContextService>();
            var tagGroupService = new Mock<ITagGroupsService>();
            var amenityGroupService = new Mock<IAmenityGroupsService>();
            #endregion

            List<Destination> destinations = new List<Destination>();

            for (int i = 0; i < 5; i++)
            {
                destinations.Add(new Destination()
                {
                    DestinationNameCurrentLanguage = "test" + i.ToString(),
                    DestinationDescriptionCurrentLanguage = "Desc",
                    DestinationID = i
                });
                destinations[i].SetDestinationNameValue("testDestinationName" + i.ToString());
            }

            destinationService.Setup(f => f.GetMatchesDestinations("test1")).Returns(destinations);

            NavBarController target = new NavBarController(destinationService.Object, amenityService.Object, propertyTypesService.Object, tagsService.Object, tagGroupService.Object, amenityGroupService.Object);

            var result = target.FindDestination("test1") as JsonResult;
            var serializer = new JavaScriptSerializer();
            var output = serializer.Serialize(result.Data);
            var returned = JsonConvert.DeserializeObject<List<Anon>>(output);

            int l = 0;
            foreach (Anon a in returned)
            {
                Destination temp = destinations[l++];
                Assert.AreEqual(temp.DestinationID, a.id);
                Assert.AreEqual(temp.DestinationNameCurrentLanguage, a.value);
            }
        }

        [TestMethod]
        public void FindExactDestinationTest()
        {
            #region Setup services
            var amenityService = new Mock<IAmenityService>();
            var destinationService = new Mock<IDestinationsService>();
            var propertyTypesService = new Mock<IPropertyTypesService>();
            var tagsService = new Mock<ITagsService>();
            var httpContextService = new Mock<IContextService>();
            var tagGroupService = new Mock<ITagGroupsService>();
            var amenityGroupService = new Mock<IAmenityGroupsService>();
            #endregion

            List<Destination> destinations = new List<Destination>();

            for (int i = 0; i < 5; i++)
            {
                destinations.Add(new Destination()
                {
                    DestinationNameCurrentLanguage = "test" + i.ToString(),
                    DestinationDescriptionCurrentLanguage = "Desc",
                    DestinationID = i
                });
                destinations[i].SetDestinationNameValue("testDestinationName" + i.ToString());
            }

            destinationService.Setup(f => f.GetMatchesDestinations(It.IsAny<string>())).Returns(destinations);

            NavBarController target = new NavBarController(destinationService.Object, amenityService.Object, propertyTypesService.Object, tagsService.Object, tagGroupService.Object, amenityGroupService.Object);

            var result = target.FindExactDestination("test1") as JsonResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("{ id = 1, value = test1 }", result.Data.ToString());
        }
    }
}
