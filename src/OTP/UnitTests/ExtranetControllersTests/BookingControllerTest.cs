﻿using BusinessLogic.EVS;
using BusinessLogic.Objects;
using BusinessLogic.Objects.Session;
using BusinessLogic.Objects.StaticContentRepresentation;
using BusinessLogic.Objects.Templates.Document;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.PaymentGateway;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.CustomObjects;
using DataAccess.Enums;
using ExtranetApp.Controllers;
using ExtranetApp.Models.Account;
using ExtranetApp.Models.Booking;
using ExtranetApp.Models.Home;
using ExtranetApp.Resources;
using ExtranetApp.Resources.Booking;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UnitTests.ExtranetControllersTests
{
    [TestClass]
    public class BookingControllerTest
    {
        [TestMethod]
        public void BookPropertyTest()
        {
            #region Setup Services
            
            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                InvoiceTotal = 1000
            };

            bookingService.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(invoice);
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel model = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            SessionRole roleGuest = new SessionRole()
            {
                Name = "RoleName",
                RoleId = 2, 
                RoleLevel = (int) RoleLevel.Guest
            };

            SessionRole roleNonGuest = new SessionRole()
            {
                Name = "RoleName",
                RoleId = 3,
                RoleLevel = (int)RoleLevel.Administrator
            };

            SessionUser user = new SessionUser()
            {
                Email = "test@test.com",
                FirstName = "FirstName",
                IsGeneratedPassword = false,
                CultureCode = "en-US",
                LastName = "LastName",
                LoggedInRoles = new List<SessionRole>() { roleGuest },
                UserID = 55
            };
            #endregion

            #region Booking as guest
            stateService.Setup(f => f.CurrentUser).Returns(user);
            bookingService.Setup(f => f.CheckUnitRatesForPeriod(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(true);

            var result = bookingController.BookProperty(model) as RedirectToRouteResult;
            var redirActionName = result.RouteValues.FirstOrDefault().Value;

            stateService.Verify(f => f.AddToSession("BookingDataInfo", model), Times.Once(), Messages.MethodWasNotCalled);
            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Contact", redirActionName);
            #endregion

            #region Booking as non guest
            user.LoggedInRoles.Clear();
            user.LoggedInRoles.Add(roleNonGuest);

            result = bookingController.BookProperty(model) as RedirectToRouteResult;
            redirActionName = result.RouteValues.FirstOrDefault().Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Welcome", redirActionName);
            #endregion
        }

        [TestMethod]
        public void WelcomeTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();

            PropertyDetailsInvoice invoice = new PropertyDetailsInvoice()
            {
                InvoiceTotal = 1000
            };

            bookingService.Setup(f => f.CalculateInvoice(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(invoice);
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();
                        
            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m"
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();

            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            UnitRate rate = new UnitRate()
            {
                DateFrom = new DateTime(2013, 10, 10),
                DateUntil = new DateTime(2013, 12, 12),
                Unit = unit,
                DailyRate = 200
            };

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            #endregion

            #region Mock services
            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            bookingService.Setup(f => f.CheckUnitRatesForPeriod(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(true);
            #endregion

            var result = bookingController.Welcome() as ViewResult;
            var returnedModel = result.Model as BookingProcessModel;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual(model.BookingInfoData.dateTripFrom, returnedModel.BookingInfoData.dateTripFrom);
            Assert.AreEqual(model.BookingInfoData.dateTripUntil, returnedModel.BookingInfoData.dateTripUntil);
            Assert.AreEqual(model.BookingInfoData.PropertyId, returnedModel.BookingInfoData.PropertyId);
            Assert.AreEqual(model.BookingInfoData.UnitId, returnedModel.BookingInfoData.UnitId);
            Assert.AreEqual(model.PropertyImages.First(), returnedModel.PropertyImages.First());
            ExtendedAssert.AreEqual(model.Property, returnedModel.Property);
            ExtendedAssert.AreEqual(model.Unit, returnedModel.Unit);
            ExtendedAssert.AreEqual(model.DestinationDetails, returnedModel.DestinationDetails);
            Assert.AreEqual(model.JoinModel.Email, returnedModel.JoinModel.Email);
            Assert.AreEqual(model.JoinModel.FirstName, returnedModel.JoinModel.FirstName);
            Assert.AreEqual(model.JoinModel.LastName, returnedModel.JoinModel.LastName);
            Assert.AreEqual(model.LoginModel.Email, returnedModel.LoginModel.Email);
            Assert.AreEqual((int)model.LoginModel.LoginRole, (int)returnedModel.LoginModel.LoginRole);
            Assert.AreEqual(model.LoginModel.Password, returnedModel.LoginModel.Password);
            Assert.AreEqual(model.PropertyUnitName, returnedModel.PropertyUnitName);
        }

        [TestMethod]
        public void ContactTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);

            List<DictionaryCountry> countries = new List<DictionaryCountry>();
            countries.Add(new DictionaryCountry()
            {
                CountryId = 1,
                CountryName_i18n = "Romania",
                CountryCode2Letters = "ro",
                CountryNameCurrentLanguage = "ro"
            });

            countries.Add(new DictionaryCountry()
            {
                CountryId = 2,
                CountryName_i18n = "Poland",
                CountryCode2Letters = "pl",
                CountryNameCurrentLanguage = "pl"
            });
            SelectList countryList = new SelectList(countries, "CountryCode2Letters", "CountryNameCurrentLanguage");

            UserProfileMinModel pm = new UserProfileMinModel()
            {
                AddressLine1 = user.Address1,
                City = user.City,
                Country = user.DictionaryCountry == null ? string.Empty : user.DictionaryCountry.CountryCode2Letters,
                Email = user.email,
                FirstName = user.Firstname,
                LastName = user.Lastname,
                PhoneNumber = user.CellPhone,
                State = user.State,
                ZIPCode = user.ZIPCode,
                CountryName = user.DictionaryCountry == null ? string.Empty : user.DictionaryCountry.CountryNameCurrentLanguage,
                UserId = user.UserID,
                Gender = user.Gender,
                BirthDay = user.BirthDate
            };

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.IsUnitAvailable(bookingInfo, null)).Returns(true);
            bookingService.Setup(f => f.DoTemporaryReservartion(bookingInfo, user)).Returns(5);
            dictionaryService.Setup(f => f.GetCountries()).Returns(countries);

            #endregion

            var result = bookingController.Contact() as ViewResult;
            var returnedModel = result.Model as BookingProcessModel;
            var returnedCountries = result.ViewData["Countries"] as SelectList;

            stateService.Verify(f => f.AddToSession("BookingReservationId", 5), Times.Once(), Messages.MethodWasNotCalled);

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);

            Assert.AreEqual(model.BookingInfoData.dateTripFrom, returnedModel.BookingInfoData.dateTripFrom);
            Assert.AreEqual(model.BookingInfoData.dateTripUntil, returnedModel.BookingInfoData.dateTripUntil);
            Assert.AreEqual(model.BookingInfoData.PropertyId, returnedModel.BookingInfoData.PropertyId);
            Assert.AreEqual(model.BookingInfoData.UnitId, returnedModel.BookingInfoData.UnitId);
            Assert.AreEqual(model.PropertyImages.First(), returnedModel.PropertyImages.First());
            ExtendedAssert.AreEqual(model.Property, returnedModel.Property);
            ExtendedAssert.AreEqual(model.Unit, returnedModel.Unit);
            ExtendedAssert.AreEqual(model.DestinationDetails, returnedModel.DestinationDetails);
            Assert.AreEqual(model.JoinModel.Email, returnedModel.JoinModel.Email);
            Assert.AreEqual(model.JoinModel.FirstName, returnedModel.JoinModel.FirstName);
            Assert.AreEqual(model.JoinModel.LastName, returnedModel.JoinModel.LastName);
            Assert.AreEqual(model.LoginModel.Email, returnedModel.LoginModel.Email);
            Assert.AreEqual((int)model.LoginModel.LoginRole, (int)returnedModel.LoginModel.LoginRole);
            Assert.AreEqual(model.LoginModel.Password, returnedModel.LoginModel.Password);
            Assert.AreEqual(model.PropertyUnitName, returnedModel.PropertyUnitName);

            Assert.AreEqual(countryList.Count(), returnedCountries.Count());
            Assert.AreEqual(countryList.ElementAt(0).Text, returnedCountries.ElementAt(0).Text);
            Assert.AreEqual(countryList.ElementAt(0).Value, returnedCountries.ElementAt(0).Value);
            Assert.AreEqual(countryList.ElementAt(1).Text, returnedCountries.ElementAt(1).Text);
            Assert.AreEqual(countryList.ElementAt(1).Value, returnedCountries.ElementAt(1).Value);
        }

        [TestMethod]
        public void ContactWithUnavaliableUnitTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);


            #region Setup objects 
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            SessionUser suser = new SessionUser(user);
            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.IsUnitAvailable(bookingInfo, null)).Returns(false);

            #endregion

            var result = bookingController.Contact() as RedirectToRouteResult;
            var redirActionName = result.RouteValues.FirstOrDefault().Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("PropertyNotAvailable", redirActionName);
        }

        [TestMethod]
        public void ContactWithVerificationTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            UserProfileMinModel userModel = new UserProfileMinModel()
            {
                AddressLine1 = "AddressLine1",
                AddressLine2 = "AddressLine2",
                BirthDay = DateTime.Now.AddYears(-29),
                City = "City",
                Country = "us",
                CountryName = "CountryName",
                Email = "test@test.com",
                FirstName = "FirstName",
                Gender = "Male",
                LastName = "LastName",
                PhoneNumber = "34252354234523",
                State = "State",
                UserId = 23,
                ZIPCode = "34533"
            };

            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();
            
            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } }
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.NumberOfGuests = 5;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            #endregion

            #region Mock services

            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.GetFromSession<int>("BookingReservationId")).Returns(5);
            
            #endregion

            #region Verification positive
            user.VerificationPositive = true;

            var result = bookingController.Contact(userModel, model) as RedirectToRouteResult;
            var redirActionName = result.RouteValues.FirstOrDefault().Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Payment", redirActionName);
            bookingService.Verify(f => f.UpdateReservationGuestNumber(model.NumberOfGuests.Value, 5));
            #endregion

            #region Verification negative
            user.VerificationPositive = false;

            result = bookingController.Contact(userModel, model) as RedirectToRouteResult;
            redirActionName = result.RouteValues.FirstOrDefault().Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("IdentityVerification", redirActionName);
            #endregion
        }

        [TestMethod]
        public void PropertyNotAvailableTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles =  new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } }
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.NumberOfGuests = 5;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();

            #endregion

            #region Mock services
            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            #endregion

            var result = bookingController.PropertyNotAvailable() as ViewResult;
            var returnedModel = result.Model as BookingProcessModel;
            var errTitle = result.ViewData["Error_Ttile"];
            var errSubTitle = result.ViewData["Error_Subtitle"];

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Selected property is not available", errTitle);
            Assert.AreEqual("We're sorry, selected property is no longer available. Choose another property or select different travel time.", errSubTitle);
            Assert.AreEqual("WizardFailure", result.ViewName);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual(model.BookingInfoData.dateTripFrom, returnedModel.BookingInfoData.dateTripFrom);
            Assert.AreEqual(model.BookingInfoData.dateTripUntil, returnedModel.BookingInfoData.dateTripUntil);
            Assert.AreEqual(model.BookingInfoData.PropertyId, returnedModel.BookingInfoData.PropertyId);
            Assert.AreEqual(model.BookingInfoData.UnitId, returnedModel.BookingInfoData.UnitId);
            Assert.AreEqual(model.PropertyImages.First(), returnedModel.PropertyImages.First());
            ExtendedAssert.AreEqual(model.Property, returnedModel.Property);
            ExtendedAssert.AreEqual(model.Unit, returnedModel.Unit);
            ExtendedAssert.AreEqual(model.DestinationDetails, returnedModel.DestinationDetails);
            Assert.AreEqual(model.JoinModel.Email, returnedModel.JoinModel.Email);
            Assert.AreEqual(model.JoinModel.FirstName, returnedModel.JoinModel.FirstName);
            Assert.AreEqual(model.JoinModel.LastName, returnedModel.JoinModel.LastName);
            Assert.AreEqual(model.LoginModel.Email, returnedModel.LoginModel.Email);
            Assert.AreEqual((int)model.LoginModel.LoginRole, (int)returnedModel.LoginModel.LoginRole);
            Assert.AreEqual(model.LoginModel.Password, returnedModel.LoginModel.Password);
            Assert.AreEqual(model.PropertyUnitName, returnedModel.PropertyUnitName);
            stateService.Verify(f => f.RemoveFromSession("BookingReservationId"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingIdentityVerificationInfo"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingDataInfo"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingResponseEVS"), Times.Once(), Messages.MethodWasNotCalled);
        }

        [TestMethod]
        public void IdentityVerificationTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "us",
                CountryCode3Letters = "usa"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();
            
            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "Smith",
                Firstname = "John",
                Address1 = "123 Main Str.",
                Address2 = "",
                State = "KY",
                ZIPCode = "40233",
                City = "Louisyville",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Parse("1950-01-31"),
                LoggedInRoles =  new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();

            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            #endregion

            #region EVS Response

            EVSResponse response = new EVSResponse();
            
            response.TransactionDetails = new TransactionDetails()
            {
                CustomerReference = "123456",
                DataProviderDuration = "123",
                Errors = null,
                TotalDuration = "1",
                TransactionDate = DateTime.Now.ToShortDateString(),
                TransactionId = "12",
                Warnings = null
            };
            response.TransactionDetails.Product = new Product()
            {
                Name = "ProductName",
                Version = "ProductVersion"
            };

            response.UserIdentityData = new UserIdentityData()
            {
                AddressHighRiskResult = new DataVerificationResult() { Code = "N", ResultText = "AddressHighRiskResult"},
                AddressTypeResult = new DataVerificationResult() { Code = "S", ResultText = "AddressTypeResult" },
                AuthenticationScore = "221",
                ChangeOfAddressResult = new DataVerificationResult() { Code = "N", ResultText = "" },
                CheckpointScore = "444",
                DateOfBirthResult = new DataVerificationResult() { Code = "1", ResultText = "" },
                DriverLicenseResult = new DataVerificationResult() { Code = "M", ResultText = "" },
                PhoneHighRiskResult = new DataVerificationResult() { Code = "M", ResultText = "PhoneHighRiskResult" },
                PhoneVerificationResult = new DataVerificationResult() { Code = "M", ResultText = "PhoneVerificationResult" },
                PrimaryResult = "PrimaryResult",
                SocialSecurityNumberResult = new DataVerificationResult() { Code = "YB", ResultText = "" }
            };
            Question[] questions = new Question[6];

            for (int i = 0; i < 6; i++)
            {
                questions[i] = new Question();
                questions[i].QuestionText = "Question " + i.ToString() + " ?";
                questions[i].QuestionType = i;
                questions[i].Answers = new Answer[10];
                for (int j = 0; j < 10; j++)
                {
                    questions[i].Answers[j] = new Answer();
                    questions[i].Answers[j].IsCorrect = (i == j) ? true : false;
                    questions[i].Answers[j].AnswerText = "Answer " + j.ToString();
                }
            }
            response.UserIdentityData.Questions = questions;

            var questionModelList = new List<IdentityVerificationQuestionModel>();

            questions.ToList().ForEach(delegate(Question q)
            {
                IdentityVerificationQuestionModel questionModel = new IdentityVerificationQuestionModel()
                {
                    QuestionText = q.QuestionText,
                    QuestionType = q.QuestionType
                };

                int aId = 1;

                q.Answers.ToList().ForEach(delegate(Answer a)
                {
                    IdentityVerificationAnswerModel answerModel = new IdentityVerificationAnswerModel()
                    {
                        AnswerId = aId,
                        AnswerText = a.AnswerText,
                        IsCorrect = a.IsCorrect
                    };
                    aId++;

                    questionModel.Answers.Add(answerModel);
                });

                questionModelList.Add(questionModel);
            });

            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.GetEVSQuestions(It.IsAny<int>())).Returns(response);

            #endregion

            var result = bookingController.IdentityVerification() as ViewResult;
            var returnedModel = (result.Model as BookingProcessModel).IdentityVerification as List<IdentityVerificationQuestionModel>;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual(2, returnedModel.Count);

            for (int i = 0; i < 2; i++)
            {
                Assert.AreEqual(questionModelList[i].QuestionText, returnedModel[i].QuestionText);
                Assert.AreEqual(questionModelList[i].QuestionType, returnedModel[i].QuestionType);
                Assert.AreEqual(questionModelList[i].SelectedAnswer, returnedModel[i].SelectedAnswer);

                for (int j = 0; j < 10; j++)
                {
                    Assert.AreEqual(questionModelList[i].Answers[j].AnswerText, returnedModel[i].Answers[j].AnswerText);
                    Assert.AreEqual(questionModelList[i].Answers[j].IsCorrect, returnedModel[i].Answers[j].IsCorrect);
                    Assert.AreEqual(questionModelList[i].Answers[j].AnswerId, returnedModel[i].Answers[j].AnswerId);
                }
            }

            // todo: with null questionModelList list
            // todo: not us country code
        }

        [TestMethod]
        public void IdentityVerification2QuestionsTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "us",
                CountryCode3Letters = "usa"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "Smith",
                Firstname = "John",
                Address1 = "123 Main Str.",
                Address2 = "",
                State = "KY",
                ZIPCode = "40233",
                City = "Louisyville",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Parse("1950-01-31"),
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);

            #endregion

            #region EVS Response

            EVSResponse response = new EVSResponse();

            response.TransactionDetails = new TransactionDetails()
            {
                CustomerReference = "123456",
                DataProviderDuration = "123",
                Errors = null,
                TotalDuration = "1",
                TransactionDate = DateTime.Now.ToShortDateString(),
                TransactionId = "12",
                Warnings = null
            };
            response.TransactionDetails.Product = new Product()
            {
                Name = "ProductName",
                Version = "ProductVersion"
            };

            response.UserIdentityData = new UserIdentityData()
            {
                AddressHighRiskResult = new DataVerificationResult() { Code = "N", ResultText = "AddressHighRiskResult" },
                AddressTypeResult = new DataVerificationResult() { Code = "S", ResultText = "AddressTypeResult" },
                AuthenticationScore = "221",
                ChangeOfAddressResult = new DataVerificationResult() { Code = "N", ResultText = "" },
                CheckpointScore = "444",
                DateOfBirthResult = new DataVerificationResult() { Code = "1", ResultText = "" },
                DriverLicenseResult = new DataVerificationResult() { Code = "M", ResultText = "" },
                PhoneHighRiskResult = new DataVerificationResult() { Code = "M", ResultText = "PhoneHighRiskResult" },
                PhoneVerificationResult = new DataVerificationResult() { Code = "M", ResultText = "PhoneVerificationResult" },
                PrimaryResult = "PrimaryResult",
                SocialSecurityNumberResult = new DataVerificationResult() { Code = "YB", ResultText = "" }
            };

            Question[] questions = new Question[2];

            for (int i = 0; i < 2; i++)
            {
                questions[i] = new Question();
                questions[i].QuestionText = "Question " + i.ToString() + " ?";
                questions[i].QuestionType = i;
                questions[i].Answers = new Answer[10];
                for (int j = 0; j < 10; j++)
                {
                    questions[i].Answers[j] = new Answer();
                    questions[i].Answers[j].IsCorrect = (i == j) ? true : false;
                    questions[i].Answers[j].AnswerText = "Answer " + j.ToString();
                }
            }
            response.UserIdentityData.Questions = questions;

            var questionModelList = new List<IdentityVerificationQuestionModel>();

            questions.ToList().ForEach(delegate(Question q)
            {
                IdentityVerificationQuestionModel questionModel = new IdentityVerificationQuestionModel()
                {
                    QuestionText = q.QuestionText,
                    QuestionType = q.QuestionType
                };

                int aId = 0;

                q.Answers.ToList().ForEach(delegate(Answer a)
                {
                    IdentityVerificationAnswerModel answerModel = new IdentityVerificationAnswerModel()
                    {
                        AnswerId = aId,
                        AnswerText = a.AnswerText,
                        IsCorrect = a.IsCorrect
                    };
                    aId++;

                    questionModel.Answers.Add(answerModel);
                });

                questionModelList.Add(questionModel);
            });

            #region Setup answers

            questionModelList[0].SelectedAnswer = 0;
            questionModelList[1].SelectedAnswer = 1;

            #endregion

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            model.IdentityVerification = questionModelList;

            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), false)).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.GetEVSQuestions(It.IsAny<int>())).Returns(response);
            stateService.Setup(f => f.GetFromSession<List<IdentityVerificationQuestionModel>>("BookingIdentityVerificationInfo")).Returns(questionModelList);
            stateService.Setup(f => f.GetFromSession<IEVSResponse>("BookingResponseEVS")).Returns(response);
            
            #endregion

            var result = bookingController.IdentityVerification(model) as RedirectToRouteResult;
            var redirActionName = result.RouteValues.FirstOrDefault().Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Payment", redirActionName);

            bookingService.Verify(f => f.SetVerificationInfo(user, true, It.IsAny<IEVSResponse>()), Times.Once(), Messages.MethodWasNotCalled);
        }

        [TestMethod]
        public void IdentityVerification4QuestionsTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "us",
                CountryCode3Letters = "usa"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "Smith",
                Firstname = "John",
                Address1 = "123 Main Str.",
                Address2 = "",
                State = "KY",
                ZIPCode = "40233",
                City = "Louisyville",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
               // VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Parse("1950-01-31"),
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();

            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);

            #endregion

            #region EVS Response

            EVSResponse response = new EVSResponse();

            response.TransactionDetails = new TransactionDetails()
            {
                CustomerReference = "123456",
                DataProviderDuration = "123",
                Errors = null,
                TotalDuration = "1",
                TransactionDate = DateTime.Now.ToShortDateString(),
                TransactionId = "12",
                Warnings = null
            };
            response.TransactionDetails.Product = new Product()
            {
                Name = "ProductName",
                Version = "ProductVersion"
            };

            response.UserIdentityData = new UserIdentityData()
            {
                AddressHighRiskResult = new DataVerificationResult() { Code = "N", ResultText = "AddressHighRiskResult" },
                AddressTypeResult = new DataVerificationResult() { Code = "S", ResultText = "AddressTypeResult" },
                AuthenticationScore = "221",
                ChangeOfAddressResult = new DataVerificationResult() { Code = "N", ResultText = "" },
                CheckpointScore = "444",
                DateOfBirthResult = new DataVerificationResult() { Code = "1", ResultText = "" },
                DriverLicenseResult = new DataVerificationResult() { Code = "M", ResultText = "" },
                PhoneHighRiskResult = new DataVerificationResult() { Code = "M", ResultText = "PhoneHighRiskResult" },
                PhoneVerificationResult = new DataVerificationResult() { Code = "M", ResultText = "PhoneVerificationResult" },
                PrimaryResult = "PrimaryResult",
                SocialSecurityNumberResult = new DataVerificationResult() { Code = "YB", ResultText = "" }
            };

            Question[] questions = new Question[4];

            for (int i = 0; i < 4; i++)
            {
                questions[i] = new Question();
                questions[i].QuestionText = "Question " + i.ToString() + " ?";
                questions[i].QuestionType = i;
                questions[i].Answers = new Answer[10];
                for (int j = 0; j < 10; j++)
                {
                    questions[i].Answers[j] = new Answer();
                    questions[i].Answers[j].IsCorrect = (i == j) ? true : false;
                    questions[i].Answers[j].AnswerText = "Answer " + j.ToString();
                }
            }
            response.UserIdentityData.Questions = questions;

            var questionModelList = new List<IdentityVerificationQuestionModel>();

            questions.ToList().ForEach(delegate(Question q)
            {
                IdentityVerificationQuestionModel questionModel = new IdentityVerificationQuestionModel()
                {
                    QuestionText = q.QuestionText,
                    QuestionType = q.QuestionType
                };

                int aId = 0;

                q.Answers.ToList().ForEach(delegate(Answer a)
                {
                    IdentityVerificationAnswerModel answerModel = new IdentityVerificationAnswerModel()
                    {
                        AnswerId = aId,
                        AnswerText = a.AnswerText,
                        IsCorrect = a.IsCorrect
                    };
                    aId++;

                    questionModel.Answers.Add(answerModel);
                });

                questionModelList.Add(questionModel);
            });

            #region Setup answers

            questionModelList[0].SelectedAnswer = 9;
            questionModelList[1].SelectedAnswer = 9;
            questionModelList[2].SelectedAnswer = 2;
            questionModelList[3].SelectedAnswer = 3;

            #endregion

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            model.IdentityVerification = questionModelList;

            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.GetEVSQuestions(It.IsAny<int>())).Returns(response);
            stateService.Setup(f => f.GetFromSession<List<IdentityVerificationQuestionModel>>("BookingIdentityVerificationInfo")).Returns(questionModelList);
            stateService.Setup(f => f.GetFromSession<IEVSResponse>("BookingResponseEVS")).Returns(response);
            bookingService.Setup(f => f.SetVerificationInfo(user, false, It.IsAny<IEVSResponse>())).Callback<User, bool, IEVSResponse>((User u, bool b, IEVSResponse r) =>
                {
                    u.VerificationPositive = false;
                });

            #endregion

            var result = bookingController.IdentityVerification(model) as ViewResult;
            var returnedModel = (result.Model as BookingProcessModel).IdentityVerification as List<IdentityVerificationQuestionModel>;

            model.IdentityVerification = model.IdentityVerification.Skip(2).Take(2).ToList();

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual(2, returnedModel.Count);

            for (int i = 0; i < 2; i++)
            {
                Assert.AreEqual(model.IdentityVerification[i].QuestionText, returnedModel[i].QuestionText);
                Assert.AreEqual(model.IdentityVerification[i].QuestionType, returnedModel[i].QuestionType);
                Assert.AreEqual(model.IdentityVerification[i].SelectedAnswer, returnedModel[i].SelectedAnswer);

                for (int j = 0; j < 10; j++)
                {
                    Assert.AreEqual(model.IdentityVerification[i].Answers[j].AnswerText, returnedModel[i].Answers[j].AnswerText);
                    Assert.AreEqual(model.IdentityVerification[i].Answers[j].IsCorrect, returnedModel[i].Answers[j].IsCorrect);
                    Assert.AreEqual(model.IdentityVerification[i].Answers[j].AnswerId, returnedModel[i].Answers[j].AnswerId);
                }
            }

            bookingService.Verify(f => f.SetVerificationInfo(user, false, It.IsAny<IEVSResponse>()), Times.Once(), Messages.MethodWasNotCalled);
        }

        [TestMethod]
        public void IdentityVerification3QuestionsTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "us",
                CountryCode3Letters = "usa"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "Smith",
                Firstname = "John",
                Address1 = "123 Main Str.",
                Address2 = "",
                State = "KY",
                ZIPCode = "40233",
                City = "Louisyville",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                // VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Parse("1950-01-31"),
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);

            #endregion

            #region EVS Response

            EVSResponse response = new EVSResponse();

            response.TransactionDetails = new TransactionDetails()
            {
                CustomerReference = "123456",
                DataProviderDuration = "123",
                Errors = null,
                TotalDuration = "1",
                TransactionDate = DateTime.Now.ToShortDateString(),
                TransactionId = "12",
                Warnings = null
            };
            response.TransactionDetails.Product = new Product()
            {
                Name = "ProductName",
                Version = "ProductVersion"
            };

            response.UserIdentityData = new UserIdentityData()
            {
                AddressHighRiskResult = new DataVerificationResult() { Code = "N", ResultText = "AddressHighRiskResult" },
                AddressTypeResult = new DataVerificationResult() { Code = "S", ResultText = "AddressTypeResult" },
                AuthenticationScore = "221",
                ChangeOfAddressResult = new DataVerificationResult() { Code = "N", ResultText = "" },
                CheckpointScore = "444",
                DateOfBirthResult = new DataVerificationResult() { Code = "1", ResultText = "" },
                DriverLicenseResult = new DataVerificationResult() { Code = "M", ResultText = "" },
                PhoneHighRiskResult = new DataVerificationResult() { Code = "M", ResultText = "PhoneHighRiskResult" },
                PhoneVerificationResult = new DataVerificationResult() { Code = "M", ResultText = "PhoneVerificationResult" },
                PrimaryResult = "PrimaryResult",
                SocialSecurityNumberResult = new DataVerificationResult() { Code = "YB", ResultText = "" }
            };

            Question[] questions = new Question[3];

            for (int i = 0; i < 3; i++)
            {
                questions[i] = new Question();
                questions[i].QuestionText = "Question " + i.ToString() + " ?";
                questions[i].QuestionType = i;
                questions[i].Answers = new Answer[10];
                for (int j = 0; j < 10; j++)
                {
                    questions[i].Answers[j] = new Answer();
                    questions[i].Answers[j].IsCorrect = (i == j) ? true : false;
                    questions[i].Answers[j].AnswerText = "Answer " + j.ToString();
                }
            }
            response.UserIdentityData.Questions = questions;

            var questionModelList = new List<IdentityVerificationQuestionModel>();

            questions.ToList().ForEach(delegate(Question q)
            {
                IdentityVerificationQuestionModel questionModel = new IdentityVerificationQuestionModel()
                {
                    QuestionText = q.QuestionText,
                    QuestionType = q.QuestionType
                };

                int aId = 0;

                q.Answers.ToList().ForEach(delegate(Answer a)
                {
                    IdentityVerificationAnswerModel answerModel = new IdentityVerificationAnswerModel()
                    {
                        AnswerId = aId,
                        AnswerText = a.AnswerText,
                        IsCorrect = a.IsCorrect
                    };
                    aId++;

                    questionModel.Answers.Add(answerModel);
                });

                questionModelList.Add(questionModel);
            });

            #region Setup answers

            questionModelList[0].SelectedAnswer = 9;
            questionModelList[1].SelectedAnswer = 1;
            questionModelList[2].SelectedAnswer = 2;

            #endregion

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            model.IdentityVerification = questionModelList;

            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.GetEVSQuestions(It.IsAny<int>())).Returns(response);
            stateService.Setup(f => f.GetFromSession<List<IdentityVerificationQuestionModel>>("BookingIdentityVerificationInfo")).Returns(questionModelList);
            stateService.Setup(f => f.GetFromSession<IEVSResponse>("BookingResponseEVS")).Returns(response);
            bookingService.Setup(f => f.SetVerificationInfo(user, false, It.IsAny<IEVSResponse>())).Callback<User, bool, IEVSResponse>((User u, bool b, IEVSResponse r) =>
            {
                u.VerificationPositive = false;
            });

            #endregion

            var result = bookingController.IdentityVerification(model) as ViewResult;
            var returnedModel = (result.Model as BookingProcessModel).IdentityVerification as List<IdentityVerificationQuestionModel>;

            model.IdentityVerification = model.IdentityVerification.Skip(1).Take(2).ToList();

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual(2, returnedModel.Count);

            for (int i = 0; i < 2; i++)
            {
                Assert.AreEqual(model.IdentityVerification[i].QuestionText, returnedModel[i].QuestionText);
                Assert.AreEqual(model.IdentityVerification[i].QuestionType, returnedModel[i].QuestionType);
                Assert.AreEqual(model.IdentityVerification[i].SelectedAnswer, returnedModel[i].SelectedAnswer);

                for (int j = 0; j < 10; j++)
                {
                    Assert.AreEqual(model.IdentityVerification[i].Answers[j].AnswerText, returnedModel[i].Answers[j].AnswerText);
                    Assert.AreEqual(model.IdentityVerification[i].Answers[j].IsCorrect, returnedModel[i].Answers[j].IsCorrect);
                    Assert.AreEqual(model.IdentityVerification[i].Answers[j].AnswerId, returnedModel[i].Answers[j].AnswerId);
                }
            }

            bookingService.Verify(f => f.SetVerificationInfo(user, false, It.IsAny<IEVSResponse>()), Times.Once(), Messages.MethodWasNotCalled);
        }

        [TestMethod]
        public void IdentityVerification4QuestionsFailedTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "us",
                CountryCode3Letters = "usa"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "Smith",
                Firstname = "John",
                Address1 = "123 Main Str.",
                Address2 = "",
                State = "KY",
                ZIPCode = "40233",
                City = "Louisyville",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                // VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                BirthDate = DateTime.Parse("1950-01-31"),
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);

            #endregion

            #region EVS Response

            EVSResponse response = new EVSResponse();

            response.TransactionDetails = new TransactionDetails()
            {
                CustomerReference = "123456",
                DataProviderDuration = "123",
                Errors = null,
                TotalDuration = "1",
                TransactionDate = DateTime.Now.ToShortDateString(),
                TransactionId = "12",
                Warnings = null
            };
            response.TransactionDetails.Product = new Product()
            {
                Name = "ProductName",
                Version = "ProductVersion"
            };

            response.UserIdentityData = new UserIdentityData()
            {
                AddressHighRiskResult = new DataVerificationResult() { Code = "N", ResultText = "AddressHighRiskResult" },
                AddressTypeResult = new DataVerificationResult() { Code = "S", ResultText = "AddressTypeResult" },
                AuthenticationScore = "221",
                ChangeOfAddressResult = new DataVerificationResult() { Code = "N", ResultText = "" },
                CheckpointScore = "444",
                DateOfBirthResult = new DataVerificationResult() { Code = "1", ResultText = "" },
                DriverLicenseResult = new DataVerificationResult() { Code = "M", ResultText = "" },
                PhoneHighRiskResult = new DataVerificationResult() { Code = "M", ResultText = "PhoneHighRiskResult" },
                PhoneVerificationResult = new DataVerificationResult() { Code = "M", ResultText = "PhoneVerificationResult" },
                PrimaryResult = "PrimaryResult",
                SocialSecurityNumberResult = new DataVerificationResult() { Code = "YB", ResultText = "" }
            };

            Question[] questions = new Question[4];

            for (int i = 0; i < 4; i++)
            {
                questions[i] = new Question();
                questions[i].QuestionText = "Question " + i.ToString() + " ?";
                questions[i].QuestionType = i;
                questions[i].Answers = new Answer[10];
                for (int j = 0; j < 10; j++)
                {
                    questions[i].Answers[j] = new Answer();
                    questions[i].Answers[j].IsCorrect = (i == j) ? true : false;
                    questions[i].Answers[j].AnswerText = "Answer " + j.ToString();
                }
            }
            response.UserIdentityData.Questions = questions;

            var questionModelList = new List<IdentityVerificationQuestionModel>();

            questions.ToList().ForEach(delegate(Question q)
            {
                IdentityVerificationQuestionModel questionModel = new IdentityVerificationQuestionModel()
                {
                    QuestionText = q.QuestionText,
                    QuestionType = q.QuestionType
                };

                int aId = 0;

                q.Answers.ToList().ForEach(delegate(Answer a)
                {
                    IdentityVerificationAnswerModel answerModel = new IdentityVerificationAnswerModel()
                    {
                        AnswerId = aId,
                        AnswerText = a.AnswerText,
                        IsCorrect = a.IsCorrect
                    };
                    aId++;

                    questionModel.Answers.Add(answerModel);
                });

                questionModelList.Add(questionModel);
            });

            #region Setup answers

            questionModelList[0].SelectedAnswer = 9;
            questionModelList[1].SelectedAnswer = 9;
            questionModelList[2].SelectedAnswer = 9;
            questionModelList[3].SelectedAnswer = 9;

            #endregion

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            model.IdentityVerification = questionModelList;

            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.GetEVSQuestions(It.IsAny<int>())).Returns(response);
            stateService.Setup(f => f.GetFromSession<List<IdentityVerificationQuestionModel>>("BookingIdentityVerificationInfo")).Returns(questionModelList);
            stateService.Setup(f => f.GetFromSession<IEVSResponse>("BookingResponseEVS")).Returns(response);
            bookingService.Setup(f => f.SetVerificationInfo(user, false, It.IsAny<IEVSResponse>())).Callback<User, bool, IEVSResponse>((User u, bool b, IEVSResponse r) =>
            {
                u.VerificationPositive = false;
            });

            #endregion

            var result = bookingController.IdentityVerification(model) as ViewResult;
            var returnedModel = (result.Model as BookingProcessModel).IdentityVerification as List<IdentityVerificationQuestionModel>;
            var result2 = bookingController.IdentityVerification((result.Model as BookingProcessModel)) as RedirectToRouteResult;
            var redirActionName = result2.RouteValues.FirstOrDefault().Value;

            model.IdentityVerification = model.IdentityVerification.Skip(2).Take(2).ToList();

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.IsNotNull(result2, Messages.ObjectIsNull);
            Assert.IsNotNull(redirActionName, Messages.ObjectIsNull);
            Assert.AreEqual("IdentityVerificationFailure", redirActionName);
            Assert.AreEqual(2, returnedModel.Count);

            for (int i = 0; i < 2; i++)
            {
                Assert.AreEqual(model.IdentityVerification[i].QuestionText, returnedModel[i].QuestionText);
                Assert.AreEqual(model.IdentityVerification[i].QuestionType, returnedModel[i].QuestionType);
                Assert.AreEqual(model.IdentityVerification[i].SelectedAnswer, returnedModel[i].SelectedAnswer);

                for (int j = 0; j < 10; j++)
                {
                    Assert.AreEqual(model.IdentityVerification[i].Answers[j].AnswerText, returnedModel[i].Answers[j].AnswerText);
                    Assert.AreEqual(model.IdentityVerification[i].Answers[j].IsCorrect, returnedModel[i].Answers[j].IsCorrect);
                    Assert.AreEqual(model.IdentityVerification[i].Answers[j].AnswerId, returnedModel[i].Answers[j].AnswerId);
                }
            }
        }

        [TestMethod]
        public void IdentityVerificationFailureTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } }
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.NumberOfGuests = 5;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();

            #endregion

            #region Mock services
            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            #endregion

            var result = bookingController.IdentityVerificationFailure() as ViewResult;
            var returnedModel = result.Model as BookingProcessModel;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual(model.BookingInfoData.dateTripFrom, returnedModel.BookingInfoData.dateTripFrom);
            Assert.AreEqual(model.BookingInfoData.dateTripUntil, returnedModel.BookingInfoData.dateTripUntil);
            Assert.AreEqual(model.BookingInfoData.PropertyId, returnedModel.BookingInfoData.PropertyId);
            Assert.AreEqual(model.BookingInfoData.UnitId, returnedModel.BookingInfoData.UnitId);
            Assert.AreEqual(model.PropertyImages.First(), returnedModel.PropertyImages.First());
            ExtendedAssert.AreEqual(model.Property, returnedModel.Property);
            ExtendedAssert.AreEqual(model.Unit, returnedModel.Unit);
            ExtendedAssert.AreEqual(model.DestinationDetails, returnedModel.DestinationDetails);
            Assert.AreEqual(model.JoinModel.Email, returnedModel.JoinModel.Email);
            Assert.AreEqual(model.JoinModel.FirstName, returnedModel.JoinModel.FirstName);
            Assert.AreEqual(model.JoinModel.LastName, returnedModel.JoinModel.LastName);
            Assert.AreEqual(model.LoginModel.Email, returnedModel.LoginModel.Email);
            Assert.AreEqual((int)model.LoginModel.LoginRole, (int)returnedModel.LoginModel.LoginRole);
            Assert.AreEqual(model.LoginModel.Password, returnedModel.LoginModel.Password);
            Assert.AreEqual(model.PropertyUnitName, returnedModel.PropertyUnitName);

            stateService.Verify(f => f.RemoveFromSession("BookingReservationId"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingIdentityVerificationInfo"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingDataInfo"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingResponseEVS"), Times.Once(), Messages.MethodWasNotCalled);
        }

        [TestMethod]
        public void InvoiceValueFailureTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue( "PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();

            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);

            List<DictionaryCountry> countries = new List<DictionaryCountry>();
            countries.Add(new DictionaryCountry()
            {
                CountryId = 1,
                CountryName_i18n = "Romania",
                CountryCode2Letters = "ro",
                CountryNameCurrentLanguage = "ro"
            });

            countries.Add(new DictionaryCountry()
            {
                CountryId = 2,
                CountryName_i18n = "Poland",
                CountryCode2Letters = "pl",
                CountryNameCurrentLanguage = "pl"
            });
            SelectList countryList = new SelectList(countries, "CountryCode2Letters", "CountryNameCurrentLanguage");

            UserProfileMinModel pm = new UserProfileMinModel()
            {
                AddressLine1 = user.Address1,
                City = user.City,
                Country = user.DictionaryCountry == null ? string.Empty : user.DictionaryCountry.CountryCode2Letters,
                Email = user.email,
                FirstName = user.Firstname,
                LastName = user.Lastname,
                PhoneNumber = user.CellPhone,
                State = user.State,
                ZIPCode = user.ZIPCode,
                CountryName = user.DictionaryCountry == null ? string.Empty : user.DictionaryCountry.CountryNameCurrentLanguage,
                UserId = user.UserID,
                Gender = user.Gender,
                BirthDay = user.BirthDate
            };

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.IsUnitAvailable(bookingInfo, null)).Returns(true);
            bookingService.Setup(f => f.DoTemporaryReservartion(bookingInfo, user)).Returns(5);
            dictionaryService.Setup(f => f.GetCountries()).Returns(countries);

            #endregion

            ViewResult result = (ViewResult) bookingController.InvoiceValueFailure();

            Assert.AreEqual(BookingProcess.InvoiceRateNotSet_Title, result.ViewData["Error_Ttile"]);
            Assert.AreEqual(BookingProcess.InvoiceRateNotSet_Description, result.ViewData["Error_Subtitle"]);

            stateService.Verify(f => f.RemoveFromSession("BookingReservationId"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingIdentityVerificationInfo"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingDataInfo"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingResponseEVS"), Times.Once(), Messages.MethodWasNotCalled);
        }

        [TestMethod]
        public void AgeVerificationFailTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                FullName = "FullName",
                BirthDate = DateTime.Now,
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            SessionUser suser = new SessionUser(user);

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.NumberOfGuests = 5;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();

            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), false)).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            settingsService.Setup(f => f.GetSettingValue("EVS.FailNotifyEmail")).Returns("test@test.com");
            authorizationService.Setup(f => f.SingOutUser());

            #endregion

            var result = bookingController.AgeVerificationFail() as RedirectToRouteResult;
            var redirActionName = result.RouteValues.FirstOrDefault().Value;
            var redirControllerName = result.RouteValues.ElementAt(1).Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Index", redirActionName);
            Assert.AreEqual("Home", redirControllerName);

            messageService.Verify(f => f.AddEmail(It.IsAny<PaymentAgeVerificationEmail>(), "en-US", null, null), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingReservationId"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingIdentityVerificationInfo"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingDataInfo"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingResponseEVS"), Times.Once(), Messages.MethodWasNotCalled);
        }

        [TestMethod]
        public void PaymentFailureTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();
                        
            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } }
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();

            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.NumberOfGuests = 5;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();

            #endregion

            #region Mock services
            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            #endregion

            var result = bookingController.PaymentFailure() as ViewResult;
            var returnedModel = result.Model as BookingProcessModel;
            var errTitle = result.ViewData["Error_Ttile"];
            var errSubTitle = result.ViewData["Error_Subtitle"];

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Payment process error", errTitle);
            Assert.AreEqual("We are sorry, there was an error on processing your payment. Booking process cannot be completed.<br /> Please make sure you input valid credit card data and try again later.", errSubTitle);
            Assert.AreEqual("WizardFailure", result.ViewName);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);
            Assert.AreEqual(model.BookingInfoData.dateTripFrom, returnedModel.BookingInfoData.dateTripFrom);
            Assert.AreEqual(model.BookingInfoData.dateTripUntil, returnedModel.BookingInfoData.dateTripUntil);
            Assert.AreEqual(model.BookingInfoData.PropertyId, returnedModel.BookingInfoData.PropertyId);
            Assert.AreEqual(model.BookingInfoData.UnitId, returnedModel.BookingInfoData.UnitId);
            Assert.AreEqual(model.PropertyImages.First(), returnedModel.PropertyImages.First());
            ExtendedAssert.AreEqual(model.Property, returnedModel.Property);
            ExtendedAssert.AreEqual(model.Unit, returnedModel.Unit);
            ExtendedAssert.AreEqual(model.DestinationDetails, returnedModel.DestinationDetails);
            Assert.AreEqual(model.JoinModel.Email, returnedModel.JoinModel.Email);
            Assert.AreEqual(model.JoinModel.FirstName, returnedModel.JoinModel.FirstName);
            Assert.AreEqual(model.JoinModel.LastName, returnedModel.JoinModel.LastName);
            Assert.AreEqual(model.LoginModel.Email, returnedModel.LoginModel.Email);
            Assert.AreEqual((int)model.LoginModel.LoginRole, (int)returnedModel.LoginModel.LoginRole);
            Assert.AreEqual(model.LoginModel.Password, returnedModel.LoginModel.Password);
            Assert.AreEqual(model.PropertyUnitName, returnedModel.PropertyUnitName);
            stateService.Verify(f => f.RemoveFromSession("BookingReservationId"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingIdentityVerificationInfo"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingDataInfo"), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession("BookingResponseEVS"), Times.Once(), Messages.MethodWasNotCalled);
        }

        [TestMethod]
        public void SaveTripDetailDocTest()
        {
            #region Setup Services

            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion


            var httpRequest = new Mock<HttpRequestBase>();
            var httpResponse = new Mock<HttpResponseBase>();
            var httpContext = new Mock<HttpContextBase>();

            httpContext.Setup(p => p.Request).Returns(httpRequest.Object);
            httpContext.Setup(p => p.Response).Returns(httpResponse.Object);

            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            bookingController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), bookingController);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();
           
            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22),
                DictionaryCulture = culture
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);

            List<DictionaryCountry> countries = new List<DictionaryCountry>();
            countries.Add(new DictionaryCountry()
            {
                CountryId = 1,
                CountryName_i18n = "Romania",
                CountryCode2Letters = "ro",
                CountryNameCurrentLanguage = "ro"
            });

            countries.Add(new DictionaryCountry()
            {
                CountryId = 2,
                CountryName_i18n = "Poland",
                CountryCode2Letters = "pl",
                CountryNameCurrentLanguage = "pl"
            });
            SelectList countryList = new SelectList(countries, "CountryId", "CountryNameCurrentLanguage");

            UserProfileMinModel pm = new UserProfileMinModel()
            {
                AddressLine1 = user.Address1,
                City = user.City,
                Country = user.DictionaryCountry == null ? string.Empty : user.DictionaryCountry.CountryCode2Letters,
                Email = user.email,
                FirstName = user.Firstname,
                LastName = user.Lastname,
                PhoneNumber = user.CellPhone,
                State = user.State,
                ZIPCode = user.ZIPCode,
                CountryName = user.DictionaryCountry == null ? String.Empty : user.DictionaryCountry.CountryNameCurrentLanguage,
                UserId = user.UserID,
                Gender = user.Gender,
                BirthDay = user.BirthDate
            };

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            model.InvoiceData = new PropertyDetailsInvoice() 
            {
                Culture = new CultureInfo("en-US"),
                InvoiceTotal = 234.0m,
                LengthOfStay = 23,
            };

            string confirmationId = "1324";

            DateTime start = bookingInfo.dateTripFrom;
            DateTime bookingDate = DateTime.Now;
            string cancellationText;
            CultureInfo cInfoUser = new CultureInfo(CultureCode.en_US.ToString());
            double diffrenceBetweenArrivalAndNow = (start - DateTime.Now).TotalDays;
            double diffrenceBetweenBookingAndNow = (DateTime.Now - bookingDate).TotalHours;
            double diffrenceBetweenBookingAndArrival = (start - bookingDate).TotalHours;
            DateTime h24AfterBooking = bookingDate.AddHours(24);
            DateTime d30BeforeArrival = start.AddDays(-30);

            if (diffrenceBetweenArrivalAndNow >= 30)
            {
                if (diffrenceBetweenBookingAndNow <= 24)
                {
                    cancellationText = String.Format(CommonFrontEnd.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser));
                }
                else
                {
                    cancellationText = String.Format(CommonFrontEnd.CancellationPolicyHoursHalfRefund, d30BeforeArrival.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser));
                }
            }
            else
            {
                if (diffrenceBetweenArrivalAndNow >= 15)
                {
                    if (diffrenceBetweenBookingAndNow <= 24)
                    {
                        cancellationText = String.Format(CommonFrontEnd.CancellationPolicyLessThen24HoursFullRefund, h24AfterBooking.ToString("hh:mm tt, MMMM dd, yyyy", cInfoUser));
                    }
                    else
                    {
                        cancellationText = CommonFrontEnd.CancellationPolicyTryingToResell;
                    }
                }
                else
                {
                    cancellationText = CommonFrontEnd.CancellationPolicyTryingToResell;
                }
            }

            TripDetails tripDetails = new TripDetails();
            tripDetails.GuestFullName = string.Format("{0}", suser.FirstName);
            tripDetails.BookedDate = DateTime.Now.ToString("g");
            tripDetails.CheckIn = bookingInfo.dateTripFrom.ToLongDateString();
            tripDetails.CheckOut = bookingInfo.dateTripUntil.ToLongDateString();
            tripDetails.CheckInTime = new DateTime(model.Property.CheckIn.Ticks).ToString("t");
            tripDetails.CheckOutTime = new DateTime(model.Property.CheckOut.Ticks).ToString("t");
            tripDetails.ConfirmationCode = confirmationId;
            tripDetails.DateBeforeArrival = bookingInfo.dateTripFrom.AddDays(-14).ToShortDateString();
            tripDetails.Directions = "";
            tripDetails.GuestFullName = string.Format("{0} {1}", suser.FirstName, suser.LastName);
            tripDetails.GuestsNumber = "2";
            tripDetails.Latitude = model.Property.Latitude.ToString().Replace(",", ".");
            tripDetails.Longitude = model.Property.Longitude.ToString().Replace(",", ".");
            tripDetails.MaxNoOfGuests = model.Unit.MaxNumberOfGuests.ToString();
            tripDetails.NightsNumber = model.InvoiceData.LengthOfStay.ToString();
            tripDetails.PropertyAddress = string.Format("{0}, {1}, {2} {3}", model.Property.Address1, model.Property.Address2, model.Property.City, model.Property.State);
            tripDetails.PropertyCountry = model.Property.DictionaryCountry.CountryNameCurrentLanguage;
            tripDetails.GuestEmail = suser.Email;
            tripDetails.PropertyName = model.Property.PropertyNameCurrentLanguage;
            tripDetails.CancellationPolicy = cancellationText;
            tripDetails.OnStreetParkingAvailable = "";
            var directions = new List<DirectionToAirport>();
            directions.Add(new DirectionToAirport () { DirectionLink = "link", Name_i18n="name" });
            directions[0].SetName("name");
            foreach (DirectionToAirport direction in directions)
            {
                tripDetails.Directions += string.Format("<a href='{0}'>{1}</a><br />", direction.DirectionLink, direction.Name.ToString());
            }

            Stream s = new MemoryStream(100);

            Reservation reservation = new Reservation()
            {
                NumberOfGuests = 2,
                DictionaryCulture = culture
            };

            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.IsUnitAvailable(bookingInfo, null)).Returns(true);
            bookingService.Setup(f => f.DoTemporaryReservartion(bookingInfo, user)).Returns(5);
            dictionaryService.Setup(f => f.GetCountries()).Returns(countries);
            documentService.Setup(f => f.CreateDocument(It.IsAny<TripDetails>(), CultureCode.en_US)).Returns(s);
            bookingService.Setup(f => f.CalculateInvoice(bookingInfo.PropertyId, bookingInfo.UnitId, bookingInfo.dateTripFrom, bookingInfo.dateTripUntil)).Returns(model.InvoiceData);
            stateService.Setup(f => f.GetFromSession<string>("BookingConfirmationId")).Returns(confirmationId);
            reservationService.Setup(f => f.GetReservationById(It.IsAny<int>())).Returns(reservation);

            #endregion
            
            var result = bookingController.SaveTripDetailDoc() as FileStreamResult;
            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("application/pdf", result.ContentType);
            httpResponse.Verify(f => f.AddHeader("Content-Disposition", "attachment; filename=" + String.Format("Ohtheplaces_TripDetails_{0}.pdf", confirmationId)), Times.Once(), Messages.MethodWasNotCalled);
        }

        [TestMethod]
        public void PaymentTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<DictionaryCountry> countries = new List<DictionaryCountry>();
            countries.Add(new DictionaryCountry()
            {
                CountryId = 1,
                CountryName_i18n = "Romania",
                CountryCode2Letters = "ro",
                CountryNameCurrentLanguage = "ro"
            });

            countries.Add(new DictionaryCountry()
            {
                CountryId = 2,
                CountryName_i18n = "Poland",
                CountryCode2Letters = "pl",
                CountryNameCurrentLanguage = "pl"
            });
            SelectList countryList = new SelectList(countries, "CountryCode2Letters", "CountryNameCurrentLanguage");


            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();
            
            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);

            UserProfileMinModel pm = new UserProfileMinModel()
            {
                AddressLine1 = user.Address1,
                City = user.City,
                Country = user.DictionaryCountry == null ? string.Empty : user.DictionaryCountry.CountryCode2Letters,
                Email = user.email,
                FirstName = user.Firstname,
                LastName = user.Lastname,
                PhoneNumber = user.CellPhone,
                State = user.State,
                ZIPCode = user.ZIPCode,
                CountryName = user.DictionaryCountry == null ? String.Empty : user.DictionaryCountry.CountryNameCurrentLanguage,
                UserId = user.UserID,
                Gender = user.Gender,
                BirthDay = user.BirthDate
            };

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();
            model.PaymentData = new PaymentModel();

            List<GuestPaymentMethod> creditCards = new List<GuestPaymentMethod>();
            creditCards.Add(new GuestPaymentMethod() { CreditCardLast4Digits = "4444", GuestPaymentMethodID = 0, User = user });
            SelectList creditCardsList = new SelectList(creditCards, "GuestPaymentMethodID", "CreditCardLast4Digits");
            
            int reservationId = 3;

            List<DataAccess.CreditCardType> creditCardTypeList = new List<DataAccess.CreditCardType>();
            creditCardTypeList.Add(new DataAccess.CreditCardType() { Name = "card" });

            Reservation reservation = new Reservation()
            {
                NumberOfGuests = 2,
                
            };

            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.GetUserCreditCardTypes(user.UserID)).Returns(creditCards);
            dictionaryService.Setup(f => f.GetCountries()).Returns(countries);
            stateService.Setup(f => f.GetFromSession<int>("BookingReservationId")).Returns(reservationId);
            settingsService.Setup(f => f.GetSettingValue("Payment.ApiLogin")).Returns("ApiLogin");
            settingsService.Setup(f => f.GetSettingValue("Payment.ApiSecret")).Returns ("ApiSecret");
            settingsService.Setup(f => f.GetSettingValue("Payment.RedirectURL")).Returns("RedirectURL");
            bookingService.Setup(f => f.GetCreditCardTypes()).Returns(creditCardTypeList);
            reservationService.Setup(f => f.GetReservationById(It.IsAny<int>())).Returns(reservation);

            #endregion

            bookingController.TempData["PAYMENT_ERROR_MESSAGE"] = "error";

            var result = bookingController.Payment() as ViewResult;
            var returnedModel = result.Model as BookingProcessModel;

            var userCreditCards = result.ViewData["UserCreditCards"] as SelectList;
            var countriesl = result.ViewData["Countries"] as SelectList;
            var months = result.ViewData["Months"] as SelectList;
            var years = result.ViewData["Years"] as SelectList;
            var creditCardList = result.ViewData["CreditCardList"] as List<DataAccess.CreditCardType>;
            var reservationl = result.ViewData["Reservation"] as Reservation;

            Assert.AreEqual(creditCardsList.First().Text, userCreditCards.First().Text);
            Assert.AreEqual(creditCardsList.First().Value, userCreditCards.First().Value);

            Assert.AreEqual(creditCardTypeList.First().Name, creditCardList.First().Name);

            Assert.AreEqual(reservation.NumberOfGuests, reservationl.NumberOfGuests);

            Assert.AreEqual(countryList.Count(), countriesl.Count());
            Assert.AreEqual(countryList.ElementAt(0).Text, countriesl.ElementAt(0).Text);
            Assert.AreEqual(countryList.ElementAt(0).Value, countriesl.ElementAt(0).Value);
            Assert.AreEqual(countryList.ElementAt(1).Text, countriesl.ElementAt(1).Text);
            Assert.AreEqual(countryList.ElementAt(1).Value, countriesl.ElementAt(1).Value);

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.IsNotNull(returnedModel, Messages.ObjectIsNull);

            Assert.AreEqual("error", result.ViewData["PAYMENT_ERROR_MESSAGE"]);

            Assert.AreEqual(model.BookingInfoData.dateTripFrom, returnedModel.BookingInfoData.dateTripFrom);
            Assert.AreEqual(model.BookingInfoData.dateTripUntil, returnedModel.BookingInfoData.dateTripUntil);
            Assert.AreEqual(model.BookingInfoData.PropertyId, returnedModel.BookingInfoData.PropertyId);
            Assert.AreEqual(model.BookingInfoData.UnitId, returnedModel.BookingInfoData.UnitId);
            Assert.AreEqual(model.PropertyImages.First(), returnedModel.PropertyImages.First());
            ExtendedAssert.AreEqual(model.Property, returnedModel.Property);
            ExtendedAssert.AreEqual(model.Unit, returnedModel.Unit);
            ExtendedAssert.AreEqual(model.DestinationDetails, returnedModel.DestinationDetails);
            Assert.AreEqual(model.JoinModel.Email, returnedModel.JoinModel.Email);
            Assert.AreEqual(model.JoinModel.FirstName, returnedModel.JoinModel.FirstName);
            Assert.AreEqual(model.JoinModel.LastName, returnedModel.JoinModel.LastName);
            Assert.AreEqual(model.LoginModel.Email, returnedModel.LoginModel.Email);
            Assert.AreEqual((int)model.LoginModel.LoginRole, (int)returnedModel.LoginModel.LoginRole);
            Assert.AreEqual(model.LoginModel.Password, returnedModel.LoginModel.Password);
            Assert.AreEqual(model.PropertyUnitName, returnedModel.PropertyUnitName);

            Assert.AreEqual(pm.AddressLine1, returnedModel.BillingAddress.AddressLine1);            
            Assert.AreEqual(pm.City, returnedModel.BillingAddress.City);
            Assert.AreEqual(pm.Country, returnedModel.BillingAddress.Country);
            Assert.AreEqual(pm.CountryName, returnedModel.BillingAddress.CountryName);
            Assert.AreEqual(pm.FirstName, returnedModel.BillingAddress.FirstName);
            Assert.AreEqual(pm.LastName, returnedModel.BillingAddress.LastName);
            Assert.AreEqual(pm.State, returnedModel.BillingAddress.State);
            Assert.AreEqual(pm.UserId, returnedModel.BillingAddress.UserId);
            Assert.AreEqual(pm.ZIPCode, returnedModel.BillingAddress.ZIPCode);

            Assert.AreEqual(CreditCardSelection.Existing, returnedModel.PaymentData.CreditCardSelection);
            Assert.AreEqual("ApiLogin", returnedModel.PaymentGatewaySettings.ApiLogin);
            Assert.AreEqual("ApiSecret", returnedModel.PaymentGatewaySettings.ApiSecret);
            Assert.AreEqual("RedirectURL", returnedModel.PaymentGatewaySettings.RedirectURL);
        }

        [TestMethod]
        public void PaymentWithNotVerifiedUserTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);
            bookingController.TempData["PAYMENT_ERROR_MESSAGE"] = null;

            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = false,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();

            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);
            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);

            #endregion

            var result = bookingController.Payment() as RedirectToRouteResult;
            Assert.IsNotNull(result, Messages.ObjectIsNull);
        }

        [TestMethod]
        public void PaymentWithModelTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "us",
                CountryCode3Letters = "usa"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = false,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            SessionUser suser = new SessionUser(user);

            int reservationId = 3;

            UserBillingAddress userBillingAdrr = new UserBillingAddress()
            {
                AddressLine1 = user.Address1,
                BirthDay = user.BirthDate,
                City = user.City,
                FirstName = user.Firstname,
                LastName = user.Lastname,
                ZIPCode = "12345",
                State = "State",
                Country = "us"
            };

            PaymentAgeTermsVerification paymentAgeTerms = new PaymentAgeTermsVerification()
            {
                UserInitials = "PP",
            };

            PaymentModel paymentModel = new PaymentModel()
            {
                GuestPaymentMethodId = 1,
            };

            TransactionResponse transactionResponse = new TransactionResponse()
            {
                CurrencyCode = "us",
                Succeded = true,
                Message = new BusinessLogic.Objects.PaymentGateway.Message() { Text = "text", MessageKey = "mkey" },
            };

            PaymentResult paymentResult = new PaymentResult()
            {
                BookingConfirmationID = "2543",
                PaymentGatewayResult = true,
                GatewayResponse = transactionResponse
            };

            #endregion

            #region Mock services
            stateService.Setup(f => f.GetFromSession<int>("BookingReservationId")).Returns(reservationId);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.PerformPayment(reservationId, It.IsAny<int>(), It.IsAny<ReservationBillingAddress>(), paymentModel.GuestPaymentMethodId.Value)).Returns(paymentResult);
            dictionaryService.Setup(f => f.GetCountryByCountryCode(userBillingAdrr.Country)).Returns(country);
            #endregion

            #region Payment succesfull
            var result = bookingController.Payment(userBillingAdrr, paymentAgeTerms, paymentModel) as RedirectToRouteResult;
            var redirActionName = result.RouteValues.FirstOrDefault().Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Confirmation", redirActionName);
            Assert.AreEqual(paymentResult.BookingConfirmationID, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).BookingConfirmationID);
            Assert.AreEqual(paymentResult.PaymentGatewayResult, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).PaymentGatewayResult);
            Assert.AreEqual(paymentResult.GatewayResponse.Succeded, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).GatewayResponse.Succeded);
            Assert.AreEqual(paymentResult.GatewayResponse.CurrencyCode, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).GatewayResponse.CurrencyCode);
            Assert.AreEqual(paymentResult.GatewayResponse.Message.MessageKey, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).GatewayResponse.Message.MessageKey);
            Assert.AreEqual(paymentResult.GatewayResponse.Message.Text, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).GatewayResponse.Message.Text);
            #endregion

            #region Skip EVS
            paymentResult.PaymentGatewayResult = false;
            result = bookingController.Payment(userBillingAdrr, paymentAgeTerms, paymentModel) as RedirectToRouteResult;
            redirActionName = result.RouteValues.FirstOrDefault().Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Payment", redirActionName);
            Assert.AreEqual(true, bookingController.TempData["BookingSkipEVS"]);
            Assert.AreEqual(paymentResult.GatewayResponse.Message.MessageKey, (bookingController.TempData["PAYMENT_ERROR_MESSAGE"] as BusinessLogic.Objects.PaymentGateway.Message).MessageKey);
            Assert.AreEqual(paymentResult.GatewayResponse.Message.Text, (bookingController.TempData["PAYMENT_ERROR_MESSAGE"] as BusinessLogic.Objects.PaymentGateway.Message).Text);
            #endregion
        }

        [TestMethod]
        public void PaymentWithModelFailedTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "us",
                CountryCode3Letters = "usa"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = false,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            SessionUser suser = new SessionUser(user);

            int reservationId = 3;

            UserBillingAddress userBillingAdrr = new UserBillingAddress()
            {
                AddressLine1 = user.Address1,
                BirthDay = user.BirthDate,
                City = user.City,
                FirstName = user.Firstname,
                LastName = user.Lastname,
                ZIPCode = "12345",
                State = "State",
                Country = "us"
            };

            PaymentAgeTermsVerification paymentAgeTerms = new PaymentAgeTermsVerification()
            {
                UserInitials = "PP",
            };

            PaymentModel paymentModel = new PaymentModel()
            {
                GuestPaymentMethodId = 1,
            };

            TransactionResponse transactionResponse = new TransactionResponse()
            {
                CurrencyCode = "us",
                Succeded = true,
                Message = new BusinessLogic.Objects.PaymentGateway.Message() { Text = "text", MessageKey = "mkey" },
            };

            PaymentResult paymentResult = new PaymentResult()
            {
                BookingConfirmationID = "2543",
                PaymentGatewayResult = true,
                GatewayResponse = transactionResponse
            };

            #endregion

            #region Mock services
            stateService.Setup(f => f.GetFromSession<int>("BookingReservationId")).Returns(reservationId);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.PerformPayment(reservationId, It.IsAny<int>(), It.IsAny<ReservationBillingAddress>(), paymentModel.GuestPaymentMethodId.Value)).Throws(new Common.Exceptions.PaymentGatewayException("message", null));
            dictionaryService.Setup(f => f.GetCountryByCountryCode(userBillingAdrr.Country)).Returns(country);
            #endregion

            var result = bookingController.Payment(userBillingAdrr, paymentAgeTerms, paymentModel) as RedirectToRouteResult;
            var redirActionName = result.RouteValues.FirstOrDefault().Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("PaymentFailure", redirActionName);
        }

        [TestMethod]
        public void PaymentGatewayRedirectTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "us",
                CountryCode3Letters = "usa"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = false,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            SessionUser suser = new SessionUser(user);

            int reservationId = 3;

            UserBillingAddress userBillingAdrr = new UserBillingAddress()
            {
                AddressLine1 = user.Address1,
                BirthDay = user.BirthDate,
                City = user.City,
                FirstName = user.Firstname,
                LastName = user.Lastname,
                ZIPCode = "12345",
                State = "State",
                Country = "us"
            };

            PaymentAgeTermsVerification paymentAgeTerms = new PaymentAgeTermsVerification()
            {
                UserInitials = "PP",
            };

            PaymentModel paymentModel = new PaymentModel()
            {
                GuestPaymentMethodId = 1,
                ReferenceToken = "token",
                CreditCardSelection = CreditCardSelection.Existing

            };

            TransactionResponse transactionResponse = new TransactionResponse()
            {
                CurrencyCode = "us",
                Succeded = true,
                Message = new BusinessLogic.Objects.PaymentGateway.Message() { Text = "text", MessageKey = "mkey" },
            };

            PaymentResult paymentResult = new PaymentResult()
            {
                BookingConfirmationID = "2543",
                PaymentGatewayResult = true,
                GatewayResponse = transactionResponse
            };

            PaymentMethodResponse response = new PaymentMethodResponse()
            {
                CardType = "type",
                CreditCardNumber = "00000000000000000000000000000000004444",
                Errors = new string[2],
                Last4Digits = "4444",
                Token = "token",
                AdditionalData = new AdditionalData()
                {
                    Address = userBillingAdrr.AddressLine1,
                    City = userBillingAdrr.City,
                    CountryId = "12",
                    FirstName = userBillingAdrr.FirstName,
                    Initials = paymentAgeTerms.UserInitials,
                    LastName = userBillingAdrr.LastName,
                    State = userBillingAdrr.State,
                    ZipCode = userBillingAdrr.ZIPCode,
                    CreditHolderId = "123",
                    
                }
            };
            
            GuestPaymentMethod payment = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = paymentModel.CreditCardNumber,
                ReferenceToken = paymentModel.ReferenceToken
            };

            bool paymentMethodRetained = false;
            #endregion

            #region Mock services
            stateService.Setup(f => f.GetFromSession<int>("BookingReservationId")).Returns(reservationId);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            dictionaryService.Setup(f => f.GetCountryByCountryCode(userBillingAdrr.Country)).Returns(country);
            paymentGatewayService.Setup(f => f.GetPaymentAdditionalData("token", reservationId)).Returns(response);
            bookingService.Setup(f => f.SetGuestPaymentMethod(payment, It.IsAny<int>(), It.IsAny<int>(), reservationId, out paymentMethodRetained)).Returns(25434);
            bookingService.Setup(f => f.PerformPayment(reservationId, It.IsAny<int>(), It.IsAny<ReservationBillingAddress>(), It.IsAny<int>())).Returns(paymentResult);
            #endregion

            #region Payment succesfull
            var result = bookingController.PaymentGatewayRedirect("token") as RedirectToRouteResult;
            var redirActionName = result.RouteValues.FirstOrDefault().Value;

            bookingService.Verify(f => f.RemoveGuestPaymentMethod(It.IsAny<int>()), Times.Once(), Messages.MethodWasNotCalled);
            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Confirmation", redirActionName);
            Assert.AreEqual(paymentResult.BookingConfirmationID, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).BookingConfirmationID);
            Assert.AreEqual(paymentResult.PaymentGatewayResult, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).PaymentGatewayResult);
            Assert.AreEqual(paymentResult.GatewayResponse.Succeded, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).GatewayResponse.Succeded);
            Assert.AreEqual(paymentResult.GatewayResponse.CurrencyCode, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).GatewayResponse.CurrencyCode);
            Assert.AreEqual(paymentResult.GatewayResponse.Message.MessageKey, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).GatewayResponse.Message.MessageKey);
            Assert.AreEqual(paymentResult.GatewayResponse.Message.Text, (bookingController.TempData["BookingTempPaymentResult"] as PaymentResult).GatewayResponse.Message.Text);
            Assert.AreEqual("There was problem with saving your credit card data. You will need to input credit card data next time on booking process.", bookingController.TempData["CONFIRMATION_ERROR_MESSAGE"]);
            #endregion

            #region Skip EVS
            paymentResult.PaymentGatewayResult = false;
            paymentMethodRetained = true;
            result = bookingController.PaymentGatewayRedirect("token") as RedirectToRouteResult;
            redirActionName = result.RouteValues.FirstOrDefault().Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("Payment", redirActionName);
            Assert.AreEqual(true, bookingController.TempData["BookingSkipEVS"]);
            Assert.AreEqual(paymentResult.GatewayResponse.Message.MessageKey, (bookingController.TempData["PAYMENT_ERROR_MESSAGE"] as BusinessLogic.Objects.PaymentGateway.Message).MessageKey);
            Assert.AreEqual(paymentResult.GatewayResponse.Message.Text, (bookingController.TempData["PAYMENT_ERROR_MESSAGE"] as BusinessLogic.Objects.PaymentGateway.Message).Text);
            #endregion
        }

        [TestMethod]
        public void PaymentGatewayRedirectFailedTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            #region Setup objects
            bool paymentMethodRetained = true;
            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "us",
                CountryCode3Letters = "usa"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();

            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = false,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            SessionUser suser = new SessionUser(user);

            int reservationId = 3;
            UserBillingAddress userBillingAdrr = new UserBillingAddress()
            {
                AddressLine1 = user.Address1,
                BirthDay = user.BirthDate,
                City = user.City,
                FirstName = user.Firstname,
                LastName = user.Lastname,
                ZIPCode = "12345",
                State = "State",
                Country = "us"
            };

            PaymentAgeTermsVerification paymentAgeTerms = new PaymentAgeTermsVerification()
            {
                UserInitials = "PP",
            };

            PaymentModel paymentModel = new PaymentModel()
            {
                GuestPaymentMethodId = 1,
                ReferenceToken = "token",
                CreditCardSelection = CreditCardSelection.Existing

            };

            TransactionResponse transactionResponse = new TransactionResponse()
            {
                CurrencyCode = "us",
                Succeded = true,
                Message = new BusinessLogic.Objects.PaymentGateway.Message() { Text = "text", MessageKey = "mkey" },
            };

            PaymentResult paymentResult = new PaymentResult()
            {
                BookingConfirmationID = "2543",
                PaymentGatewayResult = true,
                GatewayResponse = transactionResponse
            };

            PaymentMethodResponse response = new PaymentMethodResponse()
            {
                CardType = "type",
                CreditCardNumber = "00000000000000000000000000000000004444",
                Errors = new string[2],
                Last4Digits = "4444",
                Token = "token",
                AdditionalData = new AdditionalData()
                {
                    Address = userBillingAdrr.AddressLine1,
                    City = userBillingAdrr.City,
                    CountryId = "12",
                    FirstName = userBillingAdrr.FirstName,
                    Initials = paymentAgeTerms.UserInitials,
                    LastName = userBillingAdrr.LastName,
                    State = userBillingAdrr.State,
                    ZipCode = userBillingAdrr.ZIPCode,
                    CreditHolderId = "123",

                }
            };

            GuestPaymentMethod payment = new GuestPaymentMethod()
            {
                CreditCardLast4Digits = paymentModel.CreditCardNumber,
                ReferenceToken = paymentModel.ReferenceToken
            };
            #endregion

            #region Mock services
            stateService.Setup(f => f.GetFromSession<int>("BookingReservationId")).Returns(reservationId);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.PerformPayment(reservationId, It.IsAny<int>(), It.IsAny<ReservationBillingAddress>(), It.IsAny<int>())).Throws(new Common.Exceptions.PaymentGatewayException("message", null));
            paymentGatewayService.Setup(f => f.GetPaymentAdditionalData("token", reservationId)).Returns(response);
            bookingService.Setup(f => f.SetGuestPaymentMethod(payment, It.IsAny<int>(), It.IsAny<int>(), reservationId, out paymentMethodRetained)).Returns(25434);            
            #endregion

            var result = bookingController.PaymentGatewayRedirect("token") as RedirectToRouteResult;
            var redirActionName = result.RouteValues.FirstOrDefault().Value;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("PaymentFailure", redirActionName);
        }

        [TestMethod]
        public void SaveInvoiceDetailDocTest()
        {
            #region Setup Services

            var propertiesService = new Mock<IPropertiesService>();
            var stateService = new Mock<IStateService>();
            var unitService = new Mock<IUnitsService>();
            var httpContextService = new Mock<IContextService>();
            var bookingService = new Mock<IBookingService>();
            var userService = new Mock<IUserService>();
            var dictionaryService = new Mock<IDictionaryCountryService>();
            var messageService = new Mock<IMessageService>();
            var settingsService = new Mock<ISettingsService>();
            var documentService = new Mock<IDocumentService>();
            var paymentGatewayService = new Mock<IPaymentGatewayService>();
            var reservationService = new Mock<IReservationsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var httpRequest = new Mock<HttpRequestBase>();
            var httpResponse = new Mock<HttpResponseBase>();
            var httpContext = new Mock<HttpContextBase>();

            httpContext.Setup(p => p.Request).Returns(httpRequest.Object);
            httpContext.Setup(p => p.Response).Returns(httpResponse.Object);
            #endregion

            BookingController bookingController = new BookingController(propertiesService.Object, stateService.Object,
                unitService.Object, httpContextService.Object, bookingService.Object, userService.Object, dictionaryService.Object,
                messageService.Object, settingsService.Object, documentService.Object, paymentGatewayService.Object, reservationService.Object, authorizationService.Object);

            bookingController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), bookingController);


            #region Setup objects
            BookingInfoModel bookingInfo = new BookingInfoModel()
            {
                dateTripFrom = new DateTime(2013, 10, 10),
                dateTripUntil = new DateTime(2013, 12, 12),
                PropertyId = 123,
                UnitId = 12
            };

            List<string> images = new List<string>();
            images.Add("image1");

            DictionaryCountry country = new DictionaryCountry()
            {
                CountryId = 12,
                CountryCode2Letters = "pl",
                CountryCode3Letters = "pol"
            };
            country.SetCountryNameValue("PropertyDetailsWithRatingAndOneUnitTest_Poland");
            country.PersistI18nValues();
            
            DictionaryCulture culture = new DictionaryCulture()
            {
                CultureId = 15,
                CultureCode = "en-US"
            };
            culture.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_polish");
            culture.PersistI18nValues();

            User user = new User()
            {
                Lastname = "PropertyDetailsWithRatingAndOneUnitTest_Lastname",
                Firstname = "PropertyDetailsWithRatingAndOneUnitTest_Firstname",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZIPCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                CellPhone = "PropertyDetailsWithRatingAndOneUnitTest_CellPhone",
                LandLine = "PropertyDetailsWithRatingAndOneUnitTest_LandLine",
                email = "PropertyDetailsWithRatingAndOneUnitTest_email",
                DriverLicenseNbr = "PropertyDetailsWithRatingAndOneUnitTest_DriverLicenseNbr",
                PassportNbr = "PropertyDetailsWithRatingAndOneUnitTest_PassportNbr",
                SocialsecurityNbr = "PropertyDetailsWithRatingAndOneUnitTest_SocialsecurityNbr",
                DirectDepositInfo = "PropertyDetailsWithRatingAndOneUnitTest_DirectDepositInfo",
                SecurityQuestion = "PropertyDetailsWithRatingAndOneUnitTest_SecurityQuestion",
                SecurityAnswer = "PropertyDetailsWithRatingAndOneUnitTest_SecurityAnswer",
                AcceptedTCs = DateTime.Now,
                AcceptedTCsInitials = "PropertyDetailsWithRatingAndOneUnitTest_AcceptedTCsInitials",
                Password = "PropertyDetailsWithRatingAndOneUnitTest_Password",
                IsGeneratedPassword = true,
                VerificationPositive = true,
                SendMePromotions = true,
                SendInfoFavoritePlaces = true,
                SendNews = true,
                Gender = "m",
                LoggedInRoles = new List<Role>() { new Role() { Name = "role", RoleLevel = (int)RoleLevel.Guest, RoleId = 1 } },
                DictionaryCulture = culture
            };

            Destination destination = new Destination()
            {
                DestinationID = 1234
            };
            destination.SetDestinationNameValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationName");
            destination.SetDestinationDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_DestinationDescription");
            destination.PersistI18nValues();

            PropertyType propertyType = new PropertyType()
            {
                PropertyTypeId = 345
            };
            propertyType.SetNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyTypeName");
            propertyType.PersistI18nValues();

            Property property = new Property()
            {
                User = user,
                Destination = destination,
                PropertyCode = "PropertyDetailsWithRatingAndOneUnitTest_PropertyCode",
                Address1 = "PropertyDetailsWithRatingAndOneUnitTest_Address1",
                Address2 = "PropertyDetailsWithRatingAndOneUnitTest_Address2",
                State = "PropertyDetailsWithRatingAndOneUnitTest_State",
                ZIPCode = "PropertyDetailsWithRatingAndOneUnitTest_ZipCode",
                City = "PropertyDetailsWithRatingAndOneUnitTest_City",
                DictionaryCountry = country,
                Latitude = 100.12M,
                Longitude = 120.21M,
                Altitude = 210.17M,
                PropertyLive = true,
                StandardCommission = 12.0M,
                PropertyType = propertyType,
                CheckIn = new TimeSpan(21, 12, 11),
                CheckOut = new TimeSpan(12, 21, 22)
            };
            property.SetPropertyNameValue("PropertyDetailsWithRatingAndOneUnitTest_PropertyName");
            property.SetShortDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_ShortDescription");
            property.SetLongDescriptionValue("PropertyDetailsWithRatingAndOneUnitTest_LongDescription");
            property.PersistI18nValues();

            UnitType unitType = new UnitType()
            {
                UnitTypeID = 5,
                Property = property
            };
            unitType.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeTitle");
            unitType.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_UnitTypeDesc");
            unitType.PersistI18nValues();

            Unit unit = new Unit()
            {
                UnitID = 123,
                Property = property,
                UnitType = unitType,
                UnitCode = "PropertyDetailsWithRatingAndOneUnitTest_Code",
                CleaningStatus = (int)UnitCleaningStatus.Cleaned,
                MaxNumberOfBathrooms = 1,
                MaxNumberOfBedRooms = 2,
                MaxNumberOfGuests = 3
            };
            unit.SetTitleValue("PropertyDetailsWithRatingAndOneUnitTest_Title");
            unit.SetDescValue("PropertyDetailsWithRatingAndOneUnitTest_Desc");
            unit.PersistI18nValues();

            SessionUser suser = new SessionUser(user);

            List<DictionaryCountry> countries = new List<DictionaryCountry>();
            countries.Add(new DictionaryCountry()
            {
                CountryId = 1,
                CountryName_i18n = "Romania",
                CountryCode2Letters = "ro",
                CountryNameCurrentLanguage = "ro"
            });

            countries.Add(new DictionaryCountry()
            {
                CountryId = 2,
                CountryName_i18n = "Poland",
                CountryCode2Letters = "pl",
                CountryNameCurrentLanguage = "pl"
            });
            SelectList countryList = new SelectList(countries, "CountryCode2Letters", "CountryNameCurrentLanguage");

            UserProfileMinModel pm = new UserProfileMinModel()
            {
                AddressLine1 = user.Address1,
                City = user.City,
                Country = user.DictionaryCountry == null ? string.Empty : user.DictionaryCountry.CountryCode2Letters,
                Email = user.email,
                FirstName = user.Firstname,
                LastName = user.Lastname,
                PhoneNumber = user.CellPhone,
                State = user.State,
                ZIPCode = user.ZIPCode,
                CountryName = user.DictionaryCountry == null ? string.Empty : user.DictionaryCountry.CountryNameCurrentLanguage,
                UserId = user.UserID,
                Gender = user.Gender,
                BirthDay = user.BirthDate
            };

            BookingProcessModel model = new BookingProcessModel();
            model.BookingInfoData = bookingInfo;
            model.PropertyImages = images;
            model.Property = property;
            model.Unit = unit;
            model.DestinationDetails = model.Property.Destination;
            model.LoginModel = new LoginModel();
            model.JoinModel = new JoinModel();
            var propertyUnitName = new StringBuilder(model.Property.PropertyNameCurrentLanguage);
            if (model.Property.Units.Count() > 1)
            {
                propertyUnitName = propertyUnitName.Append(" / ").Append(model.Unit.UnitTitleCurrentLanguage);
            }
            model.PropertyUnitName = propertyUnitName.ToString();

            Reservation reservation = new Reservation()
            {
                Invoice = new byte[] { 1, 2, 3, 4, 5, 6 },
            };

            #endregion

            #region Mock services

            stateService.Setup(f => f.GetFromSession<BookingInfoModel>("BookingDataInfo")).Returns(bookingInfo);
            propertiesService.Setup(f => f.GetPropertyImagesByPropertyId(bookingInfo.PropertyId, PropertyStaticContentType.FullScreen)).Returns(images);
            propertiesService.Setup(f => f.GetPropertyById(It.IsAny<int>(), It.IsAny<bool>())).Returns(property);
            unitService.Setup(f => f.GetUnitById(It.IsAny<int>())).Returns(unit);
            userService.Setup(f => f.GetUserByUserId(It.IsAny<int>())).Returns(user);
            stateService.Setup(f => f.CurrentUser).Returns(suser);
            bookingService.Setup(f => f.IsUnitAvailable(bookingInfo, null)).Returns(true);
            bookingService.Setup(f => f.DoTemporaryReservartion(bookingInfo, user)).Returns(5);
            dictionaryService.Setup(f => f.GetCountries()).Returns(countries);
            reservationService.Setup(f => f.GetReservationById(It.IsAny<int>())).Returns(reservation);
            stateService.Setup(f => f.GetFromSession<string>("BookingConfirmationId")).Returns("1234");

            #endregion


            FileStreamResult result = bookingController.SaveInvoiceDetailDoc();

            byte[] invoice_data = new byte[6];
            result.FileStream.Read(invoice_data, 0, 6);
            CollectionAssert.AreEqual(reservation.Invoice, invoice_data);
        }
    }
}
