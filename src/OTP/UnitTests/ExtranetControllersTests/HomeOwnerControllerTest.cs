﻿using BusinessLogic.Enums;
using BusinessLogic.Objects.Templates.Email;
using BusinessLogic.ServiceContracts;
using DataAccess;
using DataAccess.Enums;
using ExtranetApp;
using ExtranetApp.Controllers;
using ExtranetApp.Models;
using ExtranetApp.Resources.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UnitTests.ExtranetControllersTests
{
    [TestClass]
    public class HomeOwnerControllerTest
    {
        [TestMethod]
        public void IndexTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
            var dictionaryCultureService = new Mock<IDictionaryCultureService>();
            var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);
            RedirectToRouteResult result = (RedirectToRouteResult)homeOwnerController.Index();
            Assert.AreEqual("AboutJoin", result.RouteValues["Action"]);
        }

        [TestMethod]
        public void AboutJoinTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>(); 
            var messageService = new Mock<IMessageService>(); 
            var dictionaryCountryService = new Mock <IDictionaryCountryService>();
            var dictionaryCultureService = new Mock<IDictionaryCultureService>();
            var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);

            propertiesService.Setup(f => f.GetProperties()).Returns(new List<Property>());
            configurationService.Setup(f => f.GetKeyValue(It.IsAny<CloudConfigurationKeys>())).Returns(String.Empty);
            settingsService.Setup(f => f.GetSettingValue(It.IsAny<string>())).Returns(String.Empty);

            ViewResult result = (ViewResult) homeOwnerController.AboutJoin();

            Assert.IsNotNull(result, Messages.ObjectIsNull);
        }

        [TestMethod]
        public void OwnerAgreementTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
            var dictionaryCultureService = new Mock<IDictionaryCultureService>();
            var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);

            blobService.Setup(f => f.DownloadFileByUrl(It.IsAny<string>(), It.IsAny<string>())).Returns(new MemoryStream());

            FileStreamResult result = homeOwnerController.OwnerAgreement() as FileStreamResult;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
        }

        [TestMethod]
        public void ShortTermRentalAgreementTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
            var dictionaryCultureService = new Mock<IDictionaryCultureService>();
            var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);

            blobService.Setup(f => f.DownloadFileByUrl(It.IsAny<string>(), It.IsAny<string>())).Returns(new MemoryStream());

            FileStreamResult result = homeOwnerController.ShortTermRentalAgreement() as FileStreamResult;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
        }

        [TestMethod]
        public void ConfirmTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
            var dictionaryCultureService = new Mock<IDictionaryCultureService>();
            var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);  

            UserProfileModel model = new UserProfileModel();
            model.AddressLine1 = "AddressLine1";
            model.AddressLine2 = "AddressLine2";
            model.City = "City";
            model.CountryName = "CountryName";
            model.Email = "test@test.com";
            model.FirstName = "User";
            model.LastName = "LastName";
            model.State = "State";
            model.ZIPCode = "ZIPCode";
            model.UserId = 100000;
            model.PhoneNumber = "874598872345";

            var result = homeOwnerController.Confirm(model) as PartialViewResult;
            var returnedModel = result.Model as UserProfileModel;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("JoinConfirmation", result.ViewName);
            Assert.AreEqual(model.AddressLine1, returnedModel.AddressLine1);
            Assert.AreEqual(model.AddressLine2, returnedModel.AddressLine2);
            Assert.AreEqual(model.City, returnedModel.City);
            Assert.AreEqual(model.CountryName, returnedModel.CountryName);
            Assert.AreEqual(model.Email, returnedModel.Email);
            Assert.AreEqual(model.FirstName, returnedModel.FirstName);
            Assert.AreEqual(model.LastName, returnedModel.LastName);
            Assert.AreEqual(model.State, returnedModel.State);
            Assert.AreEqual(model.ZIPCode, returnedModel.ZIPCode);
            Assert.AreEqual(model.UserId, returnedModel.UserId);
            Assert.AreEqual(model.PhoneNumber, returnedModel.PhoneNumber);
        }

        [TestMethod]
        public void ListYourHomeTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
			var dictionaryCultureService = new Mock<IDictionaryCultureService>();
			var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);  
            List<DictionaryCountry> countries = new List<DictionaryCountry>();
            countries.Add(new DictionaryCountry()
            {
                CountryId = 1,
                CountryName_i18n = "Romania",
                CountryCode2Letters = "ro",
                CountryNameCurrentLanguage = "ro"
            });

            countries.Add(new DictionaryCountry()
            {
                CountryId = 2,
                CountryName_i18n = "Poland",
                CountryCode2Letters = "pl",
                CountryNameCurrentLanguage = "pl"
            });
            SelectList countryList = new SelectList(countries, "CountryCode2Letters", "CountryNameCurrentLanguage");
            dictionaryCountryService.Setup(f => f.GetCountries()).Returns(countries);

            var result = homeOwnerController.ListYourHome() as ViewResult;
            var returnedCountries = result.ViewData["Countries"] as SelectList;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual(countryList.Count(), returnedCountries.Count());
            Assert.AreEqual(countryList.ElementAt(0).Text, returnedCountries.ElementAt(0).Text);
            Assert.AreEqual(countryList.ElementAt(0).Value, returnedCountries.ElementAt(0).Value);
            Assert.AreEqual(countryList.ElementAt(1).Text, returnedCountries.ElementAt(1).Text);
            Assert.AreEqual(countryList.ElementAt(1).Value, returnedCountries.ElementAt(1).Value);
        }

        [TestMethod]
        public void ListYourHomeWithValidModelStateTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
			var dictionaryCultureService = new Mock<IDictionaryCultureService>();
			var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);  
            UserProfileModel model = new UserProfileModel();
            model.AddressLine1 = "AddressLine1";
            model.AddressLine2 = "AddressLine2";
            model.City = "City";
            model.CountryName = "CountryName";
            model.Email = "test@test.com";
            model.FirstName = "User";
            model.LastName = "LastName";
            model.State = "State";
            model.ZIPCode = "ZIPCode";
            model.UserId = 100000;
            model.PhoneNumber = "874598872345";

            var result = homeOwnerController.ListYourHome(model) as ViewResult;

            stateService.Verify(f => f.AddToSession(SessionKeys.HomeownerBasicData.ToString(), model), Times.Once(), Messages.MethodWasNotCalled);
            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("ScheduleMeeting", result.ViewName);
        }

        [TestMethod]
        public void ListYourHomeWithInvalidModelStateTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
			var dictionaryCultureService = new Mock<IDictionaryCultureService>();
			var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);  
            List<DictionaryCountry> countries = new List<DictionaryCountry>();
            countries.Add(new DictionaryCountry()
            {
                CountryId = 1,
                CountryName_i18n = "Romania",
                CountryCode2Letters = "ro",
                CountryNameCurrentLanguage = "ro"
            });

            countries.Add(new DictionaryCountry()
            {
                CountryId = 2,
                CountryName_i18n = "Poland",
                CountryCode2Letters = "pl",
                CountryNameCurrentLanguage = "pl"
            });
            SelectList countryList = new SelectList(countries, "CountryCode2Letters", "CountryNameCurrentLanguage");
            dictionaryCountryService.Setup(f => f.GetCountries()).Returns(countries);
            homeOwnerController.ModelState.AddModelError(String.Empty, "ModelError");

            var result = homeOwnerController.ListYourHome(null) as ViewResult;
            var returnedCountries = result.ViewData["Countries"] as SelectList;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual(countryList.Count(), returnedCountries.Count());
            Assert.AreEqual(countryList.ElementAt(0).Text, returnedCountries.ElementAt(0).Text);
            Assert.AreEqual(countryList.ElementAt(0).Value, returnedCountries.ElementAt(0).Value);
            Assert.AreEqual(countryList.ElementAt(1).Text, returnedCountries.ElementAt(1).Text);
            Assert.AreEqual(countryList.ElementAt(1).Value, returnedCountries.ElementAt(1).Value);
        }

        [TestMethod]
        public void ScheduleMeetingTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
			var dictionaryCultureService = new Mock<IDictionaryCultureService>();
			var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);  
            ViewResult result = (ViewResult) homeOwnerController.ScheduleMeeting();

            Assert.IsNotNull(result, Messages.ObjectIsNull);
        }

        [TestMethod]
        public void ScheduleMeetingWithValidModelStateTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
			var dictionaryCultureService = new Mock<IDictionaryCultureService>();
			var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            Mock<HttpContextBase> context = new Mock<HttpContextBase>();
            context.Setup(c => c.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Homeowner/JoinConfirmation");

            var routes = new RouteCollection();

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            RouteData route = RouteTable.Routes.GetRouteData(context.Object);
            

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);
            homeOwnerController.Url = new UrlHelper(new RequestContext(context.Object, new RouteData()), routes);

            SchedulingMeeting model = new SchedulingMeeting();
            model.DateFrom = DateTime.Now.AddDays(10).ToString();
            model.DateTo = DateTime.Now.AddDays(20).ToString();
            model.Dates = model.DateFrom + "|" + model.DateTo;

            var result = homeOwnerController.ScheduleMeeting(model) as JsonResult;

            stateService.Verify(f => f.AddToSession(SessionKeys.HomeownerSchedulingMeetingData.ToString(), model), Times.Once(), Messages.MethodWasNotCalled);
            Assert.IsNotNull(result, Messages.ObjectIsNull);                       
            //TODO fix this - routing is not working correctly in unit tests
            //Assert.AreEqual("{ Valid = True, Message = "+route.Values["controller"]+"/"+route.Values["action"]+" }", result.Data.ToString());
        }

        [TestMethod]
        public void ScheduleMeetingWithInvalidModelStateTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
			var dictionaryCultureService = new Mock<IDictionaryCultureService>();
			var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);  
            SchedulingMeeting model = new SchedulingMeeting();
            model.DateFrom = DateTime.Now.AddDays(10).ToString();
            model.DateTo = DateTime.Now.AddDays(20).ToString();
            model.Dates = model.DateFrom + "|" + model.DateTo;
            homeOwnerController.ModelState.AddModelError(String.Empty, "Error");

            var result = homeOwnerController.ScheduleMeeting(model) as JsonResult;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("{ Valid = False, Message = "+ HomeOwner.Req_ChooseDates +" }", result.Data.ToString());
        }

        [TestMethod]
        public void JoinConfirmationUserAlreadyExistTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
			var dictionaryCultureService = new Mock<IDictionaryCultureService>();
			var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);  
            UserProfileModel model = new UserProfileModel();
            model.AddressLine1 = "AddressLine1";
            model.AddressLine2 = "AddressLine2";
            model.City = "City";
            model.CountryName = "CountryName";
            model.Email = "test@test.com";
            model.FirstName = "User";
            model.LastName = "LastName";
            model.State = "State";
            model.ZIPCode = "ZIPCode";
            model.UserId = 100000;
            model.PhoneNumber = "874598872345";
            model.Country = 1;
            model.TCInitials = "UL";

            SchedulingMeeting meeting = new SchedulingMeeting();
            meeting.DateFrom = DateTime.Now.AddDays(10).ToString();
            meeting.DateTo = DateTime.Now.AddDays(20).ToString();
            meeting.Dates = "Dates";

            DictionaryCountry dictCountry = new DictionaryCountry();
            dictCountry.CountryId = 1;
            dictCountry.SetCountryNameValue("Great Britain");
            dictCountry.PersistI18nValues();

            DictionaryCulture dictCulture = new DictionaryCulture();
            dictCulture.CultureId = 1;
            dictCulture.CultureCode = "pl-PL";
            dictCulture.SetNameValue("polish");
            dictCulture.PersistI18nValues();

            Role role = new Role();
            role.Name = "RoleName";
            role.RoleId = 2222;
            role.RoleLevel = (int) RoleLevel.Guest;

            UserRole userRole = new UserRole();
            userRole.ActivationToken = "ActivationToken";
            userRole.Role = role;

            UserRole userRoleCopy = new UserRole();
            userRoleCopy.ActivationToken = "ActivationToken";
            userRoleCopy.Role = role;

            User user = new DataAccess.User();
            user.email = model.Email;
            user.Firstname = model.FirstName;
            user.Lastname = model.LastName;
            user.Address1 = model.AddressLine1;
            user.Address2 = model.AddressLine2;
            user.City = model.City;
            user.State = model.State;
            user.ZIPCode = model.ZIPCode;
            user.CellPhone = model.PhoneNumber;
            user.DictionaryCountry = dictCountry;
            user.DictionaryCulture = dictCulture;
            user.UserRoles.Add(userRole);

            stateService.Setup(f => f.GetFromSession<UserProfileModel>(SessionKeys.HomeownerBasicData.ToString())).Returns(model);
            stateService.Setup(f => f.GetFromSession<SchedulingMeeting>(SessionKeys.HomeownerSchedulingMeetingData.ToString())).Returns(meeting);
            dictionaryCountryService.Setup(f => f.GetCountryById(model.Country)).Returns(dictCountry);
            dictionaryCultureService.Setup(f => f.GetCultureByCode(It.IsAny<string>())).Returns(dictCulture);
            userService.Setup(f => f.GetUserByEmail(model.Email)).Returns(user);
            userService.Setup(f => f.GetUserRoleByLevel(It.IsAny<User>(), It.IsAny<RoleLevel>())).Returns(userRoleCopy);
            authorizationService.Setup(f => f.GeneratePassword()).Returns("pass");
            
            var result = homeOwnerController.JoinConfirmation() as JsonResult;

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual("{ Valid = False, Message = " + Account.Error_UsersExists  + " }", result.Data.ToString());
        }

        [TestMethod]
        public void JoinConfirmationNotOwnerUserTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
			var dictionaryCultureService = new Mock<IDictionaryCultureService>();
			var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);  
            UserProfileModel model = new UserProfileModel();
            model.AddressLine1 = "AddressLine1";
            model.AddressLine2 = "AddressLine2";
            model.City = "City";
            model.CountryName = "CountryName";
            model.Email = "test@test.com";
            model.FirstName = "User";
            model.LastName = "LastName";
            model.State = "State";
            model.ZIPCode = "ZIPCode";
            model.UserId = 100000;
            model.PhoneNumber = "874598872345";
            model.Country = 1;            
            model.TCInitials = "UL";

            SchedulingMeeting meeting = new SchedulingMeeting();
            meeting.DateFrom = DateTime.Now.AddDays(10).ToString();
            meeting.DateTo = DateTime.Now.AddDays(20).ToString();
            meeting.Dates = meeting.DateFrom + "|" + meeting.DateTo;

            DictionaryCountry dictCountry = new DictionaryCountry();
            dictCountry.CountryId = 1;
            dictCountry.SetCountryNameValue("United States");
            dictCountry.PersistI18nValues();
            dictCountry.CountryCode2Letters = "us";

            DictionaryCulture dictCulture = new DictionaryCulture();
            dictCulture.CultureId = 1;
            dictCulture.CultureCode = "pl-PL";
            dictCulture.SetNameValue("polish");
            dictCulture.PersistI18nValues();

            Role role = new Role();
            role.Name = "RoleName";
            role.RoleId = 2222;
            role.RoleLevel = (int) RoleLevel.Owner;

            UserRole userRole = new UserRole();
            userRole.ActivationToken = "ActivationToken";
            userRole.Role = role;

            User user = new DataAccess.User();
            user.email = model.Email;
            user.Firstname = model.FirstName;
            user.Lastname = model.LastName;
            user.Address1 = model.AddressLine1;
            user.Address2 = model.AddressLine2;
            user.City = model.City;
            user.State = model.State;
            user.ZIPCode = model.ZIPCode;
            user.CellPhone = model.PhoneNumber;
            user.DictionaryCountry = dictCountry;
            user.DictionaryCulture = dictCulture;

            OwnerJoiningEmail ownerJoiningEmail = new OwnerJoiningEmail(model.Email);

            stateService.Setup(f => f.GetFromSession<UserProfileModel>(SessionKeys.HomeownerBasicData.ToString())).Returns(model);
            stateService.Setup(f => f.GetFromSession<SchedulingMeeting>(SessionKeys.HomeownerSchedulingMeetingData.ToString())).Returns(meeting);
            dictionaryCountryService.Setup(f => f.GetCountryById(model.Country)).Returns(dictCountry);
            dictionaryCultureService.Setup(f => f.GetCultureByCode(It.IsAny<string>())).Returns(dictCulture);
            userService.Setup(f => f.GetUserByEmail(model.Email)).Returns(user);
            userService.Setup(f => f.GetUserRoleByLevel(user, RoleLevel.Guest)).Returns(userRole);
            userService.Setup(f => f.MergeUserData(It.IsAny<User>(), It.IsAny<User>(), false)).Returns(user);
            roleService.Setup(f => f.AddUserToRole(user, RoleLevel.Owner)).Returns(userRole);
            authorizationService.Setup(f => f.GeneratePassword()).Returns("pass");
            settingsService.Setup(f => f.GetSettingValue(SettingKeyName.TicketSystemJoinEmailAddress)).Returns("test@test.com");

            var result = homeOwnerController.JoinConfirmation() as ViewResult;
            var returnedEmail = result.ViewData["Email"] as String;

            scheduledVisitsService.Verify(f => f.AddVisit(It.IsAny<ScheduledVisit>()), Times.Once(), Messages.MethodWasNotCalled);
            messageService.Verify(f => f.AddEmail(It.IsAny<OwnerJoiningEmail>(), "pl-PL", null, null), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession(SessionKeys.HomeownerBasicData.ToString()), Times.Once(), Messages.MethodWasNotCalled);
            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual(model.Email, returnedEmail);
        }

        [TestMethod]
        public void JoinConfirmationNoExistingUserTest()
        {
            #region Setup services
            var stateService = new Mock<IStateService>();
            var messageService = new Mock<IMessageService>();
            var dictionaryCountryService = new Mock<IDictionaryCountryService>();
			var dictionaryCultureService = new Mock<IDictionaryCultureService>();
			var roleService = new Mock<IRolesService>();
            var userService = new Mock<IUserService>();
            var scheduledVisitsService = new Mock<IScheduledVisitsService>();
            var propertiesService = new Mock<IPropertiesService>();
            var reservationsService = new Mock<IReservationsService>();
            var configurationService = new Mock<IConfigurationService>();
            var unitService = new Mock<IUnitsService>();
            var settingsService = new Mock<ISettingsService>();
            var authorizationService = new Mock<IAuthorizationService>();
            var blobService = new Mock<IBlobService>();
            #endregion

            HomeOwnerController homeOwnerController = new HomeOwnerController(stateService.Object,
                                                                              messageService.Object,
                                                                              dictionaryCountryService.Object,
                                                                              dictionaryCultureService.Object,
                                                                              roleService.Object,
                                                                              userService.Object,
                                                                              scheduledVisitsService.Object,
                                                                              reservationsService.Object,
                                                                              propertiesService.Object,
                                                                              configurationService.Object,
                                                                              unitService.Object,
                                                                              settingsService.Object,
                                                                              authorizationService.Object,
                                                                              blobService.Object);  
            UserProfileModel model = new UserProfileModel();
            model.AddressLine1 = "AddressLine1";
            model.AddressLine2 = "AddressLine2";
            model.City = "City";
            model.CountryName = "CountryName";
            model.Email = "test@test.com";
            model.FirstName = "User";
            model.LastName = "LastName";
            model.State = "State";
            model.ZIPCode = "ZIPCode";
            model.UserId = 100000;
            model.PhoneNumber = "874598872345";
            model.Country = 1;
            model.TCInitials = "UL";
            model.CountryCode2Letters = "en";

            SchedulingMeeting meeting = new SchedulingMeeting();
            meeting.DateFrom = DateTime.Now.AddDays(10).ToString();
            meeting.DateTo = DateTime.Now.AddDays(20).ToString();
            meeting.Dates = meeting.DateFrom + "|" + meeting.DateTo;

            DictionaryCountry dictCountry = new DictionaryCountry();
            dictCountry.CountryId = 1;
            dictCountry.SetCountryNameValue("Great Britain");
            dictCountry.PersistI18nValues();
            dictCountry.CountryCode2Letters = "en";

            DictionaryCulture dictCulture = new DictionaryCulture();
            dictCulture.CultureId = 1;
            dictCulture.CultureCode = "pl-PL";
            dictCulture.SetNameValue("polish");
            dictCulture.PersistI18nValues();

            Role role = new Role();
            role.Name = "RoleName";
            role.RoleId = 2222;
            role.RoleLevel = (int)RoleLevel.Owner;

            UserRole userRole = new UserRole();
            userRole.ActivationToken = "ActivationToken";
            userRole.Role = role;

            User user = new DataAccess.User();
            user.email = model.Email;
            user.Firstname = model.FirstName;
            user.Lastname = model.LastName;
            user.Address1 = model.AddressLine1;
            user.Address2 = model.AddressLine2;
            user.City = model.City;
            user.State = model.State;
            user.ZIPCode = model.ZIPCode;
            user.CellPhone = model.PhoneNumber;
            user.DictionaryCountry = dictCountry;
            user.DictionaryCulture = dictCulture;

            OwnerJoiningEmail ownerJoiningEmail = new OwnerJoiningEmail(model.Email);

            stateService.Setup(f => f.GetFromSession<UserProfileModel>(SessionKeys.HomeownerBasicData.ToString())).Returns(model);
            stateService.Setup(f => f.GetFromSession<SchedulingMeeting>(SessionKeys.HomeownerSchedulingMeetingData.ToString())).Returns(meeting);
            dictionaryCountryService.Setup(f => f.GetCountryById(model.Country)).Returns(dictCountry);
            dictionaryCountryService.Setup(f => f.GetCountryByCountryCode(model.CountryCode2Letters)).Returns(dictCountry);
            dictionaryCultureService.Setup(f => f.GetCultureByCode(It.IsAny<string>())).Returns(dictCulture);
            userService.Setup(f => f.GetUserByEmail(model.Email)).Returns(null as User);
            userService.Setup(f => f.GetUserRoleByLevel(user, RoleLevel.Guest)).Returns(userRole);
            userService.Setup(f => f.MergeUserData(It.IsAny<User>(), It.IsAny<User>(), false)).Returns(user);
            roleService.Setup(f => f.AddUserToRole(It.IsAny<User>(), RoleLevel.Owner)).Returns(userRole);
            authorizationService.Setup(f => f.GeneratePassword()).Returns("pass");
            settingsService.Setup(f => f.GetSettingValue(SettingKeyName.TicketSystemJoinEmailAddress)).Returns("test@test.com");

            var result = homeOwnerController.JoinConfirmation() as ViewResult;
            var returnedEmail = result.ViewData["Email"] as String;

            scheduledVisitsService.Verify(f => f.AddVisit(It.IsAny<ScheduledVisit>()), Times.Once(), Messages.MethodWasNotCalled);
            messageService.Verify(f => f.AddEmail(It.IsAny<OwnerJoiningEmail>(), "pl-PL", null, null), Times.Once(), Messages.MethodWasNotCalled);
            stateService.Verify(f => f.RemoveFromSession(SessionKeys.HomeownerBasicData.ToString()), Times.Once(), Messages.MethodWasNotCalled);
            userService.Verify(f => f.AddHomeowner(It.IsAny<User>()), Times.Once(), Messages.MethodWasNotCalled);

            Assert.IsNotNull(result, Messages.ObjectIsNull);
            Assert.AreEqual(model.Email, returnedEmail);
        }
    }
}
