﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.Helpers
{
    public static class StringWordCounter
    {
        /// <summary>
        /// Count words in given text. 
        /// 
        /// </summary>
        /// <param name="inputText"></param>
        /// <returns></returns>
        public static Int32 CountWords(this string inputText)
        {
            MatchCollection collection = Regex.Matches(inputText, @"[\S]+");
	        return collection.Count;
        }
    }
}
