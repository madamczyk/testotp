﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Common.Helpers
{
    public static class DateTimeHelper
    {
        /// <summary>
        /// Returns localized date pattern.
        /// The date format is short date, except month which is always "MMM" format
        /// </summary>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns>The localized date pattern</returns>
        public static String GetLocalizedDatePattern(System.Globalization.CultureInfo ci = null)
        {
            if (ci == null)
                ci = Thread.CurrentThread.CurrentCulture;

            string pattern = ci.DateTimeFormat.ShortDatePattern;
            pattern = Regex.Replace(pattern, "M{1,4}", "MMM");
            pattern = pattern.Replace("/", "-").Replace(".", "-");
            return pattern;
        }
        
        /// <summary>
        /// Returns localized date pattern.
        /// The date format contains digits and separators for ex. en-US localized is MM/dd/yyyy
        /// </summary>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns>The localized date pattern</returns>
        public static String GetLocalizedDigitsDatePattern(System.Globalization.CultureInfo ci = null)
        {
            if (ci == null)
            {
                ci = Thread.CurrentThread.CurrentCulture;
            }

            string datePattern = ci.DateTimeFormat.ShortDatePattern;
            datePattern = System.Text.RegularExpressions.Regex.Replace(datePattern, "d{1,4}", "dd");
            datePattern = System.Text.RegularExpressions.Regex.Replace(datePattern, "M{1,4}", "MM");
            return datePattern;
        }

        /// <summary>
        /// Returns localized time pattern
        /// </summary>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns></returns>
        public static String GetLocalizedTimePattern(System.Globalization.CultureInfo ci = null)
        {
            if (ci == null)
                ci = Thread.CurrentThread.CurrentCulture;

            return ci.DateTimeFormat.ShortTimePattern;
        }

        /// <summary>
        /// Returns localized date and time pattern
        /// The date format is short date, except month which is always "MMM" format
        /// The time format is short time
        /// </summary>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns></returns>
        public static String GetLocalizedDateTimePattern(System.Globalization.CultureInfo ci = null)
        {
            return String.Format("{0} {1}", GetLocalizedDatePattern(ci), GetLocalizedTimePattern(ci));
        }

        /// <summary>
        /// Returns localized date string. 
        /// The date format is short date, except month which is "MMM" format
        /// </summary>
        /// <param name="date">The dateTime to localize</param>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns>The localized date string</returns>
        public static String GetLocalizedDateString(DateTime date, System.Globalization.CultureInfo ci = null)
        {
            return date.ToString(GetLocalizedDatePattern(ci));
        }

        /// <summary>
        /// Returns localized date string. 
        /// The date format contains digits and separators for ex. en-US localized is MM/dd/yyyy
        /// </summary>
        /// <param name="date">The dateTime to localize</param>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns>The localized date string</returns>
        public static String GetLocalizedDigitsDateString(DateTime date, System.Globalization.CultureInfo ci = null)
        {
            return date.ToString(GetLocalizedDigitsDatePattern(ci));
        }

        /// <summary>
        /// Returns localized time string. 
        /// The date format is short date, except month which is "MMM" format
        /// </summary>
        /// <param name="time">The dateTime to localize</param>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns>The localized time string</returns>
        public static String GetLocalizedTimeString(DateTime time, System.Globalization.CultureInfo ci = null)
        {
            return time.ToString(GetLocalizedTimePattern(ci));
        }

        /// <summary>
        /// Returns localized date and time string. 
        /// The date format is short date, except month which is "MMM" format
        /// </summary>
        /// <param name="time">The dateTime to localize</param>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns>The localized time string</returns>
        public static String GetLocalizedDateTimeString(DateTime dateTime, System.Globalization.CultureInfo ci = null)
        {
            return dateTime.ToString(GetLocalizedDateTimePattern(ci));
        }
    }
}
