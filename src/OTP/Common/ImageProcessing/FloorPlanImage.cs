﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Common.ImageProcessing
{
    public static class FloorPlanImage
    {
        /// <summary>
        /// Convert floor plan to a simplified version (gray-scale, transparent backgrounds, color replacement)
        /// </summary>
        /// <param name="fileStream">The image file stream</param>
        /// <returns>The converted image</returns>
        public static Image Convert(Stream fileStream)
        {
            using (var image = Image.FromStream(fileStream))
            {
                return Convert(image);
            }
        }

        /// <summary>
        /// Convert floor plan to a simplified version (gray-scale, transparent backgrounds, color replacement)
        /// </summary>
        /// <param name="imageBitmap">The image</param>
        /// <returns>The converted image</returns>
        public static Image Convert(Image image)
        {
            List<double> transparentColors = new List<double>() { 0, 194 }; // colors to become transparent
            Dictionary<double, Bgra> replacementColors = new Dictionary<double, Bgra>() { { 61, new Bgra(255, 255, 255, 255) } }; // grayscale colors to be replaced with BGRA colors

            // convert image to gray-scale
            Image<Gray, byte> grayImage = new Image<Gray, byte>(new Bitmap(image));

            // move the white and black point to 230 and 70
            for (int i = 0; i < grayImage.Height; i++)
                for (int j = 0; j < grayImage.Width; j++)
                    grayImage[i, j] = new Gray(System.Convert.ToByte(Math.Max(0, Math.Min(255, ((grayImage[i, j].Intensity - 70) * 255) / 160))));

            // convert image to BGR with alpha channel
            Image<Bgra, Byte> finalImage = grayImage.Convert<Bgra, Byte>();

            // make specific colors transparent and replace other colors
            for (int i = 0; i < finalImage.Height; i++)
                for (int j = 0; j < finalImage.Width; j++)
                {
                    var pixel = finalImage[i, j];

                    // after converting from grayscale, all components are the same, so just use blue component
                    if (transparentColors.Contains(pixel.Blue))
                    {
                        pixel.Alpha = 0;
                        finalImage[i, j] = pixel;
                    }
                    else if (replacementColors.ContainsKey(pixel.Blue))
                    {
                        finalImage[i, j] = replacementColors[pixel.Blue];
                    }
                }

            return finalImage.ToBitmap();
        }
    }
}
