﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Validation
{
    /// <summary>
    /// Class conatains methods used to validate bank account related data
    /// </summary>
    public static class BankData
    {
        /// <summary>
        /// Validate given routing number, that is: 9 characters long, only numbers, has a valid checksum
        /// </summary>
        /// <param name="routingNumber">routing number to validate</param>
        /// <returns>true - if routing number is valid</returns>
        public static bool ValidateRoutingNumber(string routingNumber)
        {
            if (routingNumber.Length != 9 || !System.Text.RegularExpressions.Regex.IsMatch(routingNumber, @"^[0-9]+$"))
                return false;

            int[] digits = routingNumber.Select(c => (int)c - '0').ToArray();

            if (((3 * (digits[0] + digits[3] + digits[6]) + 7 * (digits[1] + digits[4] + digits[7]) + (digits[2] + digits[5] + digits[8])) % 10) == 0)
                return true;

            return false;
        }
    }
}
