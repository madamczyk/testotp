﻿using System;

namespace Common.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Limits the string to given length, if string is longer then chosen length.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length">maximum length of string to be returned (does not include indicator)</param>
        /// <param name="indicator">string added at the end of limited string</param>
        /// <returns></returns>
        public static String Limit(this string text, int length = 25, string indicator = "...")
        {
            if (String.IsNullOrEmpty(text))
                return "";

            if (text.Length > length)
                return text.Substring(0, length) + indicator;
            else
                return text;
        }
    }
}
