﻿using Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Returns localized date string. 
        /// The date format is short date, except month which is "MMM" format
        /// </summary>
        /// <param name="dateTime">The DateTime object to be localized</param>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns>The localized date string</returns>
        public static String ToLocalizedDateString(this DateTime dateTime, System.Globalization.CultureInfo ci = null)
        {
            return DateTimeHelper.GetLocalizedDateString(dateTime, ci);
        }

        /// <summary>
        /// Returns localized date string. 
        /// The date format contains digits and separators for ex. en-US localized is MM/dd/yyyy
        /// </summary>
        /// <param name="dateTime">The DateTime object to be localized</param>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns>The localized date string</returns>
        public static String ToLocalizedDigitsDateString(this DateTime dateTime, System.Globalization.CultureInfo ci = null)
        {
            return DateTimeHelper.GetLocalizedDigitsDateString(dateTime, ci);
        }

        /// <summary>
        /// Returns localized time string. 
        /// The time format is short time
        /// </summary>
        /// <param name="dateTime">The DateTime object to be localized</param>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns>The localized date string</returns>
        public static String ToLocalizedTimeString(this DateTime dateTime, System.Globalization.CultureInfo ci = null)
        {
            return DateTimeHelper.GetLocalizedTimeString(dateTime, ci);
        }

        /// <summary>
        /// Returns localized date and time string. 
        /// The date format is short date, except month which is "MMM" format
        /// The time format is short time
        /// </summary>
        /// <param name="dateTime">The DateTime object to be localized</param>
        /// <param name="ci">The cultureInfo to be used when localizing date. By default uses Thread.CurrentCulture</param>
        /// <returns>The localized date string</returns>
        public static String ToLocalizedDateTimeString(this DateTime dateTime, System.Globalization.CultureInfo ci = null)
        {
            return DateTimeHelper.GetLocalizedDateTimeString(dateTime, ci);
        }

        public static DateTime CleanMinAndSec(this DateTime dateTime)
        {
            return dateTime.Date.AddHours(dateTime.TimeOfDay.Hours);
        }
    }
}
