﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Resources;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Common
{
    public static class ResourceHelper
    {
        public static string ToBase64(string fileName)
        {
            ResourceManager rm = new ResourceManager("Common.Resources.Logos", Assembly.GetExecutingAssembly());

            var resObj = rm.GetObject(fileName);

            if (resObj != null)
            {
                MemoryStream stream = new MemoryStream();

                ((Bitmap)resObj).Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Seek(0, SeekOrigin.Begin);

                return Convert.ToBase64String(stream.ToArray());
            }
            else
                return String.Empty;
        }

        public static string ToBase64(Image image, ImageFormat format)
        {
            using (var ms = new MemoryStream())
            {
                image.Save(ms, format);

                return Convert.ToBase64String(ms.ToArray());
            }
        }
    }
}
