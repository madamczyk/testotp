﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Converters
{
    public static class Base64
    {
        public static string Encode(string text)
        {
            if (String.IsNullOrWhiteSpace(text))
                return "";

            byte[] byteArray = UTF8Encoding.UTF8.GetBytes(text);

            return Convert.ToBase64String(byteArray);
        }

        public static string Decode(string encodedText)
        {
            if (String.IsNullOrWhiteSpace(encodedText))
                return "";

            byte[] byteArray = Convert.FromBase64String(encodedText);

            return UTF8Encoding.UTF8.GetString(byteArray);
        }
    }
}
