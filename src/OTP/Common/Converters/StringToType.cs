﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace Common.Converters
{
    public static class StringToType
    {
        public static decimal? ToDecimal(this string input, bool forceValue)
        {
            decimal retVal;
            try
            {
                retVal = decimal.Parse(input);                
            }
            catch(Exception )
            {
                if (forceValue)
                return 0;
            else
                return (decimal?)null;
            }
            return retVal;
        }

        public static int? ToInt(this string input)
        {
            int retValue;
            if (!string.IsNullOrWhiteSpace(input) && int.TryParse(input, out retValue))
                return retValue;
            else
                return (int?)null;
        }

        public static DateTime? ToDateTime(this string input)
        {
            DateTime retValue;
            if (!string.IsNullOrWhiteSpace(input) && DateTime.TryParse(input, out retValue))
                return retValue;
            else
                return (DateTime?)null;
        }

        public static string ToSerializedList(this string input, string listName)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return null;    
            }
            string [] elements = input.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var query = new XElement(listName,
                        from id in elements
                        select 
                        new XElement("Item", 
                            new XAttribute("id", id)));
            return query.ToString();
        }

        public static List<int> ToIntList(this string input, string separator)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                var splitted = input.Split(separator.ToCharArray()).ToList();
                return splitted.Select(it => int.Parse(it)).ToList();
            }
            return null;
        }
    }
}
