﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    public class ExceptionHelper
    {
        static int MAX_RECURSION_LEVEL = 10;

        /// <summary>
        /// Formats error message basing on Exception object.
        /// </summary>
        /// <param name="exception"></param>
        /// <returns>Error message</returns>
        public static string FormatErrorMessage(Exception exception)
        {
            string errorMessage = string.Empty;
            int recursionLevel = 1;

            while (exception != null && recursionLevel <= MAX_RECURSION_LEVEL)
            {
                errorMessage += exception.Message + Environment.NewLine + Environment.NewLine;
                errorMessage += exception.StackTrace + Environment.NewLine + Environment.NewLine;
                exception = exception.InnerException;
                recursionLevel++;
            }
            return errorMessage;
        }
    }
}
