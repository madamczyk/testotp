﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    /// <summary>
    /// Created only for PaymentGateway process exception handling
    /// </summary>
    public class SecurityDepositException : Exception
    {
        public SecurityDepositException(string message, Exception innerException) :
            base(message, innerException)
        { }
    }
}
