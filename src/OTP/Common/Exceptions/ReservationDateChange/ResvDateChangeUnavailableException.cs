﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions.ReservationDateChange
{
    /// <summary>
    /// Reservation date chnage exception when unit is no available in selected date period
    /// </summary>
    public class ResvDateChangeUnavailableException : ResvDateChangeExceptionBase
    {

    }
}
