﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    public class TripNonCancellableException : Exception
    {
        public TripNonCancellableException()
            : base()
        {
        }

        public TripNonCancellableException(string message)
            :base(message)
        {
        }
    }
}
