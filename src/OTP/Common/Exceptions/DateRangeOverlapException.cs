﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    /// <summary>
    /// Thrown, when date ranges overlap one another
    /// </summary>
    public class DateRangeOverlapException : Exception
    {
        public DateRangeOverlapException() :
            base()
        { }
    }
}
