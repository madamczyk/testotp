﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    /// <summary>
    /// Thrown, when email sending was stopped because of critical error, without possibility of resending
    /// </summary>
    public class EmailCriticalException : Exception
    {
        public EmailCriticalException(string message, Exception innerException) :
            base(message, innerException)
        { }
    }
}
