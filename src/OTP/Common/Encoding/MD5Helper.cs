﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System;

namespace Common
{
    public class MD5Helper
    {
        public static string Encode(string stringToEncode)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(stringToEncode);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
