﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.CultureInfo
{
    public static class StringFormats
    {
        public static readonly string LongDate = "MM/dd/yyyy, hh:mm tt";
        public static readonly string ShortDate = "MM/dd/yyyy";
        public static readonly string TimeOnly = "hh:mm tt";
        public static readonly string TimeWithSeconds = "hh:mm:ss tt";
    }
}
