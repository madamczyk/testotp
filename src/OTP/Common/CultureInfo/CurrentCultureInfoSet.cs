﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public static class CurrentCultureInfoSet
    {
        public static string NumberSeparator
        {
            get { return System.Globalization.CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator; }
        }
    }
}
