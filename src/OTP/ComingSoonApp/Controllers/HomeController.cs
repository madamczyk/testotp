﻿using BusinessLogic.Managers;
using ComingSoonApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ComingSoonApp.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(SubscribeModel subscribe) 
        {
            if (ModelState.IsValid)
            {
                ViewBag.Message = "Thank you,<br /><br />We’ll send you an email when we launch!";
                RequestManager.Services.StorageTableService.AddSubscriber(subscribe.Email);
            }
            else
            {
                ViewBag.Message = "A problem occured with the subscription.";
            }
            return View();
        }
    }
}
