rem CopyResults.cmd JobName BuildName BuildDir ResultsDir
call %3\src\Builds\Azure\CmdScripts\CopyResults\copyDBLogs.cmd "%1" "%2" "%3"
call %3\src\Builds\Azure\CmdScripts\CopyResults\copyTestResults.cmd "%3" "%4"