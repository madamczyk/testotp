rem usage copyLogs.cmd JobName BuildNumber BuildDir
ECHO OFF

SET _date=%DATE:/=-%
SET DBLOG=C:\build-dir\OTP\results\%1_%2\Logs\SQLCMD_RecreateOTPDatabase_%_date%.log
SET DBLOG="%DBLOG:"=%"



rem DB Log
SET DBLOG1=%3\db\1.log
SET DBLOG2=%3\db\2.log
SET DBLOG3=%3\db\3.log
SET DBLOG4=%3\db\4.log

if exist %DBLOG1% (
	echo ----------------------------------- >> %DBLOG%
	echo Delete existing DB >> %DBLOG%
	echo ----------------------------------- >> %DBLOG%
	type %DBLOG1% >> %DBLOG%
	del %DBLOG1%
)

if exist %DBLOG2% (
	echo ----------------------------------- >> %DBLOG%
	echo Create OTPTest database >> %DBLOG%
	echo ----------------------------------- >> %DBLOG%
	type %DBLOG2% >> %DBLOG%
	del %DBLOG2%
)


if exist %DBLOG3% (
	echo ----------------------------------- >> %DBLOG%
	echo Load Base Data >> %DBLOG%
	echo ----------------------------------- >> %DBLOG%
	type %DBLOG3% >> %DBLOG%
	del %DBLOG3%
)


if exist %DBLOG4% (
	echo ----------------------------------- >> %DBLOG%
	echo Load Test Data >> %DBLOG%
	echo ----------------------------------- >> %DBLOG%
	type %DBLOG4% >> %DBLOG%
	del %DBLOG4%
)


