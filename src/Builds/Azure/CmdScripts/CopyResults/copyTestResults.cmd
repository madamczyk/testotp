rem Usage copyTestResults sourcePath resultsPath
ECHO OFF 

SET SOURCE=%1
SET SOURCE=%SOURCE:"=%
SET DESTINATION=%2
SET DESTINATION=%DESTINATION:"=%

for %%G IN ("%SOURCE%\*.trx") DO (
	move "%%G" "%DESTINATION%"
)

for /D %%G IN ("%SOURCE%\Bamboo*") DO (
	move "%%G" "%DESTINATION%"
	)
