rem CreateOTPTestSchema.cmd DevData UserName UserPass SqlServer
ECHO OFF
if exist "C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\100\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\100\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\90\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\90\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\80\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\80\Tools\Binn\SQLCMD.exe"
)

set ARGS= -U %2 -P %3 -d %6
set SQLINST= %4
set DEVDATA= %1
set PATH= %5
set TYPE= %7

rem Delete Old Files
set TMPFILE=%TMP%\otp_db_scripts.sql
if exist %TMPFILE% (
	del %TMPFILE%
)

set TMPFILETEST=%TMP%\otp_db_testdata.sql
if exist %TMPFILETEST% (
	del %TMPFILETEST%
)

set TMPFILEPRODUCTION=%TMP%\otp_db_productiondata.sql
if exist TMPFILEPRODUCTION=% (
	del %TMPFILEPRODUCTION%
)



rem dbschema.sql
type %PATH%\dbschema.sql >> %TMPFILE%
echo.>> %TMPFILE%
echo GO>> %TMPFILE%
echo.>> %TMPFILE%


rem Functions
for %%i in (%PATH%\Fn\*.sql) do (
	type %%i >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)

rem Procedures
for %%i in (%PATH%\Sp\*.sql) do (
	type %%i >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)

rem BaseData
type %PATH%\Data\BaseData.sql >> %TMPFILE%
echo.>> %TMPFILE%
echo GO>> %TMPFILE%
echo.>> %TMPFILE%

rem Staging
if %TYPE% EQU 1 (
	type %PATH%\RebuildScripts\UpdateOtpSettingsStaging.sql >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)

rem Development
if %TYPE% EQU 2 (
	type %PATH%\RebuildScripts\UpdateOtpSettingsDev.sql >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)

rem TestData
if %DEVDATA% EQU 1 (
	type %PATH%\Data\TestData.sql >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)

rem Execute Script
%SQLCMD% -I %ARGS% -S %SQLINST% -i %TMPFILE% 

if %errorlevel% EQU 0 (
	del %TMPFILE%
	if exist %TMPFILETEST% (
		del %TMPFILETEST%
	)
) 