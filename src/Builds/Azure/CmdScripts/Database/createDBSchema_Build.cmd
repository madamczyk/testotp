rem Usage CreateDB_Build.cmd SQLCMD CMDARGS SQLINSTANCE DEVDATA

rem Copy all scripts into a temp file
set TMPFILE=%TMP%\otp_db_scripts.sql
if exist %TMPFILE% (
	del %TMPFILE%
)

set TMPFILETEST=%TMP%\otp_db_testdata.sql
if exist %TMPFILETEST% (
	del %TMPFILETEST%
)

set ARGS= %2 -S %3 -d OTPTest
set DEVDATA= %4
set PATH = %5

rem dbschema.sql
type %PATH%\dbschema.sql >> %TMPFILE%
echo.>> %TMPFILE%
echo GO>> %TMPFILE%
echo.>> %TMPFILE%


rem Functions
for %%i in (%PATH%\Fn\*.sql) do (
	type %%i >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)

rem Procedures
for %%i in (%PATH%\Sp\*.sql) do (
	type %%i >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)


rem BaseData
type %PATH%\Data\BaseData.sql >> %TMPFILE%
echo.>> %TMPFILE%
echo GO>> %TMPFILE%
echo.>> %TMPFILE%

rem Execute Script
%1 %ARGS% -i %TMPFILE% >> %PATH%\3.log

rem TestData
if %errorlevel% EQU 0 if %DEVDATA% EQU 1 (
	type %PATH%\Data\TestData.sql >> %TMPFILETEST%
	echo.>> %TMPFILETEST%
	
	rem Execute TestData Script
	%1 %ARGS% -i %TMPFILETEST% >> %PATH%\4.log
)

if %errorlevel% EQU 0 (
	del %TMPFILE%
	if exist %TMPFILETEST% (
	 	del %TMPFILETEST%
	)
) else (
	ECHO ON
	ECHO !!!!!!!!!!!!!!!!!!!!!!
	ECHO Error while creating [OTPTest] Database
	ECHO Check log file for details
	ECHO !!!!!!!!!!!!!!!!!!!!!!
)




