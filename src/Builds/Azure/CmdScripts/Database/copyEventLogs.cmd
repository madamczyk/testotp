rem copyEventLogs.cmd SQLInst type
ECHO OFF
if exist "C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\100\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\100\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\90\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\90\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\80\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\80\Tools\Binn\SQLCMD.exe"
)

set SQLINST= 'localhost\sqlexpress'
set TYPE= %1
set PATH= %2

rem Delete Old Files
set TMPFILE=%TMP%\otp_copyEventLog.sql
if exist %TMPFILE% (
	del %TMPFILE%
)
rem Staging
if %TYPE% EQU 1 (
	type %PATH%\RebuildScripts\CopyEventLogStaging.sql >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)
rem Development
if %TYPE% EQU 2 (
	type %PATH%\RebuildScripts\CopyEventLogDev.sql >> %TMPFILE%
	echo.>> %TMPFILE%
	echo GO>> %TMPFILE%
	echo.>> %TMPFILE%
)


rem Execute Script
%SQLCMD% -I -S localhost\sqlexpress -i %TMPFILE% 