rem CreateOTPTestSchema.cmd DevData UserName UserPass SqlServer
ECHO OFF
if exist "C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\100\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\100\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\90\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\90\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\80\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\80\Tools\Binn\SQLCMD.exe"
)

set ARGS= -U %1 -P %2 -d %4
set SQLINST= %3


rem Execute query
%SQLCMD% -I %ARGS% -S %SQLINST% -q "DELETE FROM dbo.[EventLog]"
