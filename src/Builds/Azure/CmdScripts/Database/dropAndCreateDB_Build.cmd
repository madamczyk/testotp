ECHO OFF
if exist "C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\100\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\100\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\90\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\90\Tools\Binn\SQLCMD.exe"
) else if exist "C:\Program Files\Microsoft SQL Server\80\Tools\Binn\SQLCMD.exe" (
	set SQLCMD="C:\Program Files\Microsoft SQL Server\80\Tools\Binn\SQLCMD.exe"
)


set SQLINST= localhost\sqlexpress
set PATH= %2
set PATHSRC= %3

rem Drop OTPTest Database
%SQLCMD% -I -S %SQLINST% -i %PATH%\AdminScripts\DropOTPDB.sql >> %PATH%\1.log

if %errorlevel% EQU 0 (
	rem Create db
	%SQLCMD% -I -S %SQLINST% -i %PATH%\AdminScripts\CreateOTPDB.sql >> %PATH%\2.log	
)

if %errorlevel% EQU 0 (
	%PATHSRC%\src\Builds\Azure\CmdScripts\Database\createDBSchema_Build.cmd %SQLCMD% -I %SQLINST% %1 %PATH%	
) else ( 
	ECHO ON
	ECHO !!!!!!!!!!!!!!!!!!!!!!
	ECHO Error while recreating [OTPTest] Database
	ECHO Check log file for details
	ECHO !!!!!!!!!!!!!!!!!!!!!!
)

