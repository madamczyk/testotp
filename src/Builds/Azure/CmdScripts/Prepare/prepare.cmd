if not exist c:\build-dir\OTP\results\%1_%2 (
	mkdir c:\build-dir\OTP\results\%1_%2
	mkdir c:\build-dir\OTP\results\%1_%2\TestResults
	mkdir c:\build-dir\OTP\results\%1_%2\Packages
	mkdir c:\build-dir\OTP\results\%1_%2\Bin
	mkdir c:\build-dir\OTP\results\%1_%2\Logs
)

