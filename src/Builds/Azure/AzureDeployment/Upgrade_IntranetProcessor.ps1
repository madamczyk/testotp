Param(  $serviceName = "lgbsotpservice",
        $storageAccountName = "testohtheplacesstorage",
		$packageDir ="",
        $packageLocation = "$packageDir\Packages\IntranetProcessor\ IntranetProcessor.cspkg",
        $cloudConfigLocation = "$packageDir\Packages\IntranetProcessor\ ServiceConfiguration.Cloud.cscfg",
        $environment = "Staging",
        $deploymentLabel = "$servicename",
        $timeStampFormat = "g",
        $enableDeploymentUpgrade = 1,
        $selectedsubscription = "default"
     )


function Publish()
{
    $deployment = Get-AzureDeployment -ServiceName $serviceName -Slot $slot -ErrorVariable a -ErrorAction silentlycontinue 
    if ($a[0] -ne $null)
    {
        Write-Output "No deployment found. "
    }
	#if deployment exists, upgrade it
    #check for existing deployment and then either upgrade, delete + deploy, or cancel according to $alwaysDeleteExistingDeployments and $enableDeploymentUpgrade 

    if ($deployment.Name -ne $null)
    {
		 Write-Output "Deployment found in $servicename.  Upgrading deployment."
		UpgradeDeployment        
    } 
}

function UpgradeDeployment()
{
    write-progress -id 3 -activity "Upgrading Deployment" -Status "In progress"
    Write-Output "Upgrading Deployment: In progress"

    # perform Update-Deployment
    $setdeployment = Set-AzureDeployment -Upgrade -Slot $slot -Package $packageLocation -Configuration $cloudConfigLocation -label $deploymentLabel -ServiceName $serviceName -Force

    $completeDeployment = Get-AzureDeployment -ServiceName $serviceName -Slot $slot
    $completeDeploymentID = $completeDeployment.deploymentid

    write-progress -id 3 -activity "Upgrading Deployment" -completed -Status "Complete"
    Write-Output "Upgrading Deployment: Complete, Deployment ID: $completeDeploymentID"
}


#set remaining environment variables for Azure cmdlets
$subscription = Get-AzureSubscription $selectedsubscription
$subscriptionname = $subscription.subscriptionname
$subscriptionid = $subscription.subscriptionid
$slot = $environment

#main driver - publish & write progress to activity log
Write-Output "Azure Cloud Service deploy script started."
Write-Output "Preparing deployment of $deploymentLabel for $subscriptionname with Subscription ID $subscriptionid."

Publish

$deployment = Get-AzureDeployment -slot $slot -serviceName $servicename
$deploymentUrl = $deployment.Url

Write-Output "Created Cloud Service with URL $deploymentUrl."
Write-Output "Azure Cloud Service deploy script finished."