$ip = (New-Object System.Net.WebClient).DownloadString("http://169.254.169.254/latest/meta-data/public-ipv4")
cd C:\
Get-ChildItem "C:\Program Files (x86)\Microsoft SDKs\Windows Azure\PowerShell\Azure\*.psd1" | ForEach-Object {Import-Module $_}

$path = $args[0]
$dbpath = $args[1]
$strpass = $args[2]
$strsql = $args[3]
$strdbName = $args[4]
$strsqlDev = $args[5]

$subName = "'Oh the place DEV'"
$pass = cat C:\build-dir\otp\securepass_dev.txt | convertto-securestring
if ($strdbName -eq "OTPDev")
{
	$storagename = "otpstoragedev"		
}
else 
{
	$storagename = "otpstoragetest"		
}

$commandLine = "Set-AzureSubscription -SubscriptionName $subName -CurrentStorageAccount $storageName"
Write-Output "Setting Azure Subscription $subName storage account ($storageName)...."
invoke-expression -Command "$commandLine"
$commandLine = "Select-AzureSubscription -SubscriptionName $subName"
Write-Output "Selecting $subName Azure Subscription...."
invoke-expression -Command "$commandLine"
$commandLine = "Get-AzureSubscription"
invoke-expression -Command "$commandLine"

Write-Output "Creating new firewall rule (IP: $ip) for server $strsql..."
$commandLine = "New-AzureSqlDatabaseServerFirewallRule –RuleName BambooBuild -ServerName $strsql -StartIpAddress $ip -EndIpAddress $ip"
invoke-expression -Command "$commandLine"

Write-Output 'Copying EventLog entries...'
if ($strdbName -eq "OTPDev")
{
	$commandLine = "$path\copyEventLogs.cmd 2 $dbpath"
}
else
{
	$commandLine = "$path\copyEventLogs.cmd 1 $dbpath"
}
invoke-expression -Command "$commandLine"


$sqlAdmin = "AzureAdmin@$strsql"

Write-Output 'Creating credentials...'
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $sqlAdmin,$pass    
Write-Output 'Creating context...'   
$ctx = New-AzureSqlDatabaseServerContext -ServerName $strsql -Credential $cred         

Write-Output 'Removing old database...'
$commandLine = 'Remove-AzureSqlDatabase -Context $ctx -DatabaseName $strdbName -Force'
invoke-expression -Command "$commandLine"

Write-Output "Creating new $strdbName database...."
$commandLine = 'New-AzureSqlDatabase -Context $ctx -Database $strdbName'
invoke-expression -Command "$commandLine"

Write-Output "Creating $strdbName schema..."
if ($strdbName -eq "OTPDev")
{
	$commandLine = "$path\CreateOTPTestSchema.cmd 1 $sqlAdmin '$strpass' $strsql.database.windows.net $dbpath $strdbName 2"
}
else
{
	$commandLine = "$path\CreateOTPTestSchema.cmd 1 $sqlAdmin '$strpass' $strsql.database.windows.net $dbpath $strdbName 1"
}
invoke-expression -Command "$commandLine"

Write-Output 'Removing firewall rule...'
$commandLine = "Remove-AzureSqlDatabaseServerFirewallRule –RuleName BambooBuild -ServerName $strsql"
invoke-expression -Command "$commandLine"