$ip = (New-Object System.Net.WebClient).DownloadString("http://169.254.169.254/latest/meta-data/public-ipv4")
cd C:\
Get-ChildItem "C:\Program Files (x86)\Microsoft SDKs\Windows Azure\PowerShell\Azure\*.psd1" | ForEach-Object {Import-Module $_}

$path = $args[0]
$strpass = $args[1]
$strsql = $args[2]
$strdbName = $args[3]

$subName = "'Oh the place DEV'"
$pass = cat C:\build-dir\otp\securepass_dev.txt | convertto-securestring

Write-Output "Creating new firewall rule (IP: $ip) for server $strsql (to clear EventLog)..."
$commandLine = "New-AzureSqlDatabaseServerFirewallRule –RuleName BambooBuild -ServerName $strsql -StartIpAddress $ip -EndIpAddress $ip"
invoke-expression -Command "$commandLine"


$sqlAdmin = "AzureAdmin@$strsql"
Write-Output 'Creating credentials...'
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $sqlAdmin,$pass    
Write-Output 'Creating context...'   
$ctx = New-AzureSqlDatabaseServerContext -ServerName $strsql -Credential $cred         

Write-Output 'Clear EventLog from entries added during database rebuilding...'
$commandLine = "$path\clearEventLogs.cmd $sqlAdmin '$strpass' $strsql.database.windows.net $strdbName"
invoke-expression -Command "$commandLine"

Write-Output 'Removing firewall rule...'
$commandLine = "Remove-AzureSqlDatabaseServerFirewallRule –RuleName BambooBuild -ServerName $strsql"
invoke-expression -Command "$commandLine"