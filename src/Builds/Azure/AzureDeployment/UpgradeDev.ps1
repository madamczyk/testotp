cd C:\
Get-ChildItem "C:\Program Files (x86)\Microsoft SDKs\Windows Azure\PowerShell\Azure\*.psd1" | ForEach-Object {Import-Module $_}
cd $args[0]

$scriptsPath = $args[0]
$packagePath = $args[1]
$storagename = "otpstoragedev"
$subName = "'Oh the place DEV'"

$commandLine = "Set-AzureSubscription -SubscriptionName $subName -CurrentStorageAccount $storageName"
Write-Output "Setting Azure Subscription $subName storage account ($storageName)...."
invoke-expression -Command "$commandLine"
$commandLine = "Select-AzureSubscription -SubscriptionName $subName"
Write-Output "Selecting $subName Azure Subscription...."
invoke-expression -Command "$commandLine"

$serviceConfPath = "'$packagePath\Packages\Intranet\ ServiceConfiguration.CloudDev.cscfg'"
$commandLine = "$scriptsPath\Upgrade_Intranet.ps1 -packageDir $packagePath -serviceName OTPBackEndDev -storageAccountName $storagename -cloudConfigLocation $serviceConfPath"
Write-Output 'Starting upgrade of Intranet instances...'
invoke-expression -Command "$commandLine"
Write-Output 'Finished upgrade of Intranet...'

$serviceConfPath = "'$packagePath\Packages\Extranet\ ServiceConfiguration.CloudDev.cscfg'"
$commandLine = "$scriptsPath\Upgrade_Extranet.ps1 -packageDir $packagePath -serviceName OTPFrontEndDev -storageAccountName $storagename -cloudConfigLocation $serviceConfPath"
Write-Output 'Starting upgrade of Extranet instances...'
invoke-expression -Command "$commandLine"
Write-Output 'Finished upgrade of Extranet...'

$serviceConfPath = "'$packagePath\Packages\IntranetProcessor\ ServiceConfiguration.CloudDev.cscfg'"
$commandLine = "$scriptsPath\Upgrade_IntranetProcessor.ps1 -packageDir $packagePath -serviceName OTPProcessorDev -storageAccountName $storagename -cloudConfigLocation $serviceConfPath"
Write-Output 'Starting upgrade of IntranetProcessor instances...'
invoke-expression -Command "$commandLine"
Write-Output 'Finished upgrade of IntranetProcessor...'

